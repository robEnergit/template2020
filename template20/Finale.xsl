<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" />
	
	
	
	
	

	<xsl:template name="FINALE">
	
	<xsl:param name="comunicazione_area_clienti">
		<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI']">SI</xsl:if>
		<xsl:if test="not(./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI'])">NO</xsl:if>
	</xsl:param>
	
	<xsl:param name="color"/>
	
	<xsl:param name="f3_color">
		<xsl:if test="$color='black'">
			#DDDDDD
		</xsl:if>
		<xsl:if test="not($color='black')">
			#a3ecad
		</xsl:if>
	</xsl:param>
	<xsl:param name="f2_color">
		<xsl:if test="$color='black'">
			#BBBBBB
		</xsl:if>
		<xsl:if test="not($color='black')">
			#eaed94
		</xsl:if>
	</xsl:param>
	<xsl:param name="f1_color">
		<xsl:if test="$color='black'">
			#FFFFFF
		</xsl:if>
		<xsl:if test="not($color='black')">
			#e5c1c1
		</xsl:if>
	</xsl:param>
	
	
	<xsl:param name="bordi"/>
	
		<!--
			Tutto il contenuto è racchiuso in un blocco con un valore
			orphans molto alto, in questo modo tutti i block figli
			ereditano questo attributo, che fa in modo che di fatto
			un blocco (paragrafo) non sia mai diviso tra due pagine 
		 -->
		<fo:block widows="3" orphans="3">
			
			<!-- Blocco padre per impostazioni "globali" -->
			<fo:block xsl:use-attribute-sets="font.table">
			
				<!-- Riepilogo fattura -->
				<fo:table space-before="5mm" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(22)"/>
					<fo:table-column column-width="proportional-column-width(13)"/>
					<fo:table-column column-width="proportional-column-width(13)"/>
					<fo:table-column column-width="proportional-column-width(13)"/>
					<fo:table-column column-width="proportional-column-width(13)"/>
					<fo:table-column column-width="proportional-column-width(13)"/>
					<fo:table-column column-width="proportional-column-width(13)"/>
				
					<fo:table-header>
						
						<fo:table-row>
							<fo:table-cell xsl:use-attribute-sets="cell.header"
										   number-columns-spanned="7">
								<fo:block>
									<fo:inline>RIEPILOGO FATTURA</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
				
						<fo:table-row text-align="center">
							<fo:table-cell>
								<fo:block>SERVIZIO</fo:block>
							</fo:table-cell>
				
							<fo:table-cell>
								<fo:block>N. CONTRATTO</fo:block>
							</fo:table-cell>
				
							<fo:table-cell>
								<fo:block>IMPONIBILE</fo:block>
							</fo:table-cell>
				
							<fo:table-cell>
								<fo:block>IVA</fo:block>
							</fo:table-cell>
				
							<fo:table-cell>
								<fo:block>CODICE IVA</fo:block>
							</fo:table-cell>
				
							<fo:table-cell>
								<fo:block>IMPORTO IVA</fo:block>
							</fo:table-cell>
				
							<fo:table-cell>
								<fo:block>TOTALE</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
				
					<fo:table-body>
						<xsl:for-each select="RIEPILOGO_FATTURA">
							<fo:table-row text-align="center">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="PRODOTTO_SERVIZIO" />
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="CONTRATTO" />
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="IMPONIBILE" />
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="ALIQUOTA_IVA" />%
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										<xsl:choose>
											<xsl:when test="CODICE_IVA">
												<xsl:value-of select="CODICE_IVA" />
											</xsl:when>
											<xsl:otherwise>
												-
											</xsl:otherwise>
										</xsl:choose>
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="IMPORTO_IVA" />
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="TOTALE" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:for-each>
				
						<fo:table-row keep-with-previous="always" height="1mm">
							<fo:table-cell number-columns-spanned="7"
										   xsl:use-attribute-sets="cell.header">
								<fo:block />
							</fo:table-cell>
						</fo:table-row>
						
						<xsl:for-each select="DESCRIZIONI_CODICI_IVA">
							<fo:table-row keep-with-previous="always">
								<fo:table-cell>
									<fo:block>
										Codice IVA "<xsl:value-of select="CODICE_IVA" />"
									</fo:block>
								</fo:table-cell>
								<fo:table-cell number-columns-spanned="6">
									<fo:block>
										<xsl:value-of select="DESCRIZIONE_CODICE_IVA" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:for-each>
					</fo:table-body>
				</fo:table>
				
				<!-- Elenco fatture in attesa di pagamento -->
				<xsl:if test="FATTURE_ATTESA_PAGAMENTO">
					<fo:table table-layout="fixed" width="100%" space-before="5mm">
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(20)" />
						
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block>
										ELENCO FATTURE IN ATTESA DI PAGAMENTO
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row text-align="center">
								<fo:table-cell>
									<fo:block>
										NUMERO FATTURA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA EMISSIONE
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA SCADENZA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DOVUTO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO PAGATO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DA PAGARE
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
				
						<fo:table-body>
							<xsl:for-each select="FATTURE_ATTESA_PAGAMENTO">
								<fo:table-row text-align="center">
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="NUMERO_FATTURA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_EMISSIONE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_SCADENZA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="TOTALE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="IMPORTO_PAGATO" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="INSOLUTO" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
				
							<fo:table-row keep-with-previous="always" height="1mm">
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block />
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row keep-with-previous="always">
								<fo:table-cell number-columns-spanned="5">
									<fo:block />
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block text-align="center" font-size="8pt" font-family="universcbold">
										<xsl:value-of select="TOTALE_FATTURE_ATTESA_PAGAMENTO" /> euro
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					
				</xsl:if>
			</fo:block>
			
			
			<!-- Frase "perchè pagare entro la data di scadenza..." -->
			<fo:block space-before="5mm" xsl:use-attribute-sets="font.pagare">
				<fo:block font-family="universcbold">
					Perchè pagare entro la data di scadenza.
				</fo:block>

				<fo:block keep-with-previous="always">
					La fattura deve essere pagata entro la data di
					scadenza, pena la sospensione del servizio.
					In caso di ritardo sarà addebitata una indennità
					di mora pari al saggio di interesse legale,
					calcolata dal giorno successivo alla scadenza
					del termine previsto per il pagamento.
				</fo:block>
			</fo:block>
			
			
			<!-- Schema fasce di consumo -->
			<xsl:if test="CONTRATTO_ENERGIA">
				<fo:block space-before="5mm" xsl:use-attribute-sets="font.distrib">
					DISTRIBUZIONE FASCE ORARIE CONSUMI
					(DELIBERA 181/06 AUTORITÀ PER L'ENERGIA
					ELETTRICA E IL GAS)
				</fo:block>
				
				
				<fo:table table-layout="fixed" width="100%"
						xsl:use-attribute-sets="font.fascia_header"
						border-collapse="separate" space-before="1mm"
						keep-with-previous="always">
					<fo:table-column column-width="proportional-column-width(100)" />
					<fo:table-body>
				
				
						<fo:table-row keep-with-previous="always">
							<fo:table-cell>
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(11)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
				
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
				
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
				
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
				
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
									<fo:table-column column-width="proportional-column-width(3.5)" />
				
									<fo:table-body>
										<fo:table-row keep-with-previous="always" text-align="center">
											<fo:table-cell><fo:block>&#160;</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border"><fo:block>0-1</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>1-2</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>2-3</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>3-4</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>4-5</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>5-6</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>6-7</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>7-8</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>8-9</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>9-10</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>10-11</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>11-12</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>12-13</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>13-14</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>14-15</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>15-16</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>16-17</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>17-18</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>18-19</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>19-20</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>10-21</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>21-22</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>22-23</fo:block></fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>23-24</fo:block></fo:table-cell>
										</fo:table-row>
										
										<fo:table-row keep-with-previous="always">
											<fo:table-cell>
												<fo:table table-layout="fixed" width="100%">
													<fo:table-column column-width="proportional-column-width(100)" />
				
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell xsl:use-attribute-sets="border.left border.top border.bottom">
																<fo:block text-align="center">lun</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">mar</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">mer</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">gio</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">ven</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">sab</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">dom e festivi</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:table-cell>
				
				
											<fo:table-cell background-color="{$f3_color}"
														   xsl:use-attribute-sets="border" number-columns-spanned="24">
				
												<fo:table table-layout="fixed" width="100%">
													<fo:table-column column-width="proportional-column-width(24.5)" />
													<fo:table-column column-width="proportional-column-width(56)" />
													<fo:table-column column-width="proportional-column-width(3.5)" />
													<fo:table-body>
														<fo:table-row>
				
															<fo:table-cell>
																<fo:table table-layout="fixed" width="100%">
																	<fo:table-column column-width="proportional-column-width(100)" />
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>&#160;</fo:block>
																				<fo:block>&#160;</fo:block>
																				<fo:block text-align="center" xsl:use-attribute-sets="font.fascia">F3</fo:block>
																				<fo:block>&#160;</fo:block>
																				<fo:block>&#160;</fo:block>
																				<fo:block>&#160;</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell>
																				<fo:block>&#160;</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:table-cell>
				
															<fo:table-cell>
																<fo:table table-layout="fixed" width="100%"
																		  xsl:use-attribute-sets="border1.left border1.bottom border1.right"
																		  background-color="{$f2_color}">
																	<fo:table-column column-width="proportional-column-width(3.5)" />
																	<fo:table-column column-width="proportional-column-width(38.5)" />
																	<fo:table-column column-width="proportional-column-width(14)" />
																	<fo:table-body>
																		
																		
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block text-align="center"/>
																			</fo:table-cell>
																			<fo:table-cell background-color="{$f1_color}" display-align="center"
																				xsl:use-attribute-sets="border1.bottom border1.right border1.left">
																				<fo:block>&#160;</fo:block>
																				<fo:block>&#160;</fo:block>
																				<fo:block text-align="center" xsl:use-attribute-sets="font.fascia">F1</fo:block>
																				<fo:block>&#160;</fo:block>
																				<fo:block>&#160;</fo:block>
																			</fo:table-cell>
																			<fo:table-cell display-align="center">
																				<fo:block>&#160;</fo:block>
																				<fo:block>&#160;</fo:block>
																				<fo:block text-align="center" xsl:use-attribute-sets="font.fascia">F2</fo:block>
																				<fo:block>&#160;</fo:block>
																				<fo:block>&#160;</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row >
																			<fo:table-cell number-columns-spanned="3">
																				<fo:block>&#160;</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:table-cell>
				
															<fo:table-cell>
																<fo:block>&#160;</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
				
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
								
								
								<fo:block xsl:use-attribute-sets="font.legenda" space-before="2mm" keep-with-previous="always">
									<fo:block>
										<fo:inline font-weight="bold">F1:</fo:inline>
										ore di punta (peak). Nei giorni dal lunedì al venerdì: dalle
										ore 8.00 alle ore 19.00
									</fo:block>
								
									<fo:block>
										<fo:inline font-weight="bold">F2:</fo:inline>
										ore intermedie (mid-level). Nei giorni dal lunedì al venerdì:
										dalle ore 7.00 alle ore 8.00 e dalle ore 19.00 alle ore 23.00.
										Il sabato: dalle ore 7.00 alle ore 23.00
									</fo:block>
								
									<fo:block>
										<fo:inline font-weight="bold">F3:</fo:inline>
										ore fuori punta (off-peak). Nei giorni dal lunedì al venerdì:
										dalle ore 23.00 alle ore 7.00. La domenica e i festivi*: tutte
										le ore della giornata
									</fo:block>
								
									<fo:block font-size="5pt">
										* Si considerano festivi: 1 gennaio; 6 gennaio;
										lunedì di Pasqua; 25 Aprile; 1 maggio; 2 giugno;
										15 agosto; 1 novembre; 8 dicembre; 25 dicembre;
										26 dicembre
									</fo:block>
								</fo:block>
				
							</fo:table-cell>
						</fo:table-row>
				
					</fo:table-body>
				</fo:table>
			</xsl:if>
			
			<xsl:if test="$comunicazione_area_clienti='SI' or
						  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
						  CONTRATTO_ENERGIA[@CONSUMER='SI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
						  RINNOVI or
						  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
						  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
						  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
						  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
						  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
						  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
						  PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI']">
			
								
				<fo:table space-before="5mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(100)"/>
					
					<!-- <fo:table-header>
						<fo:table-row height="10mm">
							<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
								</xsl:if>
								<fo:block font-family="universcbold" font-size="11pt" xsl:use-attribute-sets="brd.b.000">
									COMUNICAZIONI AI CLIENTI
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header> -->
					
					<fo:table-body end-indent="0pt" start-indent="0pt">
						<fo:table-row height="3mm">
							<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
								</xsl:if>
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
							
						<fo:table-row keep-with-previous="always">
							<fo:table-cell>
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
								</xsl:if>
								
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									
									<fo:table-header>
										<fo:table-row height="6mm">
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
												<fo:block font-family="universcbold" font-size="11pt" xsl:use-attribute-sets="brd.b.000">
													COMUNICAZIONI AI CLIENTI
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-header>
									
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<fo:table-row>
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center" padding-bottom="1mm">
											
												<fo:block xsl:use-attribute-sets="font.comunicazioni"
													margin-left="2mm" margin-right="2mm">
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
														<fo:block font-family="universcbold">
															Un altro anno di convenienza con Energit per Noi
														</fo:block>
														<fo:block keep-with-previous="always">
															Siamo lieti di comunicarle che Energit per Noi rinnova la sua convenienza!
														</fo:block>
														<fo:block keep-with-previous="always">
															<fo:inline font-family="universcbold">Per tutto il 2011</fo:inline>,
															infatti, il suo contratto per la fornitura di energia elettrica le offre
															un <fo:inline font-family="universcbold">prezzo bloccato della componente energia
															completamente invariato rispetto al 2010.</fo:inline>
														</fo:block>
														<fo:block keep-with-previous="always">
															Buon risparmio con Energit per Noi!
														</fo:block>
													</xsl:if>
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']">
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<xsl:call-template name="COMUNICAZIONI_PROMO_TOP"/>
													</xsl:if>
													
													<!-- <xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='TOP3000']">
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::FRASE='VECCHIO' and
																	  ./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::NOTE='NO'">
															<fo:block font-family="universcbold">
																ENERGIT PREMIA I 3.000 MIGLIORI CLIENTI CON UNO
																<fo:inline font-size="12pt">SCONTO</fo:inline>
																ESCLUSIVO
															</fo:block>
														
															<fo:block keep-with-previous="always">
																Siamo lieti di comunicarle che lei rientra tra i
																<fo:inline font-family="universcbold">3.000 migliori Clienti Energit!</fo:inline>
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Per ringraziarla di aver scelto la nostra azienda le abbiamo riservato lo 
																<fo:inline font-family="universcbold">speciale sconto “Top3000” del valore
																di 9 € per ogni megawattora consumato</fo:inline>, che sarà applicato
																nei mesi di gennaio e dicembre 2011 ai consumi di tutte
																le sue utenze attive ininterrottamente fino a dicembre 2011.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Si assicuri subito questo speciale bonus: chiami il nostro Servizio Clienti al numero
																<fo:inline font-family="universcbold">800.19.22.22</fo:inline>
																da lunedì a venerdì (8.30 - 18.00) e comunichi il
																<fo:inline font-family="universcbold">codice promozione 3000</fo:inline>: i nostri
																operatori saranno a disposizione per registrare la sua adesione.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Se preferisce, può richiedere il suo sconto "Top3000" anche attraverso l'
																<fo:inline font-family="universcbold">Area Clienti</fo:inline>
																del nostro sito Internet
																<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
																la sua UserID <xsl:value-of select="./child::USERNAME" /> e la password.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Per aderire a questa speciale iniziativa ha tempo sino al
																<fo:inline font-family="universcbold">31 gennaio 2011</fo:inline>.
															</fo:block>
															
															<fo:block font-family="universcbold" keep-with-previous="always">
																Grazie ancora per aver scelto Energit!
															</fo:block>
															
															<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
																* In caso di recesso prima del 31/12/2011, i bonus eventualmente già erogati verranno stornati.
															</fo:block>
														</xsl:if>
														
														<xsl:if test="(./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::FRASE='VECCHIO' and
																	   ./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::NOTE='SI') 
																	   or
																	   ./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::FRASE='ATTIVO'">
															<fo:block font-family="universcbold">
																COMPLIMENTI: LEI RICEVERA' LO SCONTO TOP3000, RISERVATO AI MIGLIORI CLIENTI!
															</fo:block>
														
															<fo:block space-before="2mm" keep-with-previous="always">
																Gentile Cliente,
															</fo:block>
															
															<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
																la sua adesione all’iniziativa Top3000, riservata ai 3000 migliori clienti
																Energit, è stata registrata.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Lo <fo:inline font-family="universcbold">speciale sconto “Top3000”,
																del valore di 9 € per ogni megawattora consumato</fo:inline>, sarà
																applicato nei mesi di gennaio e dicembre 2011 ai consumi di tutte le sue
																utenze attive ininterrottamente fino a dicembre 2011.
															</fo:block>
															
															<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
																Grazie per aver scelto Energit!
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always" font-size="9pt">
																* In caso di recesso prima del 31/12/2011, i bonus eventualmente già erogati
																verranno stornati.
															</fo:block>
															
															<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::FRASE='ATTIVO'">
																<fo:block font-size="9pt" keep-with-previous="always">
																	L’erogazione del bonus è subordinata al pieno rispetto delle condizioni
																	generali di contratto da parte del Cliente.
																</fo:block>
															</xsl:if>
														</xsl:if>
														
														
														<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::FRASE='NON ATTIVO' and
																	  ./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::NOTE='NON MOROSO'">
															<fo:block font-family="universcbold">
																RICHIEDA SUBITO LO SPECIALE SCONTO TOP3000 RISERVATO AI NOSTRI MIGLIORI CLIENTI!
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Gentile Cliente,
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																non ha ancora approfittato dello speciale sconto “Top3000” riservato ai 3.000
																migliori Clienti Energit.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Lo richieda subito!
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																"Top3000", lo sconto del valore di 9 € per ogni megawattora consumato, sarà applicato
																nei mesi di gennaio e dicembre 2011 ai consumi di tutte le sue utenze attive
																ininterrottamente* fino a dicembre 2011.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Si assicuri subito questo speciale bonus: chiami il nostro Servizio Clienti al
																numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
																lunedì a venerdì (8.30 - 18.00) e comunichi
																il <fo:inline font-family="universcbold">codice promozione 3000</fo:inline>:
																i nostri operatori saranno a disposizione per registrare la sua adesione.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Se preferisce, può richiedere il suo sconto "Top3000" anche
																attraverso l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del
																nostro sito Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>,
																inserendo la sua UserID <xsl:value-of select="./child::USERNAME" /> e la password.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Le ricordiamo che per aderire a questa speciale iniziativa ha tempo sino
																al <fo:inline font-family="universcbold">31 gennaio 2011.</fo:inline>
															</fo:block>
															
															<fo:block space-before="2mm" font-size="9pt" keep-with-previous="always">
																* In caso di recesso prima del 31/12/2011, i bonus eventualmente già
																erogati verranno stornati.
															</fo:block>
															
															<fo:block font-size="9pt" keep-with-previous="always">
																L’erogazione del bonus è subordinata al pieno rispetto delle condizioni generali
																di contratto da parte del Cliente.
															</fo:block>
														</xsl:if>
														
														
														<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::FRASE='NON ATTIVO' and
																	  ./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::NOTE='MOROSO'">
															<fo:block font-family="universcbold">
																RICHIEDA LO SPECIALE SCONTO TOP3000 RISERVATO AI NOSTRI
																MIGLIORI CLIENTI!
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Gentile Cliente,
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																non ha ancora approfittato dello speciale sconto “Top3000” che
																Energit Le ha riservato.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Non perda questa occasione!
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																"Top3000", lo sconto del valore di 9 € per ogni megawattora consumato,
																sarà applicato nei mesi di gennaio e dicembre 2011 ai consumi di tutte
																le sue utenze attive ininterrottamente* fino a dicembre 2011 e riservato
																ai Clienti in regola con i pagamenti.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Si assicuri subito questo speciale bonus
																: <fo:inline font-family="universcbold">regolarizzi
																i suoi pagamenti entro 5 giorni</fo:inline> e chiami il nostro Servizio Clienti
																al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
																lunedì a venerdì (8.30 - 18.00), comunicando
																il <fo:inline font-family="universcbold">codice promozione 3000</fo:inline>.
																I nostri operatori saranno a disposizione per registrare la Sua adesione.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Se preferisce, potrà richiedere lo sconto "Top3000" anche attraverso
																l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del nostro sito
																Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>,
																inserendo la sua UserID <xsl:value-of select="./child::USERNAME" /> e la password.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Le ricordiamo che per aderire a questa speciale iniziativa ha tempo sino
																al <fo:inline font-family="universcbold">31 gennaio 2011.</fo:inline>
															</fo:block>
															
															<fo:block space-before="2mm" font-size="9pt" keep-with-previous="always">
																* In caso di recesso prima del 31/12/2011, i bonus eventualmente già
																erogati verranno stornati.
															</fo:block>
															
															<fo:block font-size="9pt" keep-with-previous="always">
																L’erogazione del bonus è subordinata al pieno rispetto delle condizioni
																generali di contratto da parte del Cliente.
															</fo:block>
														</xsl:if>
														
														
														<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::FRASE='NON ATTIVO REMINDER2' and
																	  ./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::NOTE='NON MOROSO'">
															<fo:block font-family="universcbold">
																ULTIMI GIORNI PER RICHIEDERE LO SCONTO TOP3000 A LEI RISERVATO!
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Gentile Cliente,
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																non ha ancora approfittato dello speciale sconto “Top3000” riservato ai 3.000
																migliori Clienti Energit: si faccia un regalo e lo richieda subito!
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																"Top3000", lo sconto del valore di 9 € per ogni megawattora consumato,
																sarà applicato nei mesi di gennaio e dicembre 2011 ai consumi di tutte
																le sue utenze attive ininterrottamente* fino a dicembre 2011.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Si assicuri subito questo speciale bonus: chiami il nostro Servizio
																Clienti al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
																lunedì a venerdì (8.30 - 18.00) e comunichi
																il <fo:inline font-family="universcbold">codice promozione 3000</fo:inline>:
																i nostri operatori saranno a disposizione per registrare la sua adesione.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Se preferisce, può richiedere il suo sconto "Top3000" anche
																attraverso l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del
																nostro sito Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>,
																inserendo la sua UserID <xsl:value-of select="./child::USERNAME" /> e la password.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Si affretti! Per aderire a questa speciale iniziativa ha tempo sino
																al <fo:inline font-family="universcbold">31 gennaio 2011.</fo:inline>
															</fo:block>
															
															<fo:block space-before="2mm" font-size="9pt" keep-with-previous="always">
																* In caso di recesso prima del 31/12/2011, i bonus eventualmente già erogati
																verranno stornati. L’erogazione del bonus è subordinata al pieno rispetto delle
																condizioni generali di contratto da parte del Cliente.
															</fo:block>
														</xsl:if>
														
														
														<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::FRASE='NON ATTIVO REMINDER2' and
																	  ./child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000']/child::NOTE='MOROSO'">
															<fo:block font-family="universcbold">
																ULTIMI GIORNI PER RICHIEDERE LO SCONTO TOP3000 A LEI RISERVATO!
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Gentile Cliente,
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																non ha ancora approfittato dello speciale sconto “Top3000” che
																Energit Le ha riservato.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Non perda questa occasione!
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																"Top3000", lo sconto del valore di 9 € per ogni megawattora consumato,
																sarà applicato nei mesi di gennaio e dicembre 2011 ai consumi di tutte
																le sue utenze attive ininterrottamente* fino a dicembre 2011 e riservato
																ai Clienti in regola con i pagamenti.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Si assicuri subito questo speciale bonus
																: <fo:inline font-family="universcbold">regolarizzi
																i suoi pagamenti entro 5 giorni</fo:inline> e chiami il nostro Servizio
																Clienti al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
																lunedì a venerdì (8.30 - 18.00), comunicando
																il <fo:inline font-family="universcbold">codice promozione 3000</fo:inline>.
																I nostri operatori saranno a disposizione per registrare la Sua adesione.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Se preferisce, potrà richiedere lo sconto "Top3000" anche attraverso
																l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del nostro sito
																Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>,
																inserendo la sua UserID <xsl:value-of select="./child::USERNAME" /> e la password.
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																Si affretti! Per aderire a questa speciale iniziativa ha tempo sino
																al <fo:inline font-family="universcbold">31 gennaio 2011.</fo:inline>
															</fo:block>
															
															<fo:block space-before="2mm" font-size="9pt" keep-with-previous="always">
																* In caso di recesso prima del 31/12/2011, i bonus eventualmente già
																erogati verranno stornati. L’erogazione del bonus è subordinata al pieno
																rispetto delle condizioni generali di contratto da parte del Cliente.
															</fo:block>
														</xsl:if>
													</xsl:if> -->
													
													<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI']">
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block>
															Una parte di quanto i clienti pagano per il servizio elettrico
															serve a promuovere la produzione di energia da fonti rinnovabili
															e assimilate. Per informazioni e approfondimenti su questa
															componente di spesa, visiti il sito www.autorita.energia.it
															o chiami il numero verde 800.166.654.
														</fo:block>
													</xsl:if>
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='PROMO']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<xsl:for-each select="PROMO_SERVIZI[@TIPOLOGIA='PROMO']">
															<fo:block>
																<xsl:value-of select="FRASE" />
															</fo:block>
															<xsl:if test="position()&lt;last()">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
														</xsl:for-each>
														
													</xsl:if>
													
													<xsl:variable name="allegato_exnoi">
														<xsl:if test="./child::RINNOVI/child::RINNOVO/child::ALLEGATO='AL-FAEN0110FX.pdf'">SI</xsl:if>
													</xsl:variable>
													
													<xsl:variable name="altri_allegati">
														<xsl:for-each select="./child::RINNOVI">
															<xsl:for-each select="./child::RINNOVO">
																<xsl:if test="not(./child::ALLEGATO='AL-FAEN0110FX.pdf')">SI</xsl:if>
															</xsl:for-each>
														</xsl:for-each>
													</xsl:variable>		
													
													<xsl:if test="$allegato_exnoi='SI'">
														<xsl:if test="./child::RINNOVI">
															<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or 
																		  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																		  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																		  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
														
															<xsl:for-each select="RINNOVI">
																<fo:block font-family="universcbold" keep-with-previous="always">
																	Ancora più convenienza con Energit per noi!
																</fo:block>
																<fo:block keep-with-previous="always">
																	Gentile collega,
																</fo:block>
																<fo:block keep-with-previous="always">
																	<fo:inline font-family="universcbold">Energit per noi rinnova la sua convenienza</fo:inline>
																	offrendoLe un
																	<fo:inline font-family="universcbold">prezzo della componente energia imbattibile e bloccato</fo:inline>				
																	fino al 31/12/2010!
																</fo:block>
																<fo:block keep-with-previous="always">
																	In riferimento al Suo contratto
																	<fo:inline font-family="universcbold">Energit per noi</fo:inline>
																	in vigore presso il punto di prelievo riportato in Tabella 1, 
																	con la presente Le comunichiamo la variazione delle specifiche 
																	tecniche del servizio in conformità a quanto previsto dall’art. 
																	7 delle condizioni generali di contratto.
																</fo:block>
																<fo:block keep-with-previous="always">
																	In particolare, il prodotto è stato aggiornato secondo le 
																	caratteristiche specificamente dettagliate nell’allegato A 
																	alla presente comunicazione.
																</fo:block>
																
																<fo:block font-family="universcbold" space-before="2mm" keep-with-previous="always">
																	Tabella 1
																</fo:block>
																
																<fo:table table-layout="fixed" width="100%"
																		  font-size="8pt">
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(25)" />

																	<fo:table-header>
																		<fo:table-row font-family="universcbold">
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Codice contratto</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>POD/Presa</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Prodotto aggiornato</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Data aggiornamento prodotto</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-header>

																	<fo:table-body>
																		<xsl:for-each select="RINNOVO">
																			<xsl:if test="./child::ALLEGATO='AL-FAEN0110FX.pdf'">
																				<fo:table-row>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="CONTRACT_NO" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRESA" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRODOTTO_AGGIORNATO" />
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="DATA_MODIFICA" /></fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																			</xsl:if>
																		</xsl:for-each>
																	</fo:table-body>
																</fo:table>
																
																<fo:block font-family="universcbold" space-before="2mm" keep-with-previous="always">
																	Grazie per aver scelto Energit per noi!
																</fo:block>
															</xsl:for-each>
														</xsl:if>
													</xsl:if>									
													
													<xsl:if test="substring($altri_allegati,1,2)='SI'">
														<xsl:if test="RINNOVI">
															
															<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or 
																		  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																		  $allegato_exnoi='SI' or
																		  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																		  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
														
															<xsl:for-each select="RINNOVI">
																<fo:block>
																	Gentile Cliente,
																</fo:block>
																<fo:block keep-with-previous="always">
																	con riferimento al
																	contratto di somministrazione di energia elettrica tra noi in
																	vigore presso il/i punto di prelievo/i riportato/i in Tabella 
																	<xsl:if test="$allegato_exnoi='SI'">2</xsl:if>
																	<xsl:if test="not($allegato_exnoi='SI')">1</xsl:if>
																	la presente comunicazione è una variazione delle specifiche tecniche
																	del Servizio in conformità a quanto previsto dall'articolo 7 delle
																	Condizioni Generali di Contratto.
																</fo:block>
																<fo:block keep-with-previous="always">
																	In particolare, Le comunichiamo che
																	il suo prodotto sarà aggiornato secondo le caratteristiche
																	specificamente dettagliate nell'allegato "A" alla presente
																	comunicazione.
																</fo:block>
																
																<fo:block font-family="universcbold" space-before="2mm" keep-with-previous="always">
																	Tabella
																	<xsl:if test="$allegato_exnoi='SI'">2</xsl:if>
																	<xsl:if test="not($allegato_exnoi='SI')">1</xsl:if>
																	– Punto/i di prelievo
																</fo:block>
												
																<fo:table table-layout="fixed" width="100%"
																		  font-size="8pt">
																	<fo:table-column column-width="proportional-column-width(15)" />
																	<fo:table-column column-width="proportional-column-width(15)" />
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(20)" />
												
																	<fo:table-header>
																		<fo:table-row font-family="universcbold">
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Codice contratto</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>POD/Presa</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Prodotto di origine</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Prodotto aggiornato</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Data aggiornamento</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-header>
												
																	<fo:table-body>
																		<xsl:for-each select="RINNOVO">
																			<xsl:if test="not(./child::ALLEGATO='AL-FAEN0110FX.pdf')">
																				<fo:table-row>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="CONTRACT_NO" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRESA" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRODOTTO_ORIGINE" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRODOTTO_AGGIORNATO" />
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="DATA_MODIFICA" /></fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																			</xsl:if>
																		</xsl:for-each>
																	</fo:table-body>
																</fo:table>
												
																<fo:block space-before="2mm" keep-with-previous="always">
																	Le ricordiamo che, in base all'articolo 7.2 delle
																	Condizioni Generali di Contratto, nel caso in cui
																	non intenda accettare le nuove condizioni contrattuali,
																	potrà recedere dal Contratto con comunicazione scritta
																	(anche via fax) che dovrà pervenirci entro 60 (sessanta)
																	giorni prima della data di scadenza del periodo
																	contrattuale annuo in corso.
																</fo:block>
															</xsl:for-each>
														</xsl:if>
													</xsl:if>
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															ENERGIT PREMIA LA FEDELTA' CON PIU' RISPARMI:
														</fo:block>
														
														<fo:block font-family="universcbold" keep-with-previous="always">
															DAL 1/01/2010 PER LEI BEN 8 EURO
															DI SCONTO PER OGNI MWh CONSUMATO!
														</fo:block>
														
														<fo:block keep-with-previous="always">
															A otto anni dalla sua nascita Energit premia
															i Clienti che hanno contribuito a renderla
															protagonista del mercato dell'energia elettrica in Italia.
														</fo:block>
														
														<fo:block font-family="universcbold" keep-with-previous="always">
															Ai suoi clienti più fedeli, come lei, Energit sta offrendo,
															dal 1 gennaio 2010, ben 8 euro di sconto per ogni
															megawattora consumato.
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Questo bonus
															<fo:inline font-family="universcbold">
																ha incrementato mediamente il
																risparmio già garantito in bolletta
															</fo:inline>
															(prezzo della componente energia Energit)
															<fo:inline font-family="universcbold">
																di un ulteriore 10% di
																sconto.
															</fo:inline>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Per eventuali chiarimenti il nostro Servizio Clienti è a sua
															disposizione (email
															<fo:inline font-family="universcbold">energia@energit.it</fo:inline>
															- tel.
															<fo:inline font-family="universcbold">800.19.22.22</fo:inline>).
														</fo:block>
														
														<fo:block font-family="universcbold" keep-with-previous="always">
															Energit, una scelta che paga.
														</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR']">
														
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block>
															Gentile Cliente,
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Oggetto: <fo:inline font-family="universcbold">ERRATA CORRIGE</fo:inline>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															La presente per informarLa che la proposta di contratto
															per la fornitura di energia elettrica da Lei sottoscritta
															con la Nostra Società conteneva un'erronea indicazione
															numerica delle clausole delle Condizioni Generali che Lei,
															ai sensi e per gli effetti degli Artt. 1341 e 1342 Cod.
															Civ. ha dichiarato di avere letto attentamente, accettato
															e sottoscritto espressamente.
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Pertanto riportiamo qui di seguito, per Sua corretta
															informazione, la dichiarazione da Lei sottoscritta con
															l'esatta indicazione numerica delle clausole da Lei
															espressamente accettate.
														</fo:block>
														
														<fo:block text-align="center" keep-with-previous="always">
															****
														</fo:block>
														
														<fo:block  keep-with-previous="always" font-family="universccors">
															Ai sensi e per gli effetti degli Articoli 1341 e 1342
															c.c., il sottoscritto dichiara di avere letto attentamente
															e di accettare espressamente i seguenti punti delle
															Condizioni Generali riportate sul retro:
															<fo:inline font-family="universcbold">Art. 2.2 </fo:inline>
															Esclusiva;
															<fo:inline font-family="universcbold">Art. 6 </fo:inline>
															Conclusione del Contratto;
															<fo:inline font-family="universcbold">Art. 7 </fo:inline>
															Durata, recesso e multa penitenziale;
															<fo:inline font-family="universcbold">Art. 8 </fo:inline>
															Variazione delle specifiche tecniche e delle condizioni del Contratto;
															<fo:inline font-family="universcbold">Art. 9.5 </fo:inline>
															Aggiornamento del corrispettivo;
															<fo:inline font-family="universcbold">Art. 10.6 </fo:inline>
															Limitazione della facoltà di opporre eccezioni e obbligo di pagamento
															delle fatture contestate;
															<fo:inline font-family="universcbold">Art. 11.1 </fo:inline>
															Limitazioni del Servizio;
															<fo:inline font-family="universcbold">Art. 12 </fo:inline>
															Sospensione del Servizio;
															<fo:inline font-family="universcbold">Art. 13 </fo:inline>
															Risoluzione ai sensi del 1456 c.c;
															<fo:inline font-family="universcbold">Art. 14 </fo:inline>
															Limitazione di responsabilità di Energit;
															<fo:inline font-family="universcbold">Art. 15 </fo:inline>
															Uso improprio del Servizio;
															<fo:inline font-family="universcbold">Art. 16 </fo:inline>
															Trattamento dati personali D.Lgs. 196/03;
															<fo:inline font-family="universcbold">Art. 17 </fo:inline>
															Riservatezza, pubblicità;
															<fo:inline font-family="universcbold">Art. 18 </fo:inline>
															Reclami;
															<fo:inline font-family="universcbold">Art. 21 </fo:inline>
															Cessione del Contratto;
															<fo:inline font-family="universcbold">Art. 23 </fo:inline>
															Controversie e foro competente.
														</fo:block>
														
														<fo:block  space-before="2mm" keep-with-previous="always" text-align="center">
															****
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Cordiali Saluti,
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Energ.it S.p.A.
														</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block text-align="justify">
															<fo:block text-align="left" font-family="universcbold">Nuove Condizioni Generali Energit S.p.A. - Scheda di Riepilogo degli effetti della variazione contrattuale proposta</fo:block>
															<fo:block space-before="2mm" keep-with-previous="always" text-align="left">Gentile cliente,</fo:block>
															<fo:block keep-with-previous="always" space-before="2mm">con riferimento alle Condizioni Generali di Contratto per la fornitura di energia elettrica e dei servizi associati (“<fo:inline font-family="universcbold">CGC</fo:inline>”) attualmente in vigore, comunichiamo con la presente  l’aggiornamento degli articoli 4.4, 4.5, 6.10, 7.8, 13.1, 23.1.</fo:block>
															<fo:block>La scheda di riepilogo sotto riportata è redatta ai sensi e per gli effetti dell'articolo 12.3 della delibera AEEG n. 105/06, ai sensi del quale in caso di variazione unilaterale di clausole contrattuali l'esercente la vendita deve, tra l'altro, fornire "l'illustrazione chiara, completa e comprensibile dei contenuti e degli effetti della variazione proposta".</fo:block>
															<fo:block>Dal punto di vista prettamente formale, si segnala che gli articoli 4.4, 4.5, 6.10, 7.8, 13.1, 23.1 delle CGC per la fornitura di energia elettrica e dei servizi associati stipulate con Energit S.p.A., come attualmente in vigore, saranno modificati per effetto dell'adozione delle nuove condizioni generali, applicabili dal [DATA] (le "<fo:inline font-family="universcbold">Nuove Condizioni Generali di Contratto</fo:inline>" o "<fo:inline font-family="universcbold">NCGC</fo:inline>").</fo:block>
															<fo:block>In conformità a quanto disposto dalla normativa applicabile, il presente documento si limita a specificare i contenuti e gli effetti sui diritti ed obblighi delle Parti ai sensi delle CGC che deriveranno dall'adozione delle NCGC.</fo:block>
															<fo:block>La modifica delle CGC <fo:inline text-decoration="underline">non comporta alcuna modifica al corrispettivo attualmente previsto per la fornitura di energia elettrica.</fo:inline></fo:block>
															<fo:block>Salvo ove diversamente specificato, i termini con iniziale maiuscola utilizzati nel presente documento avranno il medesimo significato loro attribuito nelle CGC e/o nelle NCGC.</fo:block>
															<fo:block space-before="2mm">Le clausole cui fa riferimento la tabella sotto riportata sono quelle previste dalle Nuove Condizioni Generali di Contratto.</fo:block>
														</fo:block>
														
														<fo:table widows="10" orphans="10" font-size="8pt" space-before="2mm" end-indent="0pt" table-layout="fixed" width="100%">
														<fo:table-column column-width="proportional-column-width(70)"/>
														<fo:table-column column-width="proportional-column-width(30)"/>
															
															<fo:table-body end-indent="0pt" start-indent="0pt" text-align="justify">
																
																<fo:table-row font-family="universcbold" text-align="center">
																	<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>SCHEDA RIEPILOGATIVA DELLE MODIFICHE PROPOSTE</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row keep-with-previous="always" font-family="universcbold">
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>Articolo</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>ILLUSTRAZIONE DEL CONTENUTO E DEGLI EFFETTI DELLA VARIAZIONE CONTRATTUALE PROPOSTA</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row keep-with-previous="always">
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors" padding="3.5pt">
																		<fo:block><fo:inline font-family="universcbold">4.4</fo:inline> Energit trasmette le richieste del Cliente al Distributore Locale, che comunque rimarrà responsabile per l’esecuzione (o la mancata esecuzione) delle prestazioni richieste. Energit cesserà di dare corso alle richieste del Cliente alla data di cessazione del Contratto per qualsivoglia causa.</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>La modifica riguarda le modalità di gestione delle pratiche di relative alla “connessione”. Energit non darà più corso alle suddette pratiche rimaste inevase alla data di cessazione del contratto, dal momento che a tale data il punto di prelievo oggetto del contratto non sarà più nel dispacciamento di Energit.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors" padding="3.5pt">
																		<fo:block><fo:inline font-family="universcbold">4.5</fo:inline> In ogni caso, il Cliente è tenuto al pagamento dei costi richiesti dal Distributore Locale per lo svolgimento delle prestazioni richieste - direttamente o per il tramite di Energit. Qualora Energit provveda al pagamento di tali costi, il Cliente sarà tenuto a rimborsarli ad Energit. In aggiunta a tali costi, Energit addebiterà al Cliente in fattura un corrispettivo fisso a titolo di contributo per le spese di gestione di ciascuna richiesta di prestazione di cui all’articolo 4.1.</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>In aggiunta ai costi previsti dal distributore locale per l’avvio delle pratiche di “connessione”, Energit addebiterà un corrispettivo a titolo di contributo spese. L’importo di tale corrispettivo verrà indicato nel modulo di apertura pratica.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors">
																		<fo:table space-before="5mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
																		<fo:table-column column-width="proportional-column-width(100)"/>
																			<fo:table-body end-indent="0pt" start-indent="0pt">
																				<fo:table-row>
																					<fo:table-cell xsl:use-attribute-sets="brd.b.000" padding="3.5pt">
																						<fo:block><fo:inline font-family="universcbold">6.10</fo:inline> A seguito dello svolgimento delle attività di cui al precedente articolo 6.9, Energit comunica al Cliente, con almeno 5 (cinque) giorni di preavviso, la data di avvio del Servizio mediante apposita comunicazione in forma scritta.</fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																				<fo:table-row keep-with-previous="always">
																					<fo:table-cell padding="3.5pt">
																						<fo:block><fo:inline font-family="universcbold">13.1</fo:inline> In aggiunta ai rimedi di cui al precedente articolo 12, Energit avrà diritto di risolvere il Contratto ai sensi dell’articolo 1456 del Codice Civile, previa comunicazione al Cliente in forma scritta e con conseguente interruzione del Servizio, in caso di inadempimento del Cliente a ciascuna delle obbligazioni di cui agli articoli: 6.6 (dichiarazioni e garanzie), 10.4 (mancato pagamento di una fattura nel termine indicato in fattura), 11 (mezzi di garanzia), 12.1 (omesso pagamento nel termine di cui all'articolo 12.1, lett. (i)) 15 (uso improprio del Servizio), 17 (riservatezza) e 21 (cessione del Contratto) del Contratto.</fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																			</fo:table-body>
																		</fo:table>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>In entrambi gli articoli in oggetto è stata eliminata l’indicazione delle modalità di invio delle comunicazioni oggetto degli articoli, fermo restando l’obbligo di effettuare tali comunicazioni in forma scritta.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors" padding="3.5pt">
																		<fo:block><fo:inline font-family="universcbold">7.8</fo:inline> Nel caso in cui il Cliente non rispetti i termini e le condizioni previsti dal presente articolo 7 per l'esercizio del diritto di recesso e, ciononostante, tale recesso risulti efficace e venga avviata una fornitura da parte di un terzo fornitore presso i Siti del Cliente, il Cliente sarà tenuto a corrispondere ad Energit un corrispettivo per l'esercizio illegittimo del diritto di recesso pari ad Euro 0,06 per ogni KWh fatturato al Cliente nei tre mesi precedenti il mese in cui è cessato il Servizio, maggiorato di un importo pari ad euro 100, che sarà oggetto di fatturazione da parte di Energit e di pagamento da parte del Cliente in conformità a quanto previsto dal successivo articolo 10.</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>Il corrispettivo per l’esercizio illegittimo del diritto di recesso da parte del Cliente viene modificato con riferimento alla componente fissa del medesimo.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors" padding="3.5pt">
																		<fo:block><fo:inline font-family="universcbold">23.1</fo:inline> Per qualunque controversia nascente dal Contratto (a titolo esemplificativo e non esaustivo, dalla sua validità, efficacia, interpretazione, esecuzione, risoluzione,rescissione) sarà competente in via esclusiva il foro di Cagliari.</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>Il foro per eventuali controversie viene trasferito da Milano a Cagliari.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																
															</fo:table-body>
														</fo:table>
														
														<fo:block space-before="2mm" text-align="justify" keep-with-previous="always">Le ricordiamo inoltre che, qualora non intenda accettare le modifiche sopra riportate avrà in ogni caso il diritto di recedere dalle CGC entro 15 (quindici) giorni dalla data di comunicazione delle modifiche, senza penali e con effetto immediato, fatti i salvi i tempi tecnici previsti per le comunicazioni con il distributore locale, mediante comunicazione scritta da inviarsi a mezzo raccomandata A/R, Fax o PEC agli indirizzi indicati all’articolo 19.1 delle CGC.</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															COMUNICAZIONE DEI DATI CATASTALI
														</fo:block>
													
														<fo:block keep-with-previous="always">
															Gentile Cliente,
														</fo:block>
														
														<fo:block keep-with-previous="always">
															<fo:inline font-family="universcbold">
															non risulta ancora pervenuta la comunicazione dei dati catastali
															identificativi dell'immobile/terreno presso cui è attiva la sua
															fornitura di energia</fo:inline>. Le ricordiamo che la
															responsabilità dell'invio di questi dati, che saranno poi trasmessi
															all'Anagrafe tributaria, è a Suo carico.
															<fo:inline font-family="universcbold">
																La invitiamo a compilare subito l'apposito modulo
																che trova nella sua Area Clienti
															</fo:inline>
															e inviarlo ad Energit! Grazie.
														</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															PARTECIPI AL PROGETTO PAPERLESS!
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Scelga la comodità di
															<fo:inline font-family="universcbold">
															ricevere la fattura nella sua casella di posta elettronica:
															</fo:inline>
															con la fattura via email contribuirà alla difesa dell’ambiente
															e potrà creare un comodo e ordinato archivio elettronico sul suo PC.
														</fo:block>
														
														<fo:block keep-with-previous="always">
															La fattura via email è:
														</fo:block>
														
														<fo:list-block keep-with-previous="always" provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
															<fo:list-item>
																<fo:list-item-label end-indent="label-end()">
																	<fo:block>-</fo:block>
																</fo:list-item-label>
																<fo:list-item-body start-indent="body-start()">
																	<fo:block>
																		<fo:inline font-family="universcbold">RISPETTOSA DELL’AMBIENTE</fo:inline>:
																		permette di risparmiare carta e quindi di salvare numerosi alberi
																	</fo:block>
																</fo:list-item-body>
															</fo:list-item>
															
															<fo:list-item keep-with-previous="always">
																<fo:list-item-label end-indent="label-end()">
																	<fo:block>-</fo:block>
																</fo:list-item-label>
																<fo:list-item-body start-indent="body-start()">
																	<fo:block>
																		<fo:inline font-family="universcbold">GRATUITA</fo:inline>:
																		non ha alcun costo per lei e le permetterà di risparmiare le spese di spedizione del documento cartaceo, pari ad 1,50 € per fattura
																	</fo:block>
																</fo:list-item-body>
															</fo:list-item>
															
															<fo:list-item keep-with-previous="always">
																<fo:list-item-label end-indent="label-end()">
																	<fo:block>-</fo:block>
																</fo:list-item-label>
																<fo:list-item-body start-indent="body-start()">
																	<fo:block>
																		<fo:inline font-family="universcbold">INNOVATIVA</fo:inline>:
																		potrà conservarla ed archiviarla comodamente nel suo PC
																	</fo:block>
																</fo:list-item-body>
															</fo:list-item>
															
															<fo:list-item keep-with-previous="always">
																<fo:list-item-label end-indent="label-end()">
																	<fo:block>-</fo:block>
																</fo:list-item-label>
																<fo:list-item-body start-indent="body-start()">
																	<fo:block>
																		<fo:inline font-family="universcbold">RAPIDA</fo:inline>:
																		la riceverà immediatamente nella sua casella email
																	</fo:block>
																</fo:list-item-body>
															</fo:list-item>
															
															<fo:list-item keep-with-previous="always">
																<fo:list-item-label end-indent="label-end()">
																	<fo:block>-</fo:block>
																</fo:list-item-label>
																<fo:list-item-body start-indent="body-start()">
																	<fo:block>
																		<fo:inline font-family="universcbold">AFFIDABILE</fo:inline>:
																		sarà disponibile in qualsiasi momento anche nella sua Area Riservata
																	</fo:block>
																</fo:list-item-body>
															</fo:list-item>
														</fo:list-block>
														
														<fo:block keep-with-previous="always">
															Passi subito alla fattura via email!
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Invii la richiesta a
															<fo:inline font-family="universcbold">paperless@energit.it</fo:inline>,
															specificando il suo codice cliente <xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_CLIENTE" />.
														</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
														   <fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															GRAZIE PER AVER SCELTO L'ENERGIA VERDE CERTIFICATA RECS!
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Gentile Cliente,
														</fo:block>
														
														<fo:block keep-with-previous="always">
															nel complimentarci per la sua sensibilità nei confronti
															dell'ambiente abbiamo il piacere di confermarle che
															Energit ha compensato i suoi consumi di energia elettrica
															del 2009 con una corrispondente quantità di certificati
															RECS, che attestano la produzione di energia elettrica
															da fonti rinnovabili come l'acqua, il vento e il sole.
														</fo:block>
													</xsl:if>
													
													
													<xsl:for-each select="PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO']">
														<xsl:if test="./ancestor::DOCUMENTO/child::CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  ./ancestor::DOCUMENTO/child::RINNOVI or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or 
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] or
																	  position()&gt;1">
														   <fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block widows="10" orphans="10">
														<xsl:if test="./child::FRASE='KO'">
															<fo:inline font-family="universcbold">BONUS ELETTRICO</fo:inline>:
															Con riferimento al contratto
															<xsl:value-of select="./child::NOTE"/>,
															la informiamo che la richiesta di ammissione
															della sua fornitura alla compensazione della
															spesa per la fornitura di energia elettrica è
															stata rigettata. Per maggiori informazioni la
															invitiamo a contattare il suo comune di residenza.
														</xsl:if>
														<xsl:if test="not(./child::FRASE='KO')">
															<fo:inline font-family="universcbold">BONUS ELETTRICO</fo:inline>:
															Con riferimento al contratto
															<xsl:value-of select="./child::NOTE"/>,
															la informiamo che la sua fornitura è ammessa alla
															compensazione della spesa per la fornitura di energia
															elettrica ai sensi del decreto ministeriale 28 dicembre
															2007. La richiesta di rinnovo deve essere effettuata
															entro <xsl:value-of select="./child::FRASE"/>.
														</xsl:if>
														</fo:block>
													</xsl:for-each>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
														   <fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															MIX MEDIO ENERGETICO NAZIONALE
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Ai sensi di quanto previsto dall’art. 2 del decreto legge 31 luglio 2009,
															riportiamo di seguito le informazioni sulla composizione del mix medio
															nazionale di fonti energetiche primarie utilizzate per la produzione.
															Per maggiori informazioni può consultare il nostro sito internet www.energit.it .
														</fo:block>
														
														<fo:block space-before="2mm" keep-with-previous="always">
															<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																<fo:table-column column-width="proportional-column-width(33)"/>
																<fo:table-column column-width="proportional-column-width(34)"/>
																<fo:table-column column-width="proportional-column-width(33)"/>
																<fo:table-body end-indent="0pt" start-indent="0pt">
																	<fo:table-row font-family="universcbold">
																		<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block text-align="center">
																				Composizione del Mix Medio Nazionale utilizzato per la produzione
																			</fo:block>
																			<fo:block text-align="center" keep-with-previous="always">
																				dell'energia elettrica immessa nel sistema elettrico nel 2008 e nel 2009
																			</fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																	
																	<fo:table-row font-family="universcbold" keep-with-previous="always">
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				Fonti primarie utilizzate
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				Anno 2009 %
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				Anno 2008 %
																			</fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																	
																	<fo:table-row keep-with-previous="always">
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				Fonti rinnovabili
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				31,6%
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				26,8%
																			</fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																	
																	<fo:table-row keep-with-previous="always">
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				Carbone
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				13,1%
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				13,3%
																			</fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																	
																	<fo:table-row keep-with-previous="always">
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				Gas Naturale
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				43,5%
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				47,8%
																			</fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																	
																	<fo:table-row keep-with-previous="always">
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				Prodotti petroliferi
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				4,3%
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				3,9%
																			</fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																	
																	<fo:table-row keep-with-previous="always">
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				Nucleare
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				1,5%
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				1,3%
																			</fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																	
																	<fo:table-row keep-with-previous="always">
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				Altre fonti
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				6,1%
																			</fo:block>
																		</fo:table-cell>
																		
																		<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																			<fo:block>
																				6,8%
																			</fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
														</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="$comunicazione_area_clienti='SI'">
													
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='TOP3000'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															UN MONDO DI NUOVI SERVIZI DA SCOPRIRE
															NELL’AREA CLIENTI ENERGIT!
														</fo:block>
														
														<fo:block font-family="universcbold" keep-with-previous="always">
															Scopra tutte le novità dell’Area Clienti su www.energit.it!
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Questo prezioso strumento sapra’ stupirla con un mondo
															di informazioni, servizi e tanto altro ancora.
															Nella sua Area Clienti potrà, per esempio:
														</fo:block>
														
														<fo:block start-indent="5mm" font-family="universcbold" keep-with-previous="always">
															<fo:block>
																<fo:inline> - Avere accesso a promozioni esclusive</fo:inline>
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Visualizzare i consumi di energia, anche nel dettaglio</fo:inline>
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Trovare utili suggerimenti per risparmiare</fo:inline>
															</fo:block>
															
															<xsl:if test="AUTOLETTURA">
																<fo:block keep-with-previous="always">
																	<fo:inline> - Comunicare la lettura del contatore</fo:inline>
																</fo:block>
															</xsl:if>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Consultare le fatture e verificare lo stato dei pagamenti</fo:inline>
															</fo:block>
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS']">
																<fo:block keep-with-previous="always">
																	<fo:inline> - Passare alla fattura via email per contribuire alla difesa dell’ambiente</fo:inline>
																</fo:block>
															</xsl:if>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Scaricare utile modulistica</fo:inline>
															</fo:block>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Acceda subito all’Area Clienti su www.energit.it attraverso la sua
															<fo:inline font-family="universcbold">UserID </fo:inline>
															<xsl:value-of select="USERNAME" /> : scoprirà un modo nuovo
															e ancora più efficiente per comunicare con Energit.
														</fo:block>
														
													</xsl:if>
													
													<xsl:if test="$bordi='NO'">
														<fo:block keep-with-previous="always" space-before="3mm" xsl:use-attribute-sets="brd.b.000" padding-left="2mm" padding-right="2mm"/>
													</xsl:if>
												
												</fo:block>
												
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row height="5mm" keep-with-previous="always">
							<fo:table-cell display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
								</xsl:if>
								<fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			
			</xsl:if>
                
                
			
			
			
		</fo:block>
	
	</xsl:template>
	
	
	<xsl:attribute-set name="font.table">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="line-height">9pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.pagare">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	    <xsl:attribute name="line-height">10pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.header">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	    <xsl:attribute name="font-family">universcbold</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	    <xsl:attribute name="padding-left">3mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border">
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.left">
		<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.right">
		<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.top">
		<xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.bottom">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.left">
		<xsl:attribute name="border-left">1pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.right">
		<xsl:attribute name="border-right">1pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.top">
		<xsl:attribute name="border-top">1pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.bottom">
		<xsl:attribute name="border-bottom">1pt solid black</xsl:attribute>
	</xsl:attribute-set>
	 
	<xsl:attribute-set name="font.distrib">
	    <xsl:attribute name="font-family">Arial</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia_header">
	    <xsl:attribute name="font-family">Arial</xsl:attribute>
	    <xsl:attribute name="font-size">6pt</xsl:attribute>
	    <xsl:attribute name="line-height">9pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia">
	    <xsl:attribute name="font-family">Arial</xsl:attribute>
	    <xsl:attribute name="font-size">20pt</xsl:attribute>
	    <xsl:attribute name="line-height">9pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.legenda">
	    <xsl:attribute name="font-family">Arial</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.comunicazioni">
		<xsl:attribute name="font-family">universc</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.title">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="line-height">4mm</xsl:attribute>
		<xsl:attribute name="space-after">4mm</xsl:attribute>
		<xsl:attribute name="font-size">11pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.separatore">
		<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="space-after">1mm</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.rinnovi">
		<xsl:attribute name="padding">0.5mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.mix_fonti">
		<xsl:attribute name="padding">0.5mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.articoli">
		<xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
		<xsl:attribute name="font-style">italic</xsl:attribute>
	</xsl:attribute-set>

</xsl:stylesheet>

