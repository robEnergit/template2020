<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:barcode="http://barcode4j.krysalis.org/ns" xmlns:math="xalan://java.lang.Math" extension-element-prefixes="math">

<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**            Inclusione templates esterni            **
********************************************************
-->
<xsl:include href="attributi.xsl"/>
<xsl:include href="Template_Energia_Multisito.xsl"/>
<xsl:include href="202_09/Template_Energia_Multisito_012011.xsl"/>
<xsl:include href="Finale.xsl"/>
<xsl:include href="202_09/Finale_012011.xsl"/>
<xsl:include href="bollettino.xsl"/>
<xsl:include href="condizioni.xsl"/>
<xsl:include href="condizioni_c11.xsl"/>
<xsl:include href="scheda_riepilogo.xsl"/>
<xsl:include href="scheda_riepilogo_c11.xsl"/>
<xsl:include href="comunicazioni_condizioni.xsl"/>
<xsl:include href="comunicazioni_condizioni_c11.xsl"/>
<xsl:include href="Autocertificazione.xsl"/>
<xsl:include href="Informativa_Qualita.xsl"/>

<!--
**********************************************************
**				Template LOTTO_FATTURE					**
**********************************************************
-->
<xsl:template match="LOTTO_FATTURE">

<fo:root>

<fo:layout-master-set>
    <!--
    ********************************************************
    **           Definizione pagina fattura               **
    ********************************************************
    -->
    <fo:simple-page-master margin-bottom="10mm" margin-left="11mm" margin-right="11mm" margin-top="5mm" master-name="pm0" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="17mm" margin-top="12mm" overflow="visible" region-name="body"/>
        <fo:region-before extent="12mm" overflow="visible" region-name="header"/>
        <fo:region-after display-align="after" extent="17mm" overflow="visible" region-name="footer"/>
    </fo:simple-page-master>

	<fo:simple-page-master margin-bottom="10mm" margin-left="11mm" margin-right="11mm" margin-top="5mm" master-name="pm0first" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="17mm" margin-top="12mm" overflow="visible" region-name="body"/>
        <fo:region-before extent="12mm" overflow="visible" region-name="headernew"/>
        <fo:region-after display-align="after" extent="17mm" overflow="visible" region-name="footer"/>
    </fo:simple-page-master>

    <fo:simple-page-master margin-bottom="0mm" margin-left="0mm" margin-right="0mm" margin-top="0mm" master-name="pm0-blank" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="0mm" margin-top="0mm" overflow="visible" region-name="body"/>
    </fo:simple-page-master>

    <!--
    ********************************************************
    **          Definizione pagina bollettino             **
    ********************************************************
    -->
    <fo:simple-page-master master-name="pm1"
		page-height="210mm" page-width="297mm" margin="0mm">
		<fo:region-body margin-top="108mm" />
		<fo:region-before extent="102mm" overflow="hidden" />
	</fo:simple-page-master>

    <!--
    *****************************************************************************
    **    Definizione pagine allegati condizioni contrattuali - comparativa    **
    *****************************************************************************
    -->
	<fo:simple-page-master margin-bottom="5mm" margin-left="10mm" margin-right="10mm" margin-top="10mm" master-name="pm0_condizioni" page-height="297mm" page-width="210mm">
		<fo:region-body margin-bottom="0mm" margin-top="5mm" overflow="visible" region-name="body" />
		<fo:region-before extent="10mm" overflow="visible" region-name="header_condizioni" />
		<fo:region-after display-align="after" extent="10mm" overflow="visible" region-name="footer_condizioni" />
	</fo:simple-page-master>

	<fo:simple-page-master margin-bottom="10mm" margin-left="10mm" margin-right="10mm" margin-top="10mm" master-name="pm0_scheda_riepilogo" page-height="297mm" page-width="210mm">
		<fo:region-body margin-bottom="0mm" margin-top="0mm" overflow="visible" region-name="body" />
		<fo:region-after display-align="after" extent="10mm" overflow="visible" region-name="footer_riepilogo" />
	</fo:simple-page-master>

	<fo:simple-page-master margin-top="5mm" margin-bottom="10mm" master-name="pm0_comunicazione_condizioni" page-height="297mm" page-width="210mm">
		<fo:region-body margin-top="3cm" margin-bottom="25mm" margin-left="29mm" margin-right="29mm" overflow="visible" region-name="body" />
		<fo:region-before extent="3cm" overflow="visible" region-name="header" />
		<fo:region-after display-align="after" extent="25mm" overflow="visible" region-name="footer" />
	</fo:simple-page-master>

	<fo:page-sequence-master master-name="document">
      <fo:repeatable-page-master-alternatives>
        <fo:conditional-page-master-reference
          master-reference="pm0" page-position="rest" blank-or-not-blank="not-blank"/>
        <fo:conditional-page-master-reference
          master-reference="pm0first" page-position="first"  blank-or-not-blank="not-blank"/>
		<fo:conditional-page-master-reference
          master-reference="pm0-blank" page-position="any" blank-or-not-blank="blank"/>
      </fo:repeatable-page-master-alternatives>
    </fo:page-sequence-master>

</fo:layout-master-set>

<xsl:apply-templates select="DOCUMENTO"/>

</fo:root>
</xsl:template>





<!--
**********************************************************
**					Template DOCUMENTO					**
**********************************************************
-->
<xsl:template match="DOCUMENTO">

<xsl:variable name="bordi">NO</xsl:variable><!--rectangle_table-->
<xsl:variable name="rectangle_frontespizio">svg/rectangle_frontespizio_20</xsl:variable>
<xsl:variable name="color">#f9b200</xsl:variable><!--#f08c02-->
<xsl:variable name="color_riquadro_scadenza">black</xsl:variable><!--black-->
<xsl:variable name="color_titolo_dettaglio">black</xsl:variable><!--black-->
<xsl:variable name="sfondo_titoli">#fdd17e</xsl:variable><!--#DDDDDD-->
<xsl:variable name="color-sezioni">black</xsl:variable><!--#004a76-->
<xsl:variable name="color-sottosezioni">black</xsl:variable><!--#004a76-->
<xsl:variable name="color-default">#000000</xsl:variable>
<xsl:variable name="svg-sezioni">'url(svg/rectangle_titolo_sezioni_short_color.svg)'</xsl:variable>
<xsl:variable name="svg-sottosezioni">'url(svg/rectangle_titolo_sottosezioni_short_color.svg)'</xsl:variable>
<xsl:variable name="svg-altre-sezioni">'url(svg/rectangle_titolo_sezioni_short_color.svg)'</xsl:variable>
<xsl:variable name="svg-dettaglio">'url(svg/rectangle_dettaglio_color.svg)'</xsl:variable>
<xsl:variable name="autocertificazione">NO</xsl:variable>
<xsl:variable name="qualita">SI</xsl:variable>

<xsl:variable name="status_piu_uno"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:variable>
<xsl:variable name="data_doc_number"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2),substring(DATA_DOCUMENTO,1,2))"/></xsl:variable>


<xsl:variable name="consumi_fatturati">
	<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
		<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::CONSUMI_FATTURATI"/>
	</xsl:if>
	<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
		<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::CONSUMI_FATTURATI"/>
	</xsl:if>
	<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
		0
	</xsl:if>
</xsl:variable>

<xsl:variable name="imponibile_no_altrepartite">
	<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00'">0</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00')">
			<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE,'.',''),',','')"/>
		</xsl:if>
	</xsl:if>
	<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
		<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00')">
			<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE,'.',''),',','')"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
			0
		</xsl:if>
	</xsl:if>
</xsl:variable>

<xsl:variable name="imponibile_sv">
	<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00'">0</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00')">
			<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA,'.',''),',','')"/>
		</xsl:if>
	</xsl:if>
	<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
		<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00')">
			<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA,'.',''),',','')"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
			0
		</xsl:if>
	</xsl:if>
</xsl:variable>







	<xsl:variable name="sv">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="sr">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="imp">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IMPOSTE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IMPOSTE='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IMPOSTE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IMPOSTE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_IMPOSTE,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>

	</xsl:variable>

	<xsl:variable name="iva_fornitura">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>

	</xsl:variable>

	<xsl:variable name="iva_odiv">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA_SU_ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA_SU_ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA_SU_ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>

	</xsl:variable>

	<xsl:variable name="iva_dc">
		<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE">
			<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IVA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IVA='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IVA,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE)">
			0
		</xsl:if>

	</xsl:variable>

	<xsl:variable name="iva">
		<xsl:value-of select="$iva_fornitura+$iva_odiv+$iva_dc"/>
	</xsl:variable>

	<xsl:variable name="odiv_fornitura">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>

	</xsl:variable>



	<xsl:variable name="imponibile_dc">
		<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE">
			<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE)">
			0
		</xsl:if>

	</xsl:variable>

	<xsl:variable name="odiv">
		<xsl:value-of select="$odiv_fornitura+$imponibile_dc"/>
	</xsl:variable>

	<xsl:variable name="bs">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>

	</xsl:variable>


	<xsl:variable name="t_rai">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_CANONE_RAI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_CANONE_RAI='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_CANONE_RAI,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_CANONE_RAI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_CANONE_RAI='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_CANONE_RAI,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="importo_no_virgola">
		<xsl:value-of select="translate(translate(./child::IMPORTO_DOCUMENTO,'.',''),',','')"/>
	</xsl:variable>

	<xsl:variable name="doc">
		<xsl:value-of select="math:abs($sv) + math:abs($sr) + math:abs($imp) + math:abs($iva) + math:abs($odiv) + math:abs($bs) + math:abs($t_rai)"/>

	</xsl:variable>

	<xsl:variable name="costo_medio">
		<xsl:if test="$imponibile_no_altrepartite &gt; 0 and $consumi_fatturati &gt; 0">
			<xsl:value-of select="round($imponibile_no_altrepartite div $consumi_fatturati) div 100"/>
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="costo_medio_sv">
		<xsl:if test="$imponibile_sv &gt; 0 and $consumi_fatturati &gt; 0">
			<xsl:value-of select="round($imponibile_sv div $consumi_fatturati) div 100"/>
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="disagio_economico">
		<xsl:if test="./child::CONTRATTO_ENERGIA/child::SEZIONE[@TIPOLOGIA='CT_FISSE']/child::PERIODO_RIFERIMENTO/child::PRODOTTO/child::DESCRIZIONE='Bonus sociale economico'">1</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SEZIONE[@TIPOLOGIA='CT_FISSE']/child::PERIODO_RIFERIMENTO/child::PRODOTTO/child::DESCRIZIONE='Bonus sociale economico')">0</xsl:if>
	</xsl:variable>


  <xsl:variable name="dettaglio_fattura">
    <xsl:choose>
      <xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">
        <xsl:value-of select="1" />
      </xsl:when>
      <xsl:when test="./child::DETTAGLIO='1'">
        <xsl:value-of select="./child::DETTAGLIO" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>


<fo:page-sequence initial-page-number="1" master-reference="document" orphans="1" white-space-collapse="true" widows="1" id="F">
    <!--
    ********************************************************
    **                     Header                         **
    ********************************************************
    -->
	<fo:static-content flow-name="headernew">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
            <fo:table-column column-width="proportional-column-width(100)"/>
            <fo:table-body end-indent="0pt" start-indent="0pt">
                <fo:table-row>
                    <fo:table-cell display-align="center" xsl:use-attribute-sets="blk.header">
                        <fo:block text-align="end">
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							<xsl:if test="$status_piu_uno &lt; 201307">
								<xsl:if test="@SPOT='NO' or not(@SPOT)">
									<fo:inline font-family="universbold" font-size="11pt">Codice Cliente <xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
								</xsl:if>
							</xsl:if>
                        </fo:block>
                        <fo:block text-align="end">
							<xsl:if test="$status_piu_uno &lt; 201307">
								<xsl:if test="@SPOT='NO' or not(@SPOT)">
									<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Energit</fo:inline>
								</xsl:if>
							</xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </fo:static-content>

    <fo:static-content flow-name="header">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
            <fo:table-column column-width="proportional-column-width(100)"/>
            <fo:table-body end-indent="0pt" start-indent="0pt">
                <fo:table-row>
                    <fo:table-cell display-align="center" xsl:use-attribute-sets="blk.header">
						<fo:block text-align="end">
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							<xsl:if test="@SPOT='NO' or not(@SPOT)">
								<fo:inline font-family="universbold" font-size="11pt">Codice Cliente <xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
							</xsl:if>
                        </fo:block>
                        <fo:block text-align="end">
							<xsl:if test="@SPOT='NO' or not(@SPOT)">
								<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Energit</fo:inline>
							</xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </fo:static-content>

    <!--
    ********************************************************
    **                     Footer                         **
    ********************************************************
    -->
    <fo:static-content flow-name="footer">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
			<fo:table-column column-width="proportional-column-width(85)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell display-align="center">
						<fo:block xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer2">
								<xsl:choose>
									<xsl:when test="@TIPO_DOCUMENTO='FATTURA'">Fattura: </xsl:when>
									<xsl:otherwise>Nota di credito: </xsl:otherwise>
								</xsl:choose>
								<xsl:value-of select="@NUMERO_DOCUMENTO"/>  del  <xsl:value-of select="./child::DATA_DOCUMENTO" />
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell display-align="center">
						<fo:block text-align="end" xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer2">
								<fo:inline xsl:use-attribute-sets="font_footer2">Pagina </fo:inline>
								<fo:inline text-align="end" xsl:use-attribute-sets="font_footer2"><fo:page-number/>/<fo:page-number-citation ref-id="{generate-id(.)}"/></fo:inline>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row height="2mm">
					<fo:table-cell number-columns-spanned="2" display-align="center" border-bottom="0.5 dashed thick black">
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row height="2mm">
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" display-align="center">
						<fo:block text-align="center" xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer">Energ.it S.p.A.  <xsl:if test="$status_piu_uno &gt; 201209 and $data_doc_number &lt; 20130127">in liquidazione</xsl:if> - <xsl:if test="$status_piu_uno &gt; 201606 and $status_piu_uno &lt; 201806">Sede Operativa:</xsl:if> Via Edward Jenner, 19/21 - 09121 Cagliari - Servizio Clienti 800.19.22.22 - Fax 800.19.22.55 - P.IVA 02605060926</fo:inline>
						</fo:block>
						<fo:block text-align="center" xsl:use-attribute-sets="blocco_footer">
							<xsl:if test="$status_piu_uno &lt; 201212"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Societa' per Azioni con Socio Unico. Direzione e Coordinamento di Alpiq Italia S.r.l.</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201211 and $status_piu_uno &lt; 201509"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società con Socio Unico soggetta ad attività di direzione e coordinamento di Onda s.r.l.</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201508 and $status_piu_uno &lt; 201511"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società soggetta a direzione e coordinamento di Enertronica S.p.A.</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201510 and $status_piu_uno &lt; 201607"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società per azioni con Socio Unico</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201606 and $status_piu_uno &lt; 201806"><fo:inline xsl:use-attribute-sets="font_footer">Sede Legale: Via Savoia, 38 - 96100 Siracusa - Iscrizione CCIAA di Cagliari n.02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società per azioni con Socio Unico</fo:inline></xsl:if>
     						<xsl:if test="$status_piu_uno &gt;= 201806"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n.02605060926 del 12/08/00 - Società per azioni con Socio Unico</fo:inline></xsl:if>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
        </fo:table>
    </fo:static-content>

    <!--
    ********************************************************
    **                  Inizio body                       **
    ********************************************************
    -->
    <fo:flow flow-name="body">
		<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>

		<!--
		********************************************************
		**               Logo E Data Matrix                   **
		********************************************************
		-->
		<fo:block-container position="absolute"
							top="2mm"
							left="0mm"
							width="90mm"
							height="22mm">
			<fo:block>
				<fo:table table-layout="fixed" width="100%">
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding-left="2mm">
								<xsl:choose>
									<xsl:when test="$color='black'">
										<!-- <xsl:attribute name="padding-top">13mm</xsl:attribute> -->
										<fo:block>
											<xsl:if test="$status_piu_uno &lt; 201212"><fo:external-graphic src="url(img/logo_energit_bn.svg)" content-width="37mm" /></xsl:if>
											<xsl:if test="$status_piu_uno &gt; 201211"><fo:external-graphic src="url(img/logo_energit_nopayoff_bn.svg)" content-width="37mm" /></xsl:if>
										</fo:block>
										<fo:block start-indent="2mm" font-family="Arial" font-size="5pt"
												  color="black">Via Edward Jenner, 19/21 - 09121 Cagliari
										</fo:block>
									</xsl:when>
									<xsl:otherwise>
										<!-- <xsl:attribute name="padding-top">13mm</xsl:attribute> -->
										<fo:block>
											<xsl:if test="$status_piu_uno &lt; 201212"><fo:external-graphic src="url(img/logo-energit.svg)" content-width="35mm" /></xsl:if>
											<xsl:if test="$status_piu_uno &gt; 201211"><fo:external-graphic src="url(img/logo_energit_nopayoff.jpg)" content-width="37mm" /></xsl:if>
										</fo:block>
										<fo:block start-indent="2mm" font-family="Arial" font-size="5pt"
												  color="black">Via Edward Jenner, 19/21 - 09121 Cagliari
										</fo:block>
									</xsl:otherwise>
								</xsl:choose>
							</fo:table-cell>

							<xsl:variable name="barcode_message_header">
								<xsl:value-of select="concat(
								'F_P', ' ',
								DATA_DOCUMENTO, ' ',
								@NUMERO_DOCUMENTO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CODICE_CLIENTE, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/INDIRIZZO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CAP, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CITTA, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/PROVINCIA)"/>
							</xsl:variable>

							<fo:table-cell text-align="right" display-align="after"
									padding-right="17mm">
								<fo:block>
									<fo:instream-foreign-object content-height="12mm" content-width="25mm">
										<barcode:barcode message="{$barcode_message_header}">
											<barcode:datamatrix>
												<barcode:quiet-zone enabled="false">0mm</barcode:quiet-zone>
												<barcode:module-width>0.7mm</barcode:module-width>
												<xsl:if test="string-length($barcode_message_header) &lt; 48">
													<barcode:shape>force-rectangle</barcode:shape>
												</xsl:if>
											</barcode:datamatrix>
										</barcode:barcode>
									</fo:instream-foreign-object>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:block-container>


		<xsl:if test="$status_piu_uno &gt; 201306 and @SPOT='NO'">
			<fo:block-container position="absolute"
								top="4mm"
								left="109.3mm"
								width="79mm"
								height="16mm"
								background-image="./svg/bordo_codice_cliente.svg"
								background-repeat="no-repeat"
								display-align="center"
								text-align="center"
								font-family="universcbold"
								font-size="14pt">
				<fo:block>
					<xsl:attribute name="color">#0065ae</xsl:attribute>
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						Codice Cliente <fo:inline font-family="universcbold" font-size="18pt"><xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
					</xsl:if>
				</fo:block>
				<fo:block>
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Energit</fo:inline>
					</xsl:if>
				</fo:block>
			</fo:block-container>
		</xsl:if>


		<!--
		*********************************************************
		**  Dati anagrafici e indirizzo di spedizione fattura  **
		*********************************************************
		-->
		<fo:block-container position="absolute"
							top="45.17mm"
							left="93mm"
							width="97mm"
							height="26mm">
			<xsl:call-template name="SPEDIZIONE_FATTURA" />
		</fo:block-container>





		<!-- BOLLETTA 2.0 -->

		<fo:block-container position="absolute"
							top="27mm"
							left="0mm"
							width="74.5mm"
							height="55mm"
							font-family="universc"
							font-size="7pt"
							background-color="#FFFFFF">

		<!--
		*********************************************
		**     Riquadro con bordi arrotondati      **
		*********************************************
		-->
		<fo:block font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>CONTATTI UTILI</fo:block>

		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(28.5)"/>
			<fo:table-column column-width="proportional-column-width(61.5)"/>
			<fo:table-body>
				<fo:table-row border-bottom="0.5 solid black" border-top="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/telefono.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							SERVIZIO CLIENTI
						</fo:block>
						<fo:block>
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							DA RETE FISSA
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							800.19.22.22
						</fo:block>
						<fo:block>
							gratuito (lun-ven 8.30 - 17.30)
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/cellulare.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							SERVIZIO CLIENTI
						</fo:block>
						<fo:block>
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							DA CELLULARE
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							070 7521 422
						</fo:block>
						<fo:block font-size="6pt">
							I costi della chiamata dipendono dal proprio operatore telefonico
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/fax.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							FAX GRATUITO
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							800.19.22.55
						</fo:block>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/web.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							SITO WEB
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							www.energit.it
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/chiocciola.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							POSTA ELETTRONICA
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							energia@energit.it
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/lettera.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							INFORMAZIONI E RECLAMI SCRITTI
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							Energit S.p.A.
						</fo:block>
						<fo:block>
							Via E. Jenner, 19/21 - 09121 Cagliari
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/guasti.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							PRONTO INTERVENTO
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::CONTATTO_DISTRIBUTORE"/>
						</fo:block>
						<fo:block>
							<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::DISTRIBUTORE"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>

		</fo:table>

		</fo:block-container>




		<fo:block-container position="absolute"
							top="83mm"
							left="0mm"
							width="190mm"
							height="170mm"
							font-family="universc"
							font-size="7pt"
							background-color="#FFFFFF">

		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="75mm"/>
			<fo:table-column column-width="5mm"/>
			<fo:table-column column-width="110mm"/>
			<fo:table-body>
				<fo:table-row height="153mm">




					<fo:table-cell padding-left="2mm" padding-right="2mm" padding-top="7mm" font-family="universc" font-size="7pt">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(<xsl:value-of select="$rectangle_frontespizio"/>.svg)</xsl:attribute>
							<xsl:call-template name="BOX_SX">
								<xsl:with-param name="color" select="$color_riquadro_scadenza"/>
								<xsl:with-param name="bs" select="$bs"/>
								<xsl:with-param name="disagio_economico" select="$disagio_economico"/>
							</xsl:call-template>
					</fo:table-cell>




					<fo:table-cell>
						<fo:block/>
					</fo:table-cell>

					<fo:table-cell padding-top="7mm" font-family="universc" font-size="7pt">

						<fo:block-container position="absolute"
							top="0mm"
							left="0mm"
							width="155mm"
							height="12mm">
							<fo:block>
								<fo:instream-foreign-object>
									<svg version="1.1"
									  xmlns="http://www.w3.org/2000/svg">
									  <xsl:attribute name="width"><xsl:value-of select="308.5"/></xsl:attribute>
									  <xsl:attribute name="height"><xsl:value-of select="20"/></xsl:attribute>
									  <xsl:attribute name="viewBox"><xsl:value-of select="'0 0 300 20'"/></xsl:attribute>
										<line x1="1" y1="7" x2="308.5" y2="7" stroke="black" stroke-width="1" />
										<!-- <rect x="199" y="1mm" width="100" height="14" rx="5" ry="5" fill="white" stroke="black" stroke-width="1"/> -->
									</svg>
								</fo:instream-foreign-object>
							</fo:block>
						</fo:block-container>

						<fo:block font-family="universcbold" font-size="14pt">
							<xsl:attribute name="color">#0065ae</xsl:attribute>
							SERVIZIO DI FORNITURA DI ENERGIA ELETTRICA
						</fo:block>

						<fo:block font-family="universcbold" font-size="12pt">
							Fattura n. <xsl:value-of select="@NUMERO_DOCUMENTO"/>  del  <xsl:value-of select="./child::DATA_DOCUMENTO" />
						</fo:block>

						<fo:block font-size="12pt">Periodo di fatturazione: <xsl:value-of select="./child::PERIODO_DOCUMENTO" /></fo:block>

						<xsl:choose>
							<xsl:when test="@TIPO_DOCUMENTO='FATTURA' and not(@COMPETENZA) and not(./child::SCADENZA_DOCUMENTO='******') and not(./child::IMPORTO_DOCUMENTO='0,00') and not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'La fattura risulta pagata')">
								<fo:block space-before="2mm" font-size="12pt">
									IL TOTALE DA PAGARE ENTRO IL <xsl:value-of select="./child::SCADENZA_DOCUMENTO" /> È <fo:inline font-family="universcbold"><xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> EURO</fo:inline>
								</fo:block>
							</xsl:when>
							<xsl:otherwise>
								<fo:block space-before="2mm" font-size="12pt">IL TOTALE DOCUMENTO È <fo:inline font-family="universcbold"><xsl:value-of select="./child::IMPORTO_DOCUMENTO" /></fo:inline> EURO</fo:block>
							</xsl:otherwise>
						</xsl:choose>

						<!-- <xsl:if test="./child::CONTRATTO_ENERGIA"> -->

						<fo:block height="20mm">
						<xsl:choose>
							<xsl:when test="./child::CONTRATTO_ENERGIA">
								<fo:block font-size="8pt" space-before="1mm"><!-- gfalchi: 10/05/2018-->
									(per <xsl:value-of select="$consumi_fatturati" /> kWh fatturati su <xsl:value-of select="count(./child::CONTRATTO_ENERGIA)" /> POD)
								</fo:block>
							</xsl:when>
							<xsl:otherwise>
									<fo:block>
										&#160;
									</fo:block>
							</xsl:otherwise>
						</xsl:choose>						
							<xsl:choose>
								<xsl:when test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO='Addebito tramite SDD e Sardex'">
									<!-- gfalchi: 10/05/2018 -->
									<xsl:if test="IMPORTO_SSX">
										<fo:block font-size="8pt" space-before="0mm">
											così ripartito: importo bonifico Sardex <xsl:value-of select="IMPORTO_SSX" /> SRD e addebito automatico SDD <xsl:value-of select="IMPORTO_SDD" /> Euro
										</fo:block>
									</xsl:if>
									<!-- 10/05/2018 -->
								</xsl:when>
								<xsl:when test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO='Addebito automatico su conto Sardex e su conto corrente bancario' or ./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO='Addebito automatico su conto Sardex'">
									<xsl:if test="IMPORTO_SSX">
										<fo:block font-size="8pt" space-before="0mm">
											Addebito automatico SDD <xsl:value-of select="IMPORTO_SDD" /> Euro e <xsl:value-of select="IMPORTO_SSX" /> SRD su conto Sardex
										</fo:block>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<fo:block>
										&#160;
									</fo:block>
								</xsl:otherwise>
							</xsl:choose>


						<!-- gfalchi: 10/05/2018 -->
						<xsl:choose>
							<xsl:when test="IMPORTO_SSX">
							<fo:block font-size="8pt" space-before="0mm">
								così ripartito: importo bonifico Sardex <xsl:value-of select="IMPORTO_SSX" /> SRD e addebito automatico SDD <xsl:value-of select="IMPORTO_SDD" /> Euro
							</fo:block>
							</xsl:when>
							<xsl:otherwise>
									<fo:block>
										&#160;
									</fo:block>
							</xsl:otherwise>
						</xsl:choose>
						<!-- 10/05/2018 -->


						</fo:block>	

						<fo:block>
							<fo:table table-layout="fixed" width="100%" border="0.5pt solid red">
								<fo:table-column width="100%"/>
								<fo:table-column width="100%"/>								
								<fo:table-body>
								
									<fo:table-row height="55mm">
									
										<!-- legenda grafico a torta (dettaglio importi) -->							
										<fo:table-cell display-align="center" border="0.5pt solid green">
		
											<xsl:call-template name="DETTAGLIO_IMPORTI">
												<xsl:with-param name="bordi" select="$bordi"/>
												<xsl:with-param name="consumi_fatturati" select="$consumi_fatturati"/>
												<xsl:with-param name="imponibile_no_altrepartite" select="$imponibile_no_altrepartite"/>
												<xsl:with-param name="imponibile_sv" select="$imponibile_sv"/>
												<xsl:with-param name="sv" select="$sv"/>
												<xsl:with-param name="sr" select="$sr"/>
												<xsl:with-param name="imp" select="$imp"/>
												<xsl:with-param name="iva" select="$iva"/>
												<xsl:with-param name="odiv" select="$odiv"/>
												<xsl:with-param name="bs" select="$bs"/>
												<xsl:with-param name="t_rai" select="$t_rai"/>
												<xsl:with-param name="doc" select="$doc"/>
												<xsl:with-param name="costo_medio" select="$costo_medio"/>
												<xsl:with-param name="costo_medio_sv" select="$costo_medio_sv"/>
											</xsl:call-template>								
		
										</fo:table-cell>
		
										<!-- grafico a torta -->								
										<fo:table-cell  display-align="center" border="0.5pt solid blue" >
		
											<xsl:call-template name="GRAFICO_TORTA">
												<xsl:with-param name="sv" select="$sv"/>
												<xsl:with-param name="sr" select="$sr"/>
												<xsl:with-param name="imp" select="$imp"/>
												<xsl:with-param name="iva" select="$iva"/>
												<xsl:with-param name="odiv" select="$odiv"/>
												<xsl:with-param name="bs" select="$bs"/>
												<xsl:with-param name="t_rai" select="$t_rai"/>
												<xsl:with-param name="importo_no_virgola" select="$importo_no_virgola"/>
											</xsl:call-template>									
										</fo:table-cell>
										
									</fo:table-row>
								
								</fo:table-body>
							</fo:table>
						</fo:block>




						<!--GRAFICO-->






						<xsl:choose>
							<xsl:when test="./RIEPILOGO_MULTISITO_ENERGIA">
								<xsl:call-template name="DETTAGLIO_IVA"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="SINTESI_FISCALE_MONOSITO3"/>
							</xsl:otherwise>
						</xsl:choose>





						<fo:table table-layout="fixed" width="100%">
						<fo:table-column column-width="108.5mm"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-family="universc" font-size="7pt" padding-top="2mm" border-top="1 solid black">


										<fo:block-container position="absolute" top="-2.5mm" left="-2.5mm" width="50mm" height="50mm">
											<fo:block>
												<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="6mm" src="img/informazioni.svg"/>
											</fo:block>
										</fo:block-container>

										<xsl:call-template name="STATO_PAGAMENTI"/>

										<xsl:call-template name="INFO_RID"/>

										<xsl:call-template name="ALTRE_COMUNICAZIONI_FRONTESPIZIO"/>

									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>

					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>
						<fo:block font-size="6pt">
							L'imposta di bollo, se dovuta, viene assolta in modo virtuale
						</fo:block>
						<fo:block font-size="6pt">
							(Aut. Agenzia Entrate, uff. Cagliari 1 n. 19756 del 29/04/2005).
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

			</fo:table-body>
		</fo:table>



		</fo:block-container>



		<fo:block-container position="absolute"
							top="84mm"
							left="24.5mm"
							width="30mm"
							height="4.5mm">

			<fo:block font-family="universcbold" font-size="10pt">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
				MERCATO LIBERO

			</fo:block>

		</fo:block-container>






		<!-- <fo:block-container position="absolute"
							top="85mm"
							left="155.5mm"
							width="30mm"
							height="4.5mm">

			<fo:block font-family="universcbold" font-size="10pt">
				MERCATO LIBERO
			</fo:block>

		</fo:block-container> -->



		<!-- BOLLETTA 2.0 -->



		<xsl:if test="@SPOT='NO' or not(@SPOT)">
<!-- 			<fo:block break-before="page"> -->
<!-- 			</fo:block> -->


			<xsl:if test="./child::CONTRATTO_ENERGIA">

				<xsl:choose>
					<xsl:when test="$status_piu_uno &lt; 201101 or substring(@COMPETENZA,8,4) &lt; 2011">
						<xsl:call-template name="CONTRATTI_MULTISITO">
							<xsl:with-param name="bordi" select="$bordi"/>
							<xsl:with-param name="color" select="$color"/>
							<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
							<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
							<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
							<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
							<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
							<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
							<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
              <xsl:with-param name="dettaglio_fattura" select="$dettaglio_fattura"/>
						</xsl:call-template>
					</xsl:when>

					<xsl:otherwise>
						<xsl:call-template name="CONTRATTI_MULTISITO_012011">
							<xsl:with-param name="bordi" select="$bordi"/>
							<xsl:with-param name="color" select="$color"/>
							<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
							<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
							<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
							<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
							<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
							<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
							<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
              <xsl:with-param name="dettaglio_fattura" select="$dettaglio_fattura"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>

			</xsl:if>


			<xsl:apply-templates select="CONTRATTO_DEPOSITO_CAUZIONALE">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>

			<xsl:apply-templates select="CONTRATTO_FONIA">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>

			<xsl:apply-templates select="CONTRATTO_SERVIZI_VARI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>

			<xsl:apply-templates select="CONTRATTO_AREASERVER">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>


			<xsl:choose>
				<xsl:when test="$status_piu_uno &lt; 201101">
					<xsl:call-template name="FINALE">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="FINALE_012011">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>


			<xsl:if test="./child::CONTRATTO_ENERGIA and $qualita='SI'">
				<xsl:call-template name="INFORMATIVA_QUALITA"/>
			</xsl:if>
		</xsl:if>


		<!--
		*********************************************************
		**  Blocco per la determinazione del numero di pagine  **
		*********************************************************
		-->
		<fo:block id="{generate-id(.)}"/>

    </fo:flow>
</fo:page-sequence>


<!--
**********************************************************
**					Rinnovi se presenti					**
**********************************************************
-->
<!-- <xsl:for-each select="./child::RINNOVI/child::RINNOVO[@VISUALIZZA='SI']">
    <fo:page-sequence master-reference="pm0-blank">
        <fo:flow flow-name="body">
					<fo:block-container position="absolute"
										top="3.75pt"
										left="-0.75pt"
										width="210mm"
										height="297mm"
										display-align="after">
						<fo:block text-align="center">
							<fo:external-graphic>
								<xsl:attribute name="height">297mm</xsl:attribute>
								<xsl:attribute name="content-height">297mm</xsl:attribute>
								<xsl:attribute name="content-width">210mm</xsl:attribute>
								<xsl:if test="./child::ALLEGATO='AL-FAEN0110FX'">
									<xsl:attribute name="src">./allegati_rinnovi/<xsl:value-of select="./child::ALLEGATO"/>-RN.pdf</xsl:attribute>
								</xsl:if>
								<xsl:if test="not(./child::ALLEGATO='AL-FAEN0110FX')">
									<xsl:attribute name="src">./allegati_rinnovi/<xsl:value-of select="./child::ALLEGATO"/>.pdf</xsl:attribute>
								</xsl:if>
							</fo:external-graphic>
						</fo:block>
					</fo:block-container>
        </fo:flow>
    </fo:page-sequence>
</xsl:for-each> -->

<!--
**********************************************************
**			  Nuove condizioni contrattuali				**
**********************************************************
-->
<!-- <xsl:if test="./child::CONTRATTO_ENERGIA[@NUOVE_CONDIZIONI='B1']">
    <xsl:call-template name="COMUNICAZIONE_CONDIZIONI"/>
    <xsl:call-template name="SCHEDA_RIEPILOGO"/>
    <xsl:call-template name="CONDIZIONI"/>
</xsl:if>

<xsl:if test="./child::CONTRATTO_ENERGIA[@NUOVE_CONDIZIONI='C11']">
    <xsl:call-template name="COMUNICAZIONE_CONDIZIONI_C11"/>
	<xsl:call-template name="SCHEDA_RIEPILOGO_C11"/>
    <xsl:call-template name="CONDIZIONI_C11"/>
</xsl:if> -->

<!--
**********************************************************
**					Autocertificazione					**
**********************************************************
-->
<!-- <xsl:if test="CONTRATTO_ENERGIA and $autocertificazione='SI' and (@SPOT='NO' or not(@SPOT))">
	<xsl:call-template name="AUTOCERTIFICAZIONE"/>
</xsl:if> -->

<!--
********************************************************
**             Accoda eventuali bollettini            **
********************************************************
-->
<xsl:if test="./child::BOLLETTINO and (@SPOT='NO' or not(@SPOT))">
	<xsl:call-template name="BOLLETTINO"/>
</xsl:if>

</xsl:template>






<!--
***********************************************
**  Riquadro lettura contatore - autolettura **
***********************************************
-->
<xsl:template name="LETTURA_AUTOLETTURA_FRONTESPIZIO">
	<xsl:choose>
		<xsl:when test="./child::AUTOLETTURA">
			<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
				AUTOLETTURA
			</fo:block>

			<xsl:choose>
				<xsl:when test="./child::AUTOLETTURA='MONO'">
					<fo:block>Per ricevere fatture allineate ai suoi consumi reali, usufruisca del servizio di autolettura!</fo:block>
				</xsl:when>

				<xsl:when test="./child::AUTOLETTURA='MULTI'">
					<fo:block>Per ricevere fatture allineate ai suoi consumi reali, usufruisca del servizio di autolettura, riservato ai punti di prelievo monorari!</fo:block>
				</xsl:when>
			</xsl:choose>

			<fo:block>Potra' trasmettere i consumi all'indirizzo autolettura@energit.it o al numero gratuito 800.1922.33 dal 25<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::DATA_AUTOLETTURA_INIZIALE" /> al <xsl:value-of select="./child::CONTRATTO_ENERGIA/child::DATA_AUTOLETTURA_FINALE" />.</fo:block>

			<fo:block>I consumi comunicati potranno essere utilizzati a partire dalla seconda autolettura.</fo:block>
		</xsl:when>

		<xsl:otherwise>
			<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
				LETTURA DEL CONTATORE
			</fo:block>

			<fo:block>La sua utenza e' inserita nel sistema di telelettura attraverso il contatore elettronico. Addebiteremo i suoi consumi effettivi ogni volta che saranno resi disponibili dal suo Distributore Locale.</fo:block>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>





<!--
***************************
**  Segnalazione guasti  **
***************************
-->
<xsl:template name="SEGNALAZIONE_GUASTI">
	<fo:block xsl:use-attribute-sets="blocco_riquadri_frontespizio">
		<fo:block text-align="center" xsl:use-attribute-sets="font_titolo_tabella_bold">
			SEGNALAZIONE GUASTI
		</fo:block>

		<fo:block>In caso di guasti dovra' contattare il Distributore Locale al numero telefonico <xsl:value-of select="./child::CONTRATTO_ENERGIA/child::CONTATTO_DISTRIBUTORE" />.</fo:block>
	</fo:block>
</xsl:template>





<!--
******************************
**  Modalita' di pagamento  **
******************************
-->
<xsl:template name="PAGAMENTI">
	<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">
		<xsl:attribute name="color">#0065ae</xsl:attribute>
		PAGAMENTI
	</fo:block>

	<xsl:choose>
		<xsl:when test="@TIPO_DOCUMENTO='FATTURA' and not(./child::IMPORTO_DOCUMENTO='0,00')">
			<xsl:choose>
				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
					<fo:block font-family="universcbold">Modalita' di pagamento</fo:block>
				</xsl:when>

				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_PE_REQ']">
					<fo:block font-family="universcbold">La modalita' di pagamento per questa fattura e'</fo:block>
				</xsl:when>

				<xsl:otherwise>
					<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'La fattura risulta pagata')">
						<fo:block font-family="universcbold">Lei ha scelto di pagare con</fo:block>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:choose>
				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_PE_REQ']">
					<fo:block>Bollettino Postale</fo:block>
					<fo:block space-before="2mm" font-family="universcbold">La sua richiesta di attivazione della modalita' addebito diretto SEPA Direct Debit (SDD) e' in attesa di conferma presso la sua Banca. Per il pagamento di questa fattura dovra' utilizzare il bollettino postale che trova nell'ultimo foglio. Grazie.</fo:block>
					<fo:block space-before="2mm" font-family="universcbold"><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CAUSALE" /></fo:block>
				</xsl:when>

				<xsl:otherwise>
					<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO" /></fo:block>

					<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bonifico Bancario'">
						<fo:block>Energ.it S.p.A.</fo:block>
					</xsl:if>

					<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::INTESTATARIO_PAGAMENTO" /></fo:block>

					<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bollettino Postale'">
						<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE" /></fo:block>
					</xsl:if>

					<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::IBAN">
						<fo:block>IBAN: <xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::IBAN" /></fo:block>
					</xsl:if>

					<!-- rdistefano -->
					<xsl:choose>
						<xsl:when test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Addebito automatico su conto Sardex e su conto corrente bancario') and not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Addebito automatico su conto Sardex') ">
							<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE = 'Sardex'">
								<fo:block>ID pagamento Sardex: 12492896</fo:block>
							</xsl:if>

							<xsl:if test="./child::IMPORTO_SSX">
								<fo:block>ID pagamento Sardex: 12492896</fo:block>
							</xsl:if>

							<fo:block font-family="universcbold"><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CAUSALE" /></fo:block>
						</xsl:when>

						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
					<!-- rdistefano -->

				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>

		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="./child::IMPORTO_DOCUMENTO='0,00'">
					<fo:block font-family="universcbold">Non c'e' niente da pagare!</fo:block>
				</xsl:when>

				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RIMBORSO_SDD']">
					<fo:block font-family="universcbold">Non c'e' niente da pagare!</fo:block>
					<fo:block font-family="universcbold">L'importo totale del documento di <xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> euro le sara' rimborsato tramite accredito sul suo conto corrente normalmente utilizzato per il pagamento delle fatture Energit.</fo:block>
				</xsl:when>

				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RIMBORSO_ASS']">
					<fo:block font-family="universcbold">Non c'e' niente da pagare!</fo:block>
					<fo:block font-family="universcbold">L'importo totale del documento di <xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> euro le sara' rimborsato tramite assegno inviato al suo indirizzo di spedizione delle fatture.</fo:block>
				</xsl:when>

				<xsl:otherwise>
					<fo:block font-family="universcbold">Per questa nota di credito non c'e' niente da pagare</fo:block>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>

	<fo:block space-before="2mm">Altre modalità di pagamento disponibili:</fo:block>
	<fo:block>

		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bonifico Bancario'">
			<fo:block>Bollettino Postale - Addebito su conto corrente bancario (SDD)</fo:block>
		</xsl:if>
		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bollettino Postale'">
			<fo:block>Bonifico Bancario - Addebito su conto corrente bancario (SDD)</fo:block>
		</xsl:if>
		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Addebito tramite SDD'">
			<fo:block>Bollettino Postale - Bonifico Bancario</fo:block>
		</xsl:if>
		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE = 'Sardex'">
			<fo:block>Bollettino Postale - Bonifico Bancario - Addebito su conto corrente bancario (SDD)</fo:block>
		</xsl:if>

		<!-- gfalchi: 10/05/2018 -->
		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Addebito tramite SDD e Sardex'">
			<fo:block>Bollettino Postale - Bonifico Bancario</fo:block>
		</xsl:if>
		<!-- gfalchi: 10/05/2018 -->

		<!-- rdistefano 09/07/2019 -->
		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Addebito automatico su conto Sardex e su conto corrente bancario'">
			<fo:block>Bollettino Postale - Bonifico Bancario</fo:block>
		</xsl:if>
		<!-- \rdistefano -->

	</fo:block>

	<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SARDEX'">
		<fo:block space-before="2mm">La informiamo che il pagamento della presente fattura potrà essere effettuato nelle modalità indicate nella proposta di contratto da lei sottoscritta, con espressa esclusione dell’utilizzo del circuito Sardex</fo:block>
	</xsl:if>

</xsl:template>





<!--
*****************************
**   Avvisi frontespizio   **
*****************************
-->
<xsl:template name="AVVISI_FRONTESPIZIO">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<!-- Comunicazione Energit per noi -->
			<fo:table-row height="27mm">
				<fo:table-cell>
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3'">

							<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute>
							<xsl:attribute name="padding">1mm</xsl:attribute> -->


							<xsl:variable name="contratto_exnoi">
								<xsl:for-each select="./child::CONTRATTO_ENERGIA">
									<xsl:if test="substring(./child::PRODOTTO_SERVIZIO,0,16)='Energit per Noi'">
										<xsl:value-of select="./child::CONTRACT_NO" />
									</xsl:if>
								</xsl:for-each>
							</xsl:variable>

							<fo:block font-family="universcbold">
								ENERGIT PER NOI RINNOVA ANCORA LA SUA CONVENIENZA
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Il risparmio Energit per Noi è confermato per il terzo anno consecutivo!
								Anche nel 2012, infatti, il suo contratto
								<xsl:value-of select="substring($contratto_exnoi,0,8)" /> le offre un
								<fo:inline font-family="universcbold">prezzo bloccato della componente energia
								invariato rispetto al 2010 e al 2011: soli 0,05 euro/kWh in tutte le fasce
								orarie</fo:inline>, il massimo risparmio e la comodità di consumare liberamente
								in tutte le ore della giornata. Potrà aumentare ulteriormente il suo risparmio
								con un uso accorto e responsabile dell’energia, che le permetterà di limitare i
								consumi e di salvaguardare l’ambiente!
							</fo:block>
						</xsl:if>

						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='NUOVO_PARTNER_INDUSTRIALE'">
							<fo:block font-family="universcbold">
								UN NUOVO PARTNER INDUSTRIALE E UN MONDO DI NUOVE OFFERTE ENERGIT: ENERGIA ELETTRICA, GAS, IMPIANTI FOTOVOLTAICI E MOLTO DI PIU’
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Dal 23/11/2012 <fo:inline font-family="universcbold">Energit ha un nuovo partner industriale: Onda Energia!</fo:inline>
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Ai nostri Clienti garantiremo la fornitura senza interruzioni,
								il mantenimento delle condizioni contrattuali e… tante occasioni di risparmio.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								<fo:inline font-family="universcbold">Per scoprire in anteprima le nuove offerte
								che le abbiamo dedicato chiami l’800.19.22.22</fo:inline>; non appena la voce guida richiederà l’inserimento
								di un <fo:inline font-family="universcbold">codice promozione</fo:inline>,
								digiti <fo:inline font-family="universcbold">50</fo:inline> sulla tastiera del telefono.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								La aspettiamo!
							</fo:block>
						</xsl:if>

						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_BANCA_CARIGE'] and
									  not(PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='NUOVO_PARTNER_INDUSTRIALE')">
							<fo:block font-family="universcbold">
								NUOVI RIFERIMENTI BANCARI PER IL PAGAMENTO DELLE FATTURE
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								La invitiamo ad utilizzare il <fo:inline font-family="universcbold">nuovo conto corrente
								bancario Energit</fo:inline> per il pagamento delle fatture.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energ.it S.p.A. <fo:inline font-family="universcbold">Codice IBAN
								<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_BANCA_CARIGE']/child::ID='COMUNICAZIONE_BANCA_CARIGE_V1'">IT43I0617516901000000790980</xsl:if>
								<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_BANCA_CARIGE']/child::ID='COMUNICAZIONE_BANCA_CARIGE_V2'">IT22X0343116901000000790980</xsl:if>
								</fo:inline>. Banca Carige Italia S.p.A.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Grazie!
							</fo:block>
						</xsl:if>

						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS_FRONTESPIZIO']/child::ID='PAPERLESS_V4' and
									  not(PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='NUOVO_PARTNER_INDUSTRIALE')">
							<fo:block font-family="universcbold">
								PASSI ALLA FATTURA VIA EMAIL: RISPARMIERA’ FINO A 18 EURO ALL’ANNO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								<fo:inline font-family="universcbold">Scelga di ricevere la fattura via email:
								risparmierà le spese di spedizione del documento cartaceo</fo:inline>, pari ad
								1,50 € per fattura. Fino a 18 euro all'anno!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								La fattura via email, inoltre, rispetta l’ambiente, arriva tempestivamente ed è a colori!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Invii la sua richiesta a <fo:inline font-family="universcbold">paperless@energit.it</fo:inline> e
								specificando il suo codice cliente <xsl:value-of select="./child::CODICE_CLIENTE" />.
							</fo:block>
						</xsl:if>

						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_FATTURA_INTEGRATIVA']/child::ID='COMUNICAZIONE_FATTURA_INTEGRATIVA_V1'">
							<fo:block space-before="2mm">
								<fo:inline font-family="universcbold">INFORMAZIONE IMPORTANTE</fo:inline>. Il presente
								documento si intende ad integrazione della precedente fattura,
								emessa in data 11/11/2012. Ci scusiamo per l’inconveniente,
								dovuto a cause tecniche, e la ringraziamo per aver scelto Energit.
							</fo:block>
						</xsl:if>

						<!-- TOP6000 -->
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE91'
									 ">
							<fo:block font-family="universcbold">
								PER LEI, GLI ESCLUSIVI SCONTI TOP6000 CHE AUMENTANO NEL TEMPO: LI RICHIEDA SUBITO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato
								gli <fo:inline font-family="universcbold">EXTRA SCONTI TOP6000, del valore di 3-6-9 euro/MWh;
								per ottenerli deve solo richiederli!</fo:inline> Non perda questa occasione: trova maggiori
								informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>

						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE92'
									 ">
							<fo:block font-family="universcbold">
								PER LEI, GLI ESCLUSIVI SCONTI TOP6000 CHE AUMENTANO NEL TEMPO: SCOPRA COME OTTENERLI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit desidera offrirle
								gli <fo:inline font-family="universcbold">EXTRA SCONTI TOP6000, del valore
								di 3-6-9 euro/MWh</fo:inline> e riservati ai Clienti in regola con i pagamenti:
								non perda questa opportunità! Trova tutti i dettagli sull’iniziativa nella
								sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>

						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE111'
									 ">
							<fo:block font-family="universcbold">
								INFORMAZIONE IMPORTANTE
							</fo:block>
							<fo:block font-family="universcbold">
								ULTIMI GIORNI PER RICHIEDERE GLI ESCLUSIVI SCONTI TOP6000 CHE AUMENTANO NEL TEMPO: SI AFFRETTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato gli <fo:inline font-family="universcbold">EXTRA SCONTI TOP6000, del valore
								di 3-6-9 euro/MWh; per ottenerli deve solo richiederli entro il 31 luglio 2012!</fo:inline> Non perda questa occasione:
								trova maggiori informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>

						<!-- EnergiTOP -->
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE128'
									 ">
							<fo:block font-family="universcbold">
								ENERGITOP LE OFFRE 3+6+12 EURO DI EXTRA SCONTO SULL’ENERGIA. SCELGA IL RISPARMIO CHE RADDOPPIA NEL TEMPO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato
								gli <fo:inline font-family="universcbold">EXTRA SCONTI ENERGITOP, del valore di 3+6+12  euro/MWh;
								per ottenerli deve solo richiederli!</fo:inline> Non perda questa occasione: trova maggiori
								informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>

						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE129'
									 ">
							<fo:block font-family="universcbold">
								ENERGITOP LE OFFRE 3+6+12 EURO DI EXTRA SCONTO SULL’ENERGIA. SCELGA IL RISPARMIO CHE RADDOPPIA NEL TEMPO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit desidera offrirle
								gli <fo:inline font-family="universcbold">EXTRA SCONTI ENERGITOP, del valore di 3+6+12
								euro/MWh</fo:inline> e riservati ai Clienti in regola con i pagamenti: non perda questa
								opportunità! Trova tutti i dettagli sull’iniziativa nella sezione “Comunicazioni ai Clienti” di
								questa fattura.
							</fo:block>
						</xsl:if>

						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE172'
									 ">
							<fo:block font-family="universcbold">
								CON MAXIBONUS ENERGIT LE OFFRE 45 euro DI EXTRA SCONTO: NE APPROFITTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit desidera offrirle
								gli <fo:inline font-family="universcbold">EXTRA SCONTI MAXIBONUS, per un totale di 45 euro</fo:inline> e riservati
								ai Clienti in regola con i pagamenti: non perda questa opportunità! Trova tutti i dettagli sull’iniziativa nella
								sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>

						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE173'
									 ">
							<fo:block font-family="universcbold">
								CON MAXIBONUS ENERGIT LE OFFRE 45 euro DI EXTRA SCONTO: NE APPROFITTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato gli
								<fo:inline font-family="universcbold">EXTRA SCONTI MAXIBONUS, per un totale di 45 euro; per ottenerli deve solo richiederli!</fo:inline> Non
								perda questa occasione: trova maggiori informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>

						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE174'
									 ">
							<fo:block font-family="universcbold">
								COMPLIMENTI, LEI RICEVE GLI EXTRA SCONTI MAXIBONUS, RISERVATI AI MIGLIORI CLIENTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Per maggiori informazioni consulti la sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>

						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE193'
									 ">
							<fo:block font-family="universcbold">
								COMPLIMENTI, LEI RICEVE GLI EXTRA SCONTI MAXIBONUS, RISERVATI AI MIGLIORI CLIENTI!
							</fo:block>

							<fo:block text-align="justify" keep-with-previous="always">
								Per maggiori informazioni consulti la sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>

			<!-- Frasi SDD ERRATO/REVOCATO - Deposito Cauzionale -->
			<fo:table-row height="11mm">
				<fo:table-cell>
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO='Addebito tramite SDD')">
							<xsl:choose>
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
									<fo:block font-family="universcbold">
										<xsl:attribute name="color">#0065ae</xsl:attribute>
										ATTENZIONE! La sua Banca ha respinto la richiesta di attivazione dell'addebito diretto SEPA Direct Debit (SDD).
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>

								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_REVOCATO']">
									<fo:block font-family="universcbold">
										<xsl:attribute name="color">#0065ae</xsl:attribute>
										Riattivi subito l'addebito diretto SEPA Direct Debit (SDD) per evitare il pagamento del deposito cauzionale!
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>

								<xsl:otherwise>
									<xsl:if test="@PRESENZA_DEPOSITO_CAUZIONALE='SI'">
										<fo:block font-family="universcbold">
											<xsl:attribute name="color">#0065ae</xsl:attribute>
											Rientri in possesso del deposito cauzionale passando all'addebito diretto SEPA Direct Debit (SDD)!
										</fo:block>
										<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>

			<!-- Avviso fatture pagate/in attesa di pagamento -->
			<fo:table-row height="14mm">
				<fo:table-cell border-bottom="0.5 dashed thick black">
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="not(./child::FATTURE_ATTESA_PAGAMENTO) or (./child::FATTURE_ATTESA_PAGAMENTO and (count(./child::FATTURE_ATTESA_PAGAMENTO) &lt; 1))">
							<fo:block font-family="universcbold">
								TUTTE LE FATTURE PRECEDENTI RISULTANO PAGATE. GRAZIE.
							</fo:block>

							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								<fo:block font-size="7pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>

						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO)=1">
							<fo:block font-family="universcbold">
								ATTENZIONE: 1 FATTURA RISULTA IN ATTESA DI PAGAMENTO, PER UN TOTALE INSOLUTO DI EURO <xsl:value-of select="./child::TOTALE_FATTURE_ATTESA_PAGAMENTO" />.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>

							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								<fo:block font-size="7pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>

						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO) &gt; 1">
							<fo:block font-family="universcbold">
								ATTENZIONE: <xsl:value-of select="count(./child::FATTURE_ATTESA_PAGAMENTO)"/> FATTURE RISULTANO IN ATTESA DI PAGAMENTO, PER UN TOTALE INSOLUTO DI EURO <xsl:value-of select="./child::TOTALE_FATTURE_ATTESA_PAGAMENTO" />.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>

							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								<fo:block font-size="7pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
</xsl:template>





<!--
**********************
**  Area Clienti    **
**********************
-->
<xsl:template name="AREA_CLIENTI">

	<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
	<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute><xsl:if test="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2),substring(DATA_DOCUMENTO,1,2)) &lt; 20200301">NUOVA </xsl:if>AREA CLIENTI</fo:block>

	<fo:block>Scopri i vantaggi e le funzionalità della nuova area clienti Energit: accedi subito su www.energit.it attraverso il tuo codice cliente.</fo:block>
	<fo:block><fo:inline font-family="universcbold">USERNAME: </fo:inline><xsl:value-of select="./child::CODICE_CLIENTE" /></fo:block>

</xsl:template>






<!--
**********************
**  Dati anagrafici **
**********************
-->
<xsl:template name="DATI_ANAGRAFICI">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.004 chr.009">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row>
				<fo:table-cell>
					<fo:block xsl:use-attribute-sets="chrbold.009">
						Cliente
					</fo:block>
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INTESTATARIO" /></fo:block>
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INDIRIZZO" /></fo:block>
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row border-bottom="0.5 dashed thick black">
				<fo:table-cell>
					<fo:block>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CITTA" />
						<xsl:if test="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA">
							<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA" /></fo:inline>
						</xsl:if>
						<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::NAZIONE='IT')">
							<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::STATO" /></fo:inline>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row>
				<fo:table-cell padding-top="1mm">
					<fo:block>
						<fo:inline xsl:use-attribute-sets="chrbold.009">
							Partita IVA
						</fo:inline>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row>
				<fo:table-cell>
					<fo:block>
						<fo:inline xsl:use-attribute-sets="chrbold.009">
							Codice Fiscale
						</fo:inline>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_FISCALE" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>

			<xsl:if test="@SPOT='NO' or not(@SPOT)">
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:inline xsl:use-attribute-sets="chrbold.009">
								UserID
							</fo:inline>
							<xsl:value-of select="./child::USERNAME" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>

			<!-- <xsl:if test="./child::CIG">

				<xsl:if test="@SPOT='NO' or not(@SPOT)">
					<fo:table-row>
						<fo:table-cell>
							<fo:block>
								<fo:inline xsl:use-attribute-sets="chrbold.009">
									Codice CIG
								</fo:inline>
								<xsl:value-of select="./child::CIG" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</xsl:if> -->
		</fo:table-body>
	</fo:table>
</xsl:template>





<!--
**********************************************
**  Riquadro fattura con bordi arrotondati  **
**********************************************
-->
<xsl:template name="BOX_SX">
	<xsl:param name="color"/>
	<xsl:param name="bs"/>
	<xsl:param name="disagio_economico"/>
	<fo:block font-family="universcbold" font-size="8pt">	<xsl:attribute name="color">#0065ae</xsl:attribute>CLIENTE</fo:block>
	<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INTESTATARIO" /></fo:block>
	<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INDIRIZZO" /><xsl:text>&#160;</xsl:text><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CITTA" /> (<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA" />)</fo:block>
	<xsl:if test="./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA">
		<fo:block><fo:inline font-family="universcbold">PARTITA IVA: </fo:inline><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA" /></fo:block>
	</xsl:if>
	<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA)">
		<fo:block><fo:inline font-family="universcbold">CODICE FISCALE: </fo:inline><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_FISCALE" /></fo:block>
	</xsl:if>
<!-- 	<fo:block><fo:inline font-family="universcbold">USERNAME: </fo:inline><xsl:value-of select="./child::USERNAME" /></fo:block> -->

	<xsl:choose>

		<xsl:when test="./child::ANAGRAFICA_DOCUMENTO/PARTITA_IVA">

			<fo:block>
				<fo:inline font-family="universcbold">CODICE DESTINATARIO:</fo:inline>
					<xsl:choose>
						<xsl:when test="./child::CODICE_DESTINATARIO!='0000000'">
							<xsl:value-of select="./child::CODICE_DESTINATARIO" />
						</xsl:when>
						<xsl:otherwise>
								0000000
							<fo:block>
								potete comunicare il vostro codice univoco direttamente a [indirizzo]
							</fo:block>
						</xsl:otherwise>
					</xsl:choose>
			</fo:block>

			<fo:block>
				<fo:inline font-family="universcbold">PEC: </fo:inline>
					<xsl:choose>
						<xsl:when test="./child::PEC_DESTINATARIO!=''">
							<xsl:value-of select="./child::PEC_DESTINATARIO" />
						</xsl:when>
						<xsl:otherwise>
								nessuna PEC comunicata, rendete nota la vostra PEC a [indirizzo]
						</xsl:otherwise>
					</xsl:choose>
			</fo:block>

		</xsl:when>

		<xsl:otherwise>
			<fo:block></fo:block>
		</xsl:otherwise>

	</xsl:choose>





	<xsl:call-template name="AREA_CLIENTI"/>

	<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>

	<xsl:call-template name="PAGAMENTI"/>

<!-- 	<fo:block space-before="3mm" border-bottom="0.5 dashed black"/> -->

<!-- 	<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>FASCE DI CONSUMO</fo:block> -->
<!-- 	<fo:list-block provisional-distance-between-starts="12pt" provisional-label-separation="3pt"> -->
<!-- 		<fo:list-item> -->
<!-- 			<fo:list-item-label end-indent="label-end()"> -->
<!-- 				<fo:block>F1</fo:block> -->
<!-- 			</fo:list-item-label> -->
<!-- 			<fo:list-item-body start-indent="body-start()"> -->
<!-- 				<fo:block> -->
<!-- 					Dalle 8.00 alle 19.00 dei giorni lunedì - venerdì. -->
<!-- 				</fo:block> -->
<!-- 			</fo:list-item-body> -->
<!-- 		</fo:list-item> -->
<!-- 		<fo:list-item> -->
<!-- 			<fo:list-item-label end-indent="label-end()"> -->
<!-- 				<fo:block>F2</fo:block> -->
<!-- 			</fo:list-item-label> -->
<!-- 			<fo:list-item-body start-indent="body-start()"> -->
<!-- 				<fo:block> -->
<!-- 					Dalle 7.00 alle 8.00 e dalle 19.00 alle 23.00 dei giorni lunedì - venerdì. -->
<!-- 				</fo:block> -->
<!-- 				<fo:block> -->
<!-- 					Dalle 7.00 alle 23.00 del sabato. -->
<!-- 				</fo:block> -->
<!-- 			</fo:list-item-body> -->
<!-- 		</fo:list-item> -->
<!-- 		<fo:list-item> -->
<!-- 			<fo:list-item-label end-indent="label-end()"> -->
<!-- 				<fo:block>F3</fo:block> -->
<!-- 			</fo:list-item-label> -->
<!-- 			<fo:list-item-body start-indent="body-start()"> -->
<!-- 				<fo:block> -->
<!-- 					Dalle 0.00 alle 7.00 e dalle 23.00 alle 24.00 dei giorni lunedì - sabato. -->
<!-- 				</fo:block> -->
<!-- 				<fo:block> -->
<!-- 					Tutte le ore per domenica e festivi. -->
<!-- 				</fo:block> -->
<!-- 			</fo:list-item-body> -->
<!-- 		</fo:list-item> -->
<!-- 	</fo:list-block> -->

<!-- 	<fo:block space-before="3mm" border-bottom="0.5 dashed black"/> -->

<!-- 	<xsl:call-template name="LETTURA_AUTOLETTURA_FRONTESPIZIO"/> -->

<!--  	<fo:block space-before="3mm" border-bottom="0.5 dashed black"/> -->

<!-- 	<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">BOLLETTA 2.0</fo:block> -->
<!-- 	<fo:block>Con la Delibera n. 501/2014/R/com l’Autorità per l’energia elettrica e il gas ha definito -->
<!-- 	le nuove linee guida per la fatturazione dell’energia elettrica e gas naturale ai clienti finali, -->
<!-- 	in vigore dal 01/01/2016. Obiettivo di tale innovazione è offrire all’utente maggiore semplicità, -->
<!-- 	trasparenza e chiarezza nella lettura della bolletta.</fo:block> -->
	<xsl:variable name="dataDoc"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2),substring(DATA_DOCUMENTO,1,2))"/></xsl:variable>


<!-- 	<xsl:if test="not($bs = 0)"> -->
<!-- 		<fo:block space-before="3mm" border-bottom="0.5 dashed black"/> -->

<!-- 		<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>BONUS ELETTRICO</fo:block> -->
<!-- 		<fo:block>La sua fornitura è ammessa alla compensazione della spesa per la -->
<!-- 		fornitura di energia elettrica ai sensi del decreto 28 dicembre 2007 -->
<!-- 		(cosiddetto bonus sociale elettrico).</fo:block> -->
 		<!-- <xsl:if test="$disagio_economico = 1"> -->
<!-- 			<fo:block>La richiesta di rinnovo deve essere effettuata entro mese/anno</fo:block> -->
<!-- 		</xsl:if> -->
<!-- 	</xsl:if> -->







	<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
		<xsl:if test="./child::CONTRATTO_ENERGIA/BONUS_PRODOTTO_CUMULATIVO!='0,00' and ./child::CONTRATTO_ENERGIA/BONUS_PRODOTTO_CUMULATIVO!='0' ">
			<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
			<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>BONUS ENERGIT</fo:block>

			<fo:block keep-with-previous="always">
	<!-- 			<fo:inline> -->
	<!-- 				In questa fattura le è riconosciuto un bonus di <xsl:value-of select="./child::CONTRATTO_ENERGIA/BONUS_PRODOTTO_CUMULATIVO"/> Euro per il periodo che intercorre dall'inizio della fornitura (<xsl:value-of select="./child::CONTRATTO_ENERGIA/INIZIO_FORNITURA"/>) a <xsl:value-of select="./child::CONTRATTO_ENERGIA/PERIODO_RIFERIMENTO"/> .  -->
	<!-- 			</fo:inline> -->
				<fo:inline>
					La informiamo che dall'inizio di fornitura (<xsl:value-of select="./child::CONTRATTO_ENERGIA/INIZIO_FORNITURA"/>)
					ad oggi, grazie ai Bonus Energit, lei ha risparmiato un importo totale di <xsl:value-of select="./child::CONTRATTO_ENERGIA/BONUS_PRODOTTO_CUMULATIVO"/> euro a netto IVA.
				</fo:inline>
			</fo:block>
		</xsl:if>
	</xsl:if>

	<xsl:choose>
		<xsl:when test="./RIEPILOGO_MULTISITO_ENERGIA or not(./CONTRATTO_ENERGIA/CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO) or sum(./CONTRATTO_ENERGIA/CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO/FASCIA/ENERGIA_ATTIVA)=0">
			<xsl:choose>
				<xsl:when test="($dataDoc &gt;= 20190201) and ($dataDoc  &lt; 20190501)">
					<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
						<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>AGGIORNAMENTI TARIFFARI ARERA</fo:block>
						<fo:block>Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia,
						 Reti e Ambiente (ARERA) ai sensi delle delibere n. 301/2012/R/eel, 654/2015/R/eel e ARG/elt 107/09,
						 soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al primo  trimestre 2019,
						 per effetto delle delibere 670/2018/R/eel, 671/2018/R/eel, 673/2018/R/eel, 708/2018/R/eel, 706/2018/R/eel e 711/2018/R/com.
						 Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale”.
						 </fo:block>
				 </xsl:when>
				 <xsl:when test="($dataDoc &gt;= 20190501) and ($dataDoc  &lt; 20190801)">
					<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
						<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>AGGIORNAMENTI TARIFFARI ARERA</fo:block>
							<fo:block>
								Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia,
								Reti e Ambiente (ARERA) ai sensi delle delibere n. 301/2012/R/eel, 654/2015/R/eel e ARG/elt 107/09,
								soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al secondo trimestre 2019,
								per effetto delle delibere 107/2019/R/com e 109/2019/R/eel.
								Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale”.
							</fo:block>
					<fo:block>
					 </fo:block>
				 </xsl:when>
				 <xsl:when test="($dataDoc &gt;= 20190801) and ($dataDoc  &lt; 20191101)">
					<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
						<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>AGGIORNAMENTI TARIFFARI ARERA</fo:block>
							<fo:block>
								Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia,
								Reti e Ambiente (ARERA) ai sensi delle delibere n. 301/2012/R/eel, 654/2015/R/eel e ARG/elt 107/09,
								soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al secondo trimestre 2019,
								per effetto delle delibere 263/2019/R/eel e 262/2019/R/com.
								Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale“..
							</fo:block>
				 </xsl:when>
				 <xsl:when test="($dataDoc &gt;= 20191101) and ($dataDoc  &lt;= 20201031)">
					<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
					<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>AGGIORNAMENTI TARIFFARI ARERA</fo:block>
						<fo:block>
							Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia,
							Reti e Ambiente (ARERA) ai sensi delle delibere n. 301/2012/R/eel, 654/2015/R/eel e ARG/elt 107/09,
							soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al quarto trimestre 2019,
							per effetto delle delibere 382/2019/R/com e 383/2019/R/eel.
							Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale“.
						</fo:block>
			 	</xsl:when>			 	
				 <xsl:otherwise>
				 	<fo:block>
				 	</fo:block>
				 </xsl:otherwise>
			 </xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="GRAFICO_CONSUMI_FASCIA"/>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>




<!--
*************************
**  Grafico a Torta    **
*************************
 -->
<xsl:template name="GRAFICO_TORTA">


	<xsl:param name="sv"/>
	<xsl:param name="sr"/>
	<xsl:param name="imp"/>
	<xsl:param name="iva"/>
	<xsl:param name="odiv"/>
	<xsl:param name="bs"/>
	<xsl:param name="t_rai"/>
	
	<xsl:param name="importo_no_virgola"/>

	<xsl:variable name="double_pi">
		<xsl:value-of select="2 * 3.1415292"/>
	</xsl:variable>




	<xsl:choose>
		<xsl:when test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE='0,00') and (./child::CONTRATTO_ENERGIA)">
			<xsl:attribute name="top">0mm</xsl:attribute>
		</xsl:when>
		<xsl:otherwise>
			<xsl:attribute name="top">4mm</xsl:attribute>
		</xsl:otherwise>
	</xsl:choose>
	<fo:block text-align="center" display-align="center">
		<fo:instream-foreign-object>
			<svg
			 	   xmlns="http://www.w3.org/2000/svg"
			 	   xmlns:xlink="http://www.w3.org/1999/xlink"
			       viewBox="-1 -1 2 2"
			       style="transform: rotate(-0.25turn)"
			       width="124"
			       height="124"
			       
		     >

			<!-- per aggiungere patterns -->
			<defs>
				<pattern id="prova" patternUnits="userSpaceOnUse" width="0.4" height="0.4">
					<image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMC42MCIgaGVpZ2h0PSIwLjMwIj4NCjxkZWZzPg0KPHJlY3QgaWQ9InIiIHdpZHRoPSIwLjMwIiBoZWlnaHQ9IjAuMTUiIGZpbGw9IiNiYjA4NWYiIHN0cm9rZS13aWR0aD0iMC4wMjUiIHN0cm9rZT0iIzdhMDU0ZCI+PC9yZWN0Pg0KPGcgaWQ9InAiPg0KPHVzZSB4bGluazpocmVmPSIjciI+PC91c2U+DQo8dXNlIHk9IjAuMTUiIHhsaW5rOmhyZWY9IiNyIj48L3VzZT4NCjx1c2UgeT0iMC4zMCIgeGxpbms6aHJlZj0iI3IiPjwvdXNlPg0KPHVzZSB5PSIwLjQ1IiB4bGluazpocmVmPSIjciI+PC91c2U+DQo8L2c+DQo8L2RlZnM+DQo8dXNlIHhsaW5rOmhyZWY9IiNwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIC0wLjI1KSBza2V3WSgwLjQwKSI+PC91c2U+DQo8dXNlIHhsaW5rOmhyZWY9IiNwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjMwIDApIHNrZXdZKC0wLjQwKSI+PC91c2U+DQo8L3N2Zz4=" x="0" y="0" width="3" height="3">
					</image>
				</pattern>
			</defs>

			<xsl:variable name="somma">
				<xsl:value-of select="math:abs($sv) + math:abs($sr) + math:abs($bs) + math:abs($imp) + math:abs($iva) + math:abs($odiv) + math:abs( $t_rai  )"/>
			</xsl:variable>
			
			<xsl:choose>
				<xsl:when test="$sv=0 and $sr=0 and $bs=0 and $imp=0 and $iva=0 and $odiv=0 and $t_rai=0">
					<circle cx="0" cy="0" r="1" stroke="black" stroke-width="0.001" fill="white"/>
					<circle cx="0" cy="0" r="0.5" stroke="black" stroke-width="0.001" fill="white"/>
				</xsl:when>
				<xsl:otherwise>

		 			<xsl:variable name="svPercent"><xsl:value-of select="math:abs($sv div $somma)"/></xsl:variable>
					<xsl:variable name="srPercent"><xsl:value-of select="math:abs($sr div $somma)"/></xsl:variable>
					<xsl:variable name="impPercent"><xsl:value-of select="math:abs($imp div $somma)"/></xsl:variable>
					<xsl:variable name="ivaPercent"><xsl:value-of select="math:abs($iva div $somma)"/></xsl:variable>
					<xsl:variable name="odivPercent"><xsl:value-of select="math:abs(math:abs($odiv) div $somma)"/></xsl:variable>
					<xsl:variable name="raiPercent"><xsl:value-of select="math:abs(($t_rai ) div $somma)"/></xsl:variable>
					<xsl:variable name="bsPercent"><xsl:value-of select="math:abs(math:abs($bs) div $somma)"/></xsl:variable>
					<xsl:variable name="cumulativePercent"><xsl:value-of select="0"/></xsl:variable>

					<xsl:choose>
						<xsl:when test="$svPercent &gt; 0">
						
							<xsl:variable name="XsvPercentStart"><xsl:value-of select="math:cos( $double_pi * $cumulativePercent)"/></xsl:variable>
							<xsl:variable name="YsvPercentStart"><xsl:value-of select="math:sin( $double_pi * $cumulativePercent)"/></xsl:variable>
							<xsl:variable name="XsvPercentEnd"><xsl:value-of select="math:cos( $double_pi * $svPercent )"/></xsl:variable>
							<xsl:variable name="YsvPercentEnd"><xsl:value-of select="math:sin( $double_pi * $svPercent )"/></xsl:variable>

							<xsl:variable name="textPercX"><xsl:value-of select="math:cos( $double_pi * ($svPercent div 2) )*0.85"/></xsl:variable>
							<xsl:variable name="textPercY"><xsl:value-of select="math:sin( $double_pi * ($svPercent div 2) )*0.85"/></xsl:variable>

							<xsl:variable name="largeArcFlagSvPercent">
							  <xsl:choose>
							    <xsl:when test="$svPercent &gt; 0.5">
							      <xsl:text>1</xsl:text>
							    </xsl:when>
							   <xsl:otherwise>
							      <xsl:text>0</xsl:text>
							    </xsl:otherwise>
							  </xsl:choose>
							</xsl:variable>


							<xsl:variable name="slice1part1"><xsl:value-of select="concat('M ',$XsvPercentStart,' ',$YsvPercentStart,' ')"/></xsl:variable>
							<xsl:variable name="slice1part2"><xsl:value-of select="concat('A 1 1 0 ',$largeArcFlagSvPercent,' ','1 ',$XsvPercentEnd,' ',$YsvPercentEnd,' L 0 0')"/></xsl:variable>
							<xsl:variable name="slice1"><xsl:value-of select="concat($slice1part1,$slice1part2)"></xsl:value-of></xsl:variable>

							<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="#CD642D" fill-opacity="1.0" >
								<xsl:attribute name="d"><xsl:value-of select="$slice1"/></xsl:attribute>
							</path>


<!-- 							<line style = "stroke: #000000;stroke-width:0.01;stroke-dasharray:5, 2"  x1="{$textPercX}" y1="{$textPercY}" x2="{$textPercX * 2.20}" y2="{$textPercY * 2.20}"/> -->

						</xsl:when>
							<xsl:otherwise>
							</xsl:otherwise>
						</xsl:choose>


						<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $svPercent"/></xsl:variable>
							<xsl:choose>
								<xsl:when test="$srPercent &gt; 0">

									<xsl:variable name="XsrPercentStart"><xsl:value-of select="math:cos( $double_pi * $cumulativePercent)"/></xsl:variable>
									<xsl:variable name="YsrPercentStart"><xsl:value-of select="math:sin( $double_pi * $cumulativePercent)"/></xsl:variable>
									<xsl:variable name="XsrPercentEnd"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent+$srPercent))"/></xsl:variable>
									<xsl:variable name="YsrPercentEnd"><xsl:value-of select="math:sin( $double_pi * ($cumulativePercent+$srPercent) )"/></xsl:variable>
 									<xsl:variable name="textPercX"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent+($srPercent div 2)) )*0.85"/></xsl:variable>
									<xsl:variable name="textPercY"><xsl:value-of select="math:sin( $double_pi * ($cumulativePercent+($srPercent div 2)) )*0.85"/></xsl:variable>

									<xsl:variable name="largeArcFlagSrPercent">
										<xsl:choose>
											<xsl:when test="$srPercent &gt; 0.5">
												<xsl:text>1</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>0</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>

								<xsl:variable name="slice2part1"><xsl:value-of select="concat('M ',$XsrPercentStart,' ',$YsrPercentStart,' ')"/></xsl:variable>
								<xsl:variable name="slice2part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagSrPercent,' ','1 ',$XsrPercentEnd,' ',$YsrPercentEnd,' L 0 0')"/></xsl:variable>
								<xsl:variable name="slice2"><xsl:value-of select="concat($slice2part1,$slice2part2)"></xsl:value-of></xsl:variable>

								<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="#CD5A46" fill-opacity="1.0">
									<xsl:attribute name="d"><xsl:value-of select="$slice2"/></xsl:attribute>
	    						</path>

<!-- 								<line style = "stroke: #000000;stroke-width:0.01;stroke-dasharray:5, 2"  x1="{$textPercX}" y1="{$textPercY}" x2="{$textPercX * 2.20}" y2="{$textPercY * 2.20}"/> -->


								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>

							<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $srPercent"/></xsl:variable>
								<xsl:choose>
									<xsl:when test="$impPercent &gt; 0">
										<xsl:variable name="XimpPercentStart"><xsl:value-of select="math:cos( $double_pi * $cumulativePercent)"/></xsl:variable>
										<xsl:variable name="YimpPercentStart"><xsl:value-of select="math:sin( $double_pi * $cumulativePercent)"/></xsl:variable>
										<xsl:variable name="XimpPercentEnd"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent+$impPercent) )"/></xsl:variable>
										<xsl:variable name="YimpPercentEnd"><xsl:value-of select="math:sin( $double_pi * ($cumulativePercent+$impPercent) )"/></xsl:variable>
										<xsl:variable name="textPercX"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent+($impPercent div 2) )) *0.85"/></xsl:variable>
										<xsl:variable name="textPercY"><xsl:value-of select="math:sin( $double_pi * ($cumulativePercent+($impPercent div 2)) ) *0.85"/></xsl:variable>

										<xsl:variable name="largeArcFlagImpPercent">
											<xsl:choose>
												<xsl:when test="$impPercent &gt; 0.5">
													<xsl:text>1</xsl:text>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text>0</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:variable>

										<xsl:variable name="slice3part1"><xsl:value-of select="concat('M ',$XimpPercentStart,' ',$YimpPercentStart,' ')"/></xsl:variable>
										<xsl:variable name="slice3part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagImpPercent,' ','1 ',$XimpPercentEnd,' ',$YimpPercentEnd,' L 0 0')"/></xsl:variable>
										<xsl:variable name="slice3"><xsl:value-of select="concat($slice3part1,$slice3part2)"></xsl:value-of></xsl:variable>

										<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="#FFAB35" fill-opacity="1.0">
						      				<xsl:attribute name="d"><xsl:value-of select="$slice3"/></xsl:attribute>
						    			</path>

<!-- 										<line style = "stroke: #000000;stroke-width:0.01;stroke-dasharray:5, 2"  x1="{$textPercX}" y1="{$textPercY}" x2="{$textPercX * 2.20}" y2="{$textPercY * 2.20}"/> -->


									</xsl:when>
									<xsl:otherwise>
									</xsl:otherwise>
								</xsl:choose>


								<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $impPercent"/></xsl:variable>
									<xsl:choose>
										<xsl:when test="$ivaPercent &gt; 0">
										<xsl:variable name="XivaPercentStart"><xsl:value-of select="math:cos( $double_pi * $cumulativePercent )"/></xsl:variable>
										<xsl:variable name="YivaPercentStart"><xsl:value-of select="math:sin( $double_pi * $cumulativePercent )"/></xsl:variable>
										<xsl:variable name="XivaPercentEnd"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent+$ivaPercent) )"/></xsl:variable>
										<xsl:variable name="YivaPercentEnd"><xsl:value-of select="math:sin( $double_pi * ($ivaPercent+$cumulativePercent) )"/></xsl:variable>

										<xsl:variable name="textPercX"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent+($ivaPercent div 2) ))*0.85"/></xsl:variable>
										<xsl:variable name="textPercY"><xsl:value-of select="math:sin( $double_pi * ($cumulativePercent+($ivaPercent div 2)) )*0.85"/></xsl:variable>

										<xsl:variable name="largeArcFlagIvaPercent">
											<xsl:choose>
												<xsl:when test="$ivaPercent &gt; 0.5">
													<xsl:text>1</xsl:text>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text>0</xsl:text>
												</xsl:otherwise>
													</xsl:choose>
												</xsl:variable>

												<xsl:variable name="slice4part1"><xsl:value-of select="concat('M ',$XivaPercentStart,' ',$YivaPercentStart,' ')"/></xsl:variable>
												<xsl:variable name="slice4part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagIvaPercent,' ','1 ',$XivaPercentEnd,' ',$YivaPercentEnd,' L 0 0')"/></xsl:variable>
												<xsl:variable name="slice4"><xsl:value-of select="concat($slice4part1,$slice4part2)"></xsl:value-of></xsl:variable>

												<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="#206BA4" fill-opacity="1.0">
						      								<xsl:attribute name="d"><xsl:value-of select="$slice4"/></xsl:attribute>
						    					</path>

<!-- 												<line style = "stroke: #000000;stroke-width:0.01;stroke-dasharray:5, 2"  x1="{$textPercX}" y1="{$textPercY}" x2="{$textPercX * 2.20}" y2="{$textPercY * 2.20}"/> -->


										</xsl:when>
										<xsl:otherwise>
										</xsl:otherwise>
								 </xsl:choose>




								<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $ivaPercent"/></xsl:variable>

									<xsl:choose>
										<xsl:when test="$odivPercent &gt; 0">

											<xsl:variable name="XodivPercentStart"><xsl:value-of select="math:cos( $double_pi * $cumulativePercent)"/></xsl:variable>
											<xsl:variable name="YodivPercentStart"><xsl:value-of select="math:sin( $double_pi * $cumulativePercent)"/></xsl:variable>
											<xsl:variable name="XodivPercentEnd"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent + $odivPercent) )"/></xsl:variable>
											<xsl:variable name="YodivPercentEnd"><xsl:value-of select="math:sin( $double_pi * ($cumulativePercent + $odivPercent) )"/></xsl:variable>

											<xsl:variable name="textPercX"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent+($odivPercent div 2) ))*0.85"/></xsl:variable>
											<xsl:variable name="textPercY"><xsl:value-of select="math:sin( $double_pi * ($cumulativePercent+($odivPercent div 2)) )*0.85"/></xsl:variable>

											<xsl:variable name="largeArcFlagOdivPercent">
												<xsl:choose>
													<xsl:when test="$odivPercent &gt; 0.5">
														<xsl:text>1</xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:text>0</xsl:text>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>

											<xsl:variable name="slice5part1"><xsl:value-of select="concat('M ',$XodivPercentStart,' ',$YodivPercentStart,' ')"/></xsl:variable>
											<xsl:variable name="slice5part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagOdivPercent,' ','1 ',$XodivPercentEnd,' ',$YodivPercentEnd,' L 0 0')"/></xsl:variable>
											<xsl:variable name="slice5"><xsl:value-of select="concat($slice5part1,$slice5part2)"></xsl:value-of></xsl:variable>

											<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="#EBF4FA" fill-opacity="1.0">
						      					<xsl:attribute name="d"><xsl:value-of select="$slice5"/></xsl:attribute>
						    				</path>

<!-- 											<line style = "stroke: #000000;stroke-width:0.01;stroke-dasharray:5, 2"  x1="{$textPercX}" y1="{$textPercY}" x2="{$textPercX * 2.20}" y2="{$textPercY * 2.20}"/> -->



											</xsl:when>
											<xsl:otherwise>
											</xsl:otherwise>
									</xsl:choose>





									<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $odivPercent"/></xsl:variable>

									<xsl:choose>
										<xsl:when test="$raiPercent &gt; 0">
											<xsl:variable name="XraiPercentStart"><xsl:value-of select="math:cos($double_pi * $cumulativePercent)"/></xsl:variable>
											<xsl:variable name="YraiPercentStart"><xsl:value-of select="math:sin($double_pi * $cumulativePercent)"/></xsl:variable>
											<xsl:variable name="XraiPercentEnd"><xsl:value-of select="math:cos($double_pi * ($cumulativePercent + $raiPercent) )"/></xsl:variable>
											<xsl:variable name="YraiPercentEnd"><xsl:value-of select="math:sin($double_pi * ($cumulativePercent + $raiPercent) )"/></xsl:variable>

											<xsl:variable name="textPercX"><xsl:value-of select="math:cos($double_pi * ($cumulativePercent+($raiPercent div 2) ))*0.85"/></xsl:variable>
											<xsl:variable name="textPercY"><xsl:value-of select="math:sin($double_pi * ($cumulativePercent+($raiPercent div 2)) )*0.85"/></xsl:variable>

											<xsl:variable name="largeArcFlagRaiPercent">
												<xsl:choose>
													<xsl:when test="$raiPercent &gt; 0.5">
														<xsl:text>1</xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:text>0</xsl:text>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>

											<xsl:variable name="slice6part1"><xsl:value-of select="concat('M ',$XraiPercentStart,' ',$YraiPercentStart,' ')"/></xsl:variable>
											<xsl:variable name="slice6part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagRaiPercent,' ','1 ',$XraiPercentEnd,' ',$YraiPercentEnd,' L 0 0')"/></xsl:variable>
											<xsl:variable name="slice6"><xsl:value-of select="concat($slice6part1,$slice6part2)"></xsl:value-of></xsl:variable>

											<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="#BBD9EE" fill-opacity="1.0">
				      							<xsl:attribute name="d"><xsl:value-of select="$slice6"/></xsl:attribute>
				    						</path>


<!-- 											<line style = "stroke: #000000;stroke-width:0.01;stroke-dasharray:5, 2"  x1="{$textPercX}" y1="{$textPercY}" x2="{$textPercX * 2.20}" y2="{$textPercY * 2.20}"/> -->


								        </xsl:when>
								        <xsl:otherwise>
									    </xsl:otherwise>
									</xsl:choose>





	    						<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $raiPercent"/></xsl:variable>

								<xsl:choose>
								<xsl:when test="$bsPercent &gt; 0">
									<xsl:variable name="XbsPercentStart"><xsl:value-of select="math:cos( $double_pi * $cumulativePercent)"/></xsl:variable>
									<xsl:variable name="YbsPercentStart"><xsl:value-of select="math:sin( $double_pi * $cumulativePercent)"/></xsl:variable>
									<xsl:variable name="XbsPercentEnd"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent + $bsPercent) )"/></xsl:variable>
									<xsl:variable name="YbsPercentEnd"><xsl:value-of select="math:sin( $double_pi * ($cumulativePercent + $bsPercent) )"/></xsl:variable>

									<xsl:variable name="textPercX"><xsl:value-of select="math:cos( $double_pi * ($cumulativePercent+($raiPercent div 2) ))*0.85"/></xsl:variable>
									<xsl:variable name="textPercY"><xsl:value-of select="math:sin( $double_pi * ($cumulativePercent+($raiPercent div 2)) )*0.85"/></xsl:variable>

									<xsl:variable name="largeArcFlagBsPercent">
										<xsl:choose>
											<xsl:when test="$bsPercent &gt; 0.5">
												<xsl:text>1</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>0</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>

									<xsl:variable name="slice7part1"><xsl:value-of select="concat('M ',$XbsPercentStart,' ',$YbsPercentStart,' ')"/></xsl:variable>
									<xsl:variable name="slice7part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagBsPercent,' ','1 ',$XbsPercentEnd,' ',$YbsPercentEnd,' L 0 0')"/></xsl:variable>
									<xsl:variable name="slice7"><xsl:value-of select="concat($slice7part1,$slice7part2)"></xsl:value-of></xsl:variable>

									<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="#54A4DE" fill-opacity="1.0">
										<xsl:attribute name="d"><xsl:value-of select="$slice7"/></xsl:attribute>
		    						</path>
		    						
<!-- 		    						<line style = "stroke: #000000;stroke-width:0.01;stroke-dasharray:5, 2"  x1="{$textPercX}" y1="{$textPercY}" x2="{$textPercX * 2.20}" y2="{$textPercY * 2.20}"/> -->
		    						
									</xsl:when>
								</xsl:choose>

		 		    			
		 		    			
		 		    			<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $bsPercent"/></xsl:variable>

								</xsl:otherwise>
				</xsl:choose>

				<circle cx="0" cy="0" r="0.7" stroke="black" stroke-width="0.01" fill="white"/>
		  </svg>

		</fo:instream-foreign-object>
		

	</fo:block>
							
						<!-- <xsl:if test="./child::CONTRATTO_ENERGIA"> -->
		<fo:block-container position="absolute"
							

							height="55mm"
							font-family="universcbold"
							font-size="28pt"
							text-align="center"

							>
							<xsl:variable name="importo">
								<xsl:value-of select="$importo_no_virgola div 100"/>
							</xsl:variable>

							<xsl:variable name="importo_length">
								<xsl:value-of select="string-length(string(./child::IMPORTO_DOCUMENTO))"/>
							</xsl:variable>

							<xsl:if test="$importo_length &lt; 6">
								<xsl:attribute name="font-size">24pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo_length &lt; 8 and $importo_length &gt; 6">
								<xsl:attribute name="font-size">21pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo_length &lt; 12 and $importo_length &gt; 8">
								<xsl:attribute name="font-size">18pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo_length &lt; 15 and $importo_length &gt; 12">
								<xsl:attribute name="font-size">15pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo_length &lt; 18 and $importo_length &gt; 15">
								<xsl:attribute name="font-size">12pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo_length &lt; 21 and $importo_length &gt; 18">
								<xsl:attribute name="font-size">9pt</xsl:attribute>
							</xsl:if>

<!-- TESTO INTERNO -->
							<!-- <xsl:if test="./child::CHARTS/child::ROWSET/child::SEZ='Bonus sociale'"> -->
							<!-- contare quantità fissa /2 per ogni linea diversa da zero -->
<!-- 							<xsl:choose> -->
<!-- 								<xsl:when test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE='0,00') and (./child::CONTRATTO_ENERGIA)"> -->
<!-- 									<xsl:attribute name="top">15mm</xsl:attribute> -->
<!-- 								</xsl:when> -->
<!-- 								<xsl:otherwise> -->
<!-- 									<xsl:attribute name="top">18mm</xsl:attribute> -->
<!-- 								</xsl:otherwise> -->
<!-- 							</xsl:choose> -->
							<fo:block font-family="universc" font-size="10pt" >
								Totale
							</fo:block>
							<fo:block >
								<xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> 
							</fo:block>
							<fo:block font-family="universc" font-size="10pt" >
								Euro
							</fo:block>

						</fo:block-container>		
						
						<!-- </xsl:if> -->						
</xsl:template>




<!--
***********************************
**  Grafico Consumi per fascia   **
***********************************
-->
<xsl:template name="GRAFICO_CONSUMI_FASCIA">
		<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
		<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>STORICO CONSUMI PER FASCIA</fo:block>
		<fo:block text-align="center" display-align="center">

			<xsl:variable name="bar_counter">
				<xsl:value-of select="count(./CONTRATTO_ENERGIA/CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO)"/>
			</xsl:variable>


			<fo:block text-align="left">
				Il seguente grafico riporta lo storico dei consumi di energia elettrica negli ultimi 12 mesi di fornitura Energit, da cui dipendono i relativi costi del servizio.
			</fo:block>

			<fo:instream-foreign-object>
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="190" height="150"  >
					<g transform="translate(-10,120)  scale(1.0,-1.0)">


						<!-- vars -->
						<xsl:variable name="picture_width" select="200"/> 		 <!-- ampiezza immagine -->
						<xsl:variable name="picture_height" select="120"/> 		 <!-- altezza immagine -->
        				<xsl:variable name="bar_distance" select="2"/> 			 <!-- distanza tra le barre del grafico -->
        				<xsl:variable name="width" select="10"/> 				 <!-- larghezza delle barre del grafico -->
           				<xsl:variable name="graph_base" select="22"/> 			 <!-- ordinata da cui parte il grafico -->
           				<xsl:variable name="picture_base" select="0"/> 			 <!-- base dell'immagine -->
        				<xsl:variable name="char_size_adjustment" select="1.5"/> <!-- fattore di correzione orizzontale nel posizionamento dei testi -->
        				<xsl:variable name="top_value_y_offset" select="5"/> 	 <!-- distanza del dato riportato in cima alle barre da ciascuna barra -->
						<xsl:variable name="show_bar_values" select="0"/>		 <!-- mostra i valori nelle barre interne (=1 si, !=1 no) -->
						<xsl:variable name="base_value_x_offset" select="15"/>   <!-- scostamento lungo le ascisse -->
						<xsl:variable name="bar_top" select="65"/>				 <!-- altezza massima barra -->


						<!-- fattore di scala y: determinazione del range di partenza; il range di partenza sarà compreso tra [ 0, max({f1}) + max({f2}) + max({f3})] -->
						<xsl:variable name="maxF1">
						  <xsl:for-each select="./CONTRATTO_ENERGIA/CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO/FASCIA[@FASCIA='F1']/ENERGIA_ATTIVA">
						    <xsl:sort select="." data-type="number" order="descending"/>
						    <xsl:if test="position() = 1"><xsl:value-of select="."/></xsl:if>
						  </xsl:for-each>
						</xsl:variable>

						<xsl:variable name="maxF2">
						  <xsl:for-each select="./CONTRATTO_ENERGIA/CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO/FASCIA[@FASCIA='F2']/ENERGIA_ATTIVA">
						    <xsl:sort select="." data-type="number" order="descending"/>
						    <xsl:if test="position() = 1"><xsl:value-of select="."/></xsl:if>
						  </xsl:for-each>
						</xsl:variable>

						<xsl:variable name="maxF3">
						  <xsl:for-each select="./CONTRATTO_ENERGIA/CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO/FASCIA[@FASCIA='F3']/ENERGIA_ATTIVA">
						    <xsl:sort select="." data-type="number" order="descending"/>
						    <xsl:if test="position() = 1"><xsl:value-of select="."/></xsl:if>
						  </xsl:for-each>
						</xsl:variable>

						<xsl:variable name="max">
							<xsl:value-of select="$maxF1+$maxF2+$maxF3"/>
						</xsl:variable>

						<!-- [A,B] => [a,b] ; applico formula: nuovoRange(valoreCorrente)= (valoreCorrente - A ) * ( (b-a)/(B-A) +a ) -->
						<!-- io voglio [0,maxF1+maxF2+maxF3] => [0,65] ; da cui: nuovoRange(valoreCorrente)= (valoreCorrente-0)*((65-0)/(maxF1+maxF2+maxF3-0)) + 0 -->
						<!-- se A e a sono zero posso chiamare y_scale_factor=(b)/(B) , ovvero  nuovoRange(valoreCorrente)=valoreCorrente*y_scale_factor -->
<!--         				<xsl:variable name="y_scale_factor"> -->
<!--         					<xsl:value-of select=" ( ($bar_top) div ($max) div 100)*100 "/> -->
<!--         				</xsl:variable>  -->
        				<xsl:variable name="y_scale_factor">

        					<xsl:choose>
								<xsl:when test="boolean($max != 0) ">
									<xsl:value-of select=" ( ($bar_top) div ($max) div 100)*100 "/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$bar_top"/>
								</xsl:otherwise>
        					</xsl:choose>

        				</xsl:variable>




        				<!-- scala -->
        				<xsl:variable name="notch1">
        					<xsl:value-of select="round(($max div 4) div 10) *10"/>
        				</xsl:variable>
        				<line style = "stroke: #A9A9A9;stroke-width:0.1;stroke-dasharray:5, 2"  x1="25" y1="{$graph_base +  (($notch1) * $y_scale_factor)}" x2="200" y2="{$graph_base + (($notch1) * $y_scale_factor)}"/>
        				<text fill="#000000" x="10" y="{-1* $graph_base - round( $notch1 * $y_scale_factor)}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$notch1"/></text>


        				<xsl:variable name="notch2">
        					<xsl:value-of select="round(($max div 2) div 10) *10"/>
        				</xsl:variable>
        				<line style = "stroke: #A9A9A9;stroke-width:0.1;stroke-dasharray:5, 2"  x1="20" y1="{$graph_base + (($notch2) * $y_scale_factor)}" x2="200" y2="{$graph_base + (($notch2) * $y_scale_factor)}"/>
        				<text fill="#000000" x="10" y="{-1* $graph_base - round( $notch2 * $y_scale_factor)}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$notch2"/></text>


        				<xsl:variable name="notch3">
        					<xsl:value-of select="round( 3*($max div 4) div 10 ) * 10"/>
        				</xsl:variable>
        				<line style = "stroke: #A9A9A9;stroke-width:0.1;stroke-dasharray:5, 2" x1="25" y1="{$graph_base + (($notch3) * $y_scale_factor)}" x2="200" y2="{$graph_base + (($notch3) * $y_scale_factor)}"/>
        				<text fill="#000000" x="10" y="{-1* $graph_base - round( $notch3 * $y_scale_factor)}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$notch3"/></text>


        				<xsl:variable name="notch4">
        					<xsl:value-of select="$max"/>
        				</xsl:variable>
        				<line style = "stroke: #A9A9A9;stroke-width:0.1;stroke-dasharray:5, 2"  x1="20" y1="{$graph_base + ($notch4 * $y_scale_factor)}" x2="200" y2="{$graph_base + ($notch4 * $y_scale_factor)}"/>
        				<text fill="#000000" x="10" y="{-1* $graph_base - round( $notch4 * $y_scale_factor)}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$notch4"/></text>




        				<!-- elementi da scorrere -->
        				<xsl:for-each select="./CONTRATTO_ENERGIA/CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO">

							<xsl:sort select="position()" data-type="number" order="descending"/>

        					<xsl:choose>
								<xsl:when test="position()>1 and boolean($bar_counter &gt; 12)">

									<xsl:variable name="pos" select="$base_value_x_offset+( (position() -1) * 10) + 2 "/>
		           					<xsl:variable name="text_pos" select="$pos+6"/>
		           					<xsl:variable name="values_text_pos" select="$pos+2"/>

									<!-- rect vars -->
									<xsl:variable name="f1_val">
										<xsl:value-of select="./FASCIA[@FASCIA='F1']/ENERGIA_ATTIVA"/>
									</xsl:variable>

									<xsl:variable name="f2_val">
										<xsl:value-of select="./FASCIA[@FASCIA='F2']/ENERGIA_ATTIVA"/>
									</xsl:variable>

									<xsl:variable name="f3_val">
										<xsl:value-of select="./FASCIA[@FASCIA='F3']/ENERGIA_ATTIVA"/>
									</xsl:variable>

									<xsl:variable name="top_value">
										<xsl:value-of select="( $f1_val +$f2_val +$f3_val )"/>
									</xsl:variable>


									<xsl:variable name="f1">
										<xsl:value-of select="($f1_val * ($y_scale_factor))"/>
									</xsl:variable>

									<xsl:variable name="f2">
										<xsl:value-of select="($f2_val * ($y_scale_factor))"/>
									</xsl:variable>

									<xsl:variable name="f3">
										<xsl:value-of select="($f3_val * ($y_scale_factor))"/>
									</xsl:variable>


									<xsl:variable name="f1_base">
										<xsl:value-of select="$graph_base"/>
									</xsl:variable>

									<xsl:variable name="f2_base">
										<xsl:value-of select="$f1_base+$f1"/>
									</xsl:variable>

									<xsl:variable name="f3_base">
										<xsl:value-of select="$f2_base+$f2"/>
									</xsl:variable>


		        					<!-- text vars -->
		        					<xsl:variable name="current_month">
		        						<xsl:value-of select="concat(substring(substring-before(concat(./@DESCRIZIONE_MESE_RIFERIMENTO,' '),' '),1,3),concat('-',substring(substring-after(./@DESCRIZIONE_MESE_RIFERIMENTO,' '),1,4)))"/>
		        					</xsl:variable>


									<xsl:variable name="f1_middle">
										<xsl:value-of select="-1* (($f1_base+$f1 div 2) - $char_size_adjustment) "/>
									</xsl:variable>

									<xsl:variable name="f2_middle">
										<xsl:value-of select="-1* (($f2_base+$f2 div 2) - $char_size_adjustment) "/>
									</xsl:variable>

									<xsl:variable name="f3_middle">
										<xsl:value-of select="-1* (($f3_base+$f3 div 2) - $char_size_adjustment) "/>
									</xsl:variable>

									<xsl:variable name="top_value_y_pos">
										<xsl:value-of select="-1* (($f3_base+$f3+$top_value_y_offset) - $char_size_adjustment) "/>
									</xsl:variable>

		        					<!-- draw F1,F2,F3 rectangles -->
		 							<rect x="{$pos + ($bar_distance * position())}" y="{$f1_base}" width="{$width}" height="{$f1}"  style="fill:rgb(255,166,0);stroke:#000000;stroke-width:0.25"/>
		 							<rect x="{$pos + ($bar_distance * position())}" y="{$f2_base}" width="{$width}" height="{$f2}"  style="fill:rgb(255,255,255);stroke:#000000;stroke-width:0.25"/>
		 							<rect x="{$pos + ($bar_distance * position())}" y="{$f3_base}" width="{$width}" height="{$f3}"  style="fill:rgb(0,117,255);stroke:#000000;stroke-width:0.25"/>

		 							<!-- bar text -->
		 							<xsl:if test="show_bar_values=1">
										<text fill="#FFFFFF" x="{$values_text_pos + ($bar_distance * position())}" y="{$f1_middle}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$f1_val"/></text>
										<text fill="#000000" x="{$values_text_pos + ($bar_distance * position())}" y="{$f2_middle}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$f2_val"/></text>
										<text fill="#FFFFFF" x="{$values_text_pos + ($bar_distance * position())}" y="{$f3_middle}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$f3_val"/></text>
									</xsl:if>
									<text fill="#000000" x="{$values_text_pos + ($bar_distance * position())}" y="{$top_value_y_pos}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$top_value"/></text>

									<!-- text (month) -->
<!-- 									<text fill="#000000" x="{ $pos + $bar_distance * position() }" y="{$pos + $bar_distance * position()}" font-size="4" font-family="Monospace" transform="translate(2,{$graph_base - 17	}) scale(0.7,-1) rotate(315)"><xsl:value-of select="$current_month"/></text> -->
									<text fill="#000000" x="{$graph_base}" y="{ $pos + $bar_distance * position() }" font-size="4" font-family="universcbold" transform="translate(7,-21) scale (1,-1) rotate(270)"><xsl:value-of select="$current_month"/></text>

								</xsl:when>
								<xsl:when test="$bar_counter &lt; 13">

									<xsl:variable name="pos" select="$base_value_x_offset+( (position()) * 10) + 2 "/>
		           					<xsl:variable name="text_pos" select="$pos+6"/>
		           					<xsl:variable name="values_text_pos" select="$pos+2"/>

									<!-- rect vars -->
									<xsl:variable name="f1_val">
										<xsl:value-of select="./FASCIA[@FASCIA='F1']/ENERGIA_ATTIVA"/>
									</xsl:variable>

									<xsl:variable name="f2_val">
										<xsl:value-of select="./FASCIA[@FASCIA='F2']/ENERGIA_ATTIVA"/>
									</xsl:variable>

									<xsl:variable name="f3_val">
										<xsl:value-of select="./FASCIA[@FASCIA='F3']/ENERGIA_ATTIVA"/>
									</xsl:variable>

									<xsl:variable name="top_value">
										<xsl:value-of select="( $f1_val +$f2_val +$f3_val )"/>
									</xsl:variable>


									<xsl:variable name="f1">
										<xsl:value-of select="($f1_val * ($y_scale_factor))"/>
									</xsl:variable>

									<xsl:variable name="f2">
										<xsl:value-of select="($f2_val * ($y_scale_factor))"/>
									</xsl:variable>

									<xsl:variable name="f3">
										<xsl:value-of select="($f3_val * ($y_scale_factor))"/>
									</xsl:variable>


									<xsl:variable name="f1_base">
										<xsl:value-of select="$graph_base"/>
									</xsl:variable>

									<xsl:variable name="f2_base">
										<xsl:value-of select="$f1_base+$f1"/>
									</xsl:variable>

									<xsl:variable name="f3_base">
										<xsl:value-of select="$f2_base+$f2"/>
									</xsl:variable>


		        					<!-- text vars -->
		        					<xsl:variable name="current_month">
		        						<xsl:value-of select="concat(substring(substring-before(concat(./@DESCRIZIONE_MESE_RIFERIMENTO,' '),' '),1,3),concat('-',substring(substring-after(./@DESCRIZIONE_MESE_RIFERIMENTO,' '),1,4)))"/>
		        					</xsl:variable>


									<xsl:variable name="f1_middle">
										<xsl:value-of select="-1* (($f1_base+$f1 div 2) - $char_size_adjustment) "/>
									</xsl:variable>

									<xsl:variable name="f2_middle">
										<xsl:value-of select="-1* (($f2_base+$f2 div 2) - $char_size_adjustment) "/>
									</xsl:variable>

									<xsl:variable name="f3_middle">
										<xsl:value-of select="-1* (($f3_base+$f3 div 2) - $char_size_adjustment) "/>
									</xsl:variable>

									<xsl:variable name="top_value_y_pos">
										<xsl:value-of select="-1* (($f3_base+$f3+$top_value_y_offset) - $char_size_adjustment) "/>
									</xsl:variable>

		        					<!-- draw F1,F2,F3 rectangles -->
		 							<rect x="{$pos + ($bar_distance * position())}" y="{$f1_base}" width="{$width}" height="{$f1}"  style="fill:rgb(255,166,0);stroke:#000000;stroke-width:0.25"/>
		 							<rect x="{$pos + ($bar_distance * position())}" y="{$f2_base}" width="{$width}" height="{$f2}"  style="fill:rgb(255,255,255);stroke:#000000;stroke-width:0.25"/>
		 							<rect x="{$pos + ($bar_distance * position())}" y="{$f3_base}" width="{$width}" height="{$f3}"  style="fill:rgb(0,117,255);stroke:#000000;stroke-width:0.25"/>

		 							<!-- bar text -->
		 							<xsl:if test="show_bar_values=1">
										<text fill="#FFFFFF" x="{$values_text_pos + ($bar_distance * position())}" y="{$f1_middle}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$f1_val"/></text>
										<text fill="#000000" x="{$values_text_pos + ($bar_distance * position())}" y="{$f2_middle}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$f2_val"/></text>
										<text fill="#FFFFFF" x="{$values_text_pos + ($bar_distance * position())}" y="{$f3_middle}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$f3_val"/></text>
									</xsl:if>
									<text fill="#000000" x="{$values_text_pos + ($bar_distance * position())}" y="{$top_value_y_pos}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)"><xsl:value-of select="$top_value"/></text>

									<!-- text (month) -->
<!-- 									<text fill="#000000" x="{ $pos + $bar_distance * position() }" y="{$pos + $bar_distance * position()}" font-size="4" font-family="Monospace" transform="translate(2,{$graph_base - 17	}) scale(0.7,-1) rotate(315)"><xsl:value-of select="$current_month"/></text> -->
									<text fill="#000000" x="{$graph_base -2 }" y="{ $pos + $bar_distance * position() }" font-size="4" font-family="universcbold" transform="translate(7,-21) scale (1,-1) rotate(270)"><xsl:value-of select="$current_month"/></text>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
        				</xsl:for-each>

        				<!-- LEGENDA VERTICALE -->

<!--         				<rect x="175" y="{44+$graph_base}" width="5" height="5" style = "fill:rgb( 255, 166,   0 );  stroke:#000000;  stroke-width:0.25"/>  -->
<!-- 						<rect x="175" y="{33+$graph_base}" width="5" height="5" style = "fill:rgb( 255, 255, 255 );  stroke:#000000;  stroke-width:0.25"/> -->
<!-- 						<rect x="175" y="{22+$graph_base}" width="5" height="5" style = "fill:rgb(   0, 117, 255 );  stroke:#000000;  stroke-width:0.25"/> -->

<!-- 						<text fill="#000000" x="182" y="{-44-$graph_base}" font-size="4" font-family="Arial" transform="translate(0,0) scale(1,-1)">F1</text> -->
<!-- 						<text fill="#000000" x="182" y="{-33-$graph_base}" font-size="4" font-family="Arial" transform="translate(0,0) scale(1,-1)">F2</text> -->
<!-- 						<text fill="#000000" x="182" y="{-22-$graph_base}" font-size="4" font-family="Arial" transform="translate(0,0) scale(1,-1)">F3</text> -->





        				<!-- LEGENDA ORIZZONTALE -->

						<rect x="{($picture_width div 5) }" y="100" width="5" height="5" style = "fill:rgb(255,166,0)  ;  stroke:#000000;  stroke-width:0.25"/>
						<rect x="{2*($picture_width div 5) }" y="100" width="5" height="5" style = "fill:rgb(255,255,255);  stroke:#000000;  stroke-width:0.25"/>
						<rect x="{3*($picture_width div 5) }" y="100" width="5" height="5" style = "fill:rgb(0,117,255)  ;  stroke:#000000;  stroke-width:0.25"/>

						<text fill="#000000" x="{1*($picture_width div 5)+6 }" y="-100" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)">F1</text>
						<text fill="#000000" x="{2*($picture_width div 5)+6 }" y="-100" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)">F2</text>
						<text fill="#000000" x="{3*($picture_width div 5)+6 }" y="-100" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)">F3</text>



						<!-- horizontal axis -->
        				<line style = "stroke: #000000;stroke-width:0.5"  x1="0" y1="{$graph_base}" x2="200" y2="{$graph_base}"/>
						<!-- vertical axis -->
        				<line style = "stroke: #000000;stroke-width:0.5"  x1="25" y1="{$graph_base}" x2="25" y2="100"/>
        				<!-- axis arrow -->
        				<line style = "stroke: #000000;stroke-width:0.5"  x1="24" y1="97" x2="25" y2="100"/>
        				<line style = "stroke: #000000;stroke-width:0.5"  x1="26" y1="97" x2="25" y2="100"/>
        				<!-- unità di misura -->
						<text fill="#000000" x="10" y="{-75-$graph_base}" font-size="4" font-family="universcbold" transform="translate(0,0) scale(1,-1)">(kWh)</text>



					</g>
				</svg>
			</fo:instream-foreign-object>


		</fo:block>
</xsl:template>




<!--
***********************************
**  Indirizzo spedizione fattura **
***********************************
-->
<xsl:template name="SPEDIZIONE_FATTURA">
	<fo:block xsl:use-attribute-sets="blocco_indirizzo_spedizione" color="black">
		<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INTESTATARIO" /></fo:block>

		<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INDIRIZZO" /></fo:block>

		<fo:block>
			<xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CITTA" />
			<xsl:if test="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA">
				<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA" /></fo:inline>
			</xsl:if>
		</fo:block>

		<xsl:if test="not(./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::NAZIONE='IT')">
			<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::STATO" /></fo:block>
		</xsl:if>
	</fo:block>
</xsl:template>





<!--
*********************************
**  Riepilogo importi fattura  **
*********************************
-->
<xsl:template name="DETTAGLIO_IVA">

	<!-- DETTAGLIO IVA EX -->
	<fo:table space-before="10mm" start-indent="3.5mm" table-layout="fixed" width="100%" display-align="center">
		<fo:table-column column-width="21mm"/>
		<fo:table-column column-width="21mm"/>
		<fo:table-column column-width="21mm"/>
		<fo:table-column column-width="21mm"/>
		<fo:table-column column-width="21mm"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
		<fo:table-row font-family="universcbold" border-bottom="0.5 solid black" text-align="start">
			<fo:table-cell number-columns-spanned="5">
				<fo:block><xsl:attribute name="color">#0065ae</xsl:attribute>DETTAGLIO IVA</fo:block>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row font-family="universcbold" display-align="center">
			<fo:table-cell text-align="start" padding-left="2mm">
				<fo:block>Imponibile</fo:block>
			</fo:table-cell>

			<fo:table-cell>
				<fo:block>Aliquota</fo:block>
			</fo:table-cell>

			<fo:table-cell>
				<fo:block>Codice IVA</fo:block>
			</fo:table-cell>

			<fo:table-cell>
				<fo:block>Importo IVA</fo:block>
			</fo:table-cell>

			<fo:table-cell text-align="end" padding-right="2mm">
				<fo:block>Totale</fo:block>
			</fo:table-cell>
		</fo:table-row>


		<xsl:for-each select="./child::DETTAGLIO_FATTURA">
			<fo:table-row display-align="center">

				<fo:table-cell text-align="start" padding-left="2mm">
					<fo:block><xsl:value-of select="./child::IMPONIBILE" /></fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ALIQUOTA_IVA" />%</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::CODICE_IVA" /></fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::IMPORTO_IVA" /></fo:block>
				</fo:table-cell>

				<fo:table-cell text-align="end" padding-right="2mm">
					<fo:block><xsl:value-of select="./child::TOTALE" /></fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:for-each>

		<fo:table-row  border-bottom="0.5 solid black">
			<fo:table-cell number-columns-spanned="5">
				<fo:block/>
			</fo:table-cell>
		</fo:table-row>

		<xsl:for-each select="DESCRIZIONI_CODICI_IVA">
			<fo:table-row font-family="universc" display-align="center" font-size="6pt">
				<fo:table-cell number-columns-spanned="2" text-align="start">
					<fo:block>
						Codice IVA "<xsl:value-of select="CODICE_IVA" />"
					</fo:block>
				</fo:table-cell>
				<fo:table-cell number-columns-spanned="3" text-align="start">
					<fo:block>
						<xsl:value-of select="DESCRIZIONE_CODICE_IVA" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:for-each>
	</fo:table-body>
	</fo:table>

</xsl:template>

<xsl:template name="test">
        <xsl:for-each select="//SEZIONE[@TIPOLOGIA='IMPOSTE']/PERIODO_RIFERIMENTO/PRODOTTO/CORRISPETTIVO_UNITARIO[not(preceding::SEZIONE[@TIPOLOGIA='IMPOSTE']/PERIODO_RIFERIMENTO/PRODOTTO/CORRISPETTIVO_UNITARIO/. = .)]">
       		<fo:block><xsl:value-of select="number(translate(translate(../PREZZO_TOTALE,'.',''),',','.'))"/></fo:block>
        </xsl:for-each>

</xsl:template>

<xsl:template name="SINTESI_FISCALE_MONOSITO2">

	<!-- Elementi radice. Non aggiungere "/" finale -->
 	<xsl:variable name="contratto_energia" select="./child::CONTRATTO_ENERGIA"/>
 	<xsl:variable name="sintesi_energia" select="$contratto_energia/child::SINTESI_ENERGIA"/>
 	<xsl:variable name="deposito_cauzionale" select="./child::CONTRATTO_DEPOSITO_CAUZIONALE"/>
 	<xsl:variable name="sezione_imposte" select="$contratto_energia/child::SEZIONE[@TIPOLOGIA='IMPOSTE']"/>
 	<xsl:variable name="sezione_canone_rai" select="$contratto_energia/child::SEZIONE[@TIPOLOGIA='CANONE_RAI']"/>
 	<xsl:variable name="riepilogo_fattura" select="./child::RIEPILOGO_FATTURA"/>



	<!-- SERVIZI VENDITA -->
	<xsl:variable name="sv">
		<xsl:if test="$sintesi_energia/child::TOTALE_SERVIZI_VENDITA='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::TOTALE_SERVIZI_VENDITA='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_SERVIZI_VENDITA,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>

	<!-- SERVIZI RETE -->
	<xsl:variable name="sr">
		<xsl:if test="$sintesi_energia/TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_SERVIZI_RETE_SENZA_BONUS,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- IMPOSTE -->
	<xsl:variable name="imp">
		<xsl:if test="$sintesi_energia/child::IMPOSTE='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::IMPOSTE='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_IMPOSTE,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- IVA FORNITURA -->
	<xsl:variable name="iva_fornitura">
		<xsl:if test="$sintesi_energia/child::IVA='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::IVA='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::IVA,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- IVA ALTRE PARTITE -->
	<xsl:variable name="iva_odiv">
		<xsl:if test="$sintesi_energia/child::IVA_SU_ONERI_DIVERSI='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::IVA_SU_ONERI_DIVERSI='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::IVA_SU_ONERI_DIVERSI,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>

	<!-- IVA DEPOSITO CAUZIONALE -->
	<xsl:variable name="iva_dc">
		<xsl:if test="../child::CONTRATTO_DEPOSITO_CAUZIONALE">
			<xsl:if test="./child::SINTESI_ENERGIA/child::TOTALE_IVA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::SINTESI_ENERGIA/child::TOTALE_IVA='0,00')">
				<xsl:value-of select="translate(translate(./child::SINTESI_ENERGIA/child::TOTALE_IVA,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE)">
			0
		</xsl:if>
	</xsl:variable>

	<!-- IVA TOTALE -->
	<xsl:variable name="iva">
		<xsl:value-of select="$iva_fornitura+$iva_odiv+$iva_dc"/>
	</xsl:variable>

	<!--  ALTRE PARTITE SENZA IVA -->
	<xsl:variable name="odiv_fornitura">
			<xsl:if test="$sintesi_energia/child::ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not($sintesi_energia/child::ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate($sintesi_energia/child::ONERI_DIVERSI,'.',''),',','')"/>
		</xsl:if>

	</xsl:variable>

	<!--  IMPONIBILE DEPOSITO CAUZIONALE -->
	<xsl:variable name="imponibile_dc">
		<xsl:if test="../child::CONTRATTO_DEPOSITO_CAUZIONALE">
			<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE)">
			0
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="odiv">
		<xsl:value-of select="$odiv_fornitura+$imponibile_dc"/>
	</xsl:variable>

	<!-- BONUS SOCIALE -->
	<xsl:variable name="bs">
			<xsl:if test="$sintesi_energia/child::BONUS_SOCIALE='0,00'">0</xsl:if>
			<xsl:if test="not($sintesi_energia/child::BONUS_SOCIALE='0,00')">
				<xsl:value-of select="translate(translate($sintesi_energia/child::BONUS_SOCIALE,'.',''),',','')"/>
			</xsl:if>
	</xsl:variable>


	<!-- CANONE RAI -->
	<xsl:variable name="t_rai">
			<xsl:if test="$sintesi_energia/child::TOTALE_CANONE_RAI='0,00'">0</xsl:if>
			<xsl:if test="not($sintesi_energia/child::TOTALE_CANONE_RAI='0,00')">
				<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_CANONE_RAI,'.',''),',','')"/>
			</xsl:if>
	</xsl:variable>

	<!-- TOTALE IMPONIBILE FATTURA STANDARD -->
	<xsl:variable name="totale_imponibile_fattura_standard">
		<xsl:if test="$sintesi_energia/child::TOTALE_IMPONIBILE='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::TOTALE_IMPONIBILE='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_IMPONIBILE,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- TOTALE BONUS DEI PRODOTTI -->
	<xsl:variable name="prodotto_bonus_euro">
			<xsl:if test="$contratto_energia/BONUS_PRODOTTO='0,00'">0</xsl:if>
			<xsl:if test="not($contratto_energia/BONUS_PRODOTTO='0,00')">
				<xsl:value-of select="translate(translate($contratto_energia/BONUS_PRODOTTO,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not($contratto_energia/child::BONUS_PRODOTTO)">
				0
			</xsl:if>
	</xsl:variable>

	<xsl:variable name="importo_no_virgola">
		<xsl:value-of select="translate(translate(./child::IMPORTO_DOCUMENTO,'.',''),',','')"/>
	</xsl:variable>

	<xsl:variable name="totale_bolletta"> <!-- no canone rai! -->
		<xsl:value-of select="$sv + $sr + $imp + $iva + $odiv + $bs"/>
	</xsl:variable>



	<xsl:variable name="totale_fattura">
		<xsl:value-of select="$sv + $sr + $imp + $iva + $odiv + $bs + $t_rai"/>
	</xsl:variable>

	<xsl:variable name="contratto_corrente">
			<xsl:if test="$contratto_energia/child::CONTRACT_NO">
				<xsl:value-of select="./child::CONTRACT_NO"/>
			</xsl:if>
			<xsl:if test="not($contratto_energia/child::CONTRACT_NO)">
				NO-CONTR
			</xsl:if>
	</xsl:variable>


		<fo:table start-indent="3.5mm" table-layout="fixed" width="92%" display-align="center" padding-top="2mm">

		<fo:table-column column-width="proportional-column-width(77)"/>
		<fo:table-column column-width="proportional-column-width(23)"/>

		<fo:table-body end-indent="0pt" start-indent="0pt">

		<fo:table-row font-family="universcbold" border-bottom="0.5 solid black" text-align="start" font-size="8.5pt">
			<fo:table-cell number-columns-spanned="2">
				<fo:block><xsl:attribute name="color">#0065ae</xsl:attribute>SINTESI FISCALE</fo:block>
			</fo:table-cell>
		</fo:table-row>


			<xsl:if test="$imp!=0">
				<fo:table-row font-size="7pt">
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="3mm">ACCISE</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm">
<!-- 						<fo:block><xsl:value-of select="translate(format-number( $imp div 100,'##########.##'),'.',',')" /></fo:block> -->
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>

			   <xsl:for-each select="$sezione_imposte/PERIODO_RIFERIMENTO">
					<fo:table-row font-size="6pt">
						<fo:table-cell padding-left="10mm">
							<fo:block>

								 <xsl:value-of select="concat( substring( ./child::PRODOTTO/DESCRIZIONE, 1,1) , translate( substring( ./child::PRODOTTO/DESCRIZIONE, 2, string-length(./child::PRODOTTO/DESCRIZIONE)-1), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz') )"/>
<!-- 								 dal <xsl:value-of select="./child::DAL"/> -->
<!-- 								 al <xsl:value-of select="./child::AL"/> -->
								  (Aliquota <xsl:value-of select="translate(format-number(number(translate(./child::PRODOTTO/CORRISPETTIVO_UNITARIO,',','.')),'#####.#####'),'.',',')"/> euro/<xsl:value-of select="./child::PRODOTTO/UNITA_DI_MISURA"/> su <xsl:value-of select="translate(format-number(number(translate(./child::PRODOTTO/QUANTITA,',','.')),'#####.#####'),'.',',')"/> <xsl:value-of select="./child::PRODOTTO/UNITA_DI_MISURA"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right" padding-right="10mm">
								<fo:block><xsl:value-of select="translate(format-number(number(translate(translate(./child::PRODOTTO/PREZZO_TOTALE,'.',''),',','.')),'##########.##'),'.',',')"/> euro</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>

				<xsl:if test="$sezione_imposte/PRECEDENTEMENTE_FATTURATO!='0,0000'">
					<fo:table-row font-size="6pt">
						<fo:table-cell padding-left="10mm">
							<fo:block>
								Precedentemente fatturato:
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right" padding-right="10mm">
								<fo:block><xsl:value-of select="translate(format-number(number(translate(translate($sezione_imposte/PRECEDENTEMENTE_FATTURATO,'.',''),',','.')),'##########.##'),'.',',')"/> euro</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>

			</xsl:if>



			<xsl:if test="$iva!=0">
				<fo:table-row font-size="7pt">
					<fo:table-cell padding-left="2mm">
							<fo:block space-before="3mm">IVA</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm">
<!-- 						<fo:block><xsl:value-of select="translate(format-number($iva div 100,'##########.##'),'.',',')" /></fo:block> -->
												<fo:block></fo:block>

					</fo:table-cell>
				</fo:table-row>
			</xsl:if>


			<xsl:for-each select="$riepilogo_fattura">

				<xsl:if test="not(./DESCRIZIONE_ALIQUOTA_IVA='0%')">

					<fo:table-row font-size="6pt">

						<fo:table-cell padding-left="10mm">
							 <fo:block >IVA Aliquota <xsl:value-of select="./DESCRIZIONE_ALIQUOTA_IVA" /> - Imponibile euro <xsl:value-of select="./IMPONIBILE" /> </fo:block>
						</fo:table-cell>

						<fo:table-cell text-align="right"  padding-right="10mm">
							<fo:block><xsl:value-of select="./IMPORTO_IVA" /> euro</fo:block>
						</fo:table-cell>

					</fo:table-row>

				</xsl:if>

				<xsl:if test="./DESCRIZIONE_ALIQUOTA_IVA='0%'">

					<fo:table-row font-size="6pt">
						<fo:table-cell padding-left="10mm">
							<xsl:variable name="descr_iva" ><xsl:value-of select="./CODICE_IVA" /></xsl:variable>
							<xsl:choose>
								<xsl:when test="contains($descr_iva,'Art. 10')">
							 		<fo:block >IVA Aliquota <xsl:value-of select="./DESCRIZIONE_ALIQUOTA_IVA" /> - Imponibile <xsl:value-of select="./IMPONIBILE" /> euro (<xsl:value-of select="./CODICE_IVA" />)  </fo:block>
							 	</xsl:when>
							 	<xsl:otherwise>
							 		<fo:block >IVA Aliquota <xsl:value-of select="./DESCRIZIONE_ALIQUOTA_IVA" /> - Non imponibile <xsl:value-of select="./IMPONIBILE" /> euro (<xsl:value-of select="./CODICE_IVA" />)  </fo:block>
							 	</xsl:otherwise>
							</xsl:choose>
						</fo:table-cell>
						<fo:table-cell text-align="right"  padding-right="10mm">
							<fo:block>
								<xsl:if test="./IMPORTO_IVA='0,00'">
									-
								</xsl:if>
								<xsl:if test="./IMPORTO_IVA!='0,00'">
									<xsl:if test="./IMPORTO_IVA"> euro </xsl:if>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

				</xsl:if>

			</xsl:for-each>


		</fo:table-body>
	</fo:table>

	<xsl:if test="$t_rai!=0">

				<fo:table space-before="2mm" start-indent="3.5mm" table-layout="fixed" width="92%" display-align="center">

				<fo:table-column column-width="proportional-column-width(80)"/>
				<fo:table-column column-width="proportional-column-width(20)"/>

				<fo:table-body end-indent="0pt" start-indent="0pt">

				<fo:table-row font-family="universcbold" border-bottom="0.5 solid black" text-align="start" font-size="8.5pt">
					<fo:table-cell number-columns-spanned="2">
						<fo:block><xsl:attribute name="color">#0065ae</xsl:attribute>CANONE RAI</fo:block>
					</fo:table-cell>
				</fo:table-row>

						<fo:table-row font-size="7pt">
							<fo:table-cell padding-left="2mm">
									<fo:block space-before="3mm"></fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right" padding-right="2mm">
								<fo:block></fo:block>
							</fo:table-cell>
						</fo:table-row>

						<xsl:for-each select="$sezione_canone_rai/PERIODO_RIFERIMENTO">
							<fo:table-row font-size="6pt">
								<fo:table-cell padding-left="10mm">
									<fo:block>
										<xsl:value-of select="./child::PRODOTTO/DESCRIZIONE"/>
										 dal <xsl:value-of select="./child::PRODOTTO/DAL"/>
										 al <xsl:value-of select="./child::PRODOTTO/AL"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell text-align="right" padding-right="10mm">
										<fo:block><xsl:value-of select="translate(format-number(number(translate(./child::PRODOTTO/PREZZO_TOTALE,',','.')),'##########.##'),'.',',')"/> euro</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:for-each>


				</fo:table-body>
				</fo:table>

		</xsl:if>

</xsl:template>


<xsl:template name="SINTESI_FISCALE_MONOSITO">

	<!-- Elementi radice. Non aggiungere "/" finale -->
 	<xsl:variable name="contratto_energia" select="./child::CONTRATTO_ENERGIA"/>
 	<xsl:variable name="sintesi_energia" select="$contratto_energia/child::SINTESI_ENERGIA"/>
 	<xsl:variable name="deposito_cauzionale" select="./child::CONTRATTO_DEPOSITO_CAUZIONALE"/>
 	<xsl:variable name="sezione_imposte" select="$contratto_energia/child::SEZIONE[@TIPOLOGIA='IMPOSTE']"/>
 	<xsl:variable name="sezione_canone_rai" select="$contratto_energia/child::SEZIONE[@TIPOLOGIA='CANONE_RAI']"/>
 	<xsl:variable name="riepilogo_fattura" select="./child::RIEPILOGO_FATTURA"/>



	<!-- SERVIZI VENDITA -->
	<xsl:variable name="sv">
		<xsl:if test="$sintesi_energia/child::TOTALE_SERVIZI_VENDITA='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::TOTALE_SERVIZI_VENDITA='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_SERVIZI_VENDITA,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>

	<!-- SERVIZI RETE -->
	<xsl:variable name="sr">
		<xsl:if test="$sintesi_energia/TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_SERVIZI_RETE_SENZA_BONUS,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- IMPOSTE -->
	<xsl:variable name="imp">
		<xsl:if test="$sintesi_energia/child::IMPOSTE='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::IMPOSTE='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_IMPOSTE,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- IVA FORNITURA -->
	<xsl:variable name="iva_fornitura">
		<xsl:if test="$sintesi_energia/child::IVA='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::IVA='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::IVA,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- IVA ALTRE PARTITE -->
	<xsl:variable name="iva_odiv">
		<xsl:if test="$sintesi_energia/child::IVA_SU_ONERI_DIVERSI='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::IVA_SU_ONERI_DIVERSI='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::IVA_SU_ONERI_DIVERSI,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>

	<!-- IVA DEPOSITO CAUZIONALE -->
	<xsl:variable name="iva_dc">
		<xsl:if test="../child::CONTRATTO_DEPOSITO_CAUZIONALE">
			<xsl:if test="./child::SINTESI_ENERGIA/child::TOTALE_IVA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::SINTESI_ENERGIA/child::TOTALE_IVA='0,00')">
				<xsl:value-of select="translate(translate(./child::SINTESI_ENERGIA/child::TOTALE_IVA,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE)">
			0
		</xsl:if>
	</xsl:variable>

	<!-- IVA TOTALE -->
	<xsl:variable name="iva">
		<xsl:value-of select="$iva_fornitura+$iva_odiv+$iva_dc"/>
	</xsl:variable>

	<!--  ALTRE PARTITE SENZA IVA -->
	<xsl:variable name="odiv_fornitura">
			<xsl:if test="$sintesi_energia/child::ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not($sintesi_energia/child::ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate($sintesi_energia/child::ONERI_DIVERSI,'.',''),',','')"/>
		</xsl:if>

	</xsl:variable>

	<!--  IMPONIBILE DEPOSITO CAUZIONALE -->
	<xsl:variable name="imponibile_dc">
		<xsl:if test="../child::CONTRATTO_DEPOSITO_CAUZIONALE">
			<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE)">
			0
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="odiv">
		<xsl:value-of select="$odiv_fornitura+$imponibile_dc"/>
	</xsl:variable>

	<!-- BONUS SOCIALE -->
	<xsl:variable name="bs">
			<xsl:if test="$sintesi_energia/child::BONUS_SOCIALE='0,00'">0</xsl:if>
			<xsl:if test="not($sintesi_energia/child::BONUS_SOCIALE='0,00')">
				<xsl:value-of select="translate(translate($sintesi_energia/child::BONUS_SOCIALE,'.',''),',','')"/>
			</xsl:if>
	</xsl:variable>


	<!-- CANONE RAI -->
	<xsl:variable name="t_rai">
			<xsl:if test="$sintesi_energia/child::TOTALE_CANONE_RAI='0,00'">0</xsl:if>
			<xsl:if test="not($sintesi_energia/child::TOTALE_CANONE_RAI='0,00')">
				<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_CANONE_RAI,'.',''),',','')"/>
			</xsl:if>
	</xsl:variable>

	<!-- TOTALE IMPONIBILE FATTURA STANDARD -->
	<xsl:variable name="totale_imponibile_fattura_standard">
		<xsl:if test="$sintesi_energia/child::TOTALE_IMPONIBILE='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::TOTALE_IMPONIBILE='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_IMPONIBILE,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- TOTALE BONUS DEI PRODOTTI -->
	<xsl:variable name="prodotto_bonus_euro">
			<xsl:if test="$contratto_energia/BONUS_PRODOTTO='0,00'">0</xsl:if>
			<xsl:if test="not($contratto_energia/BONUS_PRODOTTO='0,00')">
				<xsl:value-of select="translate(translate($contratto_energia/BONUS_PRODOTTO,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not($contratto_energia/child::BONUS_PRODOTTO)">
				0
			</xsl:if>
	</xsl:variable>

	<xsl:variable name="importo_no_virgola">
		<xsl:value-of select="translate(translate(./child::IMPORTO_DOCUMENTO,'.',''),',','')"/>
	</xsl:variable>

	<xsl:variable name="totale_bolletta"> <!-- no canone rai! -->
		<xsl:value-of select="$sv + $sr + $imp + $iva + $odiv + $bs"/>
	</xsl:variable>



	<xsl:variable name="totale_fattura">
		<xsl:value-of select="$sv + $sr + $imp + $iva + $odiv + $bs + $t_rai"/>
	</xsl:variable>

	<xsl:variable name="contratto_corrente">
			<xsl:if test="$contratto_energia/child::CONTRACT_NO">
				<xsl:value-of select="./child::CONTRACT_NO"/>
			</xsl:if>
			<xsl:if test="not($contratto_energia/child::CONTRACT_NO)">
				NO-CONTR
			</xsl:if>
	</xsl:variable>


		<fo:table space-before="10mm" start-indent="3.5mm" table-layout="fixed" width="92%" display-align="center">

		<fo:table-column column-width="proportional-column-width(77)"/>
		<fo:table-column column-width="proportional-column-width(23)"/>

		<fo:table-body end-indent="0pt" start-indent="0pt">

		<fo:table-row font-family="universcbold" border-bottom="0.5 solid black" text-align="start" font-size="8.5pt">
			<fo:table-cell number-columns-spanned="2">
				<fo:block><xsl:attribute name="color">#0065ae</xsl:attribute>SINTESI FISCALE</fo:block>
			</fo:table-cell>
		</fo:table-row>


			<xsl:if test="$imp!=0">
				<fo:table-row font-size="7pt">
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="3mm">ACCISE</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm">
<!-- 						<fo:block><xsl:value-of select="translate(format-number( $imp div 100,'##########.##'),'.',',')" /></fo:block> -->
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>

			   <xsl:for-each select="$sezione_imposte/PERIODO_RIFERIMENTO">
					<fo:table-row font-size="6pt">
						<fo:table-cell padding-left="10mm">
							<fo:block>

								 <xsl:value-of select="concat( substring( ./child::PRODOTTO/DESCRIZIONE, 1,1) , translate( substring( ./child::PRODOTTO/DESCRIZIONE, 2, string-length(./child::PRODOTTO/DESCRIZIONE)-1), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz') )"/>
<!-- 								 dal <xsl:value-of select="./child::DAL"/> -->
<!-- 								 al <xsl:value-of select="./child::AL"/> -->
								  (Aliquota <xsl:value-of select="translate(format-number(number(translate(./child::PRODOTTO/CORRISPETTIVO_UNITARIO,',','.')),'#####.#####'),'.',',')"/> euro/<xsl:value-of select="./child::PRODOTTO/UNITA_DI_MISURA"/> su <xsl:value-of select="translate(format-number(number(translate(./child::PRODOTTO/QUANTITA,',','.')),'#####.#####'),'.',',')"/> <xsl:value-of select="./child::PRODOTTO/UNITA_DI_MISURA"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right" padding-right="10mm">
								<fo:block><xsl:value-of select="translate(format-number(number(translate(translate(./child::PRODOTTO/PREZZO_TOTALE,'.',''),',','.')),'##########.##'),'.',',')"/> euro</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>

				<xsl:if test="$sezione_imposte/PRECEDENTEMENTE_FATTURATO!='0,0000'">
					<fo:table-row font-size="6pt">
						<fo:table-cell padding-left="10mm">
							<fo:block>
								Precedentemente fatturato:
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right" padding-right="10mm">
								<fo:block><xsl:value-of select="translate(format-number(number(translate(translate($sezione_imposte/PRECEDENTEMENTE_FATTURATO,'.',''),',','.')),'##########.##'),'.',',')"/> euro</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>

			</xsl:if>



			<xsl:if test="$iva!=0">
				<fo:table-row font-size="7pt">
					<fo:table-cell padding-left="2mm">
							<fo:block space-before="3mm">IVA</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm">
<!-- 						<fo:block><xsl:value-of select="translate(format-number($iva div 100,'##########.##'),'.',',')" /></fo:block> -->
												<fo:block></fo:block>

					</fo:table-cell>
				</fo:table-row>
			</xsl:if>


			<xsl:for-each select="$riepilogo_fattura">

				<xsl:if test="not(./DESCRIZIONE_ALIQUOTA_IVA='0%')">

					<fo:table-row font-size="6pt">

						<fo:table-cell padding-left="10mm">
							 <fo:block >IVA Aliquota <xsl:value-of select="./DESCRIZIONE_ALIQUOTA_IVA" /> - Imponibile euro <xsl:value-of select="./IMPONIBILE" /> </fo:block>
						</fo:table-cell>

						<fo:table-cell text-align="right"  padding-right="10mm">
							<fo:block><xsl:value-of select="./IMPORTO_IVA" /> euro</fo:block>
						</fo:table-cell>

					</fo:table-row>

				</xsl:if>

				<xsl:if test="./DESCRIZIONE_ALIQUOTA_IVA='0%'">

					<fo:table-row font-size="6pt">
						<fo:table-cell padding-left="10mm">
							<xsl:variable name="descr_iva" ><xsl:value-of select="./CODICE_IVA" /></xsl:variable>
							<xsl:choose>
								<xsl:when test="contains($descr_iva,'Art. 10')">
							 		<fo:block >IVA Aliquota <xsl:value-of select="./DESCRIZIONE_ALIQUOTA_IVA" /> - Imponibile <xsl:value-of select="./IMPONIBILE" /> euro (<xsl:value-of select="./CODICE_IVA" />)  </fo:block>
							 	</xsl:when>
							 	<xsl:otherwise>
							 		<fo:block >IVA Aliquota <xsl:value-of select="./DESCRIZIONE_ALIQUOTA_IVA" /> - Non imponibile <xsl:value-of select="./IMPONIBILE" /> euro (<xsl:value-of select="./CODICE_IVA" />)  </fo:block>
							 	</xsl:otherwise>
							</xsl:choose>
						</fo:table-cell>
						<fo:table-cell text-align="right"  padding-right="10mm">
							<fo:block>
								<xsl:if test="./IMPORTO_IVA='0,00'">
									-
								</xsl:if>
								<xsl:if test="./IMPORTO_IVA!='0,00'">
									<xsl:if test="./IMPORTO_IVA"> euro </xsl:if>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

				</xsl:if>

			</xsl:for-each>


		</fo:table-body>
	</fo:table>

	<xsl:if test="$t_rai!=0">

				<fo:table space-before="2mm" start-indent="3.5mm" table-layout="fixed" width="92%" display-align="center">

				<fo:table-column column-width="proportional-column-width(80)"/>
				<fo:table-column column-width="proportional-column-width(20)"/>

				<fo:table-body end-indent="0pt" start-indent="0pt">

				<fo:table-row font-family="universcbold" border-bottom="0.5 solid black" text-align="start" font-size="8.5pt">
					<fo:table-cell number-columns-spanned="2">
						<fo:block><xsl:attribute name="color">#0065ae</xsl:attribute>CANONE RAI</fo:block>
					</fo:table-cell>
				</fo:table-row>

						<fo:table-row font-size="7pt">
							<fo:table-cell padding-left="2mm">
									<fo:block space-before="3mm"></fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right" padding-right="2mm">
								<fo:block></fo:block>
							</fo:table-cell>
						</fo:table-row>

						<xsl:for-each select="$sezione_canone_rai/PERIODO_RIFERIMENTO">
							<fo:table-row font-size="6pt">
								<fo:table-cell padding-left="10mm">
									<fo:block>
										<xsl:value-of select="./child::PRODOTTO/DESCRIZIONE"/>
										 dal <xsl:value-of select="./child::PRODOTTO/DAL"/>
										 al <xsl:value-of select="./child::PRODOTTO/AL"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell text-align="right" padding-right="10mm">
										<fo:block><xsl:value-of select="translate(format-number(number(translate(./child::PRODOTTO/PREZZO_TOTALE,',','.')),'##########.##'),'.',',')"/> euro</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:for-each>


				</fo:table-body>
				</fo:table>

		</xsl:if>

</xsl:template>


<xsl:template name="SINTESI_FISCALE_MONOSITO3">


	<!-- Elementi radice. Non aggiungere "/" finale -->
 	<xsl:variable name="contratto_energia" select="./child::CONTRATTO_ENERGIA"/>
 	<xsl:variable name="sintesi_energia" select="$contratto_energia/child::SINTESI_ENERGIA"/>
 	<xsl:variable name="deposito_cauzionale" select="./child::CONTRATTO_DEPOSITO_CAUZIONALE"/>
 	<xsl:variable name="sezione_imposte" select="$contratto_energia/child::SEZIONE[@TIPOLOGIA='IMPOSTE']"/>
 	<xsl:variable name="sezione_altre_partite" select="$contratto_energia/child::SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']"/>
 	<xsl:variable name="sezione_canone_rai" select="$contratto_energia/child::SEZIONE[@TIPOLOGIA='CANONE_RAI']"/>
 	<xsl:variable name="riepilogo_fattura" select="./child::RIEPILOGO_FATTURA"/>



	<!-- SERVIZI VENDITA -->
	<xsl:variable name="sv">
		<xsl:if test="$sintesi_energia/child::TOTALE_SERVIZI_VENDITA='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::TOTALE_SERVIZI_VENDITA='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_SERVIZI_VENDITA,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>

	<!-- SERVIZI RETE -->
	<xsl:variable name="sr">
		<xsl:if test="$sintesi_energia/TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_SERVIZI_RETE_SENZA_BONUS,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- IMPOSTE -->
	<xsl:variable name="imp">
		<xsl:if test="$sintesi_energia/child::IMPOSTE='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::IMPOSTE='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_IMPOSTE,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- IVA FORNITURA -->
	<xsl:variable name="iva_fornitura">
		<xsl:if test="$sintesi_energia/child::IVA='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::IVA='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::IVA,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- IVA ALTRE PARTITE -->
	<xsl:variable name="iva_odiv">
		<xsl:if test="$sintesi_energia/child::IVA_SU_ONERI_DIVERSI='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::IVA_SU_ONERI_DIVERSI='0,00')">
			<xsl:value-of select="translate(translate($sintesi_energia/child::IVA_SU_ONERI_DIVERSI,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>

	<!-- IVA DEPOSITO CAUZIONALE -->
	<xsl:variable name="iva_dc">
		<xsl:if test="$deposito_cauzionale">
			<xsl:if test="$deposito_cauzionale/TOTALE_IVA='0,00'">0</xsl:if>
			<xsl:if test="not($deposito_cauzionale/TOTALE_IVA='0,00')">
				<xsl:value-of select="translate(translate($deposito_cauzionale/TOTALE_IVA,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not($deposito_cauzionale)">
			0
		</xsl:if>
	</xsl:variable>

	<!-- IVA TOTALE -->
	<xsl:variable name="iva">
		<xsl:value-of select="$iva_fornitura+$iva_odiv+$iva_dc"/>
	</xsl:variable>

	<!--  ALTRE PARTITE SENZA IVA -->
	<xsl:variable name="odiv_fornitura">
			<xsl:if test="$sintesi_energia/child::ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not($sintesi_energia/child::ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate($sintesi_energia/child::ONERI_DIVERSI,'.',''),',','')"/>
		</xsl:if>

	</xsl:variable>


	<!--  ALTRE PARTITE IMPONIBILE SUL QUALE CALCOLARE IVA -->
	<xsl:variable name="odiv_imponibile_fornitura">
		<xsl:if test="$sintesi_energia/child::ONERI_DIVERSI='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::ONERI_DIVERSI='0,00')">
			<xsl:value-of select="sum($sezione_altre_partite/*/IMPONIBILE_ODIV)"/>
		</xsl:if>
	</xsl:variable>

	<!--  IMPONIBILE DEPOSITO CAUZIONALE -->
	<xsl:variable name="imponibile_dc">
		<xsl:if test="$deposito_cauzionale">
			<xsl:if test="$deposito_cauzionale/TOTALE_IMPONIBILE='0,00'">0</xsl:if>
			<xsl:if test="not($deposito_cauzionale/TOTALE_IMPONIBILE='0,00')">
				<xsl:value-of select="translate(translate($deposito_cauzionale/TOTALE_IMPONIBILE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not($deposito_cauzionale)">
			0
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="odiv">
		<xsl:value-of select="$odiv_fornitura+$imponibile_dc"/>
	</xsl:variable>

	<!-- BONUS SOCIALE -->
	<xsl:variable name="bs">
			<xsl:if test="$sintesi_energia/child::BONUS_SOCIALE='0,00'">0</xsl:if>
			<xsl:if test="not($sintesi_energia/child::BONUS_SOCIALE='0,00')">
				<xsl:value-of select="translate(translate($sintesi_energia/child::BONUS_SOCIALE,'.',''),',','')"/>
			</xsl:if>
	</xsl:variable>


	<!-- CANONE RAI -->
	<xsl:variable name="t_rai">
			<xsl:if test="$sintesi_energia/child::TOTALE_CANONE_RAI='0,00'">0</xsl:if>
			<xsl:if test="not($sintesi_energia/child::TOTALE_CANONE_RAI='0,00')">
				<xsl:value-of select="translate(translate($sintesi_energia/child::TOTALE_CANONE_RAI,'.',''),',','')"/>
			</xsl:if>
	</xsl:variable>

	<!-- TOTALE IMPONIBILE FATTURA STANDARD -->
	<xsl:variable name="totale_imponibile_fattura_standard">
		<xsl:if test="$sintesi_energia/child::TOTALE_IMPONIBILE='0,00'">0</xsl:if>
		<xsl:if test="not($sintesi_energia/child::TOTALE_IMPONIBILE='0,00')">
			<xsl:value-of select="translate(translate( $sintesi_energia/child::TOTALE_IMPONIBILE,'.',''),',','')"/>
		</xsl:if>
	</xsl:variable>


	<!-- TOTALE BONUS DEI PRODOTTI -->
	<xsl:variable name="prodotto_bonus_euro">
			<xsl:if test="$contratto_energia/BONUS_PRODOTTO='0,00'">0</xsl:if>
			<xsl:if test="not($contratto_energia/BONUS_PRODOTTO='0,00')">
				<xsl:value-of select="translate(translate($contratto_energia/BONUS_PRODOTTO,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not($contratto_energia/child::BONUS_PRODOTTO)">
				0
			</xsl:if>
	</xsl:variable>

	<xsl:variable name="importo_no_virgola">
		<xsl:value-of select="translate(translate(./child::IMPORTO_DOCUMENTO,'.',''),',','')"/>
	</xsl:variable>

	<xsl:variable name="totale_bolletta"> <!-- no canone rai! -->
		<xsl:value-of select="$sv + $sr + $imp + $iva + $odiv + $bs"/>
	</xsl:variable>



	<xsl:variable name="totale_fattura">
		<xsl:value-of select="$sv + $sr + $imp + $iva + $odiv + $bs + $t_rai" />

	</xsl:variable>

	<xsl:variable name="contratto_corrente" >
			<xsl:if test="$contratto_energia/child::CONTRACT_NO">
				<xsl:value-of select="./child::CONTRACT_NO"/>
			</xsl:if>
			<xsl:if test="not($contratto_energia/child::CONTRACT_NO)">
				NO-CONTR
			</xsl:if>
	</xsl:variable>


		<fo:table start-indent="3.5mm" table-layout="fixed" width="96%" display-align="center" space-before="3mm">

		<fo:table-column column-width="proportional-column-width(80)"/>
		<fo:table-column column-width="proportional-column-width(20)"/>

		<fo:table-body end-indent="0pt" start-indent="0pt">

		<fo:table-row font-family="universcbold" border-bottom="0.5 solid black" text-align="start" font-size="8.5pt" >
				<fo:table-cell number-columns-spanned="2">
					<fo:block><xsl:attribute name="color">#0065ae</xsl:attribute>SINTESI FISCALE</fo:block>
				</fo:table-cell>
			</fo:table-row>

				<fo:table-row font-size="7pt" >
					<fo:table-cell padding-left="2mm" >
						<fo:block font-family="universcbold" padding-top="2mm">IMPORTI SOGGETTI AD IVA</fo:block>
					</fo:table-cell>

					<fo:table-cell text-align="left" padding-right="2mm">
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>



			<xsl:if test="($sv+$sr)!=0">
				<fo:table-row font-size="7pt">
					<fo:table-cell padding-left="2mm">
						<fo:block>Totale servizi di vendita e servizi di rete (A+B)</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm">
						<fo:block><xsl:value-of select="translate(format-number(  ($sv+$sr) div 100,'##########.##'),'.',',')" /> euro</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>

			<xsl:if test="$imp!=0">
				<fo:table-row font-size="7pt">
					<fo:table-cell padding-left="2mm">
						<fo:block>Totale accise (C)</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm">
						<fo:block><xsl:value-of select="translate(format-number( $imp div 100,'##########.##'),'.',',')" /> euro</fo:block>
					</fo:table-cell>
				</fo:table-row>

			   <xsl:for-each select="$sezione_imposte/PERIODO_RIFERIMENTO">
					<fo:table-row font-size="6pt" 	>
						<fo:table-cell padding-left="5mm">
							<fo:block font-family="universccors">
								 <xsl:value-of select="concat( substring( ./child::PRODOTTO/DESCRIZIONE, 1,1) , translate( substring( ./child::PRODOTTO/DESCRIZIONE, 2, string-length(./child::PRODOTTO/DESCRIZIONE)-1), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz') )"/>
								 dal <xsl:value-of select="./child::DAL"/>
								 al <xsl:value-of select="./child::AL"/>
								  (Aliquota <xsl:value-of select="translate(format-number(number(translate(./child::PRODOTTO/CORRISPETTIVO_UNITARIO,',','.')),'#####.#####'),'.',',')"/> euro/<xsl:value-of select="./child::PRODOTTO/UNITA_DI_MISURA"/> su <xsl:value-of select="translate(format-number(number(translate(./child::PRODOTTO/QUANTITA,',','.')),'#####.#####'),'.',',')"/> <xsl:value-of select="./child::PRODOTTO/UNITA_DI_MISURA"/> )
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right" padding-right="2mm">
								<fo:block font-family="universccors"> <xsl:value-of select="translate(format-number(number(translate(./child::PRODOTTO/PREZZO_TOTALE,',','.')),'##########.##'),'.',',')"/> euro</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>

				<xsl:if test="$sezione_imposte/PRECEDENTEMENTE_FATTURATO">
					<xsl:if test="$sezione_imposte/PRECEDENTEMENTE_FATTURATO!='0,0000'">
						<fo:table-row font-size="6pt">
							<fo:table-cell padding-left="5mm">
								<fo:block font-family="universccors">
									Storno per accise precedentemente fatturate:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right" padding-right="2mm" > <!-- da mettere in corsivo -->
									<fo:block font-family="universccors"><xsl:value-of select="translate( format-number( number( translate( translate($sezione_imposte/PRECEDENTEMENTE_FATTURATO,'.',''),',','.')),'##########.##'),'.',',')"/> euro</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:if>
				</xsl:if>

			</xsl:if>

			<xsl:if test="$bs!=0">
				<fo:table-row font-size="7pt">
					<fo:table-cell padding-left="2mm" padding-top="2mm">
							<fo:block>Bonus Sociale</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm" padding-top="2mm">
						<fo:block><xsl:value-of select="translate(format-number($bs div 100,'##########.##'),'.',',')" /> euro</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>



<!-- totale_altre_partite_con_iva -->


			<xsl:for-each select="$sezione_altre_partite/PERIODO_RIFERIMENTO/PRODOTTO">

					<xsl:choose>
						<xsl:when test="./IVA_DA_CALCOLARE_SUL_PRODOTTO != 0">
							<fo:table-row >
								<fo:table-cell padding-left="2mm" font-size="7pt">
								 	<fo:block >Altre partite (soggette ad aliquota IVA <xsl:value-of select="format-number(./IVA_DA_CALCOLARE_SUL_PRODOTTO div 100, '##%')" />) </fo:block>
								</fo:table-cell>
								<fo:table-cell text-align="right"  padding-right="2mm">
									<fo:block >
										<xsl:value-of select="translate(format-number(translate(translate(./PREZZO_TOTALE,'.',''),',','.'),'#########.##'),'.',',')" /> euro
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
			</xsl:for-each>
<!--  -->

				<fo:table-row font-size="7pt" background-color = "lightgray">
					<fo:table-cell padding-left="2mm">
							<fo:block>Totale importi soggetti ad IVA (base imponibile)</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm">
						<fo:block>
							<xsl:value-of select="translate(format-number( ( ( $totale_imponibile_fattura_standard  div 100) + $odiv_imponibile_fornitura),'##########.##'),'.',',')" /> euro

						</fo:block>
					</fo:table-cell>
				</fo:table-row>

<!-- Inserisci distanziamento -->

			<xsl:if test="$iva!=0">
				<fo:table-row font-size="7pt" >
					<fo:table-cell padding-left="2mm" >
							<fo:block font-family="universcbold" padding-top="1mm">IVA</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm">
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>


			<xsl:for-each select="$riepilogo_fattura">

				<xsl:if test="not(./DESCRIZIONE_ALIQUOTA_IVA='0%')">

					<fo:table-row font-size="7pt">
						<fo:table-cell padding-left="2mm" font-size="7pt">
							 	 <fo:block >IVA Aliquota <xsl:value-of select="./DESCRIZIONE_ALIQUOTA_IVA" /> </fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right"  padding-right="2mm">
							<fo:block><xsl:value-of select="./IMPORTO_IVA" /> euro</fo:block>
						</fo:table-cell>
					</fo:table-row>

				</xsl:if>

			</xsl:for-each>


			<fo:table-row font-size="7pt">
				<fo:table-cell padding-left="2mm" padding-top="2mm">
					<fo:block font-family="universcbold">IMPORTI NON SOGGETTI AD IVA </fo:block>
				</fo:table-cell>

				<fo:table-cell text-align="left" padding-right="2mm" padding-top="2mm">
					<fo:block></fo:block>
				</fo:table-cell>
			</fo:table-row>


			<xsl:for-each select="$riepilogo_fattura">
				<xsl:if test="./DESCRIZIONE_ALIQUOTA_IVA='0%' and ./IS_CANONE='NORAI'">
					<fo:table-row >
						<fo:table-cell padding-left="2mm" font-size="7pt">
							<xsl:variable name="descr_iva" ><xsl:value-of select="./CODICE_IVA" /></xsl:variable>
							 <fo:block >Altre partite (non soggette ad IVA) <xsl:value-of select="./CODICE_IVA" /> </fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right"  padding-right="2mm">
							<fo:block >
								<xsl:value-of select="./IMPONIBILE" /> euro
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</xsl:for-each>


			<xsl:if test="$t_rai!=0">
				<fo:table-row font-size="7pt" background-color = "lightgray">
						<fo:table-cell padding-left="2mm">
  							<fo:block>TOTALE BOLLETTA</fo:block>
  					</fo:table-cell>
					<fo:table-cell text-align="right" padding-right="2mm">
						<fo:block><xsl:value-of select="translate(format-number($totale_bolletta div 100,'##########.##'),'.',',')" /> euro</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>

			<xsl:if test="$t_rai!=0">
				<xsl:for-each select="$sezione_canone_rai/PERIODO_RIFERIMENTO/PRODOTTO">
					<fo:table-row font-size="7pt">
						<fo:table-cell padding-left="2mm">
							<fo:block>
								<xsl:value-of select="./child::DESCRIZIONE"/>
								 dal <xsl:value-of select="./child::DAL"/>
								 al <xsl:value-of select="./child::AL"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right" padding-right="2mm">
								<fo:block><xsl:value-of select="translate(format-number(number(translate(./child::PREZZO_TOTALE,',','.')),'##########.##'),'.',',')"/> euro</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</xsl:if>







			<fo:table-row font-size="7pt" background-color = "lightgray">
					<fo:table-cell padding-left="2mm">
						<fo:block>TOTALE DA PAGARE</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" padding-right="2mm">
					<fo:block><xsl:value-of select="translate(format-number($totale_fattura div 100,'##########.##'),'.',',')" /> euro</fo:block>

				</fo:table-cell>
			</fo:table-row>

		</fo:table-body>
	</fo:table>
</xsl:template>







<!-- Avviso fatture pagate/in attesa di pagamento -->
<xsl:template name="STATO_PAGAMENTI">
	<fo:table start-indent="3.5mm" table-layout="fixed" width="96%" display-align="center" text-align="start">
	<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body>
			<fo:table-row>
				<fo:table-cell padding-left="2mm">
					<fo:block>
						<xsl:if test="not(./child::FATTURE_ATTESA_PAGAMENTO) or (./child::FATTURE_ATTESA_PAGAMENTO and (count(./child::FATTURE_ATTESA_PAGAMENTO) &lt; 1))">
							<fo:block font-family="universcbold">
								<xsl:attribute name="color">#0065ae</xsl:attribute>
								TUTTE LE FATTURE PRECEDENTI RISULTANO PAGATE. GRAZIE.
							</fo:block>

							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="6pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" /> e i relativi contratti.</fo:block>
								<fo:block font-size="6pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>

						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO)=1">
							<fo:block font-family="universcbold">
								<xsl:attribute name="color">#0065ae</xsl:attribute>
								ATTENZIONE: 1 FATTURA RISULTA IN ATTESA DI PAGAMENTO, PER UN TOTALE INSOLUTO DI EURO <xsl:value-of select="./child::TOTALE_FATTURE_ATTESA_PAGAMENTO" />.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>

							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="6pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" /> e i relativi contratti.</fo:block>
								<fo:block font-size="6pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>

						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO) &gt; 1">
							<fo:block font-family="universcbold">
								<xsl:attribute name="color">#0065ae</xsl:attribute>
								ATTENZIONE: <xsl:value-of select="count(./child::FATTURE_ATTESA_PAGAMENTO)"/> FATTURE RISULTANO IN ATTESA DI PAGAMENTO, PER UN TOTALE INSOLUTO DI EURO <xsl:value-of select="./child::TOTALE_FATTURE_ATTESA_PAGAMENTO" />.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>

							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="6pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" /> e i relativi contratti.</fo:block>
								<fo:block font-size="6pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
</xsl:template>

<!-- Frasi SDD ERRATO/REVOCATO - Deposito Cauzionale -->
<xsl:template name="INFO_RID">
	<fo:table start-indent="3.5mm" space-before="3mm" table-layout="fixed" width="96%" display-align="center" text-align="start">
	<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row>
				<fo:table-cell padding-left="2mm">
					<fo:block font-family="universc">
						<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO='Addebito tramite SDD')">
							<xsl:choose>
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
									<fo:block font-family="universcbold">
										<xsl:attribute name="color">#0065ae</xsl:attribute>
										ATTENZIONE! La sua Banca ha respinto la richiesta di attivazione dell'addebito diretto SEPA Direct Debit (SDD).
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>

								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_REVOCATO']">
									<fo:block font-family="universcbold">
										<xsl:attribute name="color">#0065ae</xsl:attribute>
										Riattivi subito l'addebito diretto SEPA Direct Debit (SDD) per evitare il pagamento del deposito cauzionale!
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>

								<xsl:otherwise>
									<xsl:if test="@PRESENZA_DEPOSITO_CAUZIONALE='SI'">
										<fo:block font-family="universcbold">
											<xsl:attribute name="color">#0065ae</xsl:attribute>
											Rientri in possesso del deposito cauzionale passando all'addebito diretto SEPA Direct Debit (SDD)!
										</fo:block>
										<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
</xsl:template>



<!-- Altre comunicazioni importanti  -->
<xsl:template name="ALTRE_COMUNICAZIONI_FRONTESPIZIO">

	<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='CORRISPETTIVO_ERRATO'">
		<fo:table start-indent="3.5mm" space-before="3mm" table-layout="fixed" width="100%" display-align="center" text-align="start">
		    <fo:table-column column-width="proportional-column-width(100)"/>
  			<fo:table-body end-indent="0pt" start-indent="0pt">
  				<fo:table-row>
  					<fo:table-cell padding-left="2mm">
  						<fo:block font-family="universc">
  							<fo:inline color="#0065ae" font-family="universcbold">ATTENZIONE</fo:inline>: In questa fattura troverà il ricalcolo dei primi mesi del 2016 per una variazione a suo favore di alcuni corrispettivi di trasporto.
  						</fo:block>
  					</fo:table-cell>
  				</fo:table-row>
  			</fo:table-body>
		</fo:table>
	</xsl:if>



</xsl:template>
















<xsl:template name="DETTAGLIO_IMPORTI">
	<xsl:param name="bordi"/>
	<xsl:param name="consumi_fatturati"/>
	<xsl:param name="imponibile_no_altrepartite"/>
	<xsl:param name="imponibile_sv"/>
	<xsl:param name="sv"/><!-- servizi di vendita -->
	<xsl:param name="sr"/><!-- servizi di rete senza bonus sociale (il bonus sociale sta nei servizi di rete al 17/02/2020 )-->
	<xsl:param name="imp"/><!-- accise (imposte) -->
	<xsl:param name="iva"/><!-- iva -->
	<xsl:param name="odiv"/><!-- altre partite (oneri diversi) -->
	<xsl:param name="bs"/><!-- bonus sociale -->
	<xsl:param name="t_rai"/><!-- canone rai -->
	<xsl:param name="doc"/>
	<xsl:param name="costo_medio"/>
	<xsl:param name="costo_medio_sv"/>


	<fo:table space-before="6mm" text-align="start" font-family="universc" font-size="5pt" end-indent="0pt"  table-layout="fixed" width="100%" border="0.5pt solid cyan">
	<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row height="52mm">
				<fo:table-cell>
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute>
					</xsl:if>
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="105%">

						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(20)"/> 
						<fo:table-column column-width="proportional-column-width(20)"/>

						<fo:table-body end-indent="0pt" start-indent="0pt">
							<fo:table-row>
								<fo:table-cell padding-left="2mm" display-align="after">
									<fo:block>
									</fo:block>
								</fo:table-cell>

								<fo:table-cell display-align="after">
									<fo:block font-family="universccors">
										Euro
									</fo:block>
								</fo:table-cell>

								<fo:table-cell display-align="after">
									<fo:block font-family="universccors">
										(%)
									</fo:block>
								</fo:table-cell>
							</fo:table-row>

							<xsl:choose>
								<xsl:when test="not($sv = 0.0)">
									<fo:table-row height="4mm">

										<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
											<fo:block>
											<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/simpleColor07.svg" /></fo:inline>
												<fo:inline padding-left="2mm">TOTALE SERVIZI DI VENDITA (A)</fo:inline>
											</fo:block>
										</fo:table-cell>

										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
												<fo:block>
												<xsl:value-of select="translate($sv div 100,'.',',')"/>
											</fo:block>
										</fo:table-cell>

										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block font-family="universccors">
												<xsl:if test="round($sv div $doc * 10000) div 100 = 0">
													(0)%
												</xsl:if>
												<xsl:if test="not(round($sv div $doc * 10000) div 100 = 0)">
													(<xsl:value-of select="translate(round(math:abs($sv) div $doc * 10000) div 100,'.',',')"/>%)
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
							</xsl:when>
								<xsl:otherwise>
	 							</xsl:otherwise>
						   </xsl:choose>

							<xsl:choose>
								<xsl:when test="not($sr = 0.0)">
									<fo:table-row height="6mm">
										<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
											<fo:block>
												<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/simpleColor06.svg" /></fo:inline>
													<fo:inline padding-left="2mm">TOTALE SERVIZI DI RETE (B)</fo:inline>
											</fo:block>
										</fo:table-cell>

										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block>

												<xsl:value-of select="translate($sr div 100,'.',',')"/>
											</fo:block>
										</fo:table-cell>

										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block font-family="universccors">
												<xsl:if test="round($sr div $doc * 10000) div 100 = 0">
													(0)%
												</xsl:if>
												<xsl:if test="not(round($sr div $doc * 10000) div 100 = 0)">
													(<xsl:value-of select="translate(round(math:abs($sr) div $doc * 10000) div 100,'.',',')"/>%)
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>

							<xsl:choose>
								<xsl:when test="not($imp = 0.0)">
									<fo:table-row height="6mm">
											<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
													<fo:block>
														<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/simpleColor05.svg" /></fo:inline>
														<fo:inline padding-left="2mm">TOTALE ACCISE (C)</fo:inline>
													</fo:block>
												</fo:table-cell>

										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block>

												<xsl:value-of select="translate($imp div 100,'.',',')"/>
											</fo:block>
										</fo:table-cell>

										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block font-family="universccors">
												<xsl:if test="round($imp div $doc * 10000) div 100 = 0">
													(0)%
												</xsl:if>
												<xsl:if test="not(round($imp div $doc * 10000) div 100 = 0)">
													(<xsl:value-of select="translate(round(math:abs($imp) div $doc * 10000) div 100,'.',',')"/>%)
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:when>
								<xsl:otherwise>

								</xsl:otherwise>
							</xsl:choose>

							<xsl:choose>
								<xsl:when test="not($iva = 0.0)">
									<fo:table-row height="6mm">
										<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
											<fo:block>
												<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/simpleColor04.svg" /></fo:inline>
												<fo:inline padding-left="2mm">IVA </fo:inline><fo:inline font-size="4pt">(DETTAGLIO A SEGUIRE)</fo:inline>
											</fo:block>
										</fo:table-cell>

										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block>
												<xsl:value-of select="translate($iva div 100,'.',',')"/>
											</fo:block>
										</fo:table-cell>

										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block font-family="universccors">
												<xsl:if test="round($iva div $doc * 10000) div 100 = 0">
													(0)%
												</xsl:if>
												<xsl:if test="not(round($iva div $doc * 10000) div 100 = 0)">
													(<xsl:value-of select="translate(round(math:abs($iva) div $doc * 10000) div 100,'.',',')"/>%)
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
						</xsl:choose>

						<xsl:choose>
							<xsl:when test="not($odiv = 0.0)">
								<fo:table-row height="6mm">
									<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/simpleColor03.svg" /></fo:inline>
											<fo:inline padding-left="2mm">ALTRE PARTITE</fo:inline>
										</fo:block>
									</fo:table-cell>

									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<xsl:value-of select="translate($odiv div 100,'.',',')"/>
										</fo:block>
									</fo:table-cell>

									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
										<fo:block font-family="universccors">
											<xsl:if test="round($odiv div $doc * 10000) div 100 = 0">
												(0)%
											</xsl:if>
											<xsl:if test="not(round($odiv div $doc * 10000) div 100 = 0)">
												(<xsl:value-of select="translate(round(math:abs($odiv) div $doc * 10000) div 100,'.',',')"/>%)
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>

							<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE='0,00') and (./child::CONTRATTO_ENERGIA)">
								<fo:table-row height="6mm">
									<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/simpleColor01.svg" /></fo:inline>
											<fo:inline padding-left="2mm">BONUS SOCIALE</fo:inline>
										</fo:block>
									</fo:table-cell>

									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<xsl:value-of select="translate($bs div 100,'.',',')"/>
										</fo:block>
									</fo:table-cell>

									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
									<fo:block font-family="universccors">
										<xsl:if test="round($bs div $doc * 10000) div 100 = 0">
											(0)%
										</xsl:if>
										<xsl:if test="not(round($bs div $doc * 10000) div 100 = 0)">
											(<xsl:value-of select="translate(round(math:abs($bs) div $doc * 10000) div 100,'.',',')"/>%)
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								</fo:table-row>
							</xsl:if>

							<xsl:choose>
								<xsl:when test="$t_rai >0">
									<fo:table-row font-family="universcbold">
										<fo:table-cell padding-left="2mm" padding-top="2mm" border-bottom="1 solid black" display-align="after">
											<fo:block>
												<fo:inline>TOTALE BOLLETTA</fo:inline>
											</fo:block>
										</fo:table-cell>

										<fo:table-cell number-columns-spanned="2" padding-top="2mm" border-bottom="1 solid black" display-align="after">
											<fo:block>
												<xsl:value-of select="translate(($sv+$sr+$imp+$odiv+$iva+$bs) div 100,'.',',') "/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>


							<!-- Inizio CANONE RAI -->
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='CANONE_RAI'">
								<fo:table-row height="6mm">
									<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/simpleColor02.svg" /></fo:inline>
											<fo:inline padding-left="2mm" font-size="5pt">CANONE DI ABBONAMENTO RAI</fo:inline>
										</fo:block>
									</fo:table-cell>

									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<xsl:value-of select="translate($t_rai div 100,'.',',')"/>
										</fo:block>
									</fo:table-cell>

									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
									<fo:block font-family="universccors">
										<xsl:if test="round($t_rai div $doc * 10000) div 100 = 0">
											(0)%
										</xsl:if>
										<xsl:if test="not(round($t_rai div $doc * 10000) div 100 = 0)">
											(<xsl:value-of select="translate(round(math:abs($t_rai) div $doc * 10000) div 100,'.',',')"/>%)
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								</fo:table-row>
							</xsl:if> 
							<!-- FINE CANONE RAI -->




							<fo:table-row font-family="universcbold"> <!--  10mm -->
								<fo:table-cell padding-left="2mm" padding-top="2mm" border-bottom="1 solid black" display-align="after">
									<fo:block>
										<fo:inline>TOTALE DA PAGARE</fo:inline>
									</fo:block>
								</fo:table-cell>

								<fo:table-cell number-columns-spanned="2" padding-top="2mm" border-bottom="1 solid black" display-align="after">
									<fo:block>
										<xsl:value-of select="./child::IMPORTO_DOCUMENTO"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>


						</fo:table-body>
					</fo:table>

				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
</xsl:template>




</xsl:stylesheet>
