<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**                                                    **
**    Template per specchietto consumi e conguagli    **
**                                                    **
********************************************************
-->
<xsl:template match="LETTURE_CONSUMI_CONGUAGLI">
<xsl:param name="bordi"/>
<xsl:param name="color"/>
<xsl:param name="svg-titoli"/>
<xsl:param name="tipo"/>
<xsl:param name="mese_fattura"/>

<xsl:if test="$tipo='header'">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" text-align="start">
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-body>
		<fo:table-row keep-with-previous="always" height="2mm">
			<fo:table-cell number-columns-spanned="5">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row height="6mm">
			<fo:table-cell padding-left="3mm" number-columns-spanned="5" display-align="center">
				<xsl:attribute name="background-image"><xsl:value-of select="$svg-titoli"/></xsl:attribute>
				<fo:block text-align="start">
					<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">CONSUMI E CONGUAGLI</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row keep-with-previous="always" height="1mm">
			<fo:table-cell number-columns-spanned="5">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row keep-with-previous="always" height="3mm">
			<fo:table-cell padding-left="3mm">
				<fo:block>
					<fo:inline>PERIODO</fo:inline>
				</fo:block>
			</fo:table-cell>
									
			<fo:table-cell>
				<fo:block>
					<fo:inline>FASCIA</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>TIPOLOGIA</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>CONSUMI (kWh)</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>CONGUAGLIO (kWh)</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:if>

<xsl:if test="$tipo='body'">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" text-align="start">
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<xsl:for-each select="./child::CONSUMO">
			<fo:table-row>
				<fo:table-cell padding-left="3mm">
					<fo:block>
						<fo:inline>
							<xsl:value-of select="@DESCRIZIONE_MESE_RIFERIMENTO"/>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::FASCIA">
								<fo:inline>
									<xsl:value-of select="./child::FASCIA"/>
								</fo:inline>
							</xsl:when>
							<xsl:otherwise>
								<fo:inline>-</fo:inline>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block>
						<fo:inline>
							<xsl:value-of select="./child::TIPOLOGIA"/>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::CONSUMO='0,00'">
								<fo:inline>-</fo:inline>
							</xsl:when>
							<xsl:otherwise>
								<fo:inline>
									<xsl:value-of select="./child::CONSUMO"/>
								</fo:inline>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::CONGUAGLIO='0,00'">
								<fo:inline>-</fo:inline>
							</xsl:when>
							<xsl:otherwise>
								<fo:inline>
									<xsl:value-of select="./child::CONGUAGLIO"/>
								</fo:inline>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:for-each>
		
		
		<fo:table-row keep-with-previous="always" height="2mm">
			<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="brd.b.000">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		
		<fo:table-row keep-with-previous="always">
			<fo:table-cell number-columns-spanned="5">
				<fo:block>
					<fo:inline>Il ricalcolo di eventuali conguagli viene effettuato utilizzando i prezzi vigenti al momento del consumo.</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:if>
</xsl:template>

</xsl:stylesheet>