<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" />
	
	
	
	<xsl:include href="Comunicazioni_promo_top.xsl"/>
	

	<xsl:template name="FINALE_012011">
	
	<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:param>
	
	<xsl:param name="comunicazione_area_clienti">
		<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI']">SI</xsl:if>
		<xsl:if test="not(./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI'])">NO</xsl:if>
	</xsl:param>
	
	<xsl:param name="color"/>
	
	<xsl:param name="svg-titoli"/>
	
	<xsl:param name="f3_color">
		<xsl:if test="$color='black'">
			#DDDDDD
		</xsl:if>
		<xsl:if test="not($color='black')">
			#fdd17e
		</xsl:if>
	</xsl:param>
	<xsl:param name="f2_color">
		<xsl:if test="$color='black'">
			#777777
		</xsl:if>
		<xsl:if test="not($color='black')">
			#f9b200
		</xsl:if>
	</xsl:param>
	<xsl:param name="f1_color">
		<xsl:if test="$color='black'">
			#FFFFFF
		</xsl:if>
		<xsl:if test="not($color='black')">
			#FFFFFF
		</xsl:if>
	</xsl:param>
	
	
	<xsl:param name="bordi"/>
	
		<!--
			Tutto il contenuto è racchiuso in un blocco con un valore
			orphans molto alto, in questo modo tutti i block figli
			ereditano questo attributo, che fa in modo che di fatto
			un blocco (paragrafo) non sia mai diviso tra due pagine 
		 -->
		<fo:block widows="3" orphans="3">
			
			<!-- Blocco padre per impostazioni "globali" -->
			<fo:block xsl:use-attribute-sets="font.table">
			
				<!-- Riepilogo fattura -->
				<xsl:call-template name="TEMPLATE_RIEPILOGO_FATTURA"/>
				
				<!-- Elenco fatture in attesa di pagamento -->
				<xsl:if test="FATTURE_ATTESA_PAGAMENTO">
					<fo:table table-layout="fixed" width="100%" space-before="5mm">
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(20)" />
						
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block>
										ELENCO FATTURE IN ATTESA DI PAGAMENTO
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row text-align="center">
								<fo:table-cell>
									<fo:block>
										NUMERO FATTURA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA EMISSIONE
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA SCADENZA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DOVUTO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO PAGATO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DA PAGARE
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
				
						<fo:table-body>
							<xsl:for-each select="FATTURE_ATTESA_PAGAMENTO">
								<fo:table-row text-align="center">
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="NUMERO_FATTURA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_EMISSIONE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_SCADENZA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="TOTALE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="IMPORTO_PAGATO" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="INSOLUTO" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
				
							<fo:table-row keep-with-previous="always" height="1mm">
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block />
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row keep-with-previous="always">
								<fo:table-cell number-columns-spanned="5">
									<fo:block />
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block text-align="center" font-size="8pt" font-family="universcbold">
										<xsl:value-of select="TOTALE_FATTURE_ATTESA_PAGAMENTO" /> euro
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					
				</xsl:if>
			</fo:block>
			
			


			
	
								
				<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(100)"/>
					
					<!-- <fo:table-header>
						<fo:table-row height="10mm">
							<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
								</xsl:if>
								<fo:block font-family="universcbold" font-size="11pt" xsl:use-attribute-sets="brd.b.000">
									COMUNICAZIONI AI CLIENTI
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header> -->
					
					<fo:table-body end-indent="0pt" start-indent="0pt">
						<fo:table-row height="3mm">
							<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
								</xsl:if>
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
						
						<xsl:if test="count(PROMO_SERVIZI[@TIPOLOGIA='COM_ISTITUZIONALI']) &gt; 0">
						<fo:table-row>
						<fo:table-cell>
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
								</xsl:if>
								
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									
									<fo:table-column column-width="proportional-column-width(100)"/>
									
									<fo:table-header>
										<fo:table-row height="6mm">
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
												
												<fo:block font-family="universcbold" font-size="11pt" xsl:use-attribute-sets="brd.b.000">
													<xsl:if test="not($color='black')">
														<xsl:attribute name="color">#0065ae</xsl:attribute>
													</xsl:if>
													COMUNICAZIONI ISTITUZIONALI
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-header>
									
									<fo:table-body>
										<fo:table-row>
										
										<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center" padding-bottom="1mm">
											
												<fo:block xsl:use-attribute-sets="font.comunicazioni" margin-left="2mm" margin-right="2mm">
													<xsl:variable name="dataDoc"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2),substring(DATA_DOCUMENTO,1,2))"/></xsl:variable>
												
													<xsl:choose>
														<xsl:when test="($dataDoc &gt;= 20190201) and ($dataDoc  &lt; 20190501)">
															<fo:block space-before="3mm" />
																<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>PROVA PROVA 123</fo:block>
																<fo:block>Esempio com istituzionale</fo:block>
															
														 </xsl:when>
														 <xsl:when test="($dataDoc &gt;= 20190501) and ($dataDoc  &lt; 20190801)">
															<fo:block space-before="3mm" />
																	<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>PROVA PROVA 123</fo:block>
																<fo:block>Esempio com istituzionale</fo:block>
														 </xsl:when>
														 <xsl:when test="($dataDoc &gt;= 20190801) and ($dataDoc  &lt; 20191101)">
															<fo:block space-before="3mm" />
																	<fo:block space-before="3mm" font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>PROVA PROVA 123</fo:block>
																	<fo:block>Esempio com istituzionale</fo:block>
														 </xsl:when>	 
														 <xsl:otherwise>
														 				<fo:block>
														 				</fo:block>
														 </xsl:otherwise>
													 </xsl:choose>							
												</fo:block>
												
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
						</fo:table-row>
						</xsl:if>
						
						<fo:table-row keep-with-previous="always">
							<fo:table-cell>
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
								</xsl:if>
								
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									
									<fo:table-header>
										<fo:table-row height="6mm">
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
												
												<fo:block font-family="universcbold" font-size="11pt" xsl:use-attribute-sets="brd.b.000">
													<xsl:if test="not($color='black')">
														<xsl:attribute name="color">#0065ae</xsl:attribute>
													</xsl:if>
													COMUNICAZIONI ENERGIT
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-header>
									
									
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<fo:table-row>
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center" padding-bottom="1mm">
											
												<fo:block xsl:use-attribute-sets="font.comunicazioni" margin-left="2mm" margin-right="2mm">
													
													
													<!-- Frase "perchè pagare entro la data di scadenza..." -->
													<fo:block space-before="5mm" >
														<fo:block font-family="universcbold" >
															<xsl:if test="not($color='black')">
																<xsl:attribute name="color">#0065ae</xsl:attribute>
															</xsl:if>
															Perché pagare entro la data di scadenza
														</fo:block>
										
														<fo:block keep-with-previous="always">
															La fattura deve essere pagata per intero entro la data di scadenza per evitare i costi previsti dalla procedura di costituzione in mora, disciplinati ai sensi del D. Lgs. 231/2002 e calcolati in base al tasso di interesse definito sul Contratto, fatto salvo il diritto di Energit al risarcimento del maggior danno. Tale procedura è regolata dall’ARERA ai sensi dell’articolo 4 della Delibera 258/2015/R/com (TIMOE). 
														</fo:block>
													</fo:block>

																				
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															<xsl:if test="not($color='black')">
																<xsl:attribute name="color">#0065ae</xsl:attribute>
															</xsl:if>
															Comunicazione dei dati catastali
														</fo:block>
																									
														<fo:block keep-with-previous="always">
															<fo:inline font-family="universcbold">
															Non risulta ancora pervenuta la sua comunicazione dei dati catastali
															identificativi dell'immobile/terreno presso cui è attiva la sua
															fornitura di energia</fo:inline>. Le ricordiamo che la
															responsabilità dell'invio di questi dati, che saranno poi trasmessi
															all'Anagrafe tributaria, è a Suo carico.
															<fo:inline font-family="universcbold">
																La invitiamo a compilare subito l'apposito modulo
																che trova nella sua Area Clienti
															</fo:inline>
															e inviarlo ad Energit! 
														</fo:block>
													</xsl:if>
													
													
																										
													<!--COMUNICAZIONE ORA LEGALE (RFC 252375)-->

													
													
													<xsl:for-each select="PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO']">
														<xsl:if test="./ancestor::DOCUMENTO/child::CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  ./ancestor::DOCUMENTO/child::RINNOVI or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or 
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
																	  position()&gt;1 or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
														   <fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block widows="10" orphans="10">
														<xsl:if test="./child::FRASE='KO'">
															<fo:inline font-family="universcbold">
															<xsl:if test="not($color='black')">
																<xsl:attribute name="color">#0065ae</xsl:attribute>
															</xsl:if>
															BONUS ELETTRICO</fo:inline>:
															Con riferimento al contratto
															<xsl:value-of select="./child::NOTE"/>,
															la informiamo che la richiesta di ammissione
															della sua fornitura alla compensazione della
															spesa per la fornitura di energia elettrica è
															stata rigettata. Per maggiori informazioni la
															invitiamo a contattare il suo comune di residenza.
														</xsl:if>
														<xsl:if test="not(./child::FRASE='KO')">
															<fo:inline font-family="universcbold">
															<xsl:if test="not($color='black')">
																<xsl:attribute name="color">#0065ae</xsl:attribute>
															</xsl:if>
															BONUS ELETTRICO</fo:inline>:
															Con riferimento al contratto
															<xsl:value-of select="./child::NOTE"/>,
															la informiamo che la sua fornitura è ammessa alla
															compensazione della spesa per la fornitura di energia
															elettrica ai sensi del decreto ministeriale 28 dicembre
															2007. La richiesta di rinnovo deve essere effettuata
															entro <xsl:value-of select="./child::FRASE"/>.
														</xsl:if>
														</fo:block>
													</xsl:for-each>
													
													<xsl:if test="$comunicazione_area_clienti='SI'">
													
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															<xsl:if test="not($color='black')">
																<xsl:attribute name="color">#0065ae</xsl:attribute>
															</xsl:if>
															UN MONDO DI NUOVI SERVIZI DA SCOPRIRE
															NELL’AREA CLIENTI ENERGIT!
														</fo:block>
														
														<fo:block font-family="universcbold" keep-with-previous="always">
															Scopra tutte le novità dell’Area Clienti su www.energit.it!
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Questo prezioso strumento sapra’ stupirla con un mondo
															di informazioni, servizi e tanto altro ancora.
															Nella sua Area Clienti potrà, per esempio:
														</fo:block>
														
														<fo:block start-indent="5mm" font-family="universcbold" keep-with-previous="always">
															<fo:block>
																<fo:inline> - Avere accesso a promozioni esclusive</fo:inline>
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Visualizzare i consumi di energia, anche nel dettaglio</fo:inline>
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Trovare utili suggerimenti per risparmiare</fo:inline>
															</fo:block>
															
															<xsl:if test="AUTOLETTURA">
																<fo:block keep-with-previous="always">
																	<fo:inline> - Comunicare la lettura del contatore</fo:inline>
																</fo:block>
															</xsl:if>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Consultare le fatture e verificare lo stato dei pagamenti</fo:inline>
															</fo:block>
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS']">
																<fo:block keep-with-previous="always">
																	<fo:inline> - Passare alla fattura via email per contribuire alla difesa dell’ambiente</fo:inline>
																</fo:block>
															</xsl:if>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Scaricare utile modulistica</fo:inline>
															</fo:block>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Acceda subito all’Area Clienti su www.energit.it attraverso la sua
															<fo:inline font-family="universcbold">UserID </fo:inline>
															<xsl:value-of select="USERNAME" /> : scoprirà un modo nuovo
															e ancora più efficiente per comunicare con Energit.
														</fo:block>
														
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='REGISTRAZIONE_DATI_V1' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA'or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA_V1' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='BETA' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='APPUNTAMENTI_IN_SEDE'
																  ">
													
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
																	  $comunicazione_area_clienti='SI' or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															
															
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
															
														</xsl:if>
													</xsl:if>
														
														<!-- GUIDA ALLA LETTURA DELLA FATTURA -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1'">
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																NOVITA’: LA GUIDA ALLA LETTURA DELLA FATTURA ENERGIT NELLA SUA AREA CLIENTI
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Desidera sapere tutto sulla fattura Energit?
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline font-family="universcbold">Acceda alla sua Area Clienti
																su www.energ.it tramite la sua
																UserID <xsl:value-of select="USERNAME" /> e la password: potrà
																scaricare la Guida alla lettura della fattura Energit</fo:inline>, che
																contiene tante utili informazioni e risposte alle sue domande.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Se non ricorda la password o non ne è ancora in possesso, segua le
																indicazioni che troverà facendo click sul link “Hai smarrito la password?”
																su www.energit.it 
															</fo:block>
															
															<fo:block font-family="universcbold">
																La aspettiamo in Area Clienti!
															</fo:block>
														</xsl:if>
														
														<!-- COMUNICAZIONE PER MANDATO CONNESSIONE -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1'">
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1'">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
															
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																Desidera effettuare nuovi allacci, aumenti o riduzioni di potenza,
																altre modifiche? Siamo a sua disposizione!
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Le ricordiamo
																che <fo:inline font-family="universcbold"> la modulistica per le sue
																richieste di connessione alla rete è sempre disponibile nella sua Area
																Clienti</fo:inline> Energit su www.energit.it, alla quale potrà accedere
																tramite la sua UserID <xsl:value-of select="USERNAME" /> e la password.
															</fo:block>
														</xsl:if>
														
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2'">
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1'">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
															
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																Desidera effettuare nuovi allacci, aumenti o riduzioni di potenza,
																altre modifiche? Siamo a sua disposizione!
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Le ricordiamo
																che <fo:inline font-family="universcbold"> la modulistica per le sue richieste
																di connessione alla rete e le informazioni sugli importi richiesti sono disponibili
																nella sua Area Clienti</fo:inline> Energit su www.energit.it, alla quale potrà
																accedere tramite la sua UserID <xsl:value-of select="USERNAME" /> e la password.
															</fo:block>
														</xsl:if>
														
														<!-- REGISTRAZIONE DATI -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='REGISTRAZIONE_DATI_V1'">
															
															<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::EMAIL) or not(./child::ANAGRAFICA_DOCUMENTO/child::CELLULARE)">
																<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1' or
																			  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1' or
																			  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2'">
																	<fo:block xsl:use-attribute-sets="block.separatore">
																		&#160;
																	</fo:block>
																</xsl:if>
															</xsl:if>
															
															<!-- EMAIL e CELL MANCANTI -->
															<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::EMAIL) and not(./child::ANAGRAFICA_DOCUMENTO/child::CELLULARE)">
																
																<fo:block font-family="universcbold">
																	<xsl:if test="not($color='black')">
																		<xsl:attribute name="color">#0065ae</xsl:attribute>
																	</xsl:if>
																	Attivi nuovi canali di comunicazione con Energit!
																</fo:block>
																
																<fo:block keep-with-previous="always" font-family="universcbold">
																	La invitiamo a comunicarci il suo indirizzo email e il numero di cellulare,
																	per ricevere rapidamente utili e importanti informazioni
																	relative <xsl:if test="RIEPILOGO_MULTISITO_ENERGIA">ai
																	suoi contratti</xsl:if><xsl:if test="not(RIEPILOGO_MULTISITO_ENERGIA)">al
																	suo contratto</xsl:if>.
																</fo:block>
																
																<fo:block keep-with-previous="always">
																	Invii una richiesta firmata al numero di fax gratuito 800.19.22.55
																	o all’indirizzo energia@energit.it, specificando i dati da registrare
																	e il suo Codice Cliente <xsl:value-of select="CODICE_CLIENTE" />. 
																</fo:block>
															</xsl:if>
															
															<!-- EMAIL MANCANTE CELL PRESENTE -->
															<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::EMAIL) and ./child::ANAGRAFICA_DOCUMENTO/child::CELLULARE">
																
																<fo:block font-family="universcbold">
																	<xsl:if test="not($color='black')">
																		<xsl:attribute name="color">#0065ae</xsl:attribute>
																	</xsl:if>
																	Attivi un nuovo canale di comunicazione con Energit!
																</fo:block>
																
																<fo:block keep-with-previous="always" font-family="universcbold">
																	La invitiamo a comunicarci il suo indirizzo email, per ricevere rapidamente utili
																	e importanti informazioni relative <xsl:if test="RIEPILOGO_MULTISITO_ENERGIA">ai
																	suoi contratti</xsl:if><xsl:if test="not(RIEPILOGO_MULTISITO_ENERGIA)">al
																	suo contratto</xsl:if>.
																</fo:block>
																
																<fo:block keep-with-previous="always">
																	Invii una richiesta firmata al numero di fax gratuito 800.19.22.55
																	o all’indirizzo energia@energit.it, specificando l’email da registrare
																	e il suo Codice Cliente <xsl:value-of select="CODICE_CLIENTE" />. 
																</fo:block>
															</xsl:if>
															
															<!-- EMAIL PRESENTE CELL MANCANTE -->
															<xsl:if test="./child::ANAGRAFICA_DOCUMENTO/child::EMAIL and not(./child::ANAGRAFICA_DOCUMENTO/child::CELLULARE)">
																
																<fo:block font-family="universcbold">
																	<xsl:if test="not($color='black')">
																		<xsl:attribute name="color">#0065ae</xsl:attribute>
																	</xsl:if>
																	Attivi un nuovo canale di comunicazione con Energit!
																</fo:block>
																
																<fo:block keep-with-previous="always" font-family="universcbold">
																	La invitiamo a comunicarci il suo numero di cellulare, per ricevere rapidamente
																	utili e importanti informazioni relative <xsl:if test="RIEPILOGO_MULTISITO_ENERGIA">ai
																	suoi contratti</xsl:if><xsl:if test="not(RIEPILOGO_MULTISITO_ENERGIA)">al
																	suo contratto</xsl:if>.
																</fo:block>
																
																<fo:block keep-with-previous="always">
																	Invii una richiesta firmata al numero di fax gratuito 800.19.22.55
																	o all’indirizzo energia@energit.it, specificando il numero di rete mobile
																	da registrare e il suo Codice Cliente <xsl:value-of select="CODICE_CLIENTE" />.
																</fo:block>
															</xsl:if>
														</xsl:if>
														

<!-- 												<xsl:if test="./child::CONTRATTO_ENERGIA/BONUS_PRODOTTO_CUMULATIVO"> -->
													
<!-- 													<fo:block font-family="universcbold"> -->
<!-- 														<xsl:if test="not($color='black')"> -->
<!-- 															<xsl:attribute name="color">#0065ae</xsl:attribute> -->
<!-- 														</xsl:if> -->
<!-- 														<fo:block xsl:use-attribute-sets="block.separatore"> -->
<!-- 														&#160; -->
<!-- 														</fo:block> -->
<!-- 														Bonus kWh -->
<!-- 													</fo:block> -->
														
<!-- 													<fo:block keep-with-previous="always"> -->
<!-- 														<fo:inline> -->
<!-- 															Finora, a partire dalla prima fatturazione, cumulativamente, le è stato riconosciuto un bonus di <xsl:value-of select="./child::CONTRATTO_ENERGIA/BONUS_PRODOTTO_CUMULATIVO"/> euro.  -->
<!-- 														</fo:inline> -->
<!-- 													</fo:block> -->
													
<!-- 												</xsl:if> -->

												<fo:block font-family="universcbold">
													<xsl:if test="not($color='black')">
														<xsl:attribute name="color">#0065ae</xsl:attribute>
													</xsl:if>
													<fo:block xsl:use-attribute-sets="block.separatore">
													&#160;
													</fo:block>
													Prezzo medio fornitura
												</fo:block>
												
												<xsl:variable name="imponibile_no_altrepartite">
													<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
														<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00'">0</xsl:if>
														<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00')">
															<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE,'.',''),',','')"/>
														</xsl:if>
													</xsl:if>
													<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
														<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00'">0</xsl:if>
														<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00')">
															<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE,'.',''),',','')"/>
														</xsl:if>
														<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
															0
														</xsl:if>
													</xsl:if>
												</xsl:variable>
												
												<xsl:variable name="imponibile_sv">
													<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
														<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00'">0</xsl:if>
														<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00')">
															<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA,'.',''),',','')"/>
														</xsl:if>
													</xsl:if>
													<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
														<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00'">0</xsl:if>
														<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00')">
															<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA,'.',''),',','')"/>
														</xsl:if>
														<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
															0
														</xsl:if>
													</xsl:if>
												</xsl:variable>
												
												<xsl:variable name="consumi_fatturati">
													<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::CONSUMI_FATTURATI"/>
													</xsl:if>
													<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
														<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::CONSUMI_FATTURATI"/>
													</xsl:if>
													<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
														0
													</xsl:if>
												</xsl:variable>
												
													<xsl:variable name="costo_medio">
														<xsl:if test="$imponibile_no_altrepartite &gt; 0 and $consumi_fatturati &gt; 0">
															<xsl:value-of select="round($imponibile_no_altrepartite div $consumi_fatturati) div 100"/>
														</xsl:if>
													</xsl:variable>
													
													<xsl:variable name="costo_medio_sv">
														<xsl:if test="$imponibile_sv &gt; 0 and $consumi_fatturati &gt; 0">
															<xsl:value-of select="round($imponibile_sv div $consumi_fatturati) div 100"/>
														</xsl:if>
													</xsl:variable>
														
													<fo:block keep-with-previous="always">
														<fo:inline>
															Il costo medio unitario relativo ai soli servizi di vendita (spesa per la materia prima energia) è pari a <fo:inline><xsl:value-of select="translate(translate($costo_medio_sv,',',''),'.',',')"/></fo:inline> €/kWh ed è calcolato come rapporto 
															tra i costi per i servizi di vendita al netto dell’IVA e i relativi kWh fatturati.															
														</fo:inline>
														<fo:inline>
															Invece il costo medio unitario di questa bolletta è pari a <fo:inline><xsl:value-of select="translate(translate($costo_medio,',',''),'.',',')"/></fo:inline> €/kWh ed calcolato come rapporto tra l’importo complessivamente dovuto in bolletta e i kWh fatturati, 
															al netto di quanto fatturato nella voce altre partite.														
														</fo:inline>
													</fo:block>


													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1') or
																	  $comunicazione_area_clienti='SI' or
																	  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1' or
																	  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1' or
																	  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2' or
																	  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='REGISTRAZIONE_DATI_V1'">
														   <fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														
																<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI']/child::ID='MIX_FONTI_V3'">
																	<fo:block font-family="universcbold">
																		<xsl:if test="not($color='black')">
																			<xsl:attribute name="color">#0065ae</xsl:attribute>
																		</xsl:if>
																		MIX MEDIO ENERGETICO NAZIONALE
																	</fo:block>

																	<fo:block keep-with-previous="always">
																		Energit, ai sensi di quanto previsto dall’art. 2 del decreto legge 31 luglio 2009, pubblica le informazioni sulla composizione del mix medio nazionale per gli anni 2017 e 2018 e il mix di fonti energetiche primarie utilizzate per la produzione dell'energia elettrica fornita da Energit nello stesso periodo.
																	</fo:block>
																	
																	
																	<fo:block space-before="2mm" keep-with-previous="always">
																		<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																			<fo:table-column column-width="proportional-column-width(20)"/>
																			<fo:table-column column-width="proportional-column-width(40)"/>
																			<fo:table-column column-width="proportional-column-width(40)"/>
																			<fo:table-body end-indent="0pt" start-indent="0pt">
																				<fo:table-row font-family="universcbold">
																					<fo:table-cell text-align="center" display-align="after" border-top-style="solid" border-top-width="thin" border-left-style="solid" border-left-width="thin">
																						<fo:block >
																							FONTI PRIMARIE UTILIZZATE
																						</fo:block>
																					</fo:table-cell>
																					
																					<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:block text-align="center">
																							Composizione del mix medio nazionale utilizzato per la produzione dell’energia elettrica immessa nel sistema elettrico italiano
																						</fo:block>
																					</fo:table-cell>
																					
																					<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:block text-align="center">
																							Composizione del mix medio energetico utilizzato per la produzione dell’energia elettrica venduta da Energit
																						</fo:block>
																					</fo:table-cell>
																					
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell border-bottom-style="solid" border-bottom-width="thin" border-left-style="solid" border-left-width="thin">
																						<fo:block/>
																					</fo:table-cell>
																					<fo:table-cell>
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																							<fo:table-column column-width="proportional-column-width(20)"/>
																							<fo:table-column column-width="proportional-column-width(20)"/>
																							<fo:table-body end-indent="0pt" start-indent="0pt">
																								<fo:table-row font-family="universcbold">
																									<fo:table-cell text-align="center" border-left-style="solid" border-left-width="thin" border-right-style="solid" border-right-width="thin">
																										<fo:block>
																											2017**
																										</fo:block>
																									</fo:table-cell>
																									<fo:table-cell text-align="center" border-left-style="solid" border-left-width="thin" border-right-style="solid" border-right-width="thin">
																										<fo:block>
																											2018*
																										</fo:block>
																									</fo:table-cell>												
																								</fo:table-row>
																							</fo:table-body>
																						</fo:table>
																					</fo:table-cell>																		
																					<fo:table-cell>
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																							<fo:table-column column-width="proportional-column-width(20)"/>
																							<fo:table-column column-width="proportional-column-width(20)"/>
																							<fo:table-body end-indent="0pt" start-indent="0pt">
																								<fo:table-row font-family="universcbold">
																									<fo:table-cell text-align="center" border-left-style="solid" border-left-width="thin" border-right-style="solid" border-right-width="thin">
																										<fo:block>
																											2017**
																										</fo:block>
																									</fo:table-cell>
																									<fo:table-cell text-align="center" border-left-style="solid" border-left-width="thin" border-right-style="solid" border-right-width="thin">
																										<fo:block>
																											2018*
																										</fo:block>
																									</fo:table-cell>																									
																								</fo:table-row>
																							</fo:table-body>
																						</fo:table>
																					</fo:table-cell>
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:block>
																							Fonti rinnovabili
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												36,42%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												40,83%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																					
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												2,17%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												4,0%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																			
																					</fo:table-cell>																																										
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:block >
																							Carbone
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												13,69%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												12,47%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																			
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												20,95%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												19,99%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																			
																					</fo:table-cell>																																										
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:block>
																							Gas naturale
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												42,63%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												39,06%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																			
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												66,11%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												64,33%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																			
																					</fo:table-cell>																																										
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:block>
																							Prodotti petroliferi
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												0,76%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												0,54%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																				
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												1,15%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												0,85%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																				
																					</fo:table-cell>																																										
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:block>
																							Nucleare
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												3,62%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												4,11%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																				
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												5,16%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												5,93%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																				
																					</fo:table-cell>																																										
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:block>
																							Altre fonti
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												2,88%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												2,99%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																				
																					</fo:table-cell>
																					<fo:table-cell text-align="center" xsl:use-attribute-sets="cell.mix_fonti">
																						<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-column column-width="proportional-column-width(20)"/>
																								<fo:table-body end-indent="0pt" start-indent="0pt">
																									<fo:table-row font-family="universcbold">
																										<fo:table-cell text-align="center" border-right-style="solid" border-right-width="thin">
																											<fo:block>
																												4,46%
																											</fo:block>
																										</fo:table-cell>
																										<fo:table-cell text-align="center" >
																											<fo:block>
																												4,90%
																											</fo:block>
																										</fo:table-cell>																									
																									</fo:table-row>
																								</fo:table-body>
																							</fo:table>																				
																					</fo:table-cell>																																										
																				</fo:table-row>																																																												
																			</fo:table-body>
																		</fo:table>
																		* dato pre-consuntivo
																		** dato consuntivo
																	</fo:block>
																</xsl:if>														
														
												</xsl:if>
														

													
    											

        										
        										<xsl:variable name="dataDoc" select="10000 * substring(./child::DATA_DOCUMENTO, 7, 4) + 100 * substring(./child::DATA_DOCUMENTO, 4, 2) + substring(./child::DATA_DOCUMENTO, 1, 2)"	/>
    											
    											<xsl:choose>
	        										<xsl:when test="$dataDoc > 20180101">
			        									<!-- delibera inizio -->
														<fo:block xsl:use-attribute-sets="block.separatore">
															&#160;
														</fo:block>
														<fo:block font-family="universcbold" space-before="1mm" keep-with-previous="always">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																	Comunicazione delibera 97/2018/R/com
																</fo:block>

														<fo:block>
															Le ricordiamo che qualora siano presenti nella presente fattura consumi e/ conguagli maggiori di 24 mesi potrà eccepire la prescrizione breve e avvalersi del diritto di non versare gli importi fatturati per i suddetti mesi, ai sensi della delibera 97/2018/R/com. La sospensione del pagamento è legittimata solo qualora presenterà formale reclamo,  nelle forme previste dall’Autorità di regolazione per Energia Reti e Ambiente.
														</fo:block>
														<fo:block >
															La delibera  97/2018/R/com è disponibile sul sito www.arera.it .
														</fo:block>
		            									<!-- delibera fine -->
        											</xsl:when>
        											<xsl:otherwise>

        											</xsl:otherwise>
    											</xsl:choose>
    											
												<!-- ARERA  -->
												<fo:block xsl:use-attribute-sets="block.separatore">
													&#160;
												</fo:block>
												
												<fo:block xsl:use-attribute-sets="font.comunicazioni" >
													<xsl:variable name="dataDoc"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2),substring(DATA_DOCUMENTO,1,2))"/></xsl:variable>
												
													<xsl:choose>
														<xsl:when test="($dataDoc &gt;= 20190201) and ($dataDoc  &lt; 20190501)">
															<fo:block />
																<fo:block font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>Aggiornamenti tariffari ARERA</fo:block>
																	<fo:block>Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia,
																	 Reti e Ambiente (ARERA) ai sensi delle delibere n. 301/2012/R/eel, 654/2015/R/eel e ARG/elt 107/09, 
																	 soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al primo  trimestre 2019, 
																	 per effetto delle delibere 670/2018/R/eel, 671/2018/R/eel, 673/2018/R/eel, 708/2018/R/eel, 706/2018/R/eel e 711/2018/R/com. 
																	 Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale”.
																	</fo:block>
															
														 </xsl:when>
														 <xsl:when test="($dataDoc &gt;= 20190501) and ($dataDoc  &lt; 20190801)">
															<fo:block />
																	<fo:block font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>Aggiornamenti tariffari ARERA</fo:block>
																		<fo:block>
																			Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia, 
																			Reti e Ambiente (ARERA) ai sensi delle delibere n. 301/2012/R/eel, 654/2015/R/eel e ARG/elt 107/09, 
																			soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al secondo trimestre 2019, 
																			per effetto delle delibere 107/2019/R/com e 109/2019/R/eel. 
																			Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale”.
																		</fo:block>
																	
															
														 </xsl:when>
														 <xsl:when test="($dataDoc &gt;= 20190801) and ($dataDoc  &lt; 20191101)">
															<fo:block />
																	<fo:block font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>Aggiornamenti tariffari ARERA</fo:block>
																	<fo:block>
																		Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia, 
																		Reti e Ambiente (ARERA) ai sensi delle delibere n. 301/2012/R/eel, 654/2015/R/eel e ARG/elt 107/09, 
																		soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al secondo trimestre 2019, 
																		per effetto delle delibere 263/2019/R/eel e 262/2019/R/com. 
																		Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale“..
																	</fo:block>
														 </xsl:when>
														 <xsl:when test="($dataDoc &gt;= 20191101) and ($dataDoc  &lt;= 20201031)">
															<fo:block  font-family="universcbold" font-size="10pt"><xsl:attribute name="color">#0065ae</xsl:attribute>AGGIORNAMENTI TARIFFARI ARERA</fo:block>
															<fo:block>
																	Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia, 
																	Reti e Ambiente (ARERA) ai sensi delle delibere n. 301/2012/R/eel, 654/2015/R/eel e ARG/elt 107/09, 
																	soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al quarto trimestre 2019, 
																	per effetto delle delibere 382/2019/R/com e 383/2019/R/eel. 
																	Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale“.
															</fo:block>
			 											</xsl:when>  
														 <xsl:otherwise>
														 				<fo:block>
														 				</fo:block>
														 </xsl:otherwise>
													 </xsl:choose>							
												</fo:block>
												
												
												</fo:block>
			  						

												
											</fo:table-cell>
										 </fo:table-row>
											 
									</fo:table-body>
								</fo:table>
							
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row height="5mm" keep-with-previous="always">
							<fo:table-cell display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
								</xsl:if>
								<fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
					
					

				</fo:table>
				
				<fo:block>
				
				</fo:block>
				
				<fo:block>
				
				</fo:block>
				
				<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(100)"/>	
					<fo:table-body end-indent="0pt" start-indent="0pt">
						<fo:table-row height="3mm">
							<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
								</xsl:if>
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
							
			
						
						<fo:table-row height="5mm" keep-with-previous="always">
							<fo:table-cell display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
								</xsl:if>
								<fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
					

				</fo:table>

			
		
                
                
			
			
			
		</fo:block>
	
	</xsl:template>
	
	<xsl:template name="TEMPLATE_RIEPILOGO_FATTURA">
		
		<xsl:variable name="colonne">
			<xsl:if test="@SPOT='NO' or not(@SPOT)">6</xsl:if>
			<xsl:if test="@SPOT='SI'">5</xsl:if>
		</xsl:variable>
		
		
		<xsl:choose>	<!-- controllo per il deposito cauzionale -->
			<xsl:when test="./child::DETTAGLIO='1' or boolean(./child::RIEPILOGO_MULTISITO_ENERGIA)">
					<fo:table space-before="5mm" table-layout="fixed" width="100%" xsl:use-attribute-sets="font.table">
						<xsl:if test="@SPOT='NO' or not(@SPOT)">
							<fo:table-column column-width="proportional-column-width(22)"/>
							<fo:table-column column-width="proportional-column-width(13)"/>
							<fo:table-column column-width="proportional-column-width(13)"/>
							<fo:table-column column-width="proportional-column-width(13)"/>
							<fo:table-column column-width="proportional-column-width(13)"/>
							<fo:table-column column-width="proportional-column-width(13)"/>
							<fo:table-column column-width="proportional-column-width(13)"/>
						</xsl:if>
						<xsl:if test="@SPOT='SI'">
							<fo:table-column column-width="proportional-column-width(30)"/>
							<fo:table-column column-width="proportional-column-width(14)"/>
							<fo:table-column column-width="proportional-column-width(14)"/>
							<fo:table-column column-width="proportional-column-width(14)"/>
							<fo:table-column column-width="proportional-column-width(14)"/>
							<fo:table-column column-width="proportional-column-width(14)"/>
						</xsl:if>
					
						<fo:table-header>
							
							<fo:table-row>
								<fo:table-cell xsl:use-attribute-sets="cell.header">
									<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne+1"/></xsl:attribute>
									<fo:block>
										<fo:inline>RIEPILOGO CONTRATTI</fo:inline> <!-- ex RIEPILOGO_FATTURA -->
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
					
							<fo:table-row text-align="center" display-align="center" height="4mm">
								<xsl:if test="@SPOT='SI'">
									<xsl:attribute name="height">5mm</xsl:attribute>
								</xsl:if>
								<fo:table-cell>
									<fo:block>SERVIZIO</fo:block>
								</fo:table-cell>
								
								<xsl:if test="@SPOT='NO' or not(@SPOT)">
									<fo:table-cell>
										<fo:block>N. CONTRATTO</fo:block>
									</fo:table-cell>
								</xsl:if>
					
								<fo:table-cell>
									<fo:block>IMPONIBILE</fo:block>
								</fo:table-cell>
					
								<fo:table-cell>
									<fo:block>IVA</fo:block>
								</fo:table-cell>
					
								<fo:table-cell>
									<fo:block>CODICE IVA</fo:block>
								</fo:table-cell>
					
								<fo:table-cell>
									<fo:block>IMPORTO IVA</fo:block>
								</fo:table-cell>
					
								<fo:table-cell>
									<fo:block>TOTALE</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
					
						<fo:table-body>
						<!-- gfalchi 13/04/2018-->
							<xsl:if test="not(RIEPILOGO_FATTURA)">
									<fo:table-row text-align="center" display-align="center">
										<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI']">
											<xsl:attribute name="height">4mm</xsl:attribute>
										</xsl:if>
										<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI'] and position()&lt;last()">
											<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
										</xsl:if>
										<xsl:if test="position()=last()">
											<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
										</xsl:if>
										<fo:table-cell>
											<fo:block>-</fo:block>
										</fo:table-cell>
										
										<xsl:if test="./ancestor::DOCUMENTO[@SPOT='NO'] or not(./ancestor::DOCUMENTO[@SPOT])">
											<fo:table-cell>
												<fo:block>-</fo:block>
											</fo:table-cell>
										</xsl:if>
						
										<fo:table-cell>
											<fo:block>-</fo:block>
										</fo:table-cell>
						
										<fo:table-cell>
											<fo:block>-</fo:block>
										</fo:table-cell>
						
										<fo:table-cell>
											<fo:block>-</fo:block>
										</fo:table-cell>
						
										<fo:table-cell>
											<fo:block>-</fo:block>
										</fo:table-cell>
						
										<fo:table-cell>
											<fo:block>-</fo:block>
										</fo:table-cell>
									</fo:table-row>
							</xsl:if>	
			
							<xsl:if test="(RIEPILOGO_FATTURA)">
								<xsl:for-each select="RIEPILOGO_FATTURA">
									<fo:table-row text-align="center" display-align="center">
										<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI']">
											<xsl:attribute name="height">4mm</xsl:attribute>
										</xsl:if>
										<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI'] and position()&lt;last()">
											<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
										</xsl:if>
										<xsl:if test="position()=last()">
											<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
										</xsl:if>
										<fo:table-cell>
											<fo:block text-align="left" font-size="6pt">
												<xsl:value-of select="PRODOTTO_SERVIZIO" />
											</fo:block>
										</fo:table-cell>
										
										<xsl:if test="./ancestor::DOCUMENTO[@SPOT='NO'] or not(./ancestor::DOCUMENTO[@SPOT])">
											<fo:table-cell>
												<fo:block>
													<xsl:value-of select="CONTRATTO" />
												</fo:block>
											</fo:table-cell>
										</xsl:if>
						
										<fo:table-cell>
											<fo:block>
												<xsl:value-of select="IMPONIBILE" />
											</fo:block>
										</fo:table-cell>
						
										<fo:table-cell>
											<fo:block>
												<xsl:value-of select="ALIQUOTA_IVA" />%
											</fo:block>
										</fo:table-cell>
						
										<fo:table-cell>
											<fo:block>
												<xsl:choose>
													<xsl:when test="CODICE_IVA">
														<xsl:value-of select="CODICE_IVA" />
													</xsl:when>
													<xsl:otherwise>
														-
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
						
										<fo:table-cell>
											<fo:block>
												<xsl:value-of select="IMPORTO_IVA" />
											</fo:block>
										</fo:table-cell>
						
										<fo:table-cell>
											<fo:block>
												<xsl:value-of select="TOTALE_CON_IVA" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:for-each>
							</xsl:if>
							
							<xsl:if test="not(DESCRIZIONI_CODICI_IVA)">
								<fo:table-row keep-with-previous="always">
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="CODICE_IVA" />
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
										<fo:block>
											<xsl:value-of select="DESCRIZIONE_CODICE_IVA" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>				
							</xsl:if>				
							
							<xsl:if test="DESCRIZIONI_CODICI_IVA">				
								<xsl:for-each select="DESCRIZIONI_CODICI_IVA">
									<fo:table-row keep-with-previous="always">
										<fo:table-cell>
											<fo:block>
												Codice IVA "<xsl:value-of select="CODICE_IVA" />"
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
											<fo:block>
												<xsl:value-of select="DESCRIZIONE_CODICE_IVA" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:for-each>
							</xsl:if>
							
						</fo:table-body>
					</fo:table>				
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
		

	</xsl:template>
	
	
	<xsl:attribute-set name="font.table">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="line-height">9pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.pagare">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="line-height">7pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.header">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	    <xsl:attribute name="font-family">universcbold</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	    <xsl:attribute name="padding-left">3mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border">
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.left">
		<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.right">
		<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.top">
		<xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.bottom">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.left">
		<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.right">
		<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.top">
		<xsl:attribute name="border-top">1pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.bottom">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	 
	<xsl:attribute-set name="font.fascia_header">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">9.5pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia_text">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">6pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia">
	    <xsl:attribute name="font-family">universcbold</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.legenda">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">7pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.comunicazioni">
		<xsl:attribute name="font-family">universc</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.sepa">
		<xsl:attribute name="font-family">universc</xsl:attribute>
		<xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.title">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="line-height">4mm</xsl:attribute>
		<xsl:attribute name="space-after">4mm</xsl:attribute>
		<xsl:attribute name="font-size">11pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.separatore">
		<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="space-after">1mm</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.rinnovi">
		<xsl:attribute name="padding">0.5mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.mix_fonti">
		<xsl:attribute name="padding">0.1mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.articoli">
		<xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
		<xsl:attribute name="font-style">italic</xsl:attribute>
	</xsl:attribute-set>

</xsl:stylesheet>

