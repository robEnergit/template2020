<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" />
	
	

	<xsl:template name="COMUNICAZIONI_PROMO_TOP">
	<xsl:param name="color"/>
	
	<!--
	*******************************************
	Top3000 comunicazione e invito all'adesione
	*******************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE1'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			ENERGIT PREMIA I 3.000 MIGLIORI CLIENTI CON UNO
			<fo:inline font-size="12pt">SCONTO</fo:inline>
			ESCLUSIVO
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Siamo lieti di comunicarle che lei rientra tra i
			<fo:inline font-family="universcbold">3.000 migliori Clienti Energit!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per ringraziarla di aver scelto la nostra azienda le abbiamo riservato lo 
			<fo:inline font-family="universcbold">speciale sconto “Top3000” del valore
			di 9 € per ogni megawattora consumato</fo:inline>, che sarà applicato
			nei mesi di gennaio e dicembre 2011 ai consumi di tutte
			le sue utenze attive ininterrottamente fino a dicembre 2011.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Si assicuri subito questo speciale bonus: chiami il nostro Servizio Clienti al numero
			<fo:inline font-family="universcbold">800.19.22.22</fo:inline>
			da lunedì a venerdì (8.30 - 18.00) e comunichi il
			<fo:inline font-family="universcbold">codice promozione 3000</fo:inline>: i nostri
			operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Top3000" anche attraverso l'
			<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet
			<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per aderire a questa speciale iniziativa ha tempo sino al
			<fo:inline font-family="universcbold">31 gennaio 2011</fo:inline>.
		</fo:block>
		
		<fo:block font-family="universcbold" keep-with-previous="always">
			Grazie ancora per aver scelto Energit!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2011, i bonus eventualmente già erogati verranno stornati.
		</fo:block>
	</xsl:if>
	
	<!--
	*********************
	Top3000 post adesione
	*********************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE2'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI: LEI RICEVERA' LO SCONTO TOP3000, RISERVATO AI MIGLIORI CLIENTI!
		</fo:block>
	
		<fo:block space-before="2mm" keep-with-previous="always">
			Gentile Cliente,
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			la sua adesione all’iniziativa Top3000, riservata ai 3000 migliori clienti
			Energit, è stata registrata.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Lo <fo:inline font-family="universcbold">speciale sconto “Top3000”,
			del valore di 9 € per ogni megawattora consumato</fo:inline>, sarà
			applicato nei mesi di gennaio e dicembre 2011 ai consumi di tutte le sue
			utenze attive ininterrottamente fino a dicembre 2011.
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			Grazie per aver scelto Energit!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always" font-size="9pt">
			* In caso di recesso prima del 31/12/2011, i bonus eventualmente già erogati
			verranno stornati.
		</fo:block>
	</xsl:if>
	
	<!--
	*******************************************
	1° Reminder Top3000 PRE ADESIONE non morosi
	*******************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE3'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			RICHIEDA SUBITO LO SPECIALE SCONTO TOP3000 RISERVATO AI NOSTRI MIGLIORI CLIENTI!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Gentile Cliente,
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			non ha ancora approfittato dello speciale sconto “Top3000” riservato ai 3.000
			migliori Clienti Energit.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Lo richieda subito!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			"Top3000", lo sconto del valore di 9 € per ogni megawattora consumato, sarà applicato
			nei mesi di gennaio e dicembre 2011 ai consumi di tutte le sue utenze attive
			ininterrottamente* fino a dicembre 2011.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Si assicuri subito questo speciale bonus: chiami il nostro Servizio Clienti al
			numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
			lunedì a venerdì (8.30 - 18.00) e comunichi
			il <fo:inline font-family="universcbold">codice promozione 3000</fo:inline>:
			i nostri operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Top3000" anche
			attraverso l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del
			nostro sito Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>,
			inserendo la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Le ricordiamo che per aderire a questa speciale iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 gennaio 2011.</fo:inline>
		</fo:block>
		
		<fo:block space-before="2mm" font-size="9pt" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2011, i bonus eventualmente già
			erogati verranno stornati.
		</fo:block>
		
		<fo:block font-size="9pt" keep-with-previous="always">
			L’erogazione del bonus è subordinata al pieno rispetto delle condizioni generali
			di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	***************************************
	1° Reminder Top3000 PRE ADESIONE morosi
	***************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE4'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			RICHIEDA LO SPECIALE SCONTO TOP3000 RISERVATO AI NOSTRI
			MIGLIORI CLIENTI!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Gentile Cliente,
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			non ha ancora approfittato dello speciale sconto “Top3000” che
			Energit Le ha riservato.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Non perda questa occasione!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			"Top3000", lo sconto del valore di 9 € per ogni megawattora consumato,
			sarà applicato nei mesi di gennaio e dicembre 2011 ai consumi di tutte
			le sue utenze attive ininterrottamente* fino a dicembre 2011 e riservato
			ai Clienti in regola con i pagamenti.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Si assicuri subito questo speciale bonus
			: <fo:inline font-family="universcbold">regolarizzi
			i suoi pagamenti entro 5 giorni</fo:inline> e chiami il nostro Servizio Clienti
			al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
			lunedì a venerdì (8.30 - 18.00), comunicando
			il <fo:inline font-family="universcbold">codice promozione 3000</fo:inline>.
			I nostri operatori saranno a disposizione per registrare la Sua adesione.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Se preferisce, potrà richiedere lo sconto "Top3000" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del nostro sito
			Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>,
			inserendo la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Le ricordiamo che per aderire a questa speciale iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 gennaio 2011.</fo:inline>
		</fo:block>
		
		<fo:block space-before="2mm" font-size="9pt" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2011, i bonus eventualmente già
			erogati verranno stornati.
		</fo:block>
		
		<fo:block font-size="9pt" keep-with-previous="always">
			L’erogazione del bonus è subordinata al pieno rispetto delle condizioni
			generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	*************************************************
	1° Reminder Top3000 - Comunicazione POST ADESIONE
	*************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE5'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI: LEI RICEVERA' LO SCONTO TOP3000, RISERVATO AI MIGLIORI CLIENTI!
		</fo:block>
	
		<fo:block space-before="2mm" keep-with-previous="always">
			Gentile Cliente,
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			la sua adesione all’iniziativa Top3000, riservata ai 3000 migliori clienti
			Energit, è stata registrata.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Lo <fo:inline font-family="universcbold">speciale sconto “Top3000”,
			del valore di 9 € per ogni megawattora consumato</fo:inline>, sarà
			applicato nei mesi di gennaio e dicembre 2011 ai consumi di tutte le sue
			utenze attive ininterrottamente fino a dicembre 2011.
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			Grazie per aver scelto Energit!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always" font-size="9pt">
			* In caso di recesso prima del 31/12/2011, i bonus eventualmente già erogati
			verranno stornati.
		</fo:block>
		
		<fo:block font-size="9pt" keep-with-previous="always">
			L’erogazione del bonus è subordinata al pieno rispetto delle condizioni
			generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	*******************************************
	2° Reminder Top3000 PRE ADESIONE non morosi
	*******************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE6'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			ULTIMI GIORNI PER RICHIEDERE LO SCONTO TOP3000 A LEI RISERVATO!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Gentile Cliente,
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			non ha ancora approfittato dello speciale sconto “Top3000” riservato ai 3.000
			migliori Clienti Energit: si faccia un regalo e lo richieda subito!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			"Top3000", lo sconto del valore di 9 € per ogni megawattora consumato,
			sarà applicato nei mesi di gennaio e dicembre 2011 ai consumi di tutte
			le sue utenze attive ininterrottamente* fino a dicembre 2011.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Si assicuri subito questo speciale bonus: chiami il nostro Servizio
			Clienti al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
			lunedì a venerdì (8.30 - 18.00) e comunichi
			il <fo:inline font-family="universcbold">codice promozione 3000</fo:inline>:
			i nostri operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Top3000" anche
			attraverso l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del
			nostro sito Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>,
			inserendo la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Si affretti! Per aderire a questa speciale iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 gennaio 2011.</fo:inline>
		</fo:block>
		
		<fo:block space-before="2mm" font-size="9pt" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2011, i bonus eventualmente già erogati
			verranno stornati. L’erogazione del bonus è subordinata al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	***************************************
	2° Reminder Top3000 PRE ADESIONE morosi
	***************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE7'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			ULTIMI GIORNI PER RICHIEDERE LO SCONTO TOP3000 A LEI RISERVATO!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Gentile Cliente,
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			non ha ancora approfittato dello speciale sconto “Top3000” che
			Energit Le ha riservato.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Non perda questa occasione!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			"Top3000", lo sconto del valore di 9 € per ogni megawattora consumato,
			sarà applicato nei mesi di gennaio e dicembre 2011 ai consumi di tutte
			le sue utenze attive ininterrottamente* fino a dicembre 2011 e riservato
			ai Clienti in regola con i pagamenti.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Si assicuri subito questo speciale bonus
			: <fo:inline font-family="universcbold">regolarizzi
			i suoi pagamenti entro 5 giorni</fo:inline> e chiami il nostro Servizio
			Clienti al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
			lunedì a venerdì (8.30 - 18.00), comunicando
			il <fo:inline font-family="universcbold">codice promozione 3000</fo:inline>.
			I nostri operatori saranno a disposizione per registrare la Sua adesione.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Se preferisce, potrà richiedere lo sconto "Top3000" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del nostro sito
			Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>,
			inserendo la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Si affretti! Per aderire a questa speciale iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 gennaio 2011.</fo:inline>
		</fo:block>
		
		<fo:block space-before="2mm" font-size="9pt" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2011, i bonus eventualmente già
			erogati verranno stornati. L’erogazione del bonus è subordinata al pieno
			rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	**************************************************************
	Aggiornamento comunicazione in fattura Top3000 (post adesione)
	**************************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE8'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI: LEI RICEVE LO SCONTO TOP3000 RISERVATO AI MIGLIORI CLIENTI!
		</fo:block>
	
		<fo:block space-before="2mm" keep-with-previous="always">
			Le ricordiamo che l’applicazione dello <fo:inline font-family="universcbold">speciale
			sconto “Top3000”, del valore di 9 € per ogni megawattora consumato</fo:inline>, è
			prevista nei mesi di gennaio e dicembre 2011 sui consumi di tutte le sue utenze
			attive ininterrottamente* fino a dicembre 2011.
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			Grazie per aver scelto Energit!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always" font-size="9pt">
			* In caso di recesso prima del 31/12/2011, i bonus eventualmente già
			erogati verranno stornati.
		</fo:block>

		<fo:block font-size="9pt" keep-with-previous="always">
			L’erogazione del bonus è subordinata al pieno rispetto delle condizioni
			generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	*******************************
	Top5000 PRE ADESIONE non morosi
	*******************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE12'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			ENERGIT PREMIA I 5.000 MIGLIORI CLIENTI CON
			UNO <fo:inline font-size="12pt">SCONTO</fo:inline>
			ESCLUSIVO
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Siamo lieti di comunicarle che lei rientra tra
			i <fo:inline font-family="universcbold">5.000 migliori Clienti Energit!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per ringraziarla di aver scelto la nostra azienda le abbiamo riservato
			lo <fo:inline font-family="universcbold">speciale sconto “Top5000” del
			valore di 6 € per ogni megawattora consumato</fo:inline>, che sarà
			applicato nei mesi di luglio 2011, gennaio 2012 e dicembre 2012 ai
			consumi di tutte le sue utenze attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Si assicuri subito questo speciale bonus: chiami il nostro Servizio Clienti al
			numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
			lunedì a venerdì (8.30 - 18.00) e comunichi
			il <fo:inline font-family="universcbold">codice promozione 5000</fo:inline>: i nostri
			operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Top5000" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet
			<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per aderire a questa speciale iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 luglio 2011</fo:inline>.
		</fo:block>
		
		<fo:block font-family="universcbold" keep-with-previous="always">
			Grazie ancora per aver scelto Energit!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già
			erogati verranno stornati. L’erogazione del bonus è subordinata al
			pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	***************************
	Top5000 PRE ADESIONE morosi
	***************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE13'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			PER LEI LO SPECIALE <fo:inline font-size="12pt">SCONTO</fo:inline> TOP5000: SCOPRA COME OTTENERLO!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Abbiamo il piacere di comunicarle che lei rientra tra
			i <fo:inline font-family="universcbold">5.000 fortunati Clienti Energit
			selezionati per un’eccezionale iniziativa!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per ringraziarla di aver scelto la nostra azienda le abbiamo dedicato
			lo <fo:inline font-family="universcbold">speciale sconto “Top5000”</fo:inline>,
			riservato ai Clienti in regola con i pagamenti
			, <fo:inline font-family="universcbold">del valore di 6 € per ogni megawattora
			consumato</fo:inline>, che sarà applicato nei mesi di luglio 2011, gennaio 2012
			e dicembre 2012 ai consumi di tutte le sue utenze attive ininterrottamente* fino
			a dicembre 2012.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Si assicuri subito questo speciale bonus: regolarizzi i suoi pagamenti, chiami il
			nostro Servizio Clienti, gratuito da rete fissa, al
			numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì
			a venerdì (8.30 - 18.00) e comunichi
			il <fo:inline font-family="universcbold">codice promozione 5000</fo:inline>: i nostri
			operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Top5000" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet
			<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per aderire a questa speciale iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 luglio 2011</fo:inline>.
		</fo:block>
		
		<fo:block font-family="universcbold" keep-with-previous="always">
			Grazie ancora per aver scelto Energit!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già
			erogati verranno stornati. L’erogazione del bonus è subordinata al
			pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	**************************************************************************
	Top5000 - Comunicazione POST ADESIONE (prima dell'inizio della promozione)
	**************************************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE14'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI: LEI RICEVERA' LO SCONTO TOP5000, RISERVATO AI MIGLIORI CLIENTI!
		</fo:block>
	
		<fo:block space-before="2mm" keep-with-previous="always">
			Gentile Cliente,
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			la sua adesione all’iniziativa Top5000, riservata ai 5000 migliori
			clienti Energit, è stata registrata.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Lo <fo:inline font-family="universcbold">speciale sconto “Top5000”,
			del valore di 6 € per ogni megawattora consumato</fo:inline>, sarà
			applicato nei mesi di luglio 2011, gennaio 2012 e dicembre 2012 ai
			consumi di tutte le sue utenze attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			Grazie per aver scelto Energit!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always" font-size="9pt">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati
			verranno stornati. L’erogazione del bonus è subordinata al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	**********************************************************************
	Top5000 - Comunicazione POST ADESIONE (dopo l'inizio della promozione)
	**********************************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE15'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI, LEI RICEVE LO SCONTO TOP5000 RISERVATO AI MIGLIORI CLIENTI!
		</fo:block>
	
		<fo:block space-before="2mm" keep-with-previous="always">
			Le ricordiamo che l’applicazione
			dello <fo:inline font-family="universcbold">speciale sconto “Top5000”
			del valore di 6 € per ogni megawattora consumato</fo:inline>, è prevista
			nei mesi di luglio 2011, gennaio 2012 e dicembre 2012 sui consumi di tutte
			le sue utenze attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			Buon risparmio!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always" font-size="9pt">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati
			verranno stornati. L’erogazione del bonus è subordinata al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--Riga di separazione se coesistono TOP3000 e Customer List-->
	<!-- <xsl:if test="(./child::ID='COMUNICAZIONE16' or 
				  ./child::ID='COMUNICAZIONE17' or 
				  ./child::ID='COMUNICAZIONE18' or 
				  ./child::ID='COMUNICAZIONE19' or
				  ./child::ID='COMUNICAZIONE39' or 
				  ./child::ID='COMUNICAZIONE40')
				  and
				  ./child::ID='COMUNICAZIONE8'
				 ">
		<fo:block xsl:use-attribute-sets="block.separatore">
			&#160;
		</fo:block>
	</xsl:if> -->
	
	<!--
	*************************************
	Customer List PRE ADESIONE non morosi
	*************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE16'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			ENERGIT LE HA RISERVATO LO SPECIALE SCONTO MENO24!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Complimenti!</fo:inline> Da tanti
			anni ha scelto la fornitura Energit, contribuendo al nostro successo nel
			mercato dell’energia elettrica.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per ringraziarla le abbiamo <fo:inline font-family="universcbold">riservato
			lo speciale sconto “Meno24”</fo:inline> del valore di 24 € per ogni megawattora
			consumato, che sarà applicato nei mesi di luglio 2011, gennaio 2012 e dicembre
			2012 ai consumi di tutte le sue utenze attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Si assicuri subito questo speciale bonus: chiami il nostro Servizio Clienti al
			numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
			lunedì a venerdì (8.30 - 18.00) e comunichi
			il <fo:inline font-family="universcbold">codice promozione 24</fo:inline>; i nostri
			operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Meno24" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet
			<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per aderire a questa speciale iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 luglio 2011</fo:inline>.
		</fo:block>
		
		<fo:block font-family="universcbold" keep-with-previous="always">
			Grazie ancora per aver scelto Energit!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati
			verranno stornati. L’erogazione del bonus è subordinata al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	*********************************
	Customer List PRE ADESIONE morosi
	*********************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE17'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			PER LEI LO SPECIALE SCONTO MENO24: SCOPRA COME OTTENERLO!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Complimenti!</fo:inline> Da tanti
			anni ha scelto la fornitura Energit, contribuendo al nostro successo nel
			mercato dell’energia elettrica.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per ringraziarla le abbiamo <fo:inline font-family="universcbold">riservato
			lo speciale sconto “Meno24”</fo:inline> del valore di 24 € per ogni megawattora
			consumato e riservato ai Clienti in regola con i pagamenti, che sarà applicato
			nei mesi di luglio 2011, gennaio 2012 e dicembre 2012 ai consumi di tutte le sue
			utenze attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Si assicuri subito questo speciale bonus: regolarizzi i suoi pagamenti, chiami il
			nostro Servizio Clienti al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
			lunedì a venerdì (8.30 - 18.00) e comunichi il <fo:inline font-family="universcbold">codice
			promozione 24</fo:inline>; i nostri operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Meno24" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet
			<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per aderire a questa speciale iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 luglio 2011</fo:inline>.
		</fo:block>
		
		<fo:block font-family="universcbold" keep-with-previous="always">
			Grazie ancora per aver scelto Energit!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati
			verranno stornati. L’erogazione del bonus è subordinata al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	********************************************************************************
	Customer List - Comunicazione POST ADESIONE (prima dell'inizio della promozione)
	********************************************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE18'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI: LEI RICEVERA’ LO SPECIALE SCONTO MENO24!
		</fo:block>
	
		<fo:block space-before="2mm" keep-with-previous="always">
			Gentile Cliente,
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			la sua adesione all’iniziativa Meno24 è stata registrata.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Lo <fo:inline font-family="universcbold">speciale sconto “Meno24”,
			del valore di 24 € per ogni megawattora consumato</fo:inline>, sarà applicato
			nei mesi di luglio 2011, gennaio 2012 e dicembre 2012 ai consumi di tutte le
			sue utenze attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			Grazie per aver scelto Energit!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always" font-size="9pt">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati verranno
			stornati. L’erogazione del bonus è subordinata al pieno rispetto delle condizioni
			generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	****************************************************************************
	Customer List - Comunicazione POST ADESIONE (dopo l'inizio della promozione)
	****************************************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE19'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI, LEI RICEVE LO SPECIALE SCONTO MENO24!
		</fo:block>
	
		<fo:block space-before="2mm" keep-with-previous="always">
			Le ricordiamo che l’applicazione
			dello <fo:inline font-family="universcbold">speciale sconto “Meno24”
			del valore di 24 € per ogni megawattora consumato</fo:inline>, è prevista nei
			mesi di luglio 2011, gennaio 2012 e dicembre 2012 sui consumi di tutte le sue
			utenze attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block space-before="2mm" font-family="universcbold" keep-with-previous="always">
			Buon risparmio!
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always" font-size="9pt">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati
			verranno stornati. L’erogazione del bonus è subordinata al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	********************************************
	Top5000 PRE ADESIONE non morosi - 1°REMINDER
	********************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE37'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			RICHIEDA SUBITO LO SPECIALE <fo:inline font-size="12pt">SCONTO</fo:inline> TOP5000: E' UN REGALO DI ENERGIT!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Non ha ancora approfittato
			dello <fo:inline font-family="universcbold">speciale sconto “Top5000”</fo:inline> che Energit le ha
			riservato? <fo:inline font-family="universcbold">Lo richieda subito!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			“Top5000”, lo <fo:inline font-family="universcbold">sconto del valore di 6 € per ogni megawattora
			consumato</fo:inline>, sarà applicato nei mesi di luglio 2011, gennaio 2012 ai consumi di tutte le sue utenze
			attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Si assicuri subito questo speciale bonus</fo:inline>: chiami il nostro Servizio Clienti al
			numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì
			a venerdì (8.30 - 18.00) e comunichi il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" /> e
			il Codice promozione 5000; i nostri operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Top5000" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet
			<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per aderire a questa straordinaria iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 luglio 2011</fo:inline>.
		</fo:block>
		
		<fo:block font-family="universcbold" keep-with-previous="always">
			Grazie ancora per aver scelto Energit!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati verranno stornati.
			L’erogazione del bonus è subordinata al pieno rispetto delle condizioni generali di contratto
			da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	****************************************
	Top5000 PRE ADESIONE morosi - 1°REMINDER
	****************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE38'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			PER LEI LO SPECIALE <fo:inline font-size="12pt">SCONTO</fo:inline> TOP5000: SCOPRA COME OTTENERLO!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Non ha ancora approfittato
			dello <fo:inline font-family="universcbold">speciale sconto “Top5000”</fo:inline> offerto da
			Energit? <fo:inline font-family="universcbold">Colga questa occasione!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			“Top5000”, lo <fo:inline font-family="universcbold">sconto del valore di 6 € per ogni megawattora
			consumato</fo:inline>, riservato ai Clienti in regola con i pagamenti,
			sarà applicato nei mesi di luglio 2011, gennaio 2012 ai consumi di tutte le sue utenze
			attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Si assicuri subito questo speciale bonus: regolarizzi
			i suoi pagamenti</fo:inline>, chiami il nostro Servizio Clienti, gratuito da rete fissa, al
			numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì
			a venerdì (8.30 - 18.00) e comunichi il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" /> e
			il Codice promozione 5000; i nostri operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Top5000" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet
			<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per aderire a questa straordinaria iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 luglio 2011</fo:inline>.
		</fo:block>
		
		<fo:block font-family="universcbold" keep-with-previous="always">
			Grazie ancora per aver scelto Energit!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati
			verranno stornati. L’erogazione del bonus è subordinata al pieno rispetto
			delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	**************************************************
	Customer List PRE ADESIONE non morosi - 1°REMINDER
	**************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE39'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			RICHIEDA SUBITO LO SPECIALE <fo:inline font-size="12pt">SCONTO</fo:inline> MENO24: E' UN REGALO DI ENERGIT!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Da tanti anni ha scelto la fornitura Energit, contribuendo al nostro successo nel mercato dell’energia elettrica.
			Per ringraziarla, ancora una volta <fo:inline font-family="universcbold">le abbiamo riservato uno speciale
			sconto: lo richieda subito!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Lo <fo:inline font-family="universcbold">sconto “Meno24”, del valore di 24 € per ogni megawattora
			consumato</fo:inline>, sarà applicato nei mesi di luglio 2011, gennaio 2012 e dicembre 2012 ai consumi di tutte le sue utenze
			attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Si assicuri subito questo speciale bonus</fo:inline>: chiami il nostro Servizio Clienti al
			numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì
			a venerdì (8.30 - 18.00) e comunichi il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" /> e
			il Codice promozione 24; i nostri operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Meno24" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet
			<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per aderire a questa straordinaria iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 luglio 2011</fo:inline>.
		</fo:block>
		
		<fo:block font-family="universcbold" keep-with-previous="always">
			Grazie ancora per aver scelto Energit!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati verranno stornati.
			L’erogazione del bonus è subordinata al pieno rispetto delle condizioni generali di contratto
			da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	**********************************************
	Customer List PRE ADESIONE morosi - 1°REMINDER
	**********************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE40'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			PER LEI LO SPECIALE <fo:inline font-size="12pt">SCONTO</fo:inline> MENO24: SCOPRA COME OTTENERLO!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Da tanti anni ha scelto la fornitura Energit, contribuendo al nostro successo nel mercato dell’energia elettrica.
			Per ringraziarla, ancora una volta <fo:inline font-family="universcbold">le abbiamo riservato uno speciale
			sconto: colga questa occasione!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			L’esclusivo <fo:inline font-family="universcbold">sconto “Meno24”, del valore di 24 € per ogni megawattora
			consumato</fo:inline> e riservato ai Clienti in regola con i pagamenti, sarà applicato nei mesi di luglio 2011,
			gennaio 2012 e dicembre 2012 ai consumi di tutte le sue utenze attive ininterrottamente* fino a dicembre 2012.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Si assicuri subito questo speciale bonus: regolarizzi i suoi pagamenti</fo:inline>,
			chiami il nostro Servizio Clienti al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì
			a venerdì (8.30 - 18.00) e comunichi il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" /> e
			il Codice promozione 24; i nostri operatori saranno a disposizione per registrare la sua adesione.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Se preferisce, può richiedere il suo sconto "Meno24" anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet
			<fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per aderire a questa straordinaria iniziativa ha tempo sino
			al <fo:inline font-family="universcbold">31 luglio 2011</fo:inline>.
		</fo:block>
		
		<fo:block font-family="universcbold" keep-with-previous="always">
			Grazie ancora per aver scelto Energit!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2012, i bonus eventualmente già erogati verranno stornati.
			L’erogazione del bonus è subordinata al pieno rispetto delle condizioni generali di contratto
			da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!--
	*******************************
	Top6000 PRE ADESIONE non morosi
	*******************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE91'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			ENERGIT PREMIA I MIGLIORI CLIENTI CON GLI ESCLUSIVI SCONTI TOP6000
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Siamo lieti di comunicarle che <fo:inline font-family="universcbold">lei rientra tra i migliori Clienti Energit.</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per ringraziarla di aver scelto la nostra
			azienda <fo:inline font-family="universcbold">desideriamo offrirle…tanta
			energia scontata!</fo:inline>
		</fo:block>
		
		<fo:block>
			<fo:inline font-family="universcbold">Le abbiamo infatti riservato gli speciali sconti Top6000</fo:inline>, di valore
			crescente nel tempo e pari a
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:list-block keep-with-previous="always" provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">3 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di luglio 2012</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">6 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di gennaio 2013</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">9 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di dicembre 2013</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
			</fo:list-block>
		</fo:block>
		
		<fo:block>
			<fo:inline font-family="universcbold">Gli sconti saranno applicati ai consumi di tutte le sue utenze</fo:inline> attive
			ininterrottamente* fino a dicembre 2013.
		</fo:block>
		
		<fo:block space-before="2mm">
			Per ottenere i suoi sconti dovrà solo richiederli: chiami il nostro Servizio Clienti,
			gratuito da rete fissa, al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì a venerdì
			(8.30 - 18.00); non appena la voce guida richiederà l’inserimento
			di un <fo:inline font-family="universcbold">codice promozione</fo:inline>, digiti il
			numero <fo:inline font-family="universcbold">6000</fo:inline> sulla tastiera del telefono e
			sarà subito in contatto con i nostri operatori, pronti a registrare la sua adesione all’iniziativa!
			Non dimentichi di indicare il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" />.
		</fo:block>
		
		<fo:block font-family="universcbold" space-before="2mm">
			Se preferisce, potrà richiedere i suoi sconti Top6000 anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del nostro sito
			Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Per aderire all’iniziativa ha tempo sino al 31 luglio 2012, ma perché aspettare? Conquisti subito i suoi sconti!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2013, gli sconti eventualmente già erogati verranno stornati.
			L’erogazione degli sconti è subordinata al pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	
	
	
	
	
	
	
	
	
	<!--
	*******************************
	Top6000 PRE ADESIONE non morosi
	*******************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE111'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			ENERGIT PREMIA I MIGLIORI CLIENTI CON GLI ESCLUSIVI SCONTI TOP6000: LI RICHIEDA ENTRO IL 31 LUGLIO 2012!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Non ha ancora richiesto gli <fo:inline font-family="universcbold">speciali sconti Top6000,
			riservati ai nostri migliori Clienti: li conquisti subito!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Le ricordiamo che gli sconti, <fo:inline font-family="universcbold">di valore crescente nel tempo</fo:inline> e pari a
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:list-block keep-with-previous="always" provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">3 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di luglio 2012</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">6 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di gennaio 2013</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">9 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di dicembre 2013</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
			</fo:list-block>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			saranno applicati ai consumi di tutte le sue utenze attive ininterrottamente* fino a dicembre 2013.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Per ottenere i suoi sconti chiami il nostro Servizio Clienti,
			gratuito da rete fissa, al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì a venerdì (8.30 - 18.00);
			non appena la voce guida richiederà l’inserimento di un <fo:inline font-family="universcbold">codice promozione</fo:inline>, digiti il
			numero <fo:inline font-family="universcbold">6000</fo:inline> sulla tastiera del telefono e
			sarà subito in contatto con i nostri operatori, pronti a registrare la sua adesione all’iniziativa!
			Non dimentichi di indicare il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" />.
		</fo:block>
		
		<fo:block font-family="universcbold" space-before="2mm" keep-with-previous="always">
			Se preferisce, potrà richiedere i suoi sconti Top6000 anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del nostro sito
			Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always" font-family="universcbold">
			Approfitti subito di questa eccezionale occasione valida solo fino al 31 luglio 2012!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2013, gli sconti eventualmente già erogati verranno stornati.
			L’erogazione degli sconti è subordinata al pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!--
	***************************
	Top6000 PRE ADESIONE morosi
	***************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE92'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			ENERGIT LE HA RISERVATO GLI ESCLUSIVI SCONTI TOP6000: ECCO COME OTTENERLI
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Siamo lieti di comunicarle che lei rientra tra i <fo:inline font-family="universcbold">Clienti
			Energit selezionati per un’eccezionale iniziativa.</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			Per ringraziarla di aver scelto la nostra
			azienda <fo:inline font-family="universcbold">desideriamo offrirle…tanta
			energia scontata!</fo:inline>
		</fo:block>
		
		<fo:block>
			<fo:inline font-family="universcbold">Le abbiamo infatti dedicato gli speciali sconti Top6000</fo:inline>, riservati
			ai Clienti in regola con i pagamenti e di valore crescente nel tempo, pari a
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:list-block keep-with-previous="always" provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">3 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di luglio 2012</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">6 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di gennaio 2013</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">9 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di dicembre 2013</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
			</fo:list-block>
		</fo:block>
		
		<fo:block>
			<fo:inline font-family="universcbold">Gli sconti saranno applicati ai consumi di tutte le sue utenze</fo:inline> attive
			ininterrottamente* fino a dicembre 2013.
		</fo:block>
		
		<fo:block space-before="2mm">
			<fo:inline font-family="universcbold">Non perda questa opportunità!
			Regolarizzi i pagamenti</fo:inline> e chiami il nostro Servizio Clienti,
			gratuito da rete fissa, al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì a venerdì
			(8.30 - 18.00); non appena la voce guida richiederà l’inserimento
			di un <fo:inline font-family="universcbold">codice promozione</fo:inline>, digiti il
			numero <fo:inline font-family="universcbold">6000</fo:inline> sulla tastiera telefonica
			e sarà subito in contatto con i nostri operatori, pronti a registrare la sua adesione
			all’iniziativa! Non dimentichi di indicare il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" />.
		</fo:block>
		
		<fo:block font-family="universcbold" space-before="2mm">
			Se preferisce, potrà richiedere i suoi sconti Top6000 anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del nostro sito
			Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Per aderire all’iniziativa ha tempo sino al 31 luglio 2012, ma perché aspettare? Conquisti subito i suoi sconti!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2013, gli sconti eventualmente già erogati verranno stornati.
			L’erogazione degli sconti è subordinata al pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	
	
	
	
	
	<!--
	**************************************************************************
	Top6000 - Comunicazione POST ADESIONE (prima dell'inizio della promozione)
	**************************************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE93'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI: LEI RICEVERA’ GLI SCONTI TOP6000, RISERVATI AI MIGLIORI CLIENTI!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">La sua adesione all’iniziativa Top6000
			è stata registrata: riceverà gli speciali sconti Top6000</fo:inline>, di valore crescente
			nel tempo e pari a 3 €/MWh nel mese di luglio 2012, 6 €/MWh nel mese di gennaio 2013, 9 €/MWh nel mese di dicembre 2013.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Gli sconti saranno applicati ai consumi di tutte le sue utenze</fo:inline> attive
			ininterrottamente* fino a dicembre 2013.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			<fo:inline font-family="universcbold">Grazie per aver scelto Energit!</fo:inline>
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2013, gli sconti eventualmente già erogati verranno stornati.
			L’erogazione degli sconti è subordinata al pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	<!--
	**********************************************************************
	Top6000 - Comunicazione POST ADESIONE (dopo l'inizio della promozione)
	**********************************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE94'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI, LEI RICEVE GLI SCONTI TOP6000, RISERVATI AI MIGLIORI CLIENTI!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Le ricordiamo che <fo:inline font-family="universcbold">l’applicazione degli speciali sconti Top6000</fo:inline>, pari
			a 3 €/MWh nel mese di luglio 2012, 6 €/MWh nel mese di
		</fo:block>
		
		<fo:block keep-with-previous="always">
			gennaio 2013, 9 €/MWh nel mese di dicembre
			2013, <fo:inline font-family="universcbold">è prevista sui consumi di tutte le sue utenze</fo:inline> attive
			ininterrottamente* fino a dicembre 2013.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			<fo:inline font-family="universcbold">Grazie per aver scelto Energit!</fo:inline>
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2013, gli sconti eventualmente già erogati verranno stornati.
			L’erogazione degli sconti è subordinata al pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	<!--
	*********************************
	EnergiTOP PRE ADESIONE non morosi
	*********************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE128'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			RICHIEDA SUBITO GLI EXTRA SCONTI ENERGITOP!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Abbiamo il piacere di comunicarle che <fo:inline font-family="universcbold">lei rientra tra i migliori Clienti
			Energit.</fo:inline> Per ringraziarla di aver scelto la nostra azienda <fo:inline font-family="universcbold">desideriamo
			offrirle… gli extra sconti sull’energia che raddoppiano nel tempo!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Le abbiamo infatti riservato gli speciali sconti EnergiTOP</fo:inline>, pari a
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:list-block keep-with-previous="always" provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">3 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di luglio 2013</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">6 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di gennaio 2014</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">12 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di dicembre 2014</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
			</fo:list-block>
		</fo:block>
		
		<fo:block>
			<fo:inline font-family="universcbold">Gli sconti saranno applicati ai consumi di tutte le sue utenze</fo:inline> attive
			ininterrottamente* fino a dicembre 2014. L’attribuzione degli sconti è prevista in proporzione ai suoi consumi, per ogni
			megawattora o sua frazione.
		</fo:block>
		
		<fo:block space-before="2mm">
			Per ottenere i suoi sconti dovrà solo richiederli: chiami il nostro Servizio Clienti,
			gratuito da rete fissa, al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì a venerdì
			(8.30 - 18.00); non appena la voce guida richiederà l’inserimento
			di un <fo:inline font-family="universcbold">codice promozione</fo:inline>, digiti il
			numero <fo:inline font-family="universcbold">7000</fo:inline> sulla tastiera del telefono e
			sarà subito in contatto con i nostri operatori, pronti a registrare la sua adesione all’iniziativa!
			Non dimentichi di indicare il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" />.
		</fo:block>
		
		<fo:block space-before="2mm">
			Se preferisce, potrà richiedere i suoi sconti EnergiTOP anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del nostro sito
			Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Per aderire all’iniziativa ha tempo sino al 31 luglio 2013, ma perché aspettare? Conquisti subito i suoi sconti!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2014, gli sconti eventualmente già erogati verranno stornati.
			L’erogazione degli sconti è subordinata al pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	<!--
	*****************************
	EnergiTOP PRE ADESIONE morosi
	*****************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE129'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			SCOPRA COME OTTENERE GLI EXTRA SCONTI ENERGITOP!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Abbiamo il piacere di comunicarle che <fo:inline font-family="universcbold">lei rientra tra i Clienti Energit
			selezionati per un’eccezionale iniziativa.</fo:inline> Per ringraziarla di aver scelto la nostra azienda <fo:inline font-family="universcbold">desideriamo
			offrirle… gli extra sconti sull’energia che raddoppiano nel tempo!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Le abbiamo infatti dedicato gli speciali sconti EnergiTOP</fo:inline>, riservati ai Clienti
			in regola con i pagamenti e pari a
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:list-block keep-with-previous="always" provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">3 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di luglio 2013</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">6 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di gennaio 2014</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				
				<fo:list-item keep-with-previous="always">
					<fo:list-item-label end-indent="label-end()">
						<fo:block>-</fo:block>
					</fo:list-item-label>
					<fo:list-item-body start-indent="body-start()">
						<fo:block>
							<fo:inline font-family="universcbold">12 €</fo:inline> per ogni <fo:inline font-family="universcbold">megawattora
							consumato nel mese di dicembre 2014</fo:inline>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>
			</fo:list-block>
		</fo:block>
		
		<fo:block>
			<fo:inline font-family="universcbold">Gli sconti saranno applicati ai consumi di tutte le sue utenze</fo:inline> attive
			ininterrottamente* fino a dicembre 2014. L’attribuzione degli sconti è prevista in proporzione ai suoi consumi, per ogni
			megawattora o sua frazione.
		</fo:block>
		
		<fo:block space-before="2mm">
			<fo:inline font-family="universcbold">Non perda questa opportunità! Regolarizzi i pagamenti</fo:inline> e chiami
			il nostro Servizio Clienti, gratuito da rete fissa, al numero <fo:inline font-family="universcbold">800.19.22.22</fo:inline> da
			lunedì a venerdì (8.30 - 18.00): non appena la voce guida richiederà l’inserimento di
			un <fo:inline font-family="universcbold">codice promozione</fo:inline>, digiti il
			numero <fo:inline font-family="universcbold">7000</fo:inline> sulla tastiera del telefono e sarà subito in contatto
			con i nostri operatori, pronti a registrare la sua adesione all’iniziativa! Non dimentichi di indicare il suo
			Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" />.
		</fo:block>
		
		<fo:block space-before="2mm">
			Se preferisce, potrà richiedere i suoi sconti EnergiTOP anche attraverso
			l'<fo:inline font-family="universcbold">Area Clienti</fo:inline> del nostro sito
			Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Per aderire all’iniziativa ha tempo sino al 31 luglio 2013, ma perché aspettare? Conquisti subito i suoi sconti!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2014, gli sconti eventualmente già erogati verranno stornati.
			L’erogazione degli sconti è subordinata al pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	<!--
	****************************************************************************
	EnergiTOP - Comunicazione POST ADESIONE (prima dell'inizio della promozione)
	****************************************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE130'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI: LEI RICEVERA’ GLI EXTRA SCONTI ENERGITOP, RISERVATI AI MIGLIORI CLIENTI!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">La sua adesione all’iniziativa EnergiTOP è stata registrata:
			riceverà gli speciali sconti EnergiTOP</fo:inline>, di valore crescente nel tempo e pari a
		</fo:block>
		
		<fo:block keep-with-previous="always">
			3 €/MWh nel mese di luglio 2013, 6 €/MWh nel mese di gennaio 2014, 12 €/MWh nel mese di dicembre 2014.
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Gli sconti saranno applicati ai consumi di tutte le sue utenze</fo:inline> attive
			ininterrottamente* fino a dicembre 2014.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			<fo:inline font-family="universcbold">Grazie per aver scelto Energit!</fo:inline>
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2014, gli sconti eventualmente già erogati verranno stornati.
			L’erogazione degli sconti è subordinata al pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	<!--
	************************************************************************
	EnergiTOP - Comunicazione POST ADESIONE (dopo l'inizio della promozione)
	************************************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE131'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI, LEI RICEVE GLI EXTRA SCONTI ENERGITOP RISERVATI AI MIGLIORI CLIENTI!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Le ricordiamo che <fo:inline font-family="universcbold">l’applicazione degli speciali sconti EnergiTOP</fo:inline>,
			pari a 3 €/MWh nel mese di luglio 2013, 6 €/MWh nel mese di gennaio 2014,
		</fo:block>
		
		<fo:block keep-with-previous="always">
			12 €/MWh nel mese di dicembre 2014, <fo:inline font-family="universcbold">è prevista sui consumi di tutte le
			sue utenze</fo:inline> attive ininterrottamente* fino a dicembre 2014.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			<fo:inline font-family="universcbold">Grazie per aver scelto Energit!</fo:inline>
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* In caso di recesso prima del 31/12/2014, gli sconti eventualmente già erogati verranno stornati.
			L’erogazione degli sconti è subordinata al pieno rispetto delle condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	<!--
	**************************************************
	MAXI_BONUS - Comunicazione PRE ADESIONE morosi
	**************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE172'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			SCOPRA COME OTTENERE GLI EXTRA SCONTI MAXIBONUS!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Abbiamo il piacere di comunicarle che <fo:inline font-family="universcbold">lei rientra tra i Clienti Energit selezionati per un’eccezionale iniziativa</fo:inline>.
			Per ringraziarla di aver scelto la nostra azienda <fo:inline font-family="universcbold">desideriamo offrirle… gli extra sconti sull'energia!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Le abbiamo infatti dedicato gli speciali sconti MAXIBONUS</fo:inline>, riservati ai Clienti in regola con i pagamenti e pari a
		</fo:block>
		
		<fo:block space-before="3mm" keep-with-previous="always" text-align="center">
			<fo:inline font-family="universcbold">10 € + 5 € + 5 € + 5 € + 20 €</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always" text-align="center">
			nel 1°, 7°, 13°, 18°, 24° mese di validità dell’iniziativa
		</fo:block>
		
		<fo:block keep-with-previous="always" text-align="center">
			per un totale di <fo:inline font-family="universcbold">45 € di sconto!</fo:inline>
		</fo:block>
		
		<fo:block space-before="3mm" keep-with-previous="always">
			<fo:inline font-family="universcbold">Gli sconti saranno applicati sulle fatture di tutte le sue utenze</fo:inline>
			attive ininterrottamente nei 24 mesi di durata della promozione*.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			<fo:inline font-family="universcbold">Non perda questa opportunità! Regolarizzi i pagamenti</fo:inline>
			e chiami il nostro Servizio Clienti, gratuito da rete fissa, al numero 
			<fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì a venerdì (8.30 - 18.00): non appena la voce guida richiederà l’inserimento di un 
			<fo:inline font-family="universcbold">codice promozione</fo:inline>, digiti il numero <fo:inline font-family="universcbold">7000</fo:inline>
			sulla tastiera del telefono e sarà subito in contatto con i nostri operatori,
			pronti a registrare la sua adesione all’iniziativa! Non dimentichi di indicare il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" />.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Se preferisce, potrà richiedere i suoi sconti MaxiBonus anche attraverso l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Per aderire all’iniziativa ha tempo sino al 31/10/2014,
			ma perché aspettare? Conquisti subito i suoi sconti!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* Il periodo di validità della promozione è calcolato a partire dal mese di adesione del Cliente all’iniziativa
			o – per le utenze non ancora attive – a partire dal primo mese di attivazione della fornitura.
			In caso di interruzione della fornitura prima dei 24 mesi di validità della promozione, gli sconti eventualmente
			già erogati potrebbero essere stornati. L’erogazione degli sconti è subordinata, inoltre, al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	<!--
	**************************************************
	MAXI_BONUS - Comunicazione PRE ADESIONE non morosi
	**************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE173'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			RICHIEDA SUBITO GLI EXTRA SCONTI MAXIBONUS!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Abbiamo il piacere di comunicarle che <fo:inline font-family="universcbold">lei rientra tra i migliori Clienti Energit</fo:inline>.
			Per ringraziarla di aver scelto la nostra azienda <fo:inline font-family="universcbold">desideriamo offrirle… una pioggia di extra sconti sull'energia!</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always">
			<fo:inline font-family="universcbold">Le abbiamo infatti riservato gli speciali sconti MAXIBONUS</fo:inline>, pari a
		</fo:block>
		
		<fo:block space-before="3mm" keep-with-previous="always" text-align="center">
			<fo:inline font-family="universcbold">10 € + 5 € + 5 € + 5 € + 20 €</fo:inline>
		</fo:block>
		
		<fo:block keep-with-previous="always" text-align="center">
			nel 1°, 7°, 13°, 18°, 24° mese di validità dell’iniziativa
		</fo:block>
		
		<fo:block keep-with-previous="always" text-align="center">
			per un totale di <fo:inline font-family="universcbold">45 € di sconto!</fo:inline>
		</fo:block>
		
		<fo:block space-before="3mm" keep-with-previous="always">
			<fo:inline font-family="universcbold">Gli sconti saranno applicati sulle fatture di tutte le sue utenze</fo:inline>
			attive ininterrottamente nei 24 mesi di durata della promozione*.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Per ottenere i suoi sconti dovrà solo richiederli: chiami il nostro Servizio Clienti, gratuito da rete fissa, al numero
			<fo:inline font-family="universcbold">800.19.22.22</fo:inline> da lunedì a venerdì (8.30 - 18.00); non appena la voce guida richiederà l’inserimento di un
			<fo:inline font-family="universcbold">codice promozione</fo:inline>, digiti il numero <fo:inline font-family="universcbold">7000</fo:inline>
			sulla tastiera del telefono e sarà subito in contatto con i nostri operatori,
			pronti a registrare la sua adesione all’iniziativa! Non dimentichi di indicare il suo Codice Cliente <xsl:value-of select="./ancestor::DOCUMENTO/child::CODICE_CLIENTE" />.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Se preferisce, potrà richiedere i suoi sconti MaxiBonus anche attraverso l'<fo:inline font-family="universcbold">Area Clienti</fo:inline>
			del nostro sito Internet <fo:inline font-family="universcbold">www.energit.it</fo:inline>, inserendo
			la sua UserID <xsl:value-of select="./ancestor::DOCUMENTO/child::USERNAME" /> e la password.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			Per aderire all’iniziativa ha tempo sino al 31/10/2014,
			ma perché aspettare? Conquisti subito i suoi sconti!
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* Il periodo di validità della promozione è calcolato a partire dal mese di adesione del Cliente all’iniziativa
			o – per le utenze non ancora attive – a partire dal primo mese di attivazione della fornitura.
			In caso di interruzione della fornitura prima dei 24 mesi di validità della promozione, gli sconti eventualmente
			già erogati potrebbero essere stornati. L’erogazione degli sconti è subordinata, inoltre, al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	<!--
	***************************************************
	SCONTO ENERGIT_PREMIA - Comunicazione POST ADESIONE
	***************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE194'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI, LEI RICEVE GLI SCONTI "ENERGIT PREMIA"!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Gentile Cliente, siamo lieti di informarla che nel suo contratto di fornitura  è attiva la promozione <fo:inline font-family="universcbold">"ENERGIT PREMIA"</fo:inline> che 
			consiste in uno sconto fisso mensile in bolletta che raddoppia il suo valore ogni 6 mesi di fornitura, per tutti i 24 mesi successivi alla data di attivazione della promozione. 
			Lo sconto parte da un importo minimo di 1 euro/mese per il primo semestre, fino ad un massimo di 8 euro/mese al quarto semestre di fornitura. 
			Inoltre, quale <fo:inline font-family="universcbold">ringraziamento per aver scelto Energit</fo:inline>, in via del tutto straordinaria, per <fo:inline font-family="universcbold">i primi sei mesi il Bonus le sarà erogato al doppio del valore previsto</fo:inline>. 
			Tale Bonus, inteso nel suo valore unitario al netto di IVA, sarà sempre riportato nel dettaglio della bolletta con voce separata.
		</fo:block>
	</xsl:if>

	<!--
	***************************************************
	RESTA_E_RADDOPPIA - Comunicazione POST ADESIONE
	***************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE195'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI, LEI RICEVE GLI SCONTI "RESTA E RADDOPPIA"!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Gentile Cliente, siamo lieti di informarla che nel suo contratto di fornitura  è attiva la promozione <fo:inline font-family="universcbold">"RESTA E RADDOPPIA"</fo:inline> che 
			consiste in uno sconto fisso mensile in bolletta che raddoppia il suo valore ogni 6 mesi di fornitura, per tutti i 24 mesi successivi alla data di attivazione della promozione. 
			Lo sconto parte da un importo minimo di 1 euro/mese per il primo semestre, fino ad un massimo di 8 euro/mese al quarto semestre di fornitura. 
			Inoltre, quale <fo:inline font-family="universcbold">ringraziamento per aver scelto Energit</fo:inline>, in via del tutto straordinaria, per <fo:inline font-family="universcbold">i primi sei mesi il Bonus le sarà erogato al doppio del valore previsto</fo:inline>. 
			Tale Bonus, inteso nel suo valore unitario al netto di IVA, sarà sempre riportato nel dettaglio della bolletta con voce separata.
		</fo:block>
	</xsl:if>	
	
	<!--
	****************************************
	MAXI_BONUS - Comunicazione POST ADESIONE
	****************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE174'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI, LEI RICEVE GLI EXTRA SCONTI MAXIBONUS!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Le ricordiamo che <fo:inline font-family="universcbold">l’applicazione degli speciali sconti MAXIBONUS, pari a 10 €, 5 €, 5 €, 5 €, 20 €</fo:inline>
			nel 1°, 7°, 13°, 18°, 24° mese di validità dell’iniziativa, per un totale di <fo:inline font-family="universcbold">45 €, è prevista sulle fatture di tutte le sue utenze</fo:inline>
			attive ininterrottamente nei 24 mesi di durata della promozione*.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			<fo:inline font-family="universcbold">Grazie per aver scelto Energit!</fo:inline>
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* Il periodo di validità della promozione è calcolato a partire dal mese di adesione del Cliente all’iniziativa
			o – per le utenze non ancora attive – a partire dal primo mese di attivazione della fornitura.
			In caso di interruzione della fornitura prima dei 24 mesi di validità della promozione, gli sconti eventualmente
			già erogati potrebbero essere stornati. L’erogazione degli sconti è subordinata, inoltre, al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	
	<!--
	*************************************************
	MAXI_BONUS BUSINESS - Comunicazione POST ADESIONE
	*************************************************
	-->
	<xsl:if test="./child::ID='COMUNICAZIONE193'">
		<fo:block font-family="universcbold">
			<xsl:if test="not($color='black')">
				<xsl:attribute name="color">#0065ae</xsl:attribute>
			</xsl:if>
			COMPLIMENTI, LEI RICEVE GLI EXTRA SCONTI MAXIBONUS!
		</fo:block>
	
		<fo:block keep-with-previous="always">
			Le ricordiamo che <fo:inline font-family="universcbold">l’applicazione degli speciali sconti MAXIBONUS, pari a 20 €, 10 €, 10 €, 10 €, 40 €</fo:inline>
			nel 1°, 7°, 13°, 18°, 24° mese di validità dell’iniziativa, per un totale di <fo:inline font-family="universcbold">90 €, è prevista sulle fatture di tutte le sue utenze</fo:inline>
			business attive ininterrottamente nei 24 mesi di durata della promozione*.
		</fo:block>
		
		<fo:block space-before="2mm" keep-with-previous="always">
			<fo:inline font-family="universcbold">Grazie per aver scelto Energit!</fo:inline>
		</fo:block>
		
		<fo:block font-size="9pt" space-before="2mm" keep-with-previous="always">
			* Il periodo di validità della promozione è calcolato a partire dal mese di adesione del Cliente all’iniziativa
			o – per le utenze non ancora attive – a partire dal primo mese di attivazione della fornitura.
			In caso di interruzione della fornitura prima dei 24 mesi di validità della promozione, gli sconti eventualmente
			già erogati potrebbero essere stornati. L’erogazione degli sconti è subordinata, inoltre, al pieno rispetto delle
			condizioni generali di contratto da parte del Cliente.
		</fo:block>
	</xsl:if>
	
	
	</xsl:template>
	
</xsl:stylesheet>

