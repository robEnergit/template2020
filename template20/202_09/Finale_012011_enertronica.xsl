<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" />
	
	
	
	<xsl:include href="Comunicazioni_promo_top.xsl"/>
	

	<xsl:template name="FINALE_012011">
	
	<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:param>
	
	<xsl:param name="comunicazione_area_clienti">
		<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI']">SI</xsl:if>
		<xsl:if test="not(./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI'])">NO</xsl:if>
	</xsl:param>
	
	<xsl:param name="color"/>
	
	<xsl:param name="svg-titoli"/>
	
	<xsl:param name="f3_color">
		<xsl:if test="$color='black'">
			#DDDDDD
		</xsl:if>
		<xsl:if test="not($color='black')">
			#fdd17e
		</xsl:if>
	</xsl:param>
	<xsl:param name="f2_color">
		<xsl:if test="$color='black'">
			#777777
		</xsl:if>
		<xsl:if test="not($color='black')">
			#f9b200
		</xsl:if>
	</xsl:param>
	<xsl:param name="f1_color">
		<xsl:if test="$color='black'">
			#FFFFFF
		</xsl:if>
		<xsl:if test="not($color='black')">
			#FFFFFF
		</xsl:if>
	</xsl:param>
	
	
	<xsl:param name="bordi"/>
	
		<!--
			Tutto il contenuto è racchiuso in un blocco con un valore
			orphans molto alto, in questo modo tutti i block figli
			ereditano questo attributo, che fa in modo che di fatto
			un blocco (paragrafo) non sia mai diviso tra due pagine 
		 -->
		<fo:block widows="3" orphans="3">
			
			<!-- Blocco padre per impostazioni "globali" -->
			<fo:block xsl:use-attribute-sets="font.table">
			
				<!-- Riepilogo fattura -->
				<xsl:call-template name="TEMPLATE_RIEPILOGO_FATTURA"/>
				
				<!-- Elenco fatture in attesa di pagamento -->
				<xsl:if test="FATTURE_ATTESA_PAGAMENTO">
					<fo:table table-layout="fixed" width="100%" space-before="5mm">
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(20)" />
						
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block>
										ELENCO FATTURE IN ATTESA DI PAGAMENTO
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row text-align="center">
								<fo:table-cell>
									<fo:block>
										NUMERO FATTURA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA EMISSIONE
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA SCADENZA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DOVUTO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO PAGATO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DA PAGARE
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
				
						<fo:table-body>
							<xsl:for-each select="FATTURE_ATTESA_PAGAMENTO">
								<fo:table-row text-align="center">
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="NUMERO_FATTURA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_EMISSIONE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_SCADENZA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="TOTALE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="IMPORTO_PAGATO" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="INSOLUTO" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
				
							<fo:table-row keep-with-previous="always" height="1mm">
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block />
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row keep-with-previous="always">
								<fo:table-cell number-columns-spanned="5">
									<fo:block />
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block text-align="center" font-size="8pt" font-family="universcbold">
										<xsl:value-of select="TOTALE_FATTURE_ATTESA_PAGAMENTO" /> euro
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					
				</xsl:if>
			</fo:block>
			
			
			
			<xsl:if test="$mese_fattura &gt; '201303'">
				<!-- Nuovo Schema fasce di consumo -->
				<xsl:if test="CONTRATTO_ENERGIA">
					<fo:block space-before="5mm" xsl:use-attribute-sets="font.fascia">
						<xsl:if test="not($color='black')">
							<xsl:attribute name="color">#0065ae</xsl:attribute>
						</xsl:if>
						FASCE ORARIE DEI CONSUMI DI ENERGIA
						(definite dall'Autorità per l'energia elettrica e il gas - Delibera 181/06)
					</fo:block>
					
					<fo:table table-layout="fixed" width="100%" space-before="0.5mm">
					<fo:table-column column-width="proportional-column-width(49)" />
					<fo:table-column column-width="proportional-column-width(51)" />
					<fo:table-body>
						<fo:table-row keep-with-previous="always" text-align="center">
							<fo:table-cell>
								<fo:table table-layout="fixed" width="100%"
										xsl:use-attribute-sets="font.fascia_header"
										border-collapse="separate" space-before="0.5mm"
										keep-with-previous="always">
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									
									<fo:table-body>
										<fo:table-row keep-with-previous="always" text-align="center">
											<fo:table-cell><fo:block>&#160;</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border"><fo:block>0.00-7.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>7.00-8.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>8.00-19.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>19.00-23.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>23.00-24.00</fo:block></fo:table-cell>
										</fo:table-row>
										
										<fo:table-row keep-with-previous="always" text-align="center">
											<fo:table-cell>
												<fo:table table-layout="fixed" width="100%">
													<fo:table-column column-width="proportional-column-width(100)" />

													<fo:table-body>
														<fo:table-row>
															<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.top border.bottom">
																<fo:block text-align="center">lun - ven</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">sab</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">dom e festivi</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:table-cell>
											
											<fo:table-cell background-color="{$f3_color}" number-columns-spanned="5" xsl:use-attribute-sets="border">
												<fo:table start-indent="1.485cm" background-color="{$f2_color}" table-layout="fixed" width="4.48cm" xsl:use-attribute-sets="border.left border.right border.bottom">
													<fo:table-column column-width="4.48cm" />

													<fo:table-body>
														<fo:table-row>
															<fo:table-cell display-align="center" padding-left="-1.5cm">
																<fo:table start-indent="3cm" background-color="{$f1_color}" table-layout="fixed" width="1.48cm" xsl:use-attribute-sets="border.left border.bottom border.right">
																	<fo:table-column column-width="1.48cm" />

																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell display-align="center" padding-left="-3cm">
																				<fo:block font-family="universcbold">F1</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
																
																<fo:block text-align="center" font-family="universcbold">F2</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
												
												<fo:block><fo:inline font-family="universcbold">F3</fo:inline>
												<xsl:if test="(not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Energit per Noi') and
															  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,7)='Ecoluce') and
															  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,5)='UNICA') and
															  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,11)='EnergitCASA'))
															  or
															  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Ecoluce Azienda'">
												<fo:inline font-family="universcbold">: Il risparmio aumenta</fo:inline> concentrando i
												consumi di energia in questa fascia!
												</xsl:if>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
							
							<fo:table-cell text-align="left">
								<fo:block xsl:use-attribute-sets="font.legenda" keep-with-previous="always">
									<fo:block>
										<fo:inline font-family="universcbold">F1:</fo:inline>
										ore di punta (peak). Nei giorni dal lunedì al venerdì: dalle
										ore 8.00 alle ore 19.00
									</fo:block>
								
									<fo:block keep-with-previous="always">
										<fo:inline font-family="universcbold">F2:</fo:inline>
										ore intermedie (mid-level). Nei giorni dal lunedì al venerdì:
										dalle ore 7.00 alle ore 8.00 e dalle ore 19.00 alle ore 23.00.
										Il sabato: dalle ore 7.00 alle ore 23.00
									</fo:block>
								
									<fo:block keep-with-previous="always">
										<fo:inline font-family="universcbold">F3:</fo:inline>
										ore fuori punta (off-peak). Nei giorni dal lunedì al venerdì:
										dalle ore 23.00 alle ore 7.00. La domenica e i festivi*: tutte
										le ore della giornata
									</fo:block>
								
									<fo:block font-size="6pt" keep-with-previous="always">
										* Si considerano festivi: 1 e 6 gennaio;
										lunedì di Pasqua; 25 aprile; 1 maggio; 2 giugno;
										15 agosto; 1 novembre; 8, 25 e 26 dicembre.
									</fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
					</fo:table>
				</xsl:if>
			</xsl:if>
			
			<xsl:if test="$mese_fattura &lt; '201304'">
				<!-- Schema fasce di consumo -->
				<xsl:if test="CONTRATTO_ENERGIA">
					<fo:block space-before="5mm" xsl:use-attribute-sets="font.fascia">
						<xsl:if test="not($color='black')">
							<xsl:attribute name="color">#0065ae</xsl:attribute>
						</xsl:if>
						FASCE ORARIE DEI CONSUMI DI ENERGIA
						(definite dall'Autorità per l'energia elettrica e il gas - Delibera 181/06)
					</fo:block>
					
					
					<fo:table table-layout="fixed" width="100%"
							xsl:use-attribute-sets="font.fascia_header"
							border-collapse="separate" space-before="0.5mm"
							keep-with-previous="always">
						<fo:table-column column-width="proportional-column-width(100)" />
						<fo:table-body>
					
					
							<fo:table-row keep-with-previous="always">
								<fo:table-cell>
									<fo:table table-layout="fixed" width="100%">
										<fo:table-column column-width="proportional-column-width(11)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-body>
											<fo:table-row keep-with-previous="always" text-align="center">
												<fo:table-cell><fo:block>&#160;</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border"><fo:block>0-1</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>1-2</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>2-3</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>3-4</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>4-5</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>5-6</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>6-7</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>7-8</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>8-9</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>9-10</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>10-11</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>11-12</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>12-13</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>13-14</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>14-15</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>15-16</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>16-17</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>17-18</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>18-19</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>19-20</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>10-21</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>21-22</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>22-23</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>23-24</fo:block></fo:table-cell>
											</fo:table-row>
											
											<fo:table-row keep-with-previous="always">
												<fo:table-cell>
													<fo:table table-layout="fixed" width="100%">
														<fo:table-column column-width="proportional-column-width(100)" />
					
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.top border.bottom">
																	<fo:block text-align="center">lun - ven</fo:block>
																</fo:table-cell>
															</fo:table-row>
															<!-- <fo:table-row keep-with-previous="always">
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																	<fo:block text-align="center">mar</fo:block>
																</fo:table-cell>
															</fo:table-row>
															<fo:table-row keep-with-previous="always">
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																	<fo:block text-align="center">mer</fo:block>
																</fo:table-cell>
															</fo:table-row>
															<fo:table-row keep-with-previous="always">
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																	<fo:block text-align="center">gio</fo:block>
																</fo:table-cell>
															</fo:table-row>
															<fo:table-row keep-with-previous="always">
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																	<fo:block text-align="center">ven</fo:block>
																</fo:table-cell>
															</fo:table-row> -->
															<fo:table-row keep-with-previous="always">
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																	<fo:block text-align="center">sab</fo:block>
																</fo:table-cell>
															</fo:table-row>
															<fo:table-row keep-with-previous="always">
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																	<fo:block text-align="center">dom e festivi</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</fo:table-body>
													</fo:table>
												</fo:table-cell>
					
					
												<fo:table-cell background-color="{$f3_color}"
															   xsl:use-attribute-sets="border" number-columns-spanned="24">
					
													<fo:table table-layout="fixed" width="100%">
														<fo:table-column column-width="proportional-column-width(24.5)" />
														<fo:table-column column-width="proportional-column-width(56)" />
														<fo:table-column column-width="proportional-column-width(3.5)" />
														<fo:table-body>
															<fo:table-row>
					
																<fo:table-cell>
																	<fo:table table-layout="fixed" width="100%">
																		<fo:table-column column-width="proportional-column-width(100)" />
																		<fo:table-body>
																			<fo:table-row>
																				<fo:table-cell padding-top="2pt">
																					<!-- <fo:block>&#160;</fo:block>
																					<fo:block>&#160;</fo:block> -->
																					<xsl:if test="(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Energit per Noi' or
																								  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,7)='Ecoluce' or
																								  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,5)='UNICA' or
																								  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,11)='EnergitCASA')
																								  and
																								  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Ecoluce Azienda')">															
																						<xsl:attribute name="padding-top">8pt</xsl:attribute>
																					</xsl:if>
																					<fo:block text-align="center" xsl:use-attribute-sets="font.fascia">F3</fo:block>
																					<xsl:if test="(not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Energit per Noi') and
																								  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,7)='Ecoluce') and
																								  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,5)='UNICA') and
																								  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,11)='EnergitCASA'))
																								  or
																								  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Ecoluce Azienda'">
																						<fo:block text-align="center" xsl:use-attribute-sets="font.fascia_text"><fo:inline font-family="universcbold">Il risparmio aumenta</fo:inline> concentrando</fo:block>
																						<fo:block text-align="center" xsl:use-attribute-sets="font.fascia_text">i consumi di energia in questa fascia!</fo:block>
																					</xsl:if>
																					<!-- <fo:block>&#160;</fo:block>
																					<fo:block>&#160;</fo:block>
																					<fo:block>&#160;</fo:block> -->
																				</fo:table-cell>
																			</fo:table-row>
																			<!-- <fo:table-row keep-with-previous="always">
																				<fo:table-cell>
																					<fo:block>&#160;</fo:block>
																				</fo:table-cell>
																			</fo:table-row> -->
																		</fo:table-body>
																	</fo:table>
																</fo:table-cell>
					
																<fo:table-cell>
																	<fo:table table-layout="fixed" width="100%"
																			  xsl:use-attribute-sets="border1.left border1.bottom border1.right"
																			  background-color="{$f2_color}">
																		<fo:table-column column-width="proportional-column-width(3.5)" />
																		<fo:table-column column-width="proportional-column-width(38.5)" />
																		<fo:table-column column-width="proportional-column-width(14)" />
																		<fo:table-body>
																			
																			
																			<fo:table-row>
																				<fo:table-cell>
																					<fo:block text-align="center"/>
																				</fo:table-cell>
																				<fo:table-cell background-color="{$f1_color}" display-align="center" padding-bottom="-1pt"
																					xsl:use-attribute-sets="border1.bottom border1.right border1.left">
																					<!-- <fo:block>&#160;</fo:block>
																					<fo:block>&#160;</fo:block> -->
																					<fo:block text-align="center" xsl:use-attribute-sets="font.fascia">F1</fo:block>
																					<!-- <fo:block>&#160;</fo:block>
																					<fo:block>&#160;</fo:block> -->
																				</fo:table-cell>
																				<fo:table-cell display-align="center" padding-bottom="-8pt">
																					<!-- <fo:block>&#160;</fo:block>
																					<fo:block>&#160;</fo:block> -->
																					<fo:block text-align="center" xsl:use-attribute-sets="font.fascia">F2</fo:block>
																					<!-- <fo:block>&#160;</fo:block>
																					<fo:block>&#160;</fo:block> -->
																				</fo:table-cell>
																			</fo:table-row>
																			
																			<fo:table-row >
																				<fo:table-cell number-columns-spanned="3">
																					<fo:block>&#160;</fo:block>
																				</fo:table-cell>
																			</fo:table-row>
																		</fo:table-body>
																	</fo:table>
																</fo:table-cell>
					
																<fo:table-cell>
																	<fo:block>&#160;</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</fo:table-body>
													</fo:table>
					
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
									
									
									<fo:block xsl:use-attribute-sets="font.legenda" space-before="1mm" keep-with-previous="always">
										<fo:block>
											<fo:inline font-family="universcbold">F1:</fo:inline>
											ore di punta (peak). Nei giorni dal lunedì al venerdì: dalle
											ore 8.00 alle ore 19.00
										</fo:block>
									
										<fo:block keep-with-previous="always">
											<fo:inline font-family="universcbold">F2:</fo:inline>
											ore intermedie (mid-level). Nei giorni dal lunedì al venerdì:
											dalle ore 7.00 alle ore 8.00 e dalle ore 19.00 alle ore 23.00.
											Il sabato: dalle ore 7.00 alle ore 23.00
										</fo:block>
									
										<fo:block keep-with-previous="always">
											<fo:inline font-family="universcbold">F3:</fo:inline>
											ore fuori punta (off-peak). Nei giorni dal lunedì al venerdì:
											dalle ore 23.00 alle ore 7.00. La domenica e i festivi*: tutte
											le ore della giornata
										</fo:block>
									
										<fo:block font-size="6pt" keep-with-previous="always">
											* Si considerano festivi: 1 gennaio; 6 gennaio;
											lunedì di Pasqua; 25 Aprile; 1 maggio; 2 giugno;
											15 agosto; 1 novembre; 8 dicembre; 25 dicembre;
											26 dicembre
										</fo:block>
									</fo:block>
					
								</fo:table-cell>
							</fo:table-row>
					
						</fo:table-body>
					</fo:table>
				</xsl:if>
			</xsl:if>
			
		</fo:block>
	
	</xsl:template>
	
	<xsl:template name="TEMPLATE_RIEPILOGO_FATTURA">
		
		<xsl:variable name="colonne">
			<xsl:if test="@SPOT='NO' or not(@SPOT)">6</xsl:if>
			<xsl:if test="@SPOT='SI'">5</xsl:if>
		</xsl:variable>
		
		<fo:table space-before="5mm" table-layout="fixed" width="100%" xsl:use-attribute-sets="font.table">
			<xsl:if test="@SPOT='NO' or not(@SPOT)">
				<fo:table-column column-width="proportional-column-width(22)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
			</xsl:if>
			<xsl:if test="@SPOT='SI'">
				<fo:table-column column-width="proportional-column-width(30)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
			</xsl:if>
		
			<fo:table-header>
				
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="cell.header">
						<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne+1"/></xsl:attribute>
						<fo:block>
							<fo:inline>RIEPILOGO FATTURA</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
		
				<fo:table-row text-align="center" display-align="center" height="4mm">
					<xsl:if test="@SPOT='SI'">
						<xsl:attribute name="height">5mm</xsl:attribute>
					</xsl:if>
					<fo:table-cell>
						<fo:block>SERVIZIO</fo:block>
					</fo:table-cell>
					
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						<fo:table-cell>
							<fo:block>N. CONTRATTO</fo:block>
						</fo:table-cell>
					</xsl:if>
		
					<fo:table-cell>
						<fo:block>IMPONIBILE</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>IVA</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>CODICE IVA</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>IMPORTO IVA</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>TOTALE</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
		
			<fo:table-body>
				<xsl:for-each select="RIEPILOGO_FATTURA">
					<fo:table-row text-align="center" display-align="center">
						<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI']">
							<xsl:attribute name="height">4mm</xsl:attribute>
						</xsl:if>
						<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI'] and position()&lt;last()">
							<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
						</xsl:if>
						<xsl:if test="position()=last()">
							<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
						</xsl:if>
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="PRODOTTO_SERVIZIO" />
							</fo:block>
						</fo:table-cell>
						
						<xsl:if test="./ancestor::DOCUMENTO[@SPOT='NO'] or not(./ancestor::DOCUMENTO[@SPOT])">
							<fo:table-cell>
								<fo:block>
									<xsl:value-of select="CONTRATTO" />
								</fo:block>
							</fo:table-cell>
						</xsl:if>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="IMPONIBILE" />
							</fo:block>
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="ALIQUOTA_IVA" />%
							</fo:block>
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:choose>
									<xsl:when test="CODICE_IVA">
										<xsl:value-of select="CODICE_IVA" />
									</xsl:when>
									<xsl:otherwise>
										-
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="IMPORTO_IVA" />
							</fo:block>
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="TOTALE" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
				
				<xsl:for-each select="DESCRIZIONI_CODICI_IVA">
					<fo:table-row keep-with-previous="always">
						<fo:table-cell>
							<fo:block>
								Codice IVA "<xsl:value-of select="CODICE_IVA" />"
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
							<fo:block>
								<xsl:value-of select="DESCRIZIONE_CODICE_IVA" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	
	<xsl:attribute-set name="font.table">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="line-height">9pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.pagare">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="line-height">7pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.header">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	    <xsl:attribute name="font-family">universcbold</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	    <xsl:attribute name="padding-left">3mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border">
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.left">
		<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.right">
		<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.top">
		<xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.bottom">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.left">
		<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.right">
		<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.top">
		<xsl:attribute name="border-top">1pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.bottom">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	 
	<xsl:attribute-set name="font.fascia_header">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">9.5pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia_text">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">6pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia">
	    <xsl:attribute name="font-family">universcbold</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.legenda">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">7pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.comunicazioni">
		<xsl:attribute name="font-family">universc</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.sepa">
		<xsl:attribute name="font-family">universc</xsl:attribute>
		<xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.title">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="line-height">4mm</xsl:attribute>
		<xsl:attribute name="space-after">4mm</xsl:attribute>
		<xsl:attribute name="font-size">11pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.separatore">
		<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="space-after">1mm</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.rinnovi">
		<xsl:attribute name="padding">0.5mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.mix_fonti">
		<xsl:attribute name="padding">0.1mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.articoli">
		<xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
		<xsl:attribute name="font-style">italic</xsl:attribute>
	</xsl:attribute-set>

</xsl:stylesheet>

