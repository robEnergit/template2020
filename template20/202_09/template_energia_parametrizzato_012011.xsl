﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:fox="http://xml.apache.org/fop/extensions">
<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**                                                    **
**          Template sulla Sezione Energia            **
**                                                    **
********************************************************
-->

<xsl:template name="DEF_COLONNE_012011">
	<xsl:param name="nome"/>
	<xsl:param name="escludi_prima"/>
	<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(./ancestor::DOCUMENTO/child::DATA_DOCUMENTO,7,4),substring(./ancestor::DOCUMENTO/child::DATA_DOCUMENTO,4,2))"/></xsl:param>
	
	<xsl:choose>
		<xsl:when test="$nome='ENERGIA100'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(13)"/>
            <fo:table-column column-width="proportional-column-width(19)"/>
            <fo:table-column column-width="proportional-column-width(16)"/>
            <fo:table-column column-width="proportional-column-width(16)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='DIECI_E_LUCE'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(6)"/>
            <fo:table-column column-width="proportional-column-width(12)"/>
            <fo:table-column column-width="proportional-column-width(9)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='DIECI_E_LUCE_BONUS'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(5)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(9)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
            <fo:table-column column-width="proportional-column-width(5)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_ENERGIA_COMUNE'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
			<xsl:if test="$mese_fattura &lt; '201109'">
				<fo:table-column column-width="proportional-column-width(9)"/>
				<fo:table-column column-width="proportional-column-width(12)"/>
				<fo:table-column column-width="proportional-column-width(11)"/>
				<fo:table-column column-width="proportional-column-width(10)"/>
				<fo:table-column column-width="proportional-column-width(11)"/>
				<fo:table-column column-width="proportional-column-width(11)"/>
				<fo:table-column column-width="proportional-column-width(15)"/>
			</xsl:if>
			<xsl:if test="$mese_fattura &gt; '201108'">
				<fo:table-column column-width="proportional-column-width(14)"/><!--Aggiunto una colonna per Descrizione e slitto di uno fino a 4-->
				<fo:table-column column-width="proportional-column-width(7)"/><!--Fascia-->
				<fo:table-column column-width="proportional-column-width(12)"/><!--Unità Misura-->
				<fo:table-column column-width="proportional-column-width(12)"/><!--Prezzo Listino-->
				<fo:table-column column-width="proportional-column-width(12)"/><!--Quantità-->
				<fo:table-column column-width="proportional-column-width(14)"/><!--Totale-->
			</xsl:if>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_ENERGIA_COMUNE_BONUS'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
			<xsl:if test="$mese_fattura &lt; '201109'">
				<fo:table-column column-width="proportional-column-width(6)"/>
				<fo:table-column column-width="proportional-column-width(10)"/>
				<fo:table-column column-width="proportional-column-width(9)"/>
				<fo:table-column column-width="proportional-column-width(8)"/>
				<fo:table-column column-width="proportional-column-width(11)"/>
				<fo:table-column column-width="proportional-column-width(10)"/>
				<fo:table-column column-width="proportional-column-width(10)"/>
				<fo:table-column column-width="proportional-column-width(15)"/>
			</xsl:if>
			<xsl:if test="$mese_fattura &gt; '201108'">
				<fo:table-column column-width="proportional-column-width(8)"/>
				<fo:table-column column-width="proportional-column-width(11)"/>
				<fo:table-column column-width="proportional-column-width(11)"/>
				<fo:table-column column-width="proportional-column-width(12)"/>
				<fo:table-column column-width="proportional-column-width(11)"/>
				<fo:table-column column-width="proportional-column-width(11)"/>
				<fo:table-column column-width="proportional-column-width(15)"/>
			</xsl:if>
		</xsl:when>
		<xsl:when test="$nome='LUCERTA'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(6)"/>
            <fo:table-column column-width="proportional-column-width(14)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='MILLELUCI'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(18)"/>
            <fo:table-column column-width="proportional-column-width(18)"/>
            <fo:table-column column-width="proportional-column-width(17)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='MILLELUCI_BONUS'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(13)"/>
			<fo:table-column column-width="proportional-column-width(13)"/>
            <fo:table-column column-width="proportional-column-width(13)"/>
            <fo:table-column column-width="proportional-column-width(14)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_ENERGIA_CONSUMER_E_X_NOI'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
			<fo:table-column column-width="proportional-column-width(5)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(9)"/>
			<fo:table-column column-width="proportional-column-width(8)"/>
			<fo:table-column column-width="proportional-column-width(13)"/>	
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(9)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_ENERGIA_CONSUMER_LUC_E'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
			<fo:table-column column-width="proportional-column-width(5)"/>
			<fo:table-column column-width="proportional-column-width(12)"/>
			<fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(16)"/>
			<fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(9)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_ENERGIA_CONSUMER_BILUCE'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
			<fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(18)"/>
            <fo:table-column column-width="proportional-column-width(18)"/>
            <fo:table-column column-width="proportional-column-width(17)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_ENERGIA_CONSUMER_ECOLUCE'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
			<fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(18)"/>
            <fo:table-column column-width="proportional-column-width(18)"/>
            <fo:table-column column-width="proportional-column-width(17)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_ENERGIA_CONSUMER_ECOLUCE_BONUS'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
			<fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_BONUS_ALTO_CONSUMO'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(25)"/>
            </xsl:if>
			<fo:table-column column-width="proportional-column-width(12)"/>
			<fo:table-column column-width="proportional-column-width(16)"/>
			<fo:table-column column-width="proportional-column-width(32)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_PRIMAVERA'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(25)"/>
            </xsl:if>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_OPZIONE_VERDE'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(25)"/>
            </xsl:if>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='TEMPLATE_PREMIUM_CASA'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(25)"/>
            </xsl:if>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='PIUPIU'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(5)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
            <fo:table-column column-width="proportional-column-width(7)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
            <fo:table-column column-width="proportional-column-width(6)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='PIUPIU_BONUS'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(5)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(7)"/>
            <fo:table-column column-width="proportional-column-width(6)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
			<fo:table-column column-width="proportional-column-width(6)"/>
            <fo:table-column column-width="proportional-column-width(6)"/>
            <fo:table-column column-width="proportional-column-width(7)"/>
            <fo:table-column column-width="proportional-column-width(8)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		
		<xsl:when test="$nome='INDICIZZATO'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(7)"/>
            <fo:table-column column-width="proportional-column-width(14)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(12)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
		<xsl:when test="$nome='INDICIZZATO_BONUS'">
			<xsl:if test="$escludi_prima='NO'">
				<fo:table-column column-width="proportional-column-width(21)"/>
            </xsl:if>
            <fo:table-column column-width="proportional-column-width(7)"/>
            <fo:table-column column-width="proportional-column-width(12)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(6)"/>
            <fo:table-column column-width="proportional-column-width(9)"/>
            <fo:table-column column-width="proportional-column-width(11)"/>
            <fo:table-column column-width="proportional-column-width(9)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
		</xsl:when>
	</xsl:choose>
</xsl:template>



<xsl:template name="RICAVA_VALORE_012011">
<xsl:param name="stringa"/>
<xsl:param name="nome"/>


	<xsl:choose>
		<xsl:when test="$stringa='PREZZO_TOTALE'">
			<xsl:if test="not(./child::PREZZO_TOTALE)">-</xsl:if>
			<xsl:value-of select="./child::PREZZO_TOTALE" />
		</xsl:when>
		<xsl:when test="$stringa='CORRISPETTIVO_ENERGIT'">
			<xsl:if test="not(./child::CORRISPETTIVO_ENERGIT)">-</xsl:if>
			<xsl:value-of select="./child::CORRISPETTIVO_ENERGIT" />
		</xsl:when>
		
		<xsl:when test="$stringa='DESCRIZIONE'">
			<xsl:if test="not(./child::DESCRIZIONE)">-</xsl:if> <!--Aggiunto Descrizione -->
			<xsl:value-of select="./child::DESCRIZIONE" />
		</xsl:when>
		
		<xsl:when test="$stringa='SCONTO_ENERGIA'">
			<xsl:if test="not(./child::SCONTO_ENERGIA)">-</xsl:if>
			<xsl:value-of select="./child::SCONTO_ENERGIA" />
		</xsl:when>
		<xsl:when test="$stringa='SCONTO_ENERGIA%'">
			<xsl:if test="not(./child::SCONTO_ENERGIA)">-</xsl:if>
			<xsl:if test="./child::SCONTO_ENERGIA">
				<xsl:value-of select="./child::SCONTO_ENERGIA" />%
			</xsl:if>
		</xsl:when>
		<xsl:when test="$stringa='SCONTO_AGGIUNTIVO'">
			<xsl:if test="not(./child::SCONTO_AGGIUNTIVO)">-</xsl:if>
			<xsl:value-of select="./child::SCONTO_AGGIUNTIVO" />
		</xsl:when>
		<xsl:when test="$stringa='SCONTO_AGGIUNTIVO%'">
			<xsl:if test="not(./child::SCONTO_AGGIUNTIVO)">-</xsl:if>
			<xsl:if test="./child::SCONTO_AGGIUNTIVO">
				<xsl:value-of select="./child::SCONTO_AGGIUNTIVO" />%
			</xsl:if>
		</xsl:when>
		<!--
		<xsl:when test="$stringa='CORRISPETTIVO_UNITARIO'">
			<xsl:if test="not(./child::CORRISPETTIVO_UNITARIO)">-</xsl:if>
			<xsl:value-of select="./child::CORRISPETTIVO_UNITARIO" />
		</xsl:when>-->
		<xsl:when test="$stringa='CORRISPETTIVO_UNITARIO'">
			<xsl:if test="not(./child::CORRISPETTIVO_UNITARIO) or ./child::CORRISPETTIVO_UNITARIO='0,0' or ./child::CORRISPETTIVO_UNITARIO='0,00' or ./child::CORRISPETTIVO_UNITARIO='0,000' or ./child::CORRISPETTIVO_UNITARIO='0,0000' or ./child::CORRISPETTIVO_UNITARIO='0,00000'  or ./child::CORRISPETTIVO_UNITARIO='0,000000'">
				<xsl:if test="./child::CORRISPETTIVO_ENERGIT">
					<xsl:value-of select="./child::CORRISPETTIVO_ENERGIT" />
				</xsl:if>
			</xsl:if>
			<xsl:if test="./child::CORRISPETTIVO_UNITARIO and not(./child::CORRISPETTIVO_UNITARIO='0,0') and not(./child::CORRISPETTIVO_UNITARIO='0,00') and not(./child::CORRISPETTIVO_UNITARIO='0,000') and not(./child::CORRISPETTIVO_UNITARIO='0,0000') and not(./child::CORRISPETTIVO_UNITARIO='0,00000') and not(./child::CORRISPETTIVO_UNITARIO='0,000000')">
				<xsl:value-of select="./child::CORRISPETTIVO_UNITARIO" />
			</xsl:if>
		</xsl:when>		
		<xsl:when test="$stringa='FASCIA'">
			<xsl:if test="not(./child::FASCIA)">-</xsl:if>
			<xsl:value-of select="./child::FASCIA" />
		</xsl:when>
		
		<xsl:when test="$stringa='QUANTITA'">
			<xsl:if test="not(./child::QUANTITA)">-</xsl:if>
			<xsl:if test="./child::QUANTITA">
				<xsl:choose>
					<xsl:when test="./child::UNITA_DI_MISURA='Mese'">
						<xsl:choose>
							<xsl:when test="./child::QUANTITA>1">
								<xsl:value-of select="./child::QUANTITA"/>
								Mesi
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="./child::QUANTITA" />
								Mese
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="./child::QUANTITA" /><xsl:text> </xsl:text><xsl:value-of select="./child::UNITA_DI_MISURA" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:when>
		<xsl:when test="$stringa='euro' or $stringa='cent di euro'">
			<xsl:if test="not(./child::UNITA_DI_MISURA)">-</xsl:if>
				<xsl:if test="./child::UNITA_DI_MISURA">
					<xsl:choose>
						<xsl:when test="./child::UNITA_DI_MISURA='Mese'">
							<xsl:value-of select="$stringa" />/cliente/<xsl:value-of select="./child::UNITA_DI_MISURA" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$stringa" />/<xsl:value-of select="./child::UNITA_DI_MISURA" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
		</xsl:when>
		<xsl:when test="$stringa='INDICE_AGGIORNAMENTO'">
			<xsl:choose>
			<xsl:when test="not(./child::INDICE_AGGIORNAMENTO) or ./child::INDICE_AGGIORNAMENTO='0,0' or ./child::INDICE_AGGIORNAMENTO='0,00'">-</xsl:when>
			<xsl:otherwise><xsl:value-of select="./child::INDICE_AGGIORNAMENTO" /></xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="$stringa='BONUS_ENERGIA'">
			<xsl:if test="not(./child::BONUS_ENERGIA)">-</xsl:if>
			<xsl:value-of select="./child::BONUS_ENERGIA" />
		</xsl:when>
		<xsl:when test="$stringa='SCADENZA_LISTINO'">
			<xsl:if test="not(./child::SCADENZA_LISTINO)">-</xsl:if>
			<xsl:value-of select="./child::SCADENZA_LISTINO" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$stringa" />
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>



<xsl:template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
<xsl:param name="template"/>
<xsl:param name="nome_sezione"/>
<xsl:param name="colonne"/>
<xsl:param name="colonna1"/>
<xsl:param name="colonna2"/>
<xsl:param name="colonna3"/>
<xsl:param name="colonna4"/>
<xsl:param name="colonna5"/>
<xsl:param name="colonna6"/>
<xsl:param name="colonna7"/>
<xsl:param name="colonna8"/>
<xsl:param name="colonna9"/>
<xsl:param name="colonna10"/>
<xsl:param name="value1"/>
<xsl:param name="value2"/>
<xsl:param name="value3"/>
<xsl:param name="value4"/>
<xsl:param name="value5"/>
<xsl:param name="value6"/>
<xsl:param name="value7"/>
<xsl:param name="value8"/>
<xsl:param name="value9"/>
<xsl:param name="value10"/>


<fo:table-row height="{$altezzariga}">
    <fo:table-cell number-columns-spanned="3">

        <!--
        ******************************************************************************************************************
        **                                           COMPONENTI ENERGIA                                                 **
        ******************************************************************************************************************
        -->
        
        
            <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
				<xsl:call-template name="DEF_COLONNE_012011">
					<xsl:with-param name="nome" select="$template"/>
					<xsl:with-param name="escludi_prima" select="'NO'"/>
				</xsl:call-template>
			
                
                    
                

                    <xsl:if test="@CAMBIO_INTESTAZIONE">
                    
                        <fo:table-header>
                    
                        <!-- <xsl:if test="position()&gt;1">
                            <fo:table-row height="{$altezzariga}">
                                <fo:table-cell>   
									<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne + 1"/></xsl:attribute>
									<fo:block></fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </xsl:if> -->
                    
                        <fo:table-row  height="4mm" xsl:use-attribute-sets="blk.002 condensedcors.008">
							<fo:table-cell>
								<fo:block></fo:block>
							</fo:table-cell>
                                                        
                            <xsl:if test="not($colonna1='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna1" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>
                                                        
                            <xsl:if test="not($colonna2='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna2" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>

                            <xsl:if test="not($colonna3='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna3" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>
                                            
                            <xsl:if test="not($colonna4='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna4" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>
                            
                            <xsl:if test="not($colonna5='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna5" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>

                            <xsl:if test="not($colonna6='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna6" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>
                                                        
                            <xsl:if test="not($colonna7='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna7" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>

                            <xsl:if test="not($colonna8='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna8" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>
							
							<xsl:if test="not($colonna9='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna9" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>
							
							<xsl:if test="not($colonna10='')">
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="$colonna10" />
									</fo:block>
								</fo:table-cell>
							</xsl:if>
                        </fo:table-row>
						
						<fo:table-row keep-with-previous="always" height="{$altezzariga}" xsl:use-attribute-sets="blk.002 chrbold.008">
							<fo:table-cell xsl:use-attribute-sets="pad.l.000">
								<fo:block>
									<fo:inline xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="$nome_sezione" /></fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
						
						</fo:table-header>
                    </xsl:if>
					
					
					
					<fo:table-body>
                                
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="pad.l.000">
                            <fo:block xsl:use-attribute-sets="blk.002">
                                <fo:inline xsl:use-attribute-sets="chr.008">Dal <xsl:value-of select="./child::DAL" /> al <xsl:value-of select="./child::AL" /></fo:inline>
                            </fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
							<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
                            <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
                                <xsl:call-template name="DEF_COLONNE_012011">
									<xsl:with-param name="nome" select="$template"/>
									<xsl:with-param name="escludi_prima" select="'SI'"/>
								</xsl:call-template>
								
                                <fo:table-body end-indent="0pt" start-indent="0pt">
                                                      
                                    <xsl:for-each select="./child::PRODOTTO">
									
										<xsl:variable name="valore_colonna1">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value1"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="valore_colonna2">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value2"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="valore_colonna3">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value3"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="valore_colonna4">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value4"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="valore_colonna5">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value5"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="valore_colonna6">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value6"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="valore_colonna7">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value7"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="valore_colonna8">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value8"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="valore_colonna9">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value9"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
										<xsl:variable name="valore_colonna10">
											<xsl:call-template name="RICAVA_VALORE_012011">
												<xsl:with-param name="stringa" select="$value10"/>
												<xsl:with-param name="nome" select="$template"/>
											</xsl:call-template>
										</xsl:variable>
									
									
                                        <fo:table-row keep-with-next="always" xsl:use-attribute-sets="blk.002 chr.008">
                                            
											<xsl:if test="not($colonna1='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna1" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
											
											<xsl:if test="not($colonna2='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna2" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
                                                            
                                            <xsl:if test="not($colonna3='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna3" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
											
											<xsl:if test="not($colonna4='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna4" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
											
											<xsl:if test="not($colonna5='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna5" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
											
											<xsl:if test="not($colonna6='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna6" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
											
											<xsl:if test="not($colonna7='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna7" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
											
											<xsl:if test="not($colonna8='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna8" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
											
											<xsl:if test="not($colonna9='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna9" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
											
											<xsl:if test="not($colonna10='')">
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="$valore_colonna10" />
													</fo:block>
												</fo:table-cell>
											</xsl:if>
									
                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body>
                            </fo:table>
                        </fo:table-cell>
                    </fo:table-row>
                              
					<xsl:if test="(position()&lt;last() 
								   or (./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO 
								   and not(./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,00' 
										   or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,0000' 
										   or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,000000')
									))">
						<fo:table-row>
							<fo:table-cell>
								<fo:block></fo:block>
							</fo:table-cell>
							<fo:table-cell padding-right="12mm">
								<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<fo:table-row height="1mm">	
											<fo:table-cell border-bottom="0.25pt dashed black">
												<fo:block></fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row height="1mm">
							<fo:table-cell>
								<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne+1"/></xsl:attribute>
								<fo:block></fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:if>
					
                    <!-- <xsl:if test="position()&lt;last()">
                        <fo:table-row keep-with-previous="always" height="2mm">
                            <fo:table-cell> 
								<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne + 1"/></xsl:attribute>
								<fo:block></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:if> -->
                
                </fo:table-body>

                <!-- <fo:table-body>
					<fo:table-row>
						<fo:table-cell> 
							<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne + 1"/></xsl:attribute>
							<fo:block></fo:block>
						</fo:table-cell>
					</fo:table-row>
                </fo:table-body> -->

            </fo:table>




    </fo:table-cell>
</fo:table-row>

</xsl:template>


</xsl:stylesheet>
