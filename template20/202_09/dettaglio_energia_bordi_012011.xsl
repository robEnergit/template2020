<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<xsl:template name="Dettaglio_energia_012011">

<xsl:param name="bordi"/>
<xsl:param name="color"/>
<xsl:param name="color_titolo_dettaglio"/>
<xsl:param name="color-sezioni"/>
<xsl:param name="color-sottosezioni"/>
<xsl:param name="svg-sezioni"/>
<xsl:param name="svg-sottosezioni"/>
<xsl:param name="lines_flag">SI</xsl:param>
<xsl:param name="ultima_sezione_reti"/>
<xsl:param name="ultima_sezione_vendita"/>
<xsl:param name="ultima_sezione_imposte"/>

<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(./ancestor::DOCUMENTO/child::DATA_DOCUMENTO,7,4),substring(./ancestor::DOCUMENTO/child::DATA_DOCUMENTO,4,2))"/></xsl:param>

<xsl:variable name="titolo1">
	<xsl:choose>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo1_c_202_09"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo1_202_09"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="titolo2">
	<xsl:choose>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo2_c_202_09"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo2_202_09"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="titolo2a">
	<xsl:choose>
		<xsl:when test="./child::PRODOTTO_SERVIZIO='Energit per Noi - EN-U-PO-01'">
			<xsl:value-of select="$tipo2a_exnoi_202_09"/>
		</xsl:when>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo2a_c_202_09"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo2a_202_09"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="titolo2b">
	<xsl:choose>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo2b_c_202_09"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo2b_202_09"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!--
**************************************************************************
**                                                                      **
** CORRISPETTIVI PER ACQUISTO, VENDITA, DISPACCIAMENTO E SBILANCIAMENTO **
**                                                                      **
**************************************************************************
-->
	<xsl:if test="(@CONSUMER='NO' and
				  (./child::SEZIONE[@TIPOLOGIA=$eb_cte] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_eb] or
				  ./child::SEZIONE[@TIPOLOGIA=$pt] or
				  ./child::SEZIONE[@TIPOLOGIA=$pc_disp] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_vendita] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_potenza_vendita] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_energia_vendita] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_ea]))
				  or
				  (@CONSUMER='SI' and
				  (./child::SEZIONE[@TIPOLOGIA=$ert] or
				  ./child::SEZIONE[@TIPOLOGIA=$eb_cte] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_eb] or
				  ./child::SEZIONE[@TIPOLOGIA=$pt] or
				  ./child::SEZIONE[@TIPOLOGIA=$pc_disp] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_vendita] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_potenza_vendita] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_energia_vendita] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_ea]))
				 ">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:if test="not($ultima_sezione_reti='')">
				<xsl:attribute name="space-before">1mm</xsl:attribute>
			</xsl:if>

			<fo:table-column column-width="proportional-column-width(100)"/>

			<fo:table-body>
				<xsl:if test="not($bordi='NO')">
					<fo:table-row height="3mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
							<fo:block></fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>

				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(100)"/>

							<xsl:call-template name="Header_sezione_dettaglio_012011">
								<xsl:with-param name="path" select="$svg-sezioni"/>
								<xsl:with-param name="titolo" select="$titolo2"/>
								<xsl:with-param name="header" select="'NO'"/>
								<xsl:with-param name="colonne" select="1"/>
								<xsl:with-param name="color" select="$color-sezioni"/>
								<xsl:with-param name="macro_sezione" select="'SI'"/>
							</xsl:call-template>

							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>

										<!--Quota Fissa EA-->
										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_vendita] or
													  ./child::SEZIONE[@TIPOLOGIA=$quota_energia_vendita]
													 ">
											<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<fo:table-column column-width="proportional-column-width(24)"/>
												<fo:table-column column-width="proportional-column-width(19)"/>
												<fo:table-column column-width="proportional-column-width(14)"/>
												<fo:table-column column-width="proportional-column-width(16)"/>
												<fo:table-column column-width="proportional-column-width(12)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>

												<xsl:call-template name="Header_sezione_dettaglio_012011">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="'no_title'"/>
													<xsl:with-param name="header" select="'SI'"/>
													<xsl:with-param name="colonne" select="6"/>
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>

												<fo:table-body end-indent="0pt" start-indent="0pt">
													<fo:table-row>
														<fo:table-cell number-columns-spanned="6">
															<!--QUOTA FISSA VENDITA-->
															<xsl:call-template name="Sezione_dettaglio_generica_012011">
																<xsl:with-param name="nome_sezione" select="$quota_fissa_vendita"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA FISSA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>

															<!--Trasporto Potenza-->
															<xsl:call-template name="Sezione_dettaglio_generica_012011">
																<xsl:with-param name="nome_sezione" select="$quota_potenza_vendita"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA POTENZA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>

															<!--QUOTA ENERGIA VENDITA-->
															<xsl:call-template name="Sezione_dettaglio_generica_012011">
																<xsl:with-param name="nome_sezione" select="$quota_energia_vendita"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA ENERGIA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</xsl:if>

										<!--
										**************************************************************************
										**                                                                      **
										**                        COMPONENTI ENERGIA                            **
										**                                                                      **
										**************************************************************************
										-->
										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$pc_disp]">
											<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
											<fo:table-column column-width="proportional-column-width(75)"/>
											<fo:table-column column-width="proportional-column-width(10)"/>
											<fo:table-column column-width="proportional-column-width(15)"/>

												<xsl:call-template name="Header_sezione_dettaglio_012011">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="$titolo2a"/>
													<xsl:with-param name="header" select="'NO'"/>
													<xsl:with-param name="colonne" select="3"/>
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>

												<fo:table-body end-indent="0pt" start-indent="-1pt">
													<xsl:for-each select="./child::SEZIONE[@TIPOLOGIA=$pc_disp]">
														<xsl:for-each select="./child::PERIODO_RIFERIMENTO">

															<xsl:if test="@TEMPLATE='1'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'ENERGIA100'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="5"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Quantità'"/>
																	<xsl:with-param name="colonna4" select="'Sconto Energit (%)'"/>
																	<xsl:with-param name="colonna5" select="'Totale scontato (euro)'"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'QUANTITA'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA%'"/>
																	<xsl:with-param name="value5" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='2'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'DIECI_E_LUCE'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="8"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Sconto Energit (%)'"/>
																	<xsl:with-param name="colonna5" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna7" select="'Quantità'"/>
																	<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA%'"/>
																	<xsl:with-param name="value5" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value7" select="'QUANTITA'"/>
																	<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='2_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'DIECI_E_LUCE_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="9"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Sconto (%)'"/>
																	<xsl:with-param name="colonna5" select="'Bonus'"/>
																	<xsl:with-param name="colonna6" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna7" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna8" select="'Quantità'"/>
																	<xsl:with-param name="colonna9" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA%'"/>
																	<xsl:with-param name="value5" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value6" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value7" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value8" select="'QUANTITA'"/>
																	<xsl:with-param name="value9" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='3' and $mese_fattura &lt; '201109'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_COMUNE'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna5" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value5" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='3' and $mese_fattura &gt; '201108'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_COMUNE'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="6"/>
																	<xsl:with-param name="colonna1" select="'Componente'"/>
																	<xsl:with-param name="colonna2" select="'Fascia'"/>
																	<xsl:with-param name="colonna3" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna4" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna5" select="''"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'DESCRIZIONE'"/>
																	<xsl:with-param name="value2" select="'FASCIA'"/>
																	<xsl:with-param name="value3" select="'euro'"/>
																	<xsl:with-param name="value4" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value5" select="''"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='3_BONUS' and $mese_fattura &lt; '201109'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_COMUNE_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="8"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna5" select="'Bonus (euro/MWh)'"/>
																	<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna7" select="'Quantità'"/>
																	<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value5" select="'SCONTO_ENERGIA'"/>
																	<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value7" select="'QUANTITA'"/>
																	<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='3_BONUS' and $mese_fattura &gt; '201108'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_COMUNE_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="''"/>
																	<xsl:with-param name="colonna5" select="'Bonus (euro/kWh)'"/>
																	<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna7" select="'Quantità'"/>
																	<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="''"/>
																	<xsl:with-param name="value5" select="'SCONTO_ENERGIA'"/>
																	<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value7" select="'QUANTITA'"/>
																	<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<!-- SCT incluse nella colonna SCONTO_ENERGIA-->
															<xsl:if test="@TEMPLATE='4'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'LUCERTA'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo AEEG'"/>
																	<xsl:with-param name="colonna4" select="'Risparmio'"/>
																	<xsl:with-param name="colonna5" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'cent di euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA'"/>
																	<xsl:with-param name="value5" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='5'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'MILLELUCI'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="5"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Quantità'"/>
																	<xsl:with-param name="colonna5" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'QUANTITA'"/>
																	<xsl:with-param name="value5" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='5_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'MILLELUCI_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="6"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Bonus'"/>
																	<xsl:with-param name="colonna5" select="'Quantità'"/>
																	<xsl:with-param name="colonna6" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value5" select="'QUANTITA'"/>
																	<xsl:with-param name="value6" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<!--Colonna bonus forzata a 3 e quindi il templete sarà sempre col suffisso _BONUS-->
															<xsl:if test="@TEMPLATE='6_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_E_X_NOI'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="8"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo AEEG'"/>
																	<xsl:with-param name="colonna4" select="'Sconto (%)'"/>
																	<xsl:with-param name="colonna5" select="'Bonus (cent di euro)'"/>
																	<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna7" select="'Quantità'"/>
																	<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'cent di euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'3%'"/>
																	<xsl:with-param name="value5" select="'SCONTO_ENERGIA'"/>
																	<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value7" select="'QUANTITA'"/>
																	<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='7' or @TEMPLATE='7_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_LUC_E'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo AEEG'"/>
																	<xsl:with-param name="colonna4" select="'Bonus 5 (cent di euro)'"/>
																	<xsl:with-param name="colonna5" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'cent di euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value5" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='8'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_ECOLUCE'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="5"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Quantità'"/>
																	<xsl:with-param name="colonna5" select="'Totale scontato (euro)'"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value4" select="'QUANTITA'"/>
																	<xsl:with-param name="value5" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='8_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_ECOLUCE_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Bonus'"/>
																	<xsl:with-param name="colonna5" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale scontato (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value5" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<!--Mai fatturato un bonus su biluce. Non è necessario il template 9_BONUS-->
															<xsl:if test="@TEMPLATE='9'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_BILUCE'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="5"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Quantità'"/>
																	<xsl:with-param name="colonna5" select="'Totale scontato (euro)'"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value4" select="'QUANTITA'"/>
																	<xsl:with-param name="value5" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='10'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'PIUPIU'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="9"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Maxi Sconto'"/>
																	<xsl:with-param name="colonna5" select="'Sconto Aggiuntivo'"/>
																	<xsl:with-param name="colonna6" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna7" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna8" select="'Quantità'"/>
																	<xsl:with-param name="colonna9" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA%'"/>
																	<xsl:with-param name="value5" select="'SCONTO_AGGIUNTIVO%'"/>
																	<xsl:with-param name="value6" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value7" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value8" select="'QUANTITA'"/>
																	<xsl:with-param name="value9" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='10_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'PIUPIU_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="10"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Maxi Sconto'"/>
																	<xsl:with-param name="colonna5" select="'Sconto Aggiuntivo'"/>
																	<xsl:with-param name="colonna6" select="'Bonus'"/>
																	<xsl:with-param name="colonna7" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna8" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna9" select="'Quantità'"/>
																	<xsl:with-param name="colonna10" select="'Totale (euro)'"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA%'"/>
																	<xsl:with-param name="value5" select="'SCONTO_AGGIUNTIVO%'"/>
																	<xsl:with-param name="value6" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value7" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value8" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value9" select="'QUANTITA'"/>
																	<xsl:with-param name="value10" select="'PREZZO_TOTALE'"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='11'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'INDICIZZATO'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna5" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value5" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='11_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'INDICIZZATO_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="8"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Bonus'"/>
																	<xsl:with-param name="colonna5" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna7" select="'Quantità'"/>
																	<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value5" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value7" select="'QUANTITA'"/>
																	<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='15000_BONUS' or @TEMPLATE='15000' or @TEMPLATE='18000_BONUS' or @TEMPLATE='18000'">
																<xsl:variable name="header_colonna3">Quantità (Consumi superiori a <xsl:value-of select="./child::PRODOTTO/child::BONUS_ENERGIA" /> kWh/mese)</xsl:variable>
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_BONUS_ALTO_CONSUMO'"/>
																	<xsl:with-param name="nome_sezione" select="'BONUS ALTO CONSUMO'"/>
																	<xsl:with-param name="colonne" select="4"/>
																	<xsl:with-param name="colonna1" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna2" select="'Corrispettivo unitario'"/>
																	<xsl:with-param name="colonna3" select="$header_colonna3"/>
																	<xsl:with-param name="colonna4" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna5" select="''"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'euro'"/>
																	<xsl:with-param name="value2" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value3" select="'QUANTITA'"/>
																	<xsl:with-param name="value4" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value5" select="''"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='1001' or @TEMPLATE='1001_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_PRIMAVERA'"/>
																	<xsl:with-param name="nome_sezione" select="'BONUS ANNOLUCE'"/>
																	<xsl:with-param name="colonne" select="4"/>
																	<xsl:with-param name="colonna1" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna2" select="'Corrispettivo unitario'"/>
																	<xsl:with-param name="colonna3" select="'Quantità'"/>
																	<xsl:with-param name="colonna4" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna5" select="''"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'euro'"/>
																	<xsl:with-param name="value2" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value3" select="'QUANTITA'"/>
																	<xsl:with-param name="value4" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value5" select="''"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='1002' or @TEMPLATE='1002_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_OPZIONE_VERDE'"/>
																	<xsl:with-param name="nome_sezione" select="'OPZIONE VERDE'"/>
																	<xsl:with-param name="colonne" select="4"/>
																	<xsl:with-param name="colonna1" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna2" select="'Corrispettivo unitario'"/>
																	<xsl:with-param name="colonna3" select="'Quantità'"/>
																	<xsl:with-param name="colonna4" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna5" select="''"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'euro'"/>
																	<xsl:with-param name="value2" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value3" select="'QUANTITA'"/>
																	<xsl:with-param name="value4" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value5" select="''"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

															<xsl:if test="@TEMPLATE='1003' or @TEMPLATE='1003_BONUS'">
																<xsl:variable name="title_template_var">
																	<xsl:choose>
																		<xsl:when test="contains(./ancestor::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,'CASA')">BONUS ENERGITCASA</xsl:when>
																		<xsl:when test="contains(./ancestor::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,'PREMIUM')">BONUS ENERGITPREMIUM</xsl:when>
																		<xsl:otherwise>BONUS</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>

																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_PREMIUM_CASA'"/>
																	<xsl:with-param name="nome_sezione" select="$title_template_var"/>
																	<xsl:with-param name="colonne" select="4"/>
																	<xsl:with-param name="colonna1" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna2" select="'Corrispettivo unitario'"/>
																	<xsl:with-param name="colonna3" select="'Quantità'"/>
																	<xsl:with-param name="colonna4" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna5" select="''"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'euro'"/>
																	<xsl:with-param name="value2" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value3" select="'QUANTITA'"/>
																	<xsl:with-param name="value4" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value5" select="''"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>

														</xsl:for-each>

														<xsl:call-template name="Chiudi_sezione_012011">
															<xsl:with-param name="lines_flag" select="$lines_flag"/>
															<xsl:with-param name="colonne" select="3"/>
														</xsl:call-template>

													</xsl:for-each>

													<xsl:choose>
														<xsl:when test="./child::PRODOTTO_SERVIZIO='Energia100'">
															<fo:table-row keep-with-previous="always" height="{$altezzariga}">
																<fo:table-cell number-columns-spanned="3">
																	<fo:block xsl:use-attribute-sets="blk.000">
																		<fo:inline xsl:use-attribute-sets="chrbold.008">Energit garantisce un risparmio di <xsl:value-of select="./child::SCONTO_CONTRATTO" />% sul corrispettivo fissato trimestralmente dall'AEEG per il mercato tutelato a copertura dei costi di generazione dell'energia. Il prossimo aggiornamento avrà effetto dal 1 <xsl:value-of select="./child::DATA_AGGIORNAMENTO_AEEG" /></fo:inline>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</xsl:when>

														<xsl:when test="./child::PRODOTTO_SERVIZIO='LuCerta'">
															<fo:table-row keep-with-previous="always" height="{$altezzariga}">
																<fo:table-cell number-columns-spanned="3">
																	<fo:block xsl:use-attribute-sets="blk.000">
																		<fo:inline xsl:use-attribute-sets="chrbold.008">Energit garantisce un risparmio di <xsl:value-of select="./child::SCONTO_CONTRATTO" /> cent di euro/kWh sul corrispettivo fissato trimestralmente dall'AEEG per il mercato tutelato a copertura dei costi di generazione dell'energia. Il prossimo aggiornamento avrà effetto dal 1 <xsl:value-of select="./child::DATA_AGGIORNAMENTO_AEEG" /></fo:inline>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</xsl:when>
													</xsl:choose>
												</fo:table-body>
											</fo:table>
										</xsl:if>

										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$pt]">
											<!--
											**************************************************************************
											**                                                                      **
											**                                PERDITE                               **
											**                                                                      **
											**************************************************************************
											-->
											<fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<!-- <fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/> -->
												<fo:table-column column-width="proportional-column-width(75)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>

												<xsl:call-template name="Header_sezione_dettaglio_012011">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="$tipo4_202_09"/>
													<xsl:with-param name="header" select="'NO'"/>
													<xsl:with-param name="colonne" select="3"/>
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>

												<fo:table-body end-indent="0pt" start-indent="0pt">
													<!-- <fo:table-row>
														<fo:table-cell number-columns-spanned="5">
															<xsl:call-template name="Sezione_dettaglio_generica_012011_012011">
																<xsl:with-param name="nome_sezione" select="$pt"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA ENERGIA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>
														</fo:table-cell>
													</fo:table-row> -->

													<!--PT-->
													<xsl:for-each select="./child::SEZIONE[@TIPOLOGIA=$pt]">
														<xsl:for-each select="./child::PERIODO_RIFERIMENTO">
															<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_E_X_NOI'"/>
																<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																<xsl:with-param name="colonne" select="8"/>
																<xsl:with-param name="colonna1" select="'Fascia'"/>
																<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																<xsl:with-param name="colonna3" select="'Prezzo AEEG'"/>
																<xsl:with-param name="colonna4" select="'Sconto (%)'"/>
																<xsl:with-param name="colonna5" select="'Bonus (cent di euro)'"/>
																<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																<xsl:with-param name="colonna7" select="'Quantità'"/>
																<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																<xsl:with-param name="colonna9" select="''"/>
																<xsl:with-param name="colonna10" select="''"/>
																<xsl:with-param name="value1" select="'FASCIA'"/>
																<xsl:with-param name="value2" select="'cent di euro'"/>
																<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																<xsl:with-param name="value4" select="'3%'"/>
																<xsl:with-param name="value5" select="'SCONTO_ENERGIA'"/>
																<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																<xsl:with-param name="value7" select="'QUANTITA'"/>
																<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																<xsl:with-param name="value9" select="''"/>
																<xsl:with-param name="value10" select="''"/>
															</xsl:call-template>
														</xsl:for-each>

														<xsl:call-template name="Chiudi_sezione_012011">
															<xsl:with-param name="lines_flag" select="$lines_flag"/>
															<xsl:with-param name="colonne" select="3"/>
														</xsl:call-template>

													</xsl:for-each>
												</fo:table-body>
											</fo:table>
										</xsl:if>

									</fo:table-cell>
								</fo:table-row>

								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell xsl:use-attribute-sets="brd.b.000">
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell>
										<fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
										<fo:table-column column-width="proportional-column-width(85)"/>
										<fo:table-column column-width="proportional-column-width(15)"/>
										<fo:table-body end-indent="0pt" start-indent="0pt">
											<fo:table-row height="{$altezzariga}">
												<fo:table-cell>
													<fo:block xsl:use-attribute-sets="blk.000">
														TOTALE <xsl:value-of select="$titolo2"/>
														<xsl:if test="$lettere_sezioni='SI'">
															<fo:inline xsl:use-attribute-sets="condensedcors.008">
																(A)
															</fo:inline>
														</xsl:if>
													</fo:block>
												</fo:table-cell>

												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="./child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
										</fo:table>
									</fo:table-cell>
								</fo:table-row>

							</fo:table-body>
						</fo:table>

						<!-- <xsl:if test="@CONSUMER='SI'">
							<fo:block xsl:use-attribute-sets="blk.004 chrbold.008">
								Maggiori dettagli sulle componenti A, UC, MCT sono disponibili nella sua area riservata.
							</fo:block>
						</xsl:if> -->
					</fo:table-cell>
				</fo:table-row>

				<xsl:if test="not($bordi='NO')">
					<fo:table-row keep-with-previous="always" height="2mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
							<fo:block></fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-body>
		</fo:table>
	</xsl:if>

<!--
**************************************************************************
**                                                                      **
**      CORRISPETTIVI PER L’USO DELLE RETI E IL SERVIZIO DI MISURA      **
**                                                                      **
**************************************************************************
-->
	<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$ert] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_energia_reti] or
				  ./child::SEZIONE[@TIPOLOGIA=$tp] or
				  ./child::SEZIONE[@TIPOLOGIA=$te] or
				  ./child::SEZIONE[@TIPOLOGIA=$a_uc]
				 ">
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>

			<fo:table-body>
				<xsl:if test="not($bordi='NO')">
					<fo:table-row height="3mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
							<fo:block/>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>

				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(24)"/>
							<fo:table-column column-width="proportional-column-width(19)"/>
							<fo:table-column column-width="proportional-column-width(14)"/>
							<fo:table-column column-width="proportional-column-width(16)"/>
							<fo:table-column column-width="proportional-column-width(12)"/>
							<fo:table-column column-width="proportional-column-width(15)"/>

							<xsl:call-template name="Header_sezione_dettaglio_012011">
								<xsl:with-param name="path" select="$svg-sezioni"/>
								<xsl:with-param name="titolo" select="$titolo1"/>
								<xsl:with-param name="header" select="'SI'"/>
								<xsl:with-param name="colonne" select="6"/>
								<xsl:with-param name="color" select="$color-sezioni"/>
								<xsl:with-param name="macro_sezione" select="'SI'"/>
							</xsl:call-template>

							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row>
									<fo:table-cell number-columns-spanned="6">
										<!--Quota Fissa-->
										<xsl:call-template name="Sezione_dettaglio_generica_012011">
											<xsl:with-param name="nome_sezione" select="$a_uc"/>
											<xsl:with-param name="tipo_quota" select="'QUOTA FISSA'"/>
											<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
											<xsl:with-param name="info_totale" select="'USO RETI'"/>
											<xsl:with-param name="lines" select="$lines_flag"/>
											<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
											<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
											<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
											<xsl:with-param name="titolo" select="$titolo1"/>
										</xsl:call-template>

										<!--Trasporto Potenza-->
										<xsl:call-template name="Sezione_dettaglio_generica_012011">
											<xsl:with-param name="nome_sezione" select="$tp"/>
											<xsl:with-param name="tipo_quota" select="'QUOTA POTENZA'"/>
											<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
											<xsl:with-param name="info_totale" select="'USO RETI'"/>
											<xsl:with-param name="lines" select="$lines_flag"/>
											<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
											<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
											<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
											<xsl:with-param name="titolo" select="$titolo1"/>
										</xsl:call-template>

										<!--Trasporto Energia-->
										<xsl:call-template name="Sezione_dettaglio_generica_012011">
											<xsl:with-param name="nome_sezione" select="$te"/>
											<xsl:with-param name="tipo_quota" select="'QUOTA ENERGIA'"/>
											<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
											<xsl:with-param name="info_totale" select="'USO RETI'"/>
											<xsl:with-param name="lines" select="$lines_flag"/>
											<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
											<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
											<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
											<xsl:with-param name="titolo" select="$titolo1"/>
										</xsl:call-template>

										<!-- <xsl:if test="@CONSUMER='NO'"> -->
											<!--Energia Reattiva-->
											<!-- <xsl:call-template name="Sezione_dettaglio_generica_012011">
												<xsl:with-param name="nome_sezione" select="$ert"/>
												<xsl:with-param name="tipo_quota" select="'ENERGIA REATTIVA'"/>
												<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
												<xsl:with-param name="info_totale" select="'USO RETI'"/>
												<xsl:with-param name="lines" select="$lines_flag"/>
												<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
												<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
												<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
												<xsl:with-param name="titolo" select="$titolo1"/>
											</xsl:call-template>	 -->
										<!-- </xsl:if> -->


										<!--
										**************************************************************************
										**                                                                      **
										**                 COMPONENTI DISPACCIAMENTO E PERDITE                  **
										**                                                                      **
										**************************************************************************
										-->
										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_energia_reti] or
													  ./child::SEZIONE[@TIPOLOGIA=$ert]
													 ">
											<!-- <fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>

												<xsl:call-template name="Header_sezione_dettaglio_012011">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="$titolo2b"/>
													<xsl:with-param name="header" select="'SI'"/>
													<xsl:with-param name="colonne" select="6"/>
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>

												<fo:table-body end-indent="0pt" start-indent="0pt">
													<fo:table-row>
														<fo:table-cell number-columns-spanned="6"> -->

															<!--QUOTA ENERGIA RETI-->
															<xsl:call-template name="Sezione_dettaglio_generica_012011">
																<xsl:with-param name="nome_sezione" select="$quota_energia_reti"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA ENERGIA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
																<xsl:with-param name="info_totale" select="'USO RETI'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo1"/>
															</xsl:call-template>

															<!-- <xsl:if test="@CONSUMER='SI'"> -->
																<!--Energia Reattiva-->
																<xsl:call-template name="Sezione_dettaglio_generica_012011">
																	<xsl:with-param name="nome_sezione" select="$ert"/>
																	<xsl:with-param name="tipo_quota" select="'ENERGIA REATTIVA'"/>
																	<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
																	<xsl:with-param name="info_totale" select="'USO RETI'"/>
																	<xsl:with-param name="lines" select="$lines_flag"/>
																	<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																	<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																	<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																	<xsl:with-param name="titolo" select="$titolo1"/>
																</xsl:call-template>
															<!-- </xsl:if> -->
														<!-- </fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table> -->
										</xsl:if>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>

						<!-- <xsl:if test="@CONSUMER='NO'"> -->
							<fo:block xsl:use-attribute-sets="blk.002 chrbold.008">
								Potrà trovare nella sua Area Clienti maggiori dettagli sui corrispettivi fatturati e
								ulteriori disaggregazioni degli stessi.
							</fo:block>
							<fo:block keep-with-previous="always" xsl:use-attribute-sets="blk.002 chrbold.008">
								Gli aggiornamenti delle componenti tariffarie fatturate sono presenti sul sito web
								dell'Autorità per l'Energia Elettrica e il Gas, nella sezione atti e provvedimenti.
							</fo:block>
						<!-- </xsl:if> -->
					</fo:table-cell>
				</fo:table-row>

				<xsl:if test="not($bordi='NO')">
					<fo:table-row keep-with-previous="always" height="2mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
							<fo:block></fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-body>
		</fo:table>
	</xsl:if>

<!--
**************************************************************************
**                                                                      **
**                             IMPOSTE                                  **
**                                                                      **
**************************************************************************
-->
	<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$imposte]
				 ">
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:if test="not($ultima_sezione_reti='') or
						  not($ultima_sezione_vendita='')">
				<xsl:attribute name="space-before">1mm</xsl:attribute>
			</xsl:if>

			<fo:table-column column-width="proportional-column-width(100)"/>

			<fo:table-body>
				<xsl:if test="not($bordi='NO')">
					<fo:table-row height="3mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
							<fo:block xsl:use-attribute-sets="blk.002">
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>

				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
						</xsl:if>
						<fo:table space-before="10mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(24)"/>
						<fo:table-column column-width="proportional-column-width(19)"/>
						<fo:table-column column-width="proportional-column-width(14)"/>
						<fo:table-column column-width="proportional-column-width(16)"/>
						<fo:table-column column-width="proportional-column-width(12)"/>
						<fo:table-column column-width="proportional-column-width(15)"/>

						<xsl:call-template name="Header_sezione_dettaglio_012011">
							<xsl:with-param name="path" select="$svg-sezioni"/>
							<xsl:with-param name="titolo" select="$tipo3_202_09"/>
							<xsl:with-param name="header" select="'SI'"/>
							<xsl:with-param name="colonne" select="6"/>
							<xsl:with-param name="color" select="$color-sezioni"/>
							<xsl:with-param name="macro_sezione" select="'SI'"/>
						</xsl:call-template>

						<fo:table-body end-indent="0pt" start-indent="0pt">
							<fo:table-row>
								<fo:table-cell number-columns-spanned="6">
									<!--Imposta erariale-->
									<xsl:call-template name="Sezione_dettaglio_generica_012011">
										<xsl:with-param name="nome_sezione" select="$imposte"/>
										<xsl:with-param name="tipo_quota" select="''"/>
										<xsl:with-param name="last_section" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="info_totale" select="'IMPOSTE'"/>
										<xsl:with-param name="lines" select="$lines_flag"/>
										<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
										<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
										<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="titolo" select="$tipo3_202_09"/>
									</xsl:call-template>

									<fo:block xsl:use-attribute-sets="blk.002">
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>

				<xsl:if test="not($bordi='NO')">
					<fo:table-row keep-with-previous="always" height="2mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
							<fo:block xsl:use-attribute-sets="blk.002">
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-body>
		</fo:table>
	</xsl:if>
</xsl:template>




</xsl:stylesheet>
