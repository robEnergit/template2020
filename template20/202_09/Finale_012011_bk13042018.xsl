<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" />
	
	
	
	<xsl:include href="Comunicazioni_promo_top.xsl"/>
	

	<xsl:template name="FINALE_012011">
	
	<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:param>
	
	<xsl:param name="comunicazione_area_clienti">
		<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI']">SI</xsl:if>
		<xsl:if test="not(./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI'])">NO</xsl:if>
	</xsl:param>
	
	<xsl:param name="color"/>
	
	<xsl:param name="svg-titoli"/>
	
	<xsl:param name="f3_color">
		<xsl:if test="$color='black'">
			#DDDDDD
		</xsl:if>
		<xsl:if test="not($color='black')">
			#fdd17e
		</xsl:if>
	</xsl:param>
	<xsl:param name="f2_color">
		<xsl:if test="$color='black'">
			#777777
		</xsl:if>
		<xsl:if test="not($color='black')">
			#f9b200
		</xsl:if>
	</xsl:param>
	<xsl:param name="f1_color">
		<xsl:if test="$color='black'">
			#FFFFFF
		</xsl:if>
		<xsl:if test="not($color='black')">
			#FFFFFF
		</xsl:if>
	</xsl:param>
	
	
	<xsl:param name="bordi"/>
	
		<!--
			Tutto il contenuto è racchiuso in un blocco con un valore
			orphans molto alto, in questo modo tutti i block figli
			ereditano questo attributo, che fa in modo che di fatto
			un blocco (paragrafo) non sia mai diviso tra due pagine 
		 -->
		<fo:block widows="3" orphans="3">
			
			<!-- Blocco padre per impostazioni "globali" -->
			<fo:block xsl:use-attribute-sets="font.table">
			
				<!-- Riepilogo fattura -->
				<xsl:call-template name="TEMPLATE_RIEPILOGO_FATTURA"/>
				
				<!-- Elenco fatture in attesa di pagamento -->
				<xsl:if test="FATTURE_ATTESA_PAGAMENTO">
					<fo:table table-layout="fixed" width="100%" space-before="5mm">
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(20)" />
						
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block>
										ELENCO FATTURE IN ATTESA DI PAGAMENTO
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row text-align="center">
								<fo:table-cell>
									<fo:block>
										NUMERO FATTURA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA EMISSIONE
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA SCADENZA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DOVUTO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO PAGATO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DA PAGARE
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
				
						<fo:table-body>
							<xsl:for-each select="FATTURE_ATTESA_PAGAMENTO">
								<fo:table-row text-align="center">
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="NUMERO_FATTURA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_EMISSIONE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_SCADENZA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="TOTALE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="IMPORTO_PAGATO" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="INSOLUTO" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
				
							<fo:table-row keep-with-previous="always" height="1mm">
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block />
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row keep-with-previous="always">
								<fo:table-cell number-columns-spanned="5">
									<fo:block />
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block text-align="center" font-size="8pt" font-family="universcbold">
										<xsl:value-of select="TOTALE_FATTURE_ATTESA_PAGAMENTO" /> euro
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					
				</xsl:if>
			</fo:block>
			
			
			<!-- Frase "perchè pagare entro la data di scadenza..." -->
			<fo:block space-before="5mm" xsl:use-attribute-sets="font.pagare">
				<fo:block font-family="universcbold" font-size="8pt">
					<xsl:if test="not($color='black')">
						<xsl:attribute name="color">#0065ae</xsl:attribute>
					</xsl:if>
					Perché pagare entro la data di scadenza
				</fo:block>

				<fo:block keep-with-previous="always">
					La fattura deve essere pagata per intero entro la data di scadenza.
					In caso di ritardato pagamento (anche parziale) di qualsiasi importo
					dovuto ad Energit, dal giorno successivo alla scadenza del termine previsto
					per il pagamento decorreranno a carico del Cliente interessi di mora,
					applicati, disciplinati e determinati in conformità a quanto previsto
					dal Decreto Legislativo n. 231/2002 e calcolati in base ad un tasso di
					interesse pari a Euribor 6 mesi maggiorato 5%, fatto comunque salvo il
					diritto di Energit al risarcimento del maggior danno.
				</fo:block>
				
				<fo:block keep-with-previous="always">
					In caso di mancato pagamento (anche parziale) di qualsiasi importo dovuto
					ad Energit, decorsi 5 (cinque) giorni dalla scadenza del termine indicato
					nella fattura per il pagamento, Energit invierà al Cliente, ai sensi della
					delibera AEEG n. 04/08, una comunicazione in cui indicherà il termine, in
					ogni caso non inferiore a 5 (cinque) giorni, entro cui il Cliente sarà tenuto
					ad effettuare il pagamento di quanto dovuto ad Energit.
				</fo:block>
				
				<fo:block keep-with-previous="always">
					Decorso tale termine senza che sia pervenuto il pagamento delle somme dovute,
					Energit richiederà al Distributore Locale la sospensione della fornitura. Per
					tutti i punti di prelievo alimentati in bassa tensione, qualora sussistano le
					condizioni tecniche, la sospensione della fornitura sarà preceduta da una
					riduzione della potenza disponibile ad un livello pari al 15%.
				</fo:block>
				
				<fo:block keep-with-previous="always">
					In caso di mancato pagamento entro 15 giorni dall’intervento di riduzione della
					potenza, la Società richiederà al distributore locale la sospensione della fornitura.
				</fo:block>
				
				<fo:block keep-with-previous="always">
					Energit si riserva altresì il diritto di risolvere il contratto in caso di mancato
					pagamento delle somme dovute.
				</fo:block>
				
				<fo:block keep-with-previous="always">
					Ai sensi della Delibera AEEG n. 200/1999, la rateizzazione dei corrispettivi
					dovuti per la fornitura di energia elettrica potrà essere richiesta nel caso in
					cui, a seguito del malfunzionamento accertato del gruppo di misura per causa non
					imputabile al Cliente, sia stato richiesto il pagamento di corrispettivi per
					consumi non registrati dal gruppo di misura. La richiesta di rateizzazione dovrà
					essere presentata dal Cliente in forma scritta entro il termine fissato per il
					pagamento della fattura. In tutti gli altri casi, per Energit non sussiste l’obbligo
					di accogliere eventuali richieste di rateizzazione dei pagamenti.
				</fo:block>
			</fo:block>
			
			<!-- Nuovo Schema fasce di consumo -->
			<!-- <xsl:if test="$mese_fattura &gt; '201303'">
				<xsl:if test="CONTRATTO_ENERGIA">
					<fo:block space-before="5mm" xsl:use-attribute-sets="font.fascia">
						<xsl:if test="not($color='black')">
							<xsl:attribute name="color">#0065ae</xsl:attribute>
						</xsl:if>
						FASCE ORARIE DEI CONSUMI DI ENERGIA
						(definite dall'Autorità per l'energia elettrica e il gas - Delibera 181/06)
					</fo:block>
					
					<fo:table table-layout="fixed" width="100%" space-before="0.5mm">
					<fo:table-column column-width="proportional-column-width(49)" />
					<fo:table-column column-width="proportional-column-width(51)" />
					<fo:table-body>
						<fo:table-row keep-with-previous="always" text-align="center">
							<fo:table-cell>
								<fo:table table-layout="fixed" width="100%"
										xsl:use-attribute-sets="font.fascia_header"
										border-collapse="separate" space-before="0.5mm"
										keep-with-previous="always">
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									
									<fo:table-body>
										<fo:table-row keep-with-previous="always" text-align="center">
											<fo:table-cell><fo:block>&#160;</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border"><fo:block>0.00-7.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>7.00-8.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>8.00-19.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>19.00-23.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>23.00-24.00</fo:block></fo:table-cell>
										</fo:table-row>
										
										<fo:table-row keep-with-previous="always" text-align="center">
											<fo:table-cell>
												<fo:table table-layout="fixed" width="100%">
													<fo:table-column column-width="proportional-column-width(100)" />

													<fo:table-body>
														<fo:table-row>
															<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.top border.bottom">
																<fo:block text-align="center">lun - ven</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">sab</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">dom e festivi</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:table-cell>
											
											<fo:table-cell background-color="{$f3_color}" number-columns-spanned="5" xsl:use-attribute-sets="border">
												<fo:table start-indent="1.485cm" background-color="{$f2_color}" table-layout="fixed" width="4.48cm" xsl:use-attribute-sets="border.left border.right border.bottom">
													<fo:table-column column-width="4.48cm" />

													<fo:table-body>
														<fo:table-row>
															<fo:table-cell display-align="center" padding-left="-1.5cm">
																<fo:table start-indent="3cm" background-color="{$f1_color}" table-layout="fixed" width="1.48cm" xsl:use-attribute-sets="border.left border.bottom border.right">
																	<fo:table-column column-width="1.48cm" />

																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell display-align="center" padding-left="-3cm">
																				<fo:block font-family="universcbold">F1</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
																
																<fo:block text-align="center" font-family="universcbold">F2</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
												
												<fo:block><fo:inline font-family="universcbold">F3</fo:inline>
												<xsl:if test="(not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Energit per Noi') and
															  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,7)='Ecoluce') and
															  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,5)='UNICA') and
															  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,11)='EnergitCASA'))
															  or
															  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Ecoluce Azienda'">
												<fo:inline font-family="universcbold">: Il risparmio aumenta</fo:inline> concentrando i
												consumi di energia in questa fascia!
												</xsl:if>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
							
							<fo:table-cell text-align="left">
								<fo:block xsl:use-attribute-sets="font.legenda" keep-with-previous="always">
									<fo:block>
										<fo:inline font-family="universcbold">F1:</fo:inline>
										ore di punta (peak). Nei giorni dal lunedì al venerdì: dalle
										ore 8.00 alle ore 19.00
									</fo:block>
								
									<fo:block keep-with-previous="always">
										<fo:inline font-family="universcbold">F2:</fo:inline>
										ore intermedie (mid-level). Nei giorni dal lunedì al venerdì:
										dalle ore 7.00 alle ore 8.00 e dalle ore 19.00 alle ore 23.00.
										Il sabato: dalle ore 7.00 alle ore 23.00
									</fo:block>
								
									<fo:block keep-with-previous="always">
										<fo:inline font-family="universcbold">F3:</fo:inline>
										ore fuori punta (off-peak). Nei giorni dal lunedì al venerdì:
										dalle ore 23.00 alle ore 7.00. La domenica e i festivi*: tutte
										le ore della giornata
									</fo:block>
								
									<fo:block font-size="6pt" keep-with-previous="always">
										* Si considerano festivi: 1 e 6 gennaio;
										lunedì di Pasqua; 25 aprile; 1 maggio; 2 giugno;
										15 agosto; 1 novembre; 8, 25 e 26 dicembre.
									</fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
					</fo:table>
				</xsl:if>
			</xsl:if> -->
			
			<!-- Schema fasce di consumo -->
			<!-- <xsl:if test="$mese_fattura &lt; '201304'">
				<xsl:if test="CONTRATTO_ENERGIA">
					<fo:block space-before="5mm" xsl:use-attribute-sets="font.fascia">
						<xsl:if test="not($color='black')">
							<xsl:attribute name="color">#0065ae</xsl:attribute>
						</xsl:if>
						FASCE ORARIE DEI CONSUMI DI ENERGIA
						(definite dall'Autorità per l'energia elettrica e il gas - Delibera 181/06)
					</fo:block>
					
					
					<fo:table table-layout="fixed" width="100%"
							xsl:use-attribute-sets="font.fascia_header"
							border-collapse="separate" space-before="0.5mm"
							keep-with-previous="always">
						<fo:table-column column-width="proportional-column-width(100)" />
						<fo:table-body>
					
					
							<fo:table-row keep-with-previous="always">
								<fo:table-cell>
									<fo:table table-layout="fixed" width="100%">
										<fo:table-column column-width="proportional-column-width(11)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
										<fo:table-column column-width="proportional-column-width(3.5)" />
					
										<fo:table-body>
											<fo:table-row keep-with-previous="always" text-align="center">
												<fo:table-cell><fo:block>&#160;</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border"><fo:block>0-1</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>1-2</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>2-3</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>3-4</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>4-5</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>5-6</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>6-7</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>7-8</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>8-9</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>9-10</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>10-11</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>11-12</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>12-13</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>13-14</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>14-15</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>15-16</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>16-17</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>17-18</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>18-19</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>19-20</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>10-21</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>21-22</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>22-23</fo:block></fo:table-cell>
												<fo:table-cell padding-bottom="-1pt" display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>23-24</fo:block></fo:table-cell>
											</fo:table-row>
											
											<fo:table-row keep-with-previous="always">
												<fo:table-cell>
													<fo:table table-layout="fixed" width="100%">
														<fo:table-column column-width="proportional-column-width(100)" />
					
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.top border.bottom">
																	<fo:block text-align="center">lun - ven</fo:block>
																</fo:table-cell>
															</fo:table-row>
															<fo:table-row keep-with-previous="always">
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																	<fo:block text-align="center">sab</fo:block>
																</fo:table-cell>
															</fo:table-row>
															<fo:table-row keep-with-previous="always">
																<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																	<fo:block text-align="center">dom e festivi</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</fo:table-body>
													</fo:table>
												</fo:table-cell>
					
					
												<fo:table-cell background-color="{$f3_color}"
															   xsl:use-attribute-sets="border" number-columns-spanned="24">
					
													<fo:table table-layout="fixed" width="100%">
														<fo:table-column column-width="proportional-column-width(24.5)" />
														<fo:table-column column-width="proportional-column-width(56)" />
														<fo:table-column column-width="proportional-column-width(3.5)" />
														<fo:table-body>
															<fo:table-row>
					
																<fo:table-cell>
																	<fo:table table-layout="fixed" width="100%">
																		<fo:table-column column-width="proportional-column-width(100)" />
																		<fo:table-body>
																			<fo:table-row>
																				<fo:table-cell padding-top="2pt">
																					<xsl:if test="(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Energit per Noi' or
																								  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,7)='Ecoluce' or
																								  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,5)='UNICA' or
																								  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,11)='EnergitCASA')
																								  and
																								  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Ecoluce Azienda')">															
																						<xsl:attribute name="padding-top">8pt</xsl:attribute>
																					</xsl:if>
																					<fo:block text-align="center" xsl:use-attribute-sets="font.fascia">F3</fo:block>
																					<xsl:if test="(not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Energit per Noi') and
																								  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,7)='Ecoluce') and
																								  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,5)='UNICA') and
																								  not(substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,11)='EnergitCASA'))
																								  or
																								  substring(./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO,1,15)='Ecoluce Azienda'">
																						<fo:block text-align="center" xsl:use-attribute-sets="font.fascia_text"><fo:inline font-family="universcbold">Il risparmio aumenta</fo:inline> concentrando</fo:block>
																						<fo:block text-align="center" xsl:use-attribute-sets="font.fascia_text">i consumi di energia in questa fascia!</fo:block>
																					</xsl:if>
																				</fo:table-cell>
																			</fo:table-row>
																		</fo:table-body>
																	</fo:table>
																</fo:table-cell>
					
																<fo:table-cell>
																	<fo:table table-layout="fixed" width="100%"
																			  xsl:use-attribute-sets="border1.left border1.bottom border1.right"
																			  background-color="{$f2_color}">
																		<fo:table-column column-width="proportional-column-width(3.5)" />
																		<fo:table-column column-width="proportional-column-width(38.5)" />
																		<fo:table-column column-width="proportional-column-width(14)" />
																		<fo:table-body>
																			
																			
																			<fo:table-row>
																				<fo:table-cell>
																					<fo:block text-align="center"/>
																				</fo:table-cell>
																				<fo:table-cell background-color="{$f1_color}" display-align="center" padding-bottom="-1pt"
																					xsl:use-attribute-sets="border1.bottom border1.right border1.left">
																					<fo:block text-align="center" xsl:use-attribute-sets="font.fascia">F1</fo:block>
																				</fo:table-cell>
																				<fo:table-cell display-align="center" padding-bottom="-8pt">
																					<fo:block text-align="center" xsl:use-attribute-sets="font.fascia">F2</fo:block>
																				</fo:table-cell>
																			</fo:table-row>
																			
																			<fo:table-row >
																				<fo:table-cell number-columns-spanned="3">
																					<fo:block>&#160;</fo:block>
																				</fo:table-cell>
																			</fo:table-row>
																		</fo:table-body>
																	</fo:table>
																</fo:table-cell>
					
																<fo:table-cell>
																	<fo:block>&#160;</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</fo:table-body>
													</fo:table>
					
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
									
									
									<fo:block xsl:use-attribute-sets="font.legenda" space-before="1mm" keep-with-previous="always">
										<fo:block>
											<fo:inline font-family="universcbold">F1:</fo:inline>
											ore di punta (peak). Nei giorni dal lunedì al venerdì: dalle
											ore 8.00 alle ore 19.00
										</fo:block>
									
										<fo:block keep-with-previous="always">
											<fo:inline font-family="universcbold">F2:</fo:inline>
											ore intermedie (mid-level). Nei giorni dal lunedì al venerdì:
											dalle ore 7.00 alle ore 8.00 e dalle ore 19.00 alle ore 23.00.
											Il sabato: dalle ore 7.00 alle ore 23.00
										</fo:block>
									
										<fo:block keep-with-previous="always">
											<fo:inline font-family="universcbold">F3:</fo:inline>
											ore fuori punta (off-peak). Nei giorni dal lunedì al venerdì:
											dalle ore 23.00 alle ore 7.00. La domenica e i festivi*: tutte
											le ore della giornata
										</fo:block>
									
										<fo:block font-size="6pt" keep-with-previous="always">
											* Si considerano festivi: 1 gennaio; 6 gennaio;
											lunedì di Pasqua; 25 Aprile; 1 maggio; 2 giugno;
											15 agosto; 1 novembre; 8 dicembre; 25 dicembre;
											26 dicembre
										</fo:block>
									</fo:block>
					
								</fo:table-cell>
							</fo:table-row>
					
						</fo:table-body>
					</fo:table>
				</xsl:if>
			</xsl:if> -->
			
			<xsl:if test="$comunicazione_area_clienti='SI' or
						  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
						  CONTRATTO_ENERGIA[@CONSUMER='SI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
						  RINNOVI or
						  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
						  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
						  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
						  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
						  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
						  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
						  PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI'] or
						  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
						  $mese_fattura &gt; '201107'">
			
								
				<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(100)"/>
					
					<!-- <fo:table-header>
						<fo:table-row height="10mm">
							<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
								</xsl:if>
								<fo:block font-family="universcbold" font-size="11pt" xsl:use-attribute-sets="brd.b.000">
									COMUNICAZIONI AI CLIENTI
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header> -->
					
					<fo:table-body end-indent="0pt" start-indent="0pt">
						<fo:table-row height="3mm">
							<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
								</xsl:if>
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
							
						<fo:table-row keep-with-previous="always">
							<fo:table-cell>
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
								</xsl:if>
								
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									
									<fo:table-header>
										<fo:table-row height="6mm">
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
												
												<fo:block font-family="universcbold" font-size="11pt" xsl:use-attribute-sets="brd.b.000">
													<xsl:if test="not($color='black')">
														<xsl:attribute name="color">#0065ae</xsl:attribute>
													</xsl:if>
													COMUNICAZIONI AI CLIENTI
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-header>
									
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<fo:table-row>
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center" padding-bottom="1mm">
											
												<fo:block xsl:use-attribute-sets="font.comunicazioni"
													margin-left="2mm" margin-right="2mm">
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1'">
														<fo:block font-family="universcbold">
															E’ IL MOMENTO GIUSTO PER INVESTIRE NEL FOTOVOLTAICO ENERGIT!
														</fo:block>
														
														<fo:block keep-with-previous="always">
															<fo:inline font-family="universcbold">Dal 19/02/2013 al 20/06/2013
															per l’installazione degli impianti in Sardegna lei potrà concorrere</fo:inline>,
															se in possesso dei requisiti necessari,
															<fo:inline font-family="universcbold">all’assegnazione di un significativo
															contributo della Regione Sardegna, pari a 700 euro per ogni kW installato.</fo:inline>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Se installa un impianto da 3 kW
															<fo:inline font-family="universcbold">potrà ricevere un contributo
															del valore di 2.100 euro.</fo:inline>
														</fo:block>
														
														<fo:block space-before="2mm" keep-with-previous="always">
															<fo:inline font-family="universcbold">Energit penserà a tutto!</fo:inline>
															Scopra i dettagli su <fo:inline font-family="universcbold">www.energit.it.</fo:inline>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Per maggiori informazioni e per fissare un incontro gratuito con un nostro esperto scriva a
															<fo:inline font-family="universcbold">emancipazioneenergetica@energit.it indicando
															il suo Codice Cliente <xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_CLIENTE" />,</fo:inline>
															oppure ci contatti all’<fo:inline font-family="universcbold">800.19.22.22</fo:inline> utilizzando il
															<fo:inline font-family="universcbold">codice promozione 50</fo:inline>.
														</fo:block>
													</xsl:if>
													
													
													
													
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']">
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1'">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<!-- <xsl:if test="$mese_fattura &lt; '201104'"> -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V1'">
															<fo:block font-family="universcbold">
																Un altro anno di convenienza con Energit per Noi
															</fo:block>
															<fo:block keep-with-previous="always">
																Siamo lieti di comunicarle che Energit per Noi rinnova la sua convenienza!
															</fo:block>
															<fo:block keep-with-previous="always">
																<fo:inline font-family="universcbold">Per tutto il 2011</fo:inline>,
																infatti, il suo contratto per la fornitura di energia elettrica le offre
																un <fo:inline font-family="universcbold">prezzo bloccato della componente energia
																completamente invariato rispetto al 2010.</fo:inline>
															</fo:block>
															<fo:block keep-with-previous="always">
																Buon risparmio con Energit per Noi!
															</fo:block>
														</xsl:if>
														<!-- <xsl:if test="$mese_fattura &gt; '201103' and $mese_fattura &lt; '201108'"> -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V2'">
															<fo:block font-family="universcbold">
																Con Energit per Noi si risparmia sempre!
															</fo:block>
															<fo:block keep-with-previous="always">
																Le ricordiamo che Energit per Noi <fo:inline font-family="universcbold">offre un
																prezzo bloccato della componente energia uguale tutti i giorni e in tutte le
																fasce orarie, 24 ore su 24</fo:inline>.
															</fo:block>
															<fo:block keep-with-previous="always">
																Per risparmiare non occorre quindi concentrare i suoi consumi di energia in
																particolari orari e giorni della settimana: <fo:inline font-family="universcbold">il
																prezzo è lo stesso in F1, F2 e F3</fo:inline>. Comodo, vero?
															</fo:block>
															<fo:block keep-with-previous="always">
																<fo:inline font-family="universcbold">Buon risparmio con Energit per Noi!</fo:inline>
															</fo:block>
														</xsl:if>
													</xsl:if>
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']">
														<xsl:if test="(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<xsl:for-each select="PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']">
															<xsl:call-template name="COMUNICAZIONI_PROMO_TOP">
																<xsl:with-param name="color" select="$color"/>
															</xsl:call-template>
															<xsl:if test="position()&lt;last()">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
														</xsl:for-each>
													</xsl:if>
													
													<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI']">
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block>
															Una parte di quanto i clienti pagano per il servizio elettrico
															serve a promuovere la produzione di energia da fonti rinnovabili
															e assimilate. Per informazioni e approfondimenti su questa
															componente di spesa, visiti il sito www.autorita.energia.it
															o chiami il numero verde 800.166.654.
														</fo:block>
													</xsl:if>
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='PROMO']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<xsl:for-each select="PROMO_SERVIZI[@TIPOLOGIA='PROMO']">
															<fo:block>
																<xsl:value-of select="FRASE" />
															</fo:block>
															<xsl:if test="position()&lt;last()">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
														</xsl:for-each>
														
													</xsl:if>
													
													<xsl:variable name="allegato_exnoi">
														<xsl:if test="./child::RINNOVI/child::RINNOVO/child::ALLEGATO='AL-FAEN0110FX.pdf'">SI</xsl:if>
													</xsl:variable>
													
													<xsl:variable name="altri_allegati">
														<xsl:for-each select="./child::RINNOVI">
															<xsl:for-each select="./child::RINNOVO">
																<xsl:if test="not(./child::ALLEGATO='AL-FAEN0110FX.pdf')">SI</xsl:if>
															</xsl:for-each>
														</xsl:for-each>
													</xsl:variable>		
													
													<xsl:if test="$allegato_exnoi='SI'">
														<xsl:if test="./child::RINNOVI">
															<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or 
																		  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																		  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																		  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																		  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
														
															<xsl:for-each select="RINNOVI">
																<fo:block font-family="universcbold" keep-with-previous="always">
																	Ancora più convenienza con Energit per noi!
																</fo:block>
																<fo:block keep-with-previous="always">
																	Gentile collega,
																</fo:block>
																<fo:block keep-with-previous="always">
																	<fo:inline font-family="universcbold">Energit per noi rinnova la sua convenienza</fo:inline>
																	offrendoLe un
																	<fo:inline font-family="universcbold">prezzo della componente energia imbattibile e bloccato</fo:inline>				
																	fino al 31/12/2010!
																</fo:block>
																<fo:block keep-with-previous="always">
																	In riferimento al Suo contratto
																	<fo:inline font-family="universcbold">Energit per noi</fo:inline>
																	in vigore presso il punto di prelievo riportato in Tabella 1, 
																	con la presente Le comunichiamo la variazione delle specifiche 
																	tecniche del servizio in conformità a quanto previsto dall’art. 
																	7 delle condizioni generali di contratto.
																</fo:block>
																<fo:block keep-with-previous="always">
																	In particolare, il prodotto è stato aggiornato secondo le 
																	caratteristiche specificamente dettagliate nell’allegato A 
																	alla presente comunicazione.
																</fo:block>
																
																<fo:block font-family="universcbold" space-before="2mm" keep-with-previous="always">
																	Tabella 1
																</fo:block>
																
																<fo:table table-layout="fixed" width="100%"
																		  font-size="8pt">
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(25)" />

																	<fo:table-header>
																		<fo:table-row font-family="universcbold">
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Codice contratto</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>POD/Presa</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Prodotto aggiornato</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Data aggiornamento prodotto</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-header>

																	<fo:table-body>
																		<xsl:for-each select="RINNOVO">
																			<xsl:if test="./child::ALLEGATO='AL-FAEN0110FX.pdf'">
																				<fo:table-row>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="CONTRACT_NO" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRESA" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRODOTTO_AGGIORNATO" />
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="DATA_MODIFICA" /></fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																			</xsl:if>
																		</xsl:for-each>
																	</fo:table-body>
																</fo:table>
																
																<fo:block font-family="universcbold" space-before="2mm" keep-with-previous="always">
																	Grazie per aver scelto Energit per noi!
																</fo:block>
															</xsl:for-each>
														</xsl:if>
													</xsl:if>									
													
													<xsl:if test="substring($altri_allegati,1,2)='SI'">
														<xsl:if test="RINNOVI">
															
															<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or 
																		  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																		  $allegato_exnoi='SI' or
																		  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																		  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																		  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
														
															<xsl:for-each select="RINNOVI">
																<fo:block>
																	Gentile Cliente,
																</fo:block>
																<fo:block keep-with-previous="always">
																	con riferimento al
																	contratto di somministrazione di energia elettrica tra noi in
																	vigore presso il/i punto di prelievo/i riportato/i in Tabella 
																	<xsl:if test="$allegato_exnoi='SI'">2</xsl:if>
																	<xsl:if test="not($allegato_exnoi='SI')">1</xsl:if>
																	la presente comunicazione è una variazione delle specifiche tecniche
																	del Servizio in conformità a quanto previsto dall'articolo 7 delle
																	Condizioni Generali di Contratto.
																</fo:block>
																<fo:block keep-with-previous="always">
																	In particolare, Le comunichiamo che
																	il suo prodotto sarà aggiornato secondo le caratteristiche
																	specificamente dettagliate nell'allegato "A" alla presente
																	comunicazione.
																</fo:block>
																
																<fo:block font-family="universcbold" space-before="2mm" keep-with-previous="always">
																	Tabella
																	<xsl:if test="$allegato_exnoi='SI'">2</xsl:if>
																	<xsl:if test="not($allegato_exnoi='SI')">1</xsl:if>
																	– Punto/i di prelievo
																</fo:block>
												
																<fo:table table-layout="fixed" width="100%"
																		  font-size="8pt">
																	<fo:table-column column-width="proportional-column-width(15)" />
																	<fo:table-column column-width="proportional-column-width(15)" />
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(25)" />
																	<fo:table-column column-width="proportional-column-width(20)" />
												
																	<fo:table-header>
																		<fo:table-row font-family="universcbold">
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Codice contratto</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>POD/Presa</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Prodotto di origine</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Prodotto aggiornato</fo:block>
																			</fo:table-cell>
																			<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																				<fo:block>Data aggiornamento</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-header>
												
																	<fo:table-body>
																		<xsl:for-each select="RINNOVO">
																			<xsl:if test="not(./child::ALLEGATO='AL-FAEN0110FX.pdf')">
																				<fo:table-row>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="CONTRACT_NO" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRESA" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRODOTTO_ORIGINE" /></fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="PRODOTTO_AGGIORNATO" />
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell xsl:use-attribute-sets="cell.rinnovi">
																						<fo:block><xsl:value-of select="DATA_MODIFICA" /></fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																			</xsl:if>
																		</xsl:for-each>
																	</fo:table-body>
																</fo:table>
												
																<fo:block space-before="2mm" keep-with-previous="always">
																	Le ricordiamo che, in base all'articolo 7.2 delle
																	Condizioni Generali di Contratto, nel caso in cui
																	non intenda accettare le nuove condizioni contrattuali,
																	potrà recedere dal Contratto con comunicazione scritta
																	(anche via fax) che dovrà pervenirci entro 60 (sessanta)
																	giorni prima della data di scadenza del periodo
																	contrattuale annuo in corso.
																</fo:block>
															</xsl:for-each>
														</xsl:if>
													</xsl:if>
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															ENERGIT PREMIA LA FEDELTA' CON PIU' RISPARMI:
														</fo:block>
														
														<fo:block font-family="universcbold" keep-with-previous="always">
															DAL 1/01/2010 PER LEI BEN 8 EURO
															DI SCONTO PER OGNI MWh CONSUMATO!
														</fo:block>
														
														<fo:block keep-with-previous="always">
															A otto anni dalla sua nascita Energit premia
															i Clienti che hanno contribuito a renderla
															protagonista del mercato dell'energia elettrica in Italia.
														</fo:block>
														
														<fo:block font-family="universcbold" keep-with-previous="always">
															Ai suoi clienti più fedeli, come lei, Energit sta offrendo,
															dal 1 gennaio 2010, ben 8 euro di sconto per ogni
															megawattora consumato.
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Questo bonus
															<fo:inline font-family="universcbold">
																ha incrementato mediamente il
																risparmio già garantito in bolletta
															</fo:inline>
															(prezzo della componente energia Energit)
															<fo:inline font-family="universcbold">
																di un ulteriore 10% di
																sconto.
															</fo:inline>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Per eventuali chiarimenti il nostro Servizio Clienti è a sua
															disposizione (email
															<fo:inline font-family="universcbold">energia@energit.it</fo:inline>
															- tel.
															<fo:inline font-family="universcbold">800.19.22.22</fo:inline>).
														</fo:block>
														
														<fo:block font-family="universcbold" keep-with-previous="always">
															Energit, una scelta che paga.
														</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR']">
														
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block>
															Gentile Cliente,
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Oggetto: <fo:inline font-family="universcbold">ERRATA CORRIGE</fo:inline>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															La presente per informarLa che la proposta di contratto
															per la fornitura di energia elettrica da Lei sottoscritta
															con la Nostra Società conteneva un'erronea indicazione
															numerica delle clausole delle Condizioni Generali che Lei,
															ai sensi e per gli effetti degli Artt. 1341 e 1342 Cod.
															Civ. ha dichiarato di avere letto attentamente, accettato
															e sottoscritto espressamente.
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Pertanto riportiamo qui di seguito, per Sua corretta
															informazione, la dichiarazione da Lei sottoscritta con
															l'esatta indicazione numerica delle clausole da Lei
															espressamente accettate.
														</fo:block>
														
														<fo:block text-align="center" keep-with-previous="always">
															****
														</fo:block>
														
														<fo:block  keep-with-previous="always" font-family="universccors">
															Ai sensi e per gli effetti degli Articoli 1341 e 1342
															c.c., il sottoscritto dichiara di avere letto attentamente
															e di accettare espressamente i seguenti punti delle
															Condizioni Generali riportate sul retro:
															<fo:inline font-family="universcbold">Art. 2.2 </fo:inline>
															Esclusiva;
															<fo:inline font-family="universcbold">Art. 6 </fo:inline>
															Conclusione del Contratto;
															<fo:inline font-family="universcbold">Art. 7 </fo:inline>
															Durata, recesso e multa penitenziale;
															<fo:inline font-family="universcbold">Art. 8 </fo:inline>
															Variazione delle specifiche tecniche e delle condizioni del Contratto;
															<fo:inline font-family="universcbold">Art. 9.5 </fo:inline>
															Aggiornamento del corrispettivo;
															<fo:inline font-family="universcbold">Art. 10.6 </fo:inline>
															Limitazione della facoltà di opporre eccezioni e obbligo di pagamento
															delle fatture contestate;
															<fo:inline font-family="universcbold">Art. 11.1 </fo:inline>
															Limitazioni del Servizio;
															<fo:inline font-family="universcbold">Art. 12 </fo:inline>
															Sospensione del Servizio;
															<fo:inline font-family="universcbold">Art. 13 </fo:inline>
															Risoluzione ai sensi del 1456 c.c;
															<fo:inline font-family="universcbold">Art. 14 </fo:inline>
															Limitazione di responsabilità di Energit;
															<fo:inline font-family="universcbold">Art. 15 </fo:inline>
															Uso improprio del Servizio;
															<fo:inline font-family="universcbold">Art. 16 </fo:inline>
															Trattamento dati personali D.Lgs. 196/03;
															<fo:inline font-family="universcbold">Art. 17 </fo:inline>
															Riservatezza, pubblicità;
															<fo:inline font-family="universcbold">Art. 18 </fo:inline>
															Reclami;
															<fo:inline font-family="universcbold">Art. 21 </fo:inline>
															Cessione del Contratto;
															<fo:inline font-family="universcbold">Art. 23 </fo:inline>
															Controversie e foro competente.
														</fo:block>
														
														<fo:block  space-before="2mm" keep-with-previous="always" text-align="center">
															****
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Cordiali Saluti,
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Energ.it S.p.A.
														</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block text-align="justify">
															<fo:block text-align="left" font-family="universcbold">Nuove Condizioni Generali Energit S.p.A. - Scheda di Riepilogo degli effetti della variazione contrattuale proposta</fo:block>
															<fo:block space-before="2mm" keep-with-previous="always" text-align="left">Gentile cliente,</fo:block>
															<fo:block keep-with-previous="always" space-before="2mm">con riferimento alle Condizioni Generali di Contratto per la fornitura di energia elettrica e dei servizi associati (“<fo:inline font-family="universcbold">CGC</fo:inline>”) attualmente in vigore, comunichiamo con la presente  l’aggiornamento degli articoli 4.4, 4.5, 6.10, 7.8, 13.1, 23.1.</fo:block>
															<fo:block>La scheda di riepilogo sotto riportata è redatta ai sensi e per gli effetti dell'articolo 12.3 della delibera AEEG n. 105/06, ai sensi del quale in caso di variazione unilaterale di clausole contrattuali l'esercente la vendita deve, tra l'altro, fornire "l'illustrazione chiara, completa e comprensibile dei contenuti e degli effetti della variazione proposta".</fo:block>
															<fo:block>Dal punto di vista prettamente formale, si segnala che gli articoli 4.4, 4.5, 6.10, 7.8, 13.1, 23.1 delle CGC per la fornitura di energia elettrica e dei servizi associati stipulate con Energit S.p.A., come attualmente in vigore, saranno modificati per effetto dell'adozione delle nuove condizioni generali, applicabili dal <xsl:value-of select="./child::PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13']/child::FRASE"/> (le "<fo:inline font-family="universcbold">Nuove Condizioni Generali di Contratto</fo:inline>" o "<fo:inline font-family="universcbold">NCGC</fo:inline>").</fo:block>
															<fo:block>In conformità a quanto disposto dalla normativa applicabile, il presente documento si limita a specificare i contenuti e gli effetti sui diritti ed obblighi delle Parti ai sensi delle CGC che deriveranno dall'adozione delle NCGC.</fo:block>
															<fo:block>La modifica delle CGC <fo:inline text-decoration="underline">non comporta alcuna modifica al corrispettivo attualmente previsto per la fornitura di energia elettrica.</fo:inline></fo:block>
															<fo:block>Salvo ove diversamente specificato, i termini con iniziale maiuscola utilizzati nel presente documento avranno il medesimo significato loro attribuito nelle CGC e/o nelle NCGC.</fo:block>
															<fo:block space-before="2mm">Le clausole cui fa riferimento la tabella sotto riportata sono quelle previste dalle Nuove Condizioni Generali di Contratto.</fo:block>
														</fo:block>
														
														<fo:table widows="10" orphans="10" font-size="8pt" space-before="2mm" end-indent="0pt" table-layout="fixed" width="100%">
														<fo:table-column column-width="proportional-column-width(70)"/>
														<fo:table-column column-width="proportional-column-width(30)"/>
															
															<fo:table-body end-indent="0pt" start-indent="0pt" text-align="justify">
																
																<fo:table-row font-family="universcbold" text-align="center">
																	<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>SCHEDA RIEPILOGATIVA DELLE MODIFICHE PROPOSTE</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row keep-with-previous="always" font-family="universcbold">
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>Articolo</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>ILLUSTRAZIONE DEL CONTENUTO E DEGLI EFFETTI DELLA VARIAZIONE CONTRATTUALE PROPOSTA</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row keep-with-previous="always">
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors" padding="3.5pt">
																		<fo:block><fo:inline font-family="universcbold">4.4</fo:inline> Energit trasmette le richieste del Cliente al Distributore Locale, che comunque rimarrà responsabile per l’esecuzione (o la mancata esecuzione) delle prestazioni richieste. Energit cesserà di dare corso alle richieste del Cliente alla data di cessazione del Contratto per qualsivoglia causa.</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>La modifica riguarda le modalità di gestione delle pratiche di relative alla “connessione”. Energit non darà più corso alle suddette pratiche rimaste inevase alla data di cessazione del contratto, dal momento che a tale data il punto di prelievo oggetto del contratto non sarà più nel dispacciamento di Energit.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors" padding="3.5pt">
																		<fo:block><fo:inline font-family="universcbold">4.5</fo:inline> In ogni caso, il Cliente è tenuto al pagamento dei costi richiesti dal Distributore Locale per lo svolgimento delle prestazioni richieste - direttamente o per il tramite di Energit. Qualora Energit provveda al pagamento di tali costi, il Cliente sarà tenuto a rimborsarli ad Energit. In aggiunta a tali costi, Energit addebiterà al Cliente in fattura un corrispettivo fisso a titolo di contributo per le spese di gestione di ciascuna richiesta di prestazione di cui all’articolo 4.1.</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>In aggiunta ai costi previsti dal distributore locale per l’avvio delle pratiche di “connessione”, Energit addebiterà un corrispettivo a titolo di contributo spese. L’importo di tale corrispettivo verrà indicato nel modulo di apertura pratica.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors">
																		<fo:table space-before="5mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
																		<fo:table-column column-width="proportional-column-width(100)"/>
																			<fo:table-body end-indent="0pt" start-indent="0pt">
																				<fo:table-row>
																					<fo:table-cell xsl:use-attribute-sets="brd.b.000" padding="3.5pt">
																						<fo:block><fo:inline font-family="universcbold">6.10</fo:inline> A seguito dello svolgimento delle attività di cui al precedente articolo 6.9, Energit comunica al Cliente, con almeno 5 (cinque) giorni di preavviso, la data di avvio del Servizio mediante apposita comunicazione in forma scritta.</fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																				<fo:table-row keep-with-previous="always">
																					<fo:table-cell padding="3.5pt">
																						<fo:block><fo:inline font-family="universcbold">13.1</fo:inline> In aggiunta ai rimedi di cui al precedente articolo 12, Energit avrà diritto di risolvere il Contratto ai sensi dell’articolo 1456 del Codice Civile, previa comunicazione al Cliente in forma scritta e con conseguente interruzione del Servizio, in caso di inadempimento del Cliente a ciascuna delle obbligazioni di cui agli articoli: 6.6 (dichiarazioni e garanzie), 10.4 (mancato pagamento di una fattura nel termine indicato in fattura), 11 (mezzi di garanzia), 12.1 (omesso pagamento nel termine di cui all'articolo 12.1, lett. (i)) 15 (uso improprio del Servizio), 17 (riservatezza) e 21 (cessione del Contratto) del Contratto.</fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																			</fo:table-body>
																		</fo:table>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>In entrambi gli articoli in oggetto è stata eliminata l’indicazione delle modalità di invio delle comunicazioni oggetto degli articoli, fermo restando l’obbligo di effettuare tali comunicazioni in forma scritta.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors" padding="3.5pt">
																		<fo:block><fo:inline font-family="universcbold">7.8</fo:inline> Nel caso in cui il Cliente non rispetti i termini e le condizioni previsti dal presente articolo 7 per l'esercizio del diritto di recesso e, ciononostante, tale recesso risulti efficace e venga avviata una fornitura da parte di un terzo fornitore presso i Siti del Cliente, il Cliente sarà tenuto a corrispondere ad Energit un corrispettivo per l'esercizio illegittimo del diritto di recesso pari ad Euro 0,06 per ogni KWh fatturato al Cliente nei tre mesi precedenti il mese in cui è cessato il Servizio, maggiorato di un importo pari ad euro 100, che sarà oggetto di fatturazione da parte di Energit e di pagamento da parte del Cliente in conformità a quanto previsto dal successivo articolo 10.</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>Il corrispettivo per l’esercizio illegittimo del diritto di recesso da parte del Cliente viene modificato con riferimento alla componente fissa del medesimo.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" font-family="universccors" padding="3.5pt">
																		<fo:block><fo:inline font-family="universcbold">23.1</fo:inline> Per qualunque controversia nascente dal Contratto (a titolo esemplificativo e non esaustivo, dalla sua validità, efficacia, interpretazione, esecuzione, risoluzione,rescissione) sarà competente in via esclusiva il foro di Cagliari.</fo:block>
																	</fo:table-cell>
																	<fo:table-cell xsl:use-attribute-sets="brd.000" padding="3.5pt">
																		<fo:block>Il foro per eventuali controversie viene trasferito da Milano a Cagliari.</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																
															</fo:table-body>
														</fo:table>
														
														<fo:block space-before="2mm" text-align="justify" keep-with-previous="always">Le ricordiamo inoltre che, qualora non intenda accettare le modifiche sopra riportate avrà in ogni caso il diritto di recedere dalle CGC entro 15 (quindici) giorni dalla data di comunicazione delle modifiche, senza penali e con effetto immediato, fatti i salvi i tempi tecnici previsti per le comunicazioni con il distributore locale, mediante comunicazione scritta da inviarsi a mezzo raccomandata A/R, Fax o PEC agli indirizzi indicati all’articolo 19.1 delle CGC.</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															<xsl:if test="not($color='black')">
																<xsl:attribute name="color">#0065ae</xsl:attribute>
															</xsl:if>
															COMUNICAZIONE DEI DATI CATASTALI
														</fo:block>
													
														<fo:block keep-with-previous="always">
															Gentile Cliente,
														</fo:block>
														
														<fo:block keep-with-previous="always">
															<fo:inline font-family="universcbold">
															non risulta ancora pervenuta la comunicazione dei dati catastali
															identificativi dell'immobile/terreno presso cui è attiva la sua
															fornitura di energia</fo:inline>. Le ricordiamo che la
															responsabilità dell'invio di questi dati, che saranno poi trasmessi
															all'Anagrafe tributaria, è a Suo carico.
															<fo:inline font-family="universcbold">
																La invitiamo a compilare subito l'apposito modulo
																che trova nella sua Area Clienti
															</fo:inline>
															e inviarlo ad Energit! Grazie.
														</fo:block>
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS']/child::ID='PAPERLESS_V1'">
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																PARTECIPI AL PROGETTO PAPERLESS!
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Scelga la comodità di
																<fo:inline font-family="universcbold">
																ricevere la fattura nella sua casella di posta elettronica:
																</fo:inline>
																con la fattura via email contribuirà alla difesa dell’ambiente
																e potrà creare un comodo e ordinato archivio elettronico sul suo PC.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																La fattura via email è:
															</fo:block>
															
															<fo:list-block keep-with-previous="always" provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
																<fo:list-item>
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">RISPETTOSA DELL’AMBIENTE</fo:inline>:
																			permette di risparmiare carta e quindi di salvare numerosi alberi
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
																
																<fo:list-item keep-with-previous="always">
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">GRATUITA</fo:inline>:
																			non ha alcun costo per lei e le permetterà di risparmiare le spese di spedizione del documento cartaceo, pari ad 1,50 € per fattura
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
																
																<fo:list-item keep-with-previous="always">
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">INNOVATIVA</fo:inline>:
																			potrà conservarla ed archiviarla comodamente nel suo PC
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
																
																<fo:list-item keep-with-previous="always">
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">RAPIDA</fo:inline>:
																			la riceverà immediatamente nella sua casella email
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
																
																<fo:list-item keep-with-previous="always">
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">AFFIDABILE</fo:inline>:
																			sarà disponibile in qualsiasi momento anche nella sua Area Riservata
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
															</fo:list-block>
															
															<fo:block keep-with-previous="always">
																Passi subito alla fattura via email!
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Invii la richiesta a
																<fo:inline font-family="universcbold">paperless@energit.it</fo:inline>,
																specificando il suo codice cliente <xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_CLIENTE" />.
															</fo:block>
														</xsl:if>
														
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS']/child::ID='PAPERLESS_V2'">
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																SCELGA LA FATTURA VIA EMAIL: RISPARMIERA’ FINO A 18 EURO ALL’ANNO!
															</fo:block>
															
															<fo:block space-before="2mm" font-family="universcbold">
																Desidera ulteriori risparmi? Scelga di ricevere la fattura nella sua casella di posta elettronica!
															</fo:block>
															
															<fo:block keep-with-previous="always">
																La fattura via email è:
															</fo:block>
															
															<fo:list-block keep-with-previous="always" provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
																<fo:list-item keep-with-previous="always">
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">GRATUITA E CONVENIENTE</fo:inline>:
																			non ha alcun costo per lei e le permetterà di risparmiare le spese di
																			spedizione del documento cartaceo, pari ad 1,50 € per fattura.
																			Fino a <fo:inline font-family="universcbold">18 euro</fo:inline> all’anno!
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
																
																<fo:list-item keep-with-previous="always">
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">RAPIDA</fo:inline>:
																			la riceverà tempestivamente nella sua casella di posta
																			elettronica; mai più ritardi nella ricezione delle fatture!
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
																
																<fo:list-item>
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">RISPETTOSA DELL’AMBIENTE</fo:inline>:
																			permette di risparmiare carta e quindi di salvare numerosi alberi.
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
															</fo:list-block>
															
															<fo:block keep-with-previous="always">
																Tanti Clienti Energit  sono già passati alla fattura via email; la richieda subito anche
																lei scrivendo a <fo:inline font-family="universcbold">paperless@energit.it</fo:inline> e
																specificando il suo codice cliente <xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_CLIENTE" />.
															</fo:block>
														</xsl:if>
														
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS']/child::ID='PAPERLESS_V3'">
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																SCELGA LA FATTURA VIA EMAIL: RISPARMIERA’ FINO A 18 EURO ALL’ANNO!
															</fo:block>
															
															<fo:block space-before="2mm" font-family="universcbold">
																Desidera ulteriori risparmi? Scelga di ricevere la fattura nella sua casella di posta elettronica!
															</fo:block>
															
															<fo:block keep-with-previous="always">
																La fattura via email è:
															</fo:block>
															
															<fo:list-block keep-with-previous="always" provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
																<fo:list-item keep-with-previous="always">
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">GRATUITA E CONVENIENTE</fo:inline>:
																			non ha alcun costo per lei e le permetterà di risparmiare le spese di
																			spedizione del documento cartaceo, pari ad 1,50 € per fattura.
																			Fino a <fo:inline font-family="universcbold">18 euro</fo:inline> all’anno!
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
																
																<fo:list-item keep-with-previous="always">
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">RAPIDA</fo:inline>:
																			la riceverà tempestivamente nella sua casella di posta
																			elettronica; mai più ritardi nella ricezione delle fatture!
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
																
																<fo:list-item>
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">RISPETTOSA DELL’AMBIENTE</fo:inline>:
																			permette di risparmiare carta e quindi di salvare numerosi alberi.
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
																
																<fo:list-item>
																	<fo:list-item-label end-indent="label-end()">
																		<fo:block>-</fo:block>
																	</fo:list-item-label>
																	<fo:list-item-body start-indent="body-start()">
																		<fo:block>
																			<fo:inline font-family="universcbold">A COLORI!</fo:inline>
																		</fo:block>
																	</fo:list-item-body>
																</fo:list-item>
															</fo:list-block>
															
															<fo:block keep-with-previous="always">
																Tanti Clienti Energit  sono già passati alla fattura via email; la richieda subito anche
																lei scrivendo a <fo:inline font-family="universcbold">paperless@energit.it</fo:inline> e
																specificando il suo codice cliente <xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_CLIENTE" />.
															</fo:block>
														</xsl:if>
													</xsl:if>
													
													<!--COMUNICAZIONE ORA LEGALE (RFC 252375)-->
													<xsl:if test="$mese_fattura &gt; '201103' and $mese_fattura &lt; '201105'">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
														   <fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															CON L’ORA LEGALE BOLLETTE PIU’ LEGGERE E MINORI EMISSIONI DI CO2!
														</fo:block>
													
														<fo:block space-before="2mm" keep-with-previous="always">
															Tra sabato 26 e domenica 27 marzo <fo:inline font-family="universcbold">è
															entrata in vigore l’ora legale, che regala un’ora di più di luce nel pomeriggio
															e permette notevoli risparmi energetici</fo:inline>.
														</fo:block>
														
														<fo:block space-before="2mm" keep-with-previous="always">
															Terna, la società responsabile della gestione dei flussi di energia elettrica
															sulla rete di trasmissione nazionale, ha infatti previsto, nei 7 mesi di durata
															dell’ora legale, <fo:inline font-family="universcbold">un risparmio complessivo
															dei consumi di energia pari a ben 646,4 milioni di kilowattora</fo:inline>.
														</fo:block>
														
														<fo:block space-before="2mm" keep-with-previous="always">
															Il minor consumo di energia si potrà apprezzare anche
															nella <fo:inline font-family="universcbold">riduzione di CO2: oltre 300 mila
															tonnellate di anidride carbonica in meno nell’atmosfera!</fo:inline>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Per informazioni e approfondimenti <fo:inline text-decoration="underline">www.terna.it</fo:inline>
														</fo:block>
													</xsl:if>
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
														   <fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE']/child::ID='RECS_V1'">
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																GRAZIE PER AVER SCELTO L'ENERGIA VERDE CERTIFICATA RECS!
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Gentile Cliente,
															</fo:block>
															
															<fo:block keep-with-previous="always">
																nel complimentarci per la sua sensibilità nei confronti
																dell'ambiente abbiamo il piacere di confermarle che
																Energit ha compensato i suoi consumi di energia elettrica
																del 2009 con una corrispondente quantità di certificati
																RECS, che attestano la produzione di energia elettrica
																da fonti rinnovabili come l'acqua, il vento e il sole.
															</fo:block>
														</xsl:if>
														
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE']/child::ID='RECS_V2' or 
																	  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE']/child::ID='RECS_V3'">
															<fo:table end-indent="0pt" table-layout="fixed" width="100%">
																<fo:table-column column-width="proportional-column-width(85)"/>
																<fo:table-column column-width="proportional-column-width(15)"/>
																<fo:table-body end-indent="0pt" start-indent="0pt">
																	<fo:table-row>
																		<fo:table-cell>
																			<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE']/child::ID='RECS_V2'">
																				<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
																			</xsl:if>
																			
																			<fo:block font-family="universcbold">
																				<xsl:if test="not($color='black')">
																					<xsl:attribute name="color">#0065ae</xsl:attribute>
																				</xsl:if>
																				LA SUA ENERGIA VERDE E’ CERTIFICATA!
																			</fo:block>
																			
																			<fo:block keep-with-previous="always">
																				La ringraziamo per il suo impegno verso la sostenibilità ambientale
																				e la informiamo che i suoi consumi di energia elettrica del 2010 sono
																				stati compensati con una quantità corrispondente di certificati RECS
																				(Renewable Energy Certificate System), che attestano la produzione di
																				energia da fonti rinnovabili al 100%.
																			</fo:block>
																			
																			<fo:block keep-with-previous="always">
																				Per informazioni e approfondimenti www.recs.org
																			</fo:block>
																		</fo:table-cell>
																		
																		<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE']/child::ID='RECS_V3'">																				
																			<fo:table-cell text-align="center" padding-top="13mm">
																				<fo:block>
																					<xsl:if test="$color='black'">
																						<fo:external-graphic src="url(img/LogoRecs_grigi.svg)" content-height="16mm" />
																					</xsl:if>
																					<xsl:if test="not($color='black')">
																						<fo:external-graphic src="url(img/LogoRecs_colore.svg)" content-height="16mm" />
																					</xsl:if>
																				</fo:block>
																				
																				<fo:block keep-with-previous="always" font-size="6pt">
																					Energit is a member of RECS International
																				</fo:block>
																			</fo:table-cell>
																		</xsl:if>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
														</xsl:if>
														
													</xsl:if>
													
													
													<xsl:for-each select="PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO']">
														<xsl:if test="./ancestor::DOCUMENTO/child::CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  ./ancestor::DOCUMENTO/child::RINNOVI or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or 
																	  ./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
																	  position()&gt;1 or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
														   <fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block widows="10" orphans="10">
														<xsl:if test="./child::FRASE='KO'">
															<fo:inline font-family="universcbold">
															<xsl:if test="not($color='black')">
																<xsl:attribute name="color">#0065ae</xsl:attribute>
															</xsl:if>
															BONUS ELETTRICO</fo:inline>:
															Con riferimento al contratto
															<xsl:value-of select="./child::NOTE"/>,
															la informiamo che la richiesta di ammissione
															della sua fornitura alla compensazione della
															spesa per la fornitura di energia elettrica è
															stata rigettata. Per maggiori informazioni la
															invitiamo a contattare il suo comune di residenza.
														</xsl:if>
														<xsl:if test="not(./child::FRASE='KO')">
															<fo:inline font-family="universcbold">
															<xsl:if test="not($color='black')">
																<xsl:attribute name="color">#0065ae</xsl:attribute>
															</xsl:if>
															BONUS ELETTRICO</fo:inline>:
															Con riferimento al contratto
															<xsl:value-of select="./child::NOTE"/>,
															la informiamo che la sua fornitura è ammessa alla
															compensazione della spesa per la fornitura di energia
															elettrica ai sensi del decreto ministeriale 28 dicembre
															2007. La richiesta di rinnovo deve essere effettuata
															entro <xsl:value-of select="./child::FRASE"/>.
														</xsl:if>
														</fo:block>
													</xsl:for-each>
													
													<xsl:if test="$comunicazione_area_clienti='SI'">
													
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<fo:block font-family="universcbold">
															<xsl:if test="not($color='black')">
																<xsl:attribute name="color">#0065ae</xsl:attribute>
															</xsl:if>
															UN MONDO DI NUOVI SERVIZI DA SCOPRIRE
															NELL’AREA CLIENTI ENERGIT!
														</fo:block>
														
														<fo:block font-family="universcbold" keep-with-previous="always">
															Scopra tutte le novità dell’Area Clienti su www.energit.it!
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Questo prezioso strumento sapra’ stupirla con un mondo
															di informazioni, servizi e tanto altro ancora.
															Nella sua Area Clienti potrà, per esempio:
														</fo:block>
														
														<fo:block start-indent="5mm" font-family="universcbold" keep-with-previous="always">
															<fo:block>
																<fo:inline> - Avere accesso a promozioni esclusive</fo:inline>
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Visualizzare i consumi di energia, anche nel dettaglio</fo:inline>
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Trovare utili suggerimenti per risparmiare</fo:inline>
															</fo:block>
															
															<xsl:if test="AUTOLETTURA">
																<fo:block keep-with-previous="always">
																	<fo:inline> - Comunicare la lettura del contatore</fo:inline>
																</fo:block>
															</xsl:if>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Consultare le fatture e verificare lo stato dei pagamenti</fo:inline>
															</fo:block>
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS']">
																<fo:block keep-with-previous="always">
																	<fo:inline> - Passare alla fattura via email per contribuire alla difesa dell’ambiente</fo:inline>
																</fo:block>
															</xsl:if>
															
															<fo:block keep-with-previous="always">
																<fo:inline> - Scaricare utile modulistica</fo:inline>
															</fo:block>
														</fo:block>
														
														<fo:block keep-with-previous="always">
															Acceda subito all’Area Clienti su www.energit.it attraverso la sua
															<fo:inline font-family="universcbold">UserID </fo:inline>
															<xsl:value-of select="USERNAME" /> : scoprirà un modo nuovo
															e ancora più efficiente per comunicare con Energit.
														</fo:block>
														
													</xsl:if>
													
													
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='REGISTRAZIONE_DATI_V1' or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA'or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA_V1'
																  
																  or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='BETA'
																  
																  or
																  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='APPUNTAMENTI_IN_SEDE'
																  ">
													
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
																	  $comunicazione_area_clienti='SI' or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1')">
															
															
															<fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
															
														</xsl:if>
														
														<!-- GUIDA ALLA LETTURA DELLA FATTURA -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1'">
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																NOVITA’: LA GUIDA ALLA LETTURA DELLA FATTURA ENERGIT NELLA SUA AREA CLIENTI
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Desidera sapere tutto sulla fattura Energit?
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline font-family="universcbold">Acceda alla sua Area Clienti
																su www.energ.it tramite la sua
																UserID <xsl:value-of select="USERNAME" /> e la password: potrà
																scaricare la Guida alla lettura della fattura Energit</fo:inline>, che
																contiene tante utili informazioni e risposte alle sue domande.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Se non ricorda la password o non ne è ancora in possesso, segua le
																indicazioni che troverà facendo click sul link “Hai smarrito la password?”
																su www.energit.it 
															</fo:block>
															
															<fo:block font-family="universcbold">
																La aspettiamo in Area Clienti!
															</fo:block>
														</xsl:if>
														
														<!-- COMUNICAZIONE PER MANDATO CONNESSIONE -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1'">
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1'">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
															
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																DESIDERA EFFETTUARE NUOVI ALLACCI, AUMENTI O RIDUZIONI DI POTENZA,
																ALTRE MODIFICHE? SIAMO A SUA DISPOSIZIONE!
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Le ricordiamo
																che <fo:inline font-family="universcbold"> la modulistica per le sue
																richieste di connessione alla rete è sempre disponibile nella sua Area
																Clienti</fo:inline> Energit su www.energit.it, alla quale potrà accedere
																tramite la sua UserID <xsl:value-of select="USERNAME" /> e la password.
															</fo:block>
														</xsl:if>
														
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2'">
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1'">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
															
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																DESIDERA EFFETTUARE NUOVI ALLACCI, AUMENTI O RIDUZIONI DI POTENZA,
																ALTRE MODIFICHE? SIAMO A SUA DISPOSIZIONE!
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Le ricordiamo
																che <fo:inline font-family="universcbold"> la modulistica per le sue richieste
																di connessione alla rete e le informazioni sugli importi richiesti sono disponibili
																nella sua Area Clienti</fo:inline> Energit su www.energit.it, alla quale potrà
																accedere tramite la sua UserID <xsl:value-of select="USERNAME" /> e la password.
															</fo:block>
														</xsl:if>
														
														<!-- REGISTRAZIONE DATI -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='REGISTRAZIONE_DATI_V1'">
															
															<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::EMAIL) or not(./child::ANAGRAFICA_DOCUMENTO/child::CELLULARE)">
																<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1' or
																			  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1' or
																			  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2'">
																	<fo:block xsl:use-attribute-sets="block.separatore">
																		&#160;
																	</fo:block>
																</xsl:if>
															</xsl:if>
															
															<!-- EMAIL e CELL MANCANTI -->
															<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::EMAIL) and not(./child::ANAGRAFICA_DOCUMENTO/child::CELLULARE)">
																
																<fo:block font-family="universcbold">
																	<xsl:if test="not($color='black')">
																		<xsl:attribute name="color">#0065ae</xsl:attribute>
																	</xsl:if>
																	ATTIVI NUOVI CANALI DI COMUNICAZIONE CON ENERGIT!
																</fo:block>
																
																<fo:block keep-with-previous="always" font-family="universcbold">
																	La invitiamo a comunicarci il suo indirizzo email e il numero di cellulare,
																	per ricevere rapidamente utili e importanti informazioni
																	relative <xsl:if test="RIEPILOGO_MULTISITO_ENERGIA">ai
																	suoi contratti</xsl:if><xsl:if test="not(RIEPILOGO_MULTISITO_ENERGIA)">al
																	suo contratto</xsl:if>.
																</fo:block>
																
																<fo:block keep-with-previous="always">
																	Invii una richiesta firmata al numero di fax gratuito 800.19.22.55
																	o all’indirizzo energia@energit.it, specificando i dati da registrare
																	e il suo Codice Cliente <xsl:value-of select="CODICE_CLIENTE" />. Grazie.
																</fo:block>
															</xsl:if>
															
															<!-- EMAIL MANCANTE CELL PRESENTE -->
															<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::EMAIL) and ./child::ANAGRAFICA_DOCUMENTO/child::CELLULARE">
																
																<fo:block font-family="universcbold">
																	<xsl:if test="not($color='black')">
																		<xsl:attribute name="color">#0065ae</xsl:attribute>
																	</xsl:if>
																	ATTIVI UN NUOVO CANALE DI COMUNICAZIONE CON ENERGIT!
																</fo:block>
																
																<fo:block keep-with-previous="always" font-family="universcbold">
																	La invitiamo a comunicarci il suo indirizzo email, per ricevere rapidamente utili
																	e importanti informazioni relative <xsl:if test="RIEPILOGO_MULTISITO_ENERGIA">ai
																	suoi contratti</xsl:if><xsl:if test="not(RIEPILOGO_MULTISITO_ENERGIA)">al
																	suo contratto</xsl:if>.
																</fo:block>
																
																<fo:block keep-with-previous="always">
																	Invii una richiesta firmata al numero di fax gratuito 800.19.22.55
																	o all’indirizzo energia@energit.it, specificando l’email da registrare
																	e il suo Codice Cliente <xsl:value-of select="CODICE_CLIENTE" />. Grazie.
																</fo:block>
															</xsl:if>
															
															<!-- EMAIL PRESENTE CELL MANCANTE -->
															<xsl:if test="./child::ANAGRAFICA_DOCUMENTO/child::EMAIL and not(./child::ANAGRAFICA_DOCUMENTO/child::CELLULARE)">
																
																<fo:block font-family="universcbold">
																	<xsl:if test="not($color='black')">
																		<xsl:attribute name="color">#0065ae</xsl:attribute>
																	</xsl:if>
																	ATTIVI UN NUOVO CANALE DI COMUNICAZIONE CON ENERGIT!
																</fo:block>
																
																<fo:block keep-with-previous="always" font-family="universcbold">
																	La invitiamo a comunicarci il suo numero di cellulare, per ricevere rapidamente
																	utili e importanti informazioni relative <xsl:if test="RIEPILOGO_MULTISITO_ENERGIA">ai
																	suoi contratti</xsl:if><xsl:if test="not(RIEPILOGO_MULTISITO_ENERGIA)">al
																	suo contratto</xsl:if>.
																</fo:block>
																
																<fo:block keep-with-previous="always">
																	Invii una richiesta firmata al numero di fax gratuito 800.19.22.55
																	o all’indirizzo energia@energit.it, specificando il numero di rete mobile
																	da registrare e il suo Codice Cliente <xsl:value-of select="CODICE_CLIENTE" />.
																	Grazie.
																</fo:block>
															</xsl:if>
														</xsl:if>
														
														<!-- SEPA -->
														<!-- 
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA' or
																	  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA_V1'">
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1' or
																		  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1' or
																		  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2' or
																		  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='REGISTRAZIONE_DATI_V1'">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
															
															
															
															
															
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																MIGRAZIONE OBBLIGATORIA DEGLI STRUMENTI DI PAGAMENTO NAZIONALI ALL’AREA UNICA DEI PAGAMENTI IN EURO (SEPA)
															</fo:block>
															
															<fo:block space-before="1mm" xsl:use-attribute-sets="font.sepa">
																<fo:block space-before="1mm" keep-with-previous="always">
																	Negli ultimi anni i Paesi europei si sono impegnati nella realizzazione della SEPA
																	(acronimo di Single Euro Payments Area, l'Area unica dei pagamenti in euro) con
																	l'obiettivo di offrire ai cittadini, alle imprese e alle pubbliche amministrazioni
																	la possibilità di effettuare e ricevere pagamenti in euro senza più alcuna differenza
																	tra pagamenti nazionali ed europei.
																</fo:block>
																
																<fo:block space-before="1mm" keep-with-previous="always">
																	Ciò grazie all'adozione, da parte di tutti i Paesi, di nuovi strumenti di pagamento
																	comuni da utilizzare per disporre operazioni di addebito diretto e di bonifico
																	caratterizzati da un insieme di regole e meccanismi di funzionamento condivisi
																	dalle comunità bancarie di tutti i Paesi aderenti alla SEPA.
																</fo:block>
																
																<fo:block space-before="1mm" keep-with-previous="always">
																	Il Regolamento UE 260/2012 del Parlamento europeo e del Consiglio del 14 marzo 2012
																	ha previsto la sostituzione obbligatoria entro il 1 febbraio 2014 degli schemi di
																	addebito diretto nazionali e l’adozione di schemi di pagamento europei conformi ai
																	requisiti tecnici e commerciali individuati dal Regolamento medesimo.
																</fo:block>
																
																<fo:block space-before="1mm" keep-with-previous="always">
																	Il Regolamento prevede, inoltre, che le autorizzazioni all’addebito rilasciate al
																	beneficiario del pagamento prima del 1° febbraio 2014 per l’incasso di addebiti
																	diretti periodici secondo uno schema nazionale rimangano valide anche dopo tale data.
																</fo:block>
																
																<fo:block space-before="1mm" keep-with-previous="always">
																	Al fine di rispettare le norme introdotte dalla regolamentazione europea e, nel contempo,
																	mantenere invariate le modalità di pagamento attualmente in essere con la propria Clientela,
																	Energ.it S.p.A. informa che nel corso dei prossimi mesi procederà a porre in essere quanto
																	necessario a consentire la sostituzione, senza soluzione di continuità, delle autorizzazioni
																	all’addebito in conto (c.d. domiciliazioni RID) bancarie e postali in essere con la propria
																	Clientela con i nuovi strumenti di addebito diretto SEPA (denominati SEPA Direct Debit o SDD)
																	conformi ai requisiti del Regolamento UE 260/2012.
																</fo:block>
																
																<fo:block space-before="1mm" keep-with-previous="always">
																	Lei continuerà, comunque, a ricevere le nostre fatture in addebito sulle coordinate
																	bancarie/postali sulle quali abbiamo effettuato gli addebiti sino ad ora e nulla cambierà
																	rispetto a quanto sottoscritto nella sua richiesta RID.
																</fo:block>
																
																<fo:block space-before="1mm" keep-with-previous="always">
																	Per ulteriori informazioni sulla SEPA e sui nuovi strumenti di pagamento europei può rivolgersi
																	al suo fornitore di servizi di pagamento (banca o posta), visitare la sezione SEPA dei siti
																	istituzionali della Banca d’Italia e dell’Associazione e Bancaria Italiana.
																</fo:block>
																
																<fo:block space-before="1mm" keep-with-previous="always">
																	<fo:inline font-family="universcbold">N.B.</fo:inline> Gli altri servizi di pagamento come Ri.Ba., MAV, RAV e i Bollettini bancari e
																	postali - che non trovano una diretta corrispondenza con i servizi di addebito e di
																	bonifico SEPA – potranno, invece, continuare ad essere utilizzati.
																</fo:block>
																
																<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA_V1'">
																	<fo:block space-before="1mm" keep-with-previous="always">
																		In deroga all'obbligatorietà della comunicazione da inviare ai clienti 14 giorni prima dell'invio
																		di ogni addebito SDD, la informiamo che tale comunicazione verrà inviata contestualmente alla
																		fattura cui gli importi si riferiscono. Le ricordiamo che , come di consueto, sarà possibile
																		prendere visione della fattura nell'area del sito www.energit.it a Voi riservata, anche in
																		caso in cui la fattura non sia recapitata al domicilio prescelto in sede di stipula del
																		Contratto entro il termine di pagamento.
																	</fo:block>
																</xsl:if>
																
															</fo:block>
														</xsl:if>
														 -->
														
														
														
														<!-- APPUNTAMENTI_IN_SEDE -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='APPUNTAMENTI_IN_SEDE'">
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1' or
																		  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1' or
																		 
																		PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA' or
																		
																		PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA_V1' or
																		 PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2' or
																		  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='REGISTRAZIONE_DATI_V1'">
																
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
															
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																PER UN CONTATTO ANCORA PIU’ DIRETTO CON IL SERVIZIO CLIENTI ENERGIT, PRENOTI UN APPUNTAMENTO PRESSO LA NOSTRA SEDE!
															</fo:block>
															
															<fo:block space-before="3mm" keep-with-previous="always">
																Le ricordiamo che <fo:inline font-family="universcbold">i nostri Clienti possono contare sull’assistenza
																di un Servizio Clienti dedicato raggiungibile attraverso numerosi canali di contatto</fo:inline>:
															</fo:block>
															
															<fo:block keep-with-previous="always">
																- Il <fo:inline font-family="universcbold">numero 800.19.22.22, gratuito da rete fissa</fo:inline> e attivo dal lunedì al venerdì dalle 8.30 alle 19.30
															</fo:block>
															
															<fo:block keep-with-previous="always">
																- Il <fo:inline font-family="universcbold">numero 070 7521 422 per le chiamate da rete mobile</fo:inline>, attivo dal lunedì al venerdì dalle 8.30 alle 19.30
															</fo:block>
															
															<fo:block keep-with-previous="always">
																- L’indirizzo <fo:inline font-family="universcbold">email energia@energit.it</fo:inline>
															</fo:block>
															
															<fo:block keep-with-previous="always">
																- Il numero di <fo:inline font-family="universcbold">fax gratuito 800.19.22.55</fo:inline>, attivo 24 ore su 24
															</fo:block>
															
															<fo:block space-before="3mm" keep-with-previous="always">
																<fo:inline font-family="universcbold">Se preferisce incontrarci, saremo lieti di riceverla per appuntamento presso la nostra sede di Via Edward Jenner 19/21 a Cagliari.</fo:inline>
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline font-family="universcbold">Può fissare un incontro</fo:inline> con i nostri operatori <fo:inline font-family="universcbold">contattando il numero 070 7521</fo:inline> o direttamente presso la sede Energit.
															</fo:block>
															
															<fo:block keep-with-previous="always">
																<fo:inline font-family="universcbold">Al momento della prenotazione non dimentichi di indicare la motivazione della richiesta e il suo Codice Cliente <xsl:value-of select="CODICE_CLIENTE" /></fo:inline>: in occasione del successivo incontro potremo così fornirle una migliore assistenza e tutto il supporto necessario!
															</fo:block>
														</xsl:if>
														
														
														
														
														<!-- BETA -->
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='BETA'">
															
															<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1' or
																		  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1' or
																		 
																		PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA' or
																		
																		PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SEPA_V1' or
																		 PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2' or
																		  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='REGISTRAZIONE_DATI_V1' or
																		  
																		  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='APPUNTAMENTI_IN_SEDE'
																		  
																		  ">
																
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
															
															<fo:block space-before="1mm">
																<fo:block space-before="1mm" keep-with-previous="always">
																	Il saldo dell'importo di cui alla presente fattura, per essere valido e liberatorio, deve essere effettuato esclusivamente nei confronti di Beta Stepstone s.p.a., cessionaria del relativo credito, sul c/c IBAN: IT34W0200809432000030080275 intestato a quest'ultima e acceso presso Unicredit S.p.A., Piazza 8 novembre, angolo via Pisacane, 20129 - Milano
																</fo:block>
																
																
															</fo:block>
														</xsl:if>	
														
														
														
														
													</xsl:if>
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI']">
														<xsl:if test="CONTRATTO_ENERGIA[@CONSUMER='SI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
																	  RINNOVI or
																	  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
																	  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
																	  (PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] and $mese_fattura &lt; '201108') or
																	  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
																	  (PROMO_SERVIZI[@TIPOLOGIA='EMANCIPAZIONE_ENERGETICA']/child::ID='FOTOVOLTAICO_V1') or
																	  $comunicazione_area_clienti='SI' or
																	  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='GUIDA_ALLA_LETTURA_V1' or
																	  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V1' or
																	  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='PRATICHE_CONNESSIONE_V2' or
																	  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='REGISTRAZIONE_DATI_V1'">
														   <fo:block xsl:use-attribute-sets="block.separatore">
																&#160;
															</fo:block>
														</xsl:if>
														
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI']/child::ID='MIX_FONTI_V1'">
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																MIX MEDIO ENERGETICO NAZIONALE
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Ai sensi di quanto previsto dall’art. 2 del decreto legge 31 luglio 2009,
																riportiamo di seguito le informazioni sulla composizione del mix medio
																nazionale di fonti energetiche primarie utilizzate per la produzione.
																Per maggiori informazioni può consultare il nostro sito internet www.energit.it .
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																	<fo:table-column column-width="proportional-column-width(33)"/>
																	<fo:table-column column-width="proportional-column-width(34)"/>
																	<fo:table-column column-width="proportional-column-width(33)"/>
																	<fo:table-body end-indent="0pt" start-indent="0pt">
																		<fo:table-row font-family="universcbold">
																			<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Composizione del Mix Medio Nazionale utilizzato per la produzione
																				</fo:block>
																				<fo:block text-align="center" keep-with-previous="always">
																					dell'energia elettrica immessa nel sistema elettrico nel 2008 e nel 2009
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row font-family="universcbold" keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					Fonti primarie utilizzate
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					Anno 2009 %
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					Anno 2008 %
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					Fonti rinnovabili
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					31,6%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					26,8%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					Carbone
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					13,1%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					13,3%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					Gas Naturale
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					43,5%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					47,8%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					Prodotti petroliferi
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					4,3%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					3,9%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					Nucleare
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					1,5%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					1,3%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					Altre fonti
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					6,1%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block>
																					6,8%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</xsl:if>
														
														<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI']/child::ID='MIX_FONTI_V2'">
															<fo:block font-family="universcbold">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																MIX MEDIO ENERGETICO NAZIONALE
															</fo:block>
															
															<fo:block keep-with-previous="always">
																Ai sensi di quanto previsto dall’art. 2 del decreto legge 31 luglio 2009,
																riportiamo di seguito le informazioni sulla composizione del mix di fonti
																energetiche primarie utilizzate per la produzione dell'energia elettrica
																fornita nel 2010 e le informazioni sul mix medio nazionale 2009-2010.
																Per maggiori informazioni può consultare il nostro sito internet www.energit.it .
															</fo:block>
															
															<fo:block space-before="2mm" keep-with-previous="always">
																<fo:table font-size="8pt" end-indent="0pt" table-layout="fixed" width="100%">
																	<fo:table-column column-width="proportional-column-width(19)"/>
																	<fo:table-column column-width="proportional-column-width(25)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(25)"/>
																	<fo:table-column column-width="proportional-column-width(25)"/>
																	<fo:table-body end-indent="0pt" start-indent="0pt">
																		<fo:table-row font-family="universcbold">
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Composizione del mix medio nazionale utilizzato per la
																					produzione dell’energia elettrica immessa nel sistema
																					elettrico italiano nel 2009
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Composizione del mix energetico utilizzato per la produzione
																					dell’energia elettrica venduta dall’impresa nel 2010
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Composizione del mix medio nazionale utilizzato per la
																					produzione dell’energia elettrica immessa nel sistema
																					elettrico italiano nel 2010
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row font-family="universcbold" keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Fonti primarie utilizzate
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					2009 (%)
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					2010 (%)
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					2010 (%)
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Fonti rinnovabili
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					33,50%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					26,43%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					35,20%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Carbone
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					12,60%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					16,84%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					12,80%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Gas Naturale
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					43%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					51,51%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					43,10%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Prodotti petroliferi
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					3,40%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					2,20%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					1,70%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Nucleare
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					1,50%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					1,28%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					1,20%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		
																		<fo:table-row keep-with-previous="always">
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					Altre fonti
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					6,0%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					1,74%
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell xsl:use-attribute-sets="cell.mix_fonti">
																				<fo:block text-align="center">
																					6,0%
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</xsl:if>
													</xsl:if>
													
    											

        										
        										<xsl:variable name="dataDoc" select="10000 * substring(./child::DATA_DOCUMENTO, 7, 4) + 100 * substring(./child::DATA_DOCUMENTO, 4, 2) + substring(./child::DATA_DOCUMENTO, 1, 2)"	/>
    											<xsl:choose>
	        										<xsl:when test="$dataDoc > 20180101">
			        									<!-- delibera inizio -->
														<fo:block xsl:use-attribute-sets="block.separatore">
															&#160;
														</fo:block>
														<fo:block font-family="universcbold" space-before="1mm" keep-with-previous="always">
																<xsl:if test="not($color='black')">
																	<xsl:attribute name="color">#0065ae</xsl:attribute>
																</xsl:if>
																	COMUNICAZIONE DELIBERA 97/2018/R/com
																</fo:block>
														<fo:block >
															Gentile cliente 																	
														</fo:block>
														<fo:block>
															Le ricordiamo che qualora siano presenti nella presente fattura consumi e/ conguagli maggiori di 24 mesi potrà eccepire la prescrizione breve e avvalersi del diritto di non versare gli importi fatturati per i suddetti mesi, ai sensi della delibera 97/2018/R/com. La sospensione del pagamento è legittimata solo qualora presenterà formale reclamo,  nelle forme previste dall’Autorità di regolazione per Energia Reti e Ambiente.
														</fo:block>
														<fo:block >
															La delibera  97/2018/R/com è disponibile sul sito www.arera.it .
														</fo:block>
		            									<!-- delibera fine -->
        											</xsl:when>
        											<xsl:otherwise>

        											</xsl:otherwise>
    											</xsl:choose>
												

												
										   		
														

													
													
													<xsl:if test="$bordi='NO'">
														<fo:block keep-with-previous="always" space-before="3mm" xsl:use-attribute-sets="brd.b.000" padding-left="2mm" padding-right="2mm"/>
													</xsl:if>
												
												</fo:block>
												
											</fo:table-cell>
										 </fo:table-row>
											 
									</fo:table-body>
								</fo:table>
							
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row height="5mm" keep-with-previous="always">
							<fo:table-cell display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
								</xsl:if>
								<fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			
			</xsl:if>
                
                
			
			
			
		</fo:block>
	
	</xsl:template>
	
	<xsl:template name="TEMPLATE_RIEPILOGO_FATTURA">
		
		<xsl:variable name="colonne">
			<xsl:if test="@SPOT='NO' or not(@SPOT)">6</xsl:if>
			<xsl:if test="@SPOT='SI'">5</xsl:if>
		</xsl:variable>
		
		<fo:table space-before="5mm" table-layout="fixed" width="100%" xsl:use-attribute-sets="font.table">
			<xsl:if test="@SPOT='NO' or not(@SPOT)">
				<fo:table-column column-width="proportional-column-width(22)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
			</xsl:if>
			<xsl:if test="@SPOT='SI'">
				<fo:table-column column-width="proportional-column-width(30)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
			</xsl:if>
		
			<fo:table-header>
				
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="cell.header">
						<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne+1"/></xsl:attribute>
						<fo:block>
							<fo:inline>RIEPILOGO FATTURA</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
		
				<fo:table-row text-align="center" display-align="center" height="4mm">
					<xsl:if test="@SPOT='SI'">
						<xsl:attribute name="height">5mm</xsl:attribute>
					</xsl:if>
					<fo:table-cell>
						<fo:block>SERVIZIO</fo:block>
					</fo:table-cell>
					
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						<fo:table-cell>
							<fo:block>N. CONTRATTO</fo:block>
						</fo:table-cell>
					</xsl:if>
		
					<fo:table-cell>
						<fo:block>IMPONIBILE</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>IVA</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>CODICE IVA</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>IMPORTO IVA</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>TOTALE</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
		
			<fo:table-body>
				<xsl:for-each select="RIEPILOGO_FATTURA">
					<fo:table-row text-align="center" display-align="center">
						<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI']">
							<xsl:attribute name="height">4mm</xsl:attribute>
						</xsl:if>
						<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI'] and position()&lt;last()">
							<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
						</xsl:if>
						<xsl:if test="position()=last()">
							<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
						</xsl:if>
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="PRODOTTO_SERVIZIO" />
							</fo:block>
						</fo:table-cell>
						
						<xsl:if test="./ancestor::DOCUMENTO[@SPOT='NO'] or not(./ancestor::DOCUMENTO[@SPOT])">
							<fo:table-cell>
								<fo:block>
									<xsl:value-of select="CONTRATTO" />
								</fo:block>
							</fo:table-cell>
						</xsl:if>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="IMPONIBILE" />
							</fo:block>
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="ALIQUOTA_IVA" />%
							</fo:block>
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:choose>
									<xsl:when test="CODICE_IVA">
										<xsl:value-of select="CODICE_IVA" />
									</xsl:when>
									<xsl:otherwise>
										-
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="IMPORTO_IVA" />
							</fo:block>
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="TOTALE_CON_IVA" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
				
				<xsl:for-each select="DESCRIZIONI_CODICI_IVA">
					<fo:table-row keep-with-previous="always">
						<fo:table-cell>
							<fo:block>
								Codice IVA "<xsl:value-of select="CODICE_IVA" />"
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
							<fo:block>
								<xsl:value-of select="DESCRIZIONE_CODICE_IVA" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	
	<xsl:attribute-set name="font.table">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="line-height">9pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.pagare">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="line-height">7pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.header">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	    <xsl:attribute name="font-family">universcbold</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	    <xsl:attribute name="padding-left">3mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border">
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.left">
		<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.right">
		<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.top">
		<xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.bottom">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.left">
		<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.right">
		<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.top">
		<xsl:attribute name="border-top">1pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.bottom">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	 
	<xsl:attribute-set name="font.fascia_header">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">9.5pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia_text">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">6pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia">
	    <xsl:attribute name="font-family">universcbold</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.legenda">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">7pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.comunicazioni">
		<xsl:attribute name="font-family">universc</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.sepa">
		<xsl:attribute name="font-family">universc</xsl:attribute>
		<xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.title">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="line-height">4mm</xsl:attribute>
		<xsl:attribute name="space-after">4mm</xsl:attribute>
		<xsl:attribute name="font-size">11pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.separatore">
		<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="space-after">1mm</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.rinnovi">
		<xsl:attribute name="padding">0.5mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.mix_fonti">
		<xsl:attribute name="padding">0.1mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.articoli">
		<xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
		<xsl:attribute name="font-style">italic</xsl:attribute>
	</xsl:attribute-set>

</xsl:stylesheet>

