<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
**********************
**                  **
** SINTESI MONOSITO **
**                  **
**********************
-->
<xsl:template name="SINTESI_ENERGIA_012011">
	
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="sfondo_titoli"/>
	<xsl:param name="color-sezioni"/>
	<xsl:param name="color-sottosezioni"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:param name="svg-sottosezioni"/>
	<xsl:param name="svg-dettaglio"/>
	<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:param>
	
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		
			<fo:table-body>
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="color"><xsl:value-of select="$color_titolo_dettaglio"/></xsl:attribute>
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(30)"/>
							<fo:table-column column-width="proportional-column-width(70)"/>
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row keep-with-next="always">
									<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="pad.l.000">
										<fo:block xsl:use-attribute-sets="blk.003 chrtitolodettaglio">
											BOLLETTA DI SINTESI
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
									<fo:table-row keep-with-next="always" height="0.5mm">
										<fo:table-cell number-columns-spanned="2" border-bottom="0.5 dashed thick">
											<xsl:attribute name="border-bottom-color"><xsl:value-of select="$color_titolo_dettaglio"/></xsl:attribute>
											<fo:block/>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row keep-with-next="always" height="0.5mm">
										<fo:table-cell number-columns-spanned="2">
											<fo:block/>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row xsl:use-attribute-sets="blk.003">
										<fo:table-cell xsl:use-attribute-sets="pad.l.000">
											<fo:block xsl:use-attribute-sets="chrtitolodettaglio">
												CONTRATTO N.
												<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::CONTRACT_NO"/>
											</fo:block>
										</fo:table-cell>
										
										<fo:table-cell xsl:use-attribute-sets="pad.l.000">
											<fo:block>
												<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::PERIODO_RIFERIMENTO='')">
													<fo:inline xsl:use-attribute-sets="chrbold.008">
														PERIODO DI RIFERIMENTO
													</fo:inline>
													<fo:inline xsl:use-attribute-sets="chr.009">
														<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::PERIODO_RIFERIMENTO"/>
													</fo:inline>
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:if test="not($bordi='NO')">
								<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="not($color='black')">
								<xsl:attribute name="background-image">url(svg/rectangle_table_color_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="$bordi='NO' and $color='black'">
								<xsl:attribute name="background-image">url(svg/rectangle_table_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
				<fo:table-column column-width="proportional-column-width(100)"/>
				<fo:table-body end-indent="0pt" start-indent="-1pt">
					<fo:table-row height="19mm">
						<fo:table-cell display-align="center" background-repeat="no-repeat"> 
							<xsl:attribute name="background-image"><xsl:value-of select="$svg-dettaglio"/></xsl:attribute>
							<!-- <xsl:attribute name="color"><xsl:value-of select="$color-sottosezioni"/></xsl:attribute> -->
							<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" font-family="universcbold" font-size="8.5pt" xsl:use-attribute-sets="blk.004">
							<fo:table-column column-width="proportional-column-width(43)"/>
							<fo:table-column column-width="proportional-column-width(25)"/>
							<fo:table-column column-width="proportional-column-width(15)"/>
							<fo:table-column column-width="proportional-column-width(17)"/>
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											PRODOTTO/SERVIZIO
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::PRODOTTO_SERVIZIO"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											INIZIO FORNITURA
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::INIZIO_FORNITURA"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="pad.r.000">
										<fo:block>
											TIPOLOGIA CONTRATTO
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::TIPOLOGIA_CONTRATTO"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>				
								
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											SEDE FORNITURA
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SEDE_OPERATIVA"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											TIPOLOGIA MERCATO
											<fo:inline font-family="universc">
												Mercato libero
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:if test="./child::CONTRATTO_ENERGIA/child::POD">
												POD
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::POD"/>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell xsl:use-attribute-sets="pad.r.000">
										<fo:block>
											<xsl:if test="./child::CONTRATTO_ENERGIA/child::PRESA and $mese_fattura &lt; '201107'">
												PRESA
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::PRESA"/>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>	
								
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											TIPO CONTATORE
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::TIPO_CONTATORE"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											TENSIONE DI ALIM.
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::TENSIONE_ALIMENTAZIONE"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:if test="./child::CONTRATTO_ENERGIA/child::POTENZA_DISPONIBILE">
												POT. DISP.
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::POTENZA_DISPONIBILE"/>
													kW
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell xsl:use-attribute-sets="pad.r.000">
										<fo:block>
											<xsl:if test="./child::CONTRATTO_ENERGIA/child::POTENZA_IMPEGNATA">
												POT. IMP.
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::POTENZA_IMPEGNATA"/>
													kW
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>	
							</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>		
		</xsl:if>
		
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="block_sintesi_start font_sintesi">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">

					
			<fo:table-row height="2mm" keep-with-previous="always">
				<fo:table-cell display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_bold.svg)</xsl:attribute>
					</xsl:if>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
				
			<fo:table-row>
				<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute>
					</xsl:if>
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(45)"/>
						<fo:table-column column-width="proportional-column-width(55)"/>
						<fo:table-body end-indent="0pt" start-indent="0pt">
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE SERVIZI DI VENDITA</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE SERVIZI DI RETE</fo:inline>
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE='0,00')">
											<fo:inline> AL NETTO DEL BONUS SOCIALE</fo:inline>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE='0,00')">
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<fo:inline>BONUS SOCIALE</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
												<xsl:choose>
													<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<fo:inline>
															<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											
											<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
												<xsl:choose>
													<xsl:when test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<fo:inline>
															<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::BONUS_SOCIALE"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE IMPOSTE</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_IMPOSTE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_IMPOSTE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE IMPONIBILE</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPONIBILE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPONIBILE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_IMPONIBILE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_IMPONIBILE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>IVA</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row height="1mm">
								<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="brd.b.000">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE FORNITURA DI ENERGIA ELETTRICA E IMPOSTE</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>							
							
	<xsl:variable name="od_f">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>
	</xsl:variable>
	
	<xsl:variable name="imp_dc">
		<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE">
			<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE)">
			0
		</xsl:if>
	</xsl:variable>
	
	<xsl:variable name="od">
		<xsl:value-of select="$od_f+$imp_dc"/>
	</xsl:variable>							
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>ONERI DIVERSI</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="($od=0)">
											<fo:inline>-</fo:inline>
										</xsl:if>								
										<xsl:if test="not($od=0)">
											<fo:inline>
												<xsl:value-of select="translate($od div 100,'.',',')"/>
											</fo:inline>
										</xsl:if>										
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>IVA SU ONERI DIVERSI</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA_SU_ONERI_DIVERSI='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::IVA_SU_ONERI_DIVERSI"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='CANONE_RAI'">
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<fo:inline>CANONE DI ABBONAMENTO RAI</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
												<xsl:choose>
													<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_CANONE_RAI='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<fo:inline>
															<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_CANONE_RAI"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											
											<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
												<xsl:choose>
													<xsl:when test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_CANONE_RAI='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													
													<xsl:otherwise>
														<fo:inline>
															<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_CANONE_RAI"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>	
							</xsl:if>
							
							<fo:table-row height="1mm">
								<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="brd.b.000">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							
	<xsl:variable name="tp_f">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_DA_PAGARE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_DA_PAGARE='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_DA_PAGARE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_DA_PAGARE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_DA_PAGARE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_DA_PAGARE,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA)">
				0
			</xsl:if>
		</xsl:if>
	</xsl:variable>
	
	<xsl:variable name="tp">
		<xsl:value-of select="$tp_f+$imp_dc"/>
	</xsl:variable>								
							
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE DA PAGARE</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="($tp=0)">
											<fo:inline>-</fo:inline>
										</xsl:if>								
										<xsl:if test="not($tp=0)">
											<fo:inline>
												<xsl:value-of select="translate($tp div 100,'.',',')"/>
											</fo:inline>
										</xsl:if>										
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
						</fo:table-body>
					</fo:table>
					
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row height="2mm" keep-with-previous="always">
				<fo:table-cell display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_bold.svg)</xsl:attribute>
					</xsl:if>
					<fo:block>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
		</fo:table-body>
		</fo:table>
		
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="CONTRATTO_ENERGIA/LETTURE"/>
				<xsl:with-param name="sezione4check" select="CONTRATTO_ENERGIA/LETTURE"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="CONTRATTO_ENERGIA/LETTURE_CONSUMI_CONGUAGLI"/>
				<xsl:with-param name="sezione4check" select="CONTRATTO_ENERGIA/LETTURE_CONSUMI_CONGUAGLI/CONSUMO"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="CONTRATTO_ENERGIA/RIEPILOGO_ACCONTI_CONGUAGLIO"/>
				<xsl:with-param name="sezione4check" select="CONTRATTO_ENERGIA/RIEPILOGO_ACCONTI_CONGUAGLIO/CONSUMO"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="CONTRATTO_ENERGIA/CONSUMI_ENERGIA_ULTIMI_14_MESI"/>
				<xsl:with-param name="sezione4check" select="CONTRATTO_ENERGIA/CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			
			<!-- <xsl:apply-templates select="./child::CONTRATTO_ENERGIA/child::LETTURE">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			</xsl:apply-templates> -->
			
			<!-- <xsl:apply-templates select="./child::CONTRATTO_ENERGIA/child::LETTURE_CONSUMI_CONGUAGLI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			</xsl:apply-templates> -->
			
			<!-- <xsl:apply-templates select="./child::CONTRATTO_ENERGIA/child::RIEPILOGO_ACCONTI_CONGUAGLIO">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			</xsl:apply-templates> -->
			
			<!-- <xsl:apply-templates select="./child::CONTRATTO_ENERGIA/child::CONSUMI_ENERGIA_ULTIMI_14_MESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:apply-templates> -->
		</xsl:if>
		
</xsl:template>

</xsl:stylesheet>






