<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**                                                    **
**		   STRUTTURA PER SEZIONI SINTESI			  **
**                                                    **
********************************************************
-->
<xsl:template name="SEZIONE_SINTESI">

	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="svg-titoli"/>
	<xsl:param name="sfondo_titoli"/>
	<xsl:param name="sezione"/>
	<xsl:param name="sezione4check"/>
	<xsl:param name="mese_fattura"/>


	<xsl:if test="$sezione4check">
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="block_sintesi chr.008">
		<fo:table-column column-width="proportional-column-width(100)"/>
		
		<fo:table-header>
			<fo:table-row height="2mm">
				<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
					</xsl:if>
					
					<xsl:apply-templates select="($sezione)">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-titoli"/>
						<xsl:with-param name="tipo" select="'header'"/>
						<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
					</xsl:apply-templates>

					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row height="2mm">
				<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
					</xsl:if>
					
					<xsl:apply-templates select="($sezione)">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-titoli"/>
						<xsl:with-param name="tipo" select="'body'"/>
						<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
					</xsl:apply-templates>
					
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row height="2mm" keep-with-previous="always">
				<fo:table-cell display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
					</xsl:if>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
		</fo:table>
	</xsl:if>
	
</xsl:template>

</xsl:stylesheet>