<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
*******************************************************
**                  Header sezione                   **
*******************************************************
-->
<xsl:template name="Header_sezione_dettaglio">
	<xsl:param name="path"/>
	<xsl:param name="titolo"/>
	<xsl:param name="header"/>
	<xsl:param name="colonne"/>
	<xsl:param name="color"/>
	<xsl:param name="macro_sezione"/>
	<xsl:if test="not($titolo='no_title') or $header='SI'">
		<fo:table-header>
			<xsl:if test="not($titolo='no_title')">
				<fo:table-row keep-with-next="always">
					<xsl:attribute name="color"><xsl:value-of select="$color"/></xsl:attribute>
					<fo:table-cell>
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
						<fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(100)"/>
						<fo:table-body end-indent="0pt" start-indent="-1pt">
							<fo:table-row height="6mm">
								<fo:table-cell display-align="center" background-repeat="no-repeat"> 
									<xsl:attribute name="background-image"><xsl:value-of select="$path"/></xsl:attribute>
									<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<fo:table-row keep-with-next="always">
											<fo:table-cell xsl:use-attribute-sets="pad.l.000">
												<fo:block xsl:use-attribute-sets="blk.004">
													<xsl:if test="$macro_sezione='SI'">
														<fo:inline font-family="universcbold" font-size="9.5pt"><xsl:value-of select="$titolo"/></fo:inline>
													</xsl:if>
													<xsl:if test="$macro_sezione='NO'">
														<fo:inline font-family="universcbold" font-size="8.5pt"><xsl:value-of select="$titolo"/></fo:inline>
													</xsl:if>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>				
									</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
			
			<!--
			**********************************************************
			** Colonne di default per un numero di colonne pari a 5 **
			**********************************************************
			-->
			<xsl:if test="$header='SI'">		
				<fo:table-row keep-with-next="always" height="{$altezzariga}" xsl:use-attribute-sets="blk.002 condensedcors.008">
					<fo:table-cell xsl:use-attribute-sets="pad.l.000">
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Unità di misura</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Corrispettivi medi unitari</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Quantità</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Totale (euro)</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
		</fo:table-header>
	</xsl:if>
</xsl:template>



<!--
*******************************************************
**                   Body sezione                    **
*******************************************************
-->
<xsl:template name="Sezione_dettaglio_generica">
	<xsl:param name="nome_sezione"/>
	<xsl:param name="tipo_quota"/>
	<xsl:param name="last_section"/>
	<xsl:param name="info_totale"/>
	<xsl:param name="lines"/>
	<xsl:param name="ultima_sezione_reti"/>
	<xsl:param name="ultima_sezione_vendita"/>
	<xsl:param name="ultima_sezione_imposte"/>
	<xsl:param name="titolo"/>
	
	<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$nome_sezione]">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.002 chr.008">
			<fo:table-column column-width="proportional-column-width(25)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-header>
				<fo:table-row keep-with-next="always" height="{$altezzariga}">
					<fo:table-cell xsl:use-attribute-sets="pad.l.000">
						<fo:block>
							<fo:inline xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="$tipo_quota"/></fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell number-columns-spanned="4">
						<fo:block>
							<fo:inline xsl:use-attribute-sets="condensedcors.008"></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<xsl:for-each select="./child::SEZIONE[@TIPOLOGIA=$nome_sezione]">
					<xsl:for-each select="./child::PERIODO_RIFERIMENTO">
						<fo:table-row height="{$altezzariga_sezione}">
							<fo:table-cell display-align="center" xsl:use-attribute-sets="pad.l.000">
								<fo:block>
									<fo:inline>Dal <xsl:value-of select="./child::DAL"/> al <xsl:value-of select="./child::AL"/>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="4">
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(20)"/>
									<fo:table-column column-width="proportional-column-width(20)"/>
									<fo:table-column column-width="proportional-column-width(20)"/>
									<fo:table-column column-width="proportional-column-width(15)"/>
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<xsl:for-each select="./child::PRODOTTO">
											<fo:table-row keep-with-next="always" height="{$altezzariga_sezione}">
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:choose>
																<xsl:when test="$nome_sezione='QUOTA_FISSA_EB'">
																	mese
																</xsl:when>
															
																<xsl:when test="./child::UNITA_DI_MISURA='Mese'">
																	euro/cliente/<xsl:value-of select="./child::UNITA_DI_MISURA"/>
																</xsl:when>
																
																<xsl:otherwise>
																	euro/<xsl:value-of select="./child::UNITA_DI_MISURA"/>
																</xsl:otherwise>
															</xsl:choose>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												
												<!-- <fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:choose>
																<xsl:when test="$nome_sezione='QUOTA_FISSA_EB' or $nome_sezione='QUOTA_FISSA_EA'">
																	<xsl:value-of select="./child::CORRISPETTIVO_ENERGIT"/>
																</xsl:when>
															
																<xsl:otherwise>
																	<xsl:value-of select="./child::CORRISPETTIVO_UNITARIO"/>
																</xsl:otherwise>
															</xsl:choose>
														</fo:inline>
													</fo:block>
												</fo:table-cell> -->
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::CORRISPETTIVO_UNITARIO"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:choose>
																<xsl:when test="./child::UNITA_DI_MISURA='Mese' or $nome_sezione='QUOTA_FISSA_EB'">
																	<xsl:choose>
																		<xsl:when test="./child::QUANTITA>1">
																			<xsl:value-of select="./child::QUANTITA"/>
																			Mesi
																		</xsl:when>
																		
																		<xsl:otherwise>
																			<xsl:value-of select="./child::QUANTITA"/>
																			Mese
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:when>
																
																<xsl:otherwise>
																	<xsl:value-of select="./child::QUANTITA"/>
																	<xsl:value-of select="./child::UNITA_DI_MISURA"/>
																</xsl:otherwise>
															</xsl:choose>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::PREZZO_TOTALE"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</xsl:for-each>
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
						</fo:table-row>
						
						<!-- <xsl:if test="position()&lt;last()"> -->
						<xsl:if test="((position()&lt;last()) or (./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO and not(./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,00' or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,000000'))) and $lines='SI'">
							<fo:table-row height="1mm">
								<fo:table-cell>
									<fo:block></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-right="12mm" number-columns-spanned="4">
									<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
										<fo:table-column column-width="proportional-column-width(100)"/>
										<fo:table-body end-indent="0pt" start-indent="0pt">
											<fo:table-row height="1mm">	
												<fo:table-cell border-bottom="0.25pt dashed black">
													<fo:block></fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row height="1mm">
								<fo:table-cell number-columns-spanned="5">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:if>
						<!-- </xsl:if> -->
					</xsl:for-each>
					
					<xsl:if test="./child::PRECEDENTEMENTE_FATTURATO and not(./child::PRECEDENTEMENTE_FATTURATO='0,00' or ./child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./child::PRECEDENTEMENTE_FATTURATO='0,000000')">
						<fo:table-row keep-with-previous="always" height="{$altezzariga}">
							<fo:table-cell xsl:use-attribute-sets="pad.l.000">
								<fo:block>
									<fo:inline>Precedentemente fatturato</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="3">
								<fo:block></fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<xsl:choose>
										<xsl:when test="./child::PRECEDENTEMENTE_FATTURATO='0,00' or ./child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./child::PRECEDENTEMENTE_FATTURATO='0,000000'">
											<fo:inline>-</fo:inline>
										</xsl:when>
										
										<xsl:otherwise>
											<fo:inline>
												<xsl:value-of select="./child::PRECEDENTEMENTE_FATTURATO"/>
											</fo:inline>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:if>
					
					<xsl:if test="$lines='SI'">
						<fo:table-row keep-with-previous="always" height="1mm">
							<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="pad.lr.000">
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<fo:table-row height="1mm">	
											<fo:table-cell border-bottom="0.25pt dashed black">
												<fo:block/>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
						</fo:table-row>
					</xsl:if>
					
					<fo:table-row keep-with-previous="always" height="{$altezzariga}" xsl:use-attribute-sets="chrbold.008">
						<fo:table-cell xsl:use-attribute-sets="pad.l.000">
							<fo:block>
								Totale
							</fo:block>
						</fo:table-cell>
						
						<fo:table-cell number-columns-spanned="3">
							<fo:block/>
						</fo:table-cell>
						
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="./child::RIEPILOGO_TOTALE_SEZIONE"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row keep-with-previous="always" height="3mm">
						<fo:table-cell number-columns-spanned="5">
							<fo:block/>
						</fo:table-cell>
					</fo:table-row>
					
					<!-- <xsl:if test="not($last_section=$nome_sezione)">
						<fo:table-row keep-with-previous="always" height="3mm">
							<xsl:choose>
								<xsl:when test="$separatore='SI'">
									<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="pad.lr.000">
										<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
											<fo:table-column column-width="proportional-column-width(100)"/>
											<fo:table-body end-indent="0pt" start-indent="0pt">
												<fo:table-row height="3mm">	
													<fo:table-cell border-bottom="0.25pt dashed black">
														<fo:block></fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:table-cell>
								</xsl:when>
								<xsl:otherwise>
									<fo:table-cell number-columns-spanned="5">
										<fo:block></fo:block>
									</fo:table-cell>
								</xsl:otherwise>
							</xsl:choose>
							
						</fo:table-row>
					</xsl:if> -->
					
					<xsl:choose>
						<xsl:when test="$info_totale='USO RETI'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row keep-with-previous="always" height="{$altezzariga}">
									<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="brd.b.000">
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" height="{$altezzariga}" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="4">
										<fo:block>
											<fo:inline>TOTALE <xsl:value-of select="$titolo"/></fo:inline>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													(A)
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_CORRISPETTIVI_USO_RETI_SERVIZIO_MISURA"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>
						</xsl:when>
						
						<xsl:when test="$info_totale='VENDITA'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row keep-with-previous="always" height="{$altezzariga}">
									<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="brd.b.000">
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" height="{$altezzariga}" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="4">
										<fo:block>
											TOTALE <xsl:value-of select="$titolo"/>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_reti=''">
															(A)
														</xsl:when>
														<xsl:otherwise>
															(B)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_COSTO_GENERAZIONE_CON_CTE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>
						</xsl:when>
						
						<xsl:when test="$info_totale='IMPOSTE'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row keep-with-previous="always" height="{$altezzariga}">
									<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="brd.b.000">
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" height="{$altezzariga}" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="4">
										<fo:block>
											TOTALE <xsl:value-of select="$titolo"/>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_reti='' and
																		$ultima_sezione_vendita=''">
															(A)
														</xsl:when>
														<xsl:when test="$ultima_sezione_reti='' or
																		$ultima_sezione_vendita=''">
															(B)
														</xsl:when>
														<xsl:otherwise>
															(C)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_ENERGIA/child::SINTESI_ENERGIA/child::TOTALE_IMPOSTE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
					
					
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>