<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<xsl:template name="Dettaglio_energia">

<xsl:param name="bordi"/>
<xsl:param name="color"/>
<xsl:param name="color_titolo_dettaglio"/>
<xsl:param name="color-sezioni"/>
<xsl:param name="color-sottosezioni"/>
<xsl:param name="svg-sezioni"/>
<xsl:param name="svg-sottosezioni"/>
<xsl:param name="lines_flag">SI</xsl:param>
<xsl:param name="ultima_sezione_reti"/>
<xsl:param name="ultima_sezione_vendita"/>
<xsl:param name="ultima_sezione_imposte"/>

<xsl:variable name="titolo1">
	<xsl:choose>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo1_c"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo1"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="titolo2">
	<xsl:choose>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo2_c"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo2"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="titolo2a">
	<xsl:choose>
		<xsl:when test="./child::PRODOTTO_SERVIZIO='Energit per Noi - EN-U-PO-01'">
			<xsl:value-of select="$tipo2a_exnoi"/>
		</xsl:when>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo2a_c"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo2a"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="titolo2b">
	<xsl:choose>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo2b_c"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo2b"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!--
**************************************************************************
**                                                                      **
**      CORRISPETTIVI PER L’USO DELLE RETI E IL SERVIZIO DI MISURA      **
**                                                                      **
**************************************************************************
-->
	<xsl:if test="(@CONSUMER='NO' and
				  (./child::SEZIONE[@TIPOLOGIA=$ert] or
				  ./child::SEZIONE[@TIPOLOGIA=$tp] or
				  ./child::SEZIONE[@TIPOLOGIA=$te] or
				  ./child::SEZIONE[@TIPOLOGIA=$a_uc]))
				  or
				  (@CONSUMER='SI' and
				  (./child::SEZIONE[@TIPOLOGIA=$tp] or
				  ./child::SEZIONE[@TIPOLOGIA=$te] or
				  ./child::SEZIONE[@TIPOLOGIA=$a_uc]))
				 ">
		<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			
			<fo:table-body>
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
						</xsl:if>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(25)"/>
							<fo:table-column column-width="proportional-column-width(20)"/>
							<fo:table-column column-width="proportional-column-width(20)"/>
							<fo:table-column column-width="proportional-column-width(20)"/>
							<fo:table-column column-width="proportional-column-width(15)"/>
							
							<xsl:call-template name="Header_sezione_dettaglio">
								<xsl:with-param name="path" select="$svg-sezioni"/>
								<xsl:with-param name="titolo" select="$titolo1"/>
								<xsl:with-param name="header" select="'SI'"/>
								<xsl:with-param name="colonne" select="5"/>
								<xsl:with-param name="color" select="$color-sezioni"/>
								<xsl:with-param name="macro_sezione" select="'SI'"/>
							</xsl:call-template>

							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row>
									<fo:table-cell number-columns-spanned="5">
										<!--Quota Fissa-->
										<xsl:call-template name="Sezione_dettaglio_generica">
											<xsl:with-param name="nome_sezione" select="$a_uc"/>
											<xsl:with-param name="tipo_quota" select="'QUOTA FISSA'"/>
											<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
											<xsl:with-param name="info_totale" select="'USO RETI'"/>
											<xsl:with-param name="lines" select="$lines_flag"/>
											<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
											<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
											<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
											<xsl:with-param name="titolo" select="$titolo1"/>
										</xsl:call-template>
										
										<!--Trasporto Energia-->
										<xsl:call-template name="Sezione_dettaglio_generica">
											<xsl:with-param name="nome_sezione" select="$te"/>
											<xsl:with-param name="tipo_quota" select="'QUOTA ENERGIA'"/>
											<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
											<xsl:with-param name="info_totale" select="'USO RETI'"/>
											<xsl:with-param name="lines" select="$lines_flag"/>
											<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
											<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
											<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
											<xsl:with-param name="titolo" select="$titolo1"/>
										</xsl:call-template>
										
										<!--Trasporto Potenza-->
										<xsl:call-template name="Sezione_dettaglio_generica">
											<xsl:with-param name="nome_sezione" select="$tp"/>
											<xsl:with-param name="tipo_quota" select="'QUOTA POTENZA'"/>
											<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
											<xsl:with-param name="info_totale" select="'USO RETI'"/>
											<xsl:with-param name="lines" select="$lines_flag"/>
											<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
											<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
											<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
											<xsl:with-param name="titolo" select="$titolo1"/>
										</xsl:call-template>
										
										<xsl:if test="@CONSUMER='NO'">
											<!--Energia Reattiva-->
											<xsl:call-template name="Sezione_dettaglio_generica">
												<xsl:with-param name="nome_sezione" select="$ert"/>
												<xsl:with-param name="tipo_quota" select="'ENERGIA REATTIVA'"/>
												<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
												<xsl:with-param name="info_totale" select="'USO RETI'"/>
												<xsl:with-param name="lines" select="$lines_flag"/>
												<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
												<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
												<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
												<xsl:with-param name="titolo" select="$titolo1"/>
											</xsl:call-template>	
										</xsl:if>
									</fo:table-cell>
								</fo:table-row>	
							</fo:table-body>
						</fo:table>
						
						<xsl:if test="@CONSUMER='NO'">
							<fo:block xsl:use-attribute-sets="blk.004 chrbold.008">
								Maggiori dettagli sulle componenti A, UC, MCT sono disponibili nella sua area riservata.
							</fo:block>
						</xsl:if>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
						</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:if>
	
<!--
**************************************************************************
**                                                                      **
** CORRISPETTIVI PER ACQUISTO, VENDITA, DISPACCIAMENTO E SBILANCIAMENTO **
**                                                                      **
**************************************************************************
-->	
	<xsl:if test="(@CONSUMER='NO' and
				  (./child::SEZIONE[@TIPOLOGIA=$eb_cte] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_eb] or
				  ./child::SEZIONE[@TIPOLOGIA=$pt] or
				  ./child::SEZIONE[@TIPOLOGIA=$pc_disp] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_ea]))
				  or
				  (@CONSUMER='SI' and
				  (./child::SEZIONE[@TIPOLOGIA=$ert] or
				  ./child::SEZIONE[@TIPOLOGIA=$eb_cte] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_eb] or
				  ./child::SEZIONE[@TIPOLOGIA=$pt] or
				  ./child::SEZIONE[@TIPOLOGIA=$pc_disp] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_fissa_ea]))
				 ">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:if test="not($ultima_sezione_reti='')">
				<xsl:attribute name="space-before">2mm</xsl:attribute>
			</xsl:if>
			
			<fo:table-column column-width="proportional-column-width(100)"/>
			
			<fo:table-body>
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
						</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(100)"/>					
							
							<xsl:call-template name="Header_sezione_dettaglio">
								<xsl:with-param name="path" select="$svg-sezioni"/>
								<xsl:with-param name="titolo" select="$titolo2"/>
								<xsl:with-param name="header" select="'NO'"/>
								<xsl:with-param name="colonne" select="1"/>
								<xsl:with-param name="color" select="$color-sezioni"/>
								<xsl:with-param name="macro_sezione" select="'SI'"/>
							</xsl:call-template>
					
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										
										<!--Quota Fissa EA-->
										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_ea]">
											<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
											
												<xsl:call-template name="Header_sezione_dettaglio">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="'no_title'"/>
													<xsl:with-param name="header" select="'SI'"/>
													<xsl:with-param name="colonne" select="5"/>
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>
												
												<fo:table-body end-indent="0pt" start-indent="0pt">
													<fo:table-row>
														<fo:table-cell number-columns-spanned="5">
															<xsl:call-template name="Sezione_dettaglio_generica">
																<xsl:with-param name="nome_sezione" select="$quota_fissa_ea"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA FISSA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</xsl:if>
									
										<!--
										**************************************************************************
										**                                                                      **
										**                        COMPONENTI ENERGIA                            **
										**                                                                      **
										**************************************************************************
										-->
										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$pc_disp]">
											<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
											<fo:table-column column-width="proportional-column-width(75)"/>
											<fo:table-column column-width="proportional-column-width(10)"/>
											<fo:table-column column-width="proportional-column-width(15)"/>
											
												<xsl:call-template name="Header_sezione_dettaglio">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="$titolo2a"/>
													<xsl:with-param name="header" select="'NO'"/>
													<xsl:with-param name="colonne" select="3"/>
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>
												
												<fo:table-body end-indent="0pt" start-indent="-1pt">
													<xsl:for-each select="./child::SEZIONE[@TIPOLOGIA=$pc_disp]">
														<xsl:for-each select="./child::PERIODO_RIFERIMENTO">
														
															<xsl:if test="@TEMPLATE='1'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'ENERGIA100'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="5"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Quantità'"/>
																	<xsl:with-param name="colonna4" select="'Sconto Energit (%)'"/>
																	<xsl:with-param name="colonna5" select="'Totale scontato (euro)'"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'QUANTITA'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA%'"/>
																	<xsl:with-param name="value5" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='2'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'DIECI_E_LUCE'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="8"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Sconto Energit (%)'"/>
																	<xsl:with-param name="colonna5" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna7" select="'Quantità'"/>
																	<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA%'"/>
																	<xsl:with-param name="value5" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value7" select="'QUANTITA'"/>
																	<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='2_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'DIECI_E_LUCE_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="9"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Sconto (%)'"/>
																	<xsl:with-param name="colonna5" select="'Bonus'"/>
																	<xsl:with-param name="colonna6" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna7" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna8" select="'Quantità'"/>
																	<xsl:with-param name="colonna9" select="'Totale (euro)'"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA%'"/>
																	<xsl:with-param name="value5" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value6" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value7" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value8" select="'QUANTITA'"/>
																	<xsl:with-param name="value9" select="'PREZZO_TOTALE'"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='3'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_COMUNE'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna5" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value5" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='3_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_COMUNE_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="8"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Agg. Indice'"/>
																	<xsl:with-param name="colonna5" select="'Bonus (euro/kWh)'"/>
																	<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna7" select="'Quantità'"/>
																	<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'INDICE_AGGIORNAMENTO'"/>
																	<xsl:with-param name="value5" select="'SCONTO_ENERGIA'"/>
																	<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value7" select="'QUANTITA'"/>
																	<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<!-- SCT incluse nella colonna SCONTO_ENERGIA-->
															<xsl:if test="@TEMPLATE='4'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'LUCERTA'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo AEEG'"/>
																	<xsl:with-param name="colonna4" select="'Risparmio'"/>
																	<xsl:with-param name="colonna5" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'cent di euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'SCONTO_ENERGIA'"/>
																	<xsl:with-param name="value5" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='5'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'MILLELUCI'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="5"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Quantità'"/>
																	<xsl:with-param name="colonna5" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'QUANTITA'"/>
																	<xsl:with-param name="value5" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='5_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'MILLELUCI_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="6"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Bonus'"/>
																	<xsl:with-param name="colonna5" select="'Quantità'"/>
																	<xsl:with-param name="colonna6" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value5" select="'QUANTITA'"/>
																	<xsl:with-param name="value6" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<!--Colonna bonus forzata a 3 e quindi il templete sarà sempre col suffisso _BONUS-->
															<xsl:if test="@TEMPLATE='6_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_E_X_NOI'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="8"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo AEEG'"/>
																	<xsl:with-param name="colonna4" select="'Sconto (%)'"/>
																	<xsl:with-param name="colonna5" select="'Bonus (cent di euro)'"/>
																	<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna7" select="'Quantità'"/>
																	<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'cent di euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'3%'"/>
																	<xsl:with-param name="value5" select="'SCONTO_ENERGIA'"/>
																	<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value7" select="'QUANTITA'"/>
																	<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='7' or @TEMPLATE='7_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_LUC_E'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo AEEG'"/>
																	<xsl:with-param name="colonna4" select="'Bonus 5 (cent di euro)'"/>
																	<xsl:with-param name="colonna5" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'cent di euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value5" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='8'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_ECOLUCE'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="5"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Quantità'"/>
																	<xsl:with-param name="colonna5" select="'Totale scontato (euro)'"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value4" select="'QUANTITA'"/>
																	<xsl:with-param name="value5" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='8_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO_012011">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_ECOLUCE_BONUS'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="7"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Bonus'"/>
																	<xsl:with-param name="colonna5" select="'Prezzo Energit'"/>
																	<xsl:with-param name="colonna6" select="'Quantità'"/>
																	<xsl:with-param name="colonna7" select="'Totale scontato (euro)'"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="colonna10" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																	<xsl:with-param name="value4" select="'BONUS_ENERGIA'"/>
																	<xsl:with-param name="value5" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value6" select="'QUANTITA'"/>
																	<xsl:with-param name="value7" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																	<xsl:with-param name="value10" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<!--Mai fatturato un bonus su biluce. Non è necessario il template 9_BONUS-->
															<xsl:if test="@TEMPLATE='9'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_BILUCE'"/>
																	<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																	<xsl:with-param name="colonne" select="5"/>
																	<xsl:with-param name="colonna1" select="'Fascia'"/>
																	<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna3" select="'Prezzo Listino'"/>
																	<xsl:with-param name="colonna4" select="'Quantità'"/>
																	<xsl:with-param name="colonna5" select="'Totale scontato (euro)'"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'FASCIA'"/>
																	<xsl:with-param name="value2" select="'euro'"/>
																	<xsl:with-param name="value3" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value4" select="'QUANTITA'"/>
																	<xsl:with-param name="value5" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='15000_BONUS' or @TEMPLATE='15000' or @TEMPLATE='18000_BONUS' or @TEMPLATE='18000'">
																<xsl:variable name="header_colonna3">Quantità (Consumi superiori a <xsl:value-of select="./child::PRODOTTO/child::BONUS_ENERGIA" /> kWh/mese)</xsl:variable>
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'TEMPLATE_BONUS_ALTO_CONSUMO'"/>
																	<xsl:with-param name="nome_sezione" select="'BONUS ALTO CONSUMO'"/>
																	<xsl:with-param name="colonne" select="4"/>
																	<xsl:with-param name="colonna1" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna2" select="'Corrispettivo unitario'"/>
																	<xsl:with-param name="colonna3" select="$header_colonna3"/>
																	<xsl:with-param name="colonna4" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna5" select="''"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'euro'"/>
																	<xsl:with-param name="value2" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value3" select="'QUANTITA'"/>
																	<xsl:with-param name="value4" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value5" select="''"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='1001' or @TEMPLATE='1001_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'TEMPLATE_PRIMAVERA'"/>
																	<xsl:with-param name="nome_sezione" select="'BONUS ANNOLUCE'"/>
																	<xsl:with-param name="colonne" select="4"/>
																	<xsl:with-param name="colonna1" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna2" select="'Corrispettivo unitario'"/>
																	<xsl:with-param name="colonna3" select="'Quantità'"/>
																	<xsl:with-param name="colonna4" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna5" select="''"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'euro'"/>
																	<xsl:with-param name="value2" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value3" select="'QUANTITA'"/>
																	<xsl:with-param name="value4" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value5" select="''"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
															<xsl:if test="@TEMPLATE='1002' or @TEMPLATE='1002_BONUS'">
																<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																	<xsl:with-param name="template" select="'TEMPLATE_OPZIONE_VERDE'"/>
																	<xsl:with-param name="nome_sezione" select="'OPZIONE VERDE'"/>
																	<xsl:with-param name="colonne" select="4"/>
																	<xsl:with-param name="colonna1" select="'Unità di misura'"/>
																	<xsl:with-param name="colonna2" select="'Corrispettivo unitario'"/>
																	<xsl:with-param name="colonna3" select="'Quantità'"/>
																	<xsl:with-param name="colonna4" select="'Totale (euro)'"/>
																	<xsl:with-param name="colonna5" select="''"/>
																	<xsl:with-param name="colonna6" select="''"/>
																	<xsl:with-param name="colonna7" select="''"/>
																	<xsl:with-param name="colonna8" select="''"/>
																	<xsl:with-param name="colonna9" select="''"/>
																	<xsl:with-param name="value1" select="'euro'"/>
																	<xsl:with-param name="value2" select="'CORRISPETTIVO_ENERGIT'"/>
																	<xsl:with-param name="value3" select="'QUANTITA'"/>
																	<xsl:with-param name="value4" select="'PREZZO_TOTALE'"/>
																	<xsl:with-param name="value5" select="''"/>
																	<xsl:with-param name="value6" select="''"/>
																	<xsl:with-param name="value7" select="''"/>
																	<xsl:with-param name="value8" select="''"/>
																	<xsl:with-param name="value9" select="''"/>
																</xsl:call-template>
															</xsl:if>
															
														</xsl:for-each>
							  
														<xsl:call-template name="Chiudi_sezione">
															<xsl:with-param name="lines_flag" select="$lines_flag"/>
														</xsl:call-template>
														
													</xsl:for-each>
													
													<xsl:choose>
														<xsl:when test="./child::PRODOTTO_SERVIZIO='Energia100'">
															<fo:table-row keep-with-previous="always" height="{$altezzariga}">                    
																<fo:table-cell number-columns-spanned="3">                    
																	<fo:block xsl:use-attribute-sets="blk.000">
																		<fo:inline xsl:use-attribute-sets="chrbold.008">Energit garantisce un risparmio di <xsl:value-of select="./child::SCONTO_CONTRATTO" />% sul corrispettivo fissato trimestralmente dall'AEEG per il mercato tutelato a copertura dei costi di generazione dell'energia. Il prossimo aggiornamento avrà effetto dal 1 <xsl:value-of select="./child::DATA_AGGIORNAMENTO_AEEG" /></fo:inline>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</xsl:when>
														
														<xsl:when test="./child::PRODOTTO_SERVIZIO='LuCerta'">
															<fo:table-row keep-with-previous="always" height="{$altezzariga}">                    
																<fo:table-cell number-columns-spanned="3">                    
																	<fo:block xsl:use-attribute-sets="blk.000">
																		<fo:inline xsl:use-attribute-sets="chrbold.008">Energit garantisce un risparmio di <xsl:value-of select="./child::SCONTO_CONTRATTO" /> cent di euro/kWh sul corrispettivo fissato trimestralmente dall'AEEG per il mercato tutelato a copertura dei costi di generazione dell'energia. Il prossimo aggiornamento avrà effetto dal 1 <xsl:value-of select="./child::DATA_AGGIORNAMENTO_AEEG" /></fo:inline>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</xsl:when>
													</xsl:choose>
												</fo:table-body>
											</fo:table>
										</xsl:if>
											
										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$pt]">
											<!--
											**************************************************************************
											**                                                                      **
											**                                PERDITE                               **
											**                                                                      **
											**************************************************************************
											-->
											<fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<!-- <fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/> -->
												<fo:table-column column-width="proportional-column-width(75)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												
												<xsl:call-template name="Header_sezione_dettaglio">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="$tipo4"/>
													<xsl:with-param name="header" select="'NO'"/>
													<xsl:with-param name="colonne" select="3"/>
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>

												<fo:table-body end-indent="0pt" start-indent="0pt">
													<!-- <fo:table-row>
														<fo:table-cell number-columns-spanned="5">
															<xsl:call-template name="Sezione_dettaglio_generica">
																<xsl:with-param name="nome_sezione" select="$pt"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA ENERGIA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>
														</fo:table-cell>
													</fo:table-row> -->
													
													<!--PT-->
													<xsl:for-each select="./child::SEZIONE[@TIPOLOGIA=$pt]">
														<xsl:for-each select="./child::PERIODO_RIFERIMENTO">
															<xsl:call-template name="TEMPLATE_ENERGIA_PARAMETRIZZATO">
																<xsl:with-param name="template" select="'TEMPLATE_ENERGIA_CONSUMER_E_X_NOI'"/>
																<xsl:with-param name="nome_sezione" select="'QUOTA ENERGIA'"/>
																<xsl:with-param name="colonne" select="8"/>
																<xsl:with-param name="colonna1" select="'Fascia'"/>
																<xsl:with-param name="colonna2" select="'Unità di misura'"/>
																<xsl:with-param name="colonna3" select="'Prezzo AEEG'"/>
																<xsl:with-param name="colonna4" select="'Sconto (%)'"/>
																<xsl:with-param name="colonna5" select="'Bonus (cent di euro)'"/>
																<xsl:with-param name="colonna6" select="'Prezzo Energit'"/>
																<xsl:with-param name="colonna7" select="'Quantità'"/>
																<xsl:with-param name="colonna8" select="'Totale (euro)'"/>
																<xsl:with-param name="colonna9" select="''"/>
																<xsl:with-param name="value1" select="'FASCIA'"/>
																<xsl:with-param name="value2" select="'cent di euro'"/>
																<xsl:with-param name="value3" select="'CORRISPETTIVO_UNITARIO'"/>
																<xsl:with-param name="value4" select="'3%'"/>
																<xsl:with-param name="value5" select="'SCONTO_ENERGIA'"/>
																<xsl:with-param name="value6" select="'CORRISPETTIVO_ENERGIT'"/>
																<xsl:with-param name="value7" select="'QUANTITA'"/>
																<xsl:with-param name="value8" select="'PREZZO_TOTALE'"/>
																<xsl:with-param name="value9" select="''"/>
															</xsl:call-template>
														</xsl:for-each>
														
														<xsl:call-template name="Chiudi_sezione">
															<xsl:with-param name="lines_flag" select="$lines_flag"/>
														</xsl:call-template>
														
													</xsl:for-each>
												</fo:table-body>
											</fo:table>
										</xsl:if>
										
										<!--
										**************************************************************************
										**                                                                      **
										**                 COMPONENTI DISPACCIAMENTO E PERDITE                  **
										**                                                                      **
										**************************************************************************
										-->
										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_eb] or
													  ./child::SEZIONE[@TIPOLOGIA=$eb_cte] or
													  (./child::SEZIONE[@TIPOLOGIA=$ert] and @CONSUMER='SI')
													 ">
											<fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												
												<xsl:call-template name="Header_sezione_dettaglio">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="$titolo2b"/>
													<xsl:with-param name="header" select="'SI'"/>
													<xsl:with-param name="colonne" select="5"/>
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>

												<fo:table-body end-indent="0pt" start-indent="0pt">
													<fo:table-row>
														<fo:table-cell number-columns-spanned="5">
															<!--Quota Fissa EB-->
															<xsl:call-template name="Sezione_dettaglio_generica">
																<xsl:with-param name="nome_sezione" select="$quota_fissa_eb"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA FISSA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>
															
															<!--EB-->
															<xsl:call-template name="Sezione_dettaglio_generica">
																<xsl:with-param name="nome_sezione" select="$eb_cte"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA ENERGIA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>
															
															<xsl:if test="@CONSUMER='SI'">
																<!--Energia Reattiva-->
																<xsl:call-template name="Sezione_dettaglio_generica">
																	<xsl:with-param name="nome_sezione" select="$ert"/>
																	<xsl:with-param name="tipo_quota" select="'ENERGIA REATTIVA'"/>
																	<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																	<xsl:with-param name="info_totale" select="'VENDITA'"/>
																	<xsl:with-param name="lines" select="$lines_flag"/>
																	<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																	<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																	<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																	<xsl:with-param name="titolo" select="$titolo2"/>
																</xsl:call-template>
															</xsl:if>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</xsl:if>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>	
						
						<xsl:if test="@CONSUMER='SI'">
							<fo:block xsl:use-attribute-sets="blk.004 chrbold.008">
								Maggiori dettagli sulle componenti A, UC, MCT sono disponibili nella sua area riservata.
							</fo:block>
						</xsl:if>
					</fo:table-cell>
				</fo:table-row>
			
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
						</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:if>
<!--
**************************************************************************
**                                                                      **
**                             IMPOSTE                                  **
**                                                                      **
**************************************************************************
-->	
	<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$addizionale_provinciale] or
				  ./child::SEZIONE[@TIPOLOGIA=$addizionale_comunale] or
				  ./child::SEZIONE[@TIPOLOGIA=$imposta_erariale]
				 ">
		<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:if test="not($ultima_sezione_reti='') or
						  not($ultima_sezione_vendita='')">
				<xsl:attribute name="space-before">2mm</xsl:attribute>
			</xsl:if>
			
			<fo:table-column column-width="proportional-column-width(100)"/>
			
			<fo:table-body>
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
						</xsl:if>
						<fo:block xsl:use-attribute-sets="blk.002">
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
						</xsl:if>
						<fo:table space-before="10mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(15)"/>
						
						<xsl:call-template name="Header_sezione_dettaglio">
							<xsl:with-param name="path" select="$svg-sezioni"/>
							<xsl:with-param name="titolo" select="$tipo3"/>
							<xsl:with-param name="header" select="'SI'"/>
							<xsl:with-param name="colonne" select="5"/>
							<xsl:with-param name="color" select="$color-sezioni"/>
							<xsl:with-param name="macro_sezione" select="'SI'"/>
						</xsl:call-template>

						<fo:table-body end-indent="0pt" start-indent="0pt">
							<fo:table-row>
								<fo:table-cell number-columns-spanned="5">
									<!--Imposta erariale-->
									<xsl:call-template name="Sezione_dettaglio_generica">
										<xsl:with-param name="nome_sezione" select="$imposta_erariale"/>
										<xsl:with-param name="tipo_quota" select="'IMPOSTA ERARIALE'"/>
										<xsl:with-param name="last_section" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="info_totale" select="'IMPOSTE'"/>
										<xsl:with-param name="lines" select="$lines_flag"/>
										<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
										<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
										<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="titolo" select="$tipo3"/>
									</xsl:call-template>
									
									<!--Addizionale enti locali (business)-->
									<xsl:call-template name="Sezione_dettaglio_generica">
										<xsl:with-param name="nome_sezione" select="$addizionale_provinciale"/>
										<xsl:with-param name="tipo_quota" select="'ADDIZIONALE ENTI LOCALI'"/>
										<xsl:with-param name="last_section" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="info_totale" select="'IMPOSTE'"/>
										<xsl:with-param name="lines" select="$lines_flag"/>
										<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
										<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
										<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="titolo" select="$tipo3"/>
									</xsl:call-template>
									
									<!--Addizionale enti locali (consumer)-->
									<xsl:call-template name="Sezione_dettaglio_generica">
										<xsl:with-param name="nome_sezione" select="$addizionale_comunale"/>
										<xsl:with-param name="tipo_quota" select="'ADDIZIONALE ENTI LOCALI'"/>
										<xsl:with-param name="last_section" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="info_totale" select="'IMPOSTE'"/>
										<xsl:with-param name="lines" select="$lines_flag"/>
										<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
										<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
										<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="titolo" select="$tipo3"/>
									</xsl:call-template>
												
									<fo:block xsl:use-attribute-sets="blk.002">
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
						</fo:table>	
					</fo:table-cell>
				</fo:table-row>
			
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
						</xsl:if>
						<fo:block xsl:use-attribute-sets="blk.002">
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:if>
</xsl:template>



<xsl:template name="Chiudi_sezione">
	<xsl:param name="lines_flag"/>
	
	<xsl:if test="./child::PRECEDENTEMENTE_FATTURATO and not(./child::PRECEDENTEMENTE_FATTURATO='0,00' or ./child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./child::PRECEDENTEMENTE_FATTURATO='0,000000')">
		<fo:table-row keep-with-previous="always" height="{$altezzariga}">
			<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="pad.l.000">
				<fo:block xsl:use-attribute-sets="blk.002">
					<fo:inline xsl:use-attribute-sets="chr.008">Precedentemente fatturato</fo:inline>
				</fo:block>
			</fo:table-cell>

			<fo:table-cell>
				<fo:block xsl:use-attribute-sets="blk.002">
					<xsl:choose>
						<xsl:when test="./child::PRECEDENTEMENTE_FATTURATO='0,00' or ./child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./child::PRECEDENTEMENTE_FATTURATO='0,000000'">
							<fo:inline xsl:use-attribute-sets="chr.008">-</fo:inline>
						</xsl:when>
						<xsl:otherwise>
							<fo:inline xsl:use-attribute-sets="chr.008"><xsl:value-of select="./child::PRECEDENTEMENTE_FATTURATO" /></fo:inline>
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:if>

	<xsl:if test="$lines_flag='SI'">
		<fo:table-row keep-with-previous="always" height="1mm">
			<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="pad.lr.000">
				<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(100)"/>
					<fo:table-body end-indent="0pt" start-indent="0pt">
						<fo:table-row height="1mm">	
							<fo:table-cell border-bottom="0.25pt dashed black">
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:if>

	<fo:table-row keep-with-previous="always" height="{$altezzariga}">
		<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="pad.l.000">
			<fo:block xsl:use-attribute-sets="blk.002">
				<fo:inline xsl:use-attribute-sets="chrbold.008">Totale</fo:inline>
			</fo:block>
		</fo:table-cell>

		<fo:table-cell>
			<fo:block xsl:use-attribute-sets="blk.002">
				<fo:inline xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="./child::RIEPILOGO_TOTALE_SEZIONE" /></fo:inline>
			</fo:block>
		</fo:table-cell>
	</fo:table-row>
</xsl:template>



</xsl:stylesheet>