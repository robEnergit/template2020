<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:fox="http://xml.apache.org/fop/extensions"
	xmlns:barcode="http://barcode4j.krysalis.org/ns"
	>

	<xsl:variable name="uppercase">ABCDEFGHIJKLMNOPQRSTUVXYWZ</xsl:variable>
	<xsl:variable name="lowercase">abcdefghijklmnopqrstuvxywz</xsl:variable>

	<xsl:template name="COMUNICAZIONE_I_PAG_FATTURA">
		<xsl:variable name="color">#f9b200</xsl:variable><!--#f08c02-->
		<xsl:variable name="color-default">#000000</xsl:variable>
		
	
		<fo:block-container position="absolute" top="2mm" left="0mm" width="90mm" height="22mm">
			<fo:block>
				<fo:table table-layout="fixed" width="100%">
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding-left="2mm">
								<xsl:choose>
									<xsl:when test="$color='black'">
										<!-- <xsl:attribute name="padding-top">13mm</xsl:attribute> -->
										<fo:block>
											<fo:external-graphic src="url(img/logo_energit_nopayoff_bn.jpg)" content-width="37mm" />
										</fo:block>
										<fo:block start-indent="2mm" font-family="Arial" font-size="5pt"
												  color="black">Via Edward Jenner, 19/21 - 09121 Cagliari
										</fo:block>
									</xsl:when>
									<xsl:otherwise>
										<!-- <xsl:attribute name="padding-top">13mm</xsl:attribute> -->
										<fo:block>
											<fo:external-graphic src="url(img/logo_energit_nopayoff.jpg)" content-width="37mm" />
										</fo:block>
										<fo:block start-indent="2mm" font-family="Arial" font-size="5pt"
												  color="black">Via Edward Jenner, 19/21 - 09121 Cagliari
										</fo:block>
									</xsl:otherwise>
								</xsl:choose>
							</fo:table-cell>
							
							<!--
							<xsl:variable name="barcode_message_header">
								<xsl:value-of select="concat(
								'F_P', ' ', 
								DATA_DOCUMENTO, ' ',
								@NUMERO_DOCUMENTO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CODICE_CLIENTE, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/INDIRIZZO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CAP, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CITTA, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/PROVINCIA)"/>
							</xsl:variable>	
							
							
							<fo:table-cell text-align="right" display-align="after"
									padding-right="17mm">
								<fo:block>
									<fo:instream-foreign-object content-height="12mm" content-width="25mm">
										<barcode:barcode message="{$barcode_message_header}">
											<barcode:datamatrix>
												<barcode:quiet-zone enabled="false">0mm</barcode:quiet-zone>
												<barcode:module-width>0.7mm</barcode:module-width>
												<xsl:if test="string-length($barcode_message_header) &lt; 48">
													<barcode:shape>force-rectangle</barcode:shape>
												</xsl:if>
											</barcode:datamatrix>
										</barcode:barcode>
									</fo:instream-foreign-object>
								</fo:block>
							</fo:table-cell>
							-->
							
							<xsl:variable name="barcode_message_header">
								<xsl:value-of select="concat(
								'F_P', ' ', 
								DATA_DOCUMENTO, ' ',
								@NUMERO_DOCUMENTO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CODICE_CLIENTE, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/INDIRIZZO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CAP, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CITTA, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/PROVINCIA)"/>
							</xsl:variable>
							
							<fo:table-cell text-align="right" display-align="after"
									padding-right="17mm">
								<fo:block>
									<fo:instream-foreign-object content-height="12mm" content-width="25mm">
										<barcode:barcode message="{$barcode_message_header}">
											<barcode:datamatrix>
												<barcode:quiet-zone enabled="false">0mm</barcode:quiet-zone>
												<barcode:module-width>0.7mm</barcode:module-width>
												<xsl:if test="string-length($barcode_message_header) &lt; 48">
													<barcode:shape>force-rectangle</barcode:shape>
												</xsl:if>
											</barcode:datamatrix>
										</barcode:barcode>
									</fo:instream-foreign-object>
								</fo:block>
							</fo:table-cell>
							
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:block-container>			
			
		<fo:block-container  position="absolute" top="45.17mm" left="93mm" width="97mm" height="26mm" xsl:use-attribute-sets="address.font">
			<fo:block-container start-indent="1mm">
				
				<xsl:choose>
					<xsl:when test="tipoAnagrafica = 'PRIVATO'">
						<fo:block>Gentile</fo:block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block>Spett.le</fo:block>
					</xsl:otherwise>
				</xsl:choose>
				
				<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INTESTATARIO" /></fo:block>

				<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INDIRIZZO" /></fo:block>

				<fo:block>
					<xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CITTA" />
					<xsl:if test="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA">
						<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA" /></fo:inline>
					</xsl:if>
				</fo:block>

				<xsl:if test="not(./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::NAZIONE='IT')">
					<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::STATO" /></fo:block>
				</xsl:if>			
			</fo:block-container>
		</fo:block-container>		
	
		
		<fo:block-container position="absolute" top="75mm" left="0mm" margin-left="11mm" margin-right="11mm" width="100%" height="100%" xsl:use-attribute-sets="text.font">
			<fo:block  xsl:use-attribute-sets="text.blk"  width="79mm">
				Cagliari, <xsl:value-of select="./child::DATA_DOCUMENTO" />
			</fo:block>		
			
			<fo:block space-before="1cm" font-weight="bold"  xsl:use-attribute-sets="text.blk"  width="100%">
				Oggetto:  Fattura n. <xsl:value-of select="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::FRASE" />	<!--  <xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /> -->
			</fo:block>	
			
			<fo:block space-before="1cm">
				Gentile Cliente,
			</fo:block>
			
			<fo:block xsl:use-attribute-sets="text.blk">
				a causa di un problema tecnico, nella fattura n. <xsl:value-of select="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::FRASE" /> da Lei già ricevuta non risultano presenti tutte le componenti tariffarie dovute per la sua fornitura di energia elettrica.
			</fo:block>
			
			<fo:block xsl:use-attribute-sets="text.blk">				
				In allegato alla presente troverà quindi la fattura integrativa n. <xsl:value-of select="@NUMERO_DOCUMENTO"/>, a completamento di quanto già fatturato nel mese di giugno.
			</fo:block>
			
			<fo:block xsl:use-attribute-sets="text.blk">				
				Le spese di spedizione della presente fattura non le saranno ovviamente addebitati.
			</fo:block>			
			
			<fo:block xsl:use-attribute-sets="text.blk">				
				Ci scusiamo per l’inconveniente.				
			</fo:block>			

			<fo:block xsl:use-attribute-sets="text.blk">
				Per ulteriori chiarimenti o informazioni può contattare il Servizio Clienti Energit al numero 800192222, attivo dal Lunedì al Venerdì dalle 8.30 alle 17.30.
			</fo:block>
			
			<fo:block space-before="2cm">
				Cordiali saluti,
			</fo:block>
			
			<fo:block xsl:use-attribute-sets="text.blk">
				Energ.it S.p.A
			</fo:block>			
			
			
		</fo:block-container>		

		
		<fo:block break-before="page"></fo:block>	
		<fo:block break-before="page"></fo:block>		
	</xsl:template>

	
	<xsl:attribute-set name="text.font">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="text.blk">
		<xsl:attribute name="space-before">3mm</xsl:attribute>
		<xsl:attribute name="line-height">14pt</xsl:attribute>
	</xsl:attribute-set>	
	
	<xsl:attribute-set name="address.font">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="line-height">10pt</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>	

</xsl:stylesheet>
