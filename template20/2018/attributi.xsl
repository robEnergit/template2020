<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>


<!--
********************************************************
**                                                    **
**             Definizione parametri                  **
**                                                    **
********************************************************
-->
<xsl:param name="altezzariga">3.5mm</xsl:param>
<xsl:param name="altezzariga_sezione">0.5mm</xsl:param>
<xsl:param name="altezzariga_totale">5.5mm</xsl:param>
<xsl:param name="altezzarigavuota">1.5mm</xsl:param>
<xsl:param name="lettere_sezioni">SI</xsl:param>

<xsl:template name="ATTRIBUTI">
</xsl:template>


<!--
********************************************************
**                                                    **
**                   Attributi                        **
**                                                    **
********************************************************
-->



<xsl:attribute-set name="blk.000">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">10pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.header">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">11pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.002">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">9pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.003">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">17pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.004">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">13pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.010">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">20pt</xsl:attribute>
</xsl:attribute-set>



<!--
*************
**         **
**  FONTS  **
**         **
*************
-->
<xsl:attribute-set name="chr.006">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">6pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.008">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chravviso">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chravvisobold">
    <xsl:attribute name="font-family">universcbold</xsl:attribute>
    <xsl:attribute name="font-size">8.5pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="condensedcors.008">
    <xsl:attribute name="font-family">universccors</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.007">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">7pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.009">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chrtitolodettaglio">
    <xsl:attribute name="font-family">universcbold</xsl:attribute>
    <xsl:attribute name="font-size">12pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chrbold.008">
    <xsl:attribute name="font-family">universcbold</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set>

<!-- <xsl:attribute-set name="chrboldbis.009">
    <xsl:attribute name="font-family">universcbold</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="chrbis.009">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
</xsl:attribute-set> -->

<xsl:attribute-set name="chrbold.009">
    <xsl:attribute name="font-family">universbold</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.pagamenti">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">12pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="pad.t.003">
    <xsl:attribute name="padding-top">2mm</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="pad.l.000">
    <xsl:attribute name="padding-left">3mm</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="pad.r.000">
    <xsl:attribute name="padding-right">3mm</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="pad.lr.000">
    <xsl:attribute name="padding-left">3mm</xsl:attribute>
    <xsl:attribute name="padding-right">3mm</xsl:attribute>
</xsl:attribute-set>


<xsl:attribute-set name="pad_bottom_1mm">
    <xsl:attribute name="padding-bottom">1mm</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="pad_top_2mm">
    <xsl:attribute name="padding-top">2mm</xsl:attribute>
</xsl:attribute-set>

<!--
*************
**         **
**  BORDI  **
**         **
*************
-->
<xsl:attribute-set name="brd.b.000">
    <xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="brd.br.000">
    <xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
    <xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="brd.t.000">
    <xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="brd.000">
    <xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
    <xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
    <xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
    <xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="brd.r.000">
    <xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="brd.l.ultimi_consumi">
    <xsl:attribute name="border-left">0.5pt dotted black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blocco_riquadri_frontespizio">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">justify</xsl:attribute>
    <xsl:attribute name="line-height">13pt</xsl:attribute>
	<xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blocco_riquadro_reclami">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">center</xsl:attribute>
    <xsl:attribute name="line-height">6mm</xsl:attribute>
	<xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blocco_riquadro_reclami2">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">center</xsl:attribute>
    <xsl:attribute name="line-height">3mm</xsl:attribute>
	<xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
	<xsl:attribute name="display-align">center</xsl:attribute>
</xsl:attribute-set>

<!--                 Attributi usati nell'header                        -->

<xsl:attribute-set name="chr.condizioni_header">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">7pt</xsl:attribute>
	<xsl:attribute name="font-weight">bold</xsl:attribute>
	<xsl:attribute name="line-height">7pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.condizioni_header2">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">7pt</xsl:attribute>
	<xsl:attribute name="line-height">7pt</xsl:attribute>
</xsl:attribute-set>

<!-- .................................................................. -->



<!--                 Attributi usati nel footer                         -->
<xsl:attribute-set name="blocco_footer">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">10pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="font_footer">
    <xsl:attribute name="font-family">univers</xsl:attribute>
    <xsl:attribute name="font-size">5.5pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="font_footer2">
    <xsl:attribute name="font-family">univers</xsl:attribute>
    <xsl:attribute name="font-size">6.4pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.condizioni_footer">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">7pt</xsl:attribute>
	<xsl:attribute name="line-height">7pt</xsl:attribute>
</xsl:attribute-set>
<!-- .................................................................. -->



<!--           Attributi usati per l'indirizzo di spedizione            -->
<xsl:attribute-set name="blocco_indirizzo_spedizione">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">12pt</xsl:attribute>
	<xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">10pt</xsl:attribute>
</xsl:attribute-set>
<!-- .................................................................. -->






<!--         Attributi usati nella sintesi (mono-multisito)             -->
<xsl:attribute-set name="block_sintesi">
	<xsl:attribute name="start-indent">0pt</xsl:attribute>
	<xsl:attribute name="end-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-align">start</xsl:attribute>
	<xsl:attribute name="line-height">10pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="font_sintesi">
	<xsl:attribute name="font-family">universcbold</xsl:attribute>
	<xsl:attribute name="font-size">7pt</xsl:attribute>
</xsl:attribute-set>
<!-- .................................................................. -->






<!-- Attributi usati nella tabella consumi energia degli ultimi 12 mesi -->
<!-- <xsl:attribute-set name="font_consumi">
	<xsl:attribute name="font-family">universc</xsl:attribute>
	<xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->

<xsl:attribute-set name="block_sintesi">
	<xsl:attribute name="start-indent">0pt</xsl:attribute>
	<xsl:attribute name="end-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-align">center</xsl:attribute>
	<xsl:attribute name="line-height">9pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="block_sintesi_start">
	<xsl:attribute name="start-indent">0pt</xsl:attribute>
	<xsl:attribute name="end-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-align">start</xsl:attribute>
	<xsl:attribute name="line-height">9pt</xsl:attribute>
</xsl:attribute-set>
<!-- .................................................................. -->








<!--
********************************************************
**                                                    **
**                   Attributi                        **
**                                                    **
********************************************************
-->



<xsl:attribute-set name="chrTot">
	<xsl:attribute name="font-family">universcbold</xsl:attribute>
	<xsl:attribute name="font-size">10pt</xsl:attribute>
</xsl:attribute-set>


<xsl:attribute-set name="brd.b.spesso">
	<xsl:attribute name="border-bottom">1.5pt solid black</xsl:attribute>
</xsl:attribute-set>


<!--        Attributi usati nelle tabelle in generale        -->
<xsl:attribute-set name="font_titolo_tabella_bold">
	<xsl:attribute name="font-family">universcbold</xsl:attribute>
	<xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set>
<!-- ....................................................... -->


<!-- Attributi usati nella tabella letture_consumi_conguagli -->
<xsl:attribute-set name="block_letture">
	<xsl:attribute name="start-indent">0pt</xsl:attribute>
	<xsl:attribute name="end-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-align">start</xsl:attribute>
	<xsl:attribute name="line-height">9pt</xsl:attribute>
</xsl:attribute-set>
<!-- ....................................................... -->


<!-- <xsl:attribute-set name="blocco_salto_pagina">
	<xsl:attribute name="start-indent">0pt</xsl:attribute>
	<xsl:attribute name="end-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-align">start</xsl:attribute>
	<xsl:attribute name="line-height">6mm</xsl:attribute>
</xsl:attribute-set> -->


<!-- <xsl:attribute-set name="pad.r.000">
    <xsl:attribute name="padding-right">6mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="pad.t.001">
    <xsl:attribute name="padding-top">2mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="padding_codici_iva">
    <xsl:attribute name="padding-left">-15mm</xsl:attribute>
</xsl:attribute-set> -->	
	
<!--
*************
**         **
** PADDING **
**         **
*************
-->


<!-- <xsl:attribute-set name="pad.lt.001">
	<xsl:attribute name="padding-left">6mm</xsl:attribute>
	<xsl:attribute name="padding-top">2mm</xsl:attribute>
</xsl:attribute-set> -->
<!--
*************
**         **
**  BORDI  **
**         **
*************
-->	

	<!--
*************
**         **
** BLOCCHI **
**         **
*************
-->
<!-- DOPPIONE -->
<!-- <xsl:attribute-set name="blk.007">    
	<xsl:attribute name="start-indent">0pt</xsl:attribute>
	<xsl:attribute name="end-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-align">start</xsl:attribute>
	<xsl:attribute name="line-height">26mm</xsl:attribute>
</xsl:attribute-set> -->
<!-- <xsl:attribute-set name="blk.011">
	<xsl:attribute name="start-indent">0pt</xsl:attribute>
	<xsl:attribute name="end-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-align">start</xsl:attribute>
	<xsl:attribute name="line-height">3.5mm</xsl:attribute>
</xsl:attribute-set> -->
<!-- <xsl:attribute-set name="blk.012">
	<xsl:attribute name="start-indent">0pt</xsl:attribute>
	<xsl:attribute name="end-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-indent">3mm</xsl:attribute>
	<xsl:attribute name="text-align">start</xsl:attribute>
	<xsl:attribute name="line-height">4.5mm</xsl:attribute>
</xsl:attribute-set> -->
<!--
*************
**         **
**  FONTS  **
**         **
*************
-->
<!-- <xsl:attribute-set name="chrfooter">
	<xsl:attribute name="font-family">univers</xsl:attribute>
	<xsl:attribute name="font-size">6.4pt</xsl:attribute>
</xsl:attribute-set> -->


<!-- <xsl:attribute-set name="chrguasti">
	<xsl:attribute name="font-family">universc</xsl:attribute>
	<xsl:attribute name="font-size">9.5pt</xsl:attribute>
</xsl:attribute-set> -->

<!--        Attributi usati nelle tabelle in generale        -->
<!-- <xsl:attribute-set name="font_titolo_tabella_bold">
	<xsl:attribute name="font-family">universcbold</xsl:attribute>
	<xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->
<!-- ....................................................... -->
	
<!-- <xsl:attribute-set name="blk.001">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">2pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="blk.005">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">14mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="blk.006">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">12mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="blk.007">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">18mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="blk.008">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">8mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="blk.009">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">16mm</xsl:attribute>
</xsl:attribute-set> -->	
	
<!-- <xsl:attribute-set name="blk.salto">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">7mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="blk.3mm">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">3mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="blk.1mm">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">1mm</xsl:attribute>
</xsl:attribute-set> -->	
	
<!-- <xsl:attribute-set name="tratteggio">
    <xsl:attribute name="font-family">univers</xsl:attribute>
    <xsl:attribute name="font-size">7pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="chrbis.006">
    <xsl:attribute name="font-family">univers</xsl:attribute>
    <xsl:attribute name="font-size">6pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="font_dettaglio_fonia">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->	

<!-- <xsl:attribute-set name="universcors.008">
    <xsl:attribute name="font-family">universcors</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="chrbold.007">
    <xsl:attribute name="font-family">universcbold</xsl:attribute>
    <xsl:attribute name="font-size">7pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="chrattivo.008">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="chrbis.008">
    <xsl:attribute name="font-family">univers</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="Arial.009">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="chr.012">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">12pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="chrbold.006">
    <xsl:attribute name="font-family">universcbold</xsl:attribute>
    <xsl:attribute name="font-size">6pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="chrboldbis.008">
    <xsl:attribute name="font-family">universbold</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->

<!--
*************
**         **
** PADDING **
**         **
*************
-->
<!-- <xsl:attribute-set name="pad.t.000">
    <xsl:attribute name="padding-top">-3mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="pad.t.logo">
    <xsl:attribute name="padding-top">-7mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="pad.t.001">
    <xsl:attribute name="padding-top">-1mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="pad.b.001">
    <xsl:attribute name="padding-bottom">1mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="pad.t.002">
    <xsl:attribute name="padding-top">16pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="brd.bl.000">
    <xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
    <xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="pad_top_1mm">
    <xsl:attribute name="padding-top">1mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="pad_bottom_2mm">
    <xsl:attribute name="padding-bottom">2mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="brd.tb.000">
    <xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
    <xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="brd.l.000">
    <xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="brd.lr.000">
    <xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
    <xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
</xsl:attribute-set> -->


<!--         Attributi usati per i contatti del distributore            -->
<!-- <xsl:attribute-set name="blocco_autolettura">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">13pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="font_autolettura">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="font_autolettura_bold">
    <xsl:attribute name="font-family">universcbold</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->


<!-- .................................................................. -->



<!--   Attributi usati nella sezione "Informazioni contatti reclami"    -->
<!-- <xsl:attribute-set name="blocco_reclami_2">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">12pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="blocco_reclami">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">center</xsl:attribute>
    <xsl:attribute name="line-height">6mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="blocco_reclami_email">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">9pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="font_riquadri_frontespizio">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="font_reclami_2">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="font_reclami_3">
    <xsl:attribute name="font-family">universcbold</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->
<!-- .................................................................. -->

<!-- <xsl:attribute-set name="font_guasti">
    <xsl:attribute name="font-family">universc</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
</xsl:attribute-set> -->
<!-- .................................................................. -->



<!--   Attributi usati per inserire interlinee di vario tipo (leader)   -->
<!-- <xsl:attribute-set name="spazio_12mm">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">12mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="spazio_7mm">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">7mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="spazio_18mm">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">18mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="spazio_15mm">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">15mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="spazio_10mm">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">10mm</xsl:attribute>
</xsl:attribute-set> -->

<!-- <xsl:attribute-set name="spazio_5mm">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">5mm</xsl:attribute>
</xsl:attribute-set> -->
<!-- .................................................................. -->
</xsl:stylesheet>