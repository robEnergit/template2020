<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:barcode="http://barcode4j.krysalis.org/ns">

<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**            Inclusione templates esterni            **
********************************************************
-->
<xsl:include href="attributi.xsl"/>
<xsl:include href="Template_Energia_Multisito.xsl"/>
<xsl:include href="202_09/Template_Energia_Multisito_012011.xsl"/>
<xsl:include href="Finale.xsl"/>
<xsl:include href="202_09/Finale_012011.xsl"/>
<!-- <xsl:include href="bollettino_vecchio_selecta.xsl"/> -->
<xsl:include href="bollettino.xsl"/>
<xsl:include href="condizioni.xsl"/>
<xsl:include href="condizioni_c11.xsl"/>
<xsl:include href="scheda_riepilogo.xsl"/>
<xsl:include href="scheda_riepilogo_c11.xsl"/>
<xsl:include href="comunicazioni_condizioni.xsl"/>
<xsl:include href="comunicazioni_condizioni_c11.xsl"/>
<xsl:include href="Autocertificazione.xsl"/>
<xsl:include href="Informativa_Qualita.xsl"/>

<!--
**********************************************************
**				Template LOTTO_FATTURE					**
**********************************************************
-->
<xsl:template match="LOTTO_FATTURE">

<fo:root line-stacking-strategy="font-height">

<fo:layout-master-set>
    <!--
    ********************************************************
    **           Definizione pagina fattura               **
    ********************************************************
    -->
    <fo:simple-page-master margin-bottom="10mm" margin-left="11mm" margin-right="11mm" margin-top="5mm" master-name="pm0" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="17mm" margin-top="12mm" overflow="visible" region-name="body"/>
        <fo:region-before extent="12mm" overflow="visible" region-name="header"/>
        <fo:region-after display-align="after" extent="17mm" overflow="visible" region-name="footer"/>
    </fo:simple-page-master>
	
	<fo:simple-page-master margin-bottom="10mm" margin-left="11mm" margin-right="11mm" margin-top="5mm" master-name="pm0first" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="17mm" margin-top="12mm" overflow="visible" region-name="body"/>
        <fo:region-before extent="12mm" overflow="visible" region-name="headernew"/>
        <fo:region-after display-align="after" extent="17mm" overflow="visible" region-name="footer"/>
    </fo:simple-page-master>
    
    <fo:simple-page-master margin-bottom="0mm" margin-left="0mm" margin-right="0mm" margin-top="0mm" master-name="pm0-blank" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="0mm" margin-top="0mm" overflow="visible" region-name="body"/>
    </fo:simple-page-master>
    
    <!--
    ********************************************************
    **          Definizione pagina bollettino             **
    ********************************************************
    -->
    <fo:simple-page-master master-name="pm1"
		page-height="210mm" page-width="297mm" margin="0mm">
		<fo:region-body margin-top="108mm" />
		<fo:region-before extent="102mm" overflow="hidden" />
	</fo:simple-page-master>
    
    <!--
    *****************************************************************************
    **    Definizione pagine allegati condizioni contrattuali - comparativa    **
    *****************************************************************************
    -->
	<fo:simple-page-master margin-bottom="5mm" margin-left="10mm" margin-right="10mm" margin-top="10mm" master-name="pm0_condizioni" page-height="297mm" page-width="210mm">
		<fo:region-body margin-bottom="0mm" margin-top="5mm" overflow="visible" region-name="body" />
		<fo:region-before extent="10mm" overflow="visible" region-name="header_condizioni" />
		<fo:region-after display-align="after" extent="10mm" overflow="visible" region-name="footer_condizioni" />
	</fo:simple-page-master>

	<fo:simple-page-master margin-bottom="10mm" margin-left="10mm" margin-right="10mm" margin-top="10mm" master-name="pm0_scheda_riepilogo" page-height="297mm" page-width="210mm">
		<fo:region-body margin-bottom="0mm" margin-top="0mm" overflow="visible" region-name="body" />
		<fo:region-after display-align="after" extent="10mm" overflow="visible" region-name="footer_riepilogo" />
	</fo:simple-page-master>

	<fo:simple-page-master margin-top="5mm" margin-bottom="10mm" master-name="pm0_comunicazione_condizioni" page-height="297mm" page-width="210mm">
		<fo:region-body margin-top="3cm" margin-bottom="25mm" margin-left="29mm" margin-right="29mm" overflow="visible" region-name="body" />
		<fo:region-before extent="3cm" overflow="visible" region-name="header" />
		<fo:region-after display-align="after" extent="25mm" overflow="visible" region-name="footer" />
	</fo:simple-page-master>
    
    <fo:page-sequence-master master-name="document">
      <fo:repeatable-page-master-alternatives>
        <fo:conditional-page-master-reference
          master-reference="pm0" page-position="rest" blank-or-not-blank="not-blank"/>
        <fo:conditional-page-master-reference
          master-reference="pm0first" page-position="first"  blank-or-not-blank="not-blank"/>
		<fo:conditional-page-master-reference
          master-reference="pm0-blank" page-position="any" blank-or-not-blank="blank"/>
      </fo:repeatable-page-master-alternatives>
    </fo:page-sequence-master>
	
	
</fo:layout-master-set>

<xsl:apply-templates select="DOCUMENTO"/>

</fo:root>
</xsl:template>





<!--
**********************************************************
**					Template DOCUMENTO					**
**********************************************************
-->
<xsl:template match="DOCUMENTO">

<xsl:variable name="bordi">NO</xsl:variable><!--rectangle_table-->
<xsl:variable name="rectangle_frontespizio">svg/rectangle_frontespizio</xsl:variable>
<xsl:variable name="color">black</xsl:variable><!--#f08c02-->
<xsl:variable name="color_riquadro_scadenza">black</xsl:variable><!--black-->
<xsl:variable name="color_titolo_dettaglio">black</xsl:variable><!--black-->
<xsl:variable name="sfondo_titoli">#DDDDDD</xsl:variable><!--#DDDDDD-->
<xsl:variable name="color-sezioni">black</xsl:variable><!--#004a76-->
<xsl:variable name="color-sottosezioni">black</xsl:variable><!--#004a76-->
<xsl:variable name="color-default">#000000</xsl:variable>
<xsl:variable name="svg-sezioni">'url(svg/rectangle_titolo_sezioni_short.svg)'</xsl:variable>
<xsl:variable name="svg-sottosezioni">'url(svg/rectangle_titolo_sezioni_short_blank.svg)'</xsl:variable>
<xsl:variable name="svg-altre-sezioni">'url(svg/rectangle_titolo_sezioni_short.svg)'</xsl:variable>
<xsl:variable name="svg-dettaglio">'url(svg/rectangle_dettaglio.svg)'</xsl:variable>
<xsl:variable name="autocertificazione">NO</xsl:variable>
<xsl:variable name="qualita">SI</xsl:variable>

<xsl:variable name="status_piu_uno"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:variable>
<xsl:variable name="data_doc_number"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2),substring(DATA_DOCUMENTO,1,2))"/></xsl:variable>

<fo:page-sequence initial-page-number="1" master-reference="document" orphans="1" white-space-collapse="true" widows="1" id="F">
    <!--
    ********************************************************
    **                     Header                         **
    ********************************************************
    -->
    <fo:static-content flow-name="headernew">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
            <fo:table-column column-width="proportional-column-width(100)"/>
            <fo:table-body end-indent="0pt" start-indent="0pt">
                <fo:table-row>
                    <fo:table-cell display-align="center" xsl:use-attribute-sets="blk.header">
                        <fo:block text-align="end">
							<xsl:if test="$status_piu_uno &lt; 201307">
								<xsl:if test="@SPOT='NO' or not(@SPOT)">
									<fo:inline font-family="universbold" font-size="11pt">Codice Cliente <xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
								</xsl:if>
							</xsl:if>
                        </fo:block>
                        <fo:block text-align="end">   
							<xsl:if test="$status_piu_uno &lt; 201307">
								<xsl:if test="@SPOT='NO' or not(@SPOT)">
									<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Energit</fo:inline>
								</xsl:if>
							</xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </fo:static-content>
	
	<fo:static-content flow-name="header">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
            <fo:table-column column-width="proportional-column-width(100)"/>
            <fo:table-body end-indent="0pt" start-indent="0pt">
                <fo:table-row>
                    <fo:table-cell display-align="center" xsl:use-attribute-sets="blk.header">
                        <fo:block text-align="end">
							<xsl:if test="@SPOT='NO' or not(@SPOT)">
								<fo:inline font-family="universbold" font-size="11pt">Codice Cliente <xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
							</xsl:if>
                        </fo:block>
                        <fo:block text-align="end">   
							<xsl:if test="@SPOT='NO' or not(@SPOT)">
								<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Energit</fo:inline>
							</xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </fo:static-content>

    <!--
    ********************************************************
    **                     Footer                         **
    ********************************************************
    -->
    <fo:static-content flow-name="footer">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
			<fo:table-column column-width="proportional-column-width(85)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell display-align="center">
						<fo:block xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer2">
								<xsl:choose>
									<xsl:when test="@TIPO_DOCUMENTO='FATTURA'">Fattura: </xsl:when>
									<xsl:otherwise>Nota di credito: </xsl:otherwise>
								</xsl:choose> 
								<xsl:value-of select="@NUMERO_DOCUMENTO"/>  del  <xsl:value-of select="./child::DATA_DOCUMENTO" />
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell display-align="center">
						<fo:block text-align="end" xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer2">
								<fo:inline xsl:use-attribute-sets="font_footer2">Pagina </fo:inline>
								<fo:inline text-align="end" xsl:use-attribute-sets="font_footer2"><fo:page-number/>/<fo:page-number-citation ref-id="{generate-id(.)}"/></fo:inline>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row height="2mm">
					<fo:table-cell number-columns-spanned="2" display-align="center" border-bottom="0.5 dashed thick black">
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row height="2mm">
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" display-align="center">
						<fo:block text-align="center" xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer">Energ.it S.p.A. <xsl:if test="$status_piu_uno &gt; 201209 and $data_doc_number &lt; 20130127">in liquidazione</xsl:if> - Via Edward Jenner, 19/21 - 09121 Cagliari - Servizio Clienti 800.19.22.22 - Fax 800.19.22.55 - P.IVA 02605060926</fo:inline>
						</fo:block>
						<fo:block text-align="center" xsl:use-attribute-sets="blocco_footer">
							<xsl:if test="$status_piu_uno &lt; 201212"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Societa' per Azioni con Socio Unico. Direzione e Coordinamento di Alpiq Italia S.r.l.</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201211 and $status_piu_uno &lt; 201509"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società con Socio Unico soggetta ad attività di direzione e coordinamento di Onda s.r.l.</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201508 and $status_piu_uno &lt; 201511"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società soggetta a direzione e coordinamento di Enertronica S.p.A.</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201510"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società per azioni con Socio Unico</fo:inline></xsl:if>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
        </fo:table>
    </fo:static-content>

    <!--
    ********************************************************
    **                  Inizio body                       **
    ********************************************************
    -->
    <fo:flow flow-name="body">
		<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
		
		<!--
		********************************************************
		**               Logo E Data Matrix                   **
		********************************************************
		-->
		<fo:block-container position="absolute"
							top="-3mm"
							left="2mm"
							width="90mm"
							height="22mm">
			<fo:block>
				<fo:table table-layout="fixed" width="100%">
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<xsl:choose>
									<xsl:when test="$color='black'">
										<fo:block>
											<xsl:if test="$status_piu_uno &lt; 201212"><fo:external-graphic src="url(img/logo_energit_bn.png)" content-width="37mm" /></xsl:if>
											<xsl:if test="$status_piu_uno &gt; 201211"><fo:external-graphic src="url(img/logo_energit_nopayoff_bn.jpg)" content-width="37mm" /></xsl:if>
										</fo:block>
										<fo:block start-indent="2mm" font-family="Arial" font-size="5pt"
												  color="black">Via Edward Jenner, 19/21 - 09121 Cagliari
										</fo:block>
									</xsl:when>
									<xsl:otherwise>
										<fo:block>
											<xsl:if test="$status_piu_uno &lt; 201212"><fo:external-graphic src="url(img/logo-energit.jpg)" content-width="35mm" /></xsl:if>
											<xsl:if test="$status_piu_uno &gt; 201211"><fo:external-graphic src="url(img/logo_energit_nopayoff.jpg)" content-width="37mm" /></xsl:if>
										</fo:block>
										<fo:block start-indent="2mm" font-family="Arial" font-size="5pt"
												  color="black">Via Edward Jenner, 19/21 - 09121 Cagliari
										</fo:block>
									</xsl:otherwise>
								</xsl:choose>
							</fo:table-cell>
							
							<xsl:variable name="barcode_message_header">
								<xsl:value-of select="concat(
								'F_S', ' ', 
								DATA_DOCUMENTO, ' ',
								@NUMERO_DOCUMENTO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CODICE_CLIENTE, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/INDIRIZZO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CAP, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CITTA, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/PROVINCIA)"/>
							</xsl:variable>
							
							<fo:table-cell text-align="right" display-align="after"
									padding-right="17mm">
								<fo:block>
									<fo:instream-foreign-object content-height="12mm" content-width="25mm">
										<barcode:barcode message="{$barcode_message_header}">
											<barcode:datamatrix>
												<barcode:quiet-zone enabled="false">0mm</barcode:quiet-zone>
												<barcode:module-width>0.7mm</barcode:module-width>
												<xsl:if test="string-length($barcode_message_header) &lt; 48">
													<barcode:shape>force-rectangle</barcode:shape>
												</xsl:if>
											</barcode:datamatrix>
										</barcode:barcode>
									</fo:instream-foreign-object>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:block-container>
		

		<xsl:if test="$status_piu_uno &gt; 201306 and @SPOT='NO'">
			<fo:block-container position="absolute"
								top="4mm"
								left="109.3mm"
								width="79mm"
								height="16mm"
								background-image="./svg/bordo_codice_cliente.svg"
								background-repeat="no-repeat"
								display-align="center"
								text-align="center"
								font-family="universcbold"
								font-size="14pt">
				<fo:block>
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						Codice Cliente <fo:inline font-family="universcbold" font-size="18pt"><xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
					</xsl:if>
				</fo:block>
				<fo:block>   
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Energit</fo:inline>
					</xsl:if>
				</fo:block>
			</fo:block-container>
		</xsl:if>
		
	
		<!--
		*********************************************************
		**  Dati anagrafici e indirizzo di spedizione fattura  **
		*********************************************************
		-->
		<fo:block-container position="absolute"
							top="44.17mm"
							left="100mm"
							width="97mm"
							height="26mm">
			<xsl:call-template name="SPEDIZIONE_FATTURA" />
		</fo:block-container>
		
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" space-before="40mm">
			<fo:table-column column-width="proportional-column-width(34)"/>
			<fo:table-column column-width="proportional-column-width(66)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell display-align="center" xsl:use-attribute-sets="pad.l.000">
						<xsl:call-template name="DATI_ANAGRAFICI" />
					</fo:table-cell>

					<fo:table-cell padding-left="31.7mm" padding-top="19mm"
						border-left="0.5 dashed thick black">
						<fo:block />
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<!--
		**********************************************
		**  Riquadro fattura con bordi arrotondati  **
		**********************************************
		-->
		<fo:table  space-before="4mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row height="23mm">
					<fo:table-cell display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(<xsl:value-of select="$rectangle_frontespizio"/>.svg)</xsl:attribute>
							<xsl:call-template name="RIQUADRO_SCADENZA_FATTURA">
								<xsl:with-param name="color" select="$color_riquadro_scadenza"/>
							</xsl:call-template>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<!--
		*********************************************************
		**  Modalita' di pagamento e riepilogo importi fattura **
		*********************************************************
		-->
		<fo:table  space-before="4mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(34)"/>
			<fo:table-column column-width="proportional-column-width(66)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row height="40mm">        
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
						<fo:block>
							<xsl:call-template name="PAGAMENTI"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" border-left="0.5 dashed thick black">    
						<fo:block>
							<xsl:call-template name="DETTAGLIO_FRONTESPIZIO"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
        </fo:table>
		
		<!--
		**********************************************************
		**					Avvisi e riquadri					**
		**********************************************************
		-->
		<xsl:if test="@SPOT='NO' or not(@SPOT)">
			<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
				<fo:table-column column-width="proportional-column-width(100)"/>
				<fo:table-body>
					<fo:table-row height="80mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
							<xsl:if test="$bordi='SI'">
								<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
								<xsl:attribute name="background-image">url(svg/rectangle_frontespizio_2.svg)</xsl:attribute>
							</xsl:if>
							<!--
							*****************************
							**   Avvisi frontespizio   **
							*****************************
							-->
							<xsl:call-template name="AVVISI_FRONTESPIZIO"/>
							
							<!--
							*************************************************************************
							**  - Riquadro lettura contatore - autolettura                         **
							**  - Segnalazione guasti                                              **
							**  - Informazioni contatti e reclami                                  **
							*************************************************************************
							-->
							<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
								<fo:table-column column-width="proportional-column-width(35.5)"/>
								<fo:table-column column-width="proportional-column-width(29)"/>
								<fo:table-column column-width="proportional-column-width(35.5)"/>
								<fo:table-body end-indent="0pt" start-indent="0pt">    
									<fo:table-row>
										<xsl:if test="./child::CONTRATTO_ENERGIA">
											<!--
											***********************************************
											**  Riquadro lettura contatore - autolettura **
											***********************************************
											-->
											<fo:table-cell xsl:use-attribute-sets="pad.r.000">
												<fo:block>
													<xsl:call-template name="LETTURA_AUTOLETTURA_FRONTESPIZIO"/>
												</fo:block>
											</fo:table-cell>
											<!--
											***************************
											**  Segnalazione guasti  **
											***************************
											-->
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000" border-left="0.5 dashed thick black" border-right="0.5 dashed thick black">
												<fo:block>
													<xsl:call-template name="SEGNALAZIONE_GUASTI"/>
												</fo:block>
											</fo:table-cell>
										</xsl:if>
										<xsl:if test="not(./child::CONTRATTO_ENERGIA)">
											<fo:table-cell>
												<fo:block/>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block/>
											</fo:table-cell>
										</xsl:if>
										<!--
										***************************************
										**  Informazioni contatti e reclami  **
										***************************************
										-->
										<fo:table-cell xsl:use-attribute-sets="pad.l.000">
											<fo:block>
												<xsl:if test="$status_piu_uno &lt; 201212">
													<xsl:call-template name="CONTATTI_RECLAMI">
														<xsl:with-param name="color" select="$color"/>
													</xsl:call-template>
												</xsl:if>
												<xsl:if test="$status_piu_uno &gt; 201211">
													<xsl:call-template name="CONTATTI_RECLAMI2">
														<xsl:with-param name="color" select="$color"/>
													</xsl:call-template>
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>    
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</xsl:if>
		
		<xsl:if test="@SPOT='SI'">
			<xsl:call-template name="TEMPLATE_RIEPILOGO_FATTURA"/>
		</xsl:if>
		
		<!--
		******************************
		**  Frase imposta di bollo  **
		******************************
		-->
		<fo:block space-before="2mm" xsl:use-attribute-sets="blk.000" start-indent="3mm">
			<fo:inline xsl:use-attribute-sets="chr.006">L'imposta di bollo, se dovuta, viene assolta in modo virtuale (Aut. Agenzia Entrate, uff. Cagliari 1 n. 19756 del 29/04/2005).</fo:inline>
		</fo:block>
		
		<xsl:if test="@SPOT='NO' or not(@SPOT)">
			<fo:block break-before="page">
			</fo:block>
			
			<!--
			**********************************************************************
			**	Sintesi e dettaglio dei contratti energia presenti in fattura	**
			**********************************************************************
			-->
			<xsl:if test="./child::CONTRATTO_ENERGIA">
				<xsl:choose>
					<xsl:when test="$status_piu_uno &lt; 201101 or substring(@COMPETENZA,8,4) &lt; 2011">
						<xsl:call-template name="CONTRATTI_MULTISITO">
							<xsl:with-param name="bordi" select="$bordi"/>
							<xsl:with-param name="color" select="$color"/>
							<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
							<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
							<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
							<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
							<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
							<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
							<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="CONTRATTI_MULTISITO_012011">
							<xsl:with-param name="bordi" select="$bordi"/>
							<xsl:with-param name="color" select="$color"/>
							<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
							<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
							<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
							<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
							<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
							<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
							<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			
			<!--
			**********************
			**	Altri contratti	**
			**********************
			-->
			<xsl:apply-templates select="CONTRATTO_DEPOSITO_CAUZIONALE">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
				
			<xsl:apply-templates select="CONTRATTO_FONIA">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="CONTRATTO_SERVIZI_VARI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="CONTRATTO_AREASERVER">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
			
			<!--
			******************************************************************************
			**		Riepilogo fattura e altre sezioni									**
			**		(comunicazioni ai clienti, fatture in attesa pagamento, etc)		**
			******************************************************************************
			-->
			<xsl:choose>
				<xsl:when test="$status_piu_uno &lt; 201101">
					<xsl:call-template name="FINALE">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="FINALE_012011">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			
			<!--
			**********************************
			**  Iinformativa sulla qualità  **
			**********************************
			-->
			<xsl:if test="./child::CONTRATTO_ENERGIA and $qualita='SI'">
				<xsl:call-template name="INFORMATIVA_QUALITA"/>
			</xsl:if>
		</xsl:if>
		
		<!--
		*********************************************************
		**  Blocco per la determinazione del numero di pagine  **
		*********************************************************
		-->
		<fo:block id="{generate-id(.)}"/>
		
    </fo:flow>
</fo:page-sequence>    


<!--
**********************************************************
**					Rinnovi se presenti					**
**********************************************************
-->
<xsl:for-each select="./child::RINNOVI/child::RINNOVO[@VISUALIZZA='SI']">
    <fo:page-sequence master-reference="pm0-blank">
        <fo:flow flow-name="body">
					<fo:block-container position="absolute"
										top="3.75pt"
										left="-0.75pt"
										width="210mm"
										height="297mm"
										display-align="after">
						<fo:block text-align="center">
							<fo:external-graphic>
								<xsl:attribute name="height">297mm</xsl:attribute>
								<xsl:attribute name="content-height">297mm</xsl:attribute>
								<xsl:attribute name="content-width">210mm</xsl:attribute>
								<xsl:if test="./child::ALLEGATO='AL-FAEN0110FX'">
									<xsl:attribute name="src">./allegati_rinnovi/<xsl:value-of select="./child::ALLEGATO"/>-RN.pdf</xsl:attribute>
								</xsl:if>
								<xsl:if test="not(./child::ALLEGATO='AL-FAEN0110FX')">
									<xsl:attribute name="src">./allegati_rinnovi/<xsl:value-of select="./child::ALLEGATO"/>.pdf</xsl:attribute>
								</xsl:if>
							</fo:external-graphic>
						</fo:block>
					</fo:block-container>
        </fo:flow>
    </fo:page-sequence>
</xsl:for-each>

<!--
**********************************************************
**			  Nuove condizioni contrattuali				**
**********************************************************
-->
<xsl:if test="./child::CONTRATTO_ENERGIA[@NUOVE_CONDIZIONI='B1']">
    <!--Comunicazione nuove condizioni contrattuali business-->
    <xsl:call-template name="COMUNICAZIONE_CONDIZIONI"/>
    <!--Comparativa business-->
    <xsl:call-template name="SCHEDA_RIEPILOGO"/>
    <!--Nuove condizioni contrattuali B1-->
    <xsl:call-template name="CONDIZIONI"/>
</xsl:if>

<xsl:if test="./child::CONTRATTO_ENERGIA[@NUOVE_CONDIZIONI='C11']">
    <!--Comunicazione nuove condizioni contrattuali consumer-->
    <xsl:call-template name="COMUNICAZIONE_CONDIZIONI_C11"/>
    <!--Comparativa consumer-->
	<xsl:call-template name="SCHEDA_RIEPILOGO_C11"/>
    <!--Nuove condizioni contrattuali C11-->
    <xsl:call-template name="CONDIZIONI_C11"/>
</xsl:if>

<!--
**********************************************************
**					Autocertificazione					**
**********************************************************
-->
<xsl:if test="CONTRATTO_ENERGIA and $autocertificazione='SI' and (@SPOT='NO' or not(@SPOT))">
	<xsl:call-template name="AUTOCERTIFICAZIONE"/>
</xsl:if>

<!--
********************************************************
**             Accoda eventuali bollettini            **
********************************************************
-->
<xsl:if test="./child::BOLLETTINO and (@SPOT='NO' or not(@SPOT))">
	<xsl:call-template name="BOLLETTINO"/>
</xsl:if>

</xsl:template>





<!--
***************************************
**  Informazioni contatti e reclami  **
***************************************
-->
<xsl:template name="CONTATTI_RECLAMI">
	<xsl:param name="color"/>
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
	<fo:table-column column-width="proportional-column-width(100)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<fo:table-row>
			<fo:table-cell xsl:use-attribute-sets="blocco_riquadro_reclami">
				<fo:block xsl:use-attribute-sets="font_titolo_tabella_bold">
					INFORMAZIONI - CONTATTI - RECLAMI
				</fo:block>
				
				<xsl:if test="$color='black'">
					<fo:block space-before="10mm">
						<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="34mm" src="img/LogoServizioClienti_BN2009.jpg"/>
					</fo:block>
				</xsl:if>
				
				<xsl:if test="not($color='black')">
					<fo:block space-before="10mm">
						<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="34mm" src="img/numtel.png"/>
					</fo:block>
				</xsl:if>
				
				<fo:block>Attivo dal lunedi' al venerdi' dalle 8.30 alle 19.30</fo:block>
				
				<fo:block border-bottom="0.5 dashed thick black"/>
				
				<fo:block>energia@energit.it</fo:block>
				
				<fo:block border-bottom="0.5 dashed thick black"/>
				
				<fo:block>Energ.it S.p.A. - Via E. Jenner, 19/21 - 09121 Cagliari</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:template>

<xsl:template name="CONTATTI_RECLAMI2">
	<xsl:param name="color"/>
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
	<fo:table-column column-width="proportional-column-width(60)"/>
	<fo:table-column column-width="proportional-column-width(40)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<fo:table-row>
			<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="blocco_riquadro_reclami2">
				<fo:block xsl:use-attribute-sets="font_titolo_tabella_bold">
					INFORMAZIONI - CONTATTI - RECLAMI
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding-top="11mm" xsl:use-attribute-sets="blocco_riquadro_reclami2">
				<xsl:if test="$color='black'">
					<fo:block>
						<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="34mm" src="img/LogoServizioClienti_BN2009.jpg"/>
					</fo:block>
				</xsl:if>
				
				<xsl:if test="not($color='black')">
					<fo:block>
						<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="34mm" src="img/numtel.png"/>
					</fo:block>
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="blocco_riquadro_reclami2">
				<fo:block>Gratuito da rete fissa</fo:block>
				<fo:block>(lun-ven 8.30 - 19.30)</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding-top="1mm" number-columns-spanned="2" xsl:use-attribute-sets="blocco_riquadro_reclami2">
				<fo:block>Da cellulare comporre il numero 070 7521 422</fo:block>
				<fo:block font-size="7pt">I costi della chiamata dipendono dal proprio operatore telefonico</fo:block>
				<fo:block font-size="9pt" space-before="2mm"><fo:inline font-family="universcbold">800.19.22.55 </fo:inline>Fax Gratuito</fo:block>
				
				<fo:block border-bottom="0.5 dashed thick black" space-before="1mm"/>
				
				<fo:block font-family="universcbold" font-size="9pt" space-before="1mm">energia@energit.it</fo:block>
				
				<fo:block border-bottom="0.5 dashed thick black" space-before="1mm"/>
				
				<fo:block font-family="universcbold" font-size="9pt" space-before="1mm">Energit S.p.A. - via E. Jenner, 19/21 - 09121 Cagliari</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:template>





<!--
***********************************************
**  Riquadro lettura contatore - autolettura **
***********************************************
-->
<xsl:template name="LETTURA_AUTOLETTURA_FRONTESPIZIO">
	<fo:block xsl:use-attribute-sets="blocco_riquadri_frontespizio">
		<xsl:choose>
			<xsl:when test="./child::AUTOLETTURA">
				<fo:block text-align="center" xsl:use-attribute-sets="font_titolo_tabella_bold">
					AUTOLETTURA
				</fo:block>
				
				<xsl:choose>
					<xsl:when test="./child::AUTOLETTURA='MONO'">
						<fo:block>Per ricevere fatture allineate ai suoi consumi reali, usufruisca del servizio di autolettura!</fo:block>
					</xsl:when>
				  
					<xsl:when test="./child::AUTOLETTURA='MULTI'">
						<fo:block>Per ricevere fatture allineate ai suoi consumi reali, usufruisca del servizio di autolettura, riservato ai punti di prelievo monorari!</fo:block>
					</xsl:when>
				</xsl:choose>
				
				<fo:block xsl:use-attribute-sets="font_titolo_tabella_bold">Potra' trasmettere i consumi all'indirizzo autolettura@energit.it o al numero gratuito 800.1922.33 dal 25<xsl:value-of select="./child::CONTRATTO_ENERGIA/child::DATA_AUTOLETTURA_INIZIALE" /> al <xsl:value-of select="./child::CONTRATTO_ENERGIA/child::DATA_AUTOLETTURA_FINALE" />.</fo:block>
				
				<fo:block>I consumi comunicati potranno essere utilizzati a partire dalla seconda autolettura.</fo:block>
			</xsl:when>
		
			<xsl:otherwise>
				<fo:block text-align="center" xsl:use-attribute-sets="font_titolo_tabella_bold">
					LETTURA DEL CONTATORE
				</fo:block>
				
				<fo:block>La sua utenza e' inserita nel sistema di telelettura attraverso il contatore elettronico. Addebiteremo i suoi consumi effettivi ogni volta che saranno resi disponibili dal suo Distributore Locale.</fo:block>
			</xsl:otherwise>
		</xsl:choose>
	</fo:block>
</xsl:template>





<!--
***************************
**  Segnalazione guasti  **
***************************
-->
<xsl:template name="SEGNALAZIONE_GUASTI">
	<fo:block xsl:use-attribute-sets="blocco_riquadri_frontespizio">
		<fo:block text-align="center" xsl:use-attribute-sets="font_titolo_tabella_bold">
			SEGNALAZIONE GUASTI
		</fo:block>

		<fo:block>In caso di guasti dovra' contattare il Distributore Locale al numero telefonico <xsl:value-of select="./child::CONTRATTO_ENERGIA/child::CONTATTO_DISTRIBUTORE" />.</fo:block>
	</fo:block>
</xsl:template>





<!--
******************************
**  Modalita' di pagamento  **
******************************
-->
<xsl:template name="PAGAMENTI">
	<fo:block xsl:use-attribute-sets="chr.008">
		<fo:block xsl:use-attribute-sets="blk.003 chrbold.008">
			PAGAMENTI
		</fo:block>
		
		<xsl:choose>
			<xsl:when test="@TIPO_DOCUMENTO='FATTURA' and not(./child::IMPORTO_DOCUMENTO='0,00')">
				<xsl:choose>
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
						<fo:block xsl:use-attribute-sets="chrbold.008">Modalita' di pagamento</fo:block>
					</xsl:when>
					
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_PE_REQ']">
						<fo:block xsl:use-attribute-sets="chrbold.008">La modalita' di pagamento per questa fattura e'</fo:block>
					</xsl:when>
					
					<xsl:otherwise>
						<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'La fattura risulta pagata')">
							<fo:block xsl:use-attribute-sets="chrbold.008">Lei ha scelto di pagare con</fo:block>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				
				<xsl:choose>
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_PE_REQ']">
						<fo:block>Bollettino Postale</fo:block>					
						<fo:block space-before="2mm" xsl:use-attribute-sets="chrbold.008">La sua richiesta di attivazione della modalita' addebito diretto SEPA Direct Debit (SDD) e' in attesa di conferma presso la sua Banca. Per il pagamento di questa fattura dovra' utilizzare il bollettino postale che trova nell'ultimo foglio. Grazie.</fo:block>
						<fo:block space-before="2mm" xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CAUSALE" /></fo:block>
					</xsl:when>
					
					<xsl:otherwise>
						<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO" /></fo:block>
				
						<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bonifico Bancario'">
							<fo:block>Energ.it S.p.A.</fo:block>
						</xsl:if>
						
						<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::INTESTATARIO_PAGAMENTO" /></fo:block>
						
						<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bollettino Postale'">
							<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE" /></fo:block>
						</xsl:if>
						
						<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::IBAN">
							<fo:block xsl:use-attribute-sets="chr.009">IBAN: <xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::IBAN" /></fo:block>
						</xsl:if>
						
						<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE = 'Sardex'">
							<fo:block xsl:use-attribute-sets="chr.009">Sardex</fo:block>
						</xsl:if>
						
						<fo:block xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CAUSALE" /></fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="./child::IMPORTO_DOCUMENTO='0,00'">
						<fo:block xsl:use-attribute-sets="chr.pagamenti">Non c'e' niente da pagare!</fo:block>
					</xsl:when>
					
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RIMBORSO_SDD']">
						<fo:block xsl:use-attribute-sets="chr.pagamenti">Non c'e' niente da pagare!</fo:block>
						<fo:block xsl:use-attribute-sets="chrbold.008">L'importo totale del documento di <xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> euro le sara' rimborsato tramite accredito sul suo conto corrente normalmente utilizzato per il pagamento delle fatture Energit.</fo:block>
					</xsl:when>
					
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RIMBORSO_ASS']">
						<fo:block xsl:use-attribute-sets="chr.pagamenti">Non c'e' niente da pagare!</fo:block>
						<fo:block xsl:use-attribute-sets="chrbold.008">L'importo totale del documento di <xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> euro le sara' rimborsato tramite assegno inviato al suo indirizzo di spedizione delle fatture.</fo:block>
					</xsl:when>
					
					<xsl:otherwise>
						<fo:block xsl:use-attribute-sets="chr.pagamenti">Per questa nota di credito non c'e' niente da pagare</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</fo:block>	
</xsl:template>





<!--
*****************************
**   Avvisi frontespizio   **
*****************************
-->
<xsl:template name="AVVISI_FRONTESPIZIO">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">	
			<!-- Comunicazione Energit per noi -->
			<fo:table-row height="27mm">
				<fo:table-cell>
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3'">
						
							<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute>
							<xsl:attribute name="padding">1mm</xsl:attribute> -->
							
							
							<xsl:variable name="contratto_exnoi">
								<xsl:for-each select="./child::CONTRATTO_ENERGIA">
									<xsl:if test="substring(./child::PRODOTTO_SERVIZIO,0,16)='Energit per Noi'">
										<xsl:value-of select="./child::CONTRACT_NO" />
									</xsl:if>
								</xsl:for-each>
							</xsl:variable>
						
							<fo:block font-family="universcbold">
								ENERGIT PER NOI RINNOVA ANCORA LA SUA CONVENIENZA
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Il risparmio Energit per Noi è confermato per il terzo anno consecutivo!
								Anche nel 2012, infatti, il suo contratto
								<xsl:value-of select="substring($contratto_exnoi,0,8)" /> le offre un
								<fo:inline font-family="universcbold">prezzo bloccato della componente energia
								invariato rispetto al 2010 e al 2011: soli 0,05 euro/kWh in tutte le fasce
								orarie</fo:inline>, il massimo risparmio e la comodità di consumare liberamente
								in tutte le ore della giornata. Potrà aumentare ulteriormente il suo risparmio
								con un uso accorto e responsabile dell’energia, che le permetterà di limitare i
								consumi e di salvaguardare l’ambiente!
							</fo:block>
						</xsl:if>
						
						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='NUOVO_PARTNER_INDUSTRIALE'">
							<fo:block font-family="universcbold">
								UN NUOVO PARTNER INDUSTRIALE E UN MONDO DI NUOVE OFFERTE ENERGIT: ENERGIA ELETTRICA, GAS, IMPIANTI FOTOVOLTAICI E MOLTO DI PIU’
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Dal 23/11/2012 <fo:inline font-family="universcbold">Energit ha un nuovo partner industriale: Onda Energia!</fo:inline>
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Ai nostri Clienti garantiremo la fornitura senza interruzioni,
								il mantenimento delle condizioni contrattuali e… tante occasioni di risparmio.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								<fo:inline font-family="universcbold">Per scoprire in anteprima le nuove offerte
								che le abbiamo dedicato chiami l’800.19.22.22</fo:inline>; non appena la voce guida richiederà l’inserimento
								di un <fo:inline font-family="universcbold">codice promozione</fo:inline>,
								digiti <fo:inline font-family="universcbold">50</fo:inline> sulla tastiera del telefono.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								La aspettiamo!
							</fo:block>
						</xsl:if>
						
						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_BANCA_CARIGE'] and
									  not(PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='NUOVO_PARTNER_INDUSTRIALE')">
							<fo:block font-family="universcbold">
								NUOVI RIFERIMENTI BANCARI PER IL PAGAMENTO DELLE FATTURE
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								La invitiamo ad utilizzare il <fo:inline font-family="universcbold">nuovo conto corrente
								bancario Energit</fo:inline> per il pagamento delle fatture.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energ.it S.p.A. <fo:inline font-family="universcbold">Codice IBAN
								<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_BANCA_CARIGE']/child::ID='COMUNICAZIONE_BANCA_CARIGE_V1'">IT43I0617516901000000790980</xsl:if>
								<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_BANCA_CARIGE']/child::ID='COMUNICAZIONE_BANCA_CARIGE_V2'">IT22X0343116901000000790980</xsl:if>
								</fo:inline>. Banca Carige Italia S.p.A.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Grazie!
							</fo:block>
						</xsl:if>
						
						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS_FRONTESPIZIO']/child::ID='PAPERLESS_V4' and
									  not(PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='NUOVO_PARTNER_INDUSTRIALE')">
							<fo:block font-family="universcbold">
								PASSI ALLA FATTURA VIA EMAIL: RISPARMIERA’ FINO A 18 EURO ALL’ANNO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								<fo:inline font-family="universcbold">Scelga di ricevere la fattura via email:
								risparmierà le spese di spedizione del documento cartaceo</fo:inline>, pari ad
								1,50 € per fattura. Fino a 18 euro all'anno!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								La fattura via email, inoltre, rispetta l’ambiente, arriva tempestivamente ed è a colori!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Invii la sua richiesta a <fo:inline font-family="universcbold">paperless@energit.it</fo:inline> e
								specificando il suo codice cliente <xsl:value-of select="./child::CODICE_CLIENTE" />.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_FATTURA_INTEGRATIVA']/child::ID='COMUNICAZIONE_FATTURA_INTEGRATIVA_V1'">
							<fo:block space-before="2mm">
								<fo:inline font-family="universcbold">INFORMAZIONE IMPORTANTE</fo:inline>. Il presente
								documento si intende ad integrazione della precedente fattura,
								emessa in data 11/11/2012. Ci scusiamo per l’inconveniente,
								dovuto a cause tecniche, e la ringraziamo per aver scelto Energit.
							</fo:block>
						</xsl:if>
						
						<!-- TOP6000 -->
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE91'
									 ">
							<fo:block font-family="universcbold">
								PER LEI, GLI ESCLUSIVI SCONTI TOP6000 CHE AUMENTANO NEL TEMPO: LI RICHIEDA SUBITO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato
								gli <fo:inline font-family="universcbold">EXTRA SCONTI TOP6000, del valore di 3-6-9 euro/MWh;
								per ottenerli deve solo richiederli!</fo:inline> Non perda questa occasione: trova maggiori
								informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE92'
									 ">
							<fo:block font-family="universcbold">
								PER LEI, GLI ESCLUSIVI SCONTI TOP6000 CHE AUMENTANO NEL TEMPO: SCOPRA COME OTTENERLI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit desidera offrirle
								gli <fo:inline font-family="universcbold">EXTRA SCONTI TOP6000, del valore
								di 3-6-9 euro/MWh</fo:inline> e riservati ai Clienti in regola con i pagamenti:
								non perda questa opportunità! Trova tutti i dettagli sull’iniziativa nella
								sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE111'
									 ">
							<fo:block font-family="universcbold">
								INFORMAZIONE IMPORTANTE
							</fo:block>
							<fo:block font-family="universcbold">
								ULTIMI GIORNI PER RICHIEDERE GLI ESCLUSIVI SCONTI TOP6000 CHE AUMENTANO NEL TEMPO: SI AFFRETTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato gli <fo:inline font-family="universcbold">EXTRA SCONTI TOP6000, del valore
								di 3-6-9 euro/MWh; per ottenerli deve solo richiederli entro il 31 luglio 2012!</fo:inline> Non perda questa occasione:
								trova maggiori informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<!-- EnergiTOP -->
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE128'
									 ">
							<fo:block font-family="universcbold">
								ENERGITOP LE OFFRE 3+6+12 EURO DI EXTRA SCONTO SULL’ENERGIA. SCELGA IL RISPARMIO CHE RADDOPPIA NEL TEMPO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato
								gli <fo:inline font-family="universcbold">EXTRA SCONTI ENERGITOP, del valore di 3+6+12  euro/MWh;
								per ottenerli deve solo richiederli!</fo:inline> Non perda questa occasione: trova maggiori
								informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE129'
									 ">
							<fo:block font-family="universcbold">
								ENERGITOP LE OFFRE 3+6+12 EURO DI EXTRA SCONTO SULL’ENERGIA. SCELGA IL RISPARMIO CHE RADDOPPIA NEL TEMPO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit desidera offrirle
								gli <fo:inline font-family="universcbold">EXTRA SCONTI ENERGITOP, del valore di 3+6+12
								euro/MWh</fo:inline> e riservati ai Clienti in regola con i pagamenti: non perda questa
								opportunità! Trova tutti i dettagli sull’iniziativa nella sezione “Comunicazioni ai Clienti” di
								questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE172'
									 ">
							<fo:block font-family="universcbold">
								CON MAXIBONUS ENERGIT LE OFFRE 45 euro DI EXTRA SCONTO: NE APPROFITTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit desidera offrirle
								gli <fo:inline font-family="universcbold">EXTRA SCONTI MAXIBONUS, per un totale di 45 euro</fo:inline> e riservati
								ai Clienti in regola con i pagamenti: non perda questa opportunità! Trova tutti i dettagli sull’iniziativa nella
								sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE173'
									 ">
							<fo:block font-family="universcbold">
								CON MAXIBONUS ENERGIT LE OFFRE 45 euro DI EXTRA SCONTO: NE APPROFITTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato gli 
								<fo:inline font-family="universcbold">EXTRA SCONTI MAXIBONUS, per un totale di 45 euro; per ottenerli deve solo richiederli!</fo:inline> Non
								perda questa occasione: trova maggiori informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE174'
									 ">
							<fo:block font-family="universcbold">
								COMPLIMENTI, LEI RICEVE GLI EXTRA SCONTI MAXIBONUS, RISERVATI AI MIGLIORI CLIENTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Per maggiori informazioni consulti la sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE193'
									 ">
							<fo:block font-family="universcbold">
								COMPLIMENTI, LEI RICEVE GLI EXTRA SCONTI MAXIBONUS, RISERVATI AI MIGLIORI CLIENTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Per maggiori informazioni consulti la sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<!-- Frasi SDD ERRATO/REVOCATO - Deposito Cauzionale -->
			<fo:table-row height="11mm">
				<fo:table-cell>
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO='Addebito tramite SDD')">
							<xsl:choose>
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
									<fo:block font-family="universcbold">
										ATTENZIONE! La sua Banca ha respinto la richiesta di attivazione dell'addebito diretto SEPA Direct Debit (SDD).
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>
								
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_REVOCATO']">
									<fo:block font-family="universcbold">
										Riattivi subito l'addebito diretto SEPA Direct Debit (SDD) per evitare il pagamento del deposito cauzionale!
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>
								
								<xsl:otherwise>
									<xsl:if test="@PRESENZA_DEPOSITO_CAUZIONALE='SI'">
										<fo:block font-family="universcbold">
											Rientri in possesso del deposito cauzionale passando all'addebito diretto SEPA Direct Debit (SDD)!
										</fo:block>
										<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
									</xsl:if>  
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
						
			<!-- Avviso fatture pagate/in attesa di pagamento -->
			<fo:table-row height="14mm">
				<fo:table-cell border-bottom="0.5 dashed thick black">
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="not(./child::FATTURE_ATTESA_PAGAMENTO) or (./child::FATTURE_ATTESA_PAGAMENTO and (count(./child::FATTURE_ATTESA_PAGAMENTO) &lt; 1))">
							<fo:block font-family="universcbold">
								TUTTE LE FATTURE PRECEDENTI RISULTANO PAGATE. GRAZIE.
							</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7.5pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								<fo:block font-size="7.5pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
						
						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO)=1">
							<fo:block font-family="universcbold">
								ATTENZIONE: 1 FATTURA RISULTA IN ATTESA DI PAGAMENTO.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7.5pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								<fo:block font-size="7.5pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
						
						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO) &gt; 1">
							<fo:block font-family="universcbold">
								ATTENZIONE: <xsl:value-of select="count(./child::FATTURE_ATTESA_PAGAMENTO)"/> FATTURE RISULTANO IN ATTESA DI PAGAMENTO.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7.5pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								<fo:block font-size="7.5pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>  
		</fo:table-body>
	</fo:table>
</xsl:template>





<!--
**********************
**  Dati anagrafici **
**********************
-->
<xsl:template name="DATI_ANAGRAFICI">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.004 chr.009">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">   
			<fo:table-row>
				<fo:table-cell>
					<fo:block xsl:use-attribute-sets="chrbold.009">
						Cliente
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INTESTATARIO" /></fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INDIRIZZO" /></fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row border-bottom="0.5 dashed thick black">
				<fo:table-cell>
					<fo:block>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CITTA" />
						<xsl:if test="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA">
							<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA" /></fo:inline>
						</xsl:if>
						<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::NAZIONE='IT')">
							<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::STATO" /></fo:inline>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell padding-top="1mm">
					<fo:block>
						<fo:inline xsl:use-attribute-sets="chrbold.009">
							Partita IVA
						</fo:inline>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell>
					<fo:block>
						<fo:inline xsl:use-attribute-sets="chrbold.009">
							Codice Fiscale
						</fo:inline>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_FISCALE" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<xsl:if test="@SPOT='NO' or not(@SPOT)">
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:inline xsl:use-attribute-sets="chrbold.009">
								UserID
							</fo:inline>
							<xsl:value-of select="./child::USERNAME" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
			
			<!-- <xsl:if test="./child::CIG">
				<xsl:if test="@SPOT='NO' or not(@SPOT)">
					<fo:table-row>
						<fo:table-cell>
							<fo:block>
								<fo:inline xsl:use-attribute-sets="chrbold.009">
									Codice CIG
								</fo:inline>
								<xsl:value-of select="./child::CIG" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</xsl:if> -->
			
		</fo:table-body>
	</fo:table>
</xsl:template>





<!--
**********************************************
**  Riquadro fattura con bordi arrotondati  **
**********************************************
-->
<xsl:template name="RIQUADRO_SCADENZA_FATTURA">
	<xsl:param name="color"/>
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.004" font-family="universc" font-size="10pt">
	<xsl:attribute name="color"><xsl:value-of select="$color"/></xsl:attribute>
	<fo:table-column column-width="proportional-column-width(34)"/>
	<fo:table-column column-width="proportional-column-width(66)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">        
		<fo:table-row height="21.5mm">        
			<fo:table-cell xsl:use-attribute-sets="pad.lr.000" line-height="11pt">
				<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(100)"/>
					<fo:table-body end-indent="0pt" start-indent="0pt">   
						<fo:table-row>
							<fo:table-cell>
								<fo:block font-family="universcbold" font-size="12pt">
									<xsl:choose>
										<xsl:when test="@TIPO_DOCUMENTO='FATTURA'">
											Fattura
										</xsl:when>
										<xsl:otherwise>
											Nota di credito
										</xsl:otherwise>
									</xsl:choose>
									<xsl:value-of select="@NUMERO_DOCUMENTO" />
								</fo:block>
								
								<fo:block font-family="universcbold" font-size="12pt">del <xsl:value-of select="./child::DATA_DOCUMENTO" /></fo:block>
								
								<fo:block font-size="8pt">
											<fo:inline font-size="8pt">Frequenza di fatturazione </fo:inline>
											<xsl:value-of select="./child::PERIODICITA_FATTURAZIONE"/>
										</fo:block>
					
								
								<xsl:choose>    
									<xsl:when test="@TIPO_DOCUMENTO='FATTURA'">
										<xsl:choose>
											<xsl:when test="not(@COMPETENZA)">
												<fo:block>Periodo di fatturazione</fo:block>
												<fo:block>
													<xsl:value-of select="./child::PERIODO_DOCUMENTO" />
												</fo:block>
											</xsl:when>
											
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="@TIPO_COMPETENZA='fattura'">
														<fo:block>
															A
															<xsl:value-of select="@DESCRIZIONE_DOCUMENTO" />
															della fattura
															<xsl:value-of select="@COMPETENZA" />
														</fo:block>
													</xsl:when>
													<xsl:otherwise>
														<fo:block>
															A
															<xsl:value-of select="@DESCRIZIONE_DOCUMENTO" />
															della
														</fo:block>
														<fo:block>
															<xsl:value-of select="@TIPO_COMPETENZA" />
															<xsl:value-of select="@COMPETENZA" />
														</fo:block>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="not(@COMPETENZA)">
												<xsl:if test="./child::CONTRATTO_ENERGIA">
													<fo:block>Periodo di fatturazione</fo:block>
													<fo:block>
														<xsl:value-of select="./child::PERIODO_DOCUMENTO" />
													</fo:block>
												</xsl:if>
											</xsl:when>
											
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="@TIPO_COMPETENZA='fattura'">
														<fo:block>
															A <xsl:value-of select="@DESCRIZIONE_DOCUMENTO" /> della <xsl:value-of select="@TIPO_COMPETENZA" /><xsl:text>&#160;</xsl:text><xsl:value-of select="@COMPETENZA" />
														</fo:block>
													</xsl:when>
													<xsl:otherwise>
														<fo:block>
															A <xsl:value-of select="@DESCRIZIONE_DOCUMENTO" /> della
														</fo:block>
														
														<fo:block>
															<xsl:value-of select="@TIPO_COMPETENZA" /><xsl:text>&#160;</xsl:text><xsl:value-of select="@COMPETENZA" />
														</fo:block>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
			
			<fo:table-cell border-left="0.5 dashed thick black" xsl:use-attribute-sets="pad.lr.000">  
				<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" font-family="universcbold" font-size="18pt" text-align="end">
					<fo:table-column column-width="proportional-column-width(100)"/>
					<fo:table-body end-indent="0pt" start-indent="0pt">   
						<fo:table-row>
							<fo:table-cell>
								<xsl:choose>
									<xsl:when test="@TIPO_DOCUMENTO='FATTURA' and not(@COMPETENZA) and not(./child::SCADENZA_DOCUMENTO='******') and not(./child::IMPORTO_DOCUMENTO='0,00') and not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'La fattura risulta pagata')">                                    
										<fo:block>
											<fo:inline font-size="14pt">Il totale da pagare entro il </fo:inline>
											<xsl:value-of select="./child::SCADENZA_DOCUMENTO" />
											<fo:inline font-size="14pt"> e':</fo:inline>
										</fo:block>
									</xsl:when>
									<xsl:otherwise>
										<fo:block>Totale documento</fo:block>
									</xsl:otherwise>
								</xsl:choose>
								
								<fo:block space-before="2mm">
									<xsl:value-of select="./child::IMPORTO_DOCUMENTO" />
									euro
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:template>





<!--
***********************************
**  Indirizzo spedizione fattura **
***********************************
-->
<xsl:template name="SPEDIZIONE_FATTURA">
	<fo:block xsl:use-attribute-sets="blocco_indirizzo_spedizione" color="black">
		<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INTESTATARIO" /></fo:block>

		<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INDIRIZZO" /></fo:block>

		<fo:block>
			<xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CITTA" />
			<xsl:if test="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA">
				<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA" /></fo:inline>
			</xsl:if>
		</fo:block>

		<xsl:if test="not(./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::NAZIONE='IT')">
			<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::STATO" /></fo:block>
		</xsl:if>
	</fo:block>
</xsl:template>





<!--
*********************************
**  Riepilogo importi fattura  **
*********************************
-->
<xsl:template name="DETTAGLIO_FRONTESPIZIO">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="chr.008" display-align="center">
	<fo:table-column column-width="proportional-column-width(31)"/>
	<fo:table-column column-width="proportional-column-width(18)"/>
	<fo:table-column column-width="proportional-column-width(18)"/>
	<fo:table-column column-width="proportional-column-width(15)"/>
	<fo:table-column column-width="proportional-column-width(18)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<fo:table-row xsl:use-attribute-sets="chrbold.008">
			<fo:table-cell number-columns-spanned="5">
				<fo:block>DETTAGLIO</fo:block>
			</fo:table-cell>
		</fo:table-row>        
		
		<fo:table-row xsl:use-attribute-sets="chrbold.008">                            
			<fo:table-cell>
				<fo:block>Descrizione</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>Imponibile</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>Aliquota</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>IVA</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>Totale documento</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row height="0.5mm" border-bottom="0.5 dashed thick black">
			<fo:table-cell number-columns-spanned="5">
				<fo:block/>
			</fo:table-cell>
		</fo:table-row> 
		
		<fo:table-row height="0.5mm">
			<fo:table-cell number-columns-spanned="5">
				<fo:block/>
			</fo:table-cell>
		</fo:table-row> 
		
		<xsl:for-each select="./child::DETTAGLIO_FATTURA">       
			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::PRODOTTO_SERVIZIO" /></fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::IMPONIBILE" /></fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ALIQUOTA_IVA" />%</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::IMPORTO_IVA" /></fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::TOTALE" /></fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:for-each>
	</fo:table-body>
	</fo:table>
</xsl:template>

</xsl:stylesheet>