<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>



<!--
***********************************
**                               **
**       Letture in fattura      **
**                               **
***********************************
-->
<!-- <xsl:template match="LETTURE">
	<xsl:param name="bordi"/>
	<xsl:param name="sfondo_titoli"/>
	<xsl:param name="color"/>
	
	<xsl:if test="@TEMPLATE">
		<xsl:call-template name="LETTURE_PARAMETRIZZATO">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			<xsl:with-param name="tipologia" select="@TEMPLATE"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template> -->

<xsl:template match="LETTURE">
<xsl:param name="bordi"/>
<xsl:param name="color"/>
<xsl:param name="svg-titoli"/>
<xsl:param name="tipo"/>
<xsl:param name="mese_fattura"/>

<xsl:if test="@TEMPLATE">
	<xsl:if test="$tipo='header'">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body>
			<fo:table-row keep-with-previous="always" height="2mm">
				<fo:table-cell>
					<fo:block></fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row height="6mm">
				<fo:table-cell padding-left="3mm" display-align="center">
					<xsl:attribute name="background-image"><xsl:value-of select="$svg-titoli"/></xsl:attribute>
					<fo:block text-align="start">
						<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">LETTURE</fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
		</fo:table>
	</xsl:if>

	<xsl:if test="$tipo='body'">
		<xsl:call-template name="LETTURE_PARAMETRIZZATO">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="tipologia" select="@TEMPLATE"/>
		</xsl:call-template>
	</xsl:if>
</xsl:if>
</xsl:template>



<!--
**********************************************************************************

			  TEMPLATE LETTURE_REALE_AUTOLETTURA_AUTOLETTURA

**********************************************************************************
-->
<xsl:template name="LETTURE_PARAMETRIZZATO">
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="tipologia"/>

	<xsl:param name="title_color">
		<xsl:if test="$color='black'">#ffffff</xsl:if>
		<xsl:if test="not($color='black')">#ffffff</xsl:if>
	</xsl:param>
	<xsl:param name="title_color2">
		<xsl:if test="$color='black'">#ffffff</xsl:if>
		<xsl:if test="not($color='black')">#ffffff</xsl:if>
	</xsl:param>
	
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="block_sintesi chr.008">
	<fo:table-body>
		<fo:table-row height="2mm">
			<fo:table-cell number-columns-spanned="14">
				<fo:block/>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row keep-with-previous="always">
			<fo:table-cell>
				<fo:block/>
			</fo:table-cell>

			<xsl:if test="$tipologia='S' or $tipologia='RS' or $tipologia='RAA'">
				<fo:table-cell number-columns-spanned="3" display-align="center">
					<xsl:call-template name="Header">
						<xsl:with-param name="tipo" select="'Reale'"/>
						<xsl:with-param name="color1" select="$title_color"/>
						<xsl:with-param name="color2" select="$title_color2"/>
					</xsl:call-template>
				</fo:table-cell>
			</xsl:if>

			<xsl:if test="$tipologia='AA' or $tipologia='RAA'">
				<fo:table-cell number-columns-spanned="3" display-align="center">
					<xsl:call-template name="Header">
						<xsl:with-param name="tipo" select="'Autolettura1'"/>
						<xsl:with-param name="color1" select="$title_color"/>
						<xsl:with-param name="color2" select="$title_color2"/>
					</xsl:call-template>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="3" display-align="center">
					<xsl:call-template name="Header">
						<xsl:with-param name="tipo" select="'Autolettura2'"/>
						<xsl:with-param name="color1" select="$title_color"/>
						<xsl:with-param name="color2" select="$title_color2"/>
					</xsl:call-template>
				</fo:table-cell>
			</xsl:if>
			
			<xsl:if test="$tipologia='RS'">
				<fo:table-cell number-columns-spanned="3" display-align="center">
					<xsl:call-template name="Header">
						<xsl:with-param name="tipo" select="'Stima'"/>
						<xsl:with-param name="color1" select="$title_color"/>
						<xsl:with-param name="color2" select="$title_color2"/>
					</xsl:call-template>
				</fo:table-cell>
			</xsl:if>

			<xsl:if test="(./child::LETTURA[@TIPO='ATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='REATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='POTENZA']/child::COSTANTE)">
				<xsl:if test="$tipologia='S'">
					<fo:table-cell display-align="center">
						<!-- <fo:block>Costante*</fo:block> -->
						<xsl:call-template name="Header">
							<xsl:with-param name="tipo" select="'Costante'"/>
							<xsl:with-param name="color1" select="$title_color"/>
							<xsl:with-param name="color2" select="$title_color2"/>
						</xsl:call-template>
					</fo:table-cell>
				</xsl:if>
				<xsl:if test="not($tipologia='S')">
					<fo:table-cell display-align="center">
						<!-- <fo:block>Costante**</fo:block> -->
						<xsl:call-template name="Header">
							<xsl:with-param name="tipo" select="'Costante2'"/>
							<xsl:with-param name="color1" select="$title_color"/>
							<xsl:with-param name="color2" select="$title_color2"/>
						</xsl:call-template>
					</fo:table-cell>
				</xsl:if>
			</xsl:if>
		</fo:table-row>
		
		
		<!-- riga attiva -->
		<fo:table-row keep-with-previous="always">
			<!-- <fo:table-cell xsl:use-attribute-sets="border" display-align="center" padding-left="3pt">
				<xsl:attribute name="background-color"><xsl:value-of select="$title_color"/></xsl:attribute>
				<fo:block text-align="left" xsl:use-attribute-sets="block_sintesi_start">
					attiva
				</fo:block>
			</fo:table-cell> -->
			<xsl:call-template name="valore_lettura">
				<xsl:with-param name="valore" select="'Attiva'"/>
				<xsl:with-param name="colore" select="$title_color"/>
			</xsl:call-template>
							
			<xsl:if test="$tipologia='S' or $tipologia='RS' or $tipologia='RAA'">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::DISTRIBUTORE_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::DISTRIBUTORE_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::DISTRIBUTORE_F3"/>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="$tipologia='AA' or $tipologia='RAA'">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::STIMA_PREC_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::STIMA_PREC_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::STIMA_PREC_F3"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::STIMA_ATTUALE_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::STIMA_ATTUALE_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::STIMA_ATTUALE_F3"/>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="$tipologia='RS'">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::STIMA_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::STIMA_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::STIMA_F3"/>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="(./child::LETTURA[@TIPO='ATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='REATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='POTENZA']/child::COSTANTE)">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='ATTIVA']/child::COSTANTE"/>
				</xsl:call-template>
			</xsl:if>
		</fo:table-row>
		
		
		<!-- riga reattiva -->
		<fo:table-row keep-with-previous="always">
			<!-- <fo:table-cell xsl:use-attribute-sets="border" display-align="center" padding-left="3pt">
				<xsl:attribute name="background-color"><xsl:value-of select="$title_color"/></xsl:attribute>
				<fo:block text-align="left" xsl:use-attribute-sets="block_sintesi_start">
					reattiva
				</fo:block>
			</fo:table-cell> -->
			<xsl:call-template name="valore_lettura">
				<xsl:with-param name="valore" select="'Reattiva'"/>
				<xsl:with-param name="colore" select="$title_color"/>
			</xsl:call-template>
							
			<xsl:if test="$tipologia='S' or $tipologia='RS' or $tipologia='RAA'">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::DISTRIBUTORE_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::DISTRIBUTORE_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::DISTRIBUTORE_F3"/>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="$tipologia='AA' or $tipologia='RAA'">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::STIMA_PREC_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::STIMA_PREC_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::STIMA_PREC_F3"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::STIMA_ATTUALE_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::STIMA_ATTUALE_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::STIMA_ATTUALE_F3"/>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="$tipologia='RS'">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::STIMA_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::STIMA_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::STIMA_F3"/>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="(./child::LETTURA[@TIPO='ATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='REATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='POTENZA']/child::COSTANTE)">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='REATTIVA']/child::COSTANTE"/>
				</xsl:call-template>
			</xsl:if>
		</fo:table-row>
		
		
		<!-- riga potenza -->
		<fo:table-row keep-with-previous="always">
			<!-- <fo:table-cell xsl:use-attribute-sets="border" display-align="center" padding-left="3pt">
				<xsl:attribute name="background-color"><xsl:value-of select="$title_color"/></xsl:attribute>
				<fo:block text-align="left" xsl:use-attribute-sets="block_sintesi_start">
					potenza
				</fo:block>
			</fo:table-cell> -->
			<xsl:call-template name="valore_lettura">
				<xsl:with-param name="valore" select="'Potenza'"/>
				<xsl:with-param name="colore" select="$title_color"/>
			</xsl:call-template>
							
			<xsl:if test="$tipologia='S' or $tipologia='RS' or $tipologia='RAA'">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::DISTRIBUTORE_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::DISTRIBUTORE_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::DISTRIBUTORE_F3"/>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="$tipologia='AA' or $tipologia='RAA'">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::STIMA_PREC_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::STIMA_PREC_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::STIMA_PREC_F3"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::STIMA_ATTUALE_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::STIMA_ATTUALE_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::STIMA_ATTUALE_F3"/>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="$tipologia='RS'">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::STIMA_F1"/>
				</xsl:call-template>

				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::STIMA_F2"/>
				</xsl:call-template>
				
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::STIMA_F3"/>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="(./child::LETTURA[@TIPO='ATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='REATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='POTENZA']/child::COSTANTE)">
				<xsl:call-template name="valore_lettura">
					<xsl:with-param name="valore" select="./child::LETTURA[@TIPO='POTENZA']/child::COSTANTE"/>
				</xsl:call-template>
			</xsl:if>
		</fo:table-row>
		
		<fo:table-row keep-with-previous="always" height="2mm">
			<fo:table-cell number-columns-spanned="14">
				<xsl:if test="$bordi='NO'">
					<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
				</xsl:if>
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<xsl:if test="$tipologia='RS'">
			<fo:table-row>
				<fo:table-cell number-columns-spanned="14">
					<fo:block xsl:use-attribute-sets="block_sintesi_start">
						*I valori relativi a questa data -
						ultimo giorno del periodo di fatturazione
						- derivano da stime Energit.
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
		
		<xsl:if test="$tipologia='AA' or $tipologia='RAA'">
			<fo:table-row>
				<fo:table-cell number-columns-spanned="14">
					<fo:block xsl:use-attribute-sets="block_sintesi_start">
						*I valori relativi a questa data - ultimo giorno del periodo
						di fatturazione - derivano da una proiezione sulla base
						dell’autolettura da lei comunicata il
						<xsl:value-of select="./child::LETTURA[@TIPO='ATTIVA']/child::DATA_LETTURA_ATTUALE"/>.
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
		
		<xsl:if test="(./child::LETTURA[@TIPO='ATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='REATTIVA']/child::COSTANTE) or (./child::LETTURA[@TIPO='POTENZA']/child::COSTANTE)">
			<fo:table-row>
				<fo:table-cell number-columns-spanned="14">
					<fo:block xsl:use-attribute-sets="block_sintesi_start">
						<xsl:if test="not($tipologia='S')">*</xsl:if>*La Costante K
						del contatore è un coefficiente numerico che, moltiplicato
						per la lettura reale o la stima, consente di ottenere
						il consumo energetico espresso in kWh.
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
	</fo:table-body>
</fo:table>
</xsl:template>



<xsl:template name="valore_lettura">
	<xsl:param name="valore"/>
	<xsl:param name="colore"/>
	<!-- <fo:table-cell display-align="center" border-bottom="0.5pt solid black" border-right="0.5pt solid black" xsl:use-attribute-sets="chr.008">
		<fo:block>
			<xsl:choose>
				<xsl:when test="$valore and not($valore='')">
					<xsl:value-of select="$valore"/>
				</xsl:when>
				<xsl:otherwise>
					-
				</xsl:otherwise>
			</xsl:choose>
		</fo:block>
	</fo:table-cell> -->
	<fo:table-cell display-align="center" xsl:use-attribute-sets="chr.008">
		<fo:table border="0.5pt solid black" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell display-align="center">
						<xsl:if test="not($colore='')">
							<xsl:attribute name="background-color"><xsl:value-of select="$colore"/></xsl:attribute>
						</xsl:if>
						<fo:block>
							<xsl:choose>
								<xsl:when test="$valore and not($valore='')">
									<xsl:value-of select="$valore"/>
								</xsl:when>
								<xsl:otherwise>
									-
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</fo:table-cell>
</xsl:template>




<xsl:template name="Header">
	<xsl:param name="tipo"/>
	<xsl:param name="color1"/>
	<xsl:param name="color2"/>
	<fo:table border="0.5pt solid black" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
	<xsl:attribute name="background-color"><xsl:value-of select="$color1"/></xsl:attribute>
		<fo:table-column column-width="proportional-column-width(33.3)"/>
		<fo:table-column column-width="proportional-column-width(33.4)"/>
		<fo:table-column column-width="proportional-column-width(33.3)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">

			<fo:table-row>
				<xsl:if test="not($tipo='Costante') and not($tipo='Costante2')">
					<xsl:attribute name="background-color"><xsl:value-of select="$color2"/></xsl:attribute>
					<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
				</xsl:if>
				<xsl:if test="$tipo='Costante' or $tipo='Costante2'">
					<xsl:attribute name="border-bottom">0.5pt solid <xsl:value-of select="$color1"/></xsl:attribute>
				</xsl:if>
				<fo:table-cell number-columns-spanned="3" display-align="center">
				<!-- <fo:table-cell number-columns-spanned="3" display-align="center"> -->
					<fo:block>
						<xsl:choose>
							<xsl:when test="$tipo='Reale'">
								<fo:block>
									Reale (Distributore)
								</fo:block>
								<fo:block>
									<xsl:value-of select="./child::LETTURA[@TIPO='ATTIVA']/child::DATA_DISTRIBUTORE"/>
								</fo:block>
							</xsl:when>
							<xsl:when test="$tipo='Stima'">
								<fo:block>
									Stima (Energit)
								</fo:block>
								<fo:block>
									<xsl:value-of select="./child::LETTURA[@TIPO='ATTIVA']/child::DATA_STIMA"/>*
								</fo:block>
							</xsl:when>
							<xsl:when test="$tipo='Autolettura1'">
								<fo:block>
									Stima (Autolettura)
								</fo:block>
								<fo:block>
									<xsl:value-of select="./child::LETTURA[@TIPO='ATTIVA']/child::DATA_LETTURA_PRECEDENTE"/>
								</fo:block>
							</xsl:when>
							<xsl:when test="$tipo='Autolettura2'">
								<fo:block>
									Stima (Autolettura)
								</fo:block>
								<fo:block>
									<xsl:value-of select="./child::LETTURA[@TIPO='ATTIVA']/child::DATA_STIMA"/>*
								</fo:block>
							</xsl:when>
							<xsl:when test="$tipo='Costante'">
								<fo:block>&#160;</fo:block>
								<fo:block>
									Costante*
								</fo:block>
							</xsl:when>
							<xsl:when test="$tipo='Costante2'">
								<fo:block>&#160;</fo:block>
								<fo:block>
									Costante**
								</fo:block>
							</xsl:when>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			 
			<xsl:if test="$tipo='Costante' or $tipo='Costante2'"> 
				<fo:table-row keep-with-previous="always">
					<fo:table-cell number-columns-spanned="3" border-right="0.5pt solid black" display-align="center">
						<fo:block>&#160;</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
			
			<xsl:if test="not($tipo='Costante') and not($tipo='Costante2')">
				<fo:table-row keep-with-previous="always">
					<fo:table-cell border-right="0.5pt solid black" display-align="center">
						<fo:block>
							Fascia 1
						</fo:block>
					</fo:table-cell>
				  
					<fo:table-cell border-right="0.5pt solid black" display-align="center">
						<fo:block>
							Fascia 2
						</fo:block>
					</fo:table-cell>
				  
					<fo:table-cell display-align="center">
						<fo:block>
							Fascia 3
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>

		</fo:table-body>
	</fo:table>
</xsl:template>

</xsl:stylesheet>