<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**                                                    **
**          CONSUMI ENERGIA ULTIMI 12 MESI            **
**                                                    **
********************************************************
-->
<xsl:template match="CONSUMI_ENERGIA_ULTIMI_14_MESI">

	<xsl:param name="bordi"/>
	
	<xsl:if test="./child::CONSUMO">
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="block_sintesi chr.008">
		<fo:table-column column-width="proportional-column-width(100)"/>
		
		<fo:table-header>
			<fo:table-row>
				<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
					</xsl:if>
					
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(16)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-body>
						<fo:table-row keep-with-previous="always" height="3mm">
							<fo:table-cell number-columns-spanned="13">
								<fo:block></fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding-left="3mm" number-columns-spanned="13" xsl:use-attribute-sets="brd.b.000" display-align="center">
								<xsl:attribute name="background-color">#DDDDDD</xsl:attribute>
								<fo:block text-align="start">
									<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">CONSUMI ENERGIA DEGLI ULTIMI 12 MESI</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row keep-with-previous="always" height="1mm">
							<fo:table-cell number-columns-spanned="13">
								<fo:block></fo:block>
							</fo:table-cell>
						</fo:table-row>

						<fo:table-row keep-with-previous="always" height="3mm">
							<fo:table-cell padding-left="3mm">
								<fo:block text-align="start">
									<fo:inline>PERIODO</fo:inline>
								</fo:block>
							</fo:table-cell>
													
							<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.l.ultimi_consumi">
								<fo:block>
									<fo:inline>ENERGIA ATTIVA (kWh)</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.l.ultimi_consumi">
								<fo:block>
									<fo:inline>ENERGIA REATTIVA (kVARh)</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.l.ultimi_consumi">
								<fo:block>
									<fo:inline>POTENZA MAX (kW)</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.l.ultimi_consumi">
								<fo:block>
									<fo:inline>FATTORE DI POTENZA (COSFI)</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row keep-with-previous="always" height="4mm">
							<fo:table-cell>
								<fo:block>
									<fo:inline></fo:inline>
								</fo:block>
							</fo:table-cell>
													
							<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
								<fo:block>
									<fo:inline>Fascia 1</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<fo:inline>Fascia 2</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<fo:inline>Fascia 3</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
								<fo:block>
									<fo:inline>Fascia 1</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<fo:inline>Fascia 2</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<fo:inline>Fascia 3</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
								<fo:block>
									<fo:inline>Fascia 1</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<fo:inline>Fascia 2</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<fo:inline>Fascia 3</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
								<fo:block>
									<fo:inline>Fascia 1</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<fo:inline>Fascia 2</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<fo:inline>Fascia 3</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
					</fo:table-body>
					</fo:table>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row>
				<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
					</xsl:if>
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(16)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>
					<fo:table-column column-width="proportional-column-width(7)"/>			
					<fo:table-body end-indent="0pt" start-indent="0pt">
						<xsl:for-each select="./child::CONSUMO">
							<fo:table-row>
									<fo:table-cell padding-left="3mm">
										<fo:block text-align="start">
											<fo:inline>
												<xsl:value-of select="@DESCRIZIONE_MESE_RIFERIMENTO"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
										
									<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F1']/child::ENERGIA_ATTIVA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F1']/child::ENERGIA_ATTIVA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F2']/child::ENERGIA_ATTIVA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F2']/child::ENERGIA_ATTIVA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_ATTIVA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_ATTIVA"/></fo:inline>
												</xsl:when>
												<xsl:when test="./child::FASCIA/child::ENERGIA_ATTIVA">
													<fo:inline><xsl:value-of select="./child::FASCIA/child::ENERGIA_ATTIVA"/>*<xsl:if test="./ancestor::CONSUMI_ENERGIA_ULTIMI_14_MESI[@STIME_PRESENTI='SI']">*</xsl:if>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F1']/child::ENERGIA_REATTIVA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F1']/child::ENERGIA_REATTIVA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F2']/child::ENERGIA_REATTIVA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F2']/child::ENERGIA_REATTIVA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_REATTIVA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_REATTIVA"/></fo:inline>
												</xsl:when>
												<xsl:when test="./child::FASCIA/child::ENERGIA_REATTIVA">
													<fo:inline><xsl:value-of select="./child::FASCIA/child::ENERGIA_REATTIVA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F1']/child::POTENZA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F1']/child::POTENZA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F2']/child::POTENZA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F2']/child::POTENZA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::POTENZA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F3']/child::POTENZA"/></fo:inline>
												</xsl:when>
												<xsl:when test="./child::FASCIA/child::POTENZA">
													<fo:inline><xsl:value-of select="./child::FASCIA/child::POTENZA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F1']/child::FATTORE_DI_POTENZA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F1']/child::FATTORE_DI_POTENZA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F2']/child::FATTORE_DI_POTENZA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F2']/child::FATTORE_DI_POTENZA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:choose>
												<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::FATTORE_DI_POTENZA">
													<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F3']/child::FATTORE_DI_POTENZA"/></fo:inline>
												</xsl:when>
												<xsl:when test="./child::FASCIA/child::FATTORE_DI_POTENZA">
													<fo:inline><xsl:value-of select="./child::FASCIA/child::FATTORE_DI_POTENZA"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>-</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
							</fo:table-row>
						</xsl:for-each>
						
						<xsl:variable name='mono'>
							<xsl:for-each select="./child::CONSUMO">
								<xsl:choose>
									<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_REATTIVA"></xsl:when>
									<xsl:when test="./child::FASCIA/child::ENERGIA_REATTIVA">SI</xsl:when>
									<xsl:otherwise></xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</xsl:variable>
						
						<!-- <xsl:if test="$bordi='NO' or @STIME_PRESENTI='SI' or substring($mono,1,2)='SI'"> -->
							<fo:table-row keep-with-previous="always" height="2mm">
								<fo:table-cell number-columns-spanned="13" xsl:use-attribute-sets="brd.b.000">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
						<!-- </xsl:if> -->
						
						<fo:table-row text-align="start" xsl:use-attribute-sets="font_titolo_tabella_bold">
							<fo:table-cell>
								<fo:block>
									CONSUMO ANNUO
								</fo:block>
							</fo:table-cell>
								
							<fo:table-cell number-columns-spanned="2">
								<fo:block>
									<xsl:value-of select="@SOMMA_F1"/> KWh (F1)
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="2">
								<fo:block>
									<xsl:value-of select="@SOMMA_F2"/> KWh (F2)
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="2">
								<fo:block>
									<xsl:value-of select="@SOMMA_F3"/> KWh (F3)
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="6">
								<fo:block>
									<xsl:value-of select="@SOMMA_MONO"/> KWh (MONORARIO)
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<xsl:if test="@STIME_PRESENTI='SI'">
							<fo:table-row keep-with-previous="always">
								<fo:table-cell number-columns-spanned="13">	
									<fo:block text-align="start">* I consumi relativi a questo mese sono stati stimati</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:if>
						
						<xsl:if test="substring($mono,1,2)='SI'">
							<fo:table-row keep-with-previous="always">
								<fo:table-cell number-columns-spanned="13">	
									<fo:block text-align="start">*<xsl:if test="@STIME_PRESENTI='SI'">*</xsl:if> Consumo monorario</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:if>
					</fo:table-body>
					</fo:table>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row height="2mm" keep-with-previous="always">
				<fo:table-cell display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
					</xsl:if>
					<fo:block>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
		</fo:table>
	</xsl:if>
	
</xsl:template>

</xsl:stylesheet>