<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**                                                    **
**    Template per specchietto consumi e conguagli    **
**                                                    **
********************************************************
-->
<xsl:template match="LETTURE_CONSUMI_CONGUAGLI">
<xsl:param name="bordi"/>
<xsl:param name="color"/>
<xsl:param name="svg-titoli"/>
<xsl:param name="tipo"/>
<xsl:param name="mese_fattura"/>

<xsl:if test="$tipo='header'">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" text-align="start">
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(10)"/>
	<fo:table-column column-width="proportional-column-width(23)"/>
	<fo:table-column column-width="proportional-column-width(23)"/>
	<fo:table-column column-width="proportional-column-width(24)"/>
	<fo:table-body>
		<fo:table-row keep-with-previous="always" height="2mm">
			<fo:table-cell number-columns-spanned="5">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row height="6mm">
			<fo:table-cell padding-left="3mm" number-columns-spanned="5" display-align="center">
				<xsl:attribute name="background-image"><xsl:value-of select="$svg-titoli"/></xsl:attribute>
				<fo:block text-align="start">
					<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">CONSUMI FATTURATI E RICALCOLI </fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row keep-with-previous="always" height="1mm">
			<fo:table-cell number-columns-spanned="5">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row keep-with-previous="always" height="3mm">
			<fo:table-cell padding-left="3mm">
				<fo:block>
					<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">PERIODO</fo:inline>
				</fo:block>
			</fo:table-cell>
									
			<fo:table-cell>
				<fo:block>
					<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">FASCIA</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">TIPOLOGIA</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">CONSUMO FATTURATO (kWh)*</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">CONSUMO RICALCOLATO (kWh)**</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:if>

<xsl:if test="$tipo='body'">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" text-align="start">
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(10)"/>
	<fo:table-column column-width="proportional-column-width(23)"/>
	<fo:table-column column-width="proportional-column-width(23)"/>
	<fo:table-column column-width="proportional-column-width(24)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<xsl:for-each select="./child::CONSUMO">
			<fo:table-row>
				<fo:table-cell padding-left="3mm" text-align="left">
					<fo:block>
						<fo:inline>
							<xsl:value-of select="@DESCRIZIONE_MESE_RIFERIMENTO"/>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell text-align="left">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::FASCIA">
								<fo:inline>
									<xsl:value-of select="./child::FASCIA"/>
								</fo:inline>
							</xsl:when>
							<xsl:otherwise>
								<fo:inline>-</fo:inline>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell text-align="left">
					<fo:block>
						<fo:inline>
							<xsl:choose>
								<xsl:when test="./child::TIPOLOGIA='CONGUAGLIO'">
									Ricalcolo
								</xsl:when>
								<xsl:when test="./child::TIPOLOGIA='CONSUMO'">
									Misure Periodiche
								</xsl:when>								
								<xsl:otherwise>
									<xsl:value-of select="./child::TIPOLOGIA"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell text-align="left" padding-right="10mm">
					<fo:block>
						<xsl:value-of select="./child::CONSUMO"/>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell text-align="left" padding-right="10mm">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::CONGUAGLIO='0'">
								<fo:inline>-</fo:inline>
							</xsl:when>
							<xsl:otherwise>
								<fo:inline>
									<xsl:if test="number(./child::CONGUAGLIO)>0">+</xsl:if><xsl:value-of select="./child::CONGUAGLIO"/>	
								</fo:inline>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:for-each>
		
		
		<fo:table-row keep-with-previous="always" height="2mm">
			<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="brd.b.000">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		
		<fo:table-row keep-with-previous="always">
			<fo:table-cell number-columns-spanned="5">
				<fo:block>					
					<fo:inline>*Rappresenta i consumi fatturati nella presente bolletta distinti per periodo di riferimento e per fascia oraria.</fo:inline>
				</fo:block>
				<fo:block>
					<fo:inline>**Rappresenta la variazione tra i consumi fatturati nella presente bolletta e quelli precedentemente fatturati distinti per periodo di riferimento e per fascia oraria.</fo:inline>
				</fo:block>					

			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:if>
</xsl:template>

</xsl:stylesheet>