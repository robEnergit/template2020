<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:barcode="http://barcode4j.krysalis.org/ns">

<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**            Inclusione templates esterni            **
********************************************************
-->
<xsl:include href="attributi.xsl"/>
<xsl:include href="Template_Energia_Multisito.xsl"/>
<xsl:include href="202_09/Template_Energia_Multisito_012011.xsl"/>
<xsl:include href="Finale.xsl"/>
<xsl:include href="202_09/Finale_012011.xsl"/>
<xsl:include href="bollettino.xsl"/>
<xsl:include href="condizioni.xsl"/>
<xsl:include href="condizioni_c11.xsl"/>
<xsl:include href="scheda_riepilogo.xsl"/>
<xsl:include href="scheda_riepilogo_c11.xsl"/>
<xsl:include href="comunicazioni_condizioni.xsl"/>
<xsl:include href="comunicazioni_condizioni_c11.xsl"/>
<xsl:include href="Autocertificazione.xsl"/>
<xsl:include href="Informativa_Qualita.xsl"/>

<!--
**********************************************************
**				Template LOTTO_FATTURE					**
**********************************************************
-->
<xsl:template match="LOTTO_FATTURE">

<fo:root>

<fo:layout-master-set>
    <!--
    ********************************************************
    **           Definizione pagina fattura               **
    ********************************************************
    -->
    <fo:simple-page-master margin-bottom="10mm" margin-left="11mm" margin-right="11mm" margin-top="5mm" master-name="pm0" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="17mm" margin-top="12mm" overflow="visible" region-name="body"/>
        <fo:region-before extent="12mm" overflow="visible" region-name="header"/>
        <fo:region-after display-align="after" extent="17mm" overflow="visible" region-name="footer"/>
    </fo:simple-page-master>
	
	<fo:simple-page-master margin-bottom="10mm" margin-left="11mm" margin-right="11mm" margin-top="5mm" master-name="pm0first" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="17mm" margin-top="12mm" overflow="visible" region-name="body"/>
        <fo:region-before extent="12mm" overflow="visible" region-name="headernew"/>
        <fo:region-after display-align="after" extent="17mm" overflow="visible" region-name="footer"/>
    </fo:simple-page-master>
	
    <fo:simple-page-master margin-bottom="0mm" margin-left="0mm" margin-right="0mm" margin-top="0mm" master-name="pm0-blank" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="0mm" margin-top="0mm" overflow="visible" region-name="body"/>
    </fo:simple-page-master>
    
    <!--
    ********************************************************
    **          Definizione pagina bollettino             **
    ********************************************************
    -->
    <fo:simple-page-master master-name="pm1"
		page-height="210mm" page-width="297mm" margin="0mm">
		<fo:region-body margin-top="108mm" />
		<fo:region-before extent="102mm" overflow="hidden" />
	</fo:simple-page-master>
    
    <!--
    *****************************************************************************
    **    Definizione pagine allegati condizioni contrattuali - comparativa    **
    *****************************************************************************
    -->
	<fo:simple-page-master margin-bottom="5mm" margin-left="10mm" margin-right="10mm" margin-top="10mm" master-name="pm0_condizioni" page-height="297mm" page-width="210mm">
		<fo:region-body margin-bottom="0mm" margin-top="5mm" overflow="visible" region-name="body" />
		<fo:region-before extent="10mm" overflow="visible" region-name="header_condizioni" />
		<fo:region-after display-align="after" extent="10mm" overflow="visible" region-name="footer_condizioni" />
	</fo:simple-page-master>

	<fo:simple-page-master margin-bottom="10mm" margin-left="10mm" margin-right="10mm" margin-top="10mm" master-name="pm0_scheda_riepilogo" page-height="297mm" page-width="210mm">
		<fo:region-body margin-bottom="0mm" margin-top="0mm" overflow="visible" region-name="body" />
		<fo:region-after display-align="after" extent="10mm" overflow="visible" region-name="footer_riepilogo" />
	</fo:simple-page-master>

	<fo:simple-page-master margin-top="5mm" margin-bottom="10mm" master-name="pm0_comunicazione_condizioni" page-height="297mm" page-width="210mm">
		<fo:region-body margin-top="3cm" margin-bottom="25mm" margin-left="29mm" margin-right="29mm" overflow="visible" region-name="body" />
		<fo:region-before extent="3cm" overflow="visible" region-name="header" />
		<fo:region-after display-align="after" extent="25mm" overflow="visible" region-name="footer" />
	</fo:simple-page-master>
    
	<fo:page-sequence-master master-name="document">
      <fo:repeatable-page-master-alternatives>
        <fo:conditional-page-master-reference
          master-reference="pm0" page-position="rest" blank-or-not-blank="not-blank"/>
        <fo:conditional-page-master-reference
          master-reference="pm0first" page-position="first"  blank-or-not-blank="not-blank"/>
		<fo:conditional-page-master-reference
          master-reference="pm0-blank" page-position="any" blank-or-not-blank="blank"/>
      </fo:repeatable-page-master-alternatives>
    </fo:page-sequence-master>
	
</fo:layout-master-set>

<xsl:apply-templates select="DOCUMENTO"/>

</fo:root>
</xsl:template>





<!--
**********************************************************
**					Template DOCUMENTO					**
**********************************************************
-->
<xsl:template match="DOCUMENTO">

<xsl:variable name="bordi">NO</xsl:variable><!--rectangle_table-->
<xsl:variable name="rectangle_frontespizio">svg/rectangle_frontespizio</xsl:variable>
<xsl:variable name="color">black</xsl:variable><!--#f08c02-->
<xsl:variable name="color_riquadro_scadenza">black</xsl:variable><!--black-->
<xsl:variable name="color_titolo_dettaglio">black</xsl:variable><!--black-->
<xsl:variable name="sfondo_titoli">#DDDDDD</xsl:variable><!--#DDDDDD-->
<xsl:variable name="color-sezioni">black</xsl:variable><!--#004a76-->
<xsl:variable name="color-sottosezioni">black</xsl:variable><!--#004a76-->
<xsl:variable name="color-default">#000000</xsl:variable>
<xsl:variable name="svg-sezioni">'url(svg/rectangle_titolo_sezioni_short.svg)'</xsl:variable>
<xsl:variable name="svg-sottosezioni">'url(svg/rectangle_titolo_sezioni_short_blank.svg)'</xsl:variable>
<xsl:variable name="svg-altre-sezioni">'url(svg/rectangle_titolo_sezioni_short.svg)'</xsl:variable>
<xsl:variable name="svg-dettaglio">'url(svg/rectangle_dettaglio.svg)'</xsl:variable>
<xsl:variable name="autocertificazione">NO</xsl:variable>
<xsl:variable name="qualita">SI</xsl:variable>

<xsl:variable name="status_piu_uno"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:variable>
<xsl:variable name="data_doc_number"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2),substring(DATA_DOCUMENTO,1,2))"/></xsl:variable>

<fo:page-sequence initial-page-number="1" master-reference="document" orphans="1" white-space-collapse="true" widows="1" id="F">
    <!--
    ********************************************************
    **                     Header                         **
    ********************************************************
    -->
	<fo:static-content flow-name="headernew">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
            <fo:table-column column-width="proportional-column-width(100)"/>
            <fo:table-body end-indent="0pt" start-indent="0pt">
                <fo:table-row>
                    <fo:table-cell display-align="center" xsl:use-attribute-sets="blk.header">
						<fo:block margin-left="150mm" margin-top="5mm">
							<fo:external-graphic src="url(img/logoGasNaturalexFattura.bmp)" content-width="37mm"  />
						</fo:block>
                        <fo:block text-align="end">
							<xsl:if test="$status_piu_uno &lt; 201307">
								<xsl:if test="@SPOT='NO' or not(@SPOT)">
									<fo:inline font-family="universbold" font-size="11pt">Codice Cliente <xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
								</xsl:if>
							</xsl:if>
                        </fo:block>
                        <fo:block text-align="end">   
							<xsl:if test="$status_piu_uno &lt; 201307">
								<xsl:if test="@SPOT='NO' or not(@SPOT)">
									<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Onda</fo:inline>
								</xsl:if>
							</xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </fo:static-content>
	
    <fo:static-content flow-name="header">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
            <fo:table-column column-width="proportional-column-width(100)"/>
            <fo:table-body end-indent="0pt" start-indent="0pt">
                <fo:table-row>
                    <fo:table-cell display-align="center" xsl:use-attribute-sets="blk.header">
						<fo:block text-align="end">
							<xsl:if test="@SPOT='NO' or not(@SPOT)">
								<fo:inline font-family="universbold" font-size="11pt">Codice Cliente <xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
							</xsl:if>
                        </fo:block>
                        <fo:block text-align="end">   
							<xsl:if test="@SPOT='NO' or not(@SPOT)">
								<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Onda</fo:inline>
							</xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </fo:static-content>

    <!--
    ********************************************************
    **                     Footer                         **
    ********************************************************
    -->
    <fo:static-content flow-name="footer">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
			<fo:table-column column-width="proportional-column-width(85)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell display-align="center">
						<fo:block xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer2">
								<xsl:choose>
									<xsl:when test="@TIPO_DOCUMENTO='FATTURA'">Fattura: </xsl:when>
									<xsl:otherwise>Nota di credito: </xsl:otherwise>
								</xsl:choose> 
								<xsl:value-of select="@NUMERO_DOCUMENTO"/>  del  <xsl:value-of select="./child::DATA_DOCUMENTO" />
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell display-align="center">
						<fo:block text-align="end" xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer2">
								<fo:inline xsl:use-attribute-sets="font_footer2">Pagina </fo:inline>
								<fo:inline text-align="end" xsl:use-attribute-sets="font_footer2"><fo:page-number/>/<fo:page-number-citation ref-id="{generate-id(.)}"/></fo:inline>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row height="2mm">
					<fo:table-cell number-columns-spanned="2" display-align="center" border-bottom="0.5 dashed thick black">
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row height="2mm">
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" display-align="center">
						<fo:block text-align="center" xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer">Onda S.r.l. - Sede legale Via Gian Giacomo Porro, 8 00193 Roma - Servizio Clienti 800.58.22.73 - Fax 0931/209824</fo:inline>
						</fo:block>
						<fo:block text-align="center" xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer">Registro Imprese di Roma n. 07491751009 - Capitale sociale: euro 1.020.000,00 interamente versato - R.E.A. 1035988 - Partita Iva 07491751009</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
        </fo:table>
    </fo:static-content>

    <!--
    ********************************************************
    **                  Inizio body                       **
    ********************************************************
    -->
    <fo:flow flow-name="body">
		<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
		
		<!--
		********************************************************
		**               Logo E Data Matrix                   **
		********************************************************
		-->
		<fo:block-container position="absolute"
							top="-3mm"
							left="2mm"
							width="90mm"
							height="22mm">
			<fo:block>
				<fo:table table-layout="fixed" width="100%">
				<fo:table-column column-width="proportional-column-width(22)"/>
				<fo:table-column column-width="proportional-column-width(53)"/>
				<fo:table-column column-width="proportional-column-width(25)"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell display-align="center">
								<xsl:choose>
									<xsl:when test="$color='black'">
										<fo:block>
											<fo:external-graphic src="url(img/logo_onda_bn.jpg)" content-width="23mm" />
										</fo:block>										
									</xsl:when>
									<xsl:otherwise>
										<fo:block>
											<fo:external-graphic src="url(img/logo_onda.JPG)" content-width="20mm" />
										</fo:block>
									</xsl:otherwise>
								</xsl:choose>
							</fo:table-cell>
							
							<fo:table-cell start-indent="2mm" font-family="Arial" font-weight="bold" font-size="7pt" color="black" display-align="center">
								<fo:block>
									Direzione Generale
								</fo:block>
								<fo:block>
									Via Savoia, 38
								</fo:block>
								<fo:block>
									96100 Siracusa (SR)
								</fo:block>
							</fo:table-cell>
							
							<xsl:variable name="barcode_message_header">
								<xsl:value-of select="concat(
								'F_S', ' ', 
								DATA_DOCUMENTO, ' ',
								@NUMERO_DOCUMENTO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CODICE_CLIENTE, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/INDIRIZZO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CAP, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CITTA, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/PROVINCIA)"/>
							</xsl:variable>
							
							<fo:table-cell text-align="right" display-align="center"
									padding-right="17mm">
								<fo:block>
									<fo:instream-foreign-object content-height="12mm" content-width="25mm">
										<barcode:barcode message="{$barcode_message_header}">
											<barcode:datamatrix>
												<barcode:quiet-zone enabled="false">0mm</barcode:quiet-zone>
												<barcode:module-width>0.7mm</barcode:module-width>
												<xsl:if test="string-length($barcode_message_header) &lt; 48">
													<barcode:shape>force-rectangle</barcode:shape>
												</xsl:if>
											</barcode:datamatrix>
										</barcode:barcode>
									</fo:instream-foreign-object>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:block-container>
		
		
		<xsl:if test="$status_piu_uno &gt; 201306 and @SPOT='NO'">
			<fo:block-container position="absolute"
								top="4mm"
								left="109.3mm"
								width="79mm"
								height="16mm"
								background-image="./svg/bordo_codice_cliente.svg"
								background-repeat="no-repeat"
								display-align="center"
								text-align="center"
								font-family="universcbold"
								font-size="14pt">
				<fo:block>
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						Codice Cliente <fo:inline font-family="universcbold" font-size="18pt"><xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
					</xsl:if>
				</fo:block>
				<fo:block>   
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Onda</fo:inline>
					</xsl:if>
				</fo:block>
			</fo:block-container>
		</xsl:if>
		
	
		<!--
		*********************************************************
		**  Dati anagrafici e indirizzo di spedizione fattura  **
		*********************************************************
		-->
		<fo:block-container position="absolute"
							top="45.17mm"
							left="93mm"
							width="97mm"
							height="26mm">
			<xsl:call-template name="SPEDIZIONE_FATTURA" />
		</fo:block-container>
		
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" space-before="40mm">
			<fo:table-column column-width="proportional-column-width(34)"/>
			<fo:table-column column-width="proportional-column-width(66)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell display-align="center" xsl:use-attribute-sets="pad.l.000">
						<xsl:call-template name="DATI_ANAGRAFICI" />
					</fo:table-cell>

					<fo:table-cell padding-left="31.7mm" padding-top="19mm"
						border-left="0.5 dashed thick black">
						<fo:block />
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<!--
		**********************************************
		**  Riquadro fattura con bordi arrotondati  **
		**********************************************
		-->
		<fo:table  space-before="4mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row height="23mm">
					<fo:table-cell display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(<xsl:value-of select="$rectangle_frontespizio"/>.svg)</xsl:attribute>
							<xsl:call-template name="RIQUADRO_SCADENZA_FATTURA">
								<xsl:with-param name="color" select="$color_riquadro_scadenza"/>
							</xsl:call-template>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<!--
		*********************************************************
		**  Modalita' di pagamento e riepilogo importi fattura **
		*********************************************************
		-->
		<fo:table  space-before="4mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(34)"/>
			<fo:table-column column-width="proportional-column-width(66)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row height="40mm">        
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
						<fo:block>
							<xsl:call-template name="PAGAMENTI"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" border-left="0.5 dashed thick black">    
						<fo:block>
							<xsl:call-template name="DETTAGLIO_FRONTESPIZIO"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
        </fo:table>
		
		<!--
		**********************************************************
		**					Avvisi e riquadri					**
		**********************************************************
		-->
		<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row height="80mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
						<xsl:if test="$bordi='SI'">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/rectangle_frontespizio_2.svg)</xsl:attribute>
						</xsl:if>
						<!--
						*****************************
						**   Avvisi frontespizio   **
						*****************************
						-->
						<xsl:call-template name="AVVISI_FRONTESPIZIO"/>
						
						<!--
						*************************************************************************
						**  - Riquadro lettura contatore - autolettura                         **
						**  - Segnalazione guasti                                              **
						**  - Informazioni contatti e reclami                                  **
						*************************************************************************
						-->
						<xsl:if test="./child::CONTRATTO_GAS">
							<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(39)"/>
							<fo:table-column column-width="proportional-column-width(22)"/>
							<fo:table-column column-width="proportional-column-width(39)"/>
								<fo:table-body end-indent="0pt" start-indent="0pt">    
									<fo:table-row>
										<!--
										***********************************************
										**  Riquadro lettura contatore - autolettura **
										***********************************************
										-->
										<fo:table-cell xsl:use-attribute-sets="pad.r.000">
											<fo:block>
												<xsl:call-template name="LETTURA_AUTOLETTURA_FRONTESPIZIO"/>
											</fo:block>
										</fo:table-cell>
										<!--
										***************************
										**  Segnalazione guasti  **
										***************************
										-->
										<fo:table-cell xsl:use-attribute-sets="pad.lr.000" border-left="0.5 dashed thick black" border-right="0.5 dashed thick black">
											<fo:block>
												<xsl:call-template name="SEGNALAZIONE_GUASTI"/>
											</fo:block>
										</fo:table-cell>
										<!--
										***************************************
										**  Informazioni contatti e reclami  **
										***************************************
										-->
										<fo:table-cell xsl:use-attribute-sets="pad.l.000">
											<fo:block>
												<xsl:call-template name="CONTATTI_RECLAMI">
													<xsl:with-param name="color" select="$color"/>
												</xsl:call-template>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>    
								</fo:table-body>
							</fo:table>
						</xsl:if>
						<xsl:if test="not(./child::CONTRATTO_GAS)">
							<!--
							***************************************
							**  Informazioni contatti e reclami  **
							***************************************
							-->
							<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(25)"/>
							<fo:table-column column-width="proportional-column-width(50)"/>
							<fo:table-column column-width="proportional-column-width(25)"/>
								<fo:table-body end-indent="0pt" start-indent="0pt">    
									<fo:table-row>
										<fo:table-cell>
											<fo:block/>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:call-template name="CONTATTI_RECLAMI">
													<xsl:with-param name="color" select="$color"/>
												</xsl:call-template>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block/>
										</fo:table-cell>
									</fo:table-row> 
								</fo:table-body>
							</fo:table>
						</xsl:if>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		
		<!--
		******************************
		**  Frase imposta di bollo  **
		******************************
		-->
		<fo:block space-before="2mm" xsl:use-attribute-sets="blk.000" start-indent="3mm">
			<fo:inline xsl:use-attribute-sets="chr.006">L'imposta di bollo, se dovuta, viene assolta in modo virtuale.</fo:inline>
		</fo:block>
		
		<fo:block break-before="page">
		</fo:block>
		
		<xsl:if test="@SPOT='SI'">
			<xsl:call-template name="TEMPLATE_RIEPILOGO_FATTURA"/>
		</xsl:if>
			
		<xsl:if test="@SPOT='NO' or not(@SPOT)">
			
			<!--
			**********************************************************************
			**	Sintesi e dettaglio dei contratti energia presenti in fattura	**
			**********************************************************************
			-->
			<xsl:if test="./child::CONTRATTO_GAS">
				<xsl:choose>
					<xsl:when test="$status_piu_uno &lt; 201101 or substring(@COMPETENZA,8,4) &lt; 2011">
						<xsl:call-template name="CONTRATTI_MULTISITO">
							<xsl:with-param name="bordi" select="$bordi"/>
							<xsl:with-param name="color" select="$color"/>
							<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
							<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
							<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
							<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
							<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
							<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
							<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="CONTRATTI_MULTISITO_012011">
							<xsl:with-param name="bordi" select="$bordi"/>
							<xsl:with-param name="color" select="$color"/>
							<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
							<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
							<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
							<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
							<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
							<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
							<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			
			<!--
			**********************
			**	Altri contratti	**
			**********************
			-->
			<xsl:apply-templates select="CONTRATTO_DEPOSITO_CAUZIONALE">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
				
			<xsl:apply-templates select="CONTRATTO_FONIA">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="CONTRATTO_SERVIZI_VARI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="CONTRATTO_AREASERVER">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
			
			<!--
			******************************************************************************
			**		Riepilogo fattura e altre sezioni									**
			**		(comunicazioni ai clienti, fatture in attesa pagamento, etc)		**
			******************************************************************************
			-->
			<xsl:choose>
				<xsl:when test="$status_piu_uno &lt; 201101">
					<xsl:call-template name="FINALE">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="FINALE_012011">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			
			<!--
			**********************************
			**  Iinformativa sulla qualità  **
			**********************************
			-->
			<xsl:if test="./child::CONTRATTO_GAS and $qualita='SI'">
				<xsl:call-template name="INFORMATIVA_QUALITA"/>
			</xsl:if>
		</xsl:if>
		
		<!--
		*********************************************************
		**  Blocco per la determinazione del numero di pagine  **
		*********************************************************
		-->
		<fo:block id="{generate-id(.)}"/>
		
    </fo:flow>
</fo:page-sequence>    


<!--
**********************************************************
**					Rinnovi se presenti					**
**********************************************************
-->
<xsl:for-each select="./child::RINNOVI/child::RINNOVO[@VISUALIZZA='SI']">
    <fo:page-sequence master-reference="pm0-blank">
        <fo:flow flow-name="body">
					<fo:block-container position="absolute"
										top="3.75pt"
										left="-0.75pt"
										width="210mm"
										height="297mm"
										display-align="after">
						<fo:block text-align="center">
							<fo:external-graphic>
								<xsl:attribute name="height">297mm</xsl:attribute>
								<xsl:attribute name="content-height">297mm</xsl:attribute>
								<xsl:attribute name="content-width">210mm</xsl:attribute>
								<xsl:if test="./child::ALLEGATO='AL-FAEN0110FX'">
									<xsl:attribute name="src">./allegati_rinnovi/<xsl:value-of select="./child::ALLEGATO"/>-RN.pdf</xsl:attribute>
								</xsl:if>
								<xsl:if test="not(./child::ALLEGATO='AL-FAEN0110FX')">
									<xsl:attribute name="src">./allegati_rinnovi/<xsl:value-of select="./child::ALLEGATO"/>.pdf</xsl:attribute>
								</xsl:if>
							</fo:external-graphic>
						</fo:block>
					</fo:block-container>
        </fo:flow>
    </fo:page-sequence>
</xsl:for-each>

<!--
**********************************************************
**			  Nuove condizioni contrattuali				**
**********************************************************
-->
<xsl:if test="./child::CONTRATTO_GAS[@NUOVE_CONDIZIONI='B1']">
    <!--Comunicazione nuove condizioni contrattuali business-->
    <xsl:call-template name="COMUNICAZIONE_CONDIZIONI"/>
    <!--Comparativa business-->
    <xsl:call-template name="SCHEDA_RIEPILOGO"/>
    <!--Nuove condizioni contrattuali B1-->
    <xsl:call-template name="CONDIZIONI"/>
</xsl:if>

<xsl:if test="./child::CONTRATTO_GAS[@NUOVE_CONDIZIONI='C11']">
    <!--Comunicazione nuove condizioni contrattuali consumer-->
    <xsl:call-template name="COMUNICAZIONE_CONDIZIONI_C11"/>
    <!--Comparativa consumer-->
	<xsl:call-template name="SCHEDA_RIEPILOGO_C11"/>
    <!--Nuove condizioni contrattuali C11-->
    <xsl:call-template name="CONDIZIONI_C11"/>
</xsl:if>

<!--
**********************************************************
**					Autocertificazione					**
**********************************************************
-->
<xsl:if test="CONTRATTO_GAS and $autocertificazione='SI' and (@SPOT='NO' or not(@SPOT))">
	<xsl:call-template name="AUTOCERTIFICAZIONE"/>
</xsl:if>

<!--
********************************************************
**             Accoda eventuali bollettini            **
********************************************************
-->
<xsl:if test="./child::BOLLETTINO and (@SPOT='NO' or not(@SPOT))">
	<xsl:call-template name="BOLLETTINO"/>
</xsl:if>

</xsl:template>





<!--
***************************************
**  Informazioni contatti e reclami  **
***************************************
-->
<xsl:template name="CONTATTI_RECLAMI">
	<xsl:param name="color"/>
	<fo:table text-align="center" font-family="universc" font-size="8pt" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
	<fo:table-column column-width="proportional-column-width(100)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="universcbold" font-size="8pt">
					INFORMAZIONI - CONTATTI - RECLAMI
				</fo:block>
				
				<fo:block space-before="1mm">Servizio Clienti 800.58.22.73</fo:block>
				<fo:block>Gratuito da rete fissa</fo:block>
				<fo:block>Attivo dal lunedi' al venerdi' dalle 9.00 alle 18.00</fo:block>
				
				<fo:block space-before="1mm" border-bottom="0.5 dashed thick black"/>
				
				<fo:block space-before="1mm">Da cellulare comporre il numero 389.242.8118</fo:block>
				<fo:block>I costi della chiamata dipendono dal proprio operatore telefonico</fo:block>
				
				<fo:block space-before="1mm" border-bottom="0.5 dashed thick black"/>
				
				<fo:block space-before="1mm">Fax Servizio Clienti 0931.209824</fo:block>
				
				<fo:block space-before="1mm" border-bottom="0.5 dashed thick black"/>
				
				<fo:block space-before="1mm">www.ondaenergia.it</fo:block>
				<fo:block>customercare@ondaenergia.it</fo:block>
				
				<fo:block space-before="1mm" border-bottom="0.5 dashed thick black"/>
				
				<fo:block space-before="1mm">Invio Reclami: Onda S.r.l. - Via Savoia, 38 – 96100 Siracusa</fo:block>
				<fo:block>Sportello Clienti: negozio Onda Siracusa – Via Foro Siracusano, 18 telefono 0931.1814158</fo:block>
				<fo:block>Sportello Clienti: negozio Onda Avola – Corso Garibaldi, 75 telefono 0931.1810177</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:template>




<!--
***********************************************
**  Riquadro lettura contatore - autolettura **
***********************************************
-->
<xsl:template name="LETTURA_AUTOLETTURA_FRONTESPIZIO">
	<fo:block xsl:use-attribute-sets="blocco_riquadri_frontespizio">
		<xsl:choose>
			<xsl:when test="./child::AUTOLETTURA">
				<fo:block text-align="center" xsl:use-attribute-sets="font_titolo_tabella_bold">
					AUTOLETTURA
				</fo:block>
				
				<fo:block>Gentile Cliente, può comunicare le letture rilevate dal suo misuratore al numero verde Onda 800-582273 (dalle ore 9 alle 18 dal lunedì al venerdì) dal
				1 al 25 di ogni mese. Comunicazioni pervenute oltre il 25 saranno scartate in fase di acquisizione a sistema. Se i suoi consumi sono registrati su 3
				fasce orarie è necessario comunicare i 3 valori indicati sul suo contatore con A1,A2,A3.</fo:block>
			</xsl:when>
		
			<xsl:otherwise>
				<fo:block text-align="center" xsl:use-attribute-sets="font_titolo_tabella_bold">
					LETTURA DEL CONTATORE
				</fo:block>
				
				<fo:block>La sua utenza e' inserita nel sistema di telelettura attraverso il contatore elettronico. Addebiteremo i suoi consumi effettivi ogni volta che saranno resi disponibili dal suo Distributore Locale.</fo:block>
			</xsl:otherwise>
		</xsl:choose>
	</fo:block>
</xsl:template>





<!--
***************************
**  Segnalazione guasti  **
***************************
-->
<xsl:template name="SEGNALAZIONE_GUASTI">
	<fo:block xsl:use-attribute-sets="blocco_riquadri_frontespizio">
		<fo:block text-align="center" xsl:use-attribute-sets="font_titolo_tabella_bold">
			SEGNALAZIONE GUASTI
		</fo:block>

		<fo:block>In caso di guasti dovra' contattare il Distributore Locale al numero telefonico <xsl:value-of select="./child::CONTRATTO_GAS/child::CONTATTO_DISTRIBUTORE" />.</fo:block>
	</fo:block>
</xsl:template>





<!--
******************************
**  Modalita' di pagamento  **
******************************
-->
<xsl:template name="PAGAMENTI">
	<fo:block xsl:use-attribute-sets="chr.008">
		<fo:block xsl:use-attribute-sets="blk.003 chrbold.008">
			PAGAMENTI
		</fo:block>
		
		<xsl:choose>
			<xsl:when test="@TIPO_DOCUMENTO='FATTURA' and not(./child::IMPORTO_DOCUMENTO='0,00')">
				<xsl:choose>
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RID_ERRATO']">
						<fo:block xsl:use-attribute-sets="chrbold.008">Modalita' di pagamento</fo:block>
					</xsl:when>
					
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RID_PE_REQ']">
						<fo:block xsl:use-attribute-sets="chrbold.008">La modalita' di pagamento per questa fattura e'</fo:block>
					</xsl:when>
					
					<xsl:otherwise>
						<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'La fattura risulta pagata')">
							<fo:block xsl:use-attribute-sets="chrbold.008">Lei ha scelto di pagare con</fo:block>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				
				<xsl:choose>
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RID_PE_REQ']">
						<fo:block>Bollettino Postale</fo:block>					
						<fo:block space-before="2mm" xsl:use-attribute-sets="chrbold.008">La sua richiesta di attivazione della modalita' RID e' in attesa di conferma presso la sua banca. Per il pagamento di questa fattura dovra' utilizzare il bollettino postale che trova nell'ultimo foglio. Grazie.</fo:block>
						<fo:block space-before="2mm" xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CAUSALE" /></fo:block>
					</xsl:when>
					
					<xsl:otherwise>
						<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO" /></fo:block>
				
						<xsl:choose>
							<xsl:when test="./child::MODALITA_PAGAMENTO_DOCUMENTO[@COMUNICAZIONE='BOA']">
								<fo:block>Aet Italia S.r.l. (mandatario all’incasso)</fo:block>
							</xsl:when>
							
							<xsl:when test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bonifico Bancario'">
								<fo:block>Onda S.r.l.</fo:block>
							</xsl:when>
							
							<xsl:otherwise>
								<fo:block/>
							</xsl:otherwise>
						</xsl:choose>
						
						<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::INTESTATARIO_PAGAMENTO" /></fo:block>
						
						<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bollettino Postale'">
							<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE" /></fo:block>
						</xsl:if>
						
						<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::IBAN">
							<fo:block xsl:use-attribute-sets="chr.009">IBAN: <xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::IBAN" /></fo:block>
						</xsl:if>
						
						<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE = 'Sardex'">
							<fo:block xsl:use-attribute-sets="chr.009">Sardex</fo:block>
						</xsl:if>
						
						<fo:block xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CAUSALE" /></fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="./child::IMPORTO_DOCUMENTO='0,00'">
						<fo:block xsl:use-attribute-sets="chr.pagamenti">Non c'e' niente da pagare!</fo:block>
					</xsl:when>
					
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RIMBORSO_RID']">
						<fo:block xsl:use-attribute-sets="chr.pagamenti">Non c'e' niente da pagare!</fo:block>
						<fo:block xsl:use-attribute-sets="chrbold.008">L'importo totale del documento di <xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> euro le sara' rimborsato tramite accredito sul suo conto corrente normalmente utilizzato per il pagamento delle fatture Energit.</fo:block>
					</xsl:when>
					
					<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RIMBORSO_ASS']">
						<fo:block xsl:use-attribute-sets="chr.pagamenti">Non c'e' niente da pagare!</fo:block>
						<fo:block xsl:use-attribute-sets="chrbold.008">L'importo totale del documento di <xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> euro le sara' rimborsato tramite assegno inviato al suo indirizzo di spedizione delle fatture.</fo:block>
					</xsl:when>
					
					<xsl:otherwise>
						<fo:block xsl:use-attribute-sets="chr.pagamenti">Per questa nota di credito non c'e' niente da pagare</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</fo:block>	
</xsl:template>





<!--
*****************************
**   Avvisi frontespizio   **
*****************************
-->
<xsl:template name="AVVISI_FRONTESPIZIO">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">	
			<fo:table-row height="10mm">
				<fo:table-cell>
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<!-- Frasi RID ERRATO/REVOCATO - Deposito Cauzionale -->
			<fo:table-row height="11mm">
				<fo:table-cell>
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO='Addebito tramite RID')">
							<xsl:choose>
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RID_ERRATO']">
									<fo:block font-family="universcbold">
										ATTENZIONE! La sua Banca ha respinto la richiesta di attivazione del RID.
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>
								
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RID_REVOCATO']">
									<fo:block font-family="universcbold">
										Riattivi subito il RID per evitare il pagamento del deposito cauzionale!
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>
								
								<xsl:otherwise>
									<xsl:if test="@PRESENZA_DEPOSITO_CAUZIONALE='SI'">
										<fo:block font-family="universcbold">
											Rientri in possesso del deposito cauzionale passando al RID!
										</fo:block>
										<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
									</xsl:if>  
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row height="14mm">
			<fo:table-cell border-bottom="0.5 dashed thick black">
				<fo:block font-family="universcbold" font-size="8pt">
					IMPORTI FATTURATI IN ACCONTO SULLA BASE DI MISURE STIMATE E SOGGETTI A CONGUAGLIO
				</fo:block>
				</fo:table-cell>
			</fo:table-row>  
			<!-- Avviso fatture pagate/in attesa di pagamento -->
			
			<!--
			
			<fo:table-row height="14mm">
				<fo:table-cell border-bottom="0.5 dashed thick black">
					
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="not(./child::FATTURE_ATTESA_PAGAMENTO) or (./child::FATTURE_ATTESA_PAGAMENTO and (count(./child::FATTURE_ATTESA_PAGAMENTO) &lt; 1))">
							<fo:block font-family="universcbold">
								TUTTE LE FATTURE PRECEDENTI RISULTANO PAGATE. GRAZIE.
							</fo:block>
							<fo:block font-size="7.5pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7.5pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Onda nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
						
						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO)=1">
							<fo:block font-family="universcbold">
								ATTENZIONE: 1 FATTURA RISULTA IN ATTESA DI PAGAMENTO.
							</fo:block>
							<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1')">
								<fo:block font-size="7.5pt">Le ricordiamo che le fatture non pagate determinano la sospensione
								della fornitura a norma della delibera 04/08 AEEG</fo:block>
							</xsl:if>
							<fo:block font-size="7.5pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7.5pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Onda nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
						
						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO) &gt; 1">
							<fo:block font-family="universcbold">
								ATTENZIONE: <xsl:value-of select="count(./child::FATTURE_ATTESA_PAGAMENTO)"/> FATTURE RISULTANO IN ATTESA DI PAGAMENTO.
							</fo:block>
							<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1')">
								<fo:block font-size="7.5pt">Le ricordiamo che le fatture non pagate determinano la sospensione
								della fornitura a norma della delibera 04/08 AEEG</fo:block>
							</xsl:if>
							<fo:block font-size="7.5pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7.5pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Onda nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
					</fo:block>
					
				</fo:table-cell>
			</fo:table-row>  
			-->
		</fo:table-body>
	</fo:table>
</xsl:template>





<!--
**********************
**  Dati anagrafici **
**********************
-->
<xsl:template name="DATI_ANAGRAFICI">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.004 chr.009">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">   
			<fo:table-row>
				<fo:table-cell>
					<fo:block xsl:use-attribute-sets="chrbold.009">
						Cliente
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INTESTATARIO" /></fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INDIRIZZO" /></fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row border-bottom="0.5 dashed thick black">
				<fo:table-cell>
					<fo:block>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CITTA" />
						<xsl:if test="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA">
							<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA" /></fo:inline>
						</xsl:if>
						<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::NAZIONE='IT')">
							<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::STATO" /></fo:inline>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell padding-top="1mm">
					<fo:block>
						<fo:inline xsl:use-attribute-sets="chrbold.009">
							Partita IVA
						</fo:inline>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell>
					<fo:block>
						<fo:inline xsl:use-attribute-sets="chrbold.009">
							Codice Fiscale
						</fo:inline>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_FISCALE" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<xsl:if test="@SPOT='NO' or not(@SPOT)">
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:inline xsl:use-attribute-sets="chrbold.009">
								UserID
							</fo:inline>
							<xsl:value-of select="./child::USERNAME" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
			
			<xsl:if test="./child::CIG">
				<xsl:if test="@SPOT='NO' or not(@SPOT)">
					<fo:table-row>
						<fo:table-cell>
							<fo:block>
								<fo:inline xsl:use-attribute-sets="chrbold.009">
									Codice CIG
								</fo:inline>
								<xsl:value-of select="./child::CIG" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</xsl:if>
		</fo:table-body>
	</fo:table>
</xsl:template>





<!--
**********************************************
**  Riquadro fattura con bordi arrotondati  **
**********************************************
-->
<xsl:template name="RIQUADRO_SCADENZA_FATTURA">
	<xsl:param name="color"/>
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.004" font-family="universc" font-size="10pt">
	<xsl:attribute name="color"><xsl:value-of select="$color"/></xsl:attribute>
	<fo:table-column column-width="proportional-column-width(34)"/>
	<fo:table-column column-width="proportional-column-width(66)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">        
		<fo:table-row height="21.5mm">        
			<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
				<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(100)"/>
					<fo:table-body end-indent="0pt" start-indent="0pt">   
						<fo:table-row>
							<fo:table-cell>
								<fo:block font-family="universcbold" font-size="12pt">
									<xsl:choose>
										<xsl:when test="@TIPO_DOCUMENTO='FATTURA'">
											Fattura
										</xsl:when>
										<xsl:otherwise>
											Nota di credito
										</xsl:otherwise>
									</xsl:choose>
									<xsl:value-of select="@NUMERO_DOCUMENTO" />
								</fo:block>
								
								<fo:block font-family="universcbold" font-size="12pt">del <xsl:value-of select="./child::DATA_DOCUMENTO" /></fo:block>
								
								<xsl:choose>    
									<xsl:when test="@TIPO_DOCUMENTO='FATTURA'">
										<xsl:choose>
											<xsl:when test="not(@COMPETENZA)">
												<fo:block>Periodo di fatturazione</fo:block>
												<fo:block>
													<xsl:value-of select="./child::CONTRATTO_GAS/PERIODO_RIFERIMENTO" />
												</fo:block>
											</xsl:when>
											
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="@TIPO_COMPETENZA='fattura'">
														<fo:block>
															A
															<xsl:value-of select="@DESCRIZIONE_DOCUMENTO" />
															della fattura
															<xsl:value-of select="@COMPETENZA" />
														</fo:block>
													</xsl:when>
													<xsl:otherwise>
														<fo:block>
															A
															<xsl:value-of select="@DESCRIZIONE_DOCUMENTO" />
															della
														</fo:block>
														<fo:block>
															<xsl:value-of select="@TIPO_COMPETENZA" />
															<xsl:value-of select="@COMPETENZA" />
														</fo:block>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="not(@COMPETENZA)">
												<xsl:if test="./child::CONTRATTO_ENERGIA">
													<fo:block>Periodo di fatturazione</fo:block>
													<fo:block>
														<xsl:value-of select="./child::PERIODO_DOCUMENTO" />
													</fo:block>
												</xsl:if>
											</xsl:when>
											
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="@TIPO_COMPETENZA='fattura'">
														<fo:block>
															A <xsl:value-of select="@DESCRIZIONE_DOCUMENTO" /> della <xsl:value-of select="@TIPO_COMPETENZA" /><xsl:text>&#160;</xsl:text><xsl:value-of select="@COMPETENZA" />
														</fo:block>
													</xsl:when>
													<xsl:otherwise>
														<fo:block>
															A <xsl:value-of select="@DESCRIZIONE_DOCUMENTO" /> della
														</fo:block>
														
														<fo:block>
															<xsl:value-of select="@TIPO_COMPETENZA" /><xsl:text>&#160;</xsl:text><xsl:value-of select="@COMPETENZA" />
														</fo:block>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
			
			<fo:table-cell border-left="0.5 dashed thick black" xsl:use-attribute-sets="pad.lr.000">  
				<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" font-family="universcbold" font-size="18pt" text-align="end">
					<fo:table-column column-width="proportional-column-width(100)"/>
					<fo:table-body end-indent="0pt" start-indent="0pt">   
						<fo:table-row>
							<fo:table-cell>
								<xsl:choose>
									<xsl:when test="@TIPO_DOCUMENTO='FATTURA' and not(@COMPETENZA) and not(./child::IMPORTO_DOCUMENTO='0,00') and not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'La fattura risulta pagata')">                                    
										<fo:block>
											<fo:inline font-size="14pt">Il totale da pagare entro il </fo:inline>
											<xsl:value-of select="./child::SCADENZA_DOCUMENTO" />
											<fo:inline font-size="14pt"> e':</fo:inline>
										</fo:block>
									</xsl:when>
									<xsl:otherwise>
										<fo:block>Totale documento</fo:block>
									</xsl:otherwise>
								</xsl:choose>
								
								<fo:block space-before="2mm">
									<xsl:value-of select="./child::IMPORTO_DOCUMENTO" />
									euro
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:template>





<!--
***********************************
**  Indirizzo spedizione fattura **
***********************************
-->
<xsl:template name="SPEDIZIONE_FATTURA">
	<fo:block xsl:use-attribute-sets="blocco_indirizzo_spedizione" color="black">
		<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INTESTATARIO" /></fo:block>

		<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INDIRIZZO" /></fo:block>

		<fo:block>
			<xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CITTA" />
			<xsl:if test="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA">
				<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA" /></fo:inline>
			</xsl:if>
		</fo:block>

		<xsl:if test="not(./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::NAZIONE='IT')">
			<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::STATO" /></fo:block>
		</xsl:if>
	</fo:block>
</xsl:template>





<!--
*********************************
**  Riepilogo importi fattura  **
*********************************
-->
<xsl:template name="DETTAGLIO_FRONTESPIZIO">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="chr.008" display-align="center">
	<fo:table-column column-width="proportional-column-width(31)"/>
	<fo:table-column column-width="proportional-column-width(18)"/>
	<fo:table-column column-width="proportional-column-width(18)"/>
	<fo:table-column column-width="proportional-column-width(15)"/>
	<fo:table-column column-width="proportional-column-width(18)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<fo:table-row xsl:use-attribute-sets="chrbold.008">
			<fo:table-cell number-columns-spanned="5">
				<fo:block>DETTAGLIO</fo:block>
			</fo:table-cell>
		</fo:table-row>        
		
		<fo:table-row xsl:use-attribute-sets="chrbold.008">                            
			<fo:table-cell>
				<fo:block>Descrizione</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>Imponibile</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>Aliquota</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>IVA</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>Totale documento</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row height="0.5mm" border-bottom="0.5 dashed thick black">
			<fo:table-cell number-columns-spanned="5">
				<fo:block/>
			</fo:table-cell>
		</fo:table-row> 
		
		<fo:table-row height="0.5mm">
			<fo:table-cell number-columns-spanned="5">
				<fo:block/>
			</fo:table-cell>
		</fo:table-row> 
		
		<xsl:for-each select="./child::DETTAGLIO_FATTURA">       
			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::PRODOTTO_SERVIZIO" /></fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::IMPONIBILE" /></fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ALIQUOTA_IVA" />%</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::IMPORTO_IVA" /></fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::TOTALE" /></fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:for-each>
	</fo:table-body>
	</fo:table>
</xsl:template>

</xsl:stylesheet>