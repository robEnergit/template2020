<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**                                                    **
**    Template per specchietto consumi e conguagli    **
**                                                    **
********************************************************
-->

<xsl:template match="CONSUMI">
<xsl:param name="bordi"/>
<xsl:param name="color"/>
<xsl:param name="svg-titoli"/>
<xsl:param name="tipo"/>
<xsl:param name="mese_fattura"/>

<xsl:if test="$tipo='header'">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" text-align="center">
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-body>
		<fo:table-row keep-with-previous="always" height="2mm">
			<fo:table-cell number-columns-spanned="5">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row height="6mm">
			<fo:table-cell padding-left="3mm" number-columns-spanned="9" display-align="center">
				<xsl:attribute name="background-image"><xsl:value-of select="$svg-titoli"/></xsl:attribute>
				<fo:block text-align="start">
					<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">CONSUMI RILEVATI E FATTURATI - ACCONTI E CONGUAGLI</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row keep-with-previous="always" height="1mm">
			<fo:table-cell number-columns-spanned="9">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row keep-with-previous="always" height="3mm">
			<fo:table-cell border="0.5pt solid black" padding-left="3mm">
				<fo:block>
					<fo:inline>Mese</fo:inline>
				</fo:block>
			</fo:table-cell>
									
			<fo:table-cell border="0.5pt solid black">
				<fo:block>
					<fo:inline>Dal </fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell border="0.5pt solid black">
				<fo:block>
					<fo:inline>Al </fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell border="0.5pt solid black">
				<fo:block>
					<fo:inline>Lettura iniziale (mc)</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0.5pt solid black">
				<fo:block>
					<fo:inline>Lettura finale (mc)</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0.5pt solid black">
				<fo:block>
					<fo:inline>Consumo (mc)</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0.5pt solid black">
				<fo:block>
					<fo:inline>Coefficiente C</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0.5pt solid black">
				<fo:block>
					<fo:inline>Consumo fatturato (Smc)</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0.5pt solid black">
				<fo:block>
					<fo:inline>Tipo fatturazione</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
</xsl:if>

<xsl:if test="$tipo='body'">
	<fo:table  end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" text-align="center">
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-column column-width="proportional-column-width(20)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<xsl:for-each select="./child::CONSUMO">
			<fo:table-row>
				<fo:table-cell border="0.5pt solid black" padding-left="3mm">
					<fo:block>
						<fo:inline>
							<xsl:value-of select="@mese_riferimento"/>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::DATA_LETTURA_PRECDENTE">
								<fo:inline>
									<xsl:value-of select="./child::DATA_LETTURA_PRECDENTE"/>
								</fo:inline>
							</xsl:when>
							<xsl:otherwise>
								<fo:inline>-</fo:inline>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<fo:inline>
							<xsl:value-of select="./child::DATA_LETTURA_ATTUALE"/>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::VALORE_LETTURA_PRECEDENTE='0,00'">
								<fo:inline>-</fo:inline>
							</xsl:when>
							<xsl:otherwise>
								<fo:inline>
									<xsl:value-of select="./child::VALORE_LETTURA_PRECEDENTE"/>
								</fo:inline>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::VALORE_LETTURA_ATTUALE='0,00'">
								<fo:inline>-</fo:inline>
							</xsl:when>
							<xsl:otherwise>
								<fo:inline>
									<xsl:value-of select="./child::VALORE_LETTURA_ATTUALE"/>
								</fo:inline>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<fo:inline>
							<xsl:value-of select="./child::CONSUMO"/>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<fo:inline>
							<xsl:value-of select="./child::COEFFICIENTE_C"/>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<fo:inline>
							<xsl:value-of select="./child::CONSUMO_FATTURATO"/>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<fo:inline>
							<xsl:value-of select="./child::TIPO_FATTURAZIONE"/>	<!-- **** DATO MANCANTE *** -->
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
			</fo:table-row>
		</xsl:for-each>
		
			<fo:table-row>
				<fo:table-cell padding-left="3mm">
					<fo:block>
						<fo:inline>
						
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell >
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::DATA_LETTURA_PRECDENTE">
								<fo:inline>
									
								</fo:inline>
							</xsl:when>
							<xsl:otherwise>
								<fo:inline>-</fo:inline>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell >
					<fo:block>
						<fo:inline>
							
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell >
					<fo:block>
						
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					
					<fo:block text-align="right">
							
					</fo:block>
					
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block font-weight="bold" text-align="right">
						<fo:inline>
							Totale 
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block font-weight="bold" text-align="left">
						<fo:inline>
							consumo fatturato
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block font-weight="bold">
						<fo:inline >
							<xsl:value-of select="@CONSUMO_TOTALE_FATTURATO"/>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block>
						<fo:inline>
							<xsl:value-of select="./child::TIPO_FATTURAZIONE"/>	<!-- **** DATO MANCANTE *** -->
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
			</fo:table-row>
		
			
		
	</fo:table-body>
	</fo:table>
</xsl:if>
</xsl:template>

</xsl:stylesheet>