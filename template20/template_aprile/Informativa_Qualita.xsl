<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output encoding="UTF-8"/>

<!--
*******************************************************************
**                                                               **
**         In questo template vengono visualizzati:              **
**             - il bollettino postale                           **
**                                                               **
*******************************************************************
-->
<xsl:template name="INFORMATIVA_QUALITA"> 

<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:param>            

<!-- QUALITA' 2011 -->
<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='INFORMATIVA_QUALITA_2011_V1'">

	<fo:block break-before="page">
	</fo:block>

	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(35)"/>
		<fo:table-column column-width="proportional-column-width(65)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row height="5mm">        
				<fo:table-cell padding-top="-5mm">
					<fo:block>
						<fo:external-graphic src="img/logo-energit-BN.jpg"
							content-height="9mm" />
					</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block></fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>

	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(40)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		
		<fo:table-body text-align="center" xsl:use-attribute-sets="blk.000 chr.008">
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" number-columns-spanned="6" xsl:use-attribute-sets="brd.000">
					<fo:block>INDICATORI DI QUALITA' COMMERCIALE IN VIGORE DAL 01/07/2009</fo:block>
					<fo:block>Informativa ai sensi della delibera 164/08 dell'Autorità per l'energia elettrica e il gas</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Livelli specifici</fo:block>
					<fo:block>di qualità commerciale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Descrizioni</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Standard Specifico</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.000">
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.000 chrbold.008">
						<fo:table-column column-width="proportional-column-width(33.3)"/>
						<fo:table-column column-width="proportional-column-width(33.4)"/>
						<fo:table-column column-width="proportional-column-width(33.3)"/>
						<fo:table-body text-align="center">
							<fo:table-row>
								<fo:table-cell display-align="center" number-columns-spanned="3" xsl:use-attribute-sets="brd.b.000">
									<fo:block>Indennizzi automatici</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.r.000">
									<fo:block>Rimborso in € per esecuzione oltre lo standard ma entro un tempo doppio dello standard</fo:block>
								</fo:table-cell>
								<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.r.000">
									<fo:block>Rimborso in € per esecuzione oltre un tempo doppio dello standard ma entro un tempo triplo dello standard</fo:block>
								</fo:table-cell>
								<fo:table-cell display-align="center">
									<fo:block>Rimborso in € per esecuzione oltre un tempo triplo dello standard</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di risposta ai reclami scritti</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>E' il tempo misurato in giorni solari che intercorre tra la data di ricevimento del reclamo, presso gli appositi recapiti indicati in fattura, e la data di invio della risposta motivata al cliente, risultante dal timbro postale o dalla ricevuta del fax</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>40 giorni solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>20</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>40</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>60</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di rettifica di fatturazione</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>Il tempo di rettifica di fatturazione, è il tempo, misurato in giorni solari, intercorrente tra la data di ricevimento da parte del venditore della richiesta scritta di rettifica di fatturazione inviata dal cliente finale relativa ad una fattura già pagata, o per la quale è prevista la possibilità di rateizzazione ai sensi del contratto di fornitura, e la data di accredito della somma non dovuta, anche in misura diversa da quella richiesta. Sono inclusi i tempi per l’eventuale acquisizione da parte del venditore di dati tecnici in esclusivo possesso del distributore.</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>90 giorni solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>20</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>40</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>60</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di rettifica di doppia fatturazione</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>Il tempo di rettifica di doppia fatturazione è il tempo misurato in giorni solari, intercorrente tra la data di ricevimento da parte del venditore della richiesta scritta di rettifica di fatturazione per consumi relativi ad un punto di prelievo o di riconsegna il cui contratto di dispacciamento e/o di trasporto non sono nella disponibilità del venditore medesimo, e la data di accredito al cliente finale della somme non dovute. Sono inclusi i tempi per l’eventuale acquisizione da parte del venditore di dati tecnici in esclusivo possesso del distributore.</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>20 giorni solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>20</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>40</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>60</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>

	<fo:table space-before="5mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(40)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		
		<fo:table-body text-align="center" xsl:use-attribute-sets="blk.000 chr.008">
			
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Livelli generali</fo:block>
					<fo:block>di qualità commerciale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Descrizione</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Standard Generale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="3">
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale minima di risposte a richieste scritte di informazioni inviate entro il tempo massimo di 30 gg solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>Il tempo di risposta a richieste scritte di informazione è il tempo, misurato in giorni solari, intercorrente tra la data di ricevimento da parte del venditore della richiesta scritta di informazioni e la data di invio al richiedente da parte del venditore della risposta. Sono inclusi i tempi per l’eventuale acquisizione da parte del venditore di dati tecnici.</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>95%</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3">
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale minima di risposte a richieste scritte di rettifica di fatturazione di cui all'art 8 inviate entro il tempo massimo di 40 gg solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>Il tempo di risposta motivata a richieste scritte di rettifica di fatturazione è il tempo, misurato in giorni solari, intercorrente tra la data di ricevimento da parte del venditore della richiesta scritta di rettifica di fatturazione del cliente finale e la data di invio al cliente finale da parte del venditore della risposta motivata contenente l’esito delle azioni e degli accertamenti effettuati. Sono inclusi i tempi per l’eventuale acquisizione da parte del venditore di dati tecnici.</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>95%</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3">
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>

	<fo:table space-before="5mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(40)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		
		<fo:table-body text-align="center" xsl:use-attribute-sets="blk.000 chr.008">
		
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" number-columns-spanned="5" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NELL'ANNO 2010 - CLIENTI MULTISITO</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NELL'ANNO 2010 - CLIENTI DOMESTICI</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NELL'ANNO 2010 - CLIENTI NON DOMESTICI IN BASSA TENSIONE</xsl:when>
							<xsl:otherwise>LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NELL'ANNO 2010 - CLIENTI NON DOMESTICI IN MEDIA TENSIONE</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Livelli specifici </fo:block>
					<fo:block>di qualità commerciale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo medio effettivo di esecuzione delle prestazioni</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale effettiva di rispetto dei termini</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di risposta ai reclami scritti</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">37,39</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">28,40</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">29,26</xsl:when>
							<xsl:otherwise>34,57</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">74,31%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">97,78%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">95,24%</xsl:when>
							<xsl:otherwise>85,71%</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di rettifica di fatturazione</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">38,00</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">64,88</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">39,54</xsl:when>
							<xsl:otherwise>39,00</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">100,00%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">81,25%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">100,00%</xsl:when>
							<xsl:otherwise>100,00%</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di rettifica di doppia fatturazione</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">-</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">33,13</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">34,43</xsl:when>
							<xsl:otherwise>-</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">-</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">68,75%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">30,80%</xsl:when>
							<xsl:otherwise>-</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
		</fo:table-body>
	</fo:table>

	<fo:table space-before="5mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(40)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		
		<fo:table-body text-align="center" xsl:use-attribute-sets="blk.000 chr.008">
			
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Livelli generali </fo:block>
					<fo:block>di qualità commerciale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo medio effettivo di risposta</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale effettiva di rispetto dei termini</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale minima di risposte a richieste scritte di informazioni inviate entro il tempo massimo di 30 gg solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">33,00</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">10,00</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">12,00</xsl:when>
							<xsl:otherwise>17,00</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">76,92%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">95,27%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">93,68%</xsl:when>
							<xsl:otherwise>89,47%</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale minima di risposte a richieste scritte di rettifica di fatturazione di cui all'art 8 inviate entro il tempo massimo di 40 gg solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">27,33</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">41,51</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">38,67</xsl:when>
							<xsl:otherwise>39,00</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">91,66%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">70,96%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">63,81%</xsl:when>
							<xsl:otherwise>100,00%</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
		</fo:table-body>
	</fo:table>
</xsl:if>


<!-- QUALITA' 2010 -->
<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='INFORMATIVA_QUALITA_2010_V1'">

	<fo:block break-before="page">
	</fo:block>

	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(35)"/>
		<fo:table-column column-width="proportional-column-width(65)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row height="5mm">        
				<fo:table-cell padding-top="-5mm">
					<fo:block>
						<fo:external-graphic src="img/logo-energit-BN.jpg"
							content-height="9mm" />
					</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block></fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
	
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(40)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		
		<fo:table-body text-align="center" xsl:use-attribute-sets="blk.000 chr.008">
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" number-columns-spanned="6" xsl:use-attribute-sets="brd.000">
					<fo:block>INDICATORI DI QUALITA' COMMERCIALE IN VIGORE DAL 01/07/2009</fo:block>
					<fo:block>Informativa ai sensi della delibera 164/08 dell'Autorità per l'energia elettrica e il gas</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Livelli specifici</fo:block>
					<fo:block>di qualità commerciale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Descrizioni</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Standard Specifico</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.000">
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.000 chrbold.008">
						<fo:table-column column-width="proportional-column-width(33.3)"/>
						<fo:table-column column-width="proportional-column-width(33.4)"/>
						<fo:table-column column-width="proportional-column-width(33.3)"/>
						<fo:table-body text-align="center">
							<fo:table-row>
								<fo:table-cell display-align="center" number-columns-spanned="3" xsl:use-attribute-sets="brd.b.000">
									<fo:block>Indennizzi automatici</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.r.000">
									<fo:block>Rimborso in € per esecuzione oltre lo standard ma entro un tempo doppio dello standard</fo:block>
								</fo:table-cell>
								<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.r.000">
									<fo:block>Rimborso in € per esecuzione oltre un tempo doppio dello standard ma entro un tempo triplo dello standard</fo:block>
								</fo:table-cell>
								<fo:table-cell display-align="center">
									<fo:block>Rimborso in € per esecuzione oltre un tempo triplo dello standard</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di risposta ai reclami scritti</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>E' il tempo misurato in giorni solari che intercorre tra la data di ricevimento del reclamo, presso gli appositi recapiti indicati in fattura, e la data di invio della risposta motivata al cliente, risultante dal timbro postale o dalla ricevuta del fax</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>40 giorni solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>20</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>40</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>60</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di rettifica di fatturazione</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>Il tempo di rettifica di fatturazione, è il tempo, misurato in giorni solari, intercorrente tra la data di ricevimento da parte del venditore della richiesta scritta di rettifica di fatturazione inviata dal cliente finale relativa ad una fattura già pagata, o per la quale è prevista la possibilità di rateizzazione ai sensi del contratto di fornitura, e la data di accredito della somma non dovuta, anche in misura diversa da quella richiesta. Sono inclusi i tempi per l’eventuale acquisizione da parte del venditore di dati tecnici in esclusivo possesso del distributore.</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>90 giorni solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>20</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>40</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>60</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di rettifica di doppia fatturazione</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>Il tempo di rettifica di doppia fatturazione è il tempo misurato in giorni solari, intercorrente tra la data di ricevimento da parte del venditore della richiesta scritta di rettifica di fatturazione per consumi relativi ad un punto di prelievo o di riconsegna il cui contratto di dispacciamento e/o di trasporto non sono nella disponibilità del venditore medesimo, e la data di accredito al cliente finale della somme non dovute. Sono inclusi i tempi per l’eventuale acquisizione da parte del venditore di dati tecnici in esclusivo possesso del distributore.</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>20 giorni solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>20</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>40</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>60</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>

	<fo:table space-before="5mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(40)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		
		<fo:table-body text-align="center" xsl:use-attribute-sets="blk.000 chr.008">
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" number-columns-spanned="3" xsl:use-attribute-sets="brd.000">
					<fo:block>LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NEL SECONDO SEMESTRE DEL 2009 - CLIENTI DOMESTICI</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Livelli generali</fo:block>
					<fo:block>di qualità commerciale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Descrizione</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Standard Generale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="3">
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale minima di risposte a richieste scritte di informazioni inviate entro il tempo massimo di 30 gg solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>Il tempo di risposta a richieste scritte di informazione è il tempo, misurato in giorni solari, intercorrente tra la data di ricevimento da parte del venditore della richiesta scritta di informazioni e la data di invio al richiedente da parte del venditore della risposta. Sono inclusi i tempi per l’eventuale acquisizione da parte del venditore di dati tecnici.</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>95%</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3">
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale minima di risposte a richieste scritte di rettifica di fatturazione di cui all'art 8 inviate entro il tempo massimo di 40 gg solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" text-align="justify" padding="1mm">
					<fo:block>Il tempo di risposta motivata a richieste scritte di rettifica di fatturazione è il tempo, misurato in giorni solari, intercorrente tra la data di ricevimento da parte del venditore della richiesta scritta di rettifica di fatturazione del cliente finale e la data di invio al cliente finale da parte del venditore della risposta motivata contenente l’esito delle azioni e degli accertamenti effettuati. Sono inclusi i tempi per l’eventuale acquisizione da parte del venditore di dati tecnici.</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>95%</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3">
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>

	<fo:table space-before="5mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(40)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		
		<fo:table-body text-align="center" xsl:use-attribute-sets="blk.000 chr.008">
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" number-columns-spanned="5" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NEL SECONDO SEMESTRE DEL 2009 - CLIENTI NON DOMESTICI</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NEL SECONDO SEMESTRE DEL 2009 - CLIENTI DOMESTICI</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NEL SECONDO SEMESTRE DEL 2009 - CLIENTI NON DOMESTICI</xsl:when>
							<xsl:otherwise>LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NEL SECONDO SEMESTRE DEL 2009 - CLIENTI NON DOMESTICI</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Livelli specifici </fo:block>
					<fo:block>di qualità commerciale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo medio effettivo di esecuzione delle prestazioni</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale effettiva di rispetto dei termini</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di risposta ai reclami scritti</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">34,29 giorni solari</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">27,78 giorni solari</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">23,58 giorni solari</xsl:when>
							<xsl:otherwise>24,1 giorni solari</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">88,10%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">94,34%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">95,69%</xsl:when>
							<xsl:otherwise>100,00%</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di rettifica di fatturazione</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">21 giorni solari</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">29 giorni solari</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">48,78 giorni solari</xsl:when>
							<xsl:otherwise>-</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">100,00%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">100,00%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">86,05%</xsl:when>
							<xsl:otherwise>-</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo massimo di rettifica di doppia fatturazione</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">-</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">33,28 giorni solari</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">25,62 giorni solari</xsl:when>
							<xsl:otherwise>-</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">-</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">37,50%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">51,55%</xsl:when>
							<xsl:otherwise>-</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
		</fo:table-body>
	</fo:table>

	<fo:table space-before="5mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(40)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		<fo:table-column column-width="proportional-column-width(10)"/>
		
		<fo:table-body text-align="center" xsl:use-attribute-sets="blk.000 chr.008">
			
			<fo:table-row keep-with-next="always" xsl:use-attribute-sets="chrbold.008">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Livelli generali </fo:block>
					<fo:block>di qualità commerciale</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Tempo medio effettivo di risposta</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale effettiva di rispetto dei termini</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale minima di risposte a richieste scrittedi informazioni inviate entro il tempo massimo di 30 gg solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">19,74 giorni solari</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">5,87 giorni solari</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">7,6 giorni solari</xsl:when>
							<xsl:otherwise>12,34 giorni solari</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">90%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">98,71%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">98,76%</xsl:when>
							<xsl:otherwise>94,74%</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>Percentuale minima di risposte a richieste scritte di rettifica di fatturazione di cui all'art 8 inviate entro il tempo massimo di 40 gg solari</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">24,64 giorni solari</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">27,89 giorni solari</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">20,85 giorni solari</xsl:when>
							<xsl:otherwise>36 giorni solari</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell number-columns-spanned="3" display-align="center" xsl:use-attribute-sets="brd.000">
					<fo:block>
						<xsl:choose>
							<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA">92,31%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS[@CONSUMER='SI']">66,7%</xsl:when>
							<xsl:when test="./child::CONTRATTO_GAS/child::TENSIONE_ALIMENTAZIONE='Bassa'">88,39%</xsl:when>
							<xsl:otherwise>100%</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>
			
		</fo:table-body>
	</fo:table>
</xsl:if>

<!-- QUALITA' 2009 -->
<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='INFORMATIVA_QUALITA_2009_V1'">
	
	<fo:block break-before="page">
	</fo:block>

	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
        <fo:table-column column-width="proportional-column-width(35)"/>
        <fo:table-column column-width="proportional-column-width(65)"/>
        <fo:table-body end-indent="0pt" start-indent="0pt">
        
            <fo:table-row height="30mm">        
                <fo:table-cell>
					<fo:block>
						<fo:external-graphic src="url(img/logo-fattura-bn.jpg)" content-width="37mm" />
					</fo:block>
                </fo:table-cell>
                
                <fo:table-cell>
					<fo:block/>
                </fo:table-cell>
            </fo:table-row>
        </fo:table-body>
    </fo:table>

	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(21)"/>
		<fo:table-column column-width="proportional-column-width(24)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(13)"/>
		
		<fo:table-header>
			<fo:table-row keep-with-next="always">
				<fo:table-cell number-columns-spanned="6" xsl:use-attribute-sets="padding_informativa_bottom_titolo">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">INDICATORI DI QUALITA' COMMERCIALE IN VIGORE DAL 01/01/2006</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Informativa ai sensi della delibera 04/2004 dell'Autorità per l'energia elettrica e il gas</fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">LIVELLI SPECIFICI</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI QUALITA' COMMERCIALE</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DESCRIZIONE</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2" display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">TEMPO MASSIMO</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI ESECUZIONE DELLE PRESTAZIONI</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2" display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">INDENNIZZI AUTOMATICI</fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell number-columns-spanned="6" xsl:use-attribute-sets="brd.b.000">
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">Tempo di rettifica</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">di fatturazione</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top2 padding_informativa_bottom">
					<fo:block text-align="left" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">E' il tempo che intercorre tra la data della</fo:inline>
					</fo:block>
					<fo:block text-align="left" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">richiesta di rettifica di un importo già pagato</fo:inline>
					</fo:block>
					<fo:block text-align="left" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">dal cliente e la data di accredito della</fo:inline>
					</fo:block>
					<fo:block text-align="left" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">somma non dovuta</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Bassa</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='20pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">90 gg Solari</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Media</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='20pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">60 gg Solari</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Bassa</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='20pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">€ 60,00</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Media</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='20pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">€ 120,00</fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row keep-with-previous="always">
				<fo:table-cell number-columns-spanned="6" xsl:use-attribute-sets="brd.b.000">
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
			
	<fo:table space-before='8mm' end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(21)"/>
		<fo:table-column column-width="proportional-column-width(24)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(13)"/>
		
		<fo:table-header>
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">LIVELLI GENERALI</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI QUALITA' COMMERCIALE</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DESCRIZIONE</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2" display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">TEMPO MASSIMO</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI ESECUZIONE DELLE PRESTAZIONI</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2" display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">PERCENTUALE MINIMA</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI RISPETTO DEI TERMINI</fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell number-columns-spanned="6" xsl:use-attribute-sets="brd.b.000">
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">Tempo di risposta motivata</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">a reclami scritti o a richieste</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">di informazioni scritte</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top2 padding_informativa_bottom">
					<fo:block text-align="left" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">E' il tempo che intercorre tra la data di</fo:inline>
					</fo:block>
					<fo:block text-align="left" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">ricevimento del reclamo o della richiesta di</fo:inline>
					</fo:block>
					<fo:block text-align="left" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">informazioni e la data di invio della risposta</fo:inline>
					</fo:block>
					<fo:block text-align="left" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">motivata al cliente, risultante dal timbro</fo:inline>
					</fo:block>
					<fo:block text-align="left" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">postale o della ricevuta del fax</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Bassa</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='20pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">20 gg Lavorativi</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Media</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='20pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">20 gg Lavorativi</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Bassa</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='20pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">90%</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Media</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='20pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">95%</fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row keep-with-previous="always">
				<fo:table-cell number-columns-spanned="6" xsl:use-attribute-sets="brd.b.000">
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
	
	<fo:table space-before='16mm' end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(17)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(27)"/>
		
		<fo:table-header>
			<fo:table-row keep-with-next="always">
				<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="padding_informativa_bottom_titolo">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">LIVELLI DI QUALITA' RAGGIUNTI DA ENERG.IT S.P.A. NELL'ANNO 2006</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">LIVELLI SPECIFICI</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI QUALITA' COMMERCIALE</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2" display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">TEMPO MEDIO EFFETTIVO</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI ESECUZIONE DELLE PRESTAZIONI</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2" display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">PERCENTUALE EFFETTIVA</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI RISPETTO DEI TERMINI</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="brd.b.000">
					<fo:block/>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">Tempo di rettifica</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">di fatturazione</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Bassa</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='10pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">42 gg Lavorativi</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Media</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='10pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">19 gg Lavorativi</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Bassa</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='10pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">100,00%</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Media</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='10pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">100,00%</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row keep-with-previous="always">
				<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="brd.b.000">
					<fo:block/>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
	
	<fo:table space-before='8mm' end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(17)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(14)"/>
		<fo:table-column column-width="proportional-column-width(27)"/>
		
		<fo:table-header>
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">LIVELLI GENERALI</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI QUALITA' COMMERCIALE</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2" display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">TEMPO MEDIO EFFETTIVO</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI ESECUZIONE DELLE PRESTAZIONI</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2" display-align="center" xsl:use-attribute-sets="brd.t.000 padding_informativa_top padding_informativa_bottom_intestazione">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">PERCENTUALE EFFETTIVA</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">DI RISPETTO DEI TERMINI</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="brd.b.000">
					<fo:block/>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-next="always">
				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">Tempo di risposta motivata</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">a reclami scritti o a richieste</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">di informazioni scritte</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Bassa</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='10pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">8,2 gg Lavorativi</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Media</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='10pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">10,5 gg Lavorativi</fo:inline>
					</fo:block>
				</fo:table-cell>

				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Bassa</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='10pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">98,94%</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell display-align="center" xsl:use-attribute-sets="padding_informativa_top padding_informativa_bottom">
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Clienti in Media</fo:inline>
					</fo:block>
					<fo:block text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chrbold.008">Tensione</fo:inline>
					</fo:block>
					<fo:block space-before='10pt' text-align="center" xsl:use-attribute-sets="blk.000">
						<fo:inline xsl:use-attribute-sets="chr.008">100,00%</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row keep-with-previous="always">
				<fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="brd.b.000">
					<fo:block/>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
</xsl:if>

</xsl:template>

<xsl:attribute-set name="padding_informativa_top">
    <xsl:attribute name="padding-top">1mm</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="padding_informativa_top2">
    <xsl:attribute name="padding-top">3mm</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="padding_informativa_bottom">
    <xsl:attribute name="padding-bottom">3mm</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="padding_informativa_bottom_intestazione">
    <xsl:attribute name="padding-bottom">1mm</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="padding_informativa_bottom_titolo">
    <xsl:attribute name="padding-bottom">2mm</xsl:attribute>
</xsl:attribute-set>

</xsl:stylesheet>

