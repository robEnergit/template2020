<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:fox="http://xml.apache.org/fop/extensions">
<xsl:output encoding="UTF-8"/>

<!--
**************************************************************************************
**                 Realizzato da Marini Pierluigi                                   **
**                                                                                  **
**                 In questo template vengono visualizzati:                         **
**                 - le condizioni generali di contratto                            **
**                                                                                  **
**************************************************************************************
-->

<!--
******************************************************************************************************************
**                                       Definizione dei parametri                                              **
******************************************************************************************************************
-->
<!--
<xsl:param name="f3_color">#DDDDDD</xsl:param>
<xsl:param name="f2_color">#BBBBBB</xsl:param>
<xsl:param name="f1_color">#FFFFFF</xsl:param>
<xsl:param name="riga_schema">1mm</xsl:param>
-->
<xsl:param name="altezzariga">6.5pt</xsl:param>
<!--
..................................................................................................................
-->

<xsl:template name="SCHEDA_RIEPILOGO_C11">

<fo:page-sequence initial-page-number="1" force-page-count="no-force" master-reference="pm0_scheda_riepilogo" orphans="2" white-space-collapse="false" widows="2">
	<fo:static-content flow-name="footer_riepilogo">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row height="10.4pt">
					<fo:table-cell display-align="center">
						<fo:block text-align="center">
							<fo:inline xsl:use-attribute-sets="chr.condizioni_footer"><fo:page-number/>/<fo:page-number-citation ref-id="@id{generate-id(.)}"/></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:block xsl:use-attribute-sets="blocco_footer">
		</fo:block>
	</fo:static-content>
	
	<fo:flow flow-name="body">

		<!--
		******************************************************************************************************************
		**                                             Prima pagina                                                     **
		******************************************************************************************************************
		-->
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>

			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row height="25.2mm">
					<fo:table-cell xsl:use-attribute-sets="brd.000" display-align="center">
						<fo:block xsl:use-attribute-sets="chr.riepilogo10" text-align="center">Allegato “A”</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo10">
						  <fo:leader line-height="12.5pt"/>
						</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo10bold" text-align="center">NUOVE CONDIZIONI GENERALI - ENERGIT S.p.A.</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo10bold" text-align="center">Scheda di riepilogo degli effetti della variazione contrattuale proposta</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row height="9mm">
					<fo:table-cell>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell padding-left="5.5pt" padding-right="5.5pt">
						<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">La presente scheda di riepilogo è redatta ai sensi e per gli effetti dell'articolo 12.3 della delibera AEEG n. 105/106, ai sensi del quale in caso di variazione unilaterale di clausole contrattuali l'esercente la vendita deve, tra l'altro, fornire "<fo:inline xsl:use-attribute-sets="chr.riepilogo8italic">l'illustrazione chiara, completa e comprensibile dei contenuti e degli effetti della variazione proposta</fo:inline>".</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Dal punto di vista prettamente formale, si segnala che tutte le clausole delle condizioni generali di contratto per la fornitura di energia elettrica e dei servizi associati stipulate con la Energ.it S.p.A., come attualmente in vigore ("<fo:inline xsl:use-attribute-sets="chr.riepilogo8bold">Condizioni Generali</fo:inline>" o “<fo:inline xsl:use-attribute-sets="chr.riepilogo8bold">CG</fo:inline>”) saranno modificate per effetto dell'adozione delle nuove condizioni generali, applicabili dal <xsl:value-of select="./child::CONTRATTO_GAS[@NUOVE_CONDIZIONI='C11']/attribute::DATA_NUOVE_CONDIZIONI"/> (le "<fo:inline xsl:use-attribute-sets="chr.riepilogo8bold">Nuove Condizioni Generali</fo:inline>" o "<fo:inline xsl:use-attribute-sets="chr.riepilogo8bold">NCG</fo:inline>").</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8">
						  <fo:leader line-height="8pt"/>
						</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Molte delle modifiche apportate alle NCG rispetto le CG sono di carattere meramente formale (ad esempio, riformulazioni testuali, semplice spostamento o diversa numerazione delle clausole contrattuali). Pertanto, esse non saranno oggetto di analisi da parte della presente scheda di riepilogo non derivandone alcun effetto inerente ai rispettivi diritti ed obblighi delle Parti.</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">In conformità a quanto disposto dalla normativa applicabile, il presente documento si limita a specificare i contenuti e gli effetti sui diritti ed obblighi delle Parti ai sensi delle CG che deriveranno dall'adozione delle NCG.</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8">
						  <fo:leader line-height="8pt"/>
						</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Salvo ove diversamente specificato, i termini con iniziale maiuscola utilizzati nel presente documento avranno il medesimo significato loro attribuito nelle CG e/o nelle NCG.</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8">
						  <fo:leader line-height="8pt"/>
						</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Titolo della norma e dati di pubblicazione relativi ad ogni riferimento normativo quivi citato sono indicati in calce alle NCG.</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8">
						  <fo:leader line-height="8pt"/>
						</fo:block>
						<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Le clausole cui fa riferimento la tabella sotto riportata sono quelle previste dalle Nuove Condizioni Generali.</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row height="9mm">
					<fo:table-cell>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell>
					
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(20.8)"/>
						<fo:table-column column-width="proportional-column-width(79.2)"/>
							
							<fo:table-body end-indent="0pt" start-indent="0pt">
								
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">CLAUSOLA</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8bold" text-align="justify">ILLUSTRAZIONE DEL CONTENUTO E DEGLI EFFETTI DELLA VARIAZIONE CONTRATTUALE PROPOSTA (NCG)</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">1.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">TERMINI</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">E DEFINIZIONI</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
											<!--<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold"></fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">E DEFINIZIONI</fo:block>
											  </fo:list-item-body>
											</fo:list-item>-->
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’articolo 1 delle NCG è stato semplicemente riformulato anche al fine di prevedere nuove e specifiche definizioni.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Da tali modifiche non derivano effetti inerenti ai rispettivi diritti ed obblighi delle Parti.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">2.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">OGGETTO E</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">CARATTERISTICHE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DEL SERVIZIO</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Il precedente articolo 2.3 è diventato il nuovo articolo 3, che regolamenta con maggior dettaglio la disciplina dei rapporti tra le Parti in tema di deleghe, import e diritti CIP 6.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Da tali modifiche non derivano effetti inerenti ai rispettivi diritti ed obblighi delle Parti.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">3.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DELEGHE, IMPORT</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">E CIP6</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L'articolo 3 delle NCG corrisponde all'articolo 2.3 delle CG.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Da tali modifiche non derivano effetti inerenti ai rispettivi diritti ed obblighi delle Parti ai sensi delle NCG.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">4.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">GESTIONE TECNICA</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DELLA FORNITURA</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">In conformità a quanto previsto dalla delibera AEEG n.333/07, il nuovo articolo 4 delle NCG regolamenta la gestione tecnica della fornitura e prevede il conferimento dal Cliente ad Energit di apposito mandato gratuito per lo svolgimento delle attività propedeutiche alla fornitura del Servizio nei confronti del Distributore Locale (ad es., (a) voltura dei Siti; (b) aumento di potenza del Sito; (c) nuova attivazione; (d) subentro nelle forniture al Sito; (e) spostamento gruppo di misura; (f) spostamento impianto di misurazione; (g) verifica gruppo di misura/tensione; (h) sospensione e interruzione della fornitura; (i) diminuzione di potenza; (j) cambio tensione).</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Resta comunque salvo il diritto del Cliente alimentato in bassa tensione di rivolgersi direttamente al Distributore Locale solamente per: (i) reclami scritti o richieste scritte di informazioni relative ai servizi di distribuzione o di misura; (ii) richieste di spostamenti di linee ed impianti elettrici che non siano funzionali allo spostamento del gruppo di misura del Cliente; e (iii) richieste di spostamento di prese non attive, se effettuate da un richiedente diverso dal subentrante.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Ai sensi delle NCG, il Cliente manleverà Energit da ogni responsabilità, costo, onere o conseguenza negativa che possa derivare dallo svolgimento delle attività oggetto di mandato, essendo tali attività effettuate esclusivamente dal Distributore Locale a seguito di espressa richiesta del Cliente, che Energit, per agevolare la gestione tecnica ed operativa della fornitura del Servizio, si limita ad inoltrare al Distributore Locale.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Come conseguenza di tale novità, non sarà più necessario il conferimento di apposito e separato atto di mandato a favore di Energit, essendo quest’ultimo conferito con la sola sottoscrizione/applicazione delle Nuove Condizioni Generali.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8underline" text-align="justify">Tale meccanismo troverà applicazione anche ai Clienti attualmente serviti da Energit che non esercitino il diritto di recesso per cambiamento delle Condizioni Generali nei cui confronti, pertanto, troveranno applicazione le NCG a partire dal <xsl:value-of select="./child::CONTRATTO_GAS[@NUOVE_CONDIZIONI='C11']/attribute::DATA_NUOVE_CONDIZIONI"/></fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">5.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">SERVIZIO</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DI TRASPORTO</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">E DISPACCIAMENTO</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DELL’ENERGIA</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L'articolo 5 delle NCG corrisponde all'articolo 4 delle CG.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Da tali modifiche non derivano effetti inerenti ai rispettivi diritti ed obblighi delle Parti ai sensi delle CG.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">6.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">CONCLUSIONE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DEL CONTRATTO</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L'articolo 6 delle NCG prevede un’inversione delle posizioni delle Parti con riferimento alla fase di conclusione del Contratto.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">La Proposta contrattuale è effettuata da Energit e l’accettazione della stessa è prerogativa del Cliente.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Tale modifica riguarderà unicamente i nuovi soggetti che sottoscriveranno le NCG.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Da tali modifiche non derivano invece effetti in capo ai Clienti il cui rapporto contrattuale con Energit è già regolato dalle CG.</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>

			</fo:table-body>
		</fo:table>




		<fo:block break-before="page">
		</fo:block>


		<!--
		******************************************************************************************************************
		**                                            Seconda pagina                                                    **
		******************************************************************************************************************
		-->
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>

			<fo:table-body end-indent="0pt" start-indent="0pt">
				
				<fo:table-row>
					<fo:table-cell>
					
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(20.8)"/>
						<fo:table-column column-width="proportional-column-width(79.2)"/>
							
							<fo:table-body end-indent="0pt" start-indent="0pt">
								
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">7.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold" text-align="justify">DURATA</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold" text-align="justify">DEL CONTRATTO</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold" text-align="justify">E RECESSO</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L'articolo 7 delle NCG modifica la disciplina della durata e del recesso dal Contratto come segue:</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:list-block provisional-distance-between-starts="50pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label start-indent="20pt" end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">(i)</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">in aggiunta alle tradizionali modalità di comunicazione tra le Parti è previsto che possano essere utilizzati anche il fax o la posta elettronica certificata (PEC) al fine di trasmissione della disdetta e del recesso dal Contratto;</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">8.</fo:block>
											  
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold" text-align="justify">VARIAZIONE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold" text-align="justify">DELLE SPECIFICHE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold" text-align="justify">TECNICHE E DELLE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold" text-align="justify">CONDIZIONI</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold" text-align="justify">DEL CONTRATTO </fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 8 del NCG corrisponde all’art 7 delle CG. Salvo ed in aggiunta a modifiche prettamente formali, la disciplina inerente la proposta di variazione delle specifiche contrattuali e tecniche del Contratto sarà modificata dalle NCG come segue:</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:list-block provisional-distance-between-starts="50pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label start-indent="20pt" end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">(i)</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">anche in tale ambito le Parti potranno utilizzare come modalità di comunicazione la PEC;</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
										<fo:list-block provisional-distance-between-starts="50pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label start-indent="20pt" end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">(ii)</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">in ogni caso di variazioni delle specifiche tecniche e delle condizioni delle NCG, il Cliente avrà diritto di recedere dal Contratto mediante comunicazione scritta che dovrà pervenire ad Energit entro e non oltre 15 (non più entro 60) giorni dalla data di comunicazione delle variazioni;</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
										<fo:list-block provisional-distance-between-starts="50pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label start-indent="20pt" end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">(iii)</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">gli effetti del recesso da parte del Cliente non saranno più immediati, in quanto il recesso sarà efficace una volta trascorsi i tempi tecnici imposti dal Distributore Locale per la cessazione della fornitura di cui al Contratto;</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
										<fo:list-block provisional-distance-between-starts="50pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label start-indent="20pt" end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">(iv)</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8">qualora il Cliente richieda la modifica delle modalità e/o delle specifiche di fornitura del Servizio, Energit potrà richiedere l'adeguamento delle garanzie prestate dal Cliente per la fornitura del Servizio.</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="10pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">9.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">PREZZO</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 9 del NCG corrisponde all’art 10 delle CG. L’articolo relativo al Prezzo è stato riformulato al fine di indicarne le componenti in maniera maggiormente trasparente e conforme alle relative disposizioni contenute nelle delibere dell’AEEG.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Inoltre, in caso di variazioni del Prezzo o di componenti dello stesso per l’Anno Contrattuale successivo, il Cliente avrà diritto di recedere dal Contratto mediante comunicazione scritta che dovrà pervenire ad Energit entro 45 (e non più 30) giorni prima della scadenza dell’Anno Contrattuale in corso.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">E’ stata redatta con maggior chiarezza la clausola in base alla quale, in conformità all’articolo 12 della delibera AEEG n.105/06, è previsto l’obbligo di Energit di informare il Cliente nella prima fattura successiva dell'applicazione di meccanismi di indicizzazione e/o di adeguamento automatico del Prezzo e/o di componenti dello stesso al fine di individuare espressamente le variazioni così apportate al Prezzo e/o alle componenti dello stesso.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">10.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">FATTURAZIONE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">E PAGAMENTI</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 10 del NCG corrisponde all’art 11 delle CG. È’ stata modificata la modalità di calcolo del tasso di interessi applicabile in caso di mancato o ritardato pagamento del Cliente.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Le CG prevedevano un tasso pari al tasso medio per i crediti personali e altri finanziamenti alle famiglie effettuati dalle banche nel trimestre precedente. Le NCG prevedono che gli interessi di mora che il Cliente è obbligato a pagare ad Energit in caso di ritardato pagamento delle fatture saranno pari al tasso Euribor 6 mesi maggiorato 5%.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Le NCG prevedono altresì che, in caso di ritardi nel pagamento da parte del Cliente, saranno fatturati al Cliente eventuali costi, oneri o spese ad esso imputabili in conseguenza del proprio inadempimento, quali, a titolo esemplificativo, spese di sollecito e di gestione amministrativa e/o legale.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">11.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">MEZZI DI GARANZIA</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">– DEPOSITO</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">CAUZIONALE</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 11 del NCG corrisponde all’art 12 delle CG .La consegna del deposito cauzionale è condizione per la richiesta di attivazione del Servizio da parte di Energit al Distributore Locale.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Pertanto, fintantoché il Cliente non abbia debitamente consegnato la suddetta garanzia, Energit non sottoscriverà i contratti di cui all’articolo 5 (Servizio di Trasporto e Dispacciamento dell’Energia) ed il Servizio non potrà avere inizio.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Il deposito cauzionale sarà commisurato al valore stimato di 5 mesi di fornitura del Servizio. Energit, a seguito di analisi condotte sull’affidabilità finanziaria del Cliente e a sua insindacabile discrezione, potrà tuttavia ridurre l’importo richiesto a titolo di deposito cauzionale.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Tale modifica riguarderà unicamente i nuovi soggetti che sottoscriveranno le NCG.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Da tali modifiche non derivano invece effetti in capo ai Clienti il cui rapporto contrattuale con Energit è già regolato dalle CG.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Nel caso in cui il Cliente ometta di adeguare l’importo del deposito cauzionale entro 10 giorni dalla richiesta di Energit, Energit avrà il diritto di addebitare in fattura al Cliente l’ammontare corrispondente all’adeguamento richiesto.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Il Cliente si obbliga altresì a reintegrare il deposito cauzionale a seconda del caso, in caso di avvenuta escussione da parte di Energit, con addebito nella prima fattura utile.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">All’atto di cessazione del Contratto (e non più entro 90 dalla cessazione stessa) Energit si obbliga a restituire al Cliente (non più soltanto le somme versate a titolo di deposito cauzionale, ma) anche gli interessi sulle stesse maturati, calcolati in base al tasso d’interesse legale.</fo:block>                                
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">12.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">SOSPENSIONI</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DEL SERVIZIO</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">In ottemperanza a quanto disposto dalla delibera AEEG n. 04/08, sono stati dettagliatamente regolati termini e condizioni per la sospensione del Servizio da parte di Energit in caso di (i) mancato pagamento da parte del Cliente di qualsiasi importo dovuto ad Energit ai sensi del Contratto, nei termini ed alle condizioni ivi previste; (ii) mancata consegna e/o mancato reintegro del deposito cauzionale; e (iii) consumo anomalo od uso improprio del Servizio da parte del Cliente ai sensi del nuovo articolo 15.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">A differenza di quanto previsto dalle CG, l'articolo 12 delle NCG prevede espressamente la facoltà per Energit di sospendere e/o di non dare avvio al Servizio, previa comunicazione al Cliente, nei seguenti casi:</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">- mancata ricezione del deposito cauzionale o della fideiussione bancaria, e/o nel caso in cui il Cliente non provveda a reintegrarne l’importo entro 5 (cinque) giorni dalla richiesta a mezzo lettera raccomandata A/R da parte di Energit; e/o</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">- consumo anomalo od uso improprio del Servizio da parte del Cliente.</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>

			</fo:table-body>
		</fo:table>
		<!--
		..................................................................................................................
		-->        







		<fo:block break-before="page">
		</fo:block>





		<!--
		******************************************************************************************************************
		**                                             Terza pagina                                                     **
		******************************************************************************************************************
		-->
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>

			<fo:table-body end-indent="0pt" start-indent="0pt">
				
				<fo:table-row>
					<fo:table-cell>
					
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(20.8)"/>
						<fo:table-column column-width="proportional-column-width(79.2)"/>
							
							<fo:table-body end-indent="0pt" start-indent="0pt">
								
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">13.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">RISOLUZIONE</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Per ragioni di maggior chiarezza e trasparenza, sono state incorporate in un unico nuovo articolo (articolo 13 delle NCG) le ipotesi di risoluzione del Contratto da parte di Energit ai sensi dell’articolo 1456 del codice civile, ovvero per inadempimento da parte del Cliente agli obblighi di cui alle seguenti clausole delle NCG: 6.6 (dichiarazioni e garanzie), 10.4 (mancato pagamento di una fattura nel termine indicato in fattura), 11 (mezzi di garanzia), 12 (sospensione del Servizio); 15 (uso improprio del Servizio), 17 (riservatezza) e 21 (cessione del contratto).</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">14.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">LIMITAZIONE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DI RESPONSABILITÀ</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DI ENERGIT -</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">OBBLIGO DEL CLIENTE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DI LIMITARE GLI</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">EVENTUALI</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DANNI</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">E’ stato eliminato il precedente articolo 13.4 delle CG che prevedeva un’ipotesi di limitazione della responsabilità di Energit nei confronti del Cliente e di terzi per eventuali danni a questi causati con riferimento al Servizio.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Le restanti modifiche apportate all'articolo 14 delle NCG sono di carattere prettamente formale e non hanno impatto sulla disciplina del rapporto contrattuale tra Energit ed il Cliente.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">15.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">USO IMPROPRIO</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DEL SERVIZIO</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 15 del NCG corrisponde all’art 14 delle CG. E’ stato eliminato l’obbligo del Cliente di informare Energit in caso di cessazione e/o sospensione del Servizio imputabile a Terna, al Distributore Locale e/o ad altro soggetto preposto in esecuzione del Contratto di Trasporto e/o del Contratto di Dispacciamento.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">16.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">TRATTAMENTO</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DATI PERSONALI</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">Decreto legislativo</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">n.196/03</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 16 del NCG corrisponde all’art 15 delle CG L’articolo in oggetto è stato interessato da modifiche meramente formali, da cui non deriveranno effetti inerenti ai rispettivi diritti ed obblighi delle Parti ai sensi delle NCG.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">17.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">RISERVATEZZA,</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">PUBBLICITÀ</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 17 del NCG corrisponde all’art 16 delle CG L’articolo in oggetto è stato interessato da modifiche meramente formali, da cui non deriveranno effetti inerenti ai rispettivi diritti ed obblighi delle Parti ai sensi delle NCG.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">18.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">RECLAMI, RIMBORSI</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 18 del NCG corrisponde all’art 17 delle CG Si segnala in primo luogo l’eliminazione della seguente frase, prevista al precedente articolo 17.1: “[…] <fo:inline xsl:use-attribute-sets="chr.riepilogo8italic">entro e non oltre 30 (trenta) giorni dalla scadenza della fattura oggetto di contestazione </fo:inline>[…]”. Di conseguenza, il Cliente ha il diritto di rivolgere reclami ad Energit in ogni momento e con riferimento anche ad aspetti collegati alla fornitura del Servizio diversi dal pagamento delle fatture.</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">In conformità alle delibere AEEG n. 333/07 e n.164/08, il nuovo articolo 18 delle NCG specifica altresì espressamente:</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">(i) una specifica disciplina che impone ad Energit l’obbligo di rettificare le fatture già pagate dal Cliente e di indennizzare il Cliente - per gli importi ivi meglio specificati - in caso di ritardo nell’adempimento di tale obbligo; e</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">
											<fo:leader line-height="9.7pt"/>
										  </fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">(ii) l’obbligo in capo ad Energit di pagare al Cliente i rimborsi che il Distributore Locale abbia pagato alla stessa Energit per l’eventuale mancato rispetto da parte del Distributore Locale di livelli specifici di qualità commerciale ai sensi della delibera AEEG n. 333/07, oltre ad ogni indennizzo dovuto a causa del mancato rispetto della qualità dei servizi di vendita di energia elettrica da parte di Energit. Tali importi potranno essere accreditati al Cliente attraverso detrazione dall’importo addebitato nella prima fatturazione utile.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">19.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">COMUNICAZIONI</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">TRA LE PARTI</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’articolo in oggetto è stato interessato da modifiche meramente formali, da cui non derivano effetti inerenti ai rispettivi diritti ed obblighi delle Parti ai sensi delle NCG.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">20.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">INSERIMENTO DI</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">REGOLAMENTAZIONI</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">TECNICHE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">E CLAUSOLE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">NEGOZIALI</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 20 del NCG corrisponde all’art 21 delle CG L’articolo in oggetto è stato interessato da modifiche meramente formali, da cui non derivano effetti inerenti ai rispettivi diritti ed obblighi delle Parti ai sensi delle NCG.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">21.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">CESSIONE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DEL CONTRATTO</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">Il nuovo articolo 21 delle NCG  prevede che, in caso di cessione del Contratto, Energit sarà inoltre esonerata da ogni responsabilità nei confronti di del Cliente dalla data di comunicazione della cessione del Contratto da parte di Energit.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">22.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">VARIE</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 22 del NCG corrisponde all’art 20 delle CG L’articolo in oggetto è stato interessato da modifiche meramente formali, da cui non derivano effetti inerenti ai rispettivi diritti ed obblighi delle Parti ai sensi delle NCG.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">23.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">CONTROVERSIE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">E FORO COMPETENTE</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 23 del NCG corrisponde all’art 18 delle CG L’articolo in oggetto è stato interessato da modifiche meramente formali, da cui non derivano effetti inerenti ai rispettivi diritti ed obblighi delle Parti ai sensi delle NCG.</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:list-block provisional-distance-between-starts="14.5pt" provisional-label-separation="3pt">
											<fo:list-item>
											  <fo:list-item-label end-indent="label-end()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">24.</fo:block>
											  </fo:list-item-label>
											  <fo:list-item-body start-indent="body-start()">
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">APPROVAZIONE</fo:block>
												<fo:block xsl:use-attribute-sets="chr.riepilogo8bold">DELLE CLAUSOLE</fo:block>
											  </fo:list-item-body>
											</fo:list-item>
										</fo:list-block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="brd.000" padding-left="3.5pt" padding-right="3.5pt" padding-top="3.5pt" padding-bottom="3.5pt">
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">L’art 24 del NCG corrisponde all’art 22 delle CG A seguito delle modifiche apportate alle NCG, è stato aggiornato l'elenco delle clausole oggetto di doppia sottoscrizione ed accettazione espressa del loro contenuto da parte del Cliente ai sensi e per gli effetti degli Articoli 1341 e 1342 del Codice Civile. Ciò riguarda, in particolare, i seguenti articoli delle NCG:</fo:block>
										<fo:block xsl:use-attribute-sets="chr.riepilogo8" text-align="justify">2.2 (esclusiva), 3 (deleghe, import e CIP6), 4.5 (gestione tecnica della fornitura), 6 (conclusione del contratto), 7 (durata e recesso), 8 (variazione delle specifiche tecniche e delle condizioni del Contratto), 9.4 (aggiornamento del corrispettivo), 10 (fatturazione e pagamenti), 11 (mezzi di garanzia), 12 (sospensione del Servizio), 13 (risoluzione), 14 (limitazione di responsabilità di Energit), 15 (uso improprio del Servizio), 16 (trattamento dati personali D.Lgs. 196/03), 17 (Riservatezza, pubblicità), 18 (reclami, rimborsi), 21 (cessione del Contratto), e 23 (controversie e foro competente).</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>

			</fo:table-body>
		</fo:table>

		<fo:block id="@id{generate-id(.)}"/>
	</fo:flow>
</fo:page-sequence>


</xsl:template>

<!--
******************************************************************************************************************
**                                        Definizione attributi                                                 **
******************************************************************************************************************
-->



<xsl:attribute-set name="chr.riepilogo10">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">10pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="line-height">12.5pt</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="chr.riepilogo10bold">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">10pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
  <xsl:attribute name="font-weight">bold</xsl:attribute>
	<xsl:attribute name="line-height">12.5pt</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="chr.riepilogo8">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="line-height">9.7pt</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="chr.riepilogo8underline">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
  <xsl:attribute name="text-decoration">underline</xsl:attribute>
	<xsl:attribute name="line-height">9.7pt</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="chr.riepilogo8bold">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
  <xsl:attribute name="font-weight">bold</xsl:attribute>
	<xsl:attribute name="line-height">9.7pt</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="chr.riepilogo8italic">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="line-height">9.7pt</xsl:attribute>
  <xsl:attribute name="font-style">italic</xsl:attribute>
</xsl:attribute-set>



</xsl:stylesheet>

