<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:fox="http://xml.apache.org/fop/extensions">
<xsl:output encoding="UTF-8"/>

<!--
**************************************************************************************
**                 Realizzato da Marini Pierluigi                                   **
**                                                                                  **
**                 In questo template vengono visualizzati:                         **
**                 - le condizioni generali di contratto                            **
**                                                                                  **
**************************************************************************************
-->

<!--
******************************************************************************************************************
**                                       Definizione dei parametri                                              **
******************************************************************************************************************
-->
<!--
<xsl:param name="f3_color">#DDDDDD</xsl:param>
<xsl:param name="f2_color">#BBBBBB</xsl:param>
<xsl:param name="f1_color">#FFFFFF</xsl:param>
<xsl:param name="riga_schema">1mm</xsl:param>
-->
<xsl:param name="altezzariga">6.5pt</xsl:param>

<!--
..................................................................................................................
-->

<xsl:template name="CONDIZIONI_C11">



<!--
******************************************************************************************************************
**                                             Prima pagina                                                     **
******************************************************************************************************************
-->



<fo:page-sequence initial-page-number="1"
	force-page-count="no-force" master-reference="pm0_condizioni" orphans="2"
	white-space-collapse="false" widows="2">
	<fo:static-content flow-name="header_condizioni">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed"
			width="100%">
			<fo:table-column column-width="proportional-column-width(100)" />
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row height="3mm">
					<fo:table-cell display-align="center">
						<fo:block start-indent="19pt" text-align="left">
							<fo:inline xsl:use-attribute-sets="chr.condizioni_header">CONDIZIONI GENERALI DI CONTRATTO PER LA FORNITURA DI ENERGIA ELETTRICA E DEI SERVIZI ASSOCIATI - CLIENTI CONSUMER               </fo:inline>
							<fo:inline xsl:use-attribute-sets="chr.condizioni_header2">Allegato B</fo:inline>
						</fo:block>
						
						
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</fo:static-content>

	<fo:static-content flow-name="footer_condizioni">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed"
			width="100%">
			<fo:table-column column-width="proportional-column-width(100)" />
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell display-align="center">
						<fo:block text-align="left" start-indent="19pt">
							<fo:inline xsl:use-attribute-sets="chr.condizioni_footer" font-weight="bold">C1.1_0210                                                                                                             </fo:inline>
							<fo:inline xsl:use-attribute-sets="chr.condizioni_footer"><fo:page-number />/<fo:page-number-citation ref-id="@id1{generate-id(.)}" /></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

		<fo:block xsl:use-attribute-sets="blocco_footer">
		</fo:block>
	</fo:static-content>

	<fo:flow flow-name="body">
		<!-- 
		<xsl:call-template name="CONDIZIONI" />
		 -->
		 
		 
		 <fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
		    <fo:leader line-height="7pt"/>
		</fo:block>
		
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		    <fo:table-column column-width="proportional-column-width(48.5)"/>
			<fo:table-column column-width="proportional-column-width(2.6)"/>
		    <fo:table-column column-width="proportional-column-width(48.9)"/>
		    <fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row height="{$altezzariga}">
					<!--
					******************************************************************************************************************
					**                                             Prima colonna                                                    **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:list-block provisional-distance-between-starts="19pt" provisional-label-separation="3pt">
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">TERMINI E DEFINIZIONI</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">1.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Agli effetti del presente Contratto valgono le seguenti definizioni:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(i)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">AEEG: l’Autorità per l’Energia Elettrica e il Gas.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(ii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Allegati: i seguenti allegati alla Proposta:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(a)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">condizioni particolari di fornitura, recante l’indicazione delle caratteristiche tecniche e della tipologia del Servizio, dei relativi prezzi e delle ulteriori specifiche idonee a fornire al Cliente un’informativa esaustiva ai sensi di legge (Allegato A, di seguito "Condizioni Particolari");</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(b)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">nota informativa relativa alla comunicazione dei dati catastali (Allegato B); e</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(c)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">modulo contenente l’elenco dei Siti (Allegato C, di seguito "Modulo MULTI1").</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(iii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Anno Contrattuale: indica ciascun anno solare nel corso del periodo di validità del Contratto decorrente dalla data di avvio della fornitura del Servizio.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(iv)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Cliente: l’utente, persona fisica, che stipula il Contratto per scopi estranei all'attività imprenditoriale, commerciale, artigianale o professionale eventualmente svolta e rientrante nella categoria di cliente “domestico” ai sensi dell’articolo 2.2 (a) dell’Allegato A della delibera AEEG n. 348/07 (1) e che richiede a Energit la fornitura del Servizio alle condizioni previste dal Contratto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(v)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Condizioni Generali: le presenti condizioni generali di contratto per la regolamentazione del Servizio, facenti parte integrante e sostanziale del Contratto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(vi)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Contratto: congiuntamente, le Condizioni Generali e la Proposta, comprensiva dei relativi Allegati applicabili.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(vii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Contratto di Dispacciamento: il contratto in forza del quale Terna fornisce il servizio di dispacciamento dell’energia elettrica ai Siti.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(viii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Contratto di Trasporto: il contratto in forza del quale il Distributore Locale fornisce il servizio di trasporto dell’energia elettrica ai Siti.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(ix)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Distributore/i Locale/i: il soggetto e/o i soggetti, in caso di più Siti alimentati per il tramite di diversi distributori locali territorialmente competenti, che effettuano il servizio di trasporto dell’energia elettrica ai Siti.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(x)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit: Energ.it S.p.A., con sede in via Edward Jenner, 19/21 09121, Cagliari - Partita IVA 02605060926.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(xi)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Parti: Energit ed il Cliente, congiuntamente ed indistintamente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(xii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">PEC: posta elettronica certificata in conformità a quanto disposto dal Decreto Legislativo 7 marzo 2005 n. 82 e dal D.P.R. 11 febbraio 2005 n. 68 (2).</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(xiii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Prezzo: l’ammontare complessivo derivante dalla somma dei prezzi, costi, oneri indicati nella Proposta - specificamente, nelle Condizioni Particolari - e nelle presenti Condizioni Generali per l’energia elettrica fornita da Energit al Cliente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(xiv)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Proposta: la proposta di fornitura del Servizio, formulata da Energit al Cliente nell’apposito modulo (denominato ”Proposta di Contratto per la fornitura di energia elettrica”), comprensiva degli Allegati di volta in volta richiesti in conformità alle caratteristiche tecniche ed alla tipologia del Servizio.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(xv)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Servizio: la somministrazione di energia elettrica e dei servizi associati da Energit al Cliente ai sensi del presente Contratto, secondo le caratteristiche tecniche ed alla tipologia del Servizio specificati nelle Condizioni Particolari, alle condizioni ed ai termini di cui al Contratto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(xvi)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Servizio Clienti: il servizio di assistenza, di rilascio di informazioni di natura tecnica e/o commerciale sul Servizio e di ricezione dei reclami del Cliente messo a disposizione da Energit presso il numero di telefono gratuito 800.1922.22 (o al diverso numero indicato sul sito internet www.energit.it) nonché presso il medesimo sito internet di Energit www.energit.it.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(xvii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Sito/i: il punto di prelievo dell’energia elettrica del Cliente specificato nella Proposta ovvero, in caso di più Siti, i punti di prelievo individuati nel Modulo MULTI1.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(xviii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Terna: la società Terna - Rete Elettrica Nazionale S.p.A.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(xix)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">In caso di difformità tra le Condizioni Generali e la Proposta prevalgono le disposizioni in quest’ultima contenute.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">OGGETTO E CARATTERISTICHE DEL SERVIZIO</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">2.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Oggetto del Contratto è la fornitura del Servizio da parte di Energit presso il/i Sito/Siti del Cliente, a seconda del caso, alle condizioni di seguito specificate, in conformità alla normativa vigente come di volta in volta modificata od integrata.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">2.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente si impegna ad acquistare ed a prelevare in via esclusiva da Energit i quantitativi di energia elettrica necessaria a soddisfare il fabbisogno di ciascuno dei Siti.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">2.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Gli Allegati indicano in dettaglio le caratteristiche tecniche del Servizio, i corrispettivi facenti parte del Prezzo e le modalità di pagamento del medesimo.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">DELEGHE, IMPORT E CIP6</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">3.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Al fine di offrire condizioni economiche vantaggiose al Cliente, per la fornitura di energia elettrica Energit potrà, tra l'altro, approvvigionarsi di energia partecipando, a titolo esemplificativo e non esaustivo a procedure di assegnazione della capacità di importazione di energia, di capacità di interconnessione con l'estero, di energia CIP6 di cui al D.M. 21.11.2000 (3), etc.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">3.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Le Parti si danno atto e riconoscono che il Prezzo è stato calcolato e definito sulla base e sul presupposto essenziale che i proventi e le utilità eventualmente riconosciute sia dall’AEEG che da Terna in conseguenza della partecipazione alle procedure di cui al precedente articolo 3.1 spettino e siano riconosciuti ad Energit, in qualità di titolare del Contratto di Dispacciamento per i Siti. Pertanto, in considerazione di tali vantaggiose condizioni economiche per la fornitura del Servizio ai sensi del presente Contratto, per tutti i Siti il Cliente si impegna sin d'ora a riconoscere ad Energit eventuali proventi e utilità riconosciuti direttamente al Cliente stesso, ove applicabili, in conseguenza della partecipazione alle procedure di cui al precedente articolo 3.1.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">GESTIONE TECNICA DELLA FORNITURA</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">4.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente ha diritto di richiedere le prestazioni di cui alla delibera AEEG n. 333/07 (4) di competenza del Distributore Locale (quali, a titolo esemplificativo: (a) voltura dei Siti; (b) aumento di potenza del Sito; (c) nuova attivazione; (d) subentro nelle forniture al Sito; (e) spostamento gruppo di misura; (f) spostamento impianto di misurazione; (g) verifica gruppo di misura/tensione; (h) sospensione e interruzione della fornitura; (i) diminuzione di potenza; (j) cambio tensione) esclusivamente per il tramite di Energit. A tali fini, con la sottoscrizione del presente Contratto, il Cliente conferisce ad Energit – che accetta – apposito mandato gratuito senza rappresentanza, ai sensi dell’articolo 1705 del Codice Civile (5). Nonostante quanto precede, il Cliente avrà comunque diritto di rivolgersi direttamente al Distributore Locale per:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(a)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">reclami scritti o richieste scritte di informazioni relativi ai servizi di distribuzione o di misura;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(b)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">richieste di spostamenti di linee ed impianti elettrici che non siano funzionali allo spostamento del gruppo di misura del Cliente; e</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							
							
							
						</fo:list-block>
					</fo:table-cell>
					<!--
					******************************************************************************************************************
					**                                         Colonna di separazione                                               **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni">
							<fo:inline xsl:use-attribute-sets="chr.condizioni"></fo:inline>
						</fo:block>
					</fo:table-cell>
					<!--
					******************************************************************************************************************
					**                                           Seconda colonna                                                    **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:list-block provisional-distance-between-starts="19pt" provisional-label-separation="3pt">
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(c)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">richieste di spostamento di prese non attive, se effettuate da un richiedente diverso dal subentrante.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">4.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit informerà prontamente e terrà debitamente aggiornato il Cliente del suo operato e delle attività svolte dal Distributore Locale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">4.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit trasmette le richieste del Cliente al Distributore Locale, che comunque rimarrà responsabile per l’esecuzione (o la mancata esecuzione) delle prestazioni richieste. Energit cesserà di dare corso alle richieste del Cliente alla data di cessazione del Contratto per qualsivoglia causa.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">4.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">In ogni caso, il Cliente è tenuto al pagamento dei costi richiesti dal Distributore Locale per lo svolgimento delle prestazioni richieste - direttamente o per il tramite di Energit. Qualora Energit provveda al pagamento di tali costi, il Cliente sarà tenuto a rimborsarli ad Energit. In aggiunta a tali costi, Energit addebiterà al Cliente in fattura un corrispettivo fisso a titolo di contributo per le spese di gestione di ciascuna richiesta di prestazione di cui all’articolo 4.1.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">4.5</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente manleva sin d'ora e mantiene Energit indenne da ogni responsabilità, costo, onere, conseguenza negativa, anche nei confronti di terzi, che possano derivare dallo svolgimento del mandato di cui al presente articolo 4, con specifico ma non esaustivo riferimento ad eventuali disservizi o ritardi nella fornitura di cui al Contratto a causa delle attività, relative all'intervento richiesto, poste in essere da parte del Distributore Locale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">4.6</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">In aggiunta e fermo restando quanto sopra previsto, il Cliente potrà richiedere per iscritto ad Energit, per documentate ragioni di carattere commerciale, tecnico, operativo o gestionale proprie del Cliente (ad es. per chiusura di spazi commerciali, trasferimento di sede operativa etc.) di ridurre la fornitura di energia elettrica ai sensi del Contratto con riferimento ad uno o più Siti. Nel caso in cui Energit accetti la richiesta del Cliente, la quantità di energia elettrica somministrata ai sensi del Contratto si intenderà ridotta in misura proporzionale ai consumi di energia elettrica attribuiti a ciascuno dei Siti oggetto della richiesta di riduzione formulata dal Cliente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">4.7</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">A titolo di mero chiarimento, il Cliente riconosce sin d'ora che non rientra nelle fattispecie oggetto di quanto disposto al precedente articolo 4.6 l'avvio di nuove forniture nei confronti di qualsivoglia Sito ovvero l'inserimento di qualsiasi Sito nel contratto di dispacciamento e/o di trasporto di un fornitore diverso da Energit, che pertanto non avranno alcun rilievo ai fini del presente Contratto. In tali casi, così come nel caso di mancata accettazione da parte di Energit della richiesta del Cliente di cui al precedente articolo 4.6, il Cliente rimarrà comunque responsabile nei confronti di Energit anche in caso di mancato prelievo dell'energia per il Sito rilevante, per il pagamento del Prezzo, come espressamente indicato nel Contratto, relativo al Sito di cui si richiede il trasferimento in altro contratto di dispacciamento e/o con riferimento al quale il Cliente intenderebbe avviare e/o avvii una nuova fornitura di energia con un altro fornitore differente da Energit.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">5</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">SERVIZIO DI TRASPORTO E DISPACCIAMENTO DELL’ENERGIA</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">5.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Per la fornitura del Servizio e, in particolare, per somministrare l’energia elettrica, Energit si avvale dei servizi di trasmissione, distribuzione, dispacciamento e misura forniti da Terna e dal Distributore Locale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">5.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Con la sottoscrizione del Contratto il Cliente conferisce ad Energit mandato gratuito e senza rappresentanza, ai sensi dell’articolo 1705 del Codice Civile, affinché provveda a stipulare con il Distributore Locale il Contratto di Trasporto e con Terna il Contratto di Dispacciamento. Energit provvederà ad addebitare al Cliente i corrispettivi previsti da tali contratti.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">CONCLUSIONE DEL CONTRATTO ED AVVIO DELLE FORNITURE</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">La Proposta si intende irrevocabile, ai sensi e per gli effetti dell’articolo 1329 del Codice Civile (6) per il periodo di 60 (sessanta) giorni decorrenti dalla trasmissione della Proposta stessa al Cliente. Scaduto tale temine senza conclusione del Contratto ai sensi del successivo articolo 6.2, la stessa si intende automaticamente revocata e priva di qualsiasi effetto; nessuna responsabilità, di tipo contrattuale o extra-contrattuale, sorgerà in capo ad Energit come conseguenza della mancata accettazione della Proposta da parte del Cliente, il quale pertanto non avrà nulla a che pretendere nei confronti di Energit in tale ipotesi.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Contratto si conclude per adesione del Cliente alla Proposta, anche ai sensi della parte III, titolo III, capo I, sezione II del Decreto Legislativo n. 206/2005 (7), tramite compilazione e sottoscrizione del Cliente stesso della Proposta debitamente completata in ogni sua parte, datata e sottoscritta, munita degli Allegati applicabili alla tipologia del Servizio indicata nella Proposta stessa. Il Contratto si intende concluso nel momento in cui Energit riceve l'adesione del Cliente alla Proposta.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Ai fini della conclusione del Contratto il Cliente trasmette ad Energit la Proposta debitamente completata in ogni sua parte, datata e sottoscritta, munita degli Allegati applicabili alla tipologia del Servizio indicata nella Proposta stessa tramite (a) raccomandata A/R; (b) telefax; (c) PEC; o (d) e-mail con conferma di avvenuto ricevimento all’indirizzo di posta elettronica di Energit di cui al successivo articolo 19. La Proposta si intenderà comunque legittimamente trasmessa ad Energit, ed il Contratto concluso, nel caso in cui la medesima Proposta, debitamente completata in ogni sua parte, datata e sottoscritta ai sensi del presente articolo 6.2, sia consegnata in originale a personale commerciale, agenti, rappresentanti e/o procuratori di Energit muniti dei poteri necessari a tali fini.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente può richiedere la trasmissione della Proposta mediante richiesta telefonica al Servizio Clienti Energit di cui al successivo articolo 19 o tramite il sito internet www.energit.it, restando inteso che in tali ultime ipotesi:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.4.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit trasmetterà la Proposta al Cliente a mezzo posta o telefax entro 10 (dieci) giorni dalla richiesta del Cliente; e</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.4.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">il Cliente potrà comunque, entro e non oltre 25 (venticinque) giorni dalla richiesta ed a pena di decadenza, concludere il Contratto ai sensi del precedente articolo 6.2.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.5</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente, con la conclusione del Contratto ai sensi del precedente articolo 6.2, dichiara e garantisce:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.5.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">l'esattezza e la veridicità di tutti i dati ivi indicati, impegnandosi in tal senso anche con riguardo ad ogni altra comunicazione successiva;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.5.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">di avere la titolarità o, a qualsiasi altro titolo, il legittimo diritto di utilizzare gli impianti presso i Siti necessari per usufruire del Servizio;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.5.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">di aver risolto, alla data di attivazione del Servizio, eventuali precedenti contratti di somministrazione di energia elettrica presso i Siti e di non avere obbligazioni insolute relative a precedenti contratti di somministrazione, trasporto e dispacciamento presso i Siti. Diversamente, con la sottoscrizione del Contratto, il Cliente conferisce ad Energit mandato gratuito con rappresentanza, ai sensi dell’articolo 1704 (8) del Codice Civile, al fine di sottoscrivere in suo nome e per suo conto la comunicazione di recesso dal contratto di somministrazione di energia elettrica con il precedente fornitore;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
						</fo:list-block>
					</fo:table-cell>
				</fo:table-row>
		    </fo:table-body>
		</fo:table>
		<!--
		..................................................................................................................
		-->        
		
		
		
		
		
		
		
		<fo:block break-before="page">
		</fo:block>
		
		
		
		
		
		
		<!--
		******************************************************************************************************************
		**                                            Seconda pagina                                                    **
		******************************************************************************************************************
		-->
		
		<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
		    <fo:leader line-height="7pt"/>
		</fo:block>
		
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		    <fo:table-column column-width="proportional-column-width(48.5)"/>
			<fo:table-column column-width="proportional-column-width(2.6)"/>
		    <fo:table-column column-width="proportional-column-width(48.9)"/>
		    <fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row height="{$altezzariga}">
					<!--
					******************************************************************************************************************
					**                                             Prima colonna                                                    **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:list-block provisional-distance-between-starts="19pt" provisional-label-separation="3pt">
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.5.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">di essere un cliente finale disalimentabile ai sensi e per gli effetti della delibera AEEG n. 04/08 (9).</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.6</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente è responsabile della veridicità delle informazioni fornite e si impegna a tenere indenne Energit da ogni pretesa, richiesta o azione di terzi derivante o comunque correlata alle dichiarazioni rilasciate ai fini della conclusione del Contratto e nelle successive comunicazioni.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.7</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Con la conclusione del Contratto il Cliente si impegna a prestare tutta la necessaria collaborazione ed a sottoscrivere tutta la documentazione necessaria e/o utile - ivi incluse le condizioni tecniche del Contratto di Trasporto fornite dal Distributore Locale - all’attivazione e gestione del Servizio.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.8</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Qualora il Contratto sia concluso ai sensi del precedente articolo 6.2, Energit, ricevuta tutta la documentazione necessaria per l’avvio del Servizio da parte dal Cliente - incluso il consenso al trattamento dei dati e, qualora richiesto, il deposito cauzionale ai sensi del successivo articolo 11 - provvederà ad inoltrare richiesta di attivazione del Servizio al Distributore Locale successivamente alla stipula e/o all'effettivo inserimento dei Siti del Cliente nel Contratto di Trasporto e nel Contratto di Dispacciamento.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.9</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Resta inteso tra le Parti che la fornitura del Servizio è in ogni caso condizionata:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.9.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">all’esito favorevole delle verifiche di fattibilità tecnica ed affidabilità finanziaria del Cliente svolte da parte di Energit;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.9.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">all’attivazione da parte del Distributore Locale del servizio di trasmissione e distribuzione di energia elettrica presso i Siti;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.9.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">alla circostanza che i Siti non risultino sospesi per morosità al momento della richiesta di attivazione del servizio di trasmissione e distribuzione; e</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">6.9.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">al ricevimento di tutta la documentazione necessaria per l’avvio del Servizio che il Cliente deve trasmettere ad Energit a tal fine; ed avverrà in conformità alle condizioni ed ai termini indicati nel Contratto di Trasporto e nel Contratto di Dispacciamento e, comunque, nel rispetto della normativa vigente e dei tempi tecnici richiesti dal Distributore Locale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.10</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">A seguito dello svolgimento delle attività di cui al precedente articolo 6.9, Energit comunica al Cliente, con almeno 5 (cinque) giorni di preavviso, la data di avvio del Servizio mediante apposita comunicazione in forma scritta.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.11</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Resta inteso tra le Parti che Energit non sarà in alcun caso ritenuta responsabile per la ritardata attivazione del Servizio, qualora ciò dipenda da motivi ad essa non imputabili. La mancata attivazione del Servizio da parte del Distributore Locale entro 90 (novanta) giorni dalla data in cui Energit gli inoltra la richiesta di attivazione del Servizio presso il/i Sito/i del Cliente costituirà condizione risolutiva del Contratto e pertanto le obbligazioni delle Parti si estingueranno automaticamente ai sensi dell’articolo 1353 del Codice Civile (10). Tale condizione risolutiva è posta nell'esclusivo interesse di Energit che potrà liberamente decidere di avvalersene o meno.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">6.12</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente manleva e mantiene indenne sin d’ora Energit da ogni conseguenza negativa, spesa, costo, onere, danno o responsabilità che possa derivare, in capo al Cliente od a terzi, dalla risoluzione del presente Contratto nel caso in cui si verifichi la condizione di cui al precedente articolo 6.11 ed Energit dichiari di avvalersi di quanto ivi disposto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">7</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">DURATA DEL CONTRATTO E RECESSO</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">7.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Salvo quanto diversamente ed espressamente previsto nella Proposta e nelle presenti Condizioni Generali, il Contratto è valido ed efficace dalla sua conclusione ai sensi del precedente articolo 6.2 e rimarrà valido ed efficace per l’intero periodo di 12 (dodici) mesi successivi alla data di effettivo inizio della fornitura del Servizio di cui al precedente articolo 6.10. A tale scadenza il Contratto si considererà rinnovato tacitamente per ulteriori periodi di 12 (dodici) mesi ciascuno, successivi alla data di scadenza originaria e/o prorogata, salvo disdetta scritta di una delle Parti da inviarsi con un preavviso di almeno 45 (quarantacinque) giorni antecedenti la data rilevante di scadenza del Contratto, iniziale e/o prorogata, a mezzo telefax, lettera raccomandata A/R o PEC. In caso di disdetta di una delle Parti la fornitura del Servizio cesserà alle ore 00:00 del primo giorno successivo alla scadenza dei 12 (dodici) mesi dalla data di effettivo inizio della fornitura del Servizio.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">7.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">In ogni caso, il Cliente avrà il diritto di recedere dal Contratto senza alcuna penalità e senza specificarne il motivo inviando a Energit comunicazione scritta a mezzo raccomandata con avviso di ricevimento entro 10 (dieci) giorni dalla conclusione del Contratto ai sensi del precedente articolo 6.2. La comunicazione di recesso potrà altresì essere inviata agli indirizzi specificati al successivo articolo 19, entro gli stessi termini, anche mediante PEC, telefax o e-mail con conferma di avvenuto ricevimento, a condizione che sia confermata con raccomandata con avviso di ricevimento entro le 48 (quarantotto) ore successive.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">7.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Fermo restando quanto previsto all’articolo 7.1, il Clientie, ai sensi della delibera AEEG n. 144/07 (11), avrà diritto di recedere dal Contratto in qualsiasi momento con un preavviso di 1 (un) mese a mezzo telefax, lettera raccomandata A/R o PEC. Tale preavviso decorrerà dal primo giorno del mese successivo a quello in cui Energit abbia ricevuto la comunicazione di recesso.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">7.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit avrà diritto di recedere dal Contratto in qualsiasi momento con un preavviso di 6 (sei) mesi, a mezzo telefax, lettera raccomandata A/R o PEC.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">7.5</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">La cessazione per qualsiasi motivo del Contratto non esonererà il Cliente dall’obbligo del pagamento del Prezzo sino al momento dell’effettiva disattivazione del Servizio (che avverrà senza vincoli temporali o ritardi non giustificati), comprensivo di eventuali canoni mensili relativi al periodo di fatturazione in corso a tale momento e/o di eventuali conguagli dovuti dal Cliente in considerazione dei dati di misura del consumo effettivo del Cliente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">7.6</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Nel caso in cui il Cliente non rispetti i termini e le condizioni previsti dal presente articolo 7 per l'esercizio del diritto di recesso e, ciononostante, tale recesso risulti efficace e venga avviata una fornitura da parte di un terzo fornitore presso il/i Sito/i, il Cliente sarà tenuto a corrispondere ad Energit un corrispettivo per l'esercizio illegittimo del diritto di recesso pari ad Euro 0,06 per ogni KWh fatturato al Cliente nei tre mesi precedenti il mese in cui è cessato il Servizio che sarà oggetto di fatturazione da parte di Energit e di pagamento da parte del Cliente in conformità a quanto previsto dal successivo articolo 10.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">8</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">VARIAZIONI DELLE SPECIFICHE TECNICHE E DELLE CONDIZIONI DEL CONTRATTO</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">8.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit si riserva il diritto di variare in qualsiasi momento, per giustificati motivi di natura tecnica, commerciale o gestionale, le specifiche tecniche del Servizio e/o le condizioni di fornitura, dandone comunicazione scritta al Cliente secondo le modalità di cui al precedente articolo 6.3 e/o tramite apposita comunicazione scritta trasmessa unitamente alla fattura con preavviso non inferiore a 60 (sessanta) giorni dalla data di decorrenza di tali variazioni, informando altresì il Cliente del diritto di recedere dal</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							
							
						</fo:list-block>
					</fo:table-cell>
					<!--
					******************************************************************************************************************
					**                                         Colonna di separazione                                               **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni">
							<fo:inline xsl:use-attribute-sets="chr.condizioni"></fo:inline>
						</fo:block>
					</fo:table-cell>
					<!--
					******************************************************************************************************************
					**                                           Seconda colonna                                                    **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:list-block provisional-distance-between-starts="19pt" provisional-label-separation="3pt">
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold"></fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Contratto, qualora non intenda accettare le nuove condizioni. Qualora intenda recedere, il Cliente dovrà trasmettere ad Energit apposita comunicazione scritta nel termine di 15 (quindici) giorni dalla data di comunicazione delle variazioni da parte di Energit. Tale recesso potrà essere esercitato senza penali e sarà efficace non appena tecnicamente possibile, tenuto conto della regolamentazione del mercato e dei tempi tecnici richiesti dal Distributore Locale. In caso di recesso da parte del Cliente, pertanto, non si applicheranno le nuove condizioni e termini.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">8.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente può richiedere ad Energit, inviando apposita comunicazione al Servizio Clienti, via telefax, PEC o via e-mail, trasmettendo altresì conferma scritta ad Energit a mezzo raccomandata A/R entro 10 (dieci) giorni dalla richiesta, di modificare le modalità e le specifiche di fornitura del Servizio indicate nella Proposta.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">8.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit non ha alcun obbligo ma avrà facoltà di accogliere le richieste del Cliente ritenute compatibili con il Servizio, comunicando per iscritto al medesimo la data di inizio del nuovo tipo di fornitura ed i relativi costi, che verranno imputati al Cliente nella prima fattura successiva all'erogazione, così modificata, del Servizio. Resta inteso che Energit potrà subordinare la propria accettazione delle modifiche al Servizio richieste dal Cliente ad apposito adeguamento del Prezzo e/o del deposito cauzionale di cui al successivo articolo 11.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">9</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">PREZZO</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">9.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente, per ciascun Servizio richiesto, si impegna a corrispondere a Energit il Prezzo corrispondente, come eventualmente modificato ai sensi del precedente articolo 8 o del successivo articolo 9.4.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">9.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il corrispettivo indicato nelle Condizioni Particolari deve intendersi al netto dei seguenti ammontari:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.linea">-</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">ammontari dovuti a titolo di perdite di rete;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.linea">-</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">corrispettivi per i servizi di trasmissione, distribuzione e misura dell’energia elettrica, di cui al precedente articolo 5;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.linea">-</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">corrispettivi per la copertura degli oneri di sistema (componenti A, UC e MCT) relativi al mercato libero stabiliti dalla delibera AEEG n. 348/07 e sue successive modifiche o integrazioni;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.linea">-</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">corrispettivi per i servizi di dispacciamento, delle componenti a copertura dei costi connessi alla differenza tra perdite effettive e perdite standard nelle reti, ad esclusione di eventuali componenti di tali corrispettivi che siano di spettanza di Energit, tra cui, a titolo esemplificativo, quelli derivanti dalla partecipazione alle aste di cui al precedente articolo 3;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.linea">-</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">eventuali oneri che si rendessero necessari per l’avvio della somministrazione e/o per adeguamenti in corso di somministrazione (quali, ad esempio, volture, aumenti di potenza, verifiche misuratori etc.) e delle altre attività previste dalla delibera AEEG n. 333/07 e s.m.i. e/o per sospensioni di somministrazione che saranno fatturati dal Distributore Locale e/o da Terna ed interamente posti a carico del Cliente con separata evidenza in fattura;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.linea">-</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">ogni ulteriore corrispettivo a titolo di conguaglio, come eventualmente e di volta in volta applicabile ai sensi della normativa vigente quale, a titolo meramente esemplificativo e non esaustivo, i corrispettivi di conguaglio compensativo disciplinati dalla delibera AEEG n. 157/08 (12); e ulteriori oneri e costi che saranno eventualmente introdotti dalla AEEG o da altre autorità competenti;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.linea">-</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">che saranno pertanto fatturati da Energit in aggiunta a suddetto corrispettivo come parte integrante del Prezzo.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">9.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">A tutti gli importi fatturati ai sensi del presente Contratto sarà applicata l’IVA e ogni altro onere fiscale dovuto per legge.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">9.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Qualora le condizioni di fornitura del Servizio di cui al Contratto (con specifico riferimento a quanto indicato dalle Condizioni Particolari) prevedano un meccanismo di indicizzazione o di adeguamento automatico del Prezzo e/o di componenti dello stesso, Energit ne informerà il Cliente nella prima fattura successiva all’applicazione di tali meccanismi, individuando le variazioni così apportate al Prezzo e/o alle componenti dello stesso.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">9.5</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Almeno 60 (sessanta) giorni prima del termine di ciascun Anno Contrattuale, Energit comunicherà tramite apposita comunicazione scritta trasmessa unitamente alla fattura le eventuali variazioni del Prezzo e/o di componenti dello stesso per l'Anno Contrattuale successivo. Nel caso in cui il Cliente non intenda accettare le nuove condizioni per l’Anno Contrattuale successivo, avrà diritto di recedere dal Contratto con effetto alla scadenza dell’Anno Contrattuale in corso alla data della comunicazione della variazione, senza alcun onere aggiuntivo, ma fatto salvo quanto previsto dai precedenti articoli 7.5 e 7.6, mediante comunicazione scritta che dovrà pervenire ad Energit entro 45 (quarantacinque) giorni prima della scadenza dell’Anno Contrattuale in corso.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">10</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">FATTURAZIONE E PAGAMENTI</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">10.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Le fatture saranno emesse con cadenza mensile o bimestrale, fatte salve diverse disposizioni normative o regolamentari, sulla base dei consumi comunicati ad Energit dal Distributore Locale alla rilevazione degli stessi. Energit potrà emettere le fatture di importo inferiore a euro 30 (trenta) con diversa cadenza.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">10.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit potrà emettere fatture anche sulla base di consumi stimati, e salvo conguaglio in conformità ai dati di consumo resi disponibili dal Distributore Locale, sulla base dei consumi registrati nei corrispondenti periodi dell’Anno Contrattuale precedente o rilevati da eventuali apparecchiature di misura ausiliarie. In caso di nuovi clienti, Energit potrà stimare i consumi sulla base della tipologia del Cliente e delle informazioni che quest’ultimo abbia fornito al momento della sottoscrizione del Contratto. Energit indicherà nei documenti di fatturazione le modalità di calcolo dei consumi stimati. Il Cliente potrà richiedere che tali modalità di calcolo e/o gli importi dei consumi stimati siano modificati fornendo ad Energit copia dei documenti di fatturazione del precedente fornitore relativi ai corrispondenti periodi dell’anno precedente ovvero fornendo tutte le informazioni necessarie.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">10.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">In caso di conguagli ai sensi del precedente articolo 10.2, Energit provvederà ad emettere apposita fattura o nota di credito, a seconda dei casi, nei confronti o a favore del Cliente non prima di 180 (centottanta) giorni dalla cessazione del Contratto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">10.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il pagamento dovrà essere effettuato dal Cliente con le modalità e nei termini previsti nelle fatture stesse. In ogni caso, il termine di pagamento delle fatture non potrà essere inferiore a 20 (venti) giorni dalla data di emissione delle fatture stesse.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">10.5</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Le fatture si intendono validamente consegnate anche se inoltrate a mezzo telefax o posta elettronica. A tal fine farà fede il documento di “esito invio” rilasciato dal sistema di inoltro utilizzato.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">10.6</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente si impegna ad effettuare per intero il pagamento delle fatture emesse da Energit. Tale pagamento non potrà essere differito o ridotto neppure in caso di contestazione o reclami da parte del Cliente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
						</fo:list-block>
					</fo:table-cell>
				</fo:table-row>
		    
		    
		    
		</fo:table-body>
		</fo:table>    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		
		
		
		<fo:block break-before="page">
		</fo:block>
		
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		<!--
		******************************************************************************************************************
		**                                             Terza  pagina                                                    **
		******************************************************************************************************************
		-->
		
		<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
		    <fo:leader line-height="7pt"/>
		</fo:block>
		
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		    <fo:table-column column-width="proportional-column-width(48.5)"/>
			<fo:table-column column-width="proportional-column-width(2.6)"/>
		    <fo:table-column column-width="proportional-column-width(48.9)"/>
		    <fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row height="{$altezzariga}">
					<!--
					******************************************************************************************************************
					**                                             Prima colonna                                                    **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:list-block provisional-distance-between-starts="19pt" provisional-label-separation="3pt">
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">10.7</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Fatto salvo quanto previsto dal successivo articolo 12, in caso di ritardato pagamento (anche parziale) di qualsiasi importo dovuto ad Energit, dal giorno successivo alla scadenza del termine previsto per il pagamento decorreranno a carico del Cliente interessi di mora, applicati, disciplinati e determinati in conformità a quanto previsto dal Decreto Legislativo n. 231/2002 (13) e calcolati in base ad un tasso di interesse pari a Euribor 6 mesi maggiorato 5% (cinque per cento), fatto comunque salvo il diritto di Energit al risarcimento del maggior danno.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">10.8</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit indicherà in fattura in modo dettagliato gli eventuali ritardi nel pagamento da parte del Cliente e l'ammontare dei relativi interessi di mora ivi addebitati in aggiunta ad eventuali costi, oneri o spese imputabili al Cliente in conseguenza del proprio inadempimento, quali, a titolo esemplificativo, spese di sollecito e di gestione amministrativa e/o legale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">11</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">DEPOSITO CAUZIONALE</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">11.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Successivamente al ricevimento della Proposta, e prima di dare attivazione al Servizio, Energit si riserva il diritto di richiedere al Cliente un deposito cauzionale pari al valore economico stimato da Energit per 5 (cinque) mesi di fornitura del Servizio o altro minor valore espressamente indicato e richiesto da Energit, che la stessa, unilateralmente, ritenga adeguato sulla base dei consumi stimati del Cliente, delle modalità di pagamento da parte dello stesso del Prezzo e delle analisi condotte sull'affidabilità finanziaria del Cliente, che il Cliente avrà l’obbligo di costituire entro il termine di volta in volta indicato da Energit e che sarà condizione per la richiesta di attivazione del Servizio da parte di Energit al Distributore Locale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">11.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Su richiesta di Energit, il Cliente si obbliga ad adeguare l‘importo del deposito cauzionale in caso di irregolarità nell’uso del Servizio, ritardi nei pagamenti e/o aumento nei consumi di energia elettrica, ferma l’applicazione di ogni altro rimedio previsto dal presente Contratto e dalla legge. In caso di mancato adeguamento dell‘importo del deposito cauzionale entro 10 (dieci) giorni dalla richiesta di Energit, Energit si riserva il diritto di addebitare al Cliente, nella prima fattura utile, l’ammontare corrispondente all’adeguamento richiesto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">11.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente si obbliga altresì a reintegrare il deposito cauzionale in caso di avvenuta escussione dello stesso da parte di Energit, con addebito nella prima fattura utile.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">11.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">All'atto della cessazione dell'efficacia del Contratto, per qualsivoglia causa, e a seguito del totale versamento da parte del Cliente di tutte le somme dovute ad Energit e a terzi in base al presente Contratto, Energit restituirà al Cliente le somme eventualmente versate a titolo di deposito cauzionale oltre agli interessi sulle stesse maturati, che saranno calcolati sulla base del tasso di interesse legale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">11.5</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Prima di procedere alla restituzione delle indicate somme, Energit potrà comunque rivalersi su di esse per il soddisfacimento di ogni sua pretesa creditoria o risarcitoria nei confronti del Cliente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">12</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">SOSPENSIONI DEL SERVIZIO</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">12.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">In caso di ritardato pagamento (anche parziale) di qualsiasi importo dovuto ad Energit, decorsi 5 (cinque) giorni dalla scadenza del termine indicato nella fattura per il pagamento, Energit invierà al Cliente ai sensi della delibera AEEG n. 04/08 una comunicazione scritta a mezzo raccomandata in cui indicherà:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(i)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">il termine, in ogni caso non inferiore a 5 (cinque) giorni, entro cui il Cliente sarà tenuto ad effettuare il pagamento di quanto dovuto ad Energit;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(ii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">le modalità con cui il Cliente può comunicare ad Energit l’avvenuto pagamento; e</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(iii)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">che, in caso di omesso pagamento entro il termine di cui alla lettera (i) che precede, Energit, fatto salvo il diritto di risolvere il Contratto ai sensi dell’articolo 1456 del Codice Civile (14), avrà diritto di:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(a)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">escutere il deposito cauzionale di cui al precedente articolo 11; e/o</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(b)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">qualora l’importo dell’omesso pagamento sia di valore superiore all’importo del deposito cauzionale di cui al precedente articolo 11, di ridurre la fornitura di energia elettrica ad un livello pari al 15% (quindici per cento) della potenza disponibile, ove sussistano le condizioni tecniche del misuratore; e</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(c)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">decorsi 10 (dieci) giorni dalla riduzione della potenza, di richiedere al Distributore Locale di sospendere la fornitura. Resta inteso che l’eventuale riattivazione della fornitura a seguito di sospensione per morosità avviene con le modalità previste all’articolo 68 della delibera AEEG n. 333/07.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">12.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit avrà altresì il diritto di sospendere o, a seconda del caso, di non dare avvio al Servizio, previa comunicazione al Cliente, nei seguenti casi:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">12.2.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">mancata ricezione del deposito cauzionale di cui all’articolo 11.1 che precede, e nel caso in cui il Cliente non provveda a reintegrarne l’importo ai sensi dell’articolo 11.3; e/o</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">12.2.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">consumo anomalo od uso improprio del Servizio da parte del Cliente, ai sensi del successivo articolo 15;</fo:block>
									<fo:block xsl:use-attribute-sets="chr.condizioni">senza che ciò comporti in alcun modo un suo inadempimento al Contratto e salva in ogni caso ogni altra forma di tutela prevista dalla legge e il diritto al risarcimento dei danni.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">13</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">RISOLUZIONE</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">13.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">In aggiunta ai rimedi di cui al precedente articolo 12, Energit avrà diritto di risolvere il Contratto ai sensi dell'articolo 1456 del Codice Civile, dandone comunicazione al Cliente in forma scritta e con conseguente interruzione del Servizio, in caso di inadempimento del Cliente a ciascuna delle obbligazioni di cui agli articoli: 6.6 (dichiarazioni e garanzie), 10.4 (mancato pagamento di una fattura nel termine indicato in fattura), 11 (mezzi di garanzia), 12.1 (omesso pagamento nel termine di cui all'articolo 12.1, lett. (i)) 15 (uso improprio del Servizio), 17 (riservatezza) e 21 (cessione del Contratto) del Contratto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">13.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Resta inteso che Energit avrà diritto di inviare al Cliente una diffida ad adempiere ai sensi dell’articolo 1454 (15) del Codice Civile per ogni altro inadempimento del Cliente agli obblighi assunti ai sensi del Contratto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">14</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">LIMITAZIONE DI RESPONSABILITÀ DI ENERGIT - OBBLIGO DEL CLIENTE DI LIMITARE GLI EVENTUALI DANNI</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">14.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit non sarà responsabile dei danni causati dall’energia elettrica a valle del Sito, ancorché originati a monte dello stesso, salvo che in quest’ultima ipotesi la causa del danno sia esclusivamente imputabile a Energit. Con riferimento al precedente articolo 5, Energit non sarà ritenuta in nessun caso responsabile nel caso di ritardo o mancata somministrazione dell’energia elettrica derivante da eventuali disfunzioni o disservizi della rete di trasmissione e/o distribuzione o per eventuali inadempienze, anche parziali, dovute a fatti o atti di Terna e/o del Distributore Locale e/o di altro soggetto competente in esecuzione dei contratti di cui all’articolo 5 stipulati per l’esecuzione del presente Contratto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
		          
							
						</fo:list-block>
					</fo:table-cell>
					<!--
					******************************************************************************************************************
					**                                         Colonna di separazione                                               **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni">
							<fo:inline xsl:use-attribute-sets="chr.condizioni"></fo:inline>
						</fo:block>
					</fo:table-cell>
					<!--
					******************************************************************************************************************
					**                                           Seconda colonna                                                    **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:list-block provisional-distance-between-starts="19pt" provisional-label-separation="3pt">
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">14.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit non è e non sarà responsabile per ritardi, malfunzionamenti, sospensioni e/o interruzioni nell’erogazione del Servizio imputabili a cause di forza maggiore o caso fortuito quali guasti, sovraccarichi, interruzioni ecc. delle linee elettriche, o interventi non autorizzati da Energit o manomissioni, volontari o involontari, del Cliente e/o di terzi (ivi inclusi l’errata utilizzazione del Servizio da parte del Cliente ed il cattivo funzionamento degli apparecchi ed elaboratori elettronici utilizzati dal medesimo) che pregiudichino il funzionamento del Servizio messi a disposizione del Cliente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">14.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Fatto salvo quanto previsto all’articolo 33, comma 2, lett. a, del Decreto Legislativo 6 settembre 2005, n. 206, in ogni caso di malfunzionamenti, sospensioni e/o interruzioni nell’erogazione del Servizio la responsabilità di Energit sarà limitata ai casi di dolo o colpa grave.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">14.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit non sarà inoltre ritenuta responsabile per eventuali ritardi e/o errori di misura e/o di fatturazione da parte di Terna, del Distributore Locale in esecuzione del Contratto di Trasporto e/o del Contratto di Dispacciamento.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">14.5</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Fermo restando quanto previsto ai precedenti articoli da 14.1 a 14.4 inclusi, il Cliente si impegna a dare tempestiva comunicazione a Energit dell’eventuale malfunzionamento e/o interruzione nell’erogazione del Servizio, attivandosi nel contempo per ridurre l’eventuale danno ulteriore, e di variazioni significative delle condizioni di consumo allo scopo di consentire ad Energit adeguata pianificazione e programmazione degli approvvigionamenti di energia elettrica.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">15</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">USO IMPROPRIO DEL SERVIZIO</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">15.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">È vietato al Cliente qualsiasi uso del Servizio in difformità dalle previsioni del Contratto o delle leggi e dei regolamenti, ovvero qualsiasi uso del Servizio che possa produrre danni o turbative a Energit o a terzi, o violi comunque leggi o regolamenti. Sono altresì vietati al Cliente la rivendita o cessione dell’energia elettrica fornita ai sensi del presente Contratto, il prelievo dell’energia elettrica al di fuori dei Siti. Il Cliente sarà inoltre responsabile di qualsiasi uso improprio o fraudolento del Servizio, anche ai sensi del precedente articolo 15.1, e si obbliga a manlevare e tenere Energit indenne da ogni conseguenza negativa, pretesa, azione, eccezione, o procedimento che dovesse essere fatto valere da terzi nei confronti di Energit in connessione con il Servizio, fermo il diritto di Energit di sospendere, in qualsiasi momento e senza preavviso, l’erogazione del Servizio, qualora ritenga che detto uso improprio produca o possa produrre danni o turbative a Energit e/o a terzi o violi comunque leggi o regolamenti, salva in ogni caso ogni altra forma di tutela prevista dalla legge ed il diritto al risarcimento dei danni.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">16</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">TRATTAMENTO DATI PERSONALI DECRETO LEGISLATIVO N. 196/03 (16)</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">16.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit attesta che i dati personali forniti dal Cliente verranno trattati da Energit nel pieno rispetto della legge vigente sulla tutela e sul trattamento dei dati personali e di ogni altra legge applicabile. Presso Energit saranno raccolti e archiviati i dati anagrafici e commerciali relativi al Contratto per adempiere agli obblighi fiscali e tributari. I dati vengono inseriti in un elenco in cui sono raccolti i dati anagrafici dei contraenti. La natura del conferimento è necessaria per consentire l’adempimento degli obblighi di legge derivanti dalla messa in atto del Servizio richiesto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">16.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il mancato consenso al trattamento dei dati anagrafici e fiscali può comportare la mancata erogazione del Servizio e dunque la mancata esecuzione del Contratto e/o la mancata prosecuzione del rapporto da parte di Energit.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">16.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">In relazione al trattamento dei dati personali l’interessato ha diritto a quanto previsto ai sensi dell’articolo 7 D.Lgs. 196/03, di seguito riportato:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">16.3.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">l’interessato ha diritto di ottenere la conferma dell’esistenza o meno di dati personali che lo riguardano, anche se non ancora registrati, e la loro comunicazione in forma intelligibile.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">16.3.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">l’interessato ha diritto di ottenere l’indicazione:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(a)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">dell’origine dei dati personali;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(b)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">delle finalità e modalità del trattamento;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(c)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">della logica applicata in caso di trattamento effettuato con l’ausilio di strumenti elettronici;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(d)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">degli estremi identificativi del titolare, dei responsabili e del rappresentante designato ai sensi dell’articolo 5, comma 2;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(e)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di rappresentante designato nel territorio dello Stato, di responsabili o incaricati.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">16.3.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">L’interessato ha diritto di ottenere:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(a)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">l’aggiornamento, la rettificazione ovvero, quando vi ha interesse, l’integrazione dei dati;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(b)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli di cui non è necessaria la conservazione in relazione agli scopi per i quali i dati sono stati raccolti o successivamente trattati;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(c)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">l’attestazione che le operazioni di cui alle lettere a) e b) sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o diffusi, eccettuato il caso in cui tale adempimento si rivela impossibile o comporta un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni_piccole">16.3.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">L’interessato ha diritto di opporsi, in tutto o in parte:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(a)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">per motivi legittimi al trattamento dei dati personali che lo riguardano, ancorché pertinenti allo scopo della raccolta;</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">(b)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">al trattamento di dati personali che lo riguardano a fini di invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">17</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">RISERVATEZZA, PUBBLICITÀ</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">17.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Le Parti manterranno riservate e confidenziali le informazioni confidenziali di natura tecnica, commerciale o industriale delle quali siano, comunque, venuti a conoscenza in ragione del presente Contratto. Le Parti si impegnano altresì a non divulgare tali informazioni a soggetti terzi, sia durante la vigenza del Contratto che per i successivi cinque anni. Energit potrà comunque citare il Cliente, l’area merceologica e l’area territoriale in cui esso opera nella propria pubblicità o, comunque, a fini promozionali. Parimenti il Cliente potrà rendere noto a terzi che le loro forniture sono assicurate da Energit, mantenendo però riservati i particolari tecnici, economici e commerciali.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">18</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">RECLAMI, RIMBORSI</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">18.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Ogni reclamo del Cliente relativo al Contratto e/o alla fornitura del Servizio potrà essere inviato a Energit senza oneri aggiuntivi, per telefono, per iscritto, a mezzo telefax o per via telematica ai sensi del successivo articolo 19. Energit procederà all’esame dei reclami secondo quanto indicato sul sito www.energit.it, nel rispetto di quanto previsto dalla delibera AEEG n. 333/07 e dalla delibera AEEG n. 164/08 (17), nonché della normativa di volta in volta applicabile.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
		          
		          
		          
						</fo:list-block>
					</fo:table-cell>
				</fo:table-row>    
		    
		    
		    </fo:table-body>
		</fo:table>
		
		
		
		
		
		
		
		
		
		
		
		
		<fo:block break-before="page">
		</fo:block>
		
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		<!--
		******************************************************************************************************************
		**                                             Quarta pagina                                                    **
		******************************************************************************************************************
		-->
		
		<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
		    <fo:leader line-height="7pt"/>
		</fo:block>
		
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		    <fo:table-column column-width="proportional-column-width(48.5)"/>
			<fo:table-column column-width="proportional-column-width(2.6)"/>
		    <fo:table-column column-width="proportional-column-width(48.9)"/>
		    <fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row height="{$altezzariga}">
					<!--
					******************************************************************************************************************
					**                                             Prima colonna                                                    **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:list-block provisional-distance-between-starts="19pt" provisional-label-separation="3pt">
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">18.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Ai sensi dell’articolo 69 della delibera AEEG n. 333/07 e dalla delibera AEEG n. 164/08, nonché della normativa di volta in volta applicabile, Energit è tenuta al rispetto del livello specifico di qualità per la rettifica di fatture già pagate. Energit è tenuta pertanto ad effettuare le opportune verifiche ed i relativi pagamenti, se dovuti, entro i termini di seguito indicati. Nel caso in cui tale rettifica di fatture non sia stata eseguita entro i termini di seguito indicati, Energit è tenuta a pagare al Cliente i seguenti indennizzi:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
									
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
									
									<fo:table end-indent="0pt" start-indent="22pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(7)"/>
									<fo:table-column column-width="proportional-column-width(66)"/>
									<fo:table-column column-width="proportional-column-width(20)"/>
									<fo:table-column column-width="proportional-column-width(7)"/>
										<fo:table-body end-indent="0pt" start-indent="3pt">
											<fo:table-row height="{$altezzariga}">
												<fo:table-cell><fo:block /></fo:table-cell>
												<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" padding-top="3pt" padding-left="3pt" padding-right="3pt" padding-bottom="3pt">
													<fo:block text-align="center" xsl:use-attribute-sets="chr.condizioni">Termine massimo per il pagamento</fo:block>
												</fo:table-cell>
												<fo:table-cell text-align="center" display-align="center" xsl:use-attribute-sets="brd.000" padding-top="3pt" padding-left="3pt" padding-right="3pt" padding-bottom="3pt">
													<fo:block text-align="center" xsl:use-attribute-sets="chr.condizioni">90 giorni</fo:block>
												</fo:table-cell>
												<fo:table-cell><fo:block /></fo:table-cell>
											</fo:table-row>
											<fo:table-row height="{$altezzariga}">
												<fo:table-cell><fo:block /></fo:table-cell>
												<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" padding-top="3pt" padding-left="3pt" padding-right="3pt" padding-bottom="3pt">
													<fo:block text-align="left" xsl:use-attribute-sets="chr.condizioni">Rimborso dovuto al Cliente per esecuzione oltre standard ma entro un tempo doppio del termine</fo:block>
												</fo:table-cell>
												<fo:table-cell text-align="center" display-align="center" xsl:use-attribute-sets="brd.000" padding-top="3pt" padding-left="3pt" padding-right="3pt" padding-bottom="3pt">
													<fo:block text-align="center" xsl:use-attribute-sets="chr.condizioni">€ 30,00</fo:block>
												</fo:table-cell>
												<fo:table-cell><fo:block /></fo:table-cell>
											</fo:table-row>
											<fo:table-row height="{$altezzariga}">
												<fo:table-cell><fo:block /></fo:table-cell>
												<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" padding-top="3pt" padding-left="3pt" padding-right="3pt" padding-bottom="3pt">
													<fo:block text-align="left" xsl:use-attribute-sets="chr.condizioni">Rimborso dovuto al Cliente per esecuzione oltre standard entro un tempo triplo del termine</fo:block>
												</fo:table-cell>
												<fo:table-cell text-align="center" display-align="center" xsl:use-attribute-sets="brd.000" padding-top="3pt" padding-left="3pt" padding-right="3pt" padding-bottom="3pt">
													<fo:block text-align="center" xsl:use-attribute-sets="chr.condizioni">€ 60,00</fo:block>
												</fo:table-cell>
												<fo:table-cell><fo:block /></fo:table-cell>
											</fo:table-row>
											<fo:table-row height="{$altezzariga}">
												<fo:table-cell><fo:block /></fo:table-cell>
												<fo:table-cell display-align="center" xsl:use-attribute-sets="brd.000" padding-top="3pt" padding-left="3pt" padding-right="3pt" padding-bottom="3pt">
													<fo:block text-align="left" xsl:use-attribute-sets="chr.condizioni">Rimborso dovuto al Cliente per esecuzione oltre standard dopo un tempo triplo del termine</fo:block>
												</fo:table-cell>
												<fo:table-cell text-align="center" display-align="center" xsl:use-attribute-sets="brd.000" padding-top="3pt" padding-left="3pt" padding-right="3pt" padding-bottom="3pt">
													<fo:block text-align="center" xsl:use-attribute-sets="chr.condizioni">€ 90,00</fo:block>
												</fo:table-cell>
												<fo:table-cell><fo:block /></fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
									
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
									
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">18.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit è tenuta altresì a pagare al Cliente i rimborsi che il Distributore Locale abbia pagato alla stessa Energit per l’eventuale mancato rispetto da parte del Distributore Locale di livelli specifici di qualità commerciale ai sensi della delibera AEEG n. 333/07.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">18.4</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit corrisponderà inoltre al Cliente ogni indennizzo, qualora dovuto ai sensi della normativa di volta in volta applicabile, se dovuto a causa di mancato rispetto della qualità dei servizi di vendita di energia elettrica, ai sensi a titolo esemplificativo della delibera AEEG n. 164/08 nei limiti della sua applicabilità.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">18.5</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">In ogni caso, il pagamento del rimborso ricevuto dal Distributore Locale e/o il pagamento di ogni altro indennizzo non implica di per sé un inadempimento di Energit al Contratto, né un riconoscimento di responsabilità da parte di Energit in merito alle cause del mancato rispetto dei livelli specifici di qualità commerciale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">18.6</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit potrà accreditare al Cliente eventuali rimborsi attraverso detrazione dall’importo addebitato nella prima fatturazione utile.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">19</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">COMUNICAZIONI TRA LE PARTI</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">19.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Tutte le comunicazioni tra le Parti, ivi inclusi i reclami, le richieste, le segnalazioni per difetti di funzionamento nell’erogazione e nella fruizione del Servizio, dovranno avvenire nei modi e forme indicati nel Contratto. Gli indirizzi per qualsivoglia comunicazione a Energit - salvo successive comunicazioni di modifica - sono i seguenti: a) se mediante il servizio postale a Energ.it S.p.A., Edward Jenner, 19/21 09121 Cagliari, b) se a mezzo telefax al numero gratuito 800.1922.55 - attivo 24 ore su 24, c) se a mezzo posta elettronica, inclusa la PEC, a energia@energit.it. Energit mette inoltre a disposizione del Cliente un apposito Servizio Clienti che potrà essere contattato al numero gratuito 800.1922.22 o al diverso numero indicato sul sito internet www.energit.it, e che sarà attivo dal lunedì al venerdì dalle 8.30 alle 18.00 per fornire al Cliente informazioni di natura tecnica e/o commerciale ovvero per inoltrare le richieste di assistenza relative al Servizio.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">20</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">INSERIMENTO DI REGOLAMENTAZIONI TECNICHE E CLAUSOLE NEGOZIALI</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">20.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Durante il periodo di validità, il Contratto si intenderà modificato di diritto mediante l’inserimento di clausole negoziali o regolamentazioni tecniche che l’AEEG potrà definire ai sensi delle leggi vigenti.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">21</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">CESSIONE DEL CONTRATTO</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">21.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Cliente non può cedere il Contratto, senza il previo consenso scritto di Energit.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">21.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Energit ha il diritto di cedere o di trasferire il Contratto con tutti i suoi diritti ed obblighi, per intero o in parte, tramite semplice comunicazione al Cliente, che accetta sin d’ora. In tal caso, ed in deroga a quanto previsto dall’Articolo 1408 (18) del Codice Civile, Energit sarà esonerata da ogni responsabilità nei confronti del Cliente per l’esecuzione del Contratto dalla data di comunicazione della cessione del Contratto da parte di Energit.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">21.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">La cessione del Contratto ad opera di Energit non potrà in alcun caso comportare una diminuzione della tutela dei diritti del Cliente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">22</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">VARIE</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">22.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Il Contratto contiene la manifestazione integrale delle intese raggiunte fra le Parti in relazione all’oggetto dello stesso e pertanto prevale su qualsiasi precedente intesa fra le stesse avente il medesimo oggetto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">22.2</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">La circostanza che una delle Parti non faccia in un qualsiasi momento valere i diritti ad essa riconosciuti da una o più clausole del Contratto non potrà essere intesa come rinuncia a tali diritti, né impedirà alla stessa di pretenderne successivamente la puntuale e rigorosa osservanza.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">22.3</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Qualora una delle clausole del Contratto dovesse risultare invalida o inefficace, ovvero non acquisti efficacia, il Contratto avrà piena e completa efficacia per la restante parte, salvo che la clausola in questione rivesta carattere essenziale o sia stata per una delle Parti motivo determinante per la conclusione del Contratto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">23</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">CONTROVERSIE E FORO COMPETENTE</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">23.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Per qualunque controversia nascente dal Contratto (a titolo esemplificativo, e non esaustivo, dalla sua validità, efficacia, interpretazione, esecuzione, risoluzione, rescissione) sarà competente il giudice del luogo di residenza o di domicilio del Cliente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
										<fo:leader line-height="7pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">24</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">APPROVAZIONE DELLE CLAUSOLE</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold">24.1</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Ai sensi e per gli effetti degli Articoli 1341 (19) e 1342 (20) del Codice Civile, il Cliente dichiara espressamente di conoscere ed accettare integralmente il contenuto delle seguenti previsioni delle presenti condizioni generali: 2.2 (esclusiva), 3 (deleghe, import e CIP6), 4.6 (gestione tecnica della fornitura), 6 (conclusione del Contratto), 7 (durata e recesso), 8 (variazione delle specifiche tecniche e delle condizioni del Contratto), 9.4 (aggiornamento del corrispettivo), 10 (fatturazione e pagamenti), 11 (deposito cauzionale), 12 (sospensione del Servizio), 13 (risoluzione), 14 (limitazione di responsabilità di Energit), 15 (uso improprio del Servizio), 16 (trattamento dati personali D.Lgs. 196/03), 17 (Riservatezza, pubblicità), 18 (reclami, rimborsi), 21 (cessione del Contratto).</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
		          
		
		
						</fo:list-block>
					</fo:table-cell>
					<!--
					******************************************************************************************************************
					**                                         Colonna di separazione                                               **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni">
							<fo:inline xsl:use-attribute-sets="chr.condizioni"></fo:inline>
						</fo:block>
					</fo:table-cell>
					<!--
					******************************************************************************************************************
					**                                           Seconda colonna                                                    **
					******************************************************************************************************************
					-->
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:block xsl:use-attribute-sets="blk.condizioni_spazio">
							<fo:leader line-height="7pt"/>
						</fo:block>
						<fo:list-block provisional-distance-between-starts="19pt" provisional-label-separation="3pt">
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni" font-weight="bold"></fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">* * *</fo:block>
									<fo:block space-before="3mm" xsl:use-attribute-sets="chr.condizioni" font-weight="bold">Riferimenti normativi:</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">1)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Delibera AEEG n. 348/07 “Testo integrato delle disposizioni dell’Autorità per l’energia elettrica e il gas per l’erogazione dei servizi di trasmissione, distribuzione e misura dell’energia elettrica per il periodo di regolazione 2008-2011 e disposizioni in materia di condizioni economiche per l’erogazione del servizio di connessione”.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">2)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Decreto del Presidente della Repubblica "Regolamento recante disposizioni per l'utilizzo della posta elettronica certificata, a norma dell'articolo 27 della legge 16 gennaio 2003, n.3", pubblicato nella Gazz. Uff. 28 marzo 2005, n. 97.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">3)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Decreto Ministeriale “Cessione dei diritti e delle obbligazioni relativi all'acquisto di energia elettrica prodotta da altri operatori nazionali, da parte dell'ENEL S.p.a. al Gestore della rete di trasmissione nazionale S.p.a.” pubblicato nella Gazz. Uff. 30 novembre 2000, n. 280.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">4)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Delibera AEEG n. 333/07 “Testo integrato della regolazione della qualità dei servizi di distribuzione, misura e vendita dell’energia elettrica per il periodo di regolazione 2008-2011” pubblicata sul sito www.autorita.energia.it in data 21 dicembre 2007.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">5)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Articolo 1705 codice civile Mandato senza rappresentanza.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">6)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Articolo 1329 codice civile Proposta irrevocabile.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">7)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Decreto Legislativo 6 settembre 2005, n. 206 “Codice del consumo, a norma dell'articolo 7 della L. 29 luglio 2003, n. 229”, pubblicato nella Gazz. Uff. 8 ottobre 2005, n. 235.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">8)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Articolo 1704 codice civile Mandato con rappresentanza.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">9)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Delibera AEEG n. 04/08 “Regolazione del servizio di dispacciamento e del servizio di trasporto (trasmissione, distribuzione e misura) dell’energia elettrica nei casi di morosità dei clienti finali o di inadempimento da parte del venditore” pubblicata sul sito www.autorita.energia.it in data 30 gennaio 2008.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">10)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Articolo 1353 codice civile Contratto Condizionale.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">11)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Delibera AEEG n.144/07 “Disciplina del recesso dai contratti di fornitura di energia elettrica e di gas naturale, ai sensi dell'articolo 2, comma 12, lettera h), della legge 14 novembre 1995, n. 481” pubblicata sul sito www.autorita.energia.it in data 26 giugno 2007.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">12)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Delibera AEEG n. 157/08 “Aggiornamento per l’anno 2009 dei corrispettivi di conguaglio compensativo da applicarsi all’energia elettrica prelevata dai punti di prelievo in bassa tensione non trattati per fasce e serviti nel mercato libero nelle aree con ridotta diffusione dei sistemi di tele gestione”.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">13)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Decreto Legislativo “Attuazione della direttiva 2000/35/CE relativa alla lotta contro i ritardi di pagamento nelle transazioni commerciali” pubblicato nella Gazz. Uff. 23 ottobre 2002, n. 249.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">14)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Articolo 1456 codice civile Clausola risolutiva espressa.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">15)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Articolo 1454 codice civile Diffida ad adempiere.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">16)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Decreto Legislativo “Codice in materia di protezione dei dati personali” pubblicato nella Gazz. Uff. 29 luglio 2003, n. 174, S.O..</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">17)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Delibera AEEG n. 164/08 “Testo integrato della regolazione della qualità dei servizi di vendita di energia elettrica e di gas naturale” pubblicata sul sito www.autorita.energia.it in data 20 novembre 2008.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">18)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Articolo 1408 codice civile Rapporti fra contraente ceduto e cedente.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">19)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Articolo 1341 codice civile Condizioni generali di contratto.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
							<fo:list-item>
								<fo:list-item-label end-indent="label-end()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">20)</fo:block>
								</fo:list-item-label>
								<fo:list-item-body start-indent="body-start()">
									<fo:block xsl:use-attribute-sets="chr.condizioni">Articolo 1342 codice civile Contratto concluso mediante moduli o formulari.</fo:block>
									<fo:block xsl:use-attribute-sets="blk.gap_list">
										<fo:leader line-height="0pt"/>
									</fo:block>
								</fo:list-item-body>
						    </fo:list-item>
		          
		          
						</fo:list-block>
		          
		          
		          
					</fo:table-cell>
				</fo:table-row>    
		    
		    
		    </fo:table-body>
		</fo:table>
		 
		 
		 
		<fo:block id="@id1{generate-id(.)}" />
	</fo:flow>
</fo:page-sequence>







<!--
..................................................................................................................
-->        



</xsl:template>

<!--
******************************************************************************************************************
**                                        Definizione attributi                                                 **
******************************************************************************************************************
-->
<xsl:attribute-set name="blk.condizioni">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">8pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.condizioni_spazio">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">7pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.gap_list">
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="line-height">0pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.condizioni">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">6.5pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="line-height">7.7pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.linea">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="font-weight">bold</xsl:attribute>
	<xsl:attribute name="line-height">7pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.condizioni_piccole">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">5pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="font-weight">bold</xsl:attribute>
	<xsl:attribute name="line-height">7pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.condizioni_bold">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">6.5pt</xsl:attribute>
	<xsl:attribute name="font-weight">bold</xsl:attribute>
	<xsl:attribute name="line-height">7pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.condizioni_underline">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">6.5pt</xsl:attribute>
	<xsl:attribute name="text-decoration">underline</xsl:attribute>
	<xsl:attribute name="line-height">7pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="chr.condizioni_italic">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">6.5pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="font-style">italic</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="brd.000">
    <xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
	<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
    <xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
    <xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>






<xsl:attribute-set name="chr.riepilogo10">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">10pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="line-height">9pt</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="chr.riepilogo10bold">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">10pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
  <xsl:attribute name="font-weight">bold</xsl:attribute>
	<xsl:attribute name="line-height">9pt</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="chr.riepilogo8">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">10pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="line-height">9pt</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="chr.riepilogo8bold">
    <xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
  <xsl:attribute name="font-weight">bold</xsl:attribute>
	<xsl:attribute name="line-height">7pt</xsl:attribute>
</xsl:attribute-set>



</xsl:stylesheet>

