<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<xsl:template name="Dettaglio_energia_012011">

<xsl:param name="bordi"/>
<xsl:param name="color"/>
<xsl:param name="color_titolo_dettaglio"/>
<xsl:param name="color-sezioni"/>
<xsl:param name="color-sottosezioni"/>
<xsl:param name="svg-sezioni"/>
<xsl:param name="svg-sottosezioni"/>
<xsl:param name="lines_flag">SI</xsl:param>
<xsl:param name="ultima_sezione_reti"/>
<xsl:param name="ultima_sezione_vendita"/>
<xsl:param name="ultima_sezione_imposte"/>

<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(./ancestor::DOCUMENTO/child::DATA_DOCUMENTO,7,4),substring(./ancestor::DOCUMENTO/child::DATA_DOCUMENTO,4,2))"/></xsl:param>

<xsl:variable name="titolo1">
	<xsl:choose>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo1_c_202_09"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo1_202_09"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="titolo2">
	<xsl:choose>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo2_c_202_09"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo2_202_09"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="titolo2a">
	<xsl:choose>
		<xsl:when test="./child::PRODOTTO_SERVIZIO='Energit per Noi - EN-U-PO-01'">
			<xsl:value-of select="$tipo2a_exnoi_202_09"/>
		</xsl:when>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo2a_c_202_09"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo2a_202_09"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="titolo2b">
	<xsl:choose>
		<xsl:when test="@CONSUMER='SI'">
			<xsl:value-of select="$tipo2b_c_202_09"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$tipo2b_202_09"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
	
<!--
**************************************************************************
**                                                                      **
** CORRISPETTIVI PER ACQUISTO, VENDITA, DISPACCIAMENTO E SBILANCIAMENTO **
**                                                                      **
**************************************************************************
-->	
	<xsl:if test="(./child::SEZIONE[@TIPOLOGIA=$quota_fissa_vendita] or
				  ./child::SEZIONE[@TIPOLOGIA=$quota_variabile_vendita] or
				  ./child::SEZIONE[@TIPOLOGIA=$materia_prima_gas])
				 ">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:if test="not($ultima_sezione_reti='')">
				<xsl:attribute name="space-before">1mm</xsl:attribute>
			</xsl:if>
			
			<fo:table-column column-width="proportional-column-width(100)"/>
			
			<fo:table-body>
				<xsl:if test="not($bordi='NO')">
					<fo:table-row height="3mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
							<fo:block></fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
				
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(100)"/>					
							
							<xsl:call-template name="Header_sezione_dettaglio_012011">
								<xsl:with-param name="path" select="$svg-sezioni"/>
								<xsl:with-param name="titolo" select="$titolo2"/>
								<xsl:with-param name="header" select="'NO'"/>
								<xsl:with-param name="colonne" select="1"/>
								<xsl:with-param name="color" select="$color-sezioni"/>
								<xsl:with-param name="macro_sezione" select="'SI'"/>
							</xsl:call-template>
					
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										
										
										<!--Quota Fissa EA *********** ECCOLOOOOO *******-->
										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_vendita] or
													  ./child::SEZIONE[@TIPOLOGIA=$quota_variabile_vendita] or
													  ./child::SEZIONE[@TIPOLOGIA=$materia_prima_gas]
													 "> <!-- ultima riga non c'era -->
													 
													 
										<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_vendita]">
											<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(19)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(6)"/>
											
												<xsl:call-template name="Header_sezione_dettaglio_012011">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="'no_title'"/>
													<xsl:with-param name="header" select="'SI'"/>
													<xsl:with-param name="colonne" select="8"/> <!--era 8 -->
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>
												
												<fo:table-body end-indent="0pt" start-indent="0pt">
													<fo:table-row>
														<fo:table-cell number-columns-spanned="8"> <!-- era 7 -->
															<!--QUOTA FISSA VENDITA-->
															<xsl:call-template name="Sezione_dettaglio_generica_012011">
																<xsl:with-param name="nome_sezione" select="$quota_fissa_vendita"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA FISSA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/>
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>
															</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
												</fo:table>
										</xsl:if>
												
												
												
												
												
												
											<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_variabile_vendita]">	
												<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(19)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(6)"/>
												
												<xsl:call-template name="Header_sezione_dettaglio_012011"> <!-- _ -->
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="'no_title'"/>
													<xsl:with-param name="header" select="'SI'"/>
													<xsl:with-param name="colonne" select="8"/> <!--era 8 -->
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>
												
												<fo:table-body end-indent="0pt" start-indent="0pt">
													<fo:table-row>
														<fo:table-cell number-columns-spanned="8"> <!-- era 7 -->
															<!--QUOTA ENERGIA VENDITA-->
															
															<xsl:call-template name="Sezione_dettaglio_generica_012011_ConScaglioniMaAdUnPrezzo"> <!--  -->
																<xsl:with-param name="nome_sezione" select="$quota_variabile_vendita"/>
																<xsl:with-param name="tipo_quota" select="'QUOTA ENERGIA'"/>
																<xsl:with-param name="last_section" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="info_totale" select="'VENDITA'"/> 
																<xsl:with-param name="lines" select="$lines_flag"/>
																<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
																<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
																<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
																<xsl:with-param name="titolo" select="$titolo2"/>
															</xsl:call-template>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
									</xsl:if>
											
											
										</xsl:if>
										
										
										
										
										
									
										<!--
										**************************************************************************
										**                                                                      **
										**                        COMPONENTI ENERGIA                            **
										**                                                                      **
										**************************************************************************
										-->
										<!-- <xsl:if test="./child::SEZIONE[@TIPOLOGIA=$pc_disp]">
											<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
											<fo:table-column column-width="proportional-column-width(75)"/>
											<fo:table-column column-width="proportional-column-width(10)"/>
											<fo:table-column column-width="proportional-column-width(15)"/>
											
												<xsl:call-template name="Header_sezione_dettaglio_012011">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="$titolo2a"/>
													<xsl:with-param name="header" select="'NO'"/>
													<xsl:with-param name="colonne" select="3"/>
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>
												
												
											</fo:table>
										</xsl:if> -->
										
										
										
										
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell xsl:use-attribute-sets="brd.b.000">
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<!--
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell>
										<fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
										<fo:table-column column-width="proportional-column-width(85)"/>
										<fo:table-column column-width="proportional-column-width(15)"/>
										<fo:table-body end-indent="0pt" start-indent="0pt">
											<fo:table-row height="{$altezzariga}">                    
												<fo:table-cell>                    
													<fo:block xsl:use-attribute-sets="blk.000">
														TOTALE <xsl:value-of select="$titolo2"/>
														<xsl:if test="$lettere_sezioni='SI'">
															<fo:inline xsl:use-attribute-sets="condensedcors.008">
																(A)
															</fo:inline>
														</xsl:if>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="./child::SINTESI_ENERGIA/child::TOTALE_SERVIZI_VENDITA"/>
													</fo:block>
												</fo:table-cell>
												
											</fo:table-row>
										</fo:table-body>
										</fo:table>
									</fo:table-cell>
								</fo:table-row>
								-->
							</fo:table-body>
						</fo:table>	
						
						<!-- <xsl:if test="@CONSUMER='SI'">
							<fo:block xsl:use-attribute-sets="blk.004 chrbold.008">
								Maggiori dettagli sulle componenti A, UC, MCT sono disponibili nella sua area riservata.
							</fo:block>
						</xsl:if> -->
					</fo:table-cell>
				</fo:table-row>
				
			
				<xsl:if test="not($bordi='NO')">
					<fo:table-row keep-with-previous="always" height="2mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
							<fo:block></fo:block>
							
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-body>
			
		</fo:table>
		<fo:block xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="6pt" margin-left="3mm" margin-right="2mm">
			*Nota: la fatturazione della componente "Materia prima gas" è suscettibile di adeguamento in diretta proporzione ai valori del potere 
			calorifico superiore (PCS) pubblicati sul sito dell'impresa di trasporto, nell' ambito locale del Sito del Cliente . 
			Il prezzo di listino è il valore del prezzo della materia prima gas presente sul contratto di fornitura calcolato sul valore medio 
			nazionale del PCS, convenzionalmente pari a 0,03852 GJ/Smc, mentre ai fini della fatturazione tale valore viene adeguato al valore di 
			PCS locale del Cliente. Il parametro PCS esprime il potere calorifico superiore, ovvero la capacità del gas naturale di produrre energia 
			termica; tale valore varia a livello geografico ed è soggetto ad aggiornamento periodico da parte della società di trasporto.
		</fo:block>
	</xsl:if>



	
<!--
**************************************************************************
**                                                                      **
**      CORRISPETTIVI PER L’USO DELLE RETI E IL SERVIZIO DI MISURA      **
**                                                                      **
**************************************************************************
-->
<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_reti] or
			  ./child::SEZIONE[@TIPOLOGIA=$quota_variabile_reti]">

	<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			
			<fo:table-body>
				<xsl:if test="not($bordi='NO')">
					<fo:table-row height="3mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
							<fo:block/>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
				
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(19)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(6)"/>
							
							<xsl:call-template name="Header_sezione_dettaglio_012011">
								<xsl:with-param name="path" select="$svg-sezioni"/>
								<xsl:with-param name="titolo" select="$titolo1"/>
								<xsl:with-param name="header" select="'NO'"/>
								<xsl:with-param name="colonne" select="8"/>
								<xsl:with-param name="color" select="$color-sezioni"/>
								<xsl:with-param name="macro_sezione" select="'SI'"/>
							</xsl:call-template>
							
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row>
								<fo:table-cell number-columns-spanned="8">
									<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_reti]">
									
										<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(19)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(6)"/>
											
											<xsl:call-template name="Header_sezione_dettaglio_012011">
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="'no_title'"/>
													<xsl:with-param name="header" select="'SI'"/>
													<xsl:with-param name="colonne" select="8"/> 
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>
											<fo:table-body end-indent="0pt" start-indent="0pt">
												<fo:table-row>
													<fo:table-cell number-columns-spanned="8">
											
											
														<xsl:call-template name="Sezione_dettaglio_generica_012011">
														<xsl:with-param name="nome_sezione" select="$quota_fissa_reti"/>
														<xsl:with-param name="tipo_quota" select="'QUOTA FISSA'"/>
														<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
														<xsl:with-param name="info_totale" select="'USO RETI'"/>
														<xsl:with-param name="lines" select="$lines_flag"/>
														<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
														<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
														<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
														<xsl:with-param name="titolo" select="$titolo1"/>
														</xsl:call-template>
													</fo:table-cell>
												</fo:table-row>	
											</fo:table-body>
										</fo:table>
								
									</xsl:if>
									
									<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$quota_variabile_reti]">	
								
										<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(19)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(11)"/>
												<fo:table-column column-width="proportional-column-width(9)"/>
												<fo:table-column column-width="proportional-column-width(6)"/>
												
											<xsl:call-template name="Header_sezione_dettaglio_012011"> <!-- _ -->
													<xsl:with-param name="path" select="$svg-sottosezioni"/>
													<xsl:with-param name="titolo" select="'no_title'"/>
													<xsl:with-param name="header" select="'SI'"/>
													<xsl:with-param name="colonne" select="8"/> <!--era 8 -->
													<xsl:with-param name="color" select="$color-sottosezioni"/>
													<xsl:with-param name="macro_sezione" select="'NO'"/>
												</xsl:call-template>
											<fo:table-body end-indent="0pt" start-indent="0pt">
												<fo:table-row>
													<fo:table-cell number-columns-spanned="8">	

														<xsl:call-template name="Sezione_dettaglio_generica_012011_ConScaglioniMaAdUnPrezzo"> <!-- _ -->
															<xsl:with-param name="nome_sezione" select="$quota_variabile_reti"/>
															<xsl:with-param name="tipo_quota" select="'QUOTA ENERGIA'"/>
															<xsl:with-param name="last_section" select="$ultima_sezione_reti"/>
															<xsl:with-param name="info_totale" select="'USO RETI'"/>
															<xsl:with-param name="lines" select="$lines_flag"/>
															<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
															<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
															<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
															<xsl:with-param name="titolo" select="$titolo1"/>
														</xsl:call-template>
													</fo:table-cell>		
												</fo:table-row>	
											</fo:table-body>
										</fo:table>	
									</xsl:if>
									

									</fo:table-cell>
									</fo:table-row>
								
							</fo:table-body>
							
						</fo:table>
							
						
						
						
						<!-- <xsl:if test="@CONSUMER='NO'"> -->
						<!--
							<fo:block xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="6.5pt">
								Nota: il parametro PCS esprime il potere calorifico superiore, ovvero la capacità del gas naturale di produrre energia termica. Tale valore
								varia a livello geografico ed è soggetto ad aggiornamento periodico da parte delle società di trasporto. Nel calcolo della bolletta viene utilizzato al fine di adeguare 
								il prezzo del gas alla resa efettiva del metano utilizzato nella località di appartenenza. Il "PCS standard" è il valore medio nazionale utilizzato nelle offerte commerciali
								per la presentazione del prezzo, convenzionalmente pari a 0,03852 GJ/Smc; il "PCS Specifico" rappresenta il valore dello stesso parametro per la località di fornitura del Cliente.
							</fo:block>
							-->
							<fo:block keep-with-previous="always" xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="7pt">
								Nota: gli aggiornamenti delle componenti tariffarie fatturate sono presenti sul sito web
								dell'Autorità per l'Energia Elettrica e il Gas, nella sezione atti e provvedimenti.
							</fo:block>
							<fo:block keep-with-previous="always" xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="7pt">
								Se desidera maggiori dettagli sui corrispettivi fatturati e sulle ulteriori disaggregazioni degli stessi, 
								invii una email all’indirizzo energia@energit.it.
							</fo:block>
						<!-- </xsl:if> -->
					</fo:table-cell>
				</fo:table-row>
				
				<xsl:if test="not($bordi='NO')">
					<fo:table-row keep-with-previous="always" height="2mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
							<fo:block></fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-body>
		</fo:table>
	</xsl:if>
	
	
	
<!--
**************************************************************************
**                                                                      **
**                             IMPOSTE                                  **
**                                                                      **
**************************************************************************
-->	

	<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$imposte]
				 ">
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:if test="not($ultima_sezione_reti='') or
						  not($ultima_sezione_vendita='')">
				<xsl:attribute name="space-before">1mm</xsl:attribute>
			</xsl:if>
			
			<fo:table-column column-width="proportional-column-width(100)"/>
			
			<fo:table-body>
				<xsl:if test="not($bordi='NO')">
					<fo:table-row height="3mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
							<fo:block xsl:use-attribute-sets="blk.002">
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
				
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
						</xsl:if>
						<fo:table space-before="10mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(19)"/>
						<fo:table-column column-width="proportional-column-width(15)"/>
						<fo:table-column column-width="proportional-column-width(11)"/>
						<fo:table-column column-width="proportional-column-width(9)"/>
						<fo:table-column column-width="proportional-column-width(11)"/>
						<fo:table-column column-width="proportional-column-width(9)"/>
						<fo:table-column column-width="proportional-column-width(6)"/>
						
						<xsl:call-template name="Header_sezione_dettaglio_012011">
							<xsl:with-param name="path" select="$svg-sezioni"/>
							<xsl:with-param name="titolo" select="$tipo3_202_09"/>
							<xsl:with-param name="header" select="'SI'"/>
							<xsl:with-param name="colonne" select="8"/> 
							<xsl:with-param name="color" select="$color-sezioni"/>
							<xsl:with-param name="macro_sezione" select="'SI'"/>
						</xsl:call-template>

						<fo:table-body end-indent="0pt" start-indent="0pt">
							<fo:table-row>
								<fo:table-cell number-columns-spanned="8">
									
									<xsl:call-template name="Sezione_dettaglio_generica_012011_ConScaglioniMaAdUnPrezzo"> 
										<xsl:with-param name="nome_sezione" select="$imposte"/>
										<xsl:with-param name="tipo_quota" select="''"/>
										<xsl:with-param name="last_section" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="info_totale" select="'IMPOSTE'"/>
										<xsl:with-param name="lines" select="$lines_flag"/>
										<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
										<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
										<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
										<xsl:with-param name="titolo" select="$tipo3_202_09"/>
									</xsl:call-template>
									
									<fo:block xsl:use-attribute-sets="blk.002">
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
						</fo:table>	
					</fo:table-cell>
				</fo:table-row>
			
				<xsl:if test="not($bordi='NO')">
					<fo:table-row keep-with-previous="always" height="2mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
							<fo:block xsl:use-attribute-sets="blk.002">
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-body>
		</fo:table>
	</xsl:if>
	
</xsl:template>




</xsl:stylesheet>