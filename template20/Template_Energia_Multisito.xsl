<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:fox="http://xml.apache.org/fop/extensions">
<xsl:output encoding="UTF-8"/>

<xsl:variable name="pc_disp">EA</xsl:variable>
<xsl:variable name="pt">PT</xsl:variable>
<xsl:variable name="eb_cte">EB</xsl:variable>
<xsl:variable name="a_uc">CT_FISSE</xsl:variable>
<xsl:variable name="tp">TP</xsl:variable>
<xsl:variable name="te">TE</xsl:variable>
<xsl:variable name="ert">ERT</xsl:variable>
<xsl:variable name="imposta_erariale">IE</xsl:variable>
<xsl:variable name="oneri_diversi">ONERI_DIVERSI</xsl:variable>
<xsl:variable name="canone_rai">CANONE_RAI</xsl:variable>
<xsl:variable name="addizionale_provinciale">AP</xsl:variable>
<xsl:variable name="addizionale_comunale">AC</xsl:variable>
<xsl:variable name="quota_fissa_ea">QUOTA_FISSA_EA</xsl:variable>
<xsl:variable name="quota_fissa_eb">QUOTA_FISSA_EB</xsl:variable>
<xsl:variable name="tipo1">CORRISPETTIVI PER L'USO DELLE RETI E IL SERVIZIO DI MISURA</xsl:variable>
<xsl:variable name="tipo2">CORRISPETTIVI PER ACQUISTO, VENDITA, DISPACCIAMENTO E SBILANCIAMENTO</xsl:variable>
<xsl:variable name="tipo2a">COMPONENTI ENERGIA</xsl:variable>
<xsl:variable name="tipo2b">COMPONENTI DISPACCIAMENTO E PERDITE</xsl:variable>
<xsl:variable name="tipo3">IMPOSTE</xsl:variable>
<xsl:variable name="tipo4">COMPONENTI PERDITE</xsl:variable>
<xsl:variable name="tipo1_c">QUOTA FISSA E QUOTA POTENZA</xsl:variable>
<xsl:variable name="tipo2_c">QUOTA ENERGIA</xsl:variable>
<xsl:variable name="tipo2a_exnoi">COMPONENTI ENERGIA PURA</xsl:variable>
<xsl:variable name="tipo2a_c">COMPONENTI ENERGIA</xsl:variable>
<xsl:variable name="tipo2b_c">ALTRE COMPONENTI AEEG</xsl:variable>

<xsl:include href="dettaglio_energia_bordi.xsl"/>
<xsl:include href="Sezione_generica_dettaglio_energia.xsl"/>


<!-- <xsl:include href="sintesi_monosito.xsl"/>
<xsl:include href="sintesi_multisito.xsl"/> -->
<xsl:include href="sintesi_energia.xsl"/>
<!-- <xsl:include href="oneri_diversi_bordi.xsl"/> -->
<xsl:include href="template_letture.xsl"/>
<xsl:include href="letture_consumi_conguagli.xsl"/>
<xsl:include href="riepilogo_acconti_conguaglio.xsl"/>
<xsl:include href="consumi_energia_ultimi_14_mesi.xsl"/>

<xsl:include href="template_energia_parametrizzato.xsl"/>

<xsl:include href="altre_sezioni.xsl"/>

<!--
********************************************************
**                                                    **
**         Template per i contratti Energia           **
**                                                    **
********************************************************
-->
<xsl:template name="CONTRATTI_MULTISITO">
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="sfondo_titoli"/>
	<xsl:param name="color-sezioni"/>
	<xsl:param name="color-sottosezioni"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:param name="svg-sottosezioni"/>
	<xsl:param name="svg-dettaglio"/>
	<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:param>
	
	<!--
	********************************************************
	**                                                    **
	**             Enegia Elettrica Sintesi               **
	**                                                    **
	********************************************************
	-->
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
	<fo:table-column column-width="proportional-column-width(100)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<!-- <fo:table-row keep-with-next="always">
			<fo:table-cell xsl:use-attribute-sets="pad.l.000">
				<fo:block xsl:use-attribute-sets="blk.003 chrtitolodettaglio">
					ENERGIA ELETTRICA - SINTESI
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row keep-with-next="always" height="0.5mm">
			<fo:table-cell border-bottom="0.5 dashed thick black">
				<fo:block/>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row keep-with-next="always" height="0.5mm">
			<fo:table-cell>
				<fo:block/>
			</fo:table-cell>
		</fo:table-row> -->
		
		<fo:table-row keep-with-next="always">
			<fo:table-cell>
				<xsl:call-template name="SINTESI_ENERGIA">
					<xsl:with-param name="bordi" select="$bordi"/>
					<xsl:with-param name="color" select="$color"/>
					<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
					<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
					<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
					<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
					<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
					<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
					<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
				</xsl:call-template>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
	
	<xsl:for-each select="./child::CONTRATTO_ENERGIA">
		<!--
		********************************************************
		**                                                    **
		**                Dati di riepilogo                   **
		**                                                    **
		********************************************************
		-->
		<xsl:if test="position()=1">
			<fo:block break-before="page">
			</fo:block>
		</xsl:if>
		
		<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			
			<fo:table-body>
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<!-- <xsl:attribute name="background-image">url(svg/rectangle_table_header_dettaglio.svg)</xsl:attribute> -->
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<!-- <xsl:attribute name="background-image">url(svg/rectangle_table_body_dettaglio.svg)</xsl:attribute> -->
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(50)"/>
							<fo:table-column column-width="proportional-column-width(50)"/>
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row keep-with-next="always">
									<fo:table-cell number-columns-spanned="2">
										<fo:block xsl:use-attribute-sets="blk.003 chrtitolodettaglio">
											BOLLETTA DI SINTESI
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-next="always" height="0.5mm">
									<fo:table-cell number-columns-spanned="2">
										<fo:block>
											CONTRATTO N. <xsl:value-of select="./child::CONTRACT_NO"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<xsl:choose>
									<xsl:when test="not(./child::PERIODO_RIFERIMENTO='')">
										<fo:table-row keep-with-next="always" height="0.5mm">
											<fo:table-cell number-columns-spanned="2" border-bottom="0.5 dashed thick black">
												<fo:block/>
											</fo:table-cell>
										</fo:table-row>
										
										<fo:table-row keep-with-next="always" height="0.5mm">
											<fo:table-cell number-columns-spanned="2">
												<fo:block/>
											</fo:table-cell>
										</fo:table-row>
									
										<fo:table-row keep-with-next="always" height="2mm">
											<fo:table-cell>
												<fo:block xsl:use-attribute-sets="blk.010">
													<fo:inline xsl:use-attribute-sets="chrbold.008">PERIODO DI RIFERIMENTO </fo:inline>
													<fo:inline xsl:use-attribute-sets="chr.009"><xsl:value-of select="./child::PERIODO_RIFERIMENTO"/></fo:inline>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</xsl:when>
								</xsl:choose>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(svg/rectangle_table_footer_dettaglio.svg)</xsl:attribute> -->
							<xsl:if test="not($bordi='NO')">
								<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="not($color='black')">
								<xsl:attribute name="background-image">url(svg/rectangle_table_color_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="$bordi='NO' and $color='black'">
								<xsl:attribute name="background-image">url(svg/rectangle_table_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<xsl:if test="./ancestor::DOCUMENTO/child::RIEPILOGO_MULTISITO_ENERGIA">
			<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" font-family="universcbold" font-size="8.5pt" xsl:use-attribute-sets="blk.004">
			<fo:table-column column-width="proportional-column-width(100)"/>
				<fo:table-body end-indent="0pt" start-indent="0pt">
					<fo:table-row keep-with-next="always" height="19mm">
						<fo:table-cell display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image"><xsl:value-of select="$svg-dettaglio"/></xsl:attribute>
							<xsl:attribute name="color"><xsl:value-of select="$color"/></xsl:attribute>
							
							<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(43)"/>
							<fo:table-column column-width="proportional-column-width(25)"/>
							<fo:table-column column-width="proportional-column-width(15)"/>
							<fo:table-column column-width="proportional-column-width(17)"/>
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											PRODOTTO/SERVIZIO
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::PRODOTTO_SERVIZIO"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											INIZIO FORNITURA
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::INIZIO_FORNITURA"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="pad.r.000">
										<fo:block>
											TIPOLOGIA CONTRATTO
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::TIPOLOGIA_CONTRATTO"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>				
								
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											SEDE FORNITURA
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::SEDE_OPERATIVA"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											TIPOLOGIA MERCATO
											<fo:inline font-family="universc">
												Mercato libero
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:if test="./child::POD">
												POD
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::POD"/>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell xsl:use-attribute-sets="pad.r.000">
										<fo:block>
											<xsl:if test="./child::PRESA">
												PRESA
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::PRESA"/>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>	
								
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											TIPO CONTATORE
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::TIPO_CONTATORE"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											TENSIONE DI ALIM.
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::TENSIONE_ALIMENTAZIONE"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:if test="./child::POTENZA_DISPONIBILE">
												POT. DISP.
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::POTENZA_DISPONIBILE"/>
													kW
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell xsl:use-attribute-sets="pad.r.000">
										<fo:block>
											<xsl:if test="./child::POTENZA_IMPEGNATA">
												POT. IMP.
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::POTENZA_IMPEGNATA"/>
													kW
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>	
							</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>						
				</fo:table-body>
			</fo:table>
		
			<xsl:apply-templates select="./child::LETTURE">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="./child::LETTURE_CONSUMI_CONGUAGLI">
				<xsl:with-param name="bordi" select="$bordi"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="./child::RIEPILOGO_ACCONTI_CONGUAGLIO">
				<xsl:with-param name="bordi" select="$bordi"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="./child::CONSUMI_ENERGIA_ULTIMI_14_MESI">
				<xsl:with-param name="bordi" select="$bordi"/>
			</xsl:apply-templates>
		</xsl:if>
		
		<xsl:variable name="ultima_sezione_reti">
			<xsl:choose>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$ert] and @CONSUMER='NO'">
					<xsl:value-of select="$ert"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$tp]">
					<xsl:value-of select="$tp"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$te]">
					<xsl:value-of select="$te"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$a_uc]">
					<xsl:value-of select="$a_uc"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="ultima_sezione_vendita">
			<xsl:choose>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$ert] and @CONSUMER='SI'">
					<xsl:value-of select="$ert"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$eb_cte]">
					<xsl:value-of select="$eb_cte"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_eb]">
					<xsl:value-of select="$quota_fissa_eb"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$pt]">
					<xsl:value-of select="$pt"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$pc_disp]">
					<xsl:value-of select="$pc_disp"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_ea]">
					<xsl:value-of select="$quota_fissa_ea"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="ultima_sezione_imposte">
			<xsl:choose>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$addizionale_provinciale]">
					<xsl:value-of select="$addizionale_provinciale"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$addizionale_comunale]">
					<xsl:value-of select="$addizionale_comunale"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$imposta_erariale]">
					<xsl:value-of select="$imposta_erariale"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:call-template name="Dettaglio_energia">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
			<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
			<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
			<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
			<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
			<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
			<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
			<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
		</xsl:call-template>
		
		<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
		
			<fo:table-header>
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_header_bold.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_bold.svg)</xsl:attribute>
						</xsl:if>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">		
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.002 chrbold.008">
						<fo:table-column column-width="proportional-column-width(85)"/>
						<fo:table-column column-width="proportional-column-width(15)"/>
						<fo:table-header>
							<fo:table-row keep-with-previous="always" height="{$altezzariga}">
								<fo:table-cell>
									<fo:block>
										TOTALE netto Iva
										<xsl:if test="$lettere_sezioni='SI'">
											<fo:inline xsl:use-attribute-sets="condensedcors.008">
												<xsl:choose>
													<xsl:when test="$ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte=''">
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))
																	or
																	($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte='')">
														(A)
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	not($ultima_sezione_imposte=''))
																	or
																	(not($ultima_sezione_reti='') and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))">
														(A+B)
													</xsl:when>
													<xsl:otherwise>
														(A+B+C)
													</xsl:otherwise>
												</xsl:choose>
											</fo:inline>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="./child::SINTESI_ENERGIA/child::TOTALE_IMPONIBILE"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row keep-with-previous="always" height="{$altezzariga}">
								<fo:table-cell>
									<fo:block>
										IVA su imponibile di euro
										<xsl:if test="$lettere_sezioni='SI'">
											<fo:inline xsl:use-attribute-sets="condensedcors.008">
												<xsl:choose>
													<xsl:when test="$ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte=''">
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))
																	or
																	($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte='')">
														(B)
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	not($ultima_sezione_imposte=''))
																	or
																	(not($ultima_sezione_reti='') and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))">
														(C)
													</xsl:when>
													<xsl:otherwise>
														(D)
													</xsl:otherwise>
												</xsl:choose>
											</fo:inline>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="./child::SINTESI_ENERGIA/child::IVA"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row keep-with-previous="always" height="3mm">
								<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="brd.b.000">
									<fo:block/>
								</fo:table-cell>
							</fo:table-row>
				  
							<fo:table-row keep-with-previous="always" height="{$altezzariga}" xsl:use-attribute-sets="blk.004">
								<fo:table-cell>
									<fo:block>
										TOTALE FORNITURA DI ENERGIA ELETTRICA E IMPOSTE
										<xsl:if test="$lettere_sezioni='SI'">
											<fo:inline xsl:use-attribute-sets="condensedcors.008">
												<xsl:choose>
													<xsl:when test="$ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte=''">
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))
																	or
																	($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte='')">
														(A+B)
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	not($ultima_sezione_imposte=''))
																	or
																	(not($ultima_sezione_reti='') and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))">
														(A+B+C)
													</xsl:when>
													<xsl:otherwise>
														(A+B+C+D)
													</xsl:otherwise>
												</xsl:choose>
											</fo:inline>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="./child::SINTESI_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
						
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell>
									<fo:block/>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_footer_bold.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_bold.svg)</xsl:attribute>
						</xsl:if>
						<fo:block xsl:use-attribute-sets="blk.002">
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<xsl:apply-templates select="SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
			<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
			<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
			<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
		</xsl:apply-templates> 
		
		<xsl:apply-templates select="SEZIONE[@TIPOLOGIA='CANONE_RAI']">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
			<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
			<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
			<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
		</xsl:apply-templates> 		
		
		<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		
			<fo:table-header>
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_header_bold.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_bold.svg)</xsl:attribute>
						</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">				
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(85)"/>
						<fo:table-column column-width="proportional-column-width(15)"/>
							<fo:table-header>
								<!-- <xsl:if test="$bordi='NO'">
									<fo:table-row keep-with-previous="always" height="1mm">
										<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="pad.t.003 pad.l.000 brd.b.spesso">
											<fo:block/>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if> -->
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrTot">	
									<xsl:if test="$bordi='NO'">
										<xsl:attribute name="border-bottom">1.5pt solid black</xsl:attribute>
									</xsl:if>
									
									<fo:table-cell>
										<fo:block>
											TOTALE PUNTO DI PRELIEVO
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_reti='' and
																		$ultima_sezione_vendita='' and
																		$ultima_sezione_imposte=''">
														</xsl:when>
														<xsl:when test="($ultima_sezione_reti='' and
																		$ultima_sezione_vendita='' and
																		not($ultima_sezione_imposte=''))
																		or
																		($ultima_sezione_reti='' and
																		not($ultima_sezione_vendita='') and
																		$ultima_sezione_imposte='')
																		or
																		(not($ultima_sezione_reti='') and
																		$ultima_sezione_vendita='' and
																		$ultima_sezione_imposte='')">
															(A+BXXXXX<xsl:if test="./child::SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']/child::PERIODO_RIFERIMENTO">+C</xsl:if>
																<xsl:if test="./child::SEZIONE[@TIPOLOGIA='CANONE_RAI']/child::PERIODO_RIFERIMENTO">+D</xsl:if>)
														</xsl:when>
														<xsl:when test="($ultima_sezione_reti='' and
																		not($ultima_sezione_vendita='') and
																		not($ultima_sezione_imposte=''))
																		or
																		(not($ultima_sezione_reti='') and
																		not($ultima_sezione_vendita='') and
																		$ultima_sezione_imposte='')
																		or
																		(not($ultima_sezione_reti='') and
																		$ultima_sezione_vendita='' and
																		not($ultima_sezione_imposte=''))">
															(A+B+CXXXX<xsl:if test="./child::SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']/child::PERIODO_RIFERIMENTO">+D</xsl:if>
																  <xsl:if test="./child::SEZIONE[@TIPOLOGIA='CANONE_RAI']/child::PERIODO_RIFERIMENTO">+E</xsl:if>)
														</xsl:when>
														<xsl:otherwise>
															(A+B+C+DXXXX<xsl:if test="./child::SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']/child::PERIODO_RIFERIMENTO">+E</xsl:if>
																	<xsl:if test="./child::SEZIONE[@TIPOLOGIA='CANONE_RAI']/child::PERIODO_RIFERIMENTO">+F</xsl:if>)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./child::SINTESI_ENERGIA/child::TOTALE_DA_PAGARE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-header>
										
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<!-- <xsl:attribute name="background-image">url(<xsl:value-of select="$bordi"/>_footer_bold.svg)</xsl:attribute> -->
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_bold.svg)</xsl:attribute>
						</xsl:if>
						<fo:block xsl:use-attribute-sets="blk.002">
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
  
		<xsl:if test="position()&lt;last()">
			<fo:block break-before="page">
			</fo:block>
		</xsl:if>
	</xsl:for-each>
</xsl:template>
	
</xsl:stylesheet>
