<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" />

	<!--
		******************************************************************* **
		** ** In questo template vengono visualizzati: ** ** - il bollettino
		postale ** ** **
		*******************************************************************
	-->

	<xsl:template name="BOLLETTINO">

		<!--
			************************************************************************************
			** SPAZIO SUPERIORE BOLLETTINO **
			************************************************************************************
		-->

		<fo:page-sequence initial-page-number="1" force-page-count="no-force" master-reference="pm1" id="B">

			<fo:static-content flow-name="xsl-region-before">
				
				<fo:block-container position="absolute" top="10mm"
					left="5mm" width="280mm" height="90mm" xsl:use-attribute-sets="layout-debug font.boll">
					<fo:table table-layout="fixed" width="100%">
						<fo:table-column column-width="22%" />
						<fo:table-column column-width="78%" />
						<fo:table-body>
							<fo:table-row>
								<xsl:choose>
									<!-- Annuncio inoltro richiesta attivazione RID -->
									<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RID_PE_REQ']">
										<fo:table-cell number-columns-spanned="2"
												padding-top="6cm" font-size="10pt"
												line-height="18pt">
											<fo:block>Gentile Cliente,</fo:block>
											<fo:block>
												abbiamo inoltrato alla sua banca la richiesta
												di attivazione della modalità di pagamento
												con addebito tramite RID.
											</fo:block>
											<fo:block font-weight="bold">
												In attesa di conferma da parte della sua banca,
												la informiamo che per il pagamento di questa
												fattura dovrà utilizzare il bollettino postale
												che trova di seguito.
											</fo:block>
											<fo:block font-weight="bold">Grazie!</fo:block>
										</fo:table-cell>
									</xsl:when>
									<xsl:otherwise>
										<fo:table-cell padding-right="3mm"
												border-right="0.5pt dashed black"
												xsl:use-attribute-sets="font.boll"
												line-height="12pt" text-align="justify">
											<xsl:choose>
												<!-- Richiesta attivazione RID respinta dalla banca -->
												<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RID_ERRATO']">
													<fo:block font-size="10pt" font-weight="bold">
														ATTENZIONE! RID RESPINTO
													</fo:block>
													<fo:block space-before="2mm">
														Le inviamo il bollettino postale perché
														<fo:inline font-weight="bold">
															la richiesta di attivazione del RID è stata respinta dalla sua Banca
														</fo:inline>
														a causa di un disallineamento con i dati in nostro possesso.
													</fo:block>
													<fo:block space-before="1.5mm">
														Le ricordiamo che
														<fo:inline font-weight="bold">
															la modalità di pagamento tramite RID è l'unica
															che esclude il pagamento del deposito cauzionale!
														</fo:inline>
													</fo:block>
													<fo:block space-before="1.5mm">
														Comunicando tempestivamente i dati corretti eviterà
														il versamento del deposito; compili e firmi il 
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEI NUOVI ESTREMI RID</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="1.5mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari</fo:block>
														<fo:block space-before="1mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
												<!-- RID revocato dalla banca -->
												<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RID_REVOCATO']">
													<fo:block font-size="10pt" font-weight="bold">
														ATTENZIONE! REVOCA RID
													</fo:block>
													<fo:block space-before="4mm">
														Le inviamo il bollettino postale perché
														<fo:inline font-weight="bold">
															la sua banca ci ha notificato la revoca della posizione RID a lei intestata.
														</fo:inline>
													</fo:block>
													<fo:block space-before="2mm">
														Le ricordiamo che
														<fo:inline font-weight="bold">
															la modalità di pagamento tramite RID è l'unica
															che esclude il pagamento del deposito cauzionale!
														</fo:inline>
													</fo:block>
													<fo:block space-before="2mm">
														Richiedendo tempestivamente il ripristino del RID
														eviterà il versamento del deposito; compili e firmi il
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEI NUOVI ESTREMI RID</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="2mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari</fo:block>
														<fo:block space-before="2mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
												<!-- Avviso per il recupero del deposito cauzionale -->
												<xsl:when test="../DOCUMENTO[@PRESENZA_DEPOSITO_CAUZIONALE='SI']">
													<fo:block font-size="10pt" font-weight="bold">
														RIENTRI IN POSSESSO DEL DEPOSITO CAUZIONALE!
													</fo:block>
													<fo:block space-before="4mm">
														Le ricordiamo che
														<fo:inline font-weight="bold">
															la modalità di pagamento tramite RID
															è l'unica che esclude il pagamento del deposito cauzionale.
														</fo:inline>
													</fo:block>
													<fo:block space-before="3mm">
														Richiedendo il pagamento tramite RID Energit le
														restituirà il deposito versato; compili e firmi il
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEI NUOVI ESTREMI RID</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="3mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari</fo:block>
														<fo:block space-before="3mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
												<!-- Incentivo al RID --> 
												<xsl:when test="not(../DOCUMENTO[@PRESENZA_DEPOSITO_CAUZIONALE='SI'])">
													<fo:block font-size="10pt" font-weight="bold">
														SCELGA LA COMODITA' DEL RID!
													</fo:block>
													<fo:block space-before="4mm">
														Compili e firmi il
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEGLI ESTREMI RID</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="3mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari</fo:block>
														<fo:block space-before="3mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:block />
												</xsl:otherwise>
												
											</xsl:choose>
										</fo:table-cell>
										
										<!-- Modulo estremi rid -->
										<fo:table-cell padding-left="3mm" font-size="10pt">
											<fo:block font-weight="bold" text-align="center">
												MODULO DI COMUNICAZIONE ESTREMI RID – CLIENTE N.
												<xsl:value-of select="CODICE_CLIENTE"/>
											</fo:block>
											<fo:block font-weight="bold">Da</fo:block>
											<!-- prima riga: nome, qualifica -->
											<fo:table table-layout="fixed" width="100%">
												<fo:table-column column-width="4%" />
												<fo:table-column column-width="20%" />
												<fo:table-column column-width="11%" />
												<fo:table-column column-width="2%" />
												<fo:table-column column-width="12%" />
												<fo:table-column column-width="2%" />
												<fo:table-column column-width="18%" />
												<fo:table-column column-width="31%" />
												<fo:table-body>
													<fo:table-cell>
														<fo:block font-weight="bold">Sig.</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>&#160;&#160;in qualità di</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															<fo:external-graphic src="url(svg/unchecked.svg)"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															Cliente privato
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															<fo:external-graphic src="url(svg/unchecked.svg)"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															Rappr. Legale Azienda
														</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
												</fo:table-body>
											</fo:table>
											
											
											<!-- seconda riga: indirizzo -->
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
												<fo:table-column column-width="4%" />
												<fo:table-column column-width="30%" />
												<fo:table-column column-width="5%" />
												<fo:table-column column-width="20%" />
												<fo:table-column column-width="7%" />
												<fo:table-column column-width="17%" />
												<fo:table-column column-width="5%" />
												<fo:table-column column-width="12%" />
												<fo:table-body>
													<fo:table-cell>
														<fo:block>Via.</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>Citta</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>Tel/Fax</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>email</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
												</fo:table-body>
											</fo:table>
											
											
											<fo:block text-align="center" font-weight="bold" space-before="2mm">
												Con la presente il sottoscritto, quale intestatario
												del servizio energia elettrica Energit, comunica
												di seguito gli estremi per il pagamento delle prossime
												fatture Energit con modalità Addebito diretto su
												conto corrente (RID):
											</fo:block>
											
											 
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
												<fo:table-column column-width="17%" />
												<fo:table-column column-width="30.5%" />
												<fo:table-column column-width="6%" />
												<fo:table-column column-width="46.5%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>Intestazione del conto</fo:block>
														</fo:table-cell>
														<fo:table-cell xsl:use-attribute-sets="cell.field">
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>&#160;&#160;IBAN&#160;</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<fo:external-graphic src="url(svg/ibanfield.svg)"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
												<fo:table-column column-width="5.5%" />
												<fo:table-column column-width="42%" />
												<fo:table-column column-width="13%" />
												<fo:table-column column-width="39.5%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>Banca</fo:block>
														</fo:table-cell>
														<fo:table-cell xsl:use-attribute-sets="cell.field">
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>&#160;&#160;Agenzia/Filiale&#160;</fo:block>
														</fo:table-cell>
														<fo:table-cell xsl:use-attribute-sets="cell.field">
															<fo:block />
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											
											<fo:block text-align="justify" font-size="6pt" space-before="2mm">
												Il sottoscritto autorizza la Banca a margine a provvedere
												alla estinzione dei documenti di debito (fatture, ricevute,
												bollette, ecc.) emessi da Energ.it S.p.A. sopra citata,
												addebitando il conto sopraindicato e applicando le condizioni
												indicate nel foglio informativo analitico posto a disposizione
												del correntista e le norme previste per il servizio senza necessità,
												per la Banca, di inviare relativa contabile di addebito.
												Dichiara di essere a conoscenza che la Banca assume l'incarico
												dell'estinzione dei citati documenti che Energ.it S.p.A. invierà
												direttamente al debitore, prima della scadenza dell'obbligazione,
												a condizione che, al momento del pagamento, il conto sia in essere
												e assicuri la disponibilità sufficiente e che non sussistano
												ragioni che ne impediscono l'utilizzazione. In caso contrario
												la Banca resterà esonerata da ogni e qualsiasi responsabilità
												inerente al mancato pagamento e il pagamento stesso dovrà essere
												effettuato a Energ.it S.p.A. direttamente a cura del debitore.
												Il sottoscritto prende altresì atto che la Banca si riserva il
												diritto di recedere in ogni momento dal presente accordo. Prende
												pure atto che, ove intenda eccezionalmente sospendere l'estinzione
												di un documento di debito, dovrà dare immediato avviso alla Banca
												in tal senso entro la data di scadenza. Per quanto non espressamente
												richiamato, si applicano le “Norme che regolano i conti correnti di
												corrispondenza dei servizi connessi”: in deroga al terzo comma si
												conviene che il sottoscritto può riservarsi il diritto di chiedere
												alla Banca lo storno dell'addebito entro 5 giorni lavorativi
												dalla scadenza dell'obbligazione.
											</fo:block>
											
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
												<fo:table-column column-width="32%" />
												<fo:table-column column-width="68%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>Cod. Fiscale correntista (persona Fisica)</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<!-- TODO: nuovo svg per il codice fiscale -->
																<fo:external-graphic src="url(svg/cffield.svg)"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm" font-weight="bold">
												<fo:table-column column-width="50%" />
												<fo:table-column column-width="50%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
																<fo:table-column column-width="35%" />
																<fo:table-column column-width="65%" />
																<fo:table-body>
																	<fo:table-row>
																		<fo:table-cell>
																			<fo:block>Firma del correntista</fo:block>
																		</fo:table-cell>
																		<fo:table-cell xsl:use-attribute-sets="cell.field">
																			<fo:block />
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
															
															<fo:table table-layout="fixed" width="100%" space-before="3mm">
																<fo:table-column column-width="56%" />
																<fo:table-column column-width="44%" />
																<fo:table-body>
																	<fo:table-row>
																		<fo:table-cell>
																			<fo:block>Firma dell'intestatario del contratto</fo:block>
																		</fo:table-cell>
																		<fo:table-cell xsl:use-attribute-sets="cell.field">
																			<fo:block />
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											<fo:block font-weight="bold">
												<fo:block>IMPORTANTE</fo:block>
												<fo:block>Allegare copia del documento di identità del correntista</fo:block>
												<fo:block font-size="6pt">(se diverso dall'intestatario del contratto)</fo:block>
											</fo:block>
											
										</fo:table-cell>
									</xsl:otherwise>
								</xsl:choose>
								
								
								
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block-container>
			</fo:static-content>


			<fo:flow flow-name="xsl-region-body" xsl:use-attribute-sets="text.font">
			
				<!-- Corpo bollettino -->
				<xsl:for-each select="BOLLETTINO">
				
					<fo:block-container position="absolute" top="-4mm"
						left="0mm" width="297mm" height="102mm">
	
						<!-- Ricevuta di Versamento - body -->
						<fo:block-container position="absolute" top="0mm"
							left="0mm" width="132.5mm" height="102mm">
	
							<!-- Ricevuta di Versamento - header -->
							<fo:block-container position="absolute" top="0mm"
								left="0mm" width="132mm" height="4mm" xsl:use-attribute-sets="font.boll"
								display-align="after" border-top="0.5pt solid black"
								border-bottom="0.5pt solid black" background-color="#CCCCCC">
	
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="6%" />
									<fo:table-column column-width="60%" />
									<fo:table-column column-width="30%" />
									<fo:table-column column-width="4%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block />
											</fo:table-cell>
											<fo:table-cell text-align="left">
												<fo:block>CONTI CORRENTI POSTALI - Ricevuta di Versamento
												</fo:block>
											</fo:table-cell>
											<fo:table-cell text-align="right">
												<fo:block>
													Banco<fo:inline font-weight="bold">Posta</fo:inline>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block />
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - logo -->
							<fo:block-container position="absolute" top="5mm"
								left="4.5mm" width="27mm" height="12mm"
								display-align="after" text-align="left"
								xsl:use-attribute-sets="layout-debug">
								<fo:block>
									<fo:external-graphic src="url(img/logo_energit_bn.png)" content-width="25mm" />
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - euro box -->
							<fo:block-container position="absolute" top="7.8mm" display-align="after"
								left="37.5mm" width="7mm" height="7mm" xsl:use-attribute-sets="layout-debug">
								<fo:block>
									<fo:external-graphic src="url(img/euro.png)" content-width="7mm" content-height="7mm" />
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - "sul C/C n. ..." -->
							<fo:block-container position="absolute" top="7.7mm"
								left="45.5mm" width="8mm" height="6mm" xsl:use-attribute-sets="font.boll layout-debug"
								line-height="8pt">
								<fo:block>sul</fo:block>
								<fo:block>C/C n.</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - numero conto corrente -->
							<fo:block-container position="absolute" top="9mm"
								left="54mm" width="26mm" height="6mm" xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>35291152</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - "di Euro..." -->
							<fo:block-container position="absolute" top="10.3mm"
								left="81.2mm" width="10mm" height="4mm" xsl:use-attribute-sets="font.boll layout-debug">
								<fo:block>di Euro:</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - importo (in euro) -->
							<fo:block-container position="absolute" top="9mm"
								left="93.5mm" width="33mm" height="6mm" xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>
									<xsl:value-of select="TOT_COST_EUR" />
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - intestazione beneficiario -->
							<fo:block-container position="absolute" top="27mm"
								left="5.8mm" width="119mm" height="12mm" display-align="center"
								xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.boll">Intestato a:</fo:block>
								<fo:block xsl:use-attribute-sets="font.codeline">ENERG.IT S.p.A.
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - zona cliente top -->
							<fo:block-container position="absolute" top="32mm"
								left="5.8mm" width="119mm" height="12mm" xsl:use-attribute-sets="layout-debug">
								<fo:block />
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - zona cliente middle -->
							<fo:block-container position="absolute" top="34mm"
								left="5.8mm" width="66mm" height="34mm" display-align="center"
								text-align="left" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.boll">Eseguito da:</fo:block>
								<fo:block space-before="2mm" xsl:use-attribute-sets="font.eseg">
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/INTESTATARIO" />
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - zona cliente bottom -->
							<fo:block-container position="absolute" top="82.2mm"
								left="7.3mm" width="119mm" height="12mm" display-align="center"
								text-align="left" xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>
									Causale del versamento:
									<xsl:value-of select="./ancestor::DOCUMENTO/@NUMERO_DOCUMENTO" />
								</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - stringa causale -->
							<fo:block-container position="absolute" top="76.7mm"
								left="5.8mm" width="35mm" height="5mm" display-align="center"
								text-align="left" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.boll">Causale:</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - stringa bollo postale -->
							<fo:block-container position="absolute" top="78mm"
								left="69.5mm" width="55mm" height="5mm" display-align="center"
								text-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.boll2">BOLLO DELL'UFFICIO POSTALE</fo:block>
								<fo:block xsl:use-attribute-sets="font.boll2" font-size="4">&#160;&#160;</fo:block>
							</fo:block-container>
	
						</fo:block-container>
	
						<!-- Ricevuta di Accredito - body -->
						<fo:block-container position="absolute" top="0mm"
							left="132.5mm" width="165mm" height="102mm" border-left="0.5pt solid black">
	
							<!-- Ricevuta di Accredito - header -->
							<fo:block-container position="absolute" top="0mm"
								left="0mm" width="165mm" height="4mm" xsl:use-attribute-sets="font.boll"
								display-align="after" border-top="0.5pt solid black"
								border-bottom="0.5pt solid black" border-left="0.5pt solid black"
								background-color="#CCCCCC">
	
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="4.5%" />
									<fo:table-column column-width="61.5%" />
									<fo:table-column column-width="30%" />
									<fo:table-column column-width="4%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block />
											</fo:table-cell>
											<fo:table-cell text-align="left">
												<fo:block>CONTI CORRENTI POSTALI - Ricevuta di Accredito
												</fo:block>
											</fo:table-cell>
											<fo:table-cell text-align="right">
												<fo:block>
													Banco<fo:inline font-weight="bold">Posta</fo:inline>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block />
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
	
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - euro logo -->
							<fo:block-container position="absolute" top="7.8mm" display-align="after"
								left="7.5mm" width="7mm" height="7mm" xsl:use-attribute-sets="layout-debug">
								<fo:block>
									<fo:external-graphic src="url(img/euro.png)" content-width="7mm" content-height="7mm" />
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - "sul C/C n. ..." -->
							<fo:block-container position="absolute" top="10.3mm"
								left="17mm" width="13mm" height="4mm" xsl:use-attribute-sets="font.boll layout-debug">
								<fo:block>sul C/C n.</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - numero conto corrente -->
							<fo:block-container position="absolute" top="9mm"
								left="33.7mm" width="25mm" height="6mm" xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>35291152</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - "di Euro ..." -->
							<fo:block-container position="absolute" top="10.3mm"
								left="104mm" width="10mm" height="4mm" xsl:use-attribute-sets="font.boll layout-debug">
								<fo:block>di Euro:</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - importo (in euro) -->
							<fo:block-container position="absolute" top="9mm"
								left="115.5mm" width="25mm" height="6mm"
								xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>
									<xsl:value-of select="TOT_COST_EUR" />
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - tipo documento -->
							<fo:block-container position="absolute" top="15.5mm"
								left="7mm" width="15mm" height="6mm"
								xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>TD 896</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - intestazione -->
							<fo:block-container position="absolute" top="24mm"
								left="7mm" width="142mm" height="8.5mm" display-align="center"
								xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.boll">Intestato a:</fo:block>
								<fo:block xsl:use-attribute-sets="font.codeline">ENERG.IT S.p.A.
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - codice cliente -->
							<fo:block-container position="absolute" top="40.5mm"
								left="7mm" width="47.5mm" height="6mm"
								xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>
									<xsl:value-of select="OCR_FATT" />
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - stringa bollo postale -->
							<fo:block-container position="absolute" top="78mm"
								left="9mm" width="55mm" height="5mm" display-align="center"
								text-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.boll2">BOLLO DELL'UFFICIO POSTALE
								</fo:block>
								<fo:block xsl:use-attribute-sets="font.boll2" font-size="5">codice
									cliente</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - zona cliente -->
							<fo:block-container position="absolute" top="35.5mm"
								left="61mm" width="95mm" height="42mm" display-align="center"
								text-align="left" margin="1mm" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.boll">Eseguito da:</fo:block>
								<fo:block space-before="1mm" xsl:use-attribute-sets="font.eseg.all">
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/INTESTATARIO" />
								</fo:block>
								<fo:block space-before="1mm" xsl:use-attribute-sets="font.eseg.all">
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/INDIRIZZO" />
								</fo:block>
								<fo:block space-before="1mm" xsl:use-attribute-sets="font.eseg.all">
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/CAP" />
									&#160;&#160;&#160;&#160;
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/CITTA" />
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - "non scrivere nella zona sottostante" -->
							<fo:block-container position="absolute" top="78mm"
								left="56mm" width="95mm" height="5mm" display-align="center"
								xsl:use-attribute-sets="font.eseg layout-debug">
								<fo:block xsl:use-attribute-sets="font.eseg" text-align="center">
									&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;IMPORTANTE:
									NON SCRIVERE NELLA ZONA SOTTOSTANTE</fo:block>
								<fo:table table-layout="fixed" width="100%" font-size="5">
									<fo:table-column column-width="34%" />
									<fo:table-column column-width="34%" />
									<fo:table-column column-width="29%" />
									<fo:table-column column-width="3%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block />
											</fo:table-cell>
											<fo:table-cell>
												<fo:block>&#160;importo in euro</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block>&#160;&#160;&#160;&#160; numero conto</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block>&#160;&#160;td</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - zona autorizzazione -->
							<fo:block-container reference-orientation="90"
								position="absolute" top="30mm" left="158.5mm" width="40mm" height="9mm"
								text-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.auth">Aut. DB/SSIC/E 7271 DEL
									12/03/2002</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - zona codeline -->
							<!-- <fo:block-container position="absolute" top="83.2mm"
								left="0mm" width="165mm" height="19mm" border-top="0.5pt solid black"
								display-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.codeline" line-height="0pt" padding-left="-1.2mm" padding-top="-5mm" padding-bottom="0mm">
									<fo:inline>&#160;&#160;&#160;&lt;<xsl:value-of select="OCR_FATT" />&gt;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="OCR_EUR" />&gt;&#160;&#160;&#160;35291152&lt;&#160;&#160;896&gt;</fo:inline>
								</fo:block>
							</fo:block-container> -->
							
							<fo:block-container position="absolute" top="83.2mm"
								left="0mm" width="165mm" height="19mm" border-top="0.5pt solid black"
								display-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block/>
							</fo:block-container>
							
							<fo:block-container position="absolute" top="83.2mm"
								left="7.3mm" width="165mm" height="19mm"
								display-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.codeline" line-height="0pt" padding-left="-2mm" padding-top="1.8mm" padding-bottom="0mm">
									<fo:inline>&lt;<xsl:value-of select="OCR_FATT" />&gt;</fo:inline>
								</fo:block>
							</fo:block-container>
							
							<fo:block-container position="absolute" top="83.2mm"
								left="80.9mm" width="165mm" height="19mm" border-top="0.5pt solid black"
								display-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.codeline" line-height="0pt" padding-left="-1.2mm" padding-top="1.8mm" padding-bottom="0mm">
									<fo:inline><xsl:value-of select="OCR_EUR" />&gt;</fo:inline>
								</fo:block>
							</fo:block-container>
							
							<fo:block-container position="absolute" top="83.2mm"
								left="119.1mm" width="165mm" height="19mm" border-top="0.5pt solid black"
								display-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.codeline" line-height="0pt" padding-left="-1.2mm" padding-top="1.8mm" padding-bottom="0mm">
									<fo:inline>35291152&lt;</fo:inline>
								</fo:block>
							</fo:block-container>
							
							<fo:block-container position="absolute" top="83.2mm"
								left="147.1mm" width="165mm" height="19mm" border-top="0.5pt solid black"
								display-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.codeline" line-height="0pt" padding-left="-1.2mm" padding-top="1.8mm" padding-bottom="0mm">
									<fo:inline>896&gt;</fo:inline>
								</fo:block>
							</fo:block-container>
	
						</fo:block-container>
	
					</fo:block-container>
				
				
				</xsl:for-each>

			</fo:flow>

		</fo:page-sequence>


	</xsl:template>



	<xsl:attribute-set name="text.font">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.boll">
		<xsl:attribute name="font-family">Arial, sans-serif</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.boll2">
		<xsl:attribute name="font-family">Arial, sans-serif</xsl:attribute>
		<xsl:attribute name="font-size">6pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="layout-debug">
		<xsl:attribute name="background-color">transparent</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.codeline">
		<xsl:attribute name="font-family">ocrb</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.eseg">
		<xsl:attribute name="font-family">Arial, sans-serif</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-size">6pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.eseg.all">
		<xsl:attribute name="font-family">Arial, sans-serif</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.auth">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">6pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.field">
		<xsl:attribute name="border-bottom">1pt solid black</xsl:attribute>
	</xsl:attribute-set>


</xsl:stylesheet>

