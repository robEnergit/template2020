<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:fox="http://xml.apache.org/fop/extensions">
	<xsl:output encoding="UTF-8" />


	<xsl:template name="AUTOCERTIFICAZIONE">

		<fo:page-sequence initial-page-number="1"
			master-reference="pm0-blank" orphans="2" widows="2">
			<fo:flow flow-name="body">
				<fo:block>


					<fo:block xsl:use-attribute-sets="blk.indirizzo">
						<fo:leader line-height="3.1cm" />
					</fo:block>



					<fo:table end-indent="-3.5pt" start-indent="-3.5pt"
						table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(60)" />
						<fo:table-column column-width="proportional-column-width(40)" />


						<fo:table-body end-indent="0pt" start-indent="0pt">

							<fo:table-row>
								<fo:table-cell>
									<fo:block text-align="end">
										<fo:inline xsl:use-attribute-sets="blk.font.018">da restituire a: </fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.019">
										<fo:inline xsl:use-attribute-sets="blk.font.019">Energ.it S.p.A.</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>

							<fo:table-row>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.020">
										<fo:leader line-height="10pt" />
									</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.020">
										<fo:leader line-height="10pt" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>

							<fo:table-row>
								<fo:table-cell>
									<fo:block />
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.019">
										<fo:inline xsl:use-attribute-sets="blk.font.018">Via Efisio Melis
											26</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
									<fo:block />
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.019">
										<fo:inline xsl:use-attribute-sets="blk.font.018">09134 Cagliari</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
									<fo:block />
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.019">
										<fo:inline xsl:use-attribute-sets="blk.font.018">tel. 800.1922.22</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
									<fo:block />
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.019">
										<fo:inline xsl:use-attribute-sets="blk.font.018">fax 800.1922.55</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>

						</fo:table-body>
					</fo:table>


					<fo:block xsl:use-attribute-sets="blk.021">

						<fo:inline xsl:use-attribute-sets="blk.font.018">


							<fo:block xsl:use-attribute-sets="blk.indirizzo">
								<fo:leader line-height="3.1cm" />
							</fo:block>

							<fo:block xsl:use-attribute-sets="blk.017">
								<fo:inline xsl:use-attribute-sets="blk.font.019">AUTOCERTIFICAZIONE
									SUI REQUISITI PER IL SERVIZIO DI SALVAGUARDIA</fo:inline>
							</fo:block>
							<fo:block xsl:use-attribute-sets="blk.017">
								<fo:leader line-height="12pt" />
							</fo:block>

							<fo:block xsl:use-attribute-sets="blk.017">
								Il sottoscritto, _________________________________, nato a
								____________________________ il____________ , cod. fiscale
								_____________________ , in qualità di legale rappresentante
								di/del/della
								<xsl:value-of select="./child::ANAGRAFICA_CLIENTE/child::INTESTATARIO" />
								dichiara sotto la propria responsabilità, come certificato
								dall’ultimo bilancio approvato che, ai sensi del DL. 73 del 18
								giugno 2007, l’impresa della quale è titolare ha:
							</fo:block>

							<fo:block xsl:use-attribute-sets="blk.017">
								<fo:leader line-height="12pt" />
							</fo:block>

							<fo:block xsl:use-attribute-sets="blk.017">
								<fo:external-graphic src="url(img/check_box.jpg)"
									content-width="3mm" />
									Più di 50 dipendenti
							</fo:block>

							<fo:block xsl:use-attribute-sets="blk.017">
								<fo:leader line-height="12pt" />
							</fo:block>

							<fo:block xsl:use-attribute-sets="blk.017">
								<fo:external-graphic src="url(img/check_box.jpg)"
									content-width="3mm" />
								Un fatturato annuo superiore a 10 milioni di euro
							</fo:block>

							<fo:block xsl:use-attribute-sets="blk.017">
								<fo:leader line-height="12pt" />
							</fo:block>

						</fo:inline>

					</fo:block>



					<fo:block xsl:use-attribute-sets="blk.indirizzo">
						<fo:leader line-height="3.1cm" />
					</fo:block>



					<fo:table end-indent="-3.5pt" start-indent="-3.5pt"
						table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(60)" />
						<fo:table-column column-width="proportional-column-width(40)" />

						<fo:table-body end-indent="0pt" start-indent="0pt">

							<fo:table-row>
								<fo:table-cell>
									<fo:block />
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.019">
										<fo:inline xsl:use-attribute-sets="blk.font.018">In fede</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>

						</fo:table-body>
					</fo:table>




					<fo:block xsl:use-attribute-sets="blk.indirizzo">
						<fo:leader line-height="3.1cm" />
					</fo:block>



					<fo:table end-indent="-3.5pt" start-indent="-3.5pt"
						table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(60)" />
						<fo:table-column column-width="proportional-column-width(40)" />

						<fo:table-body end-indent="0pt" start-indent="0pt">

							<fo:table-row>
								<fo:table-cell>
									<fo:block />
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.019">
										<fo:inline xsl:use-attribute-sets="blk.font.018">_____________________</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>

							<fo:table-row>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.020">
										<fo:leader line-height="10pt" />
									</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.020">
										<fo:leader line-height="10pt" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>

							<fo:table-row>
								<fo:table-cell>
									<fo:block />
								</fo:table-cell>
								<fo:table-cell>
									<fo:block xsl:use-attribute-sets="blk.019">
										<fo:inline xsl:use-attribute-sets="blk.font.018">Timbro e firma</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>

						</fo:table-body>
					</fo:table>


					<!-- 
				<xsl:call-template name="AUTOCERTIFICAZIONE" />
				 -->
				</fo:block>
			</fo:flow>
		</fo:page-sequence>


	</xsl:template>


	

	

	

	

	
<xsl:attribute-set name="blk.017">
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-align">justify</xsl:attribute>
	<xsl:attribute name="line-height">20pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.019">
	<xsl:attribute name="start-indent">0.5cm</xsl:attribute>
	<xsl:attribute name="end-indent">0.5cm</xsl:attribute>
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="line-height">12pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.020">
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="line-height">10pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.font.019">
	<xsl:attribute name="font-family">Arial, sans-serif</xsl:attribute>
	<xsl:attribute name="font-size">10pt</xsl:attribute>
	<xsl:attribute name="font-weight">bold</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.font.018">
	<xsl:attribute name="font-family">Arial, sans-serif</xsl:attribute>
	<xsl:attribute name="font-size">10pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="blk.indirizzo">
	<xsl:attribute name="line-height">3.1cm</xsl:attribute>
</xsl:attribute-set>
	
<xsl:attribute-set name="blk.021">
	<xsl:attribute name="keep-with-next.within-column">always</xsl:attribute>
	<xsl:attribute name="start-indent">1cm</xsl:attribute>
	<xsl:attribute name="end-indent">1.5cm</xsl:attribute>
	<xsl:attribute name="text-indent">0pt</xsl:attribute>
	<xsl:attribute name="text-align">start</xsl:attribute>
	<xsl:attribute name="line-height">12pt</xsl:attribute>
</xsl:attribute-set>

</xsl:stylesheet>

