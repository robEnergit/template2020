<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" />

	<!--
		**************************************************************************************
		** 			Realizzato da Marini Pierluigi e Michele Onnis 					    	**
		**			In questo template vengono visualizzati: 								**
		**        	la comunicazione sullenuove condizioni generali di contratto 			**
		** 																					**
		**************************************************************************************
	-->


	<xsl:template name="COMUNICAZIONE_CONDIZIONI_C11">
	
	
		<fo:page-sequence initial-page-number="1"
			force-page-count="no-force"
			master-reference="pm0_comunicazione_condizioni"
			orphans="2" widows="2">
	        
	        <fo:static-content flow-name="header">
				<!--
					blocco vuoto, serve come punto di riferimento per distanziare il
					blocco successivo
				-->
				<fo:block />
			
				<!--  -->
				<fo:block space-before="15mm" start-indent="29mm">
					<fo:external-graphic src="img/logo-energit-BN.jpg"
						content-height="9mm" />
				</fo:block>
			</fo:static-content>
			
			<fo:static-content flow-name="footer">
				<fo:block font-family="Arial" font-size="7pt" line-height="8pt">
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="85%" />
						<fo:table-column column-width="15%" />
						<fo:table-body table-layout="fixed" width="100%">
							<fo:table-row>
								<fo:table-cell border-right="0.8pt solid black" padding-right="1mm">
									<fo:block text-align="right">
										<fo:inline font-weight="bold">ENERG.IT</fo:inline>
										S.p.A.
									</fo:block>
									<fo:block text-align="right">Società per Azioni con Socio Unico.
									</fo:block>
									<fo:block text-align="right">Direzione e Coordinamento di Alpiq
										Italia S.r.l.</fo:block>
									<fo:block text-align="right">
										Via Edward Jenner, 19/21
										<xsl:text>&#160;&#160;&#160;</xsl:text>09121 Cagliari<xsl:text>&#160;&#160;&#160;</xsl:text>Servizio Clienti 800.19.22.22<xsl:text>&#160;&#160;&#160;</xsl:text>Fax 800.19.22.55
									</fo:block>
									<fo:block text-align="right">Iscrizione CCIAA di Cagliari n° 02605060926 del 12/08/00<xsl:text>&#160;&#160;&#160;</xsl:text>Capitale Sociale euro 1.000.000 i.v.</fo:block>
									<fo:block text-align="right">P. IVA 02605060926</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="1mm">
									<fo:block>&#160;</fo:block>
									<fo:block>&#160;</fo:block>
									<fo:block>&#160;</fo:block>
									<fo:block>&#160;</fo:block>
									<fo:block font-weight="bold" text-align="left">www.energit.it</fo:block>
									<fo:block font-weight="bold" text-align="left">info@energit.it</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:static-content>
	        
	        <fo:flow flow-name="body">
	        	<!-- 
	            <xsl:call-template name="COMUNICAZIONE_CONDIZIONI"/>
	             -->
	             
	             <fo:block xsl:use-attribute-sets="font.text">
	             
					<fo:block space-before="10pt">Gentile Cliente,</fo:block>
					
					
					<fo:block space-before="20pt">facciamo riferimento alle condizioni
					generali di contratto per la fornitura di energia elettrica e dei
					servizi associati da Lei stipulate con la Energ.it S.p.A., come
					attualmente in vigore (le “<fo:inline xsl:use-attribute-sets="chr.riepilogo10bold">Condizioni Generali</fo:inline>”),
					per la somministrazione in Suo favore di energia elettrica presso il/i Sito/i.</fo:block>
					
					<fo:block>Ogni temine con l'iniziale maiuscola utilizzato nella presente comunicazione ma quivi non
					altrimenti definito avrà il medesimo significato ad esso attribuito dalle Condizioni Generali.</fo:block>
					
					<fo:block space-before="10pt">Al fine di meglio gestire la fornitura del Servizio da parte nostra,
					con particolare riferimento ai profili tecnici ad essa connessi, nonché in conformità alle nuove disposizioni
					introdotte dall’Autorità per l’Energia Elettrica ed il Gas, abbiamo provveduto ad aggiornare le Condizioni
					Generali che regolano la fornitura del Servizio da parte nostra tramite l'adozione di un nuovo testo di
					Condizioni Generali (le “<fo:inline xsl:use-attribute-sets="chr.riepilogo10bold">Nuove Condizioni
					Generali</fo:inline>”).</fo:block>
					
					<fo:block space-before="10pt">Le <fo:inline text-decoration="underline">Nuove Condizioni Generali troveranno integrale
					applicazione e regolamenteranno il rapporto contrattuale tra noi in vigore a partire dalla data 
					del <xsl:value-of select="./child::CONTRATTO_GAS[@NUOVE_CONDIZIONI='C11']/attribute::DATA_NUOVE_CONDIZIONI"/></fo:inline>.
					A partire da tale data, le Condizioni Generali non troveranno ulteriore applicazione e saranno integralmente sostituite
					dalle Nuove Condizioni Generali. Le segnaliamo che, in ogni caso, l'adozione ed implementazione delle Nuove Condizioni
					GeneraliA partire da tale data, le Condizioni Generali non troveranno ulteriore applicazione e saranno integralmente
					sostituite dalle Nuove Condizioni Generali. Le segnaliamo che, in ogni caso, l'adozione ed implementazione delle Nuove
					Condizioni Generali <fo:inline text-decoration="underline">non comporta alcuna modifica al
					corrispettivo attualmente previsto dalle Condizioni Generali né il pagamento di costi od oneri aggiuntivi a Suo
					carico.</fo:inline></fo:block>
					
					<fo:block space-before="10pt">In conformità a quanto previsto all’articolo 12 della delibera dell’AEEG n. 105/06, in
					allegato alla presente comunicazione, potrà trovare una scheda riepilogativa delle specifiche novità introdotte dalle
					Nuove Condizioni Generali in relazione al Servizio rispetto alle Condizioni Generali attualmente in
					essere (<fo:inline text-decoration="underline">Allegato “A”</fo:inline>) unitamente al testo
					integrale delle Nuove Condizioni Generali (<fo:inline text-decoration="underline">Allegato
					“B”</fo:inline>).</fo:block>
					
					<fo:block space-before="10pt">Le ricordiamo inoltre che, qualora non intenda accettare l’applicazione delle Nuove
					Condizioni Generali al rapporto contrattuale attualmente in essere con Energ.it S.p.A., avrà in ogni caso il diritto
					di recedere dalle Condizioni Generali, senza penali e con effetto immediato, mediante comunicazione scritta ai seguenti
					indirizzi:</fo:block>
					<fo:block>a) se mediante il servizio postale, a Energ.it S.p.A., via Edward Jenner 19/21, 09121 Cagliari;</fo:block>
					<fo:block>b) se a mezzo fax, al numero gratuito 800.1922.55.</fo:block>
					
					<fo:block space-before="10pt">Tale comunicazione dovrà pervenire ad Energit entro e non oltre 60 giorni dalla
					data di trasmissione della presente comunicazione. In mancanza di esercizio del diritto di recesso entro il suddetto
					termine, le Nuove Condizioni Generali troveranno integrale applicazione al rapporto contrattuale in essere tra Lei
					ed Energ.it S.p.A.</fo:block>
					
					<fo:block space-before="10pt">Per qualunque quesito o chiarimento su quanto precede, La invitiamo a contattare il
					nostro Servizio Clienti al numero telefonico gratuito 800.19.22.22, attivo dal lunedì al venerdì dalle 8.30 alle
					18.00.</fo:block>
					
					<fo:block space-before="30pt">Energ.it S.p.A.</fo:block>
				</fo:block>
	             
	        </fo:flow>
	    </fo:page-sequence>

	</xsl:template>

	<!--
		******************************************************************************************************************
		** Definizione attributi **
		******************************************************************************************************************
	-->

	<xsl:attribute-set name="font.text">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
		<xsl:attribute name="line-height">12.5pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="blk.space">
		<xsl:attribute name="space-before">4mm</xsl:attribute>
	</xsl:attribute-set>

</xsl:stylesheet>

