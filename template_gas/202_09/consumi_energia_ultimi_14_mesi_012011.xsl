<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
********************************************************
**                                                    **
**          CONSUMI ENERGIA ULTIMI 12 MESI            **
**                                                    **
********************************************************
-->
<xsl:template match="CONSUMI_ENERGIA_ULTIMI_14_MESI">
<xsl:param name="bordi"/>
<xsl:param name="color"/>
<xsl:param name="svg-titoli"/>
<xsl:param name="tipo"/>
<xsl:param name="mese_fattura"/>

<xsl:if test="$tipo='header'">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
	<fo:table-column column-width="proportional-column-width(14)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(9)"/>
	<fo:table-body>
		<fo:table-row keep-with-previous="always" height="2mm">
			<fo:table-cell number-columns-spanned="13">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row height="6mm">
			<fo:table-cell padding-left="3mm" number-columns-spanned="13" display-align="center">
				<xsl:attribute name="background-image"><xsl:value-of select="$svg-titoli"/></xsl:attribute>
				<fo:block text-align="start">
					<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">CONSUMI ENERGIA DEGLI ULTIMI 12 MESI </fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row keep-with-previous="always" height="1mm">
			<fo:table-cell number-columns-spanned="13">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row keep-with-previous="always" height="3mm">
			<fo:table-cell padding-left="3mm">
				<fo:block text-align="start">
					<fo:inline>PERIODO</fo:inline>
				</fo:block>
			</fo:table-cell>
									
			<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.l.ultimi_consumi">
				<fo:block>
					<fo:inline>ENERGIA ATTIVA (kWh)</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.l.ultimi_consumi">
				<fo:block>
					<fo:inline>ENERGIA REATTIVA (kVARh)</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.l.ultimi_consumi">
				<fo:block>
					<fo:inline>POTENZA MAX (kW)</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell number-columns-spanned="3" xsl:use-attribute-sets="brd.l.ultimi_consumi">
				<fo:block>
					<fo:inline>CONSUMO MEDIO GIORNALIERO (kWh)</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row keep-with-previous="always" height="4mm">
			<fo:table-cell>
				<fo:block>
					<fo:inline></fo:inline>
				</fo:block>
			</fo:table-cell>
									
			<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
				<fo:block>
					<fo:inline>Fascia 1</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>Fascia 2</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>Fascia 3</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
				<fo:block>
					<fo:inline>Fascia 1</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>Fascia 2</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>Fascia 3</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
				<fo:block>
					<fo:inline>Fascia 1</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>Fascia 2</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>Fascia 3</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
				<fo:block>
					<fo:inline>Fascia 1</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>Fascia 2</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<fo:inline>Fascia 3</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
	</fo:table-body>
	</fo:table>
</xsl:if>

<xsl:if test="$tipo='body'">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
	<fo:table-column column-width="proportional-column-width(14)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(7)"/>
	<fo:table-column column-width="proportional-column-width(9)"/>			
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<xsl:for-each select="./child::CONSUMO">
			<fo:table-row>
					<fo:table-cell padding-left="3mm">
						<fo:block text-align="start">
							<fo:inline>
								<xsl:value-of select="@DESCRIZIONE_MESE_RIFERIMENTO"/>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
						
					<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F1']/child::ENERGIA_ATTIVA">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F1']/child::ENERGIA_ATTIVA"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F2']/child::ENERGIA_ATTIVA">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F2']/child::ENERGIA_ATTIVA"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_ATTIVA">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_ATTIVA"/></fo:inline>
								</xsl:when>
								<xsl:when test="./child::FASCIA/child::ENERGIA_ATTIVA">
									<fo:inline><xsl:value-of select="./child::FASCIA/child::ENERGIA_ATTIVA"/>*<xsl:if test="./ancestor::CONSUMI_ENERGIA_ULTIMI_14_MESI[@STIME_PRESENTI='SI']">*</xsl:if>
									</fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F1']/child::ENERGIA_REATTIVA">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F1']/child::ENERGIA_REATTIVA"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F2']/child::ENERGIA_REATTIVA">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F2']/child::ENERGIA_REATTIVA"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_REATTIVA">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_REATTIVA"/></fo:inline>
								</xsl:when>
								<xsl:when test="./child::FASCIA/child::ENERGIA_REATTIVA">
									<fo:inline><xsl:value-of select="./child::FASCIA/child::ENERGIA_REATTIVA"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F1']/child::POTENZA">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F1']/child::POTENZA"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F2']/child::POTENZA">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F2']/child::POTENZA"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::POTENZA">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F3']/child::POTENZA"/></fo:inline>
								</xsl:when>
								<xsl:when test="./child::FASCIA/child::POTENZA">
									<fo:inline><xsl:value-of select="./child::FASCIA/child::POTENZA"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell xsl:use-attribute-sets="brd.l.ultimi_consumi">
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F1']/child::MEDIA_GG">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F1']/child::MEDIA_GG"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F2']/child::MEDIA_GG">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F2']/child::MEDIA_GG"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::MEDIA_GG">
									<fo:inline><xsl:value-of select="./child::FASCIA[@FASCIA='F3']/child::MEDIA_GG"/></fo:inline>
								</xsl:when>
								<xsl:when test="./child::FASCIA/child::MEDIA_GG">
									<fo:inline><xsl:value-of select="./child::FASCIA/child::MEDIA_GG"/></fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>-</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
			</fo:table-row>
		</xsl:for-each>
		
		<xsl:variable name='mono'>
			<xsl:for-each select="./child::CONSUMO">
				<xsl:choose>
					<xsl:when test="./child::FASCIA[@FASCIA='F3']/child::ENERGIA_REATTIVA"></xsl:when>
					<xsl:when test="./child::FASCIA/child::ENERGIA_REATTIVA">SI</xsl:when>
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:variable>
		
		<!-- <xsl:if test="$bordi='NO' or @STIME_PRESENTI='SI' or substring($mono,1,2)='SI'"> -->
			<fo:table-row keep-with-previous="always" height="2mm">
				<fo:table-cell padding-left="3mm" number-columns-spanned="13" xsl:use-attribute-sets="brd.b.000">
					<fo:block></fo:block>
				</fo:table-cell>
			</fo:table-row>
		<!-- </xsl:if> -->
		
		<xsl:if test="$mese_fattura &lt; '201107'">
			<fo:table-row keep-with-previous="always" text-align="start" xsl:use-attribute-sets="font_titolo_tabella_bold">
				<fo:table-cell>
					<fo:block>
						CONSUMO ANNUO
					</fo:block>
				</fo:table-cell>
					
				<fo:table-cell number-columns-spanned="2">
					<fo:block>
						<xsl:value-of select="@SOMMA_F1"/> KWh (F1)
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2">
					<fo:block>
						<xsl:value-of select="@SOMMA_F2"/> KWh (F2)
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="2">
					<fo:block>
						<xsl:value-of select="@SOMMA_F3"/> KWh (F3)
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="6">
					<fo:block>
						<xsl:value-of select="@SOMMA_MONO"/> KWh (MONORARIO)
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
		
		<xsl:if test="$mese_fattura &gt; '201106'">
			<fo:table-row keep-with-previous="always" text-align="center" xsl:use-attribute-sets="font_titolo_tabella_bold">
				<fo:table-cell text-align="start">
					<fo:block>
						CONSUMO ANNUO
					</fo:block>
				</fo:table-cell>
					
				<fo:table-cell>
					<fo:block>
						<xsl:value-of select="@SOMMA_F1"/>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block>
						<xsl:value-of select="@SOMMA_F2"/>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block>
						<xsl:value-of select="@SOMMA_F3"/>
					</fo:block>
				</fo:table-cell>
				
				
				<fo:table-cell text-align="start">
					<fo:block>
						<xsl:if test="not(@SOMMA_MONO=0)">
							(<xsl:value-of select="@SOMMA_MONO"/>*<xsl:if test="@STIME_PRESENTI='SI'">*</xsl:if>)
						</xsl:if>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="8" text-align="start">
					<fo:block>
						TOTALE COMPLESSIVO: <xsl:value-of select="@SOMMA_CONSUMI"/>
					</fo:block>
				</fo:table-cell>
				
				
			</fo:table-row>
		</xsl:if>
		
		<xsl:if test="@STIME_PRESENTI='SI'">
			<fo:table-row keep-with-previous="always">
				<fo:table-cell number-columns-spanned="13">	
					<fo:block text-align="start">* I consumi relativi a questo mese sono stati stimati</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
		
		<xsl:if test="substring($mono,1,2)='SI'">
			<fo:table-row keep-with-previous="always">
				<fo:table-cell number-columns-spanned="13">	
					<fo:block text-align="start">*<xsl:if test="@STIME_PRESENTI='SI'">*</xsl:if> Consumo monorario</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
		
		<xsl:if test="./ancestor::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA=$ert] or ./ancestor::CONTRATTO_GAS[@CONSUMER='NO']">
			<fo:table-row keep-with-previous="always">
				<fo:table-cell padding-top="2mm" number-columns-spanned="13">	
					<fo:block text-align="start" widows="3" orphans="3">L' energia reattiva e' la quota parte dell' energia elettrica
					consumata che, pur non generando lavoro utile, impegna maggiormente le linee elettriche.
					Il prelievo di energia reattiva e' dovuto alle caratteristiche di alcuni tipi di impianti
					utilizzatori e la sua fatturazione ha un carattere di penalita' per il cliente finale, allo
					scopo di indurlo al rifasamento dei propri impianti.</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
	</fo:table-body>
	</fo:table>
</xsl:if>
</xsl:template>

</xsl:stylesheet>