<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:fox="http://xml.apache.org/fop/extensions">
<xsl:output encoding="UTF-8"/>

<!-- <xsl:variable name="pc_disp">EA</xsl:variable>
<xsl:variable name="pt">PT</xsl:variable>
<xsl:variable name="eb_cte">EB</xsl:variable>
<xsl:variable name="a_uc">CT_FISSE</xsl:variable>
<xsl:variable name="tp">TP</xsl:variable>
<xsl:variable name="te">TE</xsl:variable>
<xsl:variable name="ert">ERT</xsl:variable>
<xsl:variable name="imposta_erariale">IE</xsl:variable>
<xsl:variable name="oneri_diversi">ONERI_DIVERSI</xsl:variable>
<xsl:variable name="addizionale_provinciale">AP</xsl:variable>
<xsl:variable name="addizionale_comunale">AC</xsl:variable>
<xsl:variable name="quota_fissa_ea">QUOTA_FISSA_EA</xsl:variable>
<xsl:variable name="quota_fissa_eb">QUOTA_FISSA_EB</xsl:variable> -->

<xsl:variable name="quota_fissa_vendita">QUOTA_FISSA_VENDITA</xsl:variable>
<xsl:variable name="quota_potenza_vendita">TP_VENDITA</xsl:variable>
<xsl:variable name="quota_variabile_vendita">QUOTA_VARIABILE_VENDITA</xsl:variable>
<xsl:variable name="quota_fissa_reti">QUOTA_FISSA_RETI</xsl:variable>
<xsl:variable name="quota_variabile_reti">QUOTA_VARIABILE_RETI</xsl:variable>
<xsl:variable name="imposte">IMPOSTE</xsl:variable>

<xsl:variable name="tipo1_202_09">SERVIZI DI RETE</xsl:variable>
<xsl:variable name="tipo2_202_09">SERVIZI DI VENDITA</xsl:variable>
<xsl:variable name="tipo2a_202_09">PREZZO DELL'ENERGIA (PE)</xsl:variable>
<xsl:variable name="tipo2b_202_09">SERVIZI DI VENDITA</xsl:variable>
<xsl:variable name="tipo3_202_09">IMPOSTE</xsl:variable>
<xsl:variable name="tipo4_202_09">COMPONENTI PERDITE</xsl:variable>
<xsl:variable name="tipo1_c_202_09">SERVIZI DI RETE</xsl:variable>
<xsl:variable name="tipo2_c_202_09">SERVIZI DI VENDITA</xsl:variable>
<xsl:variable name="tipo2a_exnoi_202_09">PREZZO DELL'ENERGIA (PE)</xsl:variable>
<xsl:variable name="tipo2a_c_202_09">PREZZO DELL'ENERGIA (PE)</xsl:variable>
<xsl:variable name="tipo2b_c_202_09">ALTRE COMPONENTI AEEG</xsl:variable>

<xsl:include href="dettaglio_energia_bordi_012011.xsl"/>
<xsl:include href="Sezione_generica_dettaglio_energia_012011.xsl"/>
<xsl:include href="consumi_energia_ultimi_14_mesi_012011.xsl"/>


<!-- <xsl:include href="sintesi_monosito.xsl"/>
<xsl:include href="sintesi_multisito.xsl"/> -->
<xsl:include href="sintesi_energia_012011.xsl"/>
<!-- <xsl:include href="oneri_diversi_bordi.xsl"/> -->

<xsl:include href="template_energia_parametrizzato_012011.xsl"/>
<xsl:include href="sezioni_sintesi.xsl"/>

<!--
********************************************************
**                                                    **
**         Template per i contratti Energia           **
**                                                    **
********************************************************
-->
<xsl:template name="CONTRATTI_MULTISITO_012011">
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="sfondo_titoli"/>
	<xsl:param name="color-sezioni"/>
	<xsl:param name="color-sottosezioni"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:param name="svg-sottosezioni"/>
	<xsl:param name="svg-dettaglio"/>
	<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:param>
	
	<!--
	********************************************************
	**                                                    **
	**             Enegia Elettrica Sintesi               **
	**                                                    **
	********************************************************
	-->
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
	<fo:table-column column-width="proportional-column-width(100)"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<!-- <fo:table-row keep-with-next="always">
			<fo:table-cell xsl:use-attribute-sets="pad.l.000">
				<fo:block xsl:use-attribute-sets="blk.003 chrtitolodettaglio">
					ENERGIA ELETTRICA - SINTESI
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row keep-with-next="always" height="0.5mm">
			<fo:table-cell border-bottom="0.5 dashed thick black">
				<fo:block/>
			</fo:table-cell>
		</fo:table-row>
		
		<fo:table-row keep-with-next="always" height="0.5mm">
			<fo:table-cell>
				<fo:block/>
			</fo:table-cell>
		</fo:table-row> -->
		
		<fo:table-row keep-with-next="always">
			<fo:table-cell>
				<xsl:call-template name="SINTESI_ENERGIA_012011">
					<xsl:with-param name="bordi" select="$bordi"/>
					<xsl:with-param name="color" select="$color"/>
					<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
					<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
					<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
					<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
					<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
					<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
					<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
				</xsl:call-template>
			</fo:table-cell>
		</fo:table-row>
	</fo:table-body>
	</fo:table>
	
	<xsl:for-each select="./child::CONTRATTO_GAS">
		<!--
		********************************************************
		**                                                    **
		**                Dati di riepilogo                   **
		**                                                    **
		********************************************************
		-->
		<xsl:if test="position()=1">
			<fo:block break-before="page">
			</fo:block>
		</xsl:if>
		
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			
			<fo:table-body>
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<xsl:attribute name="color"><xsl:value-of select="$color_titolo_dettaglio"/></xsl:attribute>
							<fo:table-column column-width="proportional-column-width(50)"/>
							<fo:table-column column-width="proportional-column-width(50)"/>
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row keep-with-next="always">
									<fo:table-cell number-columns-spanned="2">
										<fo:block xsl:use-attribute-sets="blk.003 chrtitolodettaglio">
											GAS NATURALE - DETTAGLIO PUNTO DI RICONSEGNA - 
											CONTRATTO N.
											<xsl:value-of select="./child::CONTRACT_NO"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<xsl:choose>
									<xsl:when test="not(./child::PERIODO_RIFERIMENTO='')">
										<fo:table-row keep-with-next="always" height="0.5mm">
											<fo:table-cell number-columns-spanned="2" border-bottom="0.5 dashed thick black">
												<xsl:attribute name="border-bottom-color"><xsl:value-of select="$color_titolo_dettaglio"/></xsl:attribute>
												<fo:block/>
											</fo:table-cell>
										</fo:table-row>
										
										<fo:table-row keep-with-next="always" height="0.5mm">
											<fo:table-cell number-columns-spanned="2">
												<fo:block/>
											</fo:table-cell>
										</fo:table-row>
									
										<fo:table-row keep-with-next="always" height="2mm">
											<fo:table-cell>
												<fo:block xsl:use-attribute-sets="blk.010">
													<fo:inline xsl:use-attribute-sets="chrbold.008">PERIODO DI RIFERIMENTO </fo:inline>
													<fo:inline xsl:use-attribute-sets="chr.009"><xsl:value-of select="./child::PERIODO_RIFERIMENTO"/></fo:inline>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</xsl:when>
								</xsl:choose>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:if test="not($bordi='NO')">
								<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="not($color='black')">
								<xsl:attribute name="background-image">url(svg/rectangle_table_color_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="$bordi='NO' and $color='black'">
								<xsl:attribute name="background-image">url(svg/rectangle_table_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<xsl:if test="./ancestor::DOCUMENTO/child::RIEPILOGO_MULTISITO_ENERGIA">
			<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" font-family="universcbold" font-size="8.5pt" xsl:use-attribute-sets="blk.004">
			<fo:table-column column-width="proportional-column-width(100)"/>
				<fo:table-body end-indent="0pt" start-indent="0pt">
					<fo:table-row keep-with-next="always" height="19mm">
						<fo:table-cell display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image"><xsl:value-of select="$svg-dettaglio"/></xsl:attribute>
							<!-- <xsl:attribute name="color"><xsl:value-of select="$color"/></xsl:attribute> -->
							
							<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(43)"/>
							<fo:table-column column-width="proportional-column-width(25)"/>
							<fo:table-column column-width="proportional-column-width(15)"/>
							<fo:table-column column-width="proportional-column-width(17)"/>
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											PRODOTTO/SERVIZIO
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::PRODOTTO_SERVIZIO"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											INIZIO FORNITURA
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::INIZIO_FORNITURA"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="pad.r.000">
										<fo:block>
											TIPOLOGIA CONTRATTO
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::TIPOLOGIA_CONTRATTO"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>				
								
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											SEDE FORNITURA
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::SEDE_OPERATIVA"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											TIPOLOGIA MERCATO
											<fo:inline font-family="universc">
												Mercato libero
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:if test="./child::POD">
												POD
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::POD"/>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell xsl:use-attribute-sets="pad.r.000">
										<fo:block>
											<xsl:if test="./child::PRESA and $mese_fattura &lt; '201107'">
												PRESA
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::PRESA"/>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>	
								
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											TIPO CONTATORE
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::TIPO_CONTATORE"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											TENSIONE DI ALIM.
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::TENSIONE_ALIMENTAZIONE"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:if test="./child::POTENZA_DISPONIBILE">
												POT. DISP.
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::POTENZA_DISPONIBILE"/>
													kW
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell xsl:use-attribute-sets="pad.r.000">
										<fo:block>
											<xsl:if test="./child::POTENZA_IMPEGNATA">
												POT. IMP.
												<fo:inline font-family="universc">
													<xsl:value-of select="./child::POTENZA_IMPEGNATA"/>
													kW
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>	
							</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>						
				</fo:table-body>
			</fo:table>
			
			
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="LETTURE"/>
				<xsl:with-param name="sezione4check" select="LETTURE"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>

			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="LETTURE_CONSUMI_CONGUAGLI"/>
				<xsl:with-param name="sezione4check" select="LETTURE_CONSUMI_CONGUAGLI/CONSUMO"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
		
			
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="RIEPILOGO_ACCONTI_CONGUAGLIO"/>
				<xsl:with-param name="sezione4check" select="RIEPILOGO_ACCONTI_CONGUAGLIO/CONSUMO"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="CONSUMI_ENERGIA_ULTIMI_14_MESI"/>
				<xsl:with-param name="sezione4check" select="CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			
			<!-- <xsl:apply-templates select="./child::LETTURE">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="./child::LETTURE_CONSUMI_CONGUAGLI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="./child::RIEPILOGO_ACCONTI_CONGUAGLIO">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			</xsl:apply-templates> -->
			
			<!-- <xsl:apply-templates select="./child::CONSUMI_ENERGIA_ULTIMI_14_MESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:apply-templates> -->
		</xsl:if>
		
		<xsl:variable name="ultima_sezione_reti">
			<xsl:choose>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$ert]">
					<xsl:value-of select="$ert"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$quota_variabile_reti]">
					<xsl:value-of select="$quota_variabile_reti"/>
				</xsl:when>
				
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_reti]">
					<xsl:value-of select="$quota_fissa_reti"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$te]">
					<xsl:value-of select="$te"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$tp]">
					<xsl:value-of select="$tp"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$a_uc]">
					<xsl:value-of select="$a_uc"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="ultima_sezione_vendita">
			<xsl:choose>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$pt]">
					<xsl:value-of select="$pt"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$pc_disp]">
					<xsl:value-of select="$pc_disp"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$quota_variabile_vendita]">
					<xsl:value-of select="$quota_variabile_vendita"/>
				</xsl:when>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$quota_potenza_vendita]">
					<xsl:value-of select="$quota_potenza_vendita"/>
				</xsl:when>
				
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$quota_fissa_vendita]">
					<xsl:value-of select="$quota_fissa_vendita"/>
				</xsl:when>
				
				<!-- nuovo -->
				<!--
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$materia_prima_gas]">
					<xsl:value-of select="$materia_prima_gas"/>
				</xsl:when>
				-->
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="ultima_sezione_imposte">
			<xsl:choose>
				<xsl:when test="./child::SEZIONE[@TIPOLOGIA=$imposte]">
					<xsl:value-of select="$imposte"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:call-template name="Dettaglio_energia_012011">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
			<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
			<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
			<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
			<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
			<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
			<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
			<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
		</xsl:call-template>
		
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
		
			
			<xsl:if test="not($bordi='NO')">
				<fo:table-header>
					<fo:table-row height="3mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_bold.svg)</xsl:attribute>
							<fo:block/>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
			</xsl:if>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">		
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.002 chrbold.008">
						<fo:table-column column-width="proportional-column-width(85)"/>
						<fo:table-column column-width="proportional-column-width(15)"/>
						<fo:table-body>
							<fo:table-row keep-with-previous="always">
								<fo:table-cell>
									<fo:block>
										TOTALE netto Iva
										<xsl:if test="$lettere_sezioni='SI'">
											<fo:inline xsl:use-attribute-sets="condensedcors.008">
												<xsl:choose>
													<xsl:when test="$ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte=''">
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))
																	or
																	($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte='')">
														(A)
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	not($ultima_sezione_imposte=''))
																	or
																	(not($ultima_sezione_reti='') and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))">
														(A+B)
													</xsl:when>
													<xsl:otherwise>
														(A+B+C)
													</xsl:otherwise>
												</xsl:choose>
											</fo:inline>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									
									<fo:block>
										<xsl:value-of select="./child::SINTESI_GAS/child::TOTALE_IMPONIBILE"/>
									</fo:block>
								</fo:table-cell>
								
							</fo:table-row>
							
							
							
							<fo:table-row keep-with-previous="always">
								<fo:table-cell>
									<fo:block>
										IVA su imponibile di euro
										<xsl:if test="$lettere_sezioni='SI'">
											<fo:inline xsl:use-attribute-sets="condensedcors.008">
												<xsl:choose>
													<xsl:when test="$ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte=''">
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))
																	or
																	($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte='')">
														(B)
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	not($ultima_sezione_imposte=''))
																	or
																	(not($ultima_sezione_reti='') and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))">
														(C)
													</xsl:when>
													<xsl:otherwise>
														(D)
													</xsl:otherwise>
												</xsl:choose>
											</fo:inline>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="./child::SINTESI_GAS/child::IVA"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							
							<fo:table-row keep-with-previous="always">
								<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="brd.b.000">
									<fo:block/>
								</fo:table-cell>
							</fo:table-row>
				  
							<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004">
								<fo:table-cell>
									<fo:block>
										TOTALE FORNITURA DI GAS NATURALE E IMPOSTE
										<xsl:if test="$lettere_sezioni='SI'">
											<fo:inline xsl:use-attribute-sets="condensedcors.008">
												<xsl:choose>
													<xsl:when test="$ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte=''">
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))
																	or
																	($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	$ultima_sezione_imposte='')">
														(A+B)
													</xsl:when>
													<xsl:when test="($ultima_sezione_reti='' and
																	not($ultima_sezione_vendita='') and
																	not($ultima_sezione_imposte=''))
																	or
																	(not($ultima_sezione_reti='') and
																	not($ultima_sezione_vendita='') and
																	$ultima_sezione_imposte='')
																	or
																	(not($ultima_sezione_reti='') and
																	$ultima_sezione_vendita='' and
																	not($ultima_sezione_imposte=''))">
														(A+B+C)
													</xsl:when>
													<xsl:otherwise>
														(A+B+C+D)
													</xsl:otherwise>
												</xsl:choose>
											</fo:inline>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="./child::SINTESI_GAS/child::TOTALE_FORNITURA_DI_GAS_NATURALE_E_IMPOSTE"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>

						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			
				<xsl:if test="not($bordi='NO')">
					<fo:table-row keep-with-previous="always" height="2mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_bold.svg)</xsl:attribute>
							<fo:block xsl:use-attribute-sets="blk.002">
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-body>
		</fo:table>
		
		<xsl:apply-templates select="SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
			<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
			<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
			<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
			<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
		</xsl:apply-templates>
		
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		
			<xsl:if test="not($bordi='NO')">
				<fo:table-header>
					<fo:table-row height="3mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_bold.svg)</xsl:attribute>
							<fo:block></fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
			</xsl:if>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">				
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(85)"/>
						<fo:table-column column-width="proportional-column-width(15)"/>
							<fo:table-header>
								<!-- <xsl:if test="$bordi='NO'">
									<fo:table-row keep-with-previous="always" height="1mm">
										<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="pad.t.003 pad.l.000 brd.b.spesso">
											<fo:block/>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if> -->
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrTot">	
									<xsl:if test="$bordi='NO'">
										<xsl:attribute name="border-bottom">1.5pt solid black</xsl:attribute>
									</xsl:if>
									
									<fo:table-cell>
										<fo:block>
											TOTALE PUNTO DI RICONSEGNA
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_reti='' and
																		$ultima_sezione_vendita='' and
																		$ultima_sezione_imposte=''">
														</xsl:when>
														<xsl:when test="($ultima_sezione_reti='' and
																		$ultima_sezione_vendita='' and
																		not($ultima_sezione_imposte=''))
																		or
																		($ultima_sezione_reti='' and
																		not($ultima_sezione_vendita='') and
																		$ultima_sezione_imposte='')
																		or
																		(not($ultima_sezione_reti='') and
																		$ultima_sezione_vendita='' and
																		$ultima_sezione_imposte='')">
															(A+B<xsl:if test="./child::SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']/child::PERIODO_RIFERIMENTO">+C</xsl:if>)
														</xsl:when>
														<xsl:when test="($ultima_sezione_reti='' and
																		not($ultima_sezione_vendita='') and
																		not($ultima_sezione_imposte=''))
																		or
																		(not($ultima_sezione_reti='') and
																		not($ultima_sezione_vendita='') and
																		$ultima_sezione_imposte='')
																		or
																		(not($ultima_sezione_reti='') and
																		$ultima_sezione_vendita='' and
																		not($ultima_sezione_imposte=''))">
															(A+B+C<xsl:if test="./child::SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']/child::PERIODO_RIFERIMENTO">+D</xsl:if>)
														</xsl:when>
														<xsl:otherwise>
															(A+B+C+D<xsl:if test="./child::SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']/child::PERIODO_RIFERIMENTO">+E</xsl:if>)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./child::SINTESI_GAS/child::TOTALE_DA_PAGARE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<xsl:variable name="nota_recesso">
									<xsl:if test="./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='CORRISPETTIVO_RECESSO']/child::ID='CORRISPETTIVO_RECESSO_V1'">SI</xsl:if>
								</xsl:variable>
								
								<xsl:variable name="nota_recesso_storno">
									<xsl:if test="./ancestor::DOCUMENTO/child::PROMO_SERVIZI[@TIPOLOGIA='CORRISPETTIVO_RECESSO']/child::ID='STORNO_CORRISPETTIVO_RECESSO_V1'">SI</xsl:if>
								</xsl:variable>
								
								<xsl:for-each select="./child::SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']/child::PERIODO_RIFERIMENTO">
									<xsl:for-each select="./child::PRODOTTO">
										<xsl:if test="@ORDINE">
											<fo:table-row xsl:use-attribute-sets="chr.007" keep-with-previous="always">
												<fo:table-cell number-columns-spanned="2">
													<fo:list-block>
														<fo:list-item>
															<fo:list-item-label end-indent="0pt">
																<fo:block>(<xsl:value-of select="@ORDINE"/>)</fo:block>
															</fo:list-item-label>
															<xsl:if test="@NOTA='3053'">
																<fo:list-item-body start-indent="7pt">
																	<fo:block>Corrispettivo per
																	il <fo:inline font-family="universcbold">mancato rispetto dei termini
																	e delle condizioni per l'esercizio del diritto di recesso.</fo:inline></fo:block>
																	<fo:block keep-with-previous="always">A titolo di
																	esempio, <fo:inline font-family="universcbold">la richiesta di recesso non
																	e' stata trasmessa</fo:inline> ad Energit oppure <fo:inline font-family="universcbold">la
																	comunicazione e' pervenuta oltre i termini previsti</fo:inline>.</fo:block>
																</fo:list-item-body>
															</xsl:if>
															
															<xsl:if test="@NOTA='3053_STORNO'">
																<fo:list-item-body start-indent="7pt">
																	<fo:block>Storno corrispettivo per
																	il <fo:inline font-family="universcbold">mancato rispetto dei termini
																	e delle condizioni per l'esercizio del diritto di recesso.</fo:inline></fo:block>
																</fo:list-item-body>
															</xsl:if>
															
															<xsl:if test="@NOTA='25100'">
																<fo:list-item-body start-indent="7pt">
																	<fo:block><fo:inline font-family="universcbold">Questo valore si riferisce
																	ad una o più fatture precedenti</fo:inline>: poiché l’importo complessivo da
																	fatturare risultava inferiore ai 30 €, Energit ha posticipato la richiesta di
																	pagamento permettendole di <fo:inline font-family="universcbold">risparmiare
																	i costi di commissione per il pagamento</fo:inline>.</fo:block>
																	<fo:block>Per verificare la corrispondenza degli importi la invitiamo a
																	confrontare il valore con la voce “Consumi e oneri con fatturazione differita”
																	riportata nelle precedenti fatture emesse con importo pari a zero.</fo:block>
																</fo:list-item-body>
															</xsl:if>
															
															<xsl:if test="@NOTA='25101'">
																<fo:list-item-body start-indent="7pt">
																	<fo:block><fo:inline font-family="universcbold">Questo importo sarà
																	addebitato successivamente.</fo:inline></fo:block>
																	<fo:block>Energit posticipa infatti la richiesta di pagamento in caso
																	di importi complessivi da fatturare inferiori ai 30 €, permettendo
																	al Cliente di <fo:inline font-family="universcbold">risparmiare i costi
																	di commissione per il pagamento</fo:inline>.</fo:block>
																	<fo:block>La fattura viene comunque emessa, con importo pari a zero,
																	per fornire al Cliente evidenza dell’importo riferito al periodo e per
																	rispettare la periodicità di fatturazione stabilita per l’utenza.</fo:block>
																</fo:list-item-body>
															</xsl:if>
															
															<xsl:if test="@NOTA='3054'">
																<fo:list-item-body start-indent="7pt">
																	<fo:block>Indennizzo automatico per mancato rispetto dei livelli specifici
																	di qualità definiti dall'Autorità per l'energia elettrica e il gas.</fo:block>
																	<fo:block>La corresponsione dell'indennizzo automatico non esclude la
																	possibilità per il richiedente di richiedere nelle opportune sedi il
																	risarcimento dell'eventuale danno ulteriore subito</fo:block>
																</fo:list-item-body>
															</xsl:if>
														</fo:list-item>
													</fo:list-block>
												</fo:table-cell>
											</fo:table-row>
										</xsl:if>
									</xsl:for-each>
								</xsl:for-each>
								
								
								
								<xsl:if test="$nota_recesso='SI' and not($nota_recesso_storno='SI')">
									<fo:table-row xsl:use-attribute-sets="chr.007" keep-with-previous="always">
										<fo:table-cell number-columns-spanned="2">
											<fo:list-block>
												<fo:list-item>
													<fo:list-item-label end-indent="0pt">
														<fo:block>*</fo:block>
													</fo:list-item-label>
													<fo:list-item-body start-indent="5pt">
														<fo:block>Corrispettivo per
														il <fo:inline font-family="universcbold">mancato rispetto dei termini
														e delle condizioni per l'esercizio del diritto di recesso.</fo:inline></fo:block>
														<fo:block keep-with-previous="always">A titolo di
														esempio, <fo:inline font-family="universcbold">la richiesta di recesso non
														e' stata trasmessa</fo:inline> ad Energit oppure <fo:inline font-family="universcbold">la
														comunicazione e' pervenuta oltre i termini previsti</fo:inline>.</fo:block>
													</fo:list-item-body>
												</fo:list-item>
											</fo:list-block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<xsl:if test="$nota_recesso_storno='SI'">
									<fo:table-row xsl:use-attribute-sets="chr.007" keep-with-previous="always">
										<fo:table-cell number-columns-spanned="2">
											<fo:list-block>
												<fo:list-item>
													<fo:list-item-label end-indent="0pt">
														<fo:block>*</fo:block>
													</fo:list-item-label>
													<fo:list-item-body start-indent="5pt">
														<fo:block>Storno corrispettivo per
														il <fo:inline font-family="universcbold">mancato rispetto dei termini
														e delle condizioni per l'esercizio del diritto di recesso.</fo:inline></fo:block>
													</fo:list-item-body>
												</fo:list-item>
											</fo:list-block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
							</fo:table-header>
										
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			
				<xsl:if test="not($bordi='NO')">
					<fo:table-row keep-with-previous="always" height="2mm">
						<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_bold.svg)</xsl:attribute>
							<fo:block xsl:use-attribute-sets="blk.002">
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-body>
		</fo:table>
  
		<xsl:if test="position()&lt;last()">
			<fo:block break-before="page">
			</fo:block>
		</xsl:if>
	</xsl:for-each>
</xsl:template>
	
</xsl:stylesheet>
