<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
**********************
**                  **
** SINTESI MONOSITO **
**                  **
**********************
-->
<xsl:template name="SINTESI_ENERGIA_012011">
	
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="sfondo_titoli"/>
	<xsl:param name="color-sezioni"/>
	<xsl:param name="color-sottosezioni"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:param name="svg-sottosezioni"/>
	<xsl:param name="svg-dettaglio"/>
	<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:param>
	
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		
			<fo:table-body>
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row keep-with-previous="always">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="color"><xsl:value-of select="$color_titolo_dettaglio"/></xsl:attribute>
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(30)"/>
							<fo:table-column column-width="proportional-column-width(70)"/>
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row keep-with-next="always">
									<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="pad.l.000">
										<fo:block xsl:use-attribute-sets="blk.003 chrtitolodettaglio">
											GAS NATURALE - SINTESI
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
									<fo:table-row keep-with-next="always" height="0.5mm">
										<fo:table-cell number-columns-spanned="2" border-bottom="0.5 dashed thick">
											<xsl:attribute name="border-bottom-color"><xsl:value-of select="$color_titolo_dettaglio"/></xsl:attribute>
											<fo:block/>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row keep-with-next="always" height="0.5mm">
										<fo:table-cell number-columns-spanned="2">
											<fo:block/>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row xsl:use-attribute-sets="blk.003">
										<fo:table-cell xsl:use-attribute-sets="pad.l.000">
											<fo:block xsl:use-attribute-sets="chrtitolodettaglio">
												CONTRATTO N.
												<xsl:value-of select="./child::CONTRATTO_GAS/child::CONTRACT_NO"/>
											</fo:block>
										</fo:table-cell>
										
										<fo:table-cell xsl:use-attribute-sets="pad.l.000">
											<fo:block>
												<xsl:if test="not(./child::CONTRATTO_GAS/child::PERIODO_RIFERIMENTO='')">
													<fo:inline xsl:use-attribute-sets="chrbold.008">
														PERIODO DI RIFERIMENTO
													</fo:inline>
													<fo:inline xsl:use-attribute-sets="chr.009">
														<xsl:value-of select="./child::CONTRATTO_GAS/child::PERIODO_RIFERIMENTO"/>
													</fo:inline>
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:if test="not($bordi='NO')">
								<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="not($color='black')">
								<xsl:attribute name="background-image">url(svg/rectangle_table_color_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="$bordi='NO' and $color='black'">
								<xsl:attribute name="background-image">url(svg/rectangle_table_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</fo:table-body>
		</fo:table>
		
		
		<!-- ************* UNDER CONSTRUCTION ********** -->
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
				<fo:table-column column-width="proportional-column-width(100)"/>
				<fo:table-body end-indent="0pt" start-indent="-1pt">
				
					<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:block/>
					</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row height="19mm">
						<fo:table-cell display-align="center" background-repeat="no-repeat"> 
							<xsl:attribute name="color"><xsl:value-of select="$color_titolo_dettaglio"/></xsl:attribute>
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:if test="not($bordi='NO')">
								<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="not($color='black')">
								<xsl:attribute name="background-image">url(svg/rectangle_table_color_body_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="$bordi='NO' and $color='black'">
								<xsl:attribute name="background-image">url(svg/rectangle_table_body_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" font-family="universcbold" font-size="8.5pt" xsl:use-attribute-sets="blk.004">
							<fo:table-column column-width="proportional-column-width(20)"/>
							<fo:table-column column-width="proportional-column-width(35)"/>
							<fo:table-column column-width="proportional-column-width(25)"/>
							<fo:table-column column-width="proportional-column-width(20)"/>
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											SEDE DI FORNITURA:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">	
									<fo:block text-align="left">
										<fo:inline font-family="universc">
											<xsl:value-of select="./child::CONTRATTO_GAS/child::SEDE_OPERATIVA"/>
										</fo:inline>
									</fo:block>	
									</fo:table-cell>
							
									<fo:table-cell>
										<fo:block>
											CODICE RE.MI.:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::REMI"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											DISTRIBUTORE LOCALE:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::DISTRIBUTORE"/> 
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											CODICE PDR:
											</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::PDR"/> 
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											TIPOLOGIA MERCATO:
											</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												Mercato libero
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											MATRICOLA CONTATORE:
									</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::MATRICOLA_CONTATORE"/> 
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											PRODOTTO/SERVIZIO:
											</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::PRODOTTO_SERVIZIO"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											TIPOLOGIA DI MISURATORE:
											</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left" font-size="7pt">
											<xsl:if test="(./child::CONTRATTO_GAS/child::TIPO_CONTATORE='T')">
											<fo:inline font-family="universc" font-size="9pt">
												<!--<xsl:value-of select="./child::CONTRATTO_GAS/child::TIPO_CONTATORE"/> -->
												Meccanico
											</fo:inline>
											</xsl:if>
											<xsl:if test="(./child::CONTRATTO_GAS/child::TIPO_CONTATORE='E')">
											<fo:inline font-family="universc" font-size="9pt">
												<!--<xsl:value-of select="./child::CONTRATTO_GAS/child::TIPO_CONTATORE"/> -->
												Elettronico
											</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											DATA INIZIO FORNITURA:
											</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::INIZIO_FORNITURA"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											POTERE CALORIFICO SUPERIORE:
											</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::PCS"/> GJ/Smc
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											CONSUMO ANNUO MEDIO:
											</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::CONSUMO_ANNUO_MEDIO"/> Smc
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											COEFFICIENTE C:
											</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::COEFFICIENTE_C"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											TIPOLOGIA FISCALE:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::TIPOLOGIA_FISCALE"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											TIPOLOGIA CONTRATTO:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::TIPOLOGIA_UTENZA"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block>
											TIPOLOGIA USO:
											</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block text-align="left">
											<fo:inline font-family="universc">
												<xsl:value-of select="./child::CONTRATTO_GAS/child::CATEGORIA_USO"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:if test="not($bordi='NO')">
								<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="not($color='black')">
								<xsl:attribute name="background-image">url(svg/rectangle_table_color_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="$bordi='NO' and $color='black'">
								<xsl:attribute name="background-image">url(svg/rectangle_table_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
				</fo:table-body>
			</fo:table>		
		</xsl:if>
		
		<!-- *********** END CONSTRUCTION ************** -->
		
		<!--
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="block_sintesi_start font_sintesi">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			
					
			<fo:table-row height="2mm" keep-with-previous="always">
				<fo:table-cell display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_bold.svg)</xsl:attribute>
					</xsl:if>
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
				
			<fo:table-row>
				<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute>
					</xsl:if>
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(45)"/>
						<fo:table-column column-width="proportional-column-width(55)"/>
						<fo:table-body end-indent="0pt" start-indent="0pt">
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE SERVIZI DI VENDITA</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_VENDITA"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE SERVIZI DI RETE</fo:inline>
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE='0,00')">
											<fo:inline> AL NETTO DEL BONUS SOCIALE</fo:inline>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_RETE_SENZA_BONUS"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE='0,00')">
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<fo:inline>BONUS SOCIALE</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
												<xsl:choose>
													<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<fo:inline>
															<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											
											<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
												<xsl:choose>
													<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<fo:inline>
															<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE IMPOSTE</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE IMPONIBILE</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPONIBILE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPONIBILE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPONIBILE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPONIBILE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>IVA</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE FORNITURA DI GAS NATURALE E IMPOSTE</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_FORNITURA_DI_GAS_NATURALE_E_IMPOSTE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_FORNITURA_DI_GAS_NATURALE_E_IMPOSTE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>ONERI DIVERSI</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::ONERI_DIVERSI='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::ONERI_DIVERSI"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>IVA SU ONERI DIVERSI</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA_SU_ONERI_DIVERSI='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA_SU_ONERI_DIVERSI"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row height="1mm">
								<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="brd.b.000">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<fo:inline>TOTALE DA PAGARE</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block>
										<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
											<xsl:choose>
												<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_DA_PAGARE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_DA_PAGARE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
										
										<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
											<xsl:choose>
												<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_DA_PAGARE='0,00'">
													<fo:inline>-</fo:inline>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:inline>
														<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_DA_PAGARE"/>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
						</fo:table-body>
					</fo:table>
					
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row height="2mm" keep-with-previous="always">
				<fo:table-cell display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_bold.svg)</xsl:attribute>
					</xsl:if>
					<fo:block>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
		</fo:table-body>
		</fo:table>
		-->
		
		<fo:block xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="7pt" space-before="10mm" start-indent="3mm">
			La invitiamo a consultare la sezione Comunicazioni ai Clienti, nell’ultimo foglio della fattura, 
			nella quale potrà trovare utili informazioni, comunicazioni dell’AEEG, offerte e suggerimenti per lei.
		</fo:block>
		
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
		
			<!-- LETTURE, CONSUMO RILEVATO E CONSUMO FATTURATO -->
			
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="CONTRATTO_GAS/LETTURE"/>
				<xsl:with-param name="sezione4check" select="CONTRATTO_GAS/LETTURE"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			
			<fo:block xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="7pt" space-before="3mm" start-indent="3mm">
			Le istruzioni per effettuare l’autolettura del contatore sono riportate nel primo foglio di questa fattura, nella sezione “Lettura del contatore”.
			Ai sensi della <fo:inline font-weight="bold">Delibera ARG/gas 64/09 le autoletture comunicate dal Cliente </fo:inline>vengono trasmesse all’impresa di distribuzione a cura di Energit e <fo:inline font-weight="bold">possono essere 
			utilizzate ai fini della fatturazione soltanto a seguito della convalida delle stesse da parte del Distributore Locale.</fo:inline>

			</fo:block>
			
			
			<!-- CONSUMI E CONGUAGLI -->
			
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="CONTRATTO_GAS/CONSUMI"/>
				<xsl:with-param name="sezione4check" select="CONTRATTO_GAS/CONSUMI/CONSUMO"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			
			
			<fo:block xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="7pt" space-before="3mm" start-indent="3mm">
			* Nota: La fatturazione in acconto o in ricalcolo è relativa a consumi stimati del cliente; la fatturazione a conguaglio riguarda invece consumi effettivi.
			</fo:block>
			<fo:block xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="7pt" space-before="3mm" start-indent="3mm">
			Tramite l’acconto vengono fatturati i consumi presunti del cliente nel mese, sulla base delle migliori stime di misura disponibili e utilizzabili al momento della fatturazione; tali consumi saranno rifatturati a conguaglio successivamente, non appena saranno disponibili i dati di misura integrali forniti dal Distributore Locale.
			</fo:block>
			<fo:block xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="7pt" space-before="3mm" start-indent="3mm">
			Il ricalcolo avviene nei casi in cui sia stato possibile, per uno o più mesi di fornitura fatturati precedentemente in acconto, effettuare stime di consumo più accurate grazie alla disponibilità di nuove misure convalidate dal Distributore Locale. Tali consumi saranno rifatturati a conguaglio successivamente, non appena saranno disponibili i dati di misura integrali forniti dal Distributore Locale.
			Il meccanismo del ricalcolo rende la fatturazione ancora più vicina ai consumi reali del cliente, riducendo l’eventualità che le successive fatture contengano conguagli di importo inatteso: in particolare, tramite il ricalcolo gli occasionali conguagli unici di importo elevato risulteranno distribuiti con più regolarità nel corso dei mesi.
			</fo:block>
			
			<fo:block xsl:use-attribute-sets="blk.002 chrbold.008" text-align="justify" font-size="7pt" space-before="3mm" start-indent="3mm">
			Attraverso il conguaglio vengono fatturati i consumi effettivi del cliente nel mese sulla base delle misure reali convalidate dal Distributore Locale su base annua; tali dati sono soggetti a rifatturazione soltanto in caso di rettifiche tardive sulle misure successivamente introdotte dal Distributore Locale e non dipendenti da Energit. 
			</fo:block>
			

			
			
			<!--
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="CONTRATTO_GAS/CONSUMI"/>
				<xsl:with-param name="sezione4check" select="CONTRATTO_GAS/CONSUMI"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			-->
			
			<!--
			<xsl:call-template name="SEZIONE_SINTESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="sezione" select="CONTRATTO_GAS/CONSUMI_ENERGIA_ULTIMI_14_MESI"/>
				<xsl:with-param name="sezione4check" select="CONTRATTO_GAS/CONSUMI_ENERGIA_ULTIMI_14_MESI/CONSUMO"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:call-template>
			-->
			
			<!-- <xsl:apply-templates select="./child::CONTRATTO_GAS/child::LETTURE">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			</xsl:apply-templates> -->
			
			<!--
			<xsl:apply-templates select="./child::CONTRATTO_GAS/child::LETTURE_CONSUMI_CONGUAGLI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			</xsl:apply-templates>
			
			 <xsl:apply-templates select="./child::CONTRATTO_GAS/child::RIEPILOGO_ACCONTI_CONGUAGLIO">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			</xsl:apply-templates>
			-->
			
			<!-- <xsl:apply-templates select="./child::CONTRATTO_GAS/child::CONSUMI_ENERGIA_ULTIMI_14_MESI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
				<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
				<xsl:with-param name="mese_fattura" select="$mese_fattura"/>
			</xsl:apply-templates> -->
		</xsl:if>
		
</xsl:template>

</xsl:stylesheet>






