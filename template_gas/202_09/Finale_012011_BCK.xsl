<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" />
	
	
	
	<xsl:include href="Comunicazioni_promo_top.xsl"/>
	

	<xsl:template name="FINALE_012011">
	
	<xsl:param name="mese_fattura"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:param>
	
	<xsl:param name="comunicazione_area_clienti">
		<xsl:if test="./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI']">SI</xsl:if>
		<xsl:if test="not(./child::PROMO_SERVIZI[@TIPOLOGIA='AREA_CLIENTI'])">NO</xsl:if>
	</xsl:param>
	
	<xsl:param name="color"/>
	
	<xsl:param name="svg-titoli"/>
	
	<xsl:param name="f3_color">
		<xsl:if test="$color='black'">
			#DDDDDD
		</xsl:if>
		<xsl:if test="not($color='black')">
			#fdd17e
		</xsl:if>
	</xsl:param>
	<xsl:param name="f2_color">
		<xsl:if test="$color='black'">
			#777777
		</xsl:if>
		<xsl:if test="not($color='black')">
			#f9b200
		</xsl:if>
	</xsl:param>
	<xsl:param name="f1_color">
		<xsl:if test="$color='black'">
			#FFFFFF
		</xsl:if>
		<xsl:if test="not($color='black')">
			#FFFFFF
		</xsl:if>
	</xsl:param>
	
	
	<xsl:param name="bordi"/>
	
		<!--
			Tutto il contenuto è racchiuso in un blocco con un valore
			orphans molto alto, in questo modo tutti i block figli
			ereditano questo attributo, che fa in modo che di fatto
			un blocco (paragrafo) non sia mai diviso tra due pagine 
		 -->
		<fo:block widows="3" orphans="3">
			
			<!-- Blocco padre per impostazioni "globali" -->
			<fo:block xsl:use-attribute-sets="font.table">
			
				<!-- Riepilogo fattura -->
				<xsl:call-template name="TEMPLATE_RIEPILOGO_FATTURA"/>
				
				<!-- Elenco fatture in attesa di pagamento -->
				<xsl:if test="FATTURE_ATTESA_PAGAMENTO">
					<fo:table table-layout="fixed" width="100%" space-before="5mm">
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(16)" />
						<fo:table-column column-width="proportional-column-width(20)" />
						
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block>
										ELENCO FATTURE IN ATTESA DI PAGAMENTO
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row text-align="center">
								<fo:table-cell>
									<fo:block>
										NUMERO FATTURA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA EMISSIONE
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										DATA SCADENZA
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DOVUTO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO PAGATO
									</fo:block>
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block>
										IMPORTO DA PAGARE
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
				
						<fo:table-body>
							<xsl:for-each select="FATTURE_ATTESA_PAGAMENTO">
								<fo:table-row text-align="center">
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="NUMERO_FATTURA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_EMISSIONE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="DATA_SCADENZA" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="TOTALE" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="IMPORTO_PAGATO" />
										</fo:block>
									</fo:table-cell>
				
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="INSOLUTO" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
				
							<fo:table-row keep-with-previous="always" height="1mm">
								<fo:table-cell number-columns-spanned="6"
											   xsl:use-attribute-sets="cell.header">
									<fo:block />
								</fo:table-cell>
							</fo:table-row>
				
							<fo:table-row keep-with-previous="always">
								<fo:table-cell number-columns-spanned="5">
									<fo:block />
								</fo:table-cell>
				
								<fo:table-cell>
									<fo:block text-align="center" font-size="8pt" font-family="universcbold">
										<xsl:value-of select="TOTALE_FATTURE_ATTESA_PAGAMENTO" /> euro
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					
				</xsl:if>
			</fo:block>
			
			
			<!-- Frase "perchè pagare entro la data di scadenza..." -->
			<!--
			<fo:block space-before="5mm" xsl:use-attribute-sets="font.pagare">
				<fo:block font-family="universcbold" font-size="8pt">
					<xsl:if test="not($color='black')">
						<xsl:attribute name="color">#0065ae</xsl:attribute>
					</xsl:if>
					Perché pagare entro la data di scadenza
				</fo:block>

				<fo:block keep-with-previous="always">
					La fattura deve essere pagata per intero entro la data di scadenza. 
					In caso di ritardato pagamento (anche parziale) di qualsiasi importo dovuto ad Energit, 
					dal giorno successivo alla scadenza del termine previsto per il pagamento decorreranno a 
					carico del Cliente gli interessi di mora, applicati, disciplinati e determinati in conformità
					a quanto previsto dal Decreto Legislativo n. 231/2002 e calcolati in base ad un tasso di 
					interesse pari a Euribor 6 mesi maggiorato 5%, fatto comunque salvo il diritto di Energit 
					al risarcimento del maggior danno. Limitatamente ai Clienti con consumi non superiori a 
					50.000 mc/anno in una o più delle fatture emesse da Energit, gli interessi di mora saranno 
					calcolati su base annua al tasso ufficiale di riferimento, così come definito ai sensi
					dell’articolo 2 del Decreto Legislativo 24 giugno 1998 n. 213 (TUR) aumentato di 3,5 punti 
					percentuali. Con esclusivo riferimento alla prima fattura in cui si registri un inadempimento
					in relazione ai pagamenti, il Cliente sarà tenuto al pagamento del solo interesse legale per 
					i primi 10 (dieci) giorni di ritardo.
	
				</fo:block>
				
				<fo:block keep-with-previous="always">
					In caso di mancato pagamento (anche parziale) di qualsiasi importo dovuto ad Energit, decorsi 5 (cinque) giorni 
					dalla scadenza del termine indicato nella fattura per il pagamento, Energit invierà al Cliente, ai sensi della
					 delibera AEEG n. ARG/gas 99/11, una comunicazione in cui indicherà il termine, in ogni caso non inferiore a 
					 5 (cinque) giorni, entro cui il Cliente sarà tenuto ad effettuare il pagamento di quanto dovuto ad Energit.
					
				</fo:block>
				
				<fo:block keep-with-previous="always">
					Decorsi inutilmente 3 giorni dal termine ultimo per il pagamento, la Società, in conformità a quanto previsto 
					dalle Condizioni Generali di contratto ed ai sensi dell'articolo 4 della delibera ARG/gas n.99/11 dell'Autorità
					dell'Energia Elettrica e del Gas e s.m.i., richiederà al distributore locale competente la chiusura del punto di 
					riconsegna (pdr) per sospensione della fornitura per morosità.  
				</fo:block>
				
				<fo:block keep-with-previous="always">
					In tale ipotesi, qualora il cliente finale non renda 
					possibile l’esecuzione della chiusura del pdr, la Società si riserva la facoltà di effettuare l’interruzione 
					dell’alimentazione del punto di riconsegna, con oneri a carico del cliente.
				</fo:block>
				
				<fo:block keep-with-previous="always">
					Energit si riserva altresì il 
					diritto di risolvere il contratto in caso di mancato pagamento delle somme dovute.
				</fo:block>
				
				<fo:block keep-with-previous="always">
					
				</fo:block>
			</fo:block>
			-->
			<!--
			<xsl:if test="$mese_fattura &gt; '201303'">
				
				<xsl:if test="CONTRATTO_GAS">
					<fo:block space-before="5mm" xsl:use-attribute-sets="font.fascia">
						<xsl:if test="not($color='black')">
							<xsl:attribute name="color">#0065ae</xsl:attribute>
						</xsl:if>
						FASCE ORARIE DEI CONSUMI DI ENERGIA
						(definite dall'Autorità per l'energia elettrica e il gas - Delibera 181/06)
					</fo:block>
					
					<fo:table table-layout="fixed" width="100%" space-before="0.5mm">
					<fo:table-column column-width="proportional-column-width(49)" />
					<fo:table-column column-width="proportional-column-width(51)" />
					<fo:table-body>
						<fo:table-row keep-with-previous="always" text-align="center">
							<fo:table-cell>
								<fo:table table-layout="fixed" width="100%"
										xsl:use-attribute-sets="font.fascia_header"
										border-collapse="separate" space-before="0.5mm"
										keep-with-previous="always">
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									<fo:table-column column-width="1.5cm" />
									
									<fo:table-body>
										<fo:table-row keep-with-previous="always" text-align="center">
											<fo:table-cell><fo:block>&#160;</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border"><fo:block>0.00-7.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>7.00-8.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>8.00-19.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>19.00-23.00</fo:block></fo:table-cell>
											<fo:table-cell display-align="center" xsl:use-attribute-sets="border.right border.top border.bottom"><fo:block>23.00-24.00</fo:block></fo:table-cell>
										</fo:table-row>
										
										<fo:table-row keep-with-previous="always" text-align="center">
											<fo:table-cell>
												<fo:table table-layout="fixed" width="100%">
													<fo:table-column column-width="proportional-column-width(100)" />

													<fo:table-body>
														<fo:table-row>
															<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.top border.bottom">
																<fo:block text-align="center">lun - ven</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">sab</fo:block>
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row keep-with-previous="always">
															<fo:table-cell display-align="center" xsl:use-attribute-sets="border.left border.bottom">
																<fo:block text-align="center">dom e festivi</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:table-cell>
											
											<fo:table-cell background-color="{$f3_color}" number-columns-spanned="5" xsl:use-attribute-sets="border">
												<fo:table start-indent="1.485cm" background-color="{$f2_color}" table-layout="fixed" width="4.48cm" xsl:use-attribute-sets="border.left border.right border.bottom">
													<fo:table-column column-width="4.48cm" />

													<fo:table-body>
														<fo:table-row>
															<fo:table-cell display-align="center" padding-left="-1.5cm">
																<fo:table start-indent="3cm" background-color="{$f1_color}" table-layout="fixed" width="1.48cm" xsl:use-attribute-sets="border.left border.bottom border.right">
																	<fo:table-column column-width="1.48cm" />

																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell display-align="center" padding-left="-3cm">
																				<fo:block font-family="universcbold">F1</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
																
																<fo:block text-align="center" font-family="universcbold">F2</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
												
												<fo:block><fo:inline font-family="universcbold">F3</fo:inline>
												<xsl:if test="(not(substring(./child::CONTRATTO_GAS/child::PRODOTTO_SERVIZIO,1,15)='Energit per Noi') and
															  not(substring(./child::CONTRATTO_GAS/child::PRODOTTO_SERVIZIO,1,7)='Ecoluce') and
															  not(substring(./child::CONTRATTO_GAS/child::PRODOTTO_SERVIZIO,1,5)='UNICA') and
															  not(substring(./child::CONTRATTO_GAS/child::PRODOTTO_SERVIZIO,1,11)='EnergitCASA'))
															  or
															  substring(./child::CONTRATTO_GAS/child::PRODOTTO_SERVIZIO,1,15)='Ecoluce Azienda'">
												<fo:inline font-family="universcbold">: Il risparmio aumenta</fo:inline> concentrando i
												consumi di energia in questa fascia!
												</xsl:if>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
							
							<fo:table-cell text-align="left">
								<fo:block xsl:use-attribute-sets="font.legenda" keep-with-previous="always">
									<fo:block>
										<fo:inline font-family="universcbold">F1:</fo:inline>
										ore di punta (peak). Nei giorni dal lunedì al venerdì: dalle
										ore 8.00 alle ore 19.00
									</fo:block>
								
									<fo:block keep-with-previous="always">
										<fo:inline font-family="universcbold">F2:</fo:inline>
										ore intermedie (mid-level). Nei giorni dal lunedì al venerdì:
										dalle ore 7.00 alle ore 8.00 e dalle ore 19.00 alle ore 23.00.
										Il sabato: dalle ore 7.00 alle ore 23.00
									</fo:block>
								
									<fo:block keep-with-previous="always">
										<fo:inline font-family="universcbold">F3:</fo:inline>
										ore fuori punta (off-peak). Nei giorni dal lunedì al venerdì:
										dalle ore 23.00 alle ore 7.00. La domenica e i festivi*: tutte
										le ore della giornata
									</fo:block>
								
									<fo:block font-size="6pt" keep-with-previous="always">
										* Si considerano festivi: 1 e 6 gennaio;
										lunedì di Pasqua; 25 aprile; 1 maggio; 2 giugno;
										15 agosto; 1 novembre; 8, 25 e 26 dicembre.
									</fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
					</fo:table>
				</xsl:if>
			</xsl:if>
			
			-->
			
			<!-- <xsl:if test="$comunicazione_area_clienti='SI' or
						  PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP'] or
						  CONTRATTO_GAS[@CONSUMER='SI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='PROMO'] or
						  RINNOVI or
						  PROMO_SERVIZI[@TIPOLOGIA='CUSTOMER_LIST'] or
						  PROMO_SERVIZI[@TIPOLOGIA='NCGC_B13'] or
						  PROMO_SERVIZI[@TIPOLOGIA='ERR_CORR'] or
						  PROMO_SERVIZI[@TIPOLOGIA='DATI_CATASTALI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS'] or
						  PROMO_SERVIZI[@TIPOLOGIA='OPZ_VERDE'] or
						  PROMO_SERVIZI[@TIPOLOGIA='BONUS_ELETTRICO'] or
						  PROMO_SERVIZI[@TIPOLOGIA='MIX_FONTI'] or
						  PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI'] or
						  ($mese_fattura &gt; '201103' and $mese_fattura &lt; '201105') or
						  $mese_fattura &gt; '201107'"> -->
			
								
				<fo:table space-before="2mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
					<fo:table-column column-width="proportional-column-width(100)"/>
					
					<fo:table-body end-indent="0pt" start-indent="0pt">
						<fo:table-row height="3mm">
							<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
								</xsl:if>
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
							
						<fo:table-row keep-with-previous="always">
							<fo:table-cell>
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
								</xsl:if>
								
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									
									<fo:table-header>
										<fo:table-row height="6mm">
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
												
												<fo:block font-family="universcbold" font-size="11pt" xsl:use-attribute-sets="brd.b.000">
													<xsl:if test="not($color='black')">
														<xsl:attribute name="color">#0065ae</xsl:attribute>
													</xsl:if>
													COMUNICAZIONI AI CLIENTI
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-header>
									
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<fo:table-row>
											<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center" padding-bottom="1mm">
											
												<fo:block xsl:use-attribute-sets="font.comunicazioni"
													margin-left="2mm" margin-right="2mm">
													
													<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']">
														<xsl:for-each select="PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']">
															<xsl:call-template name="COMUNICAZIONI_PROMO_TOP">
																<xsl:with-param name="color" select="$color"/>
															</xsl:call-template>
															<xsl:if test="position()&lt;last()">
																<fo:block xsl:use-attribute-sets="block.separatore">
																	&#160;
																</fo:block>
															</xsl:if>
														</xsl:for-each>
													</xsl:if>
													
													<fo:block xsl:use-attribute-sets="block.separatore">
														&#160;
													</fo:block>
													
													
													<fo:block font-family="universcbold" font-size="8.5pt">
														<xsl:if test="not($color='black')">
															<xsl:attribute name="color">#0065ae</xsl:attribute>
														</xsl:if>
														Perché pagare entro la data di scadenza
													</fo:block>
													
													<fo:block keep-with-previous="always" font-size="7pt">
														La fattura deve essere pagata per intero entro la data di scadenza. In caso di ritardato pagamento (anche parziale) di qualsiasi importo dovuto ad Energit, dal giorno successivo alla scadenza del termine previsto per il pagamento decorreranno a carico del Cliente gli interessi di mora, applicati, disciplinati e determinati in conformità a quanto previsto dal Decreto Legislativo n. 231/2002 e calcolati in base ad un tasso di interesse pari a Euribor 6 mesi maggiorato 5%, fatto comunque salvo il diritto di Energit al risarcimento del maggior danno. Limitatamente ai Clienti con consumi non superiori a 50.000 Smc/anno in una o più delle fatture emesse da Energit, gli interessi di mora saranno calcolati su base annua al tasso ufficiale di riferimento, così come definito ai sensi dell’articolo 2 del Decreto Legislativo 24 giugno 1998 n. 213 (TUR) aumentato di 3,5 punti percentuali. Con esclusivo riferimento alla prima fattura in cui si registri un inadempimento in relazione ai pagamenti, il Cliente sarà tenuto al pagamento del solo interesse legale per i primi 10 (dieci) giorni di ritardo.
														In caso di mancato pagamento (anche parziale) di qualsiasi importo dovuto ad Energit, decorsi 5 (cinque) giorni dalla scadenza del termine indicato nella fattura per il pagamento, Energit invierà al Cliente, ai sensi della delibera AEEG n. ARG/gas 99/11, una comunicazione in cui indicherà il termine, in ogni caso non inferiore a 5 (cinque) giorni, entro cui il Cliente sarà tenuto ad effettuare il pagamento di quanto dovuto ad Energit.
														Decorsi inutilmente 3 giorni dal termine ultimo per il pagamento, la Società, in conformità a quanto previsto dalle Condizioni Generali di contratto ed ai sensi dell'articolo 4 della delibera ARG/gas n.99/11 dell'Autorità dell'Energia Elettrica e del Gas e s.m.i., richiederà al distributore locale competente la chiusura del punto di riconsegna (pdr) per sospensione della fornitura per morosità. In tale ipotesi, qualora il cliente finale non renda possibile l’esecuzione della chiusura del pdr, la Società si riserva la facoltà di effettuare l’interruzione dell’alimentazione del punto di riconsegna, con oneri a carico del cliente. Energit si riserva altresì il diritto di risolvere il contratto in caso di mancato pagamento delle somme dovute.

													</fo:block>
													
													<!--
													<xsl:if test="$bordi='NO'">
														<fo:block keep-with-previous="always" space-before="3mm" xsl:use-attribute-sets="brd.b.000" padding-left="2mm" padding-right="2mm"/>
													</xsl:if>
													-->
													
													
													<!--xsl:if test="./child::PRODOTTO_SERVIZIO='Onda GAS FACILE - OGFXXUN0112SC'">-->
													
													<!--</xsl:if>-->
													<fo:block font-family="universcbold" keep-with-previous="always" space-before="5mm">
														ASSICURAZIONE PER I CLIENTI FINALI 
													</fo:block>
													<fo:block keep-with-previous="always">
														La informiamo che chiunque usi, anche occasionalmente, il gas fornito tramite reti di distribuzione o reti di  
														trasporto, beneficia in via automatica di una copertura assicurativa contro gli  incidenti da gas, ai sensi 
														della deliberazione 191/2013/R/gas dell’Autorità per  l’energia elettrica e il gas. Per ulteriori informazioni 
														può contattare lo Sportello per il consumatore di energia al numero verde 800.166.654 o con le  modalità indicate
														nel sito internet www.autorita.energia.it
													</fo:block>
													<fo:block font-family="universcbold" keep-with-previous="always" space-before="5mm">
														BONUS GAS PER LE FAMIGLIE IN STATO DI DISAGIO ECONOMICO
													</fo:block>
													<fo:block keep-with-previous="always">
														Il Bonus Gas è uno sconto sulle fatture del gas riservato alle famiglie a basso reddito e numerose; il bonus può essere richiesto
														al proprio Comune per l’abitazione di residenza.
														Per maggiori informazioni visiti il sito http://www.autorita.energia.it/it/bonus_gas.htm o contatti il numero verde 800.166.654.

													</fo:block>
													
													
													<xsl:if test="./child::CONTRATTO_GAS/CONSUMI_ULTIMO_ANNO/CONSUMO">
													<xsl:for-each select="./child::CONTRATTO_GAS">
													<!-- inizio -->
													<fo:block font-family="universcbold" keep-with-previous="always" space-before="5mm">
														CONSUMI DI GAS NATURALE NEGLI ULTIMI 12 MESI
													</fo:block>
													<fo:block keep-with-previous="always">
														Consulti il prospetto dei suoi consumi medi negli ultimi 12 mesi, per tenere sempre sotto controllo le variazioni dei consumi nel tempo! 
														Per i contratti attivi da meno di 12 mesi lo storico è riferito all’effettivo periodo di fornitura con Energit. 

													</fo:block>
													<fo:block space-before="3mm">
													<!--<xsl:if test="/child::CONSUMI_ENERGIA_ULTIMI_14_MESI">-->
													
													<fo:table keep-with-previous="always" table-layout="fixed" width="100%">
													<!--<xsl:for-each select="./child::CONTRATTO_GAS">-->
													
														<fo:table-column column-width="proportional-column-width(40)"/> 
														<fo:table-column column-width="proportional-column-width(20)"/>
														<fo:table-column column-width="proportional-column-width(20)"/> 
														<fo:table-column column-width="proportional-column-width(20)"/>
														<fo:table-column column-width="proportional-column-width(20)"/> 
														<fo:table-column column-width="proportional-column-width(20)"/>
														<fo:table-column column-width="proportional-column-width(20)"/> 
														<fo:table-column column-width="proportional-column-width(20)"/>
														<fo:table-column column-width="proportional-column-width(20)"/> 
														<fo:table-column column-width="proportional-column-width(20)"/>
														<fo:table-column column-width="proportional-column-width(20)"/>
														<fo:table-column column-width="proportional-column-width(20)"/>
														<fo:table-column column-width="proportional-column-width(20)"/>
														
														
														<fo:table-body>
																										
															<fo:table-row height="3mm" font-size="7pt" border-bottom="0.5pt solid black" >
																
																	<fo:table-cell border-top="0.5pt solid black" border-left="0.5pt solid black">
																		<fo:block></fo:block>
																	</fo:table-cell>
																
																
																<xsl:for-each select="./child::CONSUMI_ULTIMO_ANNO/CONSUMO">
																<fo:table-cell border-top="0.5pt solid black" border-right="0.5pt solid black" border-left="0.5pt solid black">
									
																	<fo:block>
																		<xsl:value-of select="@DESCRIZIONE_MESE_RIFERIMENTO"/>
																	</fo:block>
																</fo:table-cell>
																</xsl:for-each>
																
															</fo:table-row>
															
															<fo:table-row keep-with-previous="always" height="5.5mm" font-size="7pt" border-bottom="0.5pt solid black">
																	<fo:table-cell border-left="0.5pt solid black" border-right="0.5pt solid black">
																		<fo:block font-size="6.5pt">
																			Consumo medio mensile (Smc)
																		</fo:block>
																	</fo:table-cell>
																	<xsl:for-each select="./child::CONSUMI_ULTIMO_ANNO/CONSUMO">
																	
																	<fo:table-cell border-left="0.5pt solid black" border-right="0.5pt solid black">
									
																		<fo:block text-align="center">
																			<xsl:value-of select="./child::CONSUMO_FATTURATO"/>
																		</fo:block>
																	</fo:table-cell>
																	</xsl:for-each>
															
															</fo:table-row>
															
															<fo:table-row keep-with-previous="always" height="3mm" font-size="7pt" border-bottom="0.5pt solid black">
																	<fo:table-cell border-left="0.5pt solid black">
																		<fo:block font-size="6.5pt">
																			Consumo medio giornaliero (Smc)
																		</fo:block>
																	</fo:table-cell>
																	<xsl:for-each select="./child::CONSUMI_ULTIMO_ANNO/CONSUMO">
																	<fo:table-cell border-right="0.5pt solid black" border-left="0.5pt solid black">
									
																		<fo:block text-align="center">
																			<xsl:value-of select="./child::CONSUMO_MEDIO_GIORNALIERO"/>
																		</fo:block>
																	</fo:table-cell>
																	</xsl:for-each>
															</fo:table-row>
														</fo:table-body>
														
													</fo:table>
													
													</fo:block>
													</xsl:for-each>
													</xsl:if>
													
													<!-- fine -->
													

													<xsl:if test="$bordi='NO'">
														<fo:block keep-with-previous="always" space-before="4mm" xsl:use-attribute-sets="brd.b.000" padding-left="2mm" padding-right="2mm"/>
													</xsl:if>
													<fo:block font-family="universcbold">
														<xsl:if test="not($color='black')">
															<xsl:attribute name="color">#0065ae</xsl:attribute>
														</xsl:if>
														MODALITA' DI PAGAMENTO DELLE FATTURE
													</fo:block>
													<fo:block keep-with-previous="always">
														Le ricordiamo che i nostri clienti possono scegliere tra le seguenti modalità di pagamento delle fatture: addebito diretto SEPA Direct Debit (SDD, che ha sostituito il RID),
														
														 bonifico bancario, bollettino postale, altre modalità concordate con Energit.
													</fo:block>
															
												</fo:block>
												
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							
							</fo:table-cell>
						</fo:table-row>
						
					
						<fo:table-row height="5mm" keep-with-previous="always">
							<fo:table-cell display-align="center">
								<xsl:if test="not($bordi='NO')">
									<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
									<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
								</xsl:if>
								<fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			
			<!-- </xsl:if> -->
		</fo:block>
	
	</xsl:template>
	
	<xsl:template name="TEMPLATE_RIEPILOGO_FATTURA">
		
		<xsl:variable name="colonne">
			<xsl:if test="@SPOT='NO' or not(@SPOT)">6</xsl:if>
			<xsl:if test="@SPOT='SI'">5</xsl:if>
		</xsl:variable>
		
		<fo:table space-before="5mm" table-layout="fixed" width="100%" xsl:use-attribute-sets="font.table">
			<xsl:if test="@SPOT='NO' or not(@SPOT)">
				<fo:table-column column-width="proportional-column-width(22)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
				<fo:table-column column-width="proportional-column-width(13)"/>
			</xsl:if>
			<xsl:if test="@SPOT='SI'">
				<fo:table-column column-width="proportional-column-width(30)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
			</xsl:if>
		
			<fo:table-header>
				
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="cell.header">
						<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne+1"/></xsl:attribute>
						<fo:block>
							<fo:inline>RIEPILOGO FATTURA</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
		
				<fo:table-row text-align="center" display-align="center" height="4mm">
					<xsl:if test="@SPOT='SI'">
						<xsl:attribute name="height">5mm</xsl:attribute>
					</xsl:if>
					<fo:table-cell>
						<fo:block>SERVIZIO</fo:block>
					</fo:table-cell>
					
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						<fo:table-cell>
							<fo:block>N. CONTRATTO</fo:block>
						</fo:table-cell>
					</xsl:if>
		
					<fo:table-cell>
						<fo:block>IMPONIBILE (euro)</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>IVA</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>CODICE IVA</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>IMPORTO IVA</fo:block>
					</fo:table-cell>
		
					<fo:table-cell>
						<fo:block>TOTALE (euro)</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
		
			<fo:table-body>
				<xsl:for-each select="RIEPILOGO_FATTURA">
					<fo:table-row text-align="center" display-align="center">
						<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI']">
							<xsl:attribute name="height">4mm</xsl:attribute>
						</xsl:if>
						<xsl:if test="./ancestor::DOCUMENTO[@SPOT='SI'] and position()&lt;last()">
							<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
						</xsl:if>
						<xsl:if test="position()=last()">
							<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
						</xsl:if>
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="PRODOTTO_SERVIZIO" />
							</fo:block>
						</fo:table-cell>
						
						<xsl:if test="./ancestor::DOCUMENTO[@SPOT='NO'] or not(./ancestor::DOCUMENTO[@SPOT])">
							<fo:table-cell>
								<fo:block>
									<xsl:value-of select="CONTRATTO" />
								</fo:block>
							</fo:table-cell>
						</xsl:if>
		
						<fo:table-cell>
								<fo:block>
											<xsl:value-of select="IMPONIBILE" />	<!-- imponibile al 10% -->
										</fo:block>
									<!--
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="proportional-column-width(100)"/> 
								
								<fo:table-body>
								<fo:table-row height="3mm">
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="IMPONIBILE" />	
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
									<fo:table-cell>
									<fo:block>
										<xsl:value-of select="IMPONIBILE" />		
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								</fo:table-body>
								</fo:table>
							-->
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="DESCRIZIONE_ALIQUOTA_IVA" />	<!-- imponibile al 10% -->
							</fo:block>
							<!--
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="proportional-column-width(100)"/> 
								
								<fo:table-body>
								<fo:table-row height="3mm">
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="ALIQUOTA_IVA" />%	
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
									<fo:table-cell>
									<fo:block>
										<xsl:value-of select="ALIQUOTA_IVA" />%		
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								</fo:table-body>
							</fo:table>
							-->
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:choose>
									<xsl:when test="CODICE_IVA">
										<xsl:value-of select="CODICE_IVA" />
									</xsl:when>
									<xsl:otherwise>
										-
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="IMPORTO_IVA" />	<!-- imponibile al 10% -->
							</fo:block>
							<!--
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="proportional-column-width(100)"/> 
								
								<fo:table-body>
								<fo:table-row height="3mm">
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="IMPORTO_IVA" />	
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
									<fo:table-cell>
									<fo:block>
										<xsl:value-of select="IMPORTO_IVA" />		
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								</fo:table-body>
								</fo:table>
							-->
						</fo:table-cell>
		
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="TOTALE" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
				
				<xsl:for-each select="DESCRIZIONI_CODICI_IVA">
					<fo:table-row keep-with-previous="always">
						<fo:table-cell>
							<fo:block>
								Codice IVA "<xsl:value-of select="CODICE_IVA" />"
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
							<fo:block>
								<xsl:value-of select="DESCRIZIONE_CODICE_IVA" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	
	<xsl:attribute-set name="font.table">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="line-height">9pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.pagare">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
	    <xsl:attribute name="line-height">7pt</xsl:attribute>
	    <xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.header">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	    <xsl:attribute name="font-family">universcbold</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	    <xsl:attribute name="padding-left">3mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border">
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.left">
		<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.right">
		<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.top">
		<xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border.bottom">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.left">
		<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.right">
		<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.top">
		<xsl:attribute name="border-top">1pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="border1.bottom">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
	</xsl:attribute-set>
	 
	<xsl:attribute-set name="font.fascia_header">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">9.5pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia_text">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">6pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.fascia">
	    <xsl:attribute name="font-family">universcbold</xsl:attribute>
	    <xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.legenda">
	    <xsl:attribute name="font-family">universc</xsl:attribute>
	    <xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">7pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.comunicazioni">
		<xsl:attribute name="font-family">universc</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.sepa">
		<xsl:attribute name="font-family">universc</xsl:attribute>
		<xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.title">
		<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="line-height">4mm</xsl:attribute>
		<xsl:attribute name="space-after">4mm</xsl:attribute>
		<xsl:attribute name="font-size">11pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.separatore">
		<xsl:attribute name="border-bottom">0.5pt dashed black</xsl:attribute>
		<xsl:attribute name="font-family">universcbold</xsl:attribute>
		<xsl:attribute name="space-after">1mm</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.rinnovi">
		<xsl:attribute name="padding">0.5mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.mix_fonti">
		<xsl:attribute name="padding">0.1mm</xsl:attribute>
		<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.articoli">
		<xsl:attribute name="font-family">Arial,sans-serif</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
		<xsl:attribute name="font-style">italic</xsl:attribute>
	</xsl:attribute-set>

</xsl:stylesheet>

