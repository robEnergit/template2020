<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>



<!--
***********************************
**                               **
**       Letture in fattura      **
**                               **
***********************************
-->
<xsl:template match="LETTURE">
	<xsl:param name="bordi"/>
	<xsl:param name="sfondo_titoli"/>
	<xsl:param name="color"/>
	
	<xsl:if test="@TEMPLATE">
		<xsl:call-template name="LETTURE_PARAMETRIZZATO">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
			<xsl:with-param name="tipologia" select="@TEMPLATE"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template> 



<xsl:template match="LETTURE">
<xsl:param name="bordi"/>
<xsl:param name="color"/>
<xsl:param name="svg-titoli"/>
<xsl:param name="tipo"/>
<xsl:param name="mese_fattura"/>

	<xsl:if test="child::LETTURA">
	
	<xsl:if test="$tipo='header'">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body>
			<fo:table-row keep-with-previous="always" height="2mm">
				<fo:table-cell>
					<fo:block></fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row height="6mm">
				<fo:table-cell padding-left="3mm" display-align="center">
					<xsl:attribute name="background-image"><xsl:value-of select="$svg-titoli"/></xsl:attribute>
					<fo:block text-align="start">
						<fo:inline xsl:use-attribute-sets="font_titolo_tabella_bold">LETTURE UTILIZZATE AI FINI DELLA FATTURAZIONE </fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
		</fo:table>
	</xsl:if>

	<xsl:if test="$tipo='body'">
		<xsl:call-template name="LETTURE_PARAMETRIZZATO">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="tipologia" select="@TEMPLATE"/>
			
		</xsl:call-template>
	</xsl:if>
	
	</xsl:if>
	
</xsl:template>


<!--
**********************************************************************************

			  TEMPLATE LETTURE_REALE_AUTOLETTURA_AUTOLETTURA

**********************************************************************************
-->


<xsl:template name="LETTURE_PARAMETRIZZATO">
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="tipologia"/>

	<xsl:param name="title_color">
		<xsl:if test="$color='black'">#ffffff</xsl:if>
		<xsl:if test="not($color='black')">#ffffff</xsl:if>
	</xsl:param>
	<xsl:param name="title_color2">
		<xsl:if test="$color='black'">#ffffff</xsl:if>
		<xsl:if test="not($color='black')">#ffffff</xsl:if>
	</xsl:param>
	
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="block_sintesi chr.008">
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(15)"/>
		<fo:table-column column-width="proportional-column-width(15)"/>
		<fo:table-column column-width="proportional-column-width(15)"/>
		<fo:table-column column-width="proportional-column-width(35)"/>
		<fo:table-body>
			<fo:table-row height="3mm">
				<fo:table-cell number-columns-spanned="5">
					<fo:block/>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row keep-with-previous="always">
				
				<fo:table-cell number-columns-spanned="5" display-align="center">
					<xsl:call-template name="Header">
						<xsl:with-param name="tipo" select="'Reale'"/>
						<xsl:with-param name="color1" select="$title_color"/>
						<xsl:with-param name="color2" select="$title_color2"/>
					</xsl:call-template>
				</fo:table-cell>
			</fo:table-row>
			
			<xsl:for-each select="./child::LETTURA"> <!--[@TIPO='ATTIVA'] -->
				<fo:table-row keep-with-previous="always" font-size="7pt">
					<fo:table-cell border="0.5pt solid black" border-right="0.5pt solid black">
					<fo:block>			
						<xsl:value-of select="./ancestor::LETTURE/ancestor::CONTRATTO_GAS/MATRICOLA_CONTATORE"/>
					</fo:block>
					</fo:table-cell>
					
					<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<xsl:value-of select="./child::DATA_LETTURA"/>
					</fo:block>
					</fo:table-cell>
					<!--
					<fo:table-cell border-right="0.5pt solid black">
					<fo:block>
						<xsl:value-of select="./child::DATA_LETTURA_ATTUALE"/>
					</fo:block>
					</fo:table-cell>
					-->
					<fo:table-cell border="0.5pt solid black">
					<fo:block>
						<xsl:value-of select="./child::VALORE_LETTURA"/>
					</fo:block>
					</fo:table-cell>
					<!--
					<fo:table-cell border-right="0.5pt solid black">
					<fo:block>
						<xsl:value-of select="./child::VALORE_LETTURA_ATTUALE"/>
					</fo:block>
					</fo:table-cell>
					-->
					<fo:table-cell border="0.5pt solid black">
					<fo:block>
						  <xsl:value-of select="./child::TIPO_LETTURA"/>
					</fo:block>
					</fo:table-cell>
					<!--
					<fo:table-cell border-right="0.5pt solid black">
					<fo:block text-align="center">
						<xsl:value-of select="./child::CONSUMO_MEDIO_GIORNALIERO"/>
					</fo:block>
					</fo:table-cell>
					-->
					<fo:table-cell>
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</xsl:for-each>
			<!--
			<fo:table-row font-size="8pt">
				
				<fo:table-cell  display-align="center">
					<fo:block/>
				
				</fo:table-cell>
				<fo:table-cell  display-align="center">
					<fo:block/>
					
					
				</fo:table-cell>
				<fo:table-cell  display-align="center">
					<fo:block/>
					
					
				</fo:table-cell>
				<fo:table-cell  display-align="center">
					<fo:block text-align="right">
					Totale
					</fo:block>
				</fo:table-cell>
				<fo:table-cell  display-align="center">
					<fo:block>
					consumo 
					</fo:block>
				</fo:table-cell>
				<fo:table-cell  display-align="center">
					<fo:block text-align="left">
					 fatturato
					</fo:block>
				</fo:table-cell>
				<fo:table-cell  display-align="center">
					<fo:block text-align="right">
					55.21257
					</fo:block>
				</fo:table-cell>
				
			</fo:table-row>
			-->
		</fo:table-body>
	</fo:table>
	
	
			
</xsl:template>



<xsl:template name="valore_lettura">
	<xsl:param name="valore"/>
	<xsl:param name="colore"/>
	<!-- <fo:table-cell display-align="center" border-bottom="0.5pt solid black" border-right="0.5pt solid black" xsl:use-attribute-sets="chr.008">
		<fo:block>
			<xsl:choose>
				<xsl:when test="$valore and not($valore='')">
					<xsl:value-of select="$valore"/>
				</xsl:when>
				<xsl:otherwise>
					-
				</xsl:otherwise>
			</xsl:choose>
		</fo:block>
	</fo:table-cell> -->
	<fo:table-cell display-align="center" xsl:use-attribute-sets="chr.008">
		<fo:table border="0.5pt solid black" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(100)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell display-align="center">
						<xsl:if test="not($colore='')">
							<xsl:attribute name="background-color"><xsl:value-of select="$colore"/></xsl:attribute>
						</xsl:if>
						<fo:block>
							<xsl:choose>
								<xsl:when test="$valore and not($valore='')">
									<xsl:value-of select="$valore"/>
								</xsl:when>
								<xsl:otherwise>
									-
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</fo:table-cell>
</xsl:template>

<xsl:template name="Header">
	
	<xsl:param name="tipo"/>
	<xsl:param name="color1"/>
	<xsl:param name="color2"/>
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
	<xsl:attribute name="background-color"><xsl:value-of select="$color1"/></xsl:attribute>
		<!-- <fo:table-column column-width="proportional-column-width(14.28)"/>
		<fo:table-column column-width="proportional-column-width(14.28)"/> -->
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(15)"/>
		<fo:table-column column-width="proportional-column-width(15)"/>
		<fo:table-column column-width="proportional-column-width(15)"/>
		<fo:table-column column-width="proportional-column-width(35)"/>
		
		<fo:table-body end-indent="0pt" start-indent="0pt">
			
			<fo:table-row keep-with-previous="always">
				<!--<xsl:if test="not($tipo='Costante') and not($tipo='Costante2')">-->
					
					<fo:table-cell border="0.5pt solid black" display-align="center">
						<fo:block>
							Matricola contatore
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.5pt solid black" display-align="center">
					
						<fo:block>
							Data lettura
						</fo:block>
						
					</fo:table-cell>
					<fo:table-cell border="0.5pt solid black" display-align="center">
						<fo:block>
							Lettura (mc)
						</fo:block>
						
					</fo:table-cell>
					
					<!--
					<fo:table-cell border="0.5pt solid black" display-align="center">
						<fo:block>
							da lettura
						</fo:block>
						
					</fo:table-cell>
					-->
					<!--
					<fo:table-cell border="0.5pt solid black" display-align="center">
						<fo:block>
							a lettura
						</fo:block>
					
					</fo:table-cell>
					-->
					<fo:table-cell border="0.5pt solid black" display-align="center">
						<fo:block>
							Tipo lettura
						</fo:block>
					</fo:table-cell>
					<!--
					<fo:table-cell border="0.5pt solid black" display-align="center">
						<fo:block>
							Consumo medio giornaliero (Smc)
						</fo:block>
					</fo:table-cell>
					-->
				<!--</xsl:if>-->
					<fo:table-cell>
						<fo:block>
						</fo:block>
					</fo:table-cell>
			</fo:table-row>
			
		</fo:table-body>
		
	</fo:table>
	
</xsl:template>

</xsl:stylesheet>