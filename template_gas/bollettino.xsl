<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:barcode="http://barcode4j.krysalis.org/ns">
	
	<xsl:output encoding="UTF-8" />

	<!--
		******************************************************************* **
		** ** In questo template vengono visualizzati: ** ** - il bollettino
		postale ** ** **
		*******************************************************************
	-->
<!-- ouooouoauoaudoasudoasudo -->
	<xsl:template name="BOLLETTINO">
	
	<xsl:variable name="status_piu_uno"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:variable>

		<!--
			************************************************************************************
			** SPAZIO SUPERIORE BOLLETTINO **
			************************************************************************************
		-->

		<fo:page-sequence initial-page-number="1" force-page-count="no-force" master-reference="pm1" id="B">
				 <xsl:variable name="trad" select='translate(translate(./child::BOLLETTINO/child::TOT_COST_EUR,".",""),",",".")'/>

<xsl:if test="not($trad &gt; 1500)">

			<fo:static-content flow-name="xsl-region-before">
				
				<fo:block-container position="absolute" top="10mm"
					left="5mm" width="280mm" height="90mm" xsl:use-attribute-sets="layout-debug font.boll">
					<fo:table table-layout="fixed" width="100%">
						<fo:table-column column-width="32%" />
						<fo:table-column column-width="14%" />
						<fo:table-column column-width="54%" />
						<fo:table-body>
							<fo:table-row>
								<xsl:choose>
									
									<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_PE_REQ']">
										
										<!-- ************************************************************** -->
									<fo:table-cell padding-right="2mm" 
									            padding-left="2mm"
												padding-top="2mm"
												border-left="0.5pt dashed black"
												border-top="0.5pt dashed black"
												border-bottom="0.5pt dashed black"
												border-right="0.5pt dashed black"
												xsl:use-attribute-sets="font.boll"
												line-height="8pt" text-align="justify" font-size="7.5pt"> 
											<fo:block font-weight="bold" text-align="center" font-size="9pt">
												NEI PUNTI LIS PAGA DI LOTTOMATICA PUO’ PAGARE QUESTO BOLLETTINO IN TUTTA COMODITA’! 
											</fo:block>
											<fo:block text-align="justify" space-before="2mm">
												Siamo lieti di informarla che i nostri Clienti oggi possono recarsi in uno degli oltre 50.000 
												punti <fo:inline font-weight="bold">LIS PAGA<fo:inline font-size="4" baseline-shift="super">1</fo:inline> di Lottomatica </fo:inline>(ricevitorie, tabaccherie, edicole e bar) per pagare i bollettini postali 
												Energit in modo semplice e veloce.
											</fo:block>
											<fo:block text-align="justify" space-before="1.5mm">
												<fo:inline font-weight="bold">Il pagamento può essere effettuato con carte di credito e prepagate VISA e MasterCard, 
												carte PagoBancomat o contanti</fo:inline>. In più, se utilizza le prepagate Lottomaticard<fo:inline font-size="4" baseline-shift="super">2</fo:inline>, la commissione costa meno<fo:inline font-size="4" baseline-shift="super">3</fo:inline>.
											

											</fo:block>
											
											
												<fo:block text-align="justify" space-before="1.5mm">Cerchi il <fo:inline font-weight="bold">punto LIS PAGA </fo:inline>più vicino a lei su www.lottomaticaservizi.it.
												</fo:block>
												<fo:block text-align="justify" space-before="1.5mm" >Grazie!
												</fo:block>
												<fo:block text-align="justify" space-before="1.5mm" 
												border-bottom="0.5pt dashed black">
												</fo:block>
												
												<fo:block text-align="justify" space-before="1.5mm"><fo:inline font-weight="bold">ISTRUZIONI PER IL PUNTO VENDITA </fo:inline>- Incassi velocemente questo bollettino utilizzando il lettore ottico 
												per acquisire il <fo:inline font-weight="bold">codice a barre riportato sul bollettino</fo:inline>.</fo:block>

												 <!-- Logo lottomatica --> 
												<fo:table table-layout="fixed" width="100%" space-before="2mm">
												<fo:table-column column-width="48%" />
												<fo:table-column column-width="52%" />
												<fo:table-body>
													<fo:table-cell>
														<fo:block text-align="justify" space-before="1.5mm" font-size="6"><fo:inline font-weight="bold">Messaggio pubblicitario con finalità promozionale</fo:inline>.</fo:block>
														<fo:block text-align="justify" space-before="0.2mm" font-size="5.5" line-height="7pt"> <fo:inline baseline-shift="super">1</fo:inline> LIS PAGA è il marchio che identifica il servizio di pagamento fornito da LIS IP S.p.A. che permette di pagare  bollette, tributi e tanto altro nei punti vendita convenzionati LIS IP S.p.A. Per   
														</fo:block>
													</fo:table-cell>
													<fo:table-cell >

													<fo:block margin-top="11mm" margin-right="1mm">
						                            <fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="47mm" src="img/LIS PAGA POS_NEW_G_ALL.jpg"/>
					                              </fo:block>

													</fo:table-cell>
												</fo:table-body>
											    </fo:table>

												<fo:block text-align="justify" space-before="0.2mm" font-size="5.5" line-height="7pt"> 
												le condizioni economiche del servizio, consultare i fogli informativi disponibili presso i punti LIS PAGA e sul sito Lottomaticaservizi.it</fo:block>
												<fo:block text-align="justify" space-before="0.2mm" font-size="5.5" line-height="7pt"><fo:inline baseline-shift="super">2</fo:inline> Le prepagate Lottomaticard sono emesse da CartaLIS Imel S.p.A, anche in collaborazione con partner terzi. LIS CARD è il marchio che identifica 
												il servizio erogato da CartaLIS Imel S.p.A. che permette di acquistare e ricaricare le prepagate Lottomaticard nei punti vendita convenzionati da CartaLIS
												Imel S.p.A. Per le condizioni economiche e contrattuali, consultare i fogli informativi disponibili presso i punti LIS CARD e sul sito Lottomaticard.it
												</fo:block>
												<fo:block text-align="justify" space-before="0.2mm" font-size="5.5" line-height="7pt"><fo:inline baseline-shift="super">3</fo:inline> Per conoscere gli importi delle commissioni di pagamento vada su Lottomaticaservizi.it nella sezione LIS PAGA.
												</fo:block>
										
											
										</fo:table-cell>
										<fo:table-cell number-columns-spanned="2"
												padding-right="2mm" 
									            padding-left="2mm"
												padding-top="2mm"
												xsl:use-attribute-sets="font.boll"
												line-height="7pt" text-align="justify" font-size="7pt">
											<fo:block>Gentile Cliente,</fo:block>
											<fo:block>
												abbiamo inoltrato alla sua Banca la richiesta
												di attivazione della modalità di pagamento
												con addebito diretto SEPA Direct Debit (SDD).
											</fo:block>
											<fo:block font-weight="bold">
												In attesa di conferma da parte della sua Banca,
												la informiamo che per il pagamento di questa
												fattura dovrà utilizzare il bollettino postale
												che trova di seguito.
											</fo:block>
											<fo:block font-weight="bold">Grazie!</fo:block>
										</fo:table-cell>
										<!-- ***************** -->
									</xsl:when>
									<xsl:otherwise>
									
									
									<!-- ************************************************************** -->
									<fo:table-cell padding-right="2mm" 
									            padding-left="2mm"
												padding-top="2mm"
												border-left="0.5pt dashed black"
												border-top="0.5pt dashed black"
												border-bottom="0.5pt dashed black"
												border-right="0.5pt solid black"
												xsl:use-attribute-sets="font.boll"
												line-height="8pt" text-align="justify" font-size="7.5pt"> 
											<fo:block font-weight="bold" text-align="center" font-size="9pt">
												NEI PUNTI LIS PAGA DI LOTTOMATICA PUO’ PAGARE QUESTO BOLLETTINO IN TUTTA COMODITA’! 
											</fo:block>
											<fo:block text-align="justify" space-before="2mm">
												Siamo lieti di informarla che i nostri Clienti oggi possono recarsi in uno degli oltre 50.000 
												punti <fo:inline font-weight="bold">LIS PAGA<fo:inline font-size="4" baseline-shift="super">1</fo:inline> di Lottomatica </fo:inline>(ricevitorie, tabaccherie, edicole e bar) per pagare i bollettini postali 
												Energit in modo semplice e veloce.
											</fo:block>
											<fo:block text-align="justify" space-before="1.5mm">
												<fo:inline font-weight="bold">Il pagamento può essere effettuato con carte di credito e prepagate VISA e MasterCard, 
												carte PagoBancomat o contanti</fo:inline>. In più, se utilizza le prepagate Lottomaticard<fo:inline font-size="4" baseline-shift="super">2</fo:inline>, la commissione costa meno<fo:inline font-size="4" baseline-shift="super">3</fo:inline>.
											

											</fo:block>
											
											
												<fo:block text-align="justify" space-before="1.5mm">Cerchi il <fo:inline font-weight="bold">punto LIS PAGA </fo:inline>più vicino a lei su www.lottomaticaservizi.it.
												</fo:block>
												<fo:block text-align="justify" space-before="1.5mm" >Grazie!
												</fo:block>
												<fo:block text-align="justify" space-before="1.5mm" 
												border-bottom="0.5pt dashed black">
												</fo:block>
												
												<fo:block text-align="justify" space-before="1.5mm"><fo:inline font-weight="bold">ISTRUZIONI PER IL PUNTO VENDITA </fo:inline>- Incassi velocemente questo bollettino utilizzando il lettore ottico 
												per acquisire il <fo:inline font-weight="bold">codice a barre riportato sul bollettino</fo:inline>.</fo:block>
					
												
												
												
												
												
												 <!-- Logo lottomatica --> 
												<fo:table table-layout="fixed" width="100%" space-before="2mm">
												<fo:table-column column-width="48%" />
												<fo:table-column column-width="52%" />
												<fo:table-body>
													<fo:table-cell>
														<fo:block text-align="justify" space-before="1.5mm" font-size="6"><fo:inline font-weight="bold">Messaggio pubblicitario con finalità promozionale</fo:inline>.</fo:block>
														<fo:block text-align="justify" space-before="0.2mm" font-size="5.5" line-height="7pt"> <fo:inline baseline-shift="super">1</fo:inline> LIS PAGA è il marchio che identifica il servizio di pagamento fornito da LIS IP  S.p.A. che permette di pagare  bollette, tributi e tanto altro nei punti vendita convenzionati LIS IP S.p.A. Per </fo:block>
													</fo:table-cell>
													<fo:table-cell >

													<fo:block margin-top="11mm" margin-right="1mm">
						                            <fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="47mm" src="img/LIS PAGA POS_NEW_G_ALL.jpg"/>
					                              </fo:block>

													</fo:table-cell>
												</fo:table-body>
											    </fo:table>

												<fo:block text-align="justify" space-before="0.2mm" font-size="5.5" line-height="7pt"> 
												le condizioni economiche del servizio, consultare i fogli informativi disponibili presso i punti LIS PAGA e sul sito Lottomaticaservizi.it</fo:block>
												<fo:block text-align="justify" space-before="0.2mm" font-size="5.5" line-height="7pt"><fo:inline baseline-shift="super">2</fo:inline> Le prepagate Lottomaticard sono emesse da CartaLIS Imel S.p.A, anche in collaborazione con partner terzi. LIS CARD è il marchio che identifica 
												il servizio erogato da CartaLIS Imel S.p.A. che permette di acquistare e ricaricare le prepagate Lottomaticard nei punti vendita convenzionati da CartaLIS
												Imel S.p.A. Per le condizioni economiche e contrattuali, consultare i fogli informativi disponibili presso i punti LIS CARD e sul sito Lottomaticard.it
												</fo:block>
												<fo:block text-align="justify" space-before="0.2mm" font-size="5.5" line-height="7pt"><fo:inline baseline-shift="super">3</fo:inline> Per conoscere gli importi delle commissioni di pagamento vada su Lottomaticaservizi.it nella sezione LIS PAGA.
												</fo:block>
										
											
										</fo:table-cell>
									
									<!-- ****************************************** -->
									
										<fo:table-cell padding-right="2mm" padding-left="2mm"
												padding-top="2mm"
												border-top="0.5pt solid black"
												border-bottom="0.5pt solid black"
												border-right="0.5pt solid black"
												xsl:use-attribute-sets="font.boll"
												line-height="7pt" text-align="justify" font-size="6pt">
											<xsl:choose>	
												<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
													<fo:block font-size="8pt" font-weight="bold" text-align="center" >
														ATTENZIONE! SDD RESPINTO
													</fo:block>
													<fo:block space-before="2mm">
														Le inviamo il bollettino postale perché
														<fo:inline font-weight="bold">
															la richiesta di attivazione dell'addebito diretto SEPA Direct Debit (SDD) è stata respinta dalla sua Banca
														</fo:inline>
														a causa di un disallineamento con i dati in nostro possesso.
													</fo:block>
													<fo:block space-before="1.5mm">
														Le ricordiamo che
														<fo:inline font-weight="bold">
															la modalità di pagamento a mezzo SDD è l'unica
															che esclude il pagamento del deposito cauzionale!
														</fo:inline>
													</fo:block>
													<fo:block space-before="1.5mm">
														Comunicando tempestivamente i dati corretti eviterà
														il versamento del deposito; compili e firmi il 
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEI NUOVI ESTREMI SDD</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="1.5mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari)</fo:block>
														<fo:block space-before="1mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
											
												<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_REVOCATO']">
													<fo:block font-size="8pt" font-weight="bold" text-align="center">
														ATTENZIONE! REVOCA SDD
													</fo:block>
													<fo:block space-before="2mm">
														Le inviamo il bollettino postale perché
														<fo:inline font-weight="bold">
															la sua Banca ci ha notificato la revoca della posizione SDD a lei intestata.
														</fo:inline>
													</fo:block>
													<fo:block space-before="1.5mm">
														Le ricordiamo che
														<fo:inline font-weight="bold">
															la modalità di pagamento tramite addebito diretto SEPA Direct Debit (SDD) è l'unica
															che esclude il pagamento del deposito cauzionale!
														</fo:inline>
													</fo:block>
													<fo:block space-before="1.5mm">
														Richiedendo tempestivamente il ripristino della modalità SDD
														eviterà il versamento del deposito; compili e firmi il
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEI NUOVI ESTREMI SDD</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="1.5mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari)</fo:block>
														<fo:block space-before="1.5mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
											
												<xsl:when test="../DOCUMENTO[@PRESENZA_DEPOSITO_CAUZIONALE='SI']">
													<fo:block font-size="8pt" font-weight="bold" text-align="center">
														RIENTRI IN POSSESSO DEL DEPOSITO CAUZIONALE!
													</fo:block>
													<fo:block space-before="2mm">
														Le ricordiamo che
														<fo:inline font-weight="bold">
															la modalità di pagamento tramite addebito diretto SEPA Direct Debit (SDD)
															è l'unica che esclude il pagamento del deposito cauzionale.
														</fo:inline>
													</fo:block>
													<fo:block space-before="1.5mm">
														Richiedendo il pagamento a mezzo SDD Energit le
														restituirà il deposito versato; compili e firmi il
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEI NUOVI ESTREMI SDD</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="1.5mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari)</fo:block>
														<fo:block space-before="3mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
												
												<xsl:when test="not(../DOCUMENTO[@PRESENZA_DEPOSITO_CAUZIONALE='SI'])">
													<fo:block font-size="8pt" font-weight="bold" text-align="center">
														SCELGA LA COMODITA' DEL PAGAMENTO SDD!
													</fo:block>
													<fo:block space-before="2mm">
														Compili e firmi il
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEGLI ESTREMI SDD</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="1.5mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari)</fo:block>
														<fo:block space-before="3mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:block />
												</xsl:otherwise>
												
											</xsl:choose>
										</fo:table-cell>
										
										
										<fo:table-cell padding-left="2mm" font-size="8pt" 	
										        padding-top="2mm"
												padding-right="2mm"
												border-top="0.5pt solid black"
												border-bottom="0.5pt solid black"
												border-right="0.5pt solid black">
											<fo:block font-weight="bold" text-align="center">
												MODULO DI COMUNICAZIONE ESTREMI SDD – CLIENTE N.
												<xsl:value-of select="CODICE_CLIENTE"/>
											</fo:block>
										
											
											<fo:table table-layout="fixed" width="100%" space-before="2mm">
												<fo:table-column column-width="6%" />
												<fo:table-column column-width="18%" />
												<fo:table-column column-width="11%" />
												<fo:table-column column-width="2%" />
												<fo:table-column column-width="12%" />
												<fo:table-column column-width="2%" />
												<fo:table-column column-width="20%" />
												<fo:table-column column-width="29%" />
												<fo:table-body font-size="7pt">
													<fo:table-cell>
														<fo:block font-weight="bold">Da Sig.</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>&#160; &#160; in qualità di</fo:block>
													</fo:table-cell>
													<fo:table-cell padding-top="1mm">
														<fo:block>
															<fo:external-graphic src="url(svg/unchecked.svg)"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															&#160; Cliente privato
														</fo:block>
													</fo:table-cell>
													<fo:table-cell padding-top="1mm">
														<fo:block>
															<fo:external-graphic src="url(svg/unchecked.svg)"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															&#160; Rappr. Legale Azienda
														</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
												</fo:table-body>
											</fo:table>
											
											
										
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm" font-size="7pt">
												<fo:table-column column-width="4%" />
												<fo:table-column column-width="22%" />
												<fo:table-column column-width="5%" />
												<fo:table-column column-width="20%" />
												<fo:table-column column-width="7%" />
												<fo:table-column column-width="17%" />
												<fo:table-column column-width="5%" />
												<fo:table-column column-width="20%" />
												<fo:table-body>
													<fo:table-cell>
														<fo:block>Via</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>Città</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>Tel/Fax</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>email</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
												</fo:table-body>
											</fo:table>
											
											
											<fo:block text-align="center" font-weight="bold" space-before="2mm" font-size="7pt">
												Con la presente il sottoscritto, quale intestatario
												del servizio gas  naturale Energit, comunica
												di seguito gli estremi per il pagamento delle prossime
												fatture Energit con modalità addebito diretto SEPA Direct Debit (SDD):
											</fo:block>
											
											 
											<fo:table table-layout="fixed" width="72%" space-before="1.5mm" font-size="6pt">
												<fo:table-column column-width="16%" />
												<fo:table-column column-width="23%" />
												<fo:table-column column-width="6%" />
												<fo:table-column column-width="28%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>Intestazione conto</fo:block>
														</fo:table-cell>
														<fo:table-cell xsl:use-attribute-sets="cell.field">
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>IBAN&#160;</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<fo:external-graphic src="url(svg/ibanfield.svg)"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm" font-size="7pt">
												<fo:table-column column-width="5.5%" />
												<fo:table-column column-width="42%" />
												<fo:table-column column-width="13%" />
												<fo:table-column column-width="39.5%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>Banca</fo:block>
														</fo:table-cell>
														<fo:table-cell xsl:use-attribute-sets="cell.field">
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>&#160;&#160;Agenzia/Filiale&#160;</fo:block>
														</fo:table-cell>
														<fo:table-cell xsl:use-attribute-sets="cell.field">
															<fo:block />
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											
											<fo:block text-align="justify" font-size="6pt" space-before="2mm">
												Il sottoscritto autorizza la Banca a margine a provvedere
												alla estinzione dei documenti di debito (fatture, ricevute,
												bollette, ecc.) emessi da Energ.it S.p.A. sopra citata,
												addebitando il conto sopraindicato e applicando le condizioni
												indicate nel foglio informativo analitico posto a disposizione
												del correntista e le norme previste per il servizio senza necessità,
												per la Banca, di inviare relativa contabile di addebito.
												Dichiara di essere a conoscenza che la Banca assume l'incarico
												dell'estinzione dei citati documenti che Energ.it S.p.A. invierà
												direttamente al debitore, prima della scadenza dell'obbligazione,
												a condizione che, al momento del pagamento, il conto sia in essere
												e assicuri la disponibilità sufficiente e che non sussistano
												ragioni che ne impediscono l'utilizzazione. In caso contrario
												la Banca resterà esonerata da ogni e qualsiasi responsabilità
												inerente al mancato pagamento e il pagamento stesso dovrà essere
												effettuato a Energ.it S.p.A. direttamente a cura del debitore.
												Il sottoscritto prende altresì atto che la Banca si riserva il
												diritto di recedere in ogni momento dal presente accordo. Prende
												pure atto che, ove intenda eccezionalmente sospendere l'estinzione
												di un documento di debito, dovrà dare immediato avviso alla Banca
												in tal senso entro la data di scadenza. Per quanto non espressamente
												richiamato, si applicano le “Norme che regolano i conti correnti di
												corrispondenza dei servizi connessi”: in deroga al terzo comma si
												conviene che il sottoscritto può riservarsi il diritto di chiedere
												alla Banca lo storno dell'addebito entro 5 giorni lavorativi
												dalla scadenza dell'obbligazione.
											</fo:block>
											
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm" font-size="7pt">
												<fo:table-column column-width="32%" />
												<fo:table-column column-width="68%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>Cod. Fiscale correntista (persona Fisica)</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																
																<fo:external-graphic src="url(svg/cffield.svg)"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm" font-weight="bold" font-size="7pt">
												<fo:table-column column-width="40%" />
												<fo:table-column column-width="60%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
																<fo:table-column column-width="35%" />
																<fo:table-column column-width="65%" />
																<fo:table-body>
																	<fo:table-row>
																		<fo:table-cell>
																			<fo:block>Firma del correntista</fo:block>
																		</fo:table-cell>
																		<fo:table-cell xsl:use-attribute-sets="cell.field">
																			<fo:block />
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
															
															<fo:table table-layout="fixed" width="100%" space-before="3mm" font-size="7pt">
																<fo:table-column column-width="56%" />
																<fo:table-column column-width="44%" />
																<fo:table-body>
																	<fo:table-row>
																		<fo:table-cell>
																			<fo:block>Firma dell'intestatario del contratto</fo:block>
																		</fo:table-cell>
																		<fo:table-cell xsl:use-attribute-sets="cell.field">
																			<fo:block />
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											<fo:block font-weight="bold">
												<fo:block font-size="7pt">IMPORTANTE</fo:block>
												<fo:block font-size="7pt">Allegare copia del documento di identità del correntista</fo:block>
												<fo:block font-size="7pt">(se diverso dall'intestatario del contratto)</fo:block>
											</fo:block>
											
										</fo:table-cell>
									</xsl:otherwise>
								</xsl:choose>
								
								
								
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block-container>
			</fo:static-content>

</xsl:if>



<xsl:if test="($trad &gt; 1500)">

			<fo:static-content flow-name="xsl-region-before">
				
				<fo:block-container position="absolute" top="10mm"
					left="5mm" width="280mm" height="90mm" xsl:use-attribute-sets="layout-debug font.boll">
					<fo:table table-layout="fixed" width="100%">
						<fo:table-column column-width="22%" />
						<fo:table-column column-width="78%" />
						<fo:table-body>
							<fo:table-row>
								<xsl:choose>
									
									<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_PE_REQ']">
										<fo:table-cell number-columns-spanned="2"
												padding-top="6cm" font-size="10pt"
												line-height="18pt">
											<fo:block>Gentile Cliente,</fo:block>
											<fo:block>
												abbiamo inoltrato alla sua Banca la richiesta
												di attivazione della modalità di pagamento
												con addebito diretto SEPA Direct Debit (SDD).
											</fo:block>
											<fo:block font-weight="bold">
												In attesa di conferma da parte della sua Banca,
												la informiamo che per il pagamento di questa
												fattura dovrà utilizzare il bollettino postale
												che trova di seguito.
											</fo:block>
											<fo:block font-weight="bold">Grazie!</fo:block>
										</fo:table-cell>
									</xsl:when>
									<xsl:otherwise>
										<fo:table-cell padding-right="3mm"
												border-right="0.5pt dashed black"
												xsl:use-attribute-sets="font.boll"
												line-height="12pt" text-align="justify">
											<xsl:choose>
												
												<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
													<fo:block font-size="10pt" font-weight="bold">
														ATTENZIONE! SDD RESPINTO
													</fo:block>
													<fo:block space-before="2mm">
														Le inviamo il bollettino postale perché
														<fo:inline font-weight="bold">
															la richiesta di attivazione dell'addebito diretto SEPA Direct Debit (SDD) è stata respinta dalla sua Banca
														</fo:inline>
														a causa di un disallineamento con i dati in nostro possesso.
													</fo:block>
													<fo:block space-before="1.5mm">
														Le ricordiamo che
														<fo:inline font-weight="bold">
															la modalità di pagamento a mezzo SDD è l'unica
															che esclude il pagamento del deposito cauzionale!
														</fo:inline>
													</fo:block>
													<fo:block space-before="1.5mm">
														Comunicando tempestivamente i dati corretti eviterà
														il versamento del deposito; compili e firmi il 
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEI NUOVI ESTREMI SDD</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="1.5mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari)</fo:block>
														<fo:block space-before="1mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
											
												<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_REVOCATO']">
													<fo:block font-size="10pt" font-weight="bold">
														ATTENZIONE! REVOCA SDD
													</fo:block>
													<fo:block space-before="4mm">
														Le inviamo il bollettino postale perché
														<fo:inline font-weight="bold">
															la sua Banca ci ha notificato la revoca della posizione SDD a lei intestata.
														</fo:inline>
													</fo:block>
													<fo:block space-before="2mm">
														Le ricordiamo che
														<fo:inline font-weight="bold">
															la modalità di pagamento tramite addebito diretto SEPA Direct Debit (SDD) è l'unica
															che esclude il pagamento del deposito cauzionale!
														</fo:inline>
													</fo:block>
													<fo:block space-before="2mm">
														Richiedendo tempestivamente il ripristino della modalità SDD
														eviterà il versamento del deposito; compili e firmi il
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEI NUOVI ESTREMI SDD</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="2mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari)</fo:block>
														<fo:block space-before="2mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
											
												<xsl:when test="../DOCUMENTO[@PRESENZA_DEPOSITO_CAUZIONALE='SI']">
													<fo:block font-size="10pt" font-weight="bold">
														RIENTRI IN POSSESSO DEL DEPOSITO CAUZIONALE!
													</fo:block>
													<fo:block space-before="4mm">
														Le ricordiamo che
														<fo:inline font-weight="bold">
															la modalità di pagamento tramite addebito diretto SEPA Direct Debit (SDD)
															è l'unica che esclude il pagamento del deposito cauzionale.
														</fo:inline>
													</fo:block>
													<fo:block space-before="3mm">
														Richiedendo il pagamento a mezzo SDD Energit le
														restituirà il deposito versato; compili e firmi il
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEI NUOVI ESTREMI SDD</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="3mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari)</fo:block>
														<fo:block space-before="3mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
												
												<xsl:when test="not(../DOCUMENTO[@PRESENZA_DEPOSITO_CAUZIONALE='SI'])">
													<fo:block font-size="10pt" font-weight="bold">
														SCELGA LA COMODITA' DEL PAGAMENTO SDD!
													</fo:block>
													<fo:block space-before="4mm">
														Compili e firmi il
														<fo:inline font-weight="bold">MODULO DI COMUNICAZIONE DEGLI ESTREMI SDD</fo:inline>,
														che trova accanto, e ce lo invii tramite:
													</fo:block>
													<fo:block font-weight="bold" space-before="3mm">
														<fo:block>- FAX GRATUITO 800.19.22.55</fo:block>
														<fo:block>- EMAIL energia@energit.it</fo:block>
														<fo:block>- POSTA (Servizio Clienti Energit</fo:block>
														<fo:block>Via Edward Jenner, 19/21 - 09121 Cagliari)</fo:block>
														<fo:block space-before="3mm">Grazie!</fo:block>
													</fo:block>
												</xsl:when>
												
												<xsl:otherwise>
													<fo:block />
												</xsl:otherwise>
												
											</xsl:choose>
										</fo:table-cell>
										
										
										<fo:table-cell padding-left="3mm" font-size="10pt">
											<fo:block font-weight="bold" text-align="center">
												MODULO DI COMUNICAZIONE ESTREMI SDD – CLIENTE N.
												<xsl:value-of select="CODICE_CLIENTE"/>
											</fo:block>
											<fo:block font-weight="bold">Da</fo:block>
											
											<fo:table table-layout="fixed" width="100%">
												<fo:table-column column-width="4%" />
												<fo:table-column column-width="20%" />
												<fo:table-column column-width="11%" />
												<fo:table-column column-width="2%" />
												<fo:table-column column-width="12%" />
												<fo:table-column column-width="2%" />
												<fo:table-column column-width="18%" />
												<fo:table-column column-width="31%" />
												<fo:table-body>
													<fo:table-cell>
														<fo:block font-weight="bold">Sig.</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>&#160;&#160;in qualità di</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															<fo:external-graphic src="url(svg/unchecked.svg)"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															Cliente privato
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															<fo:external-graphic src="url(svg/unchecked.svg)"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>
															Rappr. Legale Azienda
														</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
												</fo:table-body>
											</fo:table>
											
											
										
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
												<fo:table-column column-width="4%" />
												<fo:table-column column-width="22%" />
												<fo:table-column column-width="5%" />
												<fo:table-column column-width="20%" />
												<fo:table-column column-width="7%" />
												<fo:table-column column-width="17%" />
												<fo:table-column column-width="5%" />
												<fo:table-column column-width="20%" />
												<fo:table-body>
													<fo:table-cell>
														<fo:block>Via</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>Città</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>Tel/Fax</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
													<fo:table-cell>
														<fo:block>email</fo:block>
													</fo:table-cell>
													<fo:table-cell xsl:use-attribute-sets="cell.field">
														<fo:block />
													</fo:table-cell>
												</fo:table-body>
											</fo:table>
											
											
											<fo:block text-align="center" font-weight="bold" space-before="2mm">
												Con la presente il sottoscritto, quale intestatario
												del servizio gas  naturale Energit, comunica
												di seguito gli estremi per il pagamento delle prossime
												fatture Energit con modalità addebito diretto SEPA Direct Debit (SDD):
											</fo:block>
											
											 
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
												<fo:table-column column-width="17%" />
												<fo:table-column column-width="30.5%" />
												<fo:table-column column-width="6%" />
												<fo:table-column column-width="46.5%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>Intestazione del conto</fo:block>
														</fo:table-cell>
														<fo:table-cell xsl:use-attribute-sets="cell.field">
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>&#160;&#160;IBAN&#160;</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<fo:external-graphic src="url(svg/ibanfield.svg)"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
												<fo:table-column column-width="5.5%" />
												<fo:table-column column-width="42%" />
												<fo:table-column column-width="13%" />
												<fo:table-column column-width="39.5%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>Banca</fo:block>
														</fo:table-cell>
														<fo:table-cell xsl:use-attribute-sets="cell.field">
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>&#160;&#160;Agenzia/Filiale&#160;</fo:block>
														</fo:table-cell>
														<fo:table-cell xsl:use-attribute-sets="cell.field">
															<fo:block />
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											
											<fo:block text-align="justify" font-size="6pt" space-before="2mm">
												Il sottoscritto autorizza la Banca a margine a provvedere
												alla estinzione dei documenti di debito (fatture, ricevute,
												bollette, ecc.) emessi da Energ.it S.p.A. sopra citata,
												addebitando il conto sopraindicato e applicando le condizioni
												indicate nel foglio informativo analitico posto a disposizione
												del correntista e le norme previste per il servizio senza necessità,
												per la Banca, di inviare relativa contabile di addebito.
												Dichiara di essere a conoscenza che la Banca assume l'incarico
												dell'estinzione dei citati documenti che Energ.it S.p.A. invierà
												direttamente al debitore, prima della scadenza dell'obbligazione,
												a condizione che, al momento del pagamento, il conto sia in essere
												e assicuri la disponibilità sufficiente e che non sussistano
												ragioni che ne impediscono l'utilizzazione. In caso contrario
												la Banca resterà esonerata da ogni e qualsiasi responsabilità
												inerente al mancato pagamento e il pagamento stesso dovrà essere
												effettuato a Energ.it S.p.A. direttamente a cura del debitore.
												Il sottoscritto prende altresì atto che la Banca si riserva il
												diritto di recedere in ogni momento dal presente accordo. Prende
												pure atto che, ove intenda eccezionalmente sospendere l'estinzione
												di un documento di debito, dovrà dare immediato avviso alla Banca
												in tal senso entro la data di scadenza. Per quanto non espressamente
												richiamato, si applicano le “Norme che regolano i conti correnti di
												corrispondenza dei servizi connessi”: in deroga al terzo comma si
												conviene che il sottoscritto può riservarsi il diritto di chiedere
												alla Banca lo storno dell'addebito entro 5 giorni lavorativi
												dalla scadenza dell'obbligazione.
											</fo:block>
											
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
												<fo:table-column column-width="32%" />
												<fo:table-column column-width="68%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>Cod. Fiscale correntista (persona Fisica)</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																
																<fo:external-graphic src="url(svg/cffield.svg)"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											<fo:table table-layout="fixed" width="100%" space-before="1.5mm" font-weight="bold">
												<fo:table-column column-width="50%" />
												<fo:table-column column-width="50%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:table table-layout="fixed" width="100%" space-before="1.5mm">
																<fo:table-column column-width="35%" />
																<fo:table-column column-width="65%" />
																<fo:table-body>
																	<fo:table-row>
																		<fo:table-cell>
																			<fo:block>Firma del correntista</fo:block>
																		</fo:table-cell>
																		<fo:table-cell xsl:use-attribute-sets="cell.field">
																			<fo:block />
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
															
															<fo:table table-layout="fixed" width="100%" space-before="3mm">
																<fo:table-column column-width="56%" />
																<fo:table-column column-width="44%" />
																<fo:table-body>
																	<fo:table-row>
																		<fo:table-cell>
																			<fo:block>Firma dell'intestatario del contratto</fo:block>
																		</fo:table-cell>
																		<fo:table-cell xsl:use-attribute-sets="cell.field">
																			<fo:block />
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											
											<fo:block font-weight="bold">
												<fo:block>IMPORTANTE</fo:block>
												<fo:block>Allegare copia del documento di identità del correntista</fo:block>
												<fo:block font-size="6pt">(se diverso dall'intestatario del contratto)</fo:block>
											</fo:block>
											
										</fo:table-cell>
									</xsl:otherwise>
								</xsl:choose>
								
								
								
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block-container>
			</fo:static-content>
			
</xsl:if>		
			<fo:flow flow-name="xsl-region-body" xsl:use-attribute-sets="text.font">
			
				<!-- Corpo bollettino -->
				<xsl:for-each select="BOLLETTINO">

					<xsl:variable name="barcode_message">
						<xsl:value-of select="BARCODE" />
					</xsl:variable>
				
					<xsl:variable name="datamatrix_message">
						<xsl:value-of
							select="concat($barcode_message, '                                  ')" />
					</xsl:variable>
				
					<fo:block-container position="absolute" top="0mm"
						left="0mm" width="297mm" height="102mm">
						
						<!-- Ricevuta di Versamento - body -->
						<fo:block-container position="absolute" top="0mm"
							left="0mm" width="132mm" height="102mm">
	
							<!-- Ricevuta di Versamento - header -->
							<fo:block-container position="absolute" top="0mm"
								left="0mm" width="132mm" height="4mm" xsl:use-attribute-sets="font.boll"
								display-align="after" border-top="0.5pt solid black"
								border-bottom="0.5pt solid black" background-color="#CCCCCC">
	
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="6%" />
									<fo:table-column column-width="60%" />
									<fo:table-column column-width="30%" />
									<fo:table-column column-width="4%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block />
											</fo:table-cell>
											<fo:table-cell text-align="left">
												<fo:block>CONTI CORRENTI POSTALI - Ricevuta di Versamento
												</fo:block>
											</fo:table-cell>
											<fo:table-cell text-align="right">
												<fo:block>
													Banco<fo:inline font-weight="bold">Posta</fo:inline>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block />
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - logo -->
							<fo:block-container position="absolute" top="5mm"
								left="8mm" width="27mm" height="12mm"
								display-align="after" text-align="center"
								xsl:use-attribute-sets="layout-debug">
								<fo:block>
									<xsl:if test="$status_piu_uno &lt; 201212"><fo:external-graphic src="url(img/logo_energit_bn.png)" content-width="25mm" /></xsl:if>
									<xsl:if test="$status_piu_uno &gt; 201211"><fo:external-graphic src="url(img/logo_energit_nopayoff_bn.jpg)" content-width="25mm" /></xsl:if>
								</fo:block>
							</fo:block-container>
						
							<!-- Ricevuta di Versamento - euro box -->
							<fo:block-container position="absolute" top="8.3mm" display-align="after"
								left="37mm" width="7mm" height="7mm" xsl:use-attribute-sets="layout-debug">
								<fo:block>
									<fo:external-graphic src="url(img/euro.png)" content-width="7mm" content-height="7mm" />
								</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - "sul C/C n. ..." -->
							<fo:block-container position="absolute" top="8mm"
								left="45mm" width="8mm" height="6mm" xsl:use-attribute-sets="font.boll layout-debug"
								line-height="8pt">
								<fo:block>sul</fo:block>
								<fo:block>C/C n.</fo:block>
							</fo:block-container>
						
							<!-- Ricevuta di Versamento - numero conto corrente -->
							<fo:block-container position="absolute" top="10mm"
								left="54mm" width="32mm" height="6mm" xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>35291152</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - "di Euro..." -->
							<fo:block-container position="absolute" top="11mm"
								left="87mm" width="9mm" height="4mm" xsl:use-attribute-sets="font.boll layout-debug">
								<fo:block>di Euro</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - importo (in euro) -->
							<fo:block-container position="absolute" top="10mm"
								left="98mm" width="29mm" height="6mm" xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block><xsl:value-of select="TOT_COST_EUR" /></fo:block>
							</fo:block-container>
							
							
							<!-- Ricevuta di Versamento - Codice IBAN -->
							<!-- 
							<fo:block-container position="absolute" top="16.5mm"
								left="37mm" width="80mm" height="4mm" display-align="center"
								xsl:use-attribute-sets="font.codeline layout-debug" font-size="9pt">
								<fo:block>CODICE IBAN ***************************</fo:block>
							</fo:block-container>
							 -->
							
							<!-- Ricevuta di Versamento - Codice IBAN -->
							<fo:block-container position="absolute" top="16.5mm"
								left="37mm" width="90mm" height="4mm" display-align="center"
								xsl:use-attribute-sets="font.ocrb layout-debug" font-size="5pt">
								<!-- <fo:block>CODICE IBAN ***************************</fo:block> -->
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="15%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="0.000000004%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell><fo:block>CODICE IBAN</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell><fo:block/></fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - ... intestato a ... -->
							<fo:block-container position="absolute" top="21mm"
								left="8mm" width="119mm" height="3mm" display-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.eseg">INTESTATO A</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - intestazione beneficiario -->
							<fo:block-container position="absolute" top="24.5mm"
								left="8mm" width="119mm" height="7.5mm" display-align="before" xsl:use-attribute-sets="layout-debug">
								<!-- <fo:block xsl:use-attribute-sets="font.boll">INTESTATO A</fo:block> -->
								<fo:block xsl:use-attribute-sets="font.codeline" line-height="10pt">ENERG.IT S.P.A</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - zona cliente top -->
							<fo:block-container position="absolute" top="35mm"
								left="8mm" width="119mm" height="12mm"
								xsl:use-attribute-sets="layout-debug"
								display-align="center">
								<fo:block xsl:use-attribute-sets="font.boll">Eseguito da:</fo:block>
								<fo:block xsl:use-attribute-sets="font.ocrb" font-size="11pt">
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/INTESTATARIO" />
								</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - zona cliente middle -->
							<fo:block-container position="absolute" top="49mm"
								left="8mm" width="66mm" height="34mm" display-align="center"
								text-align="left" margin="1mm"
								xsl:use-attribute-sets="font.codeline layout-debug"
								font-size="8pt">
								<fo:block>CAUSALE: <xsl:value-of select="./ancestor::DOCUMENTO/@NUMERO_DOCUMENTO" /></fo:block>
								<fo:block>DATA DI SCADENZA: <xsl:value-of select="../SCADENZA_DOCUMENTO"/></fo:block>
								<!-- <fo:block /> -->
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - zona cliente bottom -->
							<fo:block-container position="absolute" top="84.5mm"
								left="8mm" width="66mm" height="12mm"
								display-align="center" text-align="center"
								xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block />
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - stringa bollo postale -->
							<fo:block-container position="absolute" top="79.5mm"
								left="77mm" width="55mm" height="3.5mm" display-align="center" text-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.bottom">BOLLO DELL'UFF. POSTALE</fo:block>
								<fo:block xsl:use-attribute-sets="font.bottom2">&#160;&#160;</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Versamento - Data Matrix -->
							<fo:block-container position="absolute" top="84.5mm"
								left="82mm" width="45mm" height="17mm"
								text-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block padding-top="12mm">
									<fo:instream-foreign-object>
										<barcode:barcode message="{$datamatrix_message}">
											<barcode:datamatrix>
												<barcode:quiet-zone enabled="false">0mm</barcode:quiet-zone>
												<barcode:module-width>0.93mm</barcode:module-width>
												<barcode:shape>force-rectangle</barcode:shape>
											</barcode:datamatrix>
										</barcode:barcode>
									</fo:instream-foreign-object>
								</fo:block>
							</fo:block-container>
							
						</fo:block-container>
						
						<!-- Ricevuta di Accredito - body -->
						<fo:block-container position="absolute" top="0mm"
							left="132mm" width="165mm" height="102mm" border-left="0.5pt solid black">
							
							<!-- Ricevuta di Accredito - header -->
							<fo:block-container position="absolute" top="0mm"
								left="0mm" width="165mm" height="4mm" xsl:use-attribute-sets="font.boll"
								display-align="after" border-top="0.5pt solid black"
								border-bottom="0.5pt solid black" border-left="0.5pt solid black"
								background-color="#CCCCCC">
	
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="4.5%" />
									<fo:table-column column-width="61.5%" />
									<fo:table-column column-width="30%" />
									<fo:table-column column-width="4%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block />
											</fo:table-cell>
											<fo:table-cell text-align="left">
												<fo:block>CONTI CORRENTI POSTALI - Ricevuta di Accredito
												</fo:block>
											</fo:table-cell>
											<fo:table-cell text-align="right">
												<fo:block>
													Banco<fo:inline font-weight="bold">Posta</fo:inline>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block />
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
	
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - euro logo -->
							<fo:block-container position="absolute" top="8.3mm" display-align="after"
								left="7.5mm" width="7mm" height="7mm" xsl:use-attribute-sets="layout-debug">
								<fo:block>
									<fo:external-graphic src="url(img/euro.png)" content-width="7mm" content-height="7mm" />
								</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - "sul C/C n. ..." -->
							<fo:block-container position="absolute" top="11mm"
								left="19mm" width="13mm" height="4mm"
								xsl:use-attribute-sets="font.boll layout-debug">
								<fo:block>sul C/C n.</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - numero conto corrente -->
							<fo:block-container position="absolute" top="10mm"
								left="34.5mm" width="32mm" height="6mm"
								xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>35291152</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - "di Euro ..." -->
							<fo:block-container position="absolute" top="11mm"
								left="105mm" width="10mm" height="4mm"
								xsl:use-attribute-sets="font.boll layout-debug">
								<fo:block>di Euro</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - importo (in euro) -->
							<fo:block-container position="absolute" top="10mm"
								left="116mm" width="29mm" height="6mm"
								xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block><xsl:value-of select="TOT_COST_EUR" /></fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - tipo documento -->
							<fo:block-container position="absolute" top="16.5mm"
								left="7.5mm" width="15mm" height="6mm"
								xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>TD 896</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - Codice IBAN -->
							<!-- 
							<fo:block-container position="absolute" top="16.5mm"
								left="34.5mm" width="102mm" height="4mm" display-align="center"
								xsl:use-attribute-sets="font.codeline layout-debug" font-size="9pt">
								<fo:block>CODICE IBAN ***************************</fo:block>
							</fo:block-container>
							 -->
							<!-- Ricevuta di Accredito - Codice IBAN -->
							<fo:block-container position="absolute" top="16.5mm"
								left="34.5mm" width="90mm" height="4mm" display-align="center"
								xsl:use-attribute-sets="font.ocrb layout-debug" font-size="5pt">
								<!-- <fo:block>CODICE IBAN ***************************</fo:block> -->
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="15%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="3.148148148%" />
									<fo:table-column column-width="0.000000004%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell><fo:block>CODICE IBAN</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell padding="0.4mm"><fo:block xsl:use-attribute-sets="block.iban">*</fo:block></fo:table-cell>
											<fo:table-cell><fo:block/></fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - ... intestato a ... -->
							<fo:block-container position="absolute" top="21mm"
								left="7.5mm" width="142mm" height="3mm" display-align="center"
								xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.eseg">INTESTATO A</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - intestazione -->
							<fo:block-container position="absolute" top="24.5mm"
								left="7.5mm" width="142mm" height="7.5mm"
								xsl:use-attribute-sets="layout-debug"
								margin="0mm">
								<!-- <fo:block xsl:use-attribute-sets="font.boll">Intestato a:</fo:block> -->
								<fo:block xsl:use-attribute-sets="font.codeline" line-height="10pt">ENERG.IT S.P.A</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - codice cliente -->
							<fo:block-container position="absolute" top="42mm"
								left="7.5mm" width="47.5mm" height="6mm"
								xsl:use-attribute-sets="font.codeline layout-debug">
								<fo:block>
									<xsl:value-of select="OCR_FATT"/>
								</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - stringa bollo postale -->
							<fo:block-container position="absolute" top="79.5mm"
								left="0mm" width="55mm" height="3.5mm"
								display-align="center" text-align="center"
								xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.bottom">BOLLO DELL'UFF. POSTALE</fo:block>
								<fo:block xsl:use-attribute-sets="font.bottom2">codice cliente</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - zona cliente -->
							<fo:block-container position="absolute" top="35mm"
								left="58.5mm" width="95mm" height="27mm"
								display-align="center" text-align="left" margin="1mm"
								xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.boll">Eseguito da:</fo:block>
								<fo:block xsl:use-attribute-sets="font.eseg.all">
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/INTESTATARIO" />
								</fo:block>
								<fo:block xsl:use-attribute-sets="font.eseg.all">
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/INDIRIZZO" />
								</fo:block>
								<fo:block xsl:use-attribute-sets="font.eseg.all">
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/CAP" />
									&#160;&#160;&#160;&#160;
									<xsl:value-of select="../ANAGRAFICA_DOCUMENTO/CITTA" />
								</fo:block>
							</fo:block-container>
	
							<!-- Ricevuta di Accredito - BAR CODE -->
							<fo:block-container position="absolute" top="63.5mm"
								left="56.5mm" width="97mm" height="16mm"
								text-align="center"
								xsl:use-attribute-sets="layout-debug">
								<fo:block padding-top="9.5mm">
									<fo:instream-foreign-object>
										<barcode:barcode
											message="{$barcode_message}">
											<barcode:code128>
												<!-- Altezza = barra + stringa -->
												<barcode:height>16mm</barcode:height>
												<barcode:module-width>0.298mm</barcode:module-width>
												<!-- <quiet-zone enabled="true">{length:10mw}</quiet-zone> -->
												<barcode:human-readable>
													<barcode:placement>none</barcode:placement>
												</barcode:human-readable>
											</barcode:code128>
										</barcode:barcode>
									</fo:instream-foreign-object>
								</fo:block>
								<fo:block font-family="Arial" font-size="7pt" line-height="2pt">
									<xsl:value-of select="$barcode_message" />
								</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Versamento - "non scrivere nella zona sottostante" -->
							<fo:block-container position="absolute" top="79.5mm"
								left="56.5mm" width="97mm" height="3.5mm" display-align="center"
								xsl:use-attribute-sets="font.eseg layout-debug">
								<fo:block xsl:use-attribute-sets="font.bottom" text-align="center">
									IMPORTANTE: NON SCRIVERE NELLA ZONA SOTTOSTANTE
								</fo:block>
								<fo:table table-layout="fixed" width="100%"
										xsl:use-attribute-sets="font.bottom2">
									<fo:table-column column-width="28%" />
									<fo:table-column column-width="37%" />
									<fo:table-column column-width="33%" />
									<fo:table-column column-width="2%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell><fo:block /></fo:table-cell>
											<fo:table-cell><fo:block>importo in euro</fo:block></fo:table-cell>
											<fo:table-cell><fo:block>numero conto</fo:block></fo:table-cell>
											<fo:table-cell><fo:block>td</fo:block></fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - zona autorizzazione -->
							<fo:block-container reference-orientation="90"
								position="absolute" top="30mm" left="156mm" width="40mm" height="9mm"
								text-align="center" xsl:use-attribute-sets="layout-debug">
								<fo:block xsl:use-attribute-sets="font.auth">Aut. DB/SSIC/E 7271  DEL 12/03/2002</fo:block>
							</fo:block-container>
							
							<!-- Ricevuta di Accredito - zona codeline -->
							<fo:block-container position="absolute" top="83mm"
								left="0mm" width="165mm" height="19mm" border-top="0.5pt solid black"
								display-align="center">
								<fo:block xsl:use-attribute-sets="font.codeline" line-height="0pt" padding-top="2.5mm" padding-bottom="0mm">
									<fo:inline>&#160;&#160;&#160;&lt;<xsl:value-of select="OCR_FATT"/>&gt;&#160;&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="OCR_EUR"/>&gt;&#160;&#160;000035291152&lt;&#160;&#160;896&gt;</fo:inline>
								</fo:block>
							</fo:block-container>
							
						</fo:block-container>
						
					</fo:block-container>
				
				
				</xsl:for-each>

			</fo:flow>

		</fo:page-sequence>


	</xsl:template>



	<xsl:attribute-set name="text.font">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.boll">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="layout-debug">
		<xsl:attribute name="background-color">transparent</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.codeline">
		<xsl:attribute name="font-family">ocrb10</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.ocrb">
		<xsl:attribute name="font-family">ocrb</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.eseg">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-size">6pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.eseg.all">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.bottom">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">5pt</xsl:attribute>
		<xsl:attribute name="line-height">5pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="font.bottom2">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">4pt</xsl:attribute>
		<xsl:attribute name="line-height">4pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="font.auth">
		<xsl:attribute name="font-family">Arial</xsl:attribute>
		<xsl:attribute name="font-size">6pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="cell.field">
		<xsl:attribute name="border-bottom">1pt solid black</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="block.iban">
		<xsl:attribute name="border">0.4pt solid black</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>

</xsl:stylesheet>

