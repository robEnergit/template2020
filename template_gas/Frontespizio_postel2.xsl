<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:barcode="http://barcode4j.krysalis.org/ns" xmlns:math="xalan://java.lang.Math" extension-element-prefixes="math" xmlns:exslt="http://exslt.org/common" >

<xsl:output encoding="UTF-8"/>

<xsl:variable name="template_select"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:variable>

<!-- <xsl:choose>
<xsl:when test="template_select >= 1"> -->


<!--
********************************************************
**            Inclusione templates esterni            **
********************************************************
-->
<xsl:include href="attributi.xsl"/>
<xsl:include href="Template_Energia_Multisito.xsl"/>
<xsl:include href="202_09/Template_Energia_Multisito_012011.xsl"/>
<xsl:include href="Finale.xsl"/>
<xsl:include href="202_09/Finale_012011.xsl"/>
<xsl:include href="bollettino-bis.xsl"/>
<xsl:include href="condizioni.xsl"/>
<xsl:include href="condizioni_c11.xsl"/>
<xsl:include href="scheda_riepilogo.xsl"/>
<xsl:include href="scheda_riepilogo_c11.xsl"/>
<xsl:include href="comunicazioni_condizioni.xsl"/>
<xsl:include href="comunicazioni_condizioni_c11.xsl"/>
<xsl:include href="Autocertificazione.xsl"/>
<xsl:include href="Informativa_Qualita.xsl"/>

<!--
**********************************************************
**				Template LOTTO_FATTURE					**
**********************************************************
-->

<xsl:template match="LOTTO_FATTURE">

<fo:root >

<fo:layout-master-set>
    <!--
    ********************************************************
    **           Definizione pagina fattura               **
    ********************************************************
    -->
    <fo:simple-page-master margin-bottom="10mm" margin-left="11mm" margin-right="11mm" margin-top="5mm" master-name="pm0" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="17mm" margin-top="12mm" overflow="visible" region-name="body"/>
        <fo:region-before extent="12mm" overflow="visible" region-name="header"/>
        <fo:region-after display-align="after" extent="17mm" overflow="visible" region-name="footer"/>
    </fo:simple-page-master>
	
	<fo:simple-page-master margin-bottom="10mm" margin-left="11mm" margin-right="11mm" margin-top="5mm" master-name="pm0first" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="17mm" margin-top="12mm" overflow="visible" region-name="body"/>
        <fo:region-before extent="12mm" overflow="visible" region-name="headernew"/>
        <fo:region-after display-align="after" extent="17mm" overflow="visible" region-name="footer"/>
    </fo:simple-page-master>
	
    <fo:simple-page-master margin-bottom="0mm" margin-left="0mm" margin-right="0mm" margin-top="0mm" master-name="pm0-blank" page-height="297mm" page-width="210mm">
        <fo:region-body margin-bottom="0mm" margin-top="0mm" overflow="visible" region-name="body"/>
    </fo:simple-page-master>
    
    <!--
    ********************************************************
    **          Definizione pagina bollettino             **
    ********************************************************
    -->
    <fo:simple-page-master master-name="pm1"
		page-height="210mm" page-width="297mm" margin="0mm">
		<fo:region-body margin-top="108mm" />
		<fo:region-before extent="102mm" overflow="hidden" />
	</fo:simple-page-master>
    
    <!--
    *****************************************************************************
    **    Definizione pagine allegati condizioni contrattuali - comparativa    **
    *****************************************************************************
    -->
	<fo:simple-page-master margin-bottom="5mm" margin-left="10mm" margin-right="10mm" margin-top="10mm" master-name="pm0_condizioni" page-height="297mm" page-width="210mm">
		<fo:region-body margin-bottom="0mm" margin-top="5mm" overflow="visible" region-name="body" />
		<fo:region-before extent="10mm" overflow="visible" region-name="header_condizioni" />
		<fo:region-after display-align="after" extent="10mm" overflow="visible" region-name="footer_condizioni" />
	</fo:simple-page-master>

	<fo:simple-page-master margin-bottom="10mm" margin-left="10mm" margin-right="10mm" margin-top="10mm" master-name="pm0_scheda_riepilogo" page-height="297mm" page-width="210mm">
		<fo:region-body margin-bottom="0mm" margin-top="0mm" overflow="visible" region-name="body" />
		<fo:region-after display-align="after" extent="10mm" overflow="visible" region-name="footer_riepilogo" />
	</fo:simple-page-master>

	<fo:simple-page-master margin-top="5mm" margin-bottom="10mm" master-name="pm0_comunicazione_condizioni" page-height="297mm" page-width="210mm">
		<fo:region-body margin-top="3cm" margin-bottom="25mm" margin-left="29mm" margin-right="29mm" overflow="visible" region-name="body" />
		<fo:region-before extent="3cm" overflow="visible" region-name="header" />
		<fo:region-after display-align="after" extent="25mm" overflow="visible" region-name="footer" />
	</fo:simple-page-master>
    
	<fo:page-sequence-master master-name="document">
      <fo:repeatable-page-master-alternatives>
        <fo:conditional-page-master-reference
          master-reference="pm0" page-position="rest" blank-or-not-blank="not-blank"/>
        <fo:conditional-page-master-reference
          master-reference="pm0first" page-position="first"  blank-or-not-blank="not-blank"/>
		<fo:conditional-page-master-reference
          master-reference="pm0-blank" page-position="any" blank-or-not-blank="blank"/>
      </fo:repeatable-page-master-alternatives>
    </fo:page-sequence-master>
	
</fo:layout-master-set>

<xsl:apply-templates select="DOCUMENTO"/>

</fo:root>
</xsl:template>





<!--
**********************************************************
**					Template DOCUMENTO					**
**********************************************************
-->
<xsl:template match="DOCUMENTO">

<xsl:variable name="bordi">NO</xsl:variable><!--rectangle_table-->
<xsl:variable name="rectangle_frontespizio">svg/rectangle_frontespizio_20</xsl:variable>
<xsl:variable name="color">black</xsl:variable><!--#f08c02-->
<xsl:variable name="color_riquadro_scadenza">black</xsl:variable><!--black-->
<xsl:variable name="color_titolo_dettaglio">black</xsl:variable><!--black-->
<xsl:variable name="sfondo_titoli">#DDDDDD</xsl:variable><!--#DDDDDD-->
<xsl:variable name="color-sezioni">black</xsl:variable><!--#004a76-->
<xsl:variable name="color-sottosezioni">black</xsl:variable><!--#004a76-->
<xsl:variable name="color-default">#000000</xsl:variable>
<xsl:variable name="svg-sezioni">'url(svg/rectangle_titolo_sezioni_short.svg)'</xsl:variable>
<xsl:variable name="svg-sottosezioni">'url(svg/rectangle_titolo_sezioni_short_blank.svg)'</xsl:variable>
<xsl:variable name="svg-altre-sezioni">'url(svg/rectangle_titolo_sezioni_short.svg)'</xsl:variable>
<xsl:variable name="svg-dettaglio">'url(svg/rectangle_dettaglio.svg)'</xsl:variable>
<xsl:variable name="autocertificazione">NO</xsl:variable>
<xsl:variable name="qualita">SI</xsl:variable>

<xsl:variable name="status_piu_uno"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2))"/></xsl:variable>
<xsl:variable name="data_doc_number"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2),substring(DATA_DOCUMENTO,1,2))"/></xsl:variable>

 
<xsl:variable name="consumi_fatturati">
	<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
		<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::CONSUMI_FATTURATI"/>
	</xsl:if> -->
	<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
		<!-- <xsl:value-of select="CONTRATTO_GAS/CONSUMI/@CONSUMO_TOTALE_FATTURATO"/> -->
		<xsl:value-of select="translate(./child::CONTRATTO_GAS/CONSUMI/@CONSUMO_TOTALE_FATTURATO,'.','')"/>
	</xsl:if>
	<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
		0
	</xsl:if>
</xsl:variable>
 

<xsl:variable name="imponibile_no_altrepartite">
	<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_DI_GAS_NATURALE_E_IMPOSTE='0,00'">0</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_DI_GAS_NATURALE_E_IMPOSTE='0,00')">
			<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_FORNITURA_DI_GAS_NATURALE_E_IMPOSTE,'.',''),',','')"/>
		</xsl:if>
	</xsl:if>  -->
	<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
		<xsl:if test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_FORNITURA_DI_GAS_NATURALE_E_IMPOSTE='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_FORNITURA_DI_GAS_NATURALE_E_IMPOSTE='0,00')">
			<xsl:value-of select="translate(translate(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_FORNITURA_DI_GAS_NATURALE_E_IMPOSTE,'.',''),',','')"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
			0
		</xsl:if>
	</xsl:if>
</xsl:variable>






<!-- xxxxxxxxxxxxxxxxxx -->

<xsl:variable name="imponibile_sv">
	<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
		<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA='0,00'">0</xsl:if>
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA='0,00')">
			<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA,'.',''),',','')"/>
		</xsl:if>
	</xsl:if>  -->
	<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
		<xsl:if test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA='0,00')">
			<xsl:value-of select="translate(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA,'.','')"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
			0
		</xsl:if>
	</xsl:if>
</xsl:variable>


	<xsl:variable name="sv">
		<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA,'.',''),',','')"/>
			</xsl:if>
		</xsl:if> -->
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
				0
			</xsl:if>
		</xsl:if>
		<!-- <xsl:value-of select="./child::CHARTS/child::ROWSET[SEZ/text()='Servizi di vendita']/child::P14"/> -->
	</xsl:variable>
	
	<xsl:variable name="sr">
		<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS,'.',''),',','')"/>
			</xsl:if>
		</xsl:if> -->
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
				0
			</xsl:if>
		</xsl:if>
		<!-- <xsl:value-of select="./child::CHARTS/child::ROWSET[SEZ/text()='Servizi di rete']/child::P14"/> -->
	</xsl:variable>
	
	<xsl:variable name="imp">
		<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IMPOSTE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IMPOSTE='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if> -->
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IMPOSTE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IMPOSTE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
				0
			</xsl:if>
		</xsl:if>
		<!-- <xsl:value-of select="./child::CHARTS/child::ROWSET[SEZ/text()='Imposte']/child::P14"/> -->
	</xsl:variable>
	
	<xsl:variable name="iva_fornitura">
		<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA,'.',''),',','')"/>
			</xsl:if>
		</xsl:if> -->
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
				0
			</xsl:if>
		</xsl:if>
		<!-- <xsl:value-of select="./child::CHARTS/child::ROWSET[SEZ/text()='IVA']/child::P14"/> -->
	</xsl:variable>
	
	<xsl:variable name="iva_odiv">
		<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA_SU_ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if> 
		</xsl:if> -->
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA_SU_ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA_SU_ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA_SU_ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
				0
			</xsl:if>
		</xsl:if>
		<!-- <xsl:value-of select="./child::CHARTS/child::ROWSET[SEZ/text()='IVA']/child::P14"/> -->
	</xsl:variable>
	
	<xsl:variable name="iva_dc">
		<!-- <xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE">
			<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IVA='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IVA='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IVA,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>  -->
		<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE)">
			0
		</xsl:if>
		<!-- <xsl:value-of select="./child::CHARTS/child::ROWSET[SEZ/text()='IVA']/child::P14"/> -->
	</xsl:variable>
	
	<xsl:variable name="iva">
		<xsl:value-of select="$iva_fornitura+$iva_odiv+$iva_dc"/>
	</xsl:variable>
	
	<xsl:variable name="odiv_fornitura">
		<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>  -->
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::ONERI_DIVERSI='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::ONERI_DIVERSI='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::ONERI_DIVERSI,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
				0
			</xsl:if>
		</xsl:if>
		<!-- <xsl:value-of select="./child::CHARTS/child::ROWSET[SEZ/text()='Oneri diversi']/child::P14"/> -->
	</xsl:variable>
	
	<xsl:variable name="imponibile_dc">
		<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE">
			<xsl:if test="./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_DEPOSITO_CAUZIONALE/child::TOTALE_IMPONIBILE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_DEPOSITO_CAUZIONALE)">
			0
		</xsl:if>
		<!-- <xsl:value-of select="./child::CHARTS/child::ROWSET[SEZ/text()='IVA']/child::P14"/> -->
	</xsl:variable>
	
	<xsl:variable name="odiv">
		<xsl:value-of select="$odiv_fornitura+$imponibile_dc"/>
	</xsl:variable>
	
	<xsl:variable name="bs">
		<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
			<xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00')">
				<xsl:value-of select="translate(translate(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE,'.',''),',','')"/>
			</xsl:if>
		</xsl:if>  -->
		<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
			<xsl:if test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE='0,00'">0</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE='0,00')">
				<xsl:value-of select="translate(translate(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE,'.',''),',','')"/>
			</xsl:if>
			<xsl:if test="not(./child::CONTRATTO_GAS/child::SINTESI_GAS)">
				0
			</xsl:if>
		</xsl:if>
		<!-- <xsl:value-of select="./child::CHARTS/child::ROWSET[SEZ/text()='Bonus sociale']/child::P14"/> -->
	</xsl:variable>
		
	
	<xsl:variable name="importo_no_virgola">
		<xsl:value-of select="translate(translate(./child::IMPORTO_DOCUMENTO,'.',''),',','')"/>
	</xsl:variable>
	
	<xsl:variable name="doc">
		<xsl:value-of select="math:abs($sv) + math:abs($sr) + math:abs($imp) + math:abs($iva) + math:abs($odiv) + math:abs($bs)"/>
		<!-- <xsl:value-of select="translate(translate(./child::IMPORTO_DOCUMENTO,'.',''),',','')"/> -->
	</xsl:variable>
	
<!--
 	<xsl:variable name="costo_medio">
		<xsl:if test="$imponibile_no_altrepartite &gt; 0 and $consumi_fatturati &gt; 0">
			<xsl:value-of select="round($imponibile_no_altrepartite div $consumi_fatturati) div 100"/>
		</xsl:if>
	</xsl:variable>  

	-->

<!-- 	
	<xsl:variable name="costo_medio">
		<xsl:if test="translate( translate( $imponibile_sv,'.','' ), ',' ,'.') &gt; 0 and translate( translate( $consumi_fatturati,'.','' ), ',' ,'.') &gt; 0"> 
			<xsl:value-of select="format-number( translate( translate( $imponibile_no_altrepartite,'.','' ), ',' ,'.') div translate( translate( $consumi_fatturati,'.','' ), ',' ,'.'), '##.####' )"/>
		</xsl:if>
	</xsl:variable>  
--> 

<!-- Aggiunte 28/06 -->
	<xsl:variable name="imposte">
		<xsl:if test="./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='IMPOSTE']/PRECEDENTEMENTE_FATTURATO='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='IMPOSTE']/PRECEDENTEMENTE_FATTURATO='0,00')">
			<xsl:value-of select="./child::CONTRATTO_GAS/SEZIONE[@TIPOLOGIA='IMPOSTE']/PRECEDENTEMENTE_FATTURATO"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='IMPOSTE'])">
			0
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="quota_fissa_reti">
		<xsl:if test="./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_FISSA_RETI']/PRECEDENTEMENTE_FATTURATO='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_FISSA_RETI']/PRECEDENTEMENTE_FATTURATO='0,00')">
			<xsl:value-of select="./child::CONTRATTO_GAS/SEZIONE[@TIPOLOGIA='QUOTA_FISSA_RETI']/PRECEDENTEMENTE_FATTURATO"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_FISSA_RETI'])">
			0
		</xsl:if>
	</xsl:variable>
	
	<xsl:variable name="quota_variabile_reti">
		<xsl:if test="./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_VARIABILE_RETI']/PRECEDENTEMENTE_FATTURATO='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_VARIABILE_RETI']/PRECEDENTEMENTE_FATTURATO='0,00')">
			<xsl:value-of select="./child::CONTRATTO_GAS/SEZIONE[@TIPOLOGIA='QUOTA_VARIABILE_RETI']/PRECEDENTEMENTE_FATTURATO"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_VARIABILE_RETI'])">
			0
		</xsl:if>
	</xsl:variable>
	

	
	<xsl:variable name="quota_fissa_vendita">
		<xsl:if test="./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_FISSA_VENDITA']/PRECEDENTEMENTE_FATTURATO='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_FISSA_VENDITA']/PRECEDENTEMENTE_FATTURATO='0,00')">
			<xsl:value-of select="./child::CONTRATTO_GAS/SEZIONE[@TIPOLOGIA='QUOTA_FISSA_VENDITA']/PRECEDENTEMENTE_FATTURATO"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_FISSA_VENDITA'])">
			0
		</xsl:if>
	</xsl:variable>
	
	<xsl:variable name="quota_variabile_vendita">
		<xsl:if test="./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_VARIABILE_VENDITA']/PRECEDENTEMENTE_FATTURATO='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_VARIABILE_VENDITA']/PRECEDENTEMENTE_FATTURATO='0,00')">
			<xsl:value-of select="./child::CONTRATTO_GAS/SEZIONE[@TIPOLOGIA='QUOTA_VARIABILE_VENDITA']/PRECEDENTEMENTE_FATTURATO"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='QUOTA_VARIABILE_VENDITA'])">
			0
		</xsl:if>
	</xsl:variable>

	
	<xsl:variable name="costo_medio_sv">
			<xsl:value-of select="translate( format-number( (number( translate($imponibile_sv,',','.')) - (  number(translate($quota_variabile_vendita,',','.')) + number(translate($quota_fissa_vendita,',','.'))) ) div number(translate($consumi_fatturati,',','.') ) ,'##.####' ), '.',',')"/>		
	</xsl:variable>

	<xsl:variable name="totale_da_pagare">
		<xsl:if test="./child::CONTRATTO_GAS/TOTALE='0,00'">0</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/TOTALE='0,00')">
			<xsl:value-of select="./child::CONTRATTO_GAS/TOTALE"/>
		</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/TOTALE)">
			0
		</xsl:if>
	</xsl:variable>

	<xsl:variable name="costo_medio">
			<xsl:value-of select="translate( format-number( (number( translate($totale_da_pagare,',','.')) - ( number(translate($quota_fissa_reti,',','.')) + number(translate($quota_variabile_reti,',','.')) + number(translate($imposte,',','.')) + number(translate($quota_variabile_vendita,',','.')) + number(translate($quota_fissa_vendita,',','.')) + 0 ) ) div number(translate($consumi_fatturati,',','.')),'##.####'),'.',',')"/>		
	</xsl:variable>
	

	<xsl:variable name="disagio_economico">
		<xsl:if test="./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='CT_FISSE']/child::PERIODO_RIFERIMENTO/child::PRODOTTO/child::DESCRIZIONE='Bonus sociale economico'">1</xsl:if>
		<xsl:if test="not(./child::CONTRATTO_GAS/child::SEZIONE[@TIPOLOGIA='CT_FISSE']/child::PERIODO_RIFERIMENTO/child::PRODOTTO/child::DESCRIZIONE='Bonus sociale economico')">0</xsl:if>
	</xsl:variable>





<fo:page-sequence initial-page-number="1" master-reference="document" orphans="1" white-space-collapse="true" widows="1" id="F">
    <!--
    ********************************************************
    **                     Header                         **
    ********************************************************
    -->
	<fo:static-content flow-name="headernew">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
            <fo:table-column column-width="proportional-column-width(100)"/>
            <fo:table-body end-indent="0pt" start-indent="0pt">
                <fo:table-row>
                    <fo:table-cell display-align="center" xsl:use-attribute-sets="blk.header">
                    	<fo:block margin-left="150mm" margin-top="5mm">
							<fo:external-graphic src="url(img/logoGasNaturalexFattura.svg)" content-width="37mm"  />
						</fo:block>
                        <fo:block text-align="end">
							<xsl:if test="$status_piu_uno &lt; 201307">
								<xsl:if test="@SPOT='NO' or not(@SPOT)">
									<fo:inline font-family="universbold" font-size="11pt">Codice Cliente <xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
								</xsl:if>
							</xsl:if>
                        </fo:block>
                        <fo:block text-align="end">   
							<xsl:if test="$status_piu_uno &lt; 201307">
								<xsl:if test="@SPOT='NO' or not(@SPOT)">
									<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Energit</fo:inline>
								</xsl:if>
							</xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </fo:static-content>
	
    <fo:static-content flow-name="header">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
            <fo:table-column column-width="proportional-column-width(100)"/>
            <fo:table-body end-indent="0pt" start-indent="0pt">
                <fo:table-row>
                    <fo:table-cell display-align="center" xsl:use-attribute-sets="blk.header">
						<fo:block text-align="end">
							<xsl:if test="@SPOT='NO' or not(@SPOT)">
								<fo:inline font-family="universbold" font-size="11pt">Codice Cliente <xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
							</xsl:if>
                        </fo:block>
                        <fo:block text-align="end">   
							<xsl:if test="@SPOT='NO' or not(@SPOT)">
								<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Energit</fo:inline>
							</xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </fo:static-content>

    <!--
    ********************************************************
    **                     Footer                         **
    ********************************************************
    -->
    <fo:static-content flow-name="footer">
        <fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
			<fo:table-column column-width="proportional-column-width(85)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell display-align="center">
						<fo:block xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer2">
								<xsl:choose>
									<xsl:when test="@TIPO_DOCUMENTO='FATTURA'">Fattura: </xsl:when>
									<xsl:otherwise>Nota di credito: </xsl:otherwise>
								</xsl:choose> 
								<xsl:value-of select="@NUMERO_DOCUMENTO"/>  del  <xsl:value-of select="./child::DATA_DOCUMENTO" />
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell display-align="center">
						<fo:block text-align="end" xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer2">
								<fo:inline xsl:use-attribute-sets="font_footer2">Pagina </fo:inline>
								<fo:inline text-align="end" xsl:use-attribute-sets="font_footer2"><fo:page-number/>/<fo:page-number-citation ref-id="{generate-id(.)}"/></fo:inline>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row height="0">
					<fo:table-cell number-columns-spanned="2" display-align="center" border-bottom="0.5 dashed thick black">
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row height="2mm">
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" display-align="center">
						<fo:block text-align="center" xsl:use-attribute-sets="blocco_footer">
							<fo:inline xsl:use-attribute-sets="font_footer">Energ.it S.p.A.  <xsl:if test="$status_piu_uno &gt; 201209 and $data_doc_number &lt; 20130127">in liquidazione</xsl:if> - <xsl:if test="$status_piu_uno &gt; 201606">Sede Operativa:</xsl:if> Via Edward Jenner, 19/21 - 09121 Cagliari - Servizio Clienti 800.19.22.22 - Fax 800.19.22.55 - P.IVA 02605060926</fo:inline>
						</fo:block>
						<fo:block text-align="center" xsl:use-attribute-sets="blocco_footer">
							<xsl:if test="$status_piu_uno &lt; 201212"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Societa' per Azioni con Socio Unico. Direzione e Coordinamento di Alpiq Italia S.r.l.</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201211 and $status_piu_uno &lt; 201509"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società con Socio Unico soggetta ad attività di direzione e coordinamento di Onda s.r.l.</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201508 and $status_piu_uno &lt; 201511"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società soggetta a direzione e coordinamento di Enertronica S.p.A.</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201510 and $status_piu_uno &lt; 201607"><fo:inline xsl:use-attribute-sets="font_footer">Iscrizione CCIAA di Cagliari n. 02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società per azioni con Socio Unico</fo:inline></xsl:if>
							<xsl:if test="$status_piu_uno &gt; 201606"><fo:inline xsl:use-attribute-sets="font_footer">Sede Legale: Via Savoia, 38 - 96100 Siracusa - Iscrizione CCIAA di Cagliari n.02605060926 del 12/08/00 - Cap. Soc. euro 1.000.000 i.v. - Società per azioni con Socio Unico</fo:inline></xsl:if>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
        </fo:table>
    </fo:static-content>

    <!--
    ********************************************************
    **                  Inizio body                       **
    ********************************************************
    -->
    <fo:flow flow-name="body">
		<xsl:attribute name="color"><xsl:value-of select="$color-default"/></xsl:attribute>
		
		<!--
		********************************************************
		**               Logo E Data Matrix                   **
		********************************************************
		-->
		<fo:block-container position="absolute"
							top="2mm"
							left="0mm"
							width="90mm"
							height="22mm">
			<fo:block>
				<fo:table table-layout="fixed" width="100%">
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding-left="2mm">
								<xsl:choose>
									<xsl:when test="$color='black'">
										<!-- <xsl:attribute name="padding-top">13mm</xsl:attribute> -->
										<fo:block>
											<xsl:if test="$status_piu_uno &lt; 201212"><fo:external-graphic src="url(img/logo_energit_bn.svg)" content-width="37mm" /></xsl:if>
											<xsl:if test="$status_piu_uno &gt; 201211"><fo:external-graphic src="url(img/logo_energit_nopayoff_bn.svg)" content-width="37mm" /></xsl:if>
										</fo:block>
										<fo:block start-indent="2mm" font-family="Arial" font-size="5pt"
												  color="black">Via Edward Jenner, 19/21 - 09121 Cagliari
										</fo:block>
									</xsl:when>
									<xsl:otherwise>
										<!-- <xsl:attribute name="padding-top">13mm</xsl:attribute> -->
										<fo:block>
											<xsl:if test="$status_piu_uno &lt; 201212"><fo:external-graphic src="url(img/logo-energit.svg)" content-width="35mm" /></xsl:if>
											<xsl:if test="$status_piu_uno &gt; 201211"><fo:external-graphic src="url(img/logo_energit_nopayoff.svg)" content-width="37mm" /></xsl:if>
										</fo:block>
										<fo:block start-indent="2mm" font-family="Arial" font-size="5pt"
												  color="black">Via Edward Jenner, 19/21 - 09121 Cagliari
										</fo:block>
									</xsl:otherwise>
								</xsl:choose>
							</fo:table-cell>
							
							<xsl:variable name="barcode_message_header">
								<xsl:value-of select="concat(
								'F_P', ' ', 
								DATA_DOCUMENTO, ' ',
								@NUMERO_DOCUMENTO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CODICE_CLIENTE, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/INDIRIZZO, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CAP, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/CITTA, ' ',
								INDIRIZZO_SPEDIZIONE_DOCUMENTO/PROVINCIA)"/>
							</xsl:variable>
							
							<fo:table-cell text-align="right" display-align="after"
									padding-right="17mm">
								<fo:block>
									<fo:instream-foreign-object content-height="12mm" content-width="25mm">
										<barcode:barcode message="{$barcode_message_header}">
											<barcode:datamatrix>
												<barcode:quiet-zone enabled="false">0mm</barcode:quiet-zone>
												<barcode:module-width>0.7mm</barcode:module-width>
												<xsl:if test="string-length($barcode_message_header) &lt; 48">
													<barcode:shape>force-rectangle</barcode:shape>
												</xsl:if>
											</barcode:datamatrix>
										</barcode:barcode>
									</fo:instream-foreign-object>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:block-container>
		
		
		<xsl:if test="$status_piu_uno &gt; 201306 and @SPOT='NO'">
			<fo:block-container position="absolute"
								top="4mm"
								left="109.3mm"
								width="79mm"
								height="16mm"
								background-image="./svg/bordo_codice_cliente.svg"
								background-repeat="no-repeat"
								display-align="center"
								text-align="center"
								font-family="universcbold"
								font-size="14pt">
				<fo:block>
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						Codice Cliente <fo:inline font-family="universcbold" font-size="18pt"><xsl:value-of select="./child::CODICE_CLIENTE" /> - <xsl:value-of select="./child::INVOICE_NO" /></fo:inline>
					</xsl:if>
				</fo:block>
				<fo:block>   
					<xsl:if test="@SPOT='NO' or not(@SPOT)">
						<fo:inline font-family="univers" font-size="9pt">Da utilizzare in tutte le comunicazioni con Energit</fo:inline>
					</xsl:if>
				</fo:block>
			</fo:block-container>
		</xsl:if>
		
	
		<!--
		*********************************************************
		**  Dati anagrafici e indirizzo di spedizione fattura  **
		*********************************************************
		-->
		<fo:block-container position="absolute"
							top="45.17mm"
							left="93mm"
							width="97mm"
							height="26mm">
			<xsl:call-template name="SPEDIZIONE_FATTURA" />
		</fo:block-container>
		
		
		
		
		
		<!-- BOLLETTA 2.0 -->
		
		<fo:block-container position="absolute"
							top="27mm"
							left="0mm"
							width="74.5mm"
							height="55mm"
							font-family="universc"
							font-size="7pt"
							background-color="#FFFFFF">
		
		<!--
		*********************************************
		**     Riquadro con bordi arrotondati      **
		*********************************************
		-->
		<fo:block font-family="universcbold" font-size="10pt">RICHIESTA INFORMAZIONI O RECLAMI</fo:block>
		
		<fo:table space-before="1mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(28.5)"/>
			<fo:table-column column-width="proportional-column-width(61.5)"/>
			<fo:table-body>
				<fo:table-row border-bottom="0.5 solid black" border-top="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/telefono.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							SERVIZIO CLIENTI
						</fo:block>
						<fo:block>
							DA RETE FISSA
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							800.19.22.22
						</fo:block>
						<fo:block>
							gratuito (lun-ven 8.30 - 17.30)
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/cellulare.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							SERVIZIO CLIENTI
						</fo:block>
						<fo:block>
							DA CELLULARE
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							070 7521 422
						</fo:block>
						<fo:block font-size="6pt">
							I costi della chiamata dipendono dal proprio operatore telefonico
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/fax.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							FAX GRATUITO
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							800.19.22.55
						</fo:block>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/web.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							SITO WEB
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							www.energit.it
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/chiocciola.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							POSTA ELETTRONICA
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							energia@energit.it
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/lettera.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							INFORMAZIONI E RECLAMI SCRITTI
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							Energit S.p.A.
						</fo:block>
						<fo:block>
							Via E. Jenner, 19/21 - 09121 Cagliari
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row border-bottom="0.5 solid black">
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="5mm" src="img/prontointervento-gas.svg"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							PRONTO INTERVENTO
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<xsl:value-of select="./child::CONTRATTO_GAS/child::DISTRIBUTORE"/>
						</fo:block>
						<fo:block>
							<xsl:value-of select="./child::CONTRATTO_GAS/child::CONTATTO_DISTRIBUTORE"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
				
		</fo:table>
		
		</fo:block-container>
		
		
		
		<fo:block-container position="absolute"
							top="83mm"
							left="0mm"
							width="190mm"
							height="170mm"
							font-family="universc"
							font-size="7pt"
							background-color="#FFFFFF">
		
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="75mm"/>
			<fo:table-column column-width="5mm"/>
			<fo:table-column column-width="110mm"/>
			<fo:table-body>
				<fo:table-row height="153mm">
					<fo:table-cell padding-left="2mm" padding-right="2mm" padding-top="7mm" font-family="universc" font-size="7pt">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(<xsl:value-of select="$rectangle_frontespizio"/>.svg)</xsl:attribute>
							<xsl:call-template name="BOX_SX">
								<xsl:with-param name="color" select="$color_riquadro_scadenza"/>
								<xsl:with-param name="bs" select="$bs"/>
								<xsl:with-param name="disagio_economico" select="$disagio_economico"/>
							</xsl:call-template>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block/>
					</fo:table-cell>
					
					<fo:table-cell padding-top="7mm" font-family="universc" font-size="7pt">
					
						<fo:block-container position="absolute"
							top="0mm"
							left="0mm"
							width="155mm"
							height="12mm">
							<fo:block>
								<fo:instream-foreign-object>
									<svg version="1.1"
									  xmlns="http://www.w3.org/2000/svg">
									  <xsl:attribute name="width"><xsl:value-of select="308.5"/></xsl:attribute>
									  <xsl:attribute name="height"><xsl:value-of select="20"/></xsl:attribute>
									  <xsl:attribute name="viewBox"><xsl:value-of select="'0 0 300 20'"/></xsl:attribute>
										<line x1="1" y1="7" x2="308.5" y2="7" stroke="black" stroke-width="1" />
										<!-- <rect x="199" y="1mm" width="100" height="14" rx="5" ry="5" fill="white" stroke="black" stroke-width="1"/> -->
									</svg>
								</fo:instream-foreign-object>
							</fo:block>
						</fo:block-container>
					
						<fo:block font-family="universcbold" font-size="14pt">
							SERVIZIO DI FORNITURA DI GAS NATURALE
						</fo:block>
						<fo:block font-family="universcbold" font-size="12pt">
							Fattura n. <xsl:value-of select="@NUMERO_DOCUMENTO"/>  del  <xsl:value-of select="./child::DATA_DOCUMENTO" />
						</fo:block>
						<fo:block font-size="12pt">Periodo di fatturazione: <xsl:value-of select="./child::PERIODO_DOCUMENTO" /></fo:block>
						
						<xsl:choose>
							<xsl:when test="@TIPO_DOCUMENTO='FATTURA' and not(@COMPETENZA) and not(./child::SCADENZA_DOCUMENTO='******') and not(./child::IMPORTO_DOCUMENTO='0,00') and not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'La fattura risulta pagata')">                                    
								<fo:block space-before="2mm" font-size="12pt">
									IL TOTALE DA PAGARE ENTRO IL <xsl:value-of select="./child::SCADENZA_DOCUMENTO" /> È <fo:inline font-family="universcbold"><xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> EURO</fo:inline>
								</fo:block>
							</xsl:when>
							<xsl:otherwise>
								<fo:block space-before="2mm" font-size="12pt">IL TOTALE DOCUMENTO È <fo:inline font-family="universcbold"><xsl:value-of select="./child::IMPORTO_DOCUMENTO" /></fo:inline> EURO</fo:block>
							</xsl:otherwise>
						</xsl:choose>
						
						<!-- <xsl:if test="./child::CONTRATTO_GAS"> -->
						
						<xsl:if test="./child::CONTRATTO_GAS">
							<fo:block font-size="10pt">
								(per <xsl:value-of select="$consumi_fatturati" /> Smc fatturati su <xsl:value-of select="count(./child::CONTRATTO_GAS)" /> PDR)
							</fo:block>
						</xsl:if>
						<xsl:if test="not(./child::CONTRATTO_GAS)">
							<fo:block font-size="10pt">
								&#160;
							</fo:block>
						</xsl:if>
							
						<xsl:call-template name="DETTAGLIO_IMPORTI">
							<xsl:with-param name="bordi" select="$bordi"/>
							<xsl:with-param name="consumi_fatturati" select="$consumi_fatturati"/>
							<xsl:with-param name="imponibile_no_altrepartite" select="$imponibile_no_altrepartite"/>
							<xsl:with-param name="imponibile_sv" select="$imponibile_sv"/>
							<xsl:with-param name="sv" select="$sv"/>
							<xsl:with-param name="sr" select="$sr"/>
							<xsl:with-param name="imp" select="$imp"/>
							<xsl:with-param name="iva" select="$iva"/>
							<xsl:with-param name="odiv" select="$odiv"/>
							<xsl:with-param name="bs" select="$bs"/>
							<xsl:with-param name="doc" select="$doc"/>
							<xsl:with-param name="costo_medio" select="$costo_medio"/>
							<xsl:with-param name="costo_medio_sv" select="$costo_medio_sv"/>
						</xsl:call-template>
						
						<fo:block-container position="absolute"
							left="50mm"
							width="65mm"
							height="54mm">
							<xsl:choose>
								<xsl:when test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE='0,00') and (./child::CONTRATTO_GAS)">
									<!-- <xsl:attribute name="top">34mm</xsl:attribute> -->
									<xsl:attribute name="top">40mm</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<!-- <xsl:attribute name="top">30mm</xsl:attribute> -->
									<xsl:attribute name="top">35mm</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<!--  inserire qua controllo per la data per mostrare grafico nelle fatture successive a marzo e non mostrarlo nelle fatture precedenti -->
							<fo:block text-align="center" display-align="center">
								<fo:instream-foreign-object>
								 <!-- <svg:svg>
									<xsl:attribute name="width"><xsl:value-of select="180"/></xsl:attribute>
									<xsl:attribute name="height"><xsl:value-of select="154"/></xsl:attribute>
									<xsl:attribute name="viewBox"><xsl:value-of select="'0 0 180 154'"/></xsl:attribute> -->
									<svg  
								 	   xmlns="http://www.w3.org/2000/svg"
								 	   xmlns:xlink="http://www.w3.org/1999/xlink" 
								       viewBox="-1 -1 2 2"
								       style="transform: rotate(-0.25turn)"
								       width="154" 
								       height="154" 
								       >

									<defs>
									    <pattern id="prova" patternUnits="userSpaceOnUse" width="0.4" height="0.4"> 
									      <image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMC42MCIgaGVpZ2h0PSIwLjMwIj4NCjxkZWZzPg0KPHJlY3QgaWQ9InIiIHdpZHRoPSIwLjMwIiBoZWlnaHQ9IjAuMTUiIGZpbGw9IiNiYjA4NWYiIHN0cm9rZS13aWR0aD0iMC4wMjUiIHN0cm9rZT0iIzdhMDU0ZCI+PC9yZWN0Pg0KPGcgaWQ9InAiPg0KPHVzZSB4bGluazpocmVmPSIjciI+PC91c2U+DQo8dXNlIHk9IjAuMTUiIHhsaW5rOmhyZWY9IiNyIj48L3VzZT4NCjx1c2UgeT0iMC4zMCIgeGxpbms6aHJlZj0iI3IiPjwvdXNlPg0KPHVzZSB5PSIwLjQ1IiB4bGluazpocmVmPSIjciI+PC91c2U+DQo8L2c+DQo8L2RlZnM+DQo8dXNlIHhsaW5rOmhyZWY9IiNwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIC0wLjI1KSBza2V3WSgwLjQwKSI+PC91c2U+DQo8dXNlIHhsaW5rOmhyZWY9IiNwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjMwIDApIHNrZXdZKC0wLjQwKSI+PC91c2U+DQo8L3N2Zz4=" x="0" y="0" width="3" height="3">
									      </image>
									    </pattern>
									    
									    <pattern id="pattern02" patternUnits="userSpaceOnUse" width="0.08" height="0.08"> 									    	 
									    	<image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxJyBoZWlnaHQ9JzEgJz4NCiAgPHJlY3Qgd2lkdGg9JzAuMDgnIGhlaWdodD0nMC4wOCcgZmlsbD0nI2ZmZicvPg0KICA8cGF0aCBkPSdNMCAwTDAuMDggMC4wOFpNMC4wOCAwTDAgMC4wOFonIHN0cm9rZS13aWR0aD0nMC4wMDUnIHN0cm9rZT0nI2FhYScvPg0KPC9zdmc+DQo=" x="0" y="0" width="0.08" height="0.08"> 
									    	</image>
									    </pattern>
									    <pattern id="verticalstripe" patternUnits="userSpaceOnUse" width="'0.06" height="0.49"> 
									    	<image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScwLjA2JyBoZWlnaHQ9JzAuNDknPg0KICA8cmVjdCB3aWR0aD0nMC4wMycgaGVpZ2h0PScwLjUnIGZpbGw9JyNmZmYnLz4NCiAgPHJlY3QgeD0nMC4wMycgd2lkdGg9JzAuMDEnIGhlaWdodD0nMC41JyBmaWxsPScjY2NjJy8+DQo8L3N2Zz4NCg==" x="0" y="0" width="0.06" height="0.49"> 
									    	</image> 
									    </pattern>
									    <pattern id="lightstripe" patternUnits="userSpaceOnUse" width="0.01" height="0.01"> 
									    	<image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc1JyBoZWlnaHQ9JzUnPgogIDxyZWN0IHdpZHRoPSc1JyBoZWlnaHQ9JzUnIGZpbGw9J3doaXRlJy8+CiAgPHBhdGggZD0nTTAgNUw1IDBaTTYgNEw0IDZaTS0xIDFMMSAtMVonIHN0cm9rZT0nIzg4OCcgc3Ryb2tlLXdpZHRoPScxJy8+Cjwvc3ZnPg==" x="0" y="0" width="0.01" height="0.01"> 
									    	</image> 
									    </pattern>
										<pattern id="pattern04" patternUnits="userSpaceOnUse" width="0.1" height="0.1"> 
											<image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScwLjEnIGhlaWdodD0nMC4xJz4NCiAgPHJlY3Qgd2lkdGg9JzAuMScgaGVpZ2h0PScwLjEnIGZpbGw9J3NpbHZlcicgLz4NCiAgPGNpcmNsZSBjeD0nMC4wNCcgY3k9JzAuMDQnIHI9JzAuMDQnIGZpbGw9J3doaXRlJy8+DQo8L3N2Zz4=" x="0" y="0" width="0.1" height="0.1"> 
											</image> 
										</pattern>
									    <pattern id="pattern01" patternUnits="userSpaceOnUse" width="0.06" height="0.06"> 
									    	<image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHhtbG5zOnhsaW5rPSdodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rJyB3aWR0aD0nMC4wNicgaGVpZ2h0PScwLjA2Jz4NCiAgPHJlY3Qgd2lkdGg9JzAuMDYnIGhlaWdodD0nMC4wNicgZmlsbD0nI2VlZWVlZScvPg0KICA8ZyBpZD0nYyc+DQogICAgPHJlY3Qgd2lkdGg9JzAuMDMnIGhlaWdodD0nMC4wMycgZmlsbD0nI2U2ZTZlNicvPg0KICAgIDxyZWN0IHk9JzAuMDEnIHdpZHRoPScwLjAzJyBoZWlnaHQ9JzAuMDInIGZpbGw9JyNkOGQ4ZDgnLz4NCiAgPC9nPg0KICA8dXNlIHhsaW5rOmhyZWY9JyNjJyB4PScwLjAzJyB5PScwLjAzJy8+DQo8L3N2Zz4=" x="0" y="0" width="6" height="6"> 
									    	</image> 
									    </pattern>
									    <pattern id="pattern03" patternUnits="userSpaceOnUse" width="0.1" height="0.1"> 
									    	<!-- <image xlink:href="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMC4xJyBoZWlnaHQ9JzAuMScgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4NCiAgPHBhdGggZD0nTTAgMEwwLjA0IDAuMDQnIHN0cm9rZT0nI2FhYScgZmlsbD0nI2FhYScgc3Ryb2tlLXdpZHRoPScwLjAxJy8+DQogIDxwYXRoIGQ9J00wLjAyNSAwTDAuMDUgMC4wMjVMMC4wNSAwLjA1TDAuMDkgMC4wOUwwLjA1IDAuMDVMMC4xIDAuMDVMMC4xIDAnIHN0cm9rZT0nI2FhYScgZmlsbD0nI2FhYScgc3Ryb2tlLXdpZHRoPScwLjAxJy8+DQogIDxwYXRoIGQ9J00wLjA1IDAuMUwwLjA1IDAuMDc1TDAuMDc1IDAuMScgc3Ryb2tlPScjYWFhJyBmaWxsPScjYWFhJyBzdHJva2Utd2lkdGg9JzAuMDEnLz4NCjwvc3ZnPg0K" x="0" y="0" width="0.1" height="0.1"> --> 
									    	<image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScwLjEwJyBoZWlnaHQ9JzAuMTAnPg0KICA8cmVjdCB3aWR0aD0nMC4xMCcgaGVpZ2h0PScwLjEwJyBmaWxsPSd3aGl0ZScvPg0KICA8cGF0aCBkPSdNLTAuMDEsMC4wMSBsMC4wMiwtMC4wMg0KICAgICAgICAgICBNMCwwLjEwIGwwLjEwLC0wLjEwDQogICAgICAgICAgIE0wLjA5LDAuMTEgbDAuMDIsLTAuMDInIHN0cm9rZT0nYmxhY2snIHN0cm9rZS13aWR0aD0nMC4wMycvPg0KPC9zdmc+" x="0" y="0" width="0.1" height="0.1">
									    	
									    	</image> 
									    </pattern>
									     <pattern id="pattern06" patternUnits="userSpaceOnUse" width="0.10" height="0.10"> 
										     <image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScwLjEnIGhlaWdodD0nMC4xJz4NCiAgPHJlY3Qgd2lkdGg9JzAuMTAnIGhlaWdodD0nMC4xMCcgZmlsbD0nd2hpdGUnIC8+DQogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScwLjAyJyBoZWlnaHQ9JzAuMDInIGZpbGw9J3NpbHZlcicgLz4NCjwvc3ZnPg==" x="0" y="0" width="0.10" height="0.10"> 
										     </image> 
									     </pattern>
										 <pattern id="pattern07" patternUnits="userSpaceOnUse" width="0.1" height="0.1"> 
										   <!--  <image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScwLjEnIGhlaWdodD0nMC4xJz4NCiAgPHJlY3Qgd2lkdGg9JzAuMScgaGVpZ2h0PScwLjEnIGZpbGw9J3doaXRlJyAvPg0KICA8cmVjdCB4PScwJyB5PScwJyB3aWR0aD0nMC4wOScgaGVpZ2h0PScwLjA5JyBmaWxsPSdzaWx2ZXInIC8+DQo8L3N2Zz4=" x="0" y="0" width="0.1" height="0.1"> 
										 	</image>  -->
										 	<image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScwLjEwJyBoZWlnaHQ9JzAuMTAnPg0KICA8cmVjdCB3aWR0aD0nMC4xMCcgaGVpZ2h0PScwLjEwJyBmaWxsPSd3aGl0ZScgLz4NCiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzAuMDknIGhlaWdodD0nMC4wOScgZmlsbD0nYmxhY2snIC8+DQo8L3N2Zz4=" x="0" y="0" width="0.1" height="0.1"> 
										 	</image>
										 </pattern>
 										 <pattern id="pattern05" patternUnits="userSpaceOnUse" width="0.1" height="0.1"> 
 										 	<!-- <image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+DQogIDxyZWN0IHdpZHRoPScwLjEnIGhlaWdodD0nMC4xJyBmaWxsPSd3aGl0ZScgLz4NCiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzAuMScgaGVpZ2h0PScwLjAxJyBmaWxsPSdzaWx2ZXInIC8+DQo8L3N2Zz4=" x="0" y="0" width="0.1" height="0.1"> 
 										 	</image>  -->
 										 	<image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScwLjEwJyBoZWlnaHQ9JzAuMTAnPg0KICA8cmVjdCB3aWR0aD0nMC4xMCcgaGVpZ2h0PScwLjEwJyBmaWxsPSdibGFjaycgLz4NCiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzAuMTAnIGhlaWdodD0nMC4wMicgZmlsbD0nZ3JleScgLz4NCjwvc3ZnPg==" x="0" y="0" width="0.1" height="0.2"> --> 
 										 	</image>
 										 </pattern> 		     
									</defs>					
										 	
								  								  

									<xsl:variable name="somma"><xsl:value-of select="math:abs($sv) + math:abs($sr) + math:abs($bs) + math:abs($imp) + math:abs($iva) + math:abs($odiv)"/></xsl:variable>
									<xsl:choose>
										<xsl:when test="$sv=0 and $sr=0 and $bs=0 and $imp=0 and $iva=0 and $odiv=0 ">
										 	<circle cx="0" cy="0" r="1" stroke="black" stroke-width="0.001" fill="white"/>
											<circle cx="0" cy="0" r="0.5" stroke="black" stroke-width="0.001" fill="white"/>
										</xsl:when>
										<xsl:otherwise>
		 									<xsl:variable name="svPercent"><xsl:value-of select="math:abs($sv div $somma)"/></xsl:variable>
											<xsl:variable name="srPercent"><xsl:value-of select="math:abs($sr div $somma)"/></xsl:variable>
											<xsl:variable name="impPercent"><xsl:value-of select="math:abs($imp div $somma)"/></xsl:variable>
											<xsl:variable name="ivaPercent"><xsl:value-of select="math:abs($iva div $somma)"/></xsl:variable>
											<xsl:variable name="odivPercent"><xsl:value-of select="math:abs(math:abs($odiv) div $somma)"/></xsl:variable>
											
											<xsl:variable name="bsPercent"><xsl:value-of select="math:abs(math:abs($bs) div $somma)"/></xsl:variable>
											
		<!--  								<xsl:variable name="svPercent"><xsl:value-of select="0.6"/></xsl:variable>	
											<xsl:variable name="srPercent"><xsl:value-of select="0.0416"/></xsl:variable>
											<xsl:variable name="impPercent"><xsl:value-of select="0.0416"/></xsl:variable>
											<xsl:variable name="ivaPercent"><xsl:value-of select="0.0416"/></xsl:variable>
											<xsl:variable name="odivPercent"><xsl:value-of select="0.0416"/></xsl:variable>
											<xsl:variable name="raiPercent"><xsl:value-of select="0.0416"/></xsl:variable>
											<xsl:variable name="bsPercent"><xsl:value-of select="0.0416"/></xsl:variable> -->
	
											
											<xsl:variable name="cumulativePercent"><xsl:value-of select="0"/></xsl:variable>
									
											<xsl:choose>
												<xsl:when test="$svPercent &gt; 0">
													<xsl:variable name="XsvPercentStart"><xsl:value-of select="math:cos(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="YsvPercentStart"><xsl:value-of select="math:sin(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="XsvPercentEnd"><xsl:value-of select="math:cos(2 * 3.1415292 * $svPercent )"/></xsl:variable>
													<xsl:variable name="YsvPercentEnd"><xsl:value-of select="math:sin(2 * 3.1415292 * $svPercent )"/></xsl:variable>
														
					 								<xsl:variable name="textPercX"><xsl:value-of select="math:cos(2 * 3.1415292 * ($svPercent div 2) )*0.75"/></xsl:variable>
													<xsl:variable name="textPercY"><xsl:value-of select="math:sin(2 * 3.1415292 * ($svPercent div 2) )*0.75"/></xsl:variable>
														
													<xsl:variable name="largeArcFlagSvPercent">
													  <xsl:choose>
													    <xsl:when test="$svPercent &gt; 0.5">
													      <xsl:text>1</xsl:text>
													    </xsl:when>
													    <xsl:otherwise>
													      <xsl:text>0</xsl:text>
													    </xsl:otherwise>
													  </xsl:choose>
													</xsl:variable>
																						
																																									
													<xsl:variable name="slice1part1"><xsl:value-of select="concat('M ',$XsvPercentStart,' ',$YsvPercentStart,' ')"/></xsl:variable>
													<xsl:variable name="slice1part2"><xsl:value-of select="concat('A 1 1 0 ',$largeArcFlagSvPercent,' ','1 ',$XsvPercentEnd,' ',$YsvPercentEnd,' L 0 0')"/></xsl:variable>
													<xsl:variable name="slice1"><xsl:value-of select="concat($slice1part1,$slice1part2)"></xsl:value-of></xsl:variable>
													
													<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="url(#pattern01)" fill-opacity="1.0" >    									
							      								<xsl:attribute name="d"><xsl:value-of select="$slice1"/></xsl:attribute> 
							    					</path>
											
<!-- 				 									<svg:text font-size='0.08' font-weight='bold'>
												    	<xsl:attribute name="x"><xsl:value-of select="$textPercX"/></xsl:attribute>
												        <xsl:attribute name="y"><xsl:value-of select="$textPercY"/></xsl:attribute>
				 								        <xsl:value-of select="translate(format-number( ($svPercent * 100), '#.##'),'.',',')" />%
												    </svg:text>	 -->									
												</xsl:when>
												<xsl:otherwise>
												</xsl:otherwise>									    
											</xsl:choose>


				 							<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $svPercent"/></xsl:variable>		
											<xsl:choose>
												<xsl:when test="$srPercent &gt; 0">
													<xsl:variable name="XsrPercentStart"><xsl:value-of select="math:cos(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="YsrPercentStart"><xsl:value-of select="math:sin(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="XsrPercentEnd"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent+$srPercent))"/></xsl:variable>
													<xsl:variable name="YsrPercentEnd"><xsl:value-of select="math:sin(2 * 3.1415292 * ($cumulativePercent+$srPercent) )"/></xsl:variable>									
 													<xsl:variable name="textPercX"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent+($srPercent div 2)) )*0.75"/></xsl:variable>
													<xsl:variable name="textPercY"><xsl:value-of select="math:sin(2 * 3.1415292 * ($cumulativePercent+($srPercent div 2)) )*0.75"/></xsl:variable> 
													
												
													<xsl:variable name="largeArcFlagSrPercent">
													  <xsl:choose>
													    <xsl:when test="$srPercent &gt; 0.5">
													      <xsl:text>1</xsl:text>
													    </xsl:when>
													    <xsl:otherwise>
													      <xsl:text>0</xsl:text>
													    </xsl:otherwise>
													  </xsl:choose>
													</xsl:variable>
				
													<xsl:variable name="slice2part1"><xsl:value-of select="concat('M ',$XsrPercentStart,' ',$YsrPercentStart,' ')"/></xsl:variable>
													<xsl:variable name="slice2part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagSrPercent,' ','1 ',$XsrPercentEnd,' ',$YsrPercentEnd,' L 0 0')"/></xsl:variable>
													<xsl:variable name="slice2"><xsl:value-of select="concat($slice2part1,$slice2part2)"></xsl:value-of></xsl:variable>
													
													<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="url(#pattern02)" fill-opacity="1.0">
						      									<xsl:attribute name="d"><xsl:value-of select="$slice2"/></xsl:attribute> 
						    						</path>
				
<!-- 													<svg:text font-size="0.08" font-weight="bold">
											          <xsl:attribute name="x"><xsl:value-of select="$textPercX"/></xsl:attribute>
											          <xsl:attribute name="y"><xsl:value-of select="$textPercY"/></xsl:attribute>
											          <xsl:value-of select="translate(format-number( ($srPercent * 100), '#.##'),'.',',')" />%
											        </svg:text> -->
		 										</xsl:when>
		 										<xsl:otherwise>
												</xsl:otherwise>
		 									</xsl:choose>
											
											<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $srPercent"/></xsl:variable>																							
											<xsl:choose>
												<xsl:when test="$impPercent &gt; 0">																
													<xsl:variable name="XimpPercentStart"><xsl:value-of select="math:cos(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="YimpPercentStart"><xsl:value-of select="math:sin(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="XimpPercentEnd"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent+$impPercent) )"/></xsl:variable>
													<xsl:variable name="YimpPercentEnd"><xsl:value-of select="math:sin(2 * 3.1415292 * ($cumulativePercent+$impPercent) )"/></xsl:variable>
													<xsl:variable name="textPercX"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent+($impPercent div 2) )) *0.75"/></xsl:variable>
													<xsl:variable name="textPercY"><xsl:value-of select="math:sin(2 * 3.1415292 * ($cumulativePercent+($impPercent div 2)) ) *0.75"/></xsl:variable>

													<xsl:variable name="largeArcFlagImpPercent">
													  <xsl:choose>
													    <xsl:when test="$impPercent &gt; 0.5">
													      <xsl:text>1</xsl:text>
													    </xsl:when>
													    <xsl:otherwise>
													      <xsl:text>0</xsl:text>
													    </xsl:otherwise>
													  </xsl:choose>
													</xsl:variable>
				
													<xsl:variable name="slice3part1"><xsl:value-of select="concat('M ',$XimpPercentStart,' ',$YimpPercentStart,' ')"/></xsl:variable>
													<xsl:variable name="slice3part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagImpPercent,' ','1 ',$XimpPercentEnd,' ',$YimpPercentEnd,' L 0 0')"/></xsl:variable>
													<xsl:variable name="slice3"><xsl:value-of select="concat($slice3part1,$slice3part2)"></xsl:value-of></xsl:variable>
													
													<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="url(#pattern03)" fill-opacity="1.0">
						      									<xsl:attribute name="d"><xsl:value-of select="$slice3"/></xsl:attribute> 
						    						</path>
				
<!-- 													<svg:text font-size="0.08" font-weight="bold" fill="#BEBEBE">
											          <xsl:attribute name="x"><xsl:value-of select="$textPercX"/></xsl:attribute>
											          <xsl:attribute name="y"><xsl:value-of select="$textPercY"/></xsl:attribute>
											          <xsl:value-of select="translate(format-number( ($impPercent * 100), '#.##'),'.',',')" />%
											        </svg:text> -->
												</xsl:when>
											    <xsl:otherwise>
												</xsl:otherwise>
											</xsl:choose>
		
											
											<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $impPercent"/></xsl:variable>		
											<xsl:choose>
												<xsl:when test="$ivaPercent &gt; 0">		
													<xsl:variable name="XivaPercentStart"><xsl:value-of select="math:cos(2 * 3.1415292 * $cumulativePercent )"/></xsl:variable>
													<xsl:variable name="YivaPercentStart"><xsl:value-of select="math:sin(2 * 3.1415292 * $cumulativePercent )"/></xsl:variable>
													<xsl:variable name="XivaPercentEnd"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent+$ivaPercent) )"/></xsl:variable>
													<xsl:variable name="YivaPercentEnd"><xsl:value-of select="math:sin(2 * 3.1415292 * ($ivaPercent+$cumulativePercent) )"/></xsl:variable>
				
													<xsl:variable name="textPercX"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent+($ivaPercent div 2) ))*0.75"/></xsl:variable>
													<xsl:variable name="textPercY"><xsl:value-of select="math:sin(2 * 3.1415292 * ($cumulativePercent+($ivaPercent div 2)) )*0.75"/></xsl:variable>
													
													<xsl:variable name="largeArcFlagIvaPercent">
													  <xsl:choose>
													    <xsl:when test="$ivaPercent &gt; 0.5">
													      <xsl:text>1</xsl:text>
													    </xsl:when>
													    <xsl:otherwise>
													      <xsl:text>0</xsl:text>
													    </xsl:otherwise>
													  </xsl:choose>
													</xsl:variable>
				
													<xsl:variable name="slice4part1"><xsl:value-of select="concat('M ',$XivaPercentStart,' ',$YivaPercentStart,' ')"/></xsl:variable>
													<xsl:variable name="slice4part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagIvaPercent,' ','1 ',$XivaPercentEnd,' ',$YivaPercentEnd,' L 0 0')"/></xsl:variable>
													<xsl:variable name="slice4"><xsl:value-of select="concat($slice4part1,$slice4part2)"></xsl:value-of></xsl:variable>
				
													<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="url(#pattern04)" fill-opacity="1.0">
						      									<xsl:attribute name="d"><xsl:value-of select="$slice4"/></xsl:attribute> 
						    						</path>
				
<!-- 													<svg:text font-size="0.08" font-weight="bold" >
											          <xsl:attribute name="x"><xsl:value-of select="$textPercX"/></xsl:attribute>
											          <xsl:attribute name="y"><xsl:value-of select="$textPercY"/></xsl:attribute>
											          <xsl:value-of select="translate(format-number( ($ivaPercent * 100), '#.##'),'.',',')" />%
											        </svg:text> -->
											    </xsl:when>
											    <xsl:otherwise>
												</xsl:otherwise>
										    </xsl:choose>
		



											<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $ivaPercent"/></xsl:variable>

											<xsl:choose>
												<xsl:when test="$odivPercent &gt; 0">
		
													<xsl:variable name="XodivPercentStart"><xsl:value-of select="math:cos(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="YodivPercentStart"><xsl:value-of select="math:sin(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="XodivPercentEnd"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent + $odivPercent) )"/></xsl:variable>
													<xsl:variable name="YodivPercentEnd"><xsl:value-of select="math:sin(2 * 3.1415292 * ($cumulativePercent + $odivPercent) )"/></xsl:variable>
													
													<xsl:variable name="textPercX"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent+($odivPercent div 2) ))*0.75"/></xsl:variable>
													<xsl:variable name="textPercY"><xsl:value-of select="math:sin(2 * 3.1415292 * ($cumulativePercent+($odivPercent div 2)) )*0.75"/></xsl:variable>
													
													<xsl:variable name="largeArcFlagOdivPercent">
													  <xsl:choose>
													    <xsl:when test="$odivPercent &gt; 0.5">
													      <xsl:text>1</xsl:text>
													    </xsl:when>
													    <xsl:otherwise>
													      <xsl:text>0</xsl:text>
													    </xsl:otherwise>
													  </xsl:choose>
													</xsl:variable>
				
													<xsl:variable name="slice5part1"><xsl:value-of select="concat('M ',$XodivPercentStart,' ',$YodivPercentStart,' ')"/></xsl:variable>
													<xsl:variable name="slice5part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagOdivPercent,' ','1 ',$XodivPercentEnd,' ',$YodivPercentEnd,' L 0 0')"/></xsl:variable>
													<xsl:variable name="slice5"><xsl:value-of select="concat($slice5part1,$slice5part2)"></xsl:value-of></xsl:variable>
				
													<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="url(#pattern05)" fill-opacity="1.0">
						      									<xsl:attribute name="d"><xsl:value-of select="$slice5"/></xsl:attribute> 
						    						</path> 
				
<!-- 													<svg:text font-size="0.08" font-weight="bold" fill="#BEBEBE">
											          <xsl:attribute name="x"><xsl:value-of select="$textPercX"/></xsl:attribute>
											          <xsl:attribute name="y"><xsl:value-of select="$textPercY"/></xsl:attribute>
											          <xsl:value-of select="translate(format-number( ($odivPercent * 100), '#.##'),'.',',')" />%
											        </svg:text> -->
											
											    </xsl:when>
											    <xsl:otherwise>
											    </xsl:otherwise>
										    </xsl:choose>
		
		
		
		
											
											<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $odivPercent"/></xsl:variable>
											
		
		
		
				    
											
											<xsl:choose>
												<xsl:when test="$bsPercent &gt; 0">
													<xsl:variable name="XbsPercentStart"><xsl:value-of select="math:cos(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="YbsPercentStart"><xsl:value-of select="math:sin(2 * 3.1415292 * $cumulativePercent)"/></xsl:variable>
													<xsl:variable name="XbsPercentEnd"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent + $bsPercent) )"/></xsl:variable>
													<xsl:variable name="YbsPercentEnd"><xsl:value-of select="math:sin(2 * 3.1415292 * ($cumulativePercent + $bsPercent) )"/></xsl:variable>
				
													<xsl:variable name="textPercX"><xsl:value-of select="math:cos(2 * 3.1415292 * ($cumulativePercent+($odivPercent div 2) ))*0.75"/></xsl:variable>
													<xsl:variable name="textPercY"><xsl:value-of select="math:sin(2 * 3.1415292 * ($cumulativePercent+($odivPercent div 2)) )*0.75"/></xsl:variable>
				
													<xsl:variable name="largeArcFlagBsPercent">
													  <xsl:choose>
													    <xsl:when test="$bsPercent &gt; 0.5">
													      <xsl:text>1</xsl:text>
													    </xsl:when>
													    <xsl:otherwise>
													      <xsl:text>0</xsl:text>
													    </xsl:otherwise>
													  </xsl:choose>
													</xsl:variable>
				
													<xsl:variable name="slice7part1"><xsl:value-of select="concat('M ',$XbsPercentStart,' ',$YbsPercentStart,' ')"/></xsl:variable>
													<xsl:variable name="slice7part2"><xsl:value-of select="concat('A 1 1 0 ', $largeArcFlagBsPercent,' ','1 ',$XbsPercentEnd,' ',$YbsPercentEnd,' L 0 0')"/></xsl:variable>
													<xsl:variable name="slice7"><xsl:value-of select="concat($slice7part1,$slice7part2)"></xsl:value-of></xsl:variable>
				
													<path stroke="black" stroke-width="0.01" stroke-linejoin="round" fill="url(#pattern06)" fill-opacity="1.0">
						      									<xsl:attribute name="d"><xsl:value-of select="$slice7"/></xsl:attribute> 
						    						</path> 
						    						
						    						
	<!-- 					      						<svg:text font-size="0.08" font-weight="bold">
											          <xsl:attribute name="x"><xsl:value-of select="$textPercX"/></xsl:attribute>
											          <xsl:attribute name="y"><xsl:value-of select="$textPercY"/></xsl:attribute>
											          <xsl:value-of select="translate(format-number( ($bsPercent * 100), '#.##'),'.',',')" />%
											        </svg:text> -->
												</xsl:when>
											</xsl:choose>
				    						
		 		    						<xsl:variable name="cumulativePercent"><xsl:value-of select="$cumulativePercent + $bsPercent"/></xsl:variable>
										
										</xsl:otherwise>
									</xsl:choose>
									
								
											    						
 		    						<circle cx="0" cy="0" r="0.5" stroke="black" stroke-width="0.01" fill="white"/>

								  </svg>
							
								</fo:instream-foreign-object>
							</fo:block>
						</fo:block-container>
							
						
						
						<!-- <xsl:if test="./child::CONTRATTO_GAS"> -->
						<fo:block-container position="absolute"
							left="68mm"
							width="30mm"
							height="25mm"
							font-family="universcbold"
							font-size="28pt"
							text-align="center">
							<xsl:variable name="importo">
								<xsl:value-of select="$importo_no_virgola div 100"/>
							</xsl:variable>
							
							<xsl:if test="$importo &lt; 1000">
								<xsl:attribute name="font-size">24pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo &lt; 10000 and $importo &gt; 1000">
								<xsl:attribute name="font-size">22pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo &lt; 100000 and $importo &gt; 10000">
								<xsl:attribute name="font-size">20pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo &lt; 1000000 and $importo &gt; 100000">
								<xsl:attribute name="font-size">18pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo &lt; 10000000 and $importo &gt; 1000000">
								<xsl:attribute name="font-size">15pt</xsl:attribute>
							</xsl:if>
							<xsl:if test="$importo &lt; 100000000 and $importo &gt; 10000000">
								<xsl:attribute name="font-size">13pt</xsl:attribute>
							</xsl:if>
							
							<!-- <xsl:if test="./child::CHARTS/child::ROWSET/child::SEZ='Bonus sociale'"> -->
							<xsl:choose>
								<xsl:when test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE='0,00') and (./child::CONTRATTO_GAS)">
									<xsl:attribute name="top">58mm</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="top">53mm</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<fo:block font-family="universc" font-size="10pt">
								Totale
							</fo:block>
							<fo:block>
								<xsl:value-of select="./child::IMPORTO_DOCUMENTO" />
							</fo:block>
							<fo:block font-family="universc" font-size="10pt">
								Euro
							</fo:block>
						</fo:block-container>
						<!-- </xsl:if> -->
						
						
						
						<xsl:call-template name="DETTAGLIO_IVA"/>
						
						
						
						<fo:table space-before="7mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="108.5mm"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-family="universc" font-size="7pt" padding-top="2mm" border-top="1 solid black">
										<!-- <xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
										<xsl:attribute name="background-image">url(svg/rectangle_frontespizio_20b.svg)</xsl:attribute> -->
										
										<fo:block-container position="absolute"
											top="-2.5mm"
											left="-2.5mm"
											width="50mm"
											height="50mm">
											<fo:block>
												<fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" width="6mm" src="img/informazioni.svg"/>
											</fo:block>
										</fo:block-container>
										
										<xsl:call-template name="STATO_PAGAMENTI"/>
						
										<xsl:call-template name="INFO_RID"/>
										
										<xsl:call-template name="ALTRE_COMUNICAZIONI_FRONTESPIZIO"/>

									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>
						<fo:block font-size="6pt">
							L'imposta di bollo, se dovuta, viene assolta in modo virtuale
						</fo:block>
						<fo:block font-size="6pt">
							(Aut. Agenzia Entrate, uff. Cagliari 1 n. 19756 del 29/04/2005).
						</fo:block>	
					</fo:table-cell>
				</fo:table-row>

			</fo:table-body>
		</fo:table>
		
<!--		<fo:block space-before="6mm" font-size="6pt">
			L'imposta di bollo, se dovuta, viene assolta in modo virtuale (Aut. Agenzia Entrate, uff. Cagliari 1 n. 19756 del 29/04/2005).
		</fo:block> -->
		
		</fo:block-container>


		
		<fo:block-container position="absolute"
							top="84mm"
							left="24.5mm"
							width="30mm"
							height="4.5mm">
		
			<fo:block font-family="universcbold" font-size="10pt" text-align='justify'>
				MERCATO LIBERO
				
			</fo:block>
		
		</fo:block-container>





		
		<!-- <fo:block-container position="absolute"
							top="85mm"
							left="155.5mm"
							width="30mm"
							height="4.5mm">
		
			<fo:block font-family="universcbold" font-size="10pt">
				MERCATO LIBERO
			</fo:block>
		
		</fo:block-container> -->
		
		
		
		<!-- BOLLETTA 2.0 -->
		
		
		
		<xsl:if test="@SPOT='NO' or not(@SPOT)">
			<fo:block break-before="page">
			</fo:block>
			
			
			<xsl:if test="./child::CONTRATTO_GAS">
				<xsl:choose>
					<xsl:when test="$status_piu_uno &lt; 201101 or substring(@COMPETENZA,8,4) &lt; 2011">
						<xsl:call-template name="CONTRATTI_MULTISITO">
							<xsl:with-param name="bordi" select="$bordi"/>
							<xsl:with-param name="color" select="$color"/>
							<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
							<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
							<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
							<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
							<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
							<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
							<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="CONTRATTI_MULTISITO_012011">
							<xsl:with-param name="bordi" select="$bordi"/>
							<xsl:with-param name="color" select="$color"/>
							<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
							<xsl:with-param name="sfondo_titoli" select="$sfondo_titoli"/>
							<xsl:with-param name="color-sezioni" select="$color-sezioni"/>
							<xsl:with-param name="color-sottosezioni" select="$color-sottosezioni"/>
							<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
							<xsl:with-param name="svg-sottosezioni" select="$svg-sottosezioni"/>
							<xsl:with-param name="svg-dettaglio" select="$svg-dettaglio"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			
			
			<xsl:apply-templates select="CONTRATTO_DEPOSITO_CAUZIONALE">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
				
			<xsl:apply-templates select="CONTRATTO_FONIA">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="CONTRATTO_SERVIZI_VARI">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
			
			<xsl:apply-templates select="CONTRATTO_AREASERVER">
				<xsl:with-param name="bordi" select="$bordi"/>
				<xsl:with-param name="color" select="$color"/>
				<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
				<xsl:with-param name="svg-sezioni" select="$svg-altre-sezioni"/>
			</xsl:apply-templates>
			
			
			<xsl:choose>
				<xsl:when test="$status_piu_uno &lt; 201101">
					<xsl:call-template name="FINALE">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="FINALE_012011">
						<xsl:with-param name="bordi" select="$bordi"/>
						<xsl:with-param name="color" select="$color"/>
						<xsl:with-param name="svg-titoli" select="$svg-sezioni"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			
			
			<xsl:if test="./child::CONTRATTO_GAS and $qualita='SI'">
				<xsl:call-template name="INFORMATIVA_QUALITA"/>
			</xsl:if>
		</xsl:if>

		
		<!--
		*********************************************************
		**  Blocco per la determinazione del numero di pagine  **
		*********************************************************
		-->
		<fo:block id="{generate-id(.)}"/>
		
    </fo:flow>
</fo:page-sequence>    


<!--
**********************************************************
**					Rinnovi se presenti					**
**********************************************************
-->
<!-- <xsl:for-each select="./child::RINNOVI/child::RINNOVO[@VISUALIZZA='SI']">
    <fo:page-sequence master-reference="pm0-blank">
        <fo:flow flow-name="body">
					<fo:block-container position="absolute"
										top="3.75pt"
										left="-0.75pt"
										width="210mm"
										height="297mm"
										display-align="after">
						<fo:block text-align="center">
							<fo:external-graphic>
								<xsl:attribute name="height">297mm</xsl:attribute>
								<xsl:attribute name="content-height">297mm</xsl:attribute>
								<xsl:attribute name="content-width">210mm</xsl:attribute>
								<xsl:if test="./child::ALLEGATO='AL-FAEN0110FX'">
									<xsl:attribute name="src">./allegati_rinnovi/<xsl:value-of select="./child::ALLEGATO"/>-RN.pdf</xsl:attribute>
								</xsl:if>
								<xsl:if test="not(./child::ALLEGATO='AL-FAEN0110FX')">
									<xsl:attribute name="src">./allegati_rinnovi/<xsl:value-of select="./child::ALLEGATO"/>.pdf</xsl:attribute>
								</xsl:if>
							</fo:external-graphic>
						</fo:block>
					</fo:block-container>
        </fo:flow>
    </fo:page-sequence>
</xsl:for-each> -->

<!--
**********************************************************
**			  Nuove condizioni contrattuali				**
**********************************************************
-->
<!-- <xsl:if test="./child::CONTRATTO_GAS[@NUOVE_CONDIZIONI='B1']">
    <xsl:call-template name="COMUNICAZIONE_CONDIZIONI"/>
    <xsl:call-template name="SCHEDA_RIEPILOGO"/>
    <xsl:call-template name="CONDIZIONI"/>
</xsl:if>

<xsl:if test="./child::CONTRATTO_GAS[@NUOVE_CONDIZIONI='C11']">
    <xsl:call-template name="COMUNICAZIONE_CONDIZIONI_C11"/>
	<xsl:call-template name="SCHEDA_RIEPILOGO_C11"/>
    <xsl:call-template name="CONDIZIONI_C11"/>
</xsl:if> -->

<!--
**********************************************************
**					Autocertificazione					**
**********************************************************
-->
<!-- <xsl:if test="CONTRATTO_GAS and $autocertificazione='SI' and (@SPOT='NO' or not(@SPOT))">
	<xsl:call-template name="AUTOCERTIFICAZIONE"/>
</xsl:if> -->

<!--
********************************************************
**             Accoda eventuali bollettini            **
********************************************************
-->
<xsl:if test="./child::BOLLETTINO and (@SPOT='NO' or not(@SPOT))">
	<xsl:call-template name="BOLLETTINO"/>
</xsl:if>

</xsl:template>



<!--
***********************************************
**  Riquadro lettura contatore - autolettura **
***********************************************
-->
<xsl:template name="LETTURA_AUTOLETTURA_FRONTESPIZIO">
	<xsl:choose>
		<xsl:when test="./child::AUTOLETTURA">
			<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">
				AUTOLETTURA
			</fo:block>
			
			<xsl:choose>
				<xsl:when test="./child::AUTOLETTURA='MONO'">
					<fo:block>Per ricevere fatture allineate ai suoi consumi reali, usufruisca del servizio di autolettura!</fo:block>
				</xsl:when>
			  
				<xsl:when test="./child::AUTOLETTURA='MULTI'">
					<fo:block>Per ricevere fatture allineate ai suoi consumi reali, usufruisca del servizio di autolettura, riservato ai punti di prelievo monorari!</fo:block>
				</xsl:when>
			</xsl:choose>
			
			<fo:block>Potra' trasmettere i consumi all'indirizzo autolettura@energit.it o al numero gratuito 800.1922.33 dal 25<xsl:value-of select="./child::CONTRATTO_GAS/child::DATA_AUTOLETTURA_INIZIALE" /> al <xsl:value-of select="./child::CONTRATTO_GAS/child::DATA_AUTOLETTURA_FINALE" />.</fo:block>
			
			<fo:block>I consumi comunicati potranno essere utilizzati a partire dalla seconda autolettura.</fo:block>
		</xsl:when>
	
		<xsl:otherwise>
			<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">
				LETTURA DEL CONTATORE
			</fo:block>
			
			<fo:block>Per ricevere fatture sempre allineate ai suoi consumi reali comunichi la lettura del contatore nei giorni dal 28 al 4 di ogni mese! Annoti le cifre del contatore riportate su sfondo nero e ce le indichi, unitamente al suo Codice Cliente, via email all'indirizzo autolettura@energit.it, via fax al numero 800.19.22.55, oppure chiamando l'800.19.22.22.</fo:block>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>





<!--
***************************
**  Segnalazione guasti  **
***************************
-->
<xsl:template name="SEGNALAZIONE_GUASTI">
	<fo:block xsl:use-attribute-sets="blocco_riquadri_frontespizio">
		<fo:block text-align="center" xsl:use-attribute-sets="font_titolo_tabella_bold">
			SEGNALAZIONE GUASTI
		</fo:block>

		<fo:block>In caso di guasti dovra' contattare il Distributore Locale al numero telefonico <xsl:value-of select="./child::CONTRATTO_GAS/child::CONTATTO_DISTRIBUTORE" />.</fo:block>
	</fo:block>
</xsl:template>





<!--
******************************
**  Modalita' di pagamento  **
******************************
-->
<xsl:template name="PAGAMENTI">
	<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">
		PAGAMENTI
	</fo:block>
	
	<xsl:choose>
		<xsl:when test="@TIPO_DOCUMENTO='FATTURA' and not(./child::IMPORTO_DOCUMENTO='0,00')">
			<xsl:choose>
				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
					<fo:block font-family="universcbold">Modalita' di pagamento</fo:block>
				</xsl:when>
				
				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_PE_REQ']">
					<fo:block font-family="universcbold">La modalita' di pagamento per questa fattura e'</fo:block>
				</xsl:when>
				
				<xsl:otherwise>
					<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'La fattura risulta pagata')">
						<fo:block font-family="universcbold">Lei ha scelto di pagare con</fo:block>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			
			<xsl:choose>
				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_PE_REQ']">
					<fo:block>Bollettino Postale</fo:block>					
					<fo:block space-before="2mm" font-family="universcbold">La sua richiesta di attivazione della modalita' addebito diretto SEPA Direct Debit (SDD) e' in attesa di conferma presso la sua Banca. Per il pagamento di questa fattura dovra' utilizzare il bollettino postale che trova nell'ultimo foglio. Grazie.</fo:block>
					<fo:block space-before="2mm" font-family="universcbold"><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CAUSALE" /></fo:block>
				</xsl:when>
				
				<xsl:otherwise>
					<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO" /></fo:block>
			
					<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bonifico Bancario'">
						<fo:block>Energ.it S.p.A.</fo:block>
					</xsl:if>
					
					<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::INTESTATARIO_PAGAMENTO" /></fo:block>
					
					<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bollettino Postale'">
						<fo:block><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE" /></fo:block>
					</xsl:if>
					
					<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::IBAN">
						<fo:block>IBAN: <xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::IBAN" /></fo:block>
					</xsl:if>
					
					<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE = 'Sardex'">
						<fo:block>Sardex</fo:block>
					</xsl:if>
					
					<fo:block font-family="universcbold"><xsl:value-of select="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CAUSALE" /></fo:block>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="./child::IMPORTO_DOCUMENTO='0,00'">
					<fo:block font-family="universcbold">Non c'e' niente da pagare!</fo:block>
				</xsl:when>
				
				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RIMBORSO_SDD']">
					<fo:block font-family="universcbold">Non c'e' niente da pagare!</fo:block>
					<fo:block font-family="universcbold">L'importo totale del documento di <xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> euro le sara' rimborsato tramite accredito sul suo conto corrente normalmente utilizzato per il pagamento delle fatture Energit.</fo:block>
				</xsl:when>
				
				<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='RIMBORSO_ASS']">
					<fo:block font-family="universcbold">Non c'e' niente da pagare!</fo:block>
					<fo:block font-family="universcbold">L'importo totale del documento di <xsl:value-of select="./child::IMPORTO_DOCUMENTO" /> euro le sara' rimborsato tramite assegno inviato al suo indirizzo di spedizione delle fatture.</fo:block>
				</xsl:when>
				
				<xsl:otherwise>
					<fo:block font-family="universcbold">Per questa nota di credito non c'e' niente da pagare</fo:block>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
	
	<fo:block space-before="2mm">Altre modalità di pagamento disponibili:</fo:block>
	<fo:block>
		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bonifico Bancario'">
			<fo:block>Bollettino Postale - Addebito su conto corrente bancario (SDD)</fo:block>
		</xsl:if>
		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Bollettino Postale'">
			<fo:block>Bonifico Bancario - Addebito su conto corrente bancario (SDD)</fo:block>
		</xsl:if>
		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO = 'Addebito tramite SDD'">
			<fo:block>Bollettino Postale - Bonifico Bancario</fo:block>
		</xsl:if>
		<xsl:if test="./child::MODALITA_PAGAMENTO_DOCUMENTO/child::CONTO_CORRENTE = 'Sardex'">
			<fo:block>Bollettino Postale - Bonifico Bancario - Addebito su conto corrente bancario (SDD)</fo:block>
		</xsl:if>
	</fo:block>
	
	<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='SARDEX'">
		<fo:block space-before="2mm">La informiamo che il pagamento della presente fattura potrà essere effettuato nelle modalità indicate nella proposta di contratto da lei sottoscritta, con espressa esclusione dell’utilizzo del circuito Sardex</fo:block>
	</xsl:if>
	
</xsl:template>





<!--
*****************************
**   Avvisi frontespizio   **
*****************************
-->
<xsl:template name="AVVISI_FRONTESPIZIO">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">	
			<!-- Comunicazione Energit per noi -->
			<fo:table-row height="27mm">
				<fo:table-cell>
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3'">
						
							<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute>
							<xsl:attribute name="padding">1mm</xsl:attribute> -->
							
							
							<xsl:variable name="contratto_exnoi">
								<xsl:for-each select="./child::CONTRATTO_GAS">
									<xsl:if test="substring(./child::PRODOTTO_SERVIZIO,0,16)='Energit per Noi'">
										<xsl:value-of select="./child::CONTRACT_NO" />
									</xsl:if>
								</xsl:for-each>
							</xsl:variable>
						
							<fo:block font-family="universcbold">
								ENERGIT PER NOI RINNOVA ANCORA LA SUA CONVENIENZA
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Il risparmio Energit per Noi è confermato per il terzo anno consecutivo!
								Anche nel 2012, infatti, il suo contratto
								<xsl:value-of select="substring($contratto_exnoi,0,8)" /> le offre un
								<fo:inline font-family="universcbold">prezzo bloccato della componente energia
								invariato rispetto al 2010 e al 2011: soli 0,05 euro/kWh in tutte le fasce
								orarie</fo:inline>, il massimo risparmio e la comodità di consumare liberamente
								in tutte le ore della giornata. Potrà aumentare ulteriormente il suo risparmio
								con un uso accorto e responsabile dell’energia, che le permetterà di limitare i
								consumi e di salvaguardare l’ambiente!
							</fo:block>
						</xsl:if>
						
						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='NUOVO_PARTNER_INDUSTRIALE'">
							<fo:block font-family="universcbold">
								UN NUOVO PARTNER INDUSTRIALE E UN MONDO DI NUOVE OFFERTE ENERGIT: ENERGIA ELETTRICA, GAS, IMPIANTI FOTOVOLTAICI E MOLTO DI PIU’
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Dal 23/11/2012 <fo:inline font-family="universcbold">Energit ha un nuovo partner industriale: Onda Energia!</fo:inline>
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Ai nostri Clienti garantiremo la fornitura senza interruzioni,
								il mantenimento delle condizioni contrattuali e… tante occasioni di risparmio.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								<fo:inline font-family="universcbold">Per scoprire in anteprima le nuove offerte
								che le abbiamo dedicato chiami l’800.19.22.22</fo:inline>; non appena la voce guida richiederà l’inserimento
								di un <fo:inline font-family="universcbold">codice promozione</fo:inline>,
								digiti <fo:inline font-family="universcbold">50</fo:inline> sulla tastiera del telefono.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								La aspettiamo!
							</fo:block>
						</xsl:if>

						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_BANCA_CARIGE'] and
									  not(PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='NUOVO_PARTNER_INDUSTRIALE')">
							<fo:block font-family="universcbold">
								NUOVI RIFERIMENTI BANCARI PER IL PAGAMENTO DELLE FATTURE
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								La invitiamo ad utilizzare il <fo:inline font-family="universcbold">nuovo conto corrente
								bancario Energit</fo:inline> per il pagamento delle fatture.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energ.it S.p.A. <fo:inline font-family="universcbold">Codice IBAN
								<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_BANCA_CARIGE']/child::ID='COMUNICAZIONE_BANCA_CARIGE_V1'">IT43I0617516901000000790980</xsl:if>
								<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_BANCA_CARIGE']/child::ID='COMUNICAZIONE_BANCA_CARIGE_V2'">IT22X0343116901000000790980</xsl:if>
								</fo:inline>. Banca Carige Italia S.p.A.
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Grazie!
							</fo:block>
						</xsl:if>

						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='SERV_PAPERLESS_FRONTESPIZIO']/child::ID='PAPERLESS_V4' and
									  not(PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='NUOVO_PARTNER_INDUSTRIALE')">
							<fo:block font-family="universcbold">
								PASSI ALLA FATTURA VIA EMAIL: RISPARMIERA’ FINO A 18 EURO ALL’ANNO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								<fo:inline font-family="universcbold">Scelga di ricevere la fattura via email:
								risparmierà le spese di spedizione del documento cartaceo</fo:inline>, pari ad
								1,50 € per fattura. Fino a 18 euro all'anno!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								La fattura via email, inoltre, rispetta l’ambiente, arriva tempestivamente ed è a colori!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Invii la sua richiesta a <fo:inline font-family="universcbold">paperless@energit.it</fo:inline> e
								specificando il suo codice cliente <xsl:value-of select="./child::CODICE_CLIENTE" />.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_FATTURA_INTEGRATIVA']/child::ID='COMUNICAZIONE_FATTURA_INTEGRATIVA_V1'">
							<fo:block space-before="2mm">
								<fo:inline font-family="universcbold">INFORMAZIONE IMPORTANTE</fo:inline>. Il presente
								documento si intende ad integrazione della precedente fattura,
								emessa in data 11/11/2012. Ci scusiamo per l’inconveniente,
								dovuto a cause tecniche, e la ringraziamo per aver scelto Energit.
							</fo:block>
						</xsl:if>
						
						<!-- TOP6000 -->
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE91'
									 ">
							<fo:block font-family="universcbold">
								PER LEI, GLI ESCLUSIVI SCONTI TOP6000 CHE AUMENTANO NEL TEMPO: LI RICHIEDA SUBITO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato
								gli <fo:inline font-family="universcbold">EXTRA SCONTI TOP6000, del valore di 3-6-9 euro/MWh;
								per ottenerli deve solo richiederli!</fo:inline> Non perda questa occasione: trova maggiori
								informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE92'
									 ">
							<fo:block font-family="universcbold">
								PER LEI, GLI ESCLUSIVI SCONTI TOP6000 CHE AUMENTANO NEL TEMPO: SCOPRA COME OTTENERLI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit desidera offrirle
								gli <fo:inline font-family="universcbold">EXTRA SCONTI TOP6000, del valore
								di 3-6-9 euro/MWh</fo:inline> e riservati ai Clienti in regola con i pagamenti:
								non perda questa opportunità! Trova tutti i dettagli sull’iniziativa nella
								sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE111'
									 ">
							<fo:block font-family="universcbold">
								INFORMAZIONE IMPORTANTE
							</fo:block>
							<fo:block font-family="universcbold">
								ULTIMI GIORNI PER RICHIEDERE GLI ESCLUSIVI SCONTI TOP6000 CHE AUMENTANO NEL TEMPO: SI AFFRETTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato gli <fo:inline font-family="universcbold">EXTRA SCONTI TOP6000, del valore
								di 3-6-9 euro/MWh; per ottenerli deve solo richiederli entro il 31 luglio 2012!</fo:inline> Non perda questa occasione:
								trova maggiori informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<!-- EnergiTOP -->
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE128'
									 ">
							<fo:block font-family="universcbold">
								ENERGITOP LE OFFRE 3+6+12 EURO DI EXTRA SCONTO SULL’ENERGIA. SCELGA IL RISPARMIO CHE RADDOPPIA NEL TEMPO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato
								gli <fo:inline font-family="universcbold">EXTRA SCONTI ENERGITOP, del valore di 3+6+12  euro/MWh;
								per ottenerli deve solo richiederli!</fo:inline> Non perda questa occasione: trova maggiori
								informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE129'
									 ">
							<fo:block font-family="universcbold">
								ENERGITOP LE OFFRE 3+6+12 EURO DI EXTRA SCONTO SULL’ENERGIA. SCELGA IL RISPARMIO CHE RADDOPPIA NEL TEMPO!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit desidera offrirle
								gli <fo:inline font-family="universcbold">EXTRA SCONTI ENERGITOP, del valore di 3+6+12
								euro/MWh</fo:inline> e riservati ai Clienti in regola con i pagamenti: non perda questa
								opportunità! Trova tutti i dettagli sull’iniziativa nella sezione “Comunicazioni ai Clienti” di
								questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE172'
									 ">
							<fo:block font-family="universcbold">
								CON MAXIBONUS ENERGIT LE OFFRE 45 euro DI EXTRA SCONTO: NE APPROFITTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit desidera offrirle
								gli <fo:inline font-family="universcbold">EXTRA SCONTI MAXIBONUS, per un totale di 45 euro</fo:inline> e riservati
								ai Clienti in regola con i pagamenti: non perda questa opportunità! Trova tutti i dettagli sull’iniziativa nella
								sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE173'
									 ">
							<fo:block font-family="universcbold">
								CON MAXIBONUS ENERGIT LE OFFRE 45 euro DI EXTRA SCONTO: NE APPROFITTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Energit le ha riservato gli 
								<fo:inline font-family="universcbold">EXTRA SCONTI MAXIBONUS, per un totale di 45 euro; per ottenerli deve solo richiederli!</fo:inline> Non
								perda questa occasione: trova maggiori informazioni nella sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE174'
									 ">
							<fo:block font-family="universcbold">
								COMPLIMENTI, LEI RICEVE GLI EXTRA SCONTI MAXIBONUS, RISERVATI AI MIGLIORI CLIENTI!
							</fo:block>
							<fo:block text-align="justify" keep-with-previous="always">
								Per maggiori informazioni consulti la sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
						
						<xsl:if test="not(PROMO_SERVIZI[@TIPOLOGIA='COMUNICAZIONE_EXNOI']/child::ID='EXNOI_V3') and
									  PROMO_SERVIZI[@TIPOLOGIA='PROMOZIONI_TOP']/child::ID='COMUNICAZIONE193'
									 ">
							<fo:block font-family="universcbold">
								COMPLIMENTI, LEI RICEVE GLI EXTRA SCONTI MAXIBONUS, RISERVATI AI MIGLIORI CLIENTI!
							</fo:block>

							<fo:block text-align="justify" keep-with-previous="always">
								Per maggiori informazioni consulti la sezione “Comunicazioni ai Clienti” di questa fattura.
							</fo:block>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<!-- Frasi SDD ERRATO/REVOCATO - Deposito Cauzionale -->
			<fo:table-row height="11mm">
				<fo:table-cell>
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO='Addebito tramite SDD')">
							<xsl:choose>
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
									<fo:block font-family="universcbold">
										ATTENZIONE! La sua Banca ha respinto la richiesta di attivazione dell'addebito diretto SEPA Direct Debit (SDD).
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>
								
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_REVOCATO']">
									<fo:block font-family="universcbold">
										Riattivi subito l'addebito diretto SEPA Direct Debit (SDD) per evitare il pagamento del deposito cauzionale!
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>
								
								<xsl:otherwise>
									<xsl:if test="@PRESENZA_DEPOSITO_CAUZIONALE='SI'">
										<fo:block font-family="universcbold">
											Rientri in possesso del deposito cauzionale passando all'addebito diretto SEPA Direct Debit (SDD)!
										</fo:block>
										<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
									</xsl:if>  
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
						
			<!-- Avviso fatture pagate/in attesa di pagamento -->
			<fo:table-row height="14mm">
				<fo:table-cell border-bottom="0.5 dashed thick black">
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc" font-size="9pt">
						<xsl:if test="not(./child::FATTURE_ATTESA_PAGAMENTO) or (./child::FATTURE_ATTESA_PAGAMENTO and (count(./child::FATTURE_ATTESA_PAGAMENTO) &lt; 1))">
							<fo:block font-family="universcbold">
								TUTTE LE FATTURE PRECEDENTI RISULTANO PAGATE. GRAZIE.
							</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7.5pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								<fo:block font-size="7.5pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
						
						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO)=1">
							<fo:block font-family="universcbold">
								ATTENZIONE: 1 FATTURA RISULTA IN ATTESA DI PAGAMENTO, PER UN TOTALE INSOLUTO DI EURO <xsl:value-of select="./child::TOTALE_FATTURE_ATTESA_PAGAMENTO" />.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7.5pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								<fo:block font-size="7.5pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
						
						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO) &gt; 1">
							<fo:block font-family="universcbold">
								ATTENZIONE: <xsl:value-of select="count(./child::FATTURE_ATTESA_PAGAMENTO)"/> FATTURE RISULTANO IN ATTESA DI PAGAMENTO, PER UN TOTALE INSOLUTO DI EURO <xsl:value-of select="./child::TOTALE_FATTURE_ATTESA_PAGAMENTO" />.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="7.5pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" />, che include i
								contratti indicati nel “Riepilogo fattura” riportato alla fine del documento.</fo:block>
								<fo:block font-size="7.5pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>  
		</fo:table-body>
	</fo:table>
</xsl:template>





<!--
**********************
**  Dati anagrafici **
**********************
-->
<xsl:template name="DATI_ANAGRAFICI">
	<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.004 chr.009">
		<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">   
			<fo:table-row>
				<fo:table-cell>
					<fo:block xsl:use-attribute-sets="chrbold.009">
						Cliente
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INTESTATARIO" /></fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INDIRIZZO" /></fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row border-bottom="0.5 dashed thick black">
				<fo:table-cell>
					<fo:block>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CITTA" />
						<xsl:if test="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA">
							<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA" /></fo:inline>
						</xsl:if>
						<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::NAZIONE='IT')">
							<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::STATO" /></fo:inline>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell padding-top="1mm">
					<fo:block>
						<fo:inline xsl:use-attribute-sets="chrbold.009">
							Partita IVA
						</fo:inline>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell>
					<fo:block>
						<fo:inline xsl:use-attribute-sets="chrbold.009">
							Codice Fiscale
						</fo:inline>
						<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_FISCALE" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<xsl:if test="@SPOT='NO' or not(@SPOT)">
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:inline xsl:use-attribute-sets="chrbold.009">
								UserID
							</fo:inline>
							<xsl:value-of select="./child::USERNAME" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
			
			<!-- <xsl:if test="./child::CIG">

				<xsl:if test="@SPOT='NO' or not(@SPOT)">
					<fo:table-row>
						<fo:table-cell>
							<fo:block>
								<fo:inline xsl:use-attribute-sets="chrbold.009">
									Codice CIG
								</fo:inline>
								<xsl:value-of select="./child::CIG" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</xsl:if> -->
		</fo:table-body>
	</fo:table>
</xsl:template>





<!--
**********************************************
**  Riquadro fattura con bordi arrotondati  **
**********************************************
-->
<xsl:template name="BOX_SX">
	<xsl:param name="color"/>
	<xsl:param name="bs"/>
	<xsl:param name="disagio_economico"/>
	<fo:block font-family="universcbold" font-size="10pt">CLIENTE</fo:block>
	<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INTESTATARIO" /></fo:block>
	<fo:block><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::INDIRIZZO" /><xsl:text>&#160;</xsl:text><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CITTA" /> (<xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PROVINCIA" />)</fo:block>
	<xsl:if test="./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA">
		<fo:block><fo:inline font-family="universcbold">PARTITA IVA: </fo:inline><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA" /></fo:block>
	</xsl:if>
	<xsl:if test="not(./child::ANAGRAFICA_DOCUMENTO/child::PARTITA_IVA)">
		<fo:block><fo:inline font-family="universcbold">CODICE FISCALE: </fo:inline><xsl:value-of select="./child::ANAGRAFICA_DOCUMENTO/child::CODICE_FISCALE" /></fo:block>
	</xsl:if>
	<fo:block><fo:inline font-family="universcbold">USERNAME: </fo:inline><xsl:value-of select="./child::USERNAME" /></fo:block>
	
	<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
	
	<xsl:call-template name="PAGAMENTI"/>
	
	<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
	
	<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">AGGIORNAMENTI TARIFFARI ARERA</fo:block>
	<!-- <xsl:variable name="status_piu_uno"><xsl:value-of select="concat(substring(./DATA_DOCUMENTO,7,4),substring(./DATA_DOCUMENTO,4,2))"/></xsl:variable>-->
	<xsl:variable name="dataDoc"><xsl:value-of select="concat(substring(DATA_DOCUMENTO,7,4),substring(DATA_DOCUMENTO,4,2),substring(DATA_DOCUMENTO,1,2))"/></xsl:variable>

	 <!-- COMMENTATO PER EMERGENZA BILLOUT 7094 DECOMMENTARE E TESTARE!!!" -->
	 <xsl:choose>
<!--	 	<xsl:when test="$dataDoc &gt;= 20180701 and $status_piu_uno &lt; 201808">
			<fo:block>Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia, Reti e Ambiente (ARERA) ai sensi delle delibere n. ARG/gas 64/09, 229/2012/R/gas, 775/2016/R/gas e 402/2013/R/com, soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al secondo trimestre 2018, per effetto delle delibere 172/2018/R/COM e 189/2018/R/gas. Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale”.</fo:block>	
	 	</xsl:when>
	 	<xsl:when test="$status_piu_uno &gt;= 20180801 and $status_piu_uno &lt; 201810">
			<fo:block>Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia, Reti e Ambiente (ARERA) ai sensi delle delibere n. ARG/ gas 64/09, 229/2012/R/gas, 775/2016/R/gas e 402/2013/R/com, soggette ad aggiornamento periodico trimestrale. L’ultimo  aggiornamento si riferisce al terzo trimestre 2018, per effetto delle delibere 365/2018/R/gas e 359/2018/R/com. Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale”.</fo:block>
	 	</xsl:when> -->
<!--	 	<xsl:when test="$status_piu_uno &gt; 20181001 and $status_piu_uno &lt; 20190401"> 
 	 		<fo:block>Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia, Reti e Ambiente (ARERA) ai sensi delle delibere n. ARG/ gas 64/09, 229/2012/R/gas, 775/2016/R/gas e 402/2013/R/com, soggette ad aggiornamento periodico trimestrale. L’ultimo  aggiornamento si riferisce al quarto trimestre 2018, per effetto delle delibere 477/2018/R/gas e 475/2018/R/com. Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale”.</fo:block> 
		</xsl:when> -->
		<xsl:when test="$dataDoc &gt;= 20190201 and $dataDoc &lt; 20190501">
			<fo:block>Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia, Reti e Ambiente (ARERA) ai sensi delle delibere n. 667/2018/R/gas, 707/2018/R/gas,709/2018/R/gas e 711/2018/R/com, soggette ad aggiornamento periodico trimestrale. L’ultimo  aggiornamento si riferisce al primo trimestre 2019. Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it – sezione “Aggiornamento Trimestrale”.</fo:block>
		</xsl:when>
 		<xsl:when test="$dataDoc &gt;= 20190501 and $dataDoc &lt; 20190801">
			<fo:block>Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia, Reti e Ambiente (ARERA) ai sensi delle delibere n. ARG/ gas 64/09, 229/2012/R/gas, 775/2016/R/gas e 402/2013/R/com, soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al secondo trimestre 2019, per effetto delle delibere 107/2019/R/com e 108/2018/R/gas. Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it –sezione “Aggiornamento Trimestrale”</fo:block>
	    </xsl:when>
 		<xsl:when test="$dataDoc &gt;= 20190801 and $dataDoc &lt; 20191101">
			<fo:block>Questa bolletta recepisce le condizioni economiche fissate dall’Autorità di Regolazione per Energia, Reti e Ambiente (ARERA) ai sensi delle delibere n. ARG/ gas 64/09, 229/2012/R/gas, 775/2016/R/gas e 402/2013/R/com, soggette ad aggiornamento periodico trimestrale. L’ultimo aggiornamento si riferisce al terzo trimestre 2019, per effetto delle delibere 264/2019/R/gas e 262/2019/R/com. Per maggiori informazioni e dettagli, visita il sito dell’ARERA all’indirizzo www.arera.it –sezione “Aggiornamento Trimestrale”</fo:block>
	    </xsl:when>		
		<xsl:otherwise>
			<fo:block></fo:block>
		</xsl:otherwise>	
	</xsl:choose> 
	<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
	
	<xsl:call-template name="LETTURA_AUTOLETTURA_FRONTESPIZIO"/>
	
	<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
	
	<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">ENERGIT INFORMA</fo:block>
	<fo:block>A  partire dal 01 Giugno 2018 Energit ha aggiornato la grafica della prima pagina della tua bolletta di gas naturale, sempre in conformità con le disposizioni normative imposte dall’Autorità di Regolazione per l’Energia, Reti e Ambiente  di cui alla delibera  n. 501/2014/R/com. Obiettivo di questa innovazione è quella di offrirti una bolletta sempre più chiara, trasparente e facile da comprendere.</fo:block>
	
	<xsl:if test="not($bs = 0)">
		<fo:block space-before="3mm" border-bottom="0.5 dashed black"/>
		
		<fo:block space-before="3mm" font-family="universcbold" font-size="10pt">BONUS ELETTRICO</fo:block>
		<fo:block>La sua fornitura è ammessa alla compensazione della spesa per la
		fornitura di energia elettrica ai sensi del decreto 28 dicembre 2007
		(cosiddetto bonus sociale elettrico).</fo:block>
		<!-- <xsl:if test="$disagio_economico = 1">
			<fo:block>La richiesta di rinnovo deve essere effettuata entro mese/anno</fo:block>
		</xsl:if> -->
	</xsl:if>

	
</xsl:template>





<!--
***********************************
**  Indirizzo spedizione fattura **
***********************************
-->
<xsl:template name="SPEDIZIONE_FATTURA">
	<fo:block xsl:use-attribute-sets="blocco_indirizzo_spedizione" color="black">
		<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INTESTATARIO" /></fo:block>

		<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::INDIRIZZO" /></fo:block>

		<fo:block>
			<xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CAP" /><xsl:text>&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::CITTA" />
			<xsl:if test="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA">
				<fo:inline><xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::PROVINCIA" /></fo:inline>
			</xsl:if>
		</fo:block>

		<xsl:if test="not(./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::NAZIONE='IT')">
			<fo:block><xsl:value-of select="./child::INDIRIZZO_SPEDIZIONE_DOCUMENTO/child::STATO" /></fo:block>
		</xsl:if>
	</fo:block>
</xsl:template>





<!--
*********************************
**  Riepilogo importi fattura  **
*********************************
-->
<xsl:template name="DETTAGLIO_IVA">
	<fo:table space-before="10mm" start-indent="3.5mm" table-layout="fixed" width="100%" display-align="center">
	<fo:table-column column-width="21mm"/>
	<fo:table-column column-width="21mm"/>
	<fo:table-column column-width="21mm"/>
	<fo:table-column column-width="21mm"/>
	<fo:table-column column-width="21mm"/>
	<fo:table-body end-indent="0pt" start-indent="0pt">
		<fo:table-row font-family="universcbold" border-bottom="0.5 solid black" text-align="start">
			<fo:table-cell number-columns-spanned="5">
				<fo:block>DETTAGLIO IVA</fo:block>
			</fo:table-cell>
		</fo:table-row>        
		
		<fo:table-row font-family="universcbold" display-align="center">                            
			<fo:table-cell text-align="start" padding-left="2mm">
				<fo:block>Imponibile</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>Aliquota</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>Codice IVA</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>Importo IVA</fo:block>
			</fo:table-cell>
			
			<!-- <fo:table-cell text-align="end" padding-right="2mm">
				<fo:block>Totale</fo:block>
			</fo:table-cell> -->
		</fo:table-row>
		
		

		
		<xsl:for-each select="./child::DETTAGLIO_FATTURA">       
			<xsl:sort select="DESCRIZIONE_ALIQUOTA_IVA"/>
			<fo:table-row display-align="center">
				
				<fo:table-cell text-align="start" padding-left="2mm">
					<fo:block><xsl:value-of select="./child::IMPONIBILE" /></fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::ALIQUOTA_IVA" />%</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::CODICE_IVA" /></fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block><xsl:value-of select="./child::IMPORTO_IVA" /></fo:block>
				</fo:table-cell>
				
				<!-- <fo:table-cell text-align="end" padding-right="2mm">
					<fo:block><xsl:value-of select="./child::TOTALE" /></fo:block>
				</fo:table-cell> -->
			</fo:table-row> 
		</xsl:for-each>
		
		<!--  aggiungere totale -->

		
		<fo:table-row  border-bottom="0.5 solid black">
			<fo:table-cell number-columns-spanned="5">
				<fo:block/>
			</fo:table-cell>
		</fo:table-row> 
		
		<fo:table-row display-align="center">
				
				<fo:table-cell text-align="start" padding-left="2mm">
					<fo:block></fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block></fo:block>
				</fo:table-cell>
				
				<fo:table-cell >
					<fo:block >
						<fo:inline font-family="universcbold" font-style="bold">Totale:
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
 				<fo:table-cell >
					<fo:block>
						<fo:inline font-family="universcbold" font-style="bold">
							<xsl:choose>
								<xsl:when test="./child::SOMMA_IMPORTO_IVA_DETTAGLI_FATTURA">
									<xsl:value-of select="translate(format-number(./child::SOMMA_IMPORTO_IVA_DETTAGLI_FATTURA,'##.####'), '.' , ',' )"/>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell>
					<fo:block></fo:block>
				</fo:table-cell>
				
				<!-- <fo:table-cell text-align="end" padding-right="2mm">
					<fo:block><xsl:value-of select="./child::TOTALE" /></fo:block>
				</fo:table-cell> -->
				
		</fo:table-row> 



				<xsl:for-each select="./child::DESCRIZIONI_CODICI_IVA">
					<fo:table-row font-family="universc" display-align="center" font-size="6pt">
						<fo:table-cell number-columns-spanned="2" text-align="start">
							<fo:block>
								Codice IVA "<xsl:value-of select="./child::CODICE_IVA" />"
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="3" text-align="start">
							<fo:block>
								<xsl:value-of select="DESCRIZIONE_CODICE_IVA" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
		
	</fo:table-body>
	</fo:table>
</xsl:template>







<!-- Avviso fatture pagate/in attesa di pagamento -->
<xsl:template name="STATO_PAGAMENTI">
	<fo:table start-indent="3.5mm" table-layout="fixed" width="100%" display-align="center" text-align="start">
	<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row>
				<fo:table-cell padding-left="2mm">
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block>
						<xsl:if test="not(./child::FATTURE_ATTESA_PAGAMENTO) or (./child::FATTURE_ATTESA_PAGAMENTO and (count(./child::FATTURE_ATTESA_PAGAMENTO) &lt; 1))">
							<fo:block font-family="universcbold">
								TUTTE LE FATTURE PRECEDENTI RISULTANO PAGATE. GRAZIE.
							</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="6pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" /> e i relativi contratti.</fo:block>
								<fo:block font-size="6pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
						
						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO)=1">
							<fo:block font-family="universcbold">
								ATTENZIONE: 1 FATTURA RISULTA IN ATTESA DI PAGAMENTO, PER UN TOTALE INSOLUTO DI EURO <xsl:value-of select="./child::TOTALE_FATTURE_ATTESA_PAGAMENTO" />.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="6pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" /> e i relativi contratti.</fo:block>
								<fo:block font-size="6pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
						
						<xsl:if test="count(./child::FATTURE_ATTESA_PAGAMENTO) &gt; 1">
							<fo:block font-family="universcbold">
								ATTENZIONE: <xsl:value-of select="count(./child::FATTURE_ATTESA_PAGAMENTO)"/> FATTURE RISULTANO IN ATTESA DI PAGAMENTO, PER UN TOTALE INSOLUTO DI EURO <xsl:value-of select="./child::TOTALE_FATTURE_ATTESA_PAGAMENTO" />.
							</fo:block>
							<fo:block>Se ha gia' provveduto, invii copia del pagamento a gestionecrediti@energit.it o via fax al n.800.19.22.11</fo:block>
							
							<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='MULTI_INVOICE']/child::ID='MULTI_INVOICE_V1'">
								<fo:block font-size="6pt">L’informazione sullo stato dei pagamenti riguarda le fatture emesse per il codice
								cliente <xsl:value-of select="./child::CODICE_CLIENTE" />-<xsl:value-of select="./child::INVOICE_NO" /> e i relativi contratti.</fo:block>
								<fo:block font-size="6pt">La invitiamo a verificare la situazione dei pagamenti
								riguardanti gli altri contratti sottoscritti con Energit nelle relative fatture.</fo:block>
							</xsl:if>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>  
		</fo:table-body>
	</fo:table>
</xsl:template>

<!-- Frasi SDD ERRATO/REVOCATO - Deposito Cauzionale -->
<xsl:template name="INFO_RID">
	<fo:table start-indent="3.5mm" space-before="3mm" table-layout="fixed" width="100%" display-align="center" text-align="start">
	<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row>
				<fo:table-cell padding-left="2mm">
					<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
					<fo:block font-family="universc">
						<xsl:if test="not(./child::MODALITA_PAGAMENTO_DOCUMENTO/child::MODALITA_PAGAMENTO='Addebito tramite SDD')">
							<xsl:choose>
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_ERRATO']">
									<fo:block font-family="universcbold">
										ATTENZIONE! La sua Banca ha respinto la richiesta di attivazione dell'addebito diretto SEPA Direct Debit (SDD).
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>
								
								<xsl:when test="PROMO_SERVIZI[@TIPOLOGIA='SDD_REVOCATO']">
									<fo:block font-family="universcbold">
										Riattivi subito l'addebito diretto SEPA Direct Debit (SDD) per evitare il pagamento del deposito cauzionale!
									</fo:block>
									<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
								</xsl:when>
								
								<xsl:otherwise>
									<xsl:if test="@PRESENZA_DEPOSITO_CAUZIONALE='SI'">
										<fo:block font-family="universcbold">
											Rientri in possesso del deposito cauzionale passando all'addebito diretto SEPA Direct Debit (SDD)!
										</fo:block>
										<fo:block>Trova maggiori informazioni insieme al bollettino postale allegato a questa fattura.</fo:block>
									</xsl:if>  
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
</xsl:template>



<!-- Altre comunicazioni importanti  -->
<xsl:template name="ALTRE_COMUNICAZIONI_FRONTESPIZIO">
	
	<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='CORRISPETTIVO_ERRATO'">
		<fo:table start-indent="3.5mm" space-before="3mm" table-layout="fixed" width="100%" display-align="center" text-align="start">
		<fo:table-column column-width="proportional-column-width(100)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<!-- <xsl:attribute name="border">0.5pt solid black</xsl:attribute> -->
						<fo:block font-family="universc">
							<fo:inline font-family="universcbold">ATTENZIONE</fo:inline>: Gentile cliente, in questa fattura troverà il ricalcolo dei primi mesi del 2016 per una variazione a suo favore di alcuni corrispettivi di trasporto.
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:if>
	
	<xsl:if test="PROMO_SERVIZI[@TIPOLOGIA='ALTRE_COMUNICAZIONI']/child::ID='CANONE_RAI'">
		<fo:table start-indent="3.5mm" space-before="3mm" table-layout="fixed" width="100%" display-align="center" text-align="start">
		<fo:table-column column-width="proportional-column-width(100)"/>
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block font-family="universc">
							<fo:inline font-family="universcbold">ATTENZIONE</fo:inline>: Gentile cliente, in questa fattura troverà l'addebito del canone RAI.
							I dati personali raccolti per la fornitura dell'energia elettrica sono utilizzati, in base alla tipologia di cliente domestico residente, 
							anche ai fini dell'individuazione dell'intestatario del canone di abbonamento e del relativo addebito contestuale alla bolletta, che, in 
							caso di cliente domestico residente con potenza impegnata fino a 3kW (tariffa D2 della spesa per il trasporto e la gestione del contatore), 
							avverrà senza ulteriori verifiche sulla residenza.
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:if>	
	
</xsl:template>
















<xsl:template name="DETTAGLIO_IMPORTI">
	<xsl:param name="bordi"/>
	<xsl:param name="consumi_fatturati"/>
	<xsl:param name="imponibile_no_altrepartite"/>
	<xsl:param name="imponibile_sv"/>
	<xsl:param name="sv"/>
	<xsl:param name="sr"/>
	<xsl:param name="imp"/>
	<xsl:param name="iva"/>
	<xsl:param name="odiv"/>
	<xsl:param name="bs"/>
	<xsl:param name="doc"/>
	<xsl:param name="costo_medio"/>
	<xsl:param name="costo_medio_sv"/>
	
	
	<fo:table space-before="6mm" text-align="start" font-family="universc" font-size="6pt" end-indent="0pt" start-indent="3.5mm" table-layout="fixed" width="100%">
	<fo:table-column column-width="proportional-column-width(100)"/>
		<fo:table-body end-indent="0pt" start-indent="0pt">
			<fo:table-row>
				<fo:table-cell>
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_bold.svg)</xsl:attribute>
					</xsl:if>
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="55%">
						
						<!--  <fo:table-column column-width="proportional-column-width(28.5)"/>
						<fo:table-column column-width="proportional-column-width(10)"/>
						<fo:table-column column-width="proportional-column-width(41)"/>
						<fo:table-column column-width="proportional-column-width(20.5)"/> -->
						
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						
						<fo:table-body end-indent="0pt" start-indent="0pt">
							<fo:table-row>
								<fo:table-cell padding-left="2mm" display-align="after">
									<fo:block>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell display-align="after">
									<fo:block font-family="universccors">
										Euro
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell display-align="after">
									<fo:block font-family="universccors">
										(%)
									</fo:block>
								</fo:table-cell>
							</fo:table-row>

							<xsl:choose>
								<xsl:when test="not($sv = 0.0)">							
									<fo:table-row height="4mm">
			
												<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
													<fo:block>
													<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/pattern01.svg" /></fo:inline>
														<fo:inline padding-left="2mm">TOTALE SERVIZI DI VENDITA</fo:inline>
													</fo:block>
												</fo:table-cell>
		
												<fo:table-cell border-bottom="0.5 solid black" display-align="after">
												<fo:block>
											<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
												<xsl:choose>
													<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<xsl:variable name="sv"><xsl:value-of select="concat(substring-before(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA, ','),substring-after(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA, ','))"/></xsl:variable>
														<fo:inline>
															<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_VENDITA"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											
											<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
												<xsl:choose>
													<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<xsl:variable name="sv"><xsl:value-of select="concat(substring-before(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA, ','),substring-after(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA, ','))"/></xsl:variable>
														<fo:inline>
															<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if> -->
												<xsl:value-of select="translate($sv div 100,'.',',')"/>
											</fo:block>
										</fo:table-cell>
									
										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block font-family="universccors">
												<xsl:if test="round($sv div $doc * 10000) div 100 = 0">
													(0)%
												</xsl:if>
												<xsl:if test="not(round($sv div $doc * 10000) div 100 = 0)">
													<!-- (<xsl:value-of select="concat(substring-before(round(math:abs($sv) div $doc * 10000) div 100,'.'),',',substring-after(round(math:abs($sv) div $doc * 10000) div 100,'.'))"/>%) -->
													(<xsl:value-of select="translate(round(math:abs($sv) div $doc * 10000) div 100,'.',',')"/>%)
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
							</xsl:when>
								<xsl:otherwise>
	 							</xsl:otherwise>
						   </xsl:choose>
							
							<xsl:choose>
								<xsl:when test="not($sr = 0.0)">
									<fo:table-row height="6mm">
										<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
											<fo:block>
												<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/pattern02.svg" /></fo:inline>
												<fo:inline padding-left="2mm">TOTALE SERVIZI DI RETE</fo:inline>
											</fo:block>
										</fo:table-cell>
										
										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block>
												<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
													<xsl:choose>
														<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS='0,00'">
															<fo:inline>-</fo:inline>
														</xsl:when>
														<xsl:otherwise>
															<xsl:variable name="sr"><xsl:value-of select="concat(substring-before(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS, ','),substring-after(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS, ','))"/></xsl:variable>
															<fo:inline>
																<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS"/>
															</fo:inline>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if>
												
												<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
													<xsl:choose>
														<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS='0,00'">
															<fo:inline>-</fo:inline>
														</xsl:when>
														<xsl:otherwise>
															<xsl:variable name="sr"><xsl:value-of select="concat(substring-before(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS, ','),substring-after(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS, ','))"/></xsl:variable>
															<fo:inline>
																<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE_SENZA_BONUS"/>
															</fo:inline>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if> -->
												<xsl:value-of select="translate($sr div 100,'.',',')"/>
											</fo:block>
										</fo:table-cell>
										
										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block font-family="universccors">
												<xsl:if test="round($sr div $doc * 10000) div 100 = 0">
													(0)%
												</xsl:if>
												<xsl:if test="not(round($sr div $doc * 10000) div 100 = 0)">
													<!-- (<xsl:value-of select="concat(substring-before(round(math:abs($sr) div $doc * 10000) div 100,'.'),',',substring-after(round(math:abs($sr) div $doc * 10000) div 100,'.'))"/>%) -->
													(<xsl:value-of select="translate(round(math:abs($sr) div $doc * 10000) div 100,'.',',')"/>%)
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
							
							<xsl:choose>
								<xsl:when test="not($imp = 0.0)">
									<fo:table-row height="6mm">
											<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
													<fo:block>
														<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/pattern03.svg" /></fo:inline>
														<fo:inline padding-left="2mm">TOTALE IMPOSTE</fo:inline>
													</fo:block>
												</fo:table-cell>
										
										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block>
												<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
													<xsl:choose>
														<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE='0,00'">
															<fo:inline>-</fo:inline>
														</xsl:when>
														<xsl:otherwise>
															<xsl:variable name="imp"><xsl:value-of select="concat(substring-before(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE, ','),substring-after(./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE, ','))"/></xsl:variable>
															<fo:inline>
																<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::TOTALE_IMPOSTE"/>
															</fo:inline>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if>
												
												<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
													<xsl:choose>
														<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE='0,00'">
															<fo:inline>-</fo:inline>
														</xsl:when>
														
														<xsl:otherwise>
															<xsl:variable name="imp"><xsl:value-of select="concat(substring-before(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE, ','),substring-after(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE, ','))"/></xsl:variable>
															<fo:inline>
																<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE"/>
															</fo:inline>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if> -->
												<xsl:value-of select="translate($imp div 100,'.',',')"/>
											</fo:block>
										</fo:table-cell>
										
										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block font-family="universccors">
												<xsl:if test="round($imp div $doc * 10000) div 100 = 0">
													(0)%
												</xsl:if>
												<xsl:if test="not(round($imp div $doc * 10000) div 100 = 0)">
													<!-- (<xsl:value-of select="concat(substring-before(round(math:abs($imp) div $doc * 10000) div 100,'.'),',',substring-after(round(math:abs($imp) div $doc * 10000) div 100,'.'))"/>%) -->
													(<xsl:value-of select="translate(round(math:abs($imp) div $doc * 10000) div 100,'.',',')"/>%)
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:when>
								<xsl:otherwise>

								</xsl:otherwise>
							</xsl:choose>
							
							<xsl:choose>
								<xsl:when test="not($iva = 0.0)">							
									<fo:table-row height="6mm">
										<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
											<fo:block>
												<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/pattern04.svg" /></fo:inline>
												<fo:inline padding-left="2mm">IVA </fo:inline><fo:inline font-size="4pt">(DETTAGLIO A SEGUIRE)</fo:inline>
											</fo:block>
										</fo:table-cell>
										
										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block>
												<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
													<xsl:choose>
														<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA='0,00'">
															<fo:inline>-</fo:inline>
														</xsl:when>
														<xsl:otherwise>
															<xsl:variable name="iva"><xsl:value-of select="concat(substring-before(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA, ','),substring-after(./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA, ','))"/></xsl:variable>
															<fo:inline>
																<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::IVA"/>
															</fo:inline>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if>
												
												<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
													<xsl:choose>
														<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA='0,00'">
															<fo:inline>-</fo:inline>
														</xsl:when>
														
														<xsl:otherwise>
															<xsl:variable name="iva"><xsl:value-of select="concat(substring-before(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA, ','),substring-after(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA, ','))"/></xsl:variable>
															<fo:inline>
																<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::IVA"/>
															</fo:inline>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if> -->
												<xsl:value-of select="translate($iva div 100,'.',',')"/>
											</fo:block>
										</fo:table-cell>
										
										<fo:table-cell border-bottom="0.5 solid black" display-align="after">
											<fo:block font-family="universccors">
												<xsl:if test="round($iva div $doc * 10000) div 100 = 0">
													(0)%
												</xsl:if>
												<xsl:if test="not(round($iva div $doc * 10000) div 100 = 0)">
													<!-- (<xsl:value-of select="concat(substring-before(round(math:abs($iva) div $doc * 10000) div 100,'.'),',',substring-after(round(math:abs($iva) div $doc * 10000) div 100,'.'))"/>%) -->
													(<xsl:value-of select="translate(round(math:abs($iva) div $doc * 10000) div 100,'.',',')"/>%)
												</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
						</xsl:choose>
						
						<xsl:choose>
							<xsl:when test="not($odiv = 0.0)">
								<fo:table-row height="6mm">
									<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/pattern05.svg" /></fo:inline>
											<fo:inline padding-left="2mm">ALTRE PARTITE</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
												<xsl:choose>
													<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<xsl:variable name="odiv"><xsl:value-of select="concat(substring-before(./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI, ','),substring-after(./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI, ','))"/></xsl:variable>
														<fo:inline>
															<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::ONERI_DIVERSI"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											
											<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
												<xsl:choose>
													<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::ONERI_DIVERSI='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													
													<xsl:otherwise>
														<xsl:variable name="odiv"><xsl:value-of select="concat(substring-before(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::ONERI_DIVERSI, ','),substring-after(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::ONERI_DIVERSI, ','))"/></xsl:variable>
														<fo:inline>
															<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::ONERI_DIVERSI"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if> -->
											<xsl:value-of select="translate($odiv div 100,'.',',')"/>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
										<fo:block font-family="universccors">
											<xsl:if test="round($odiv div $doc * 10000) div 100 = 0">
												(0)%
											</xsl:if>
											<xsl:if test="not(round($odiv div $doc * 10000) div 100 = 0)">
												<!-- (<xsl:value-of select="concat(substring-before(round(math:abs($odiv) div $doc * 10000) div 100,'.'),',',substring-after(round(math:abs($odiv) div $doc * 10000) div 100,'.'))"/>%) -->
												(<xsl:value-of select="translate(round(math:abs($odiv) div $doc * 10000) div 100,'.',',')"/>%)
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
							
							<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00') and not(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE='0,00') and (./child::CONTRATTO_GAS)">
								<fo:table-row height="6mm">
									<fo:table-cell padding-left="2mm" border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<fo:inline><fo:external-graphic content-height="scale-to-fit" content-width="scale-to-fit" height="3.5mm" width="3.5mm" src="img/pattern06.svg" /></fo:inline>
											<fo:inline padding-left="2mm">BONUS SOCIALE</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
										<fo:block>
											<!-- <xsl:if test="./child::RIEPILOGO_MULTISITO_ENERGIA">
												<xsl:choose>
													<xsl:when test="./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<xsl:variable name="bs"><xsl:value-of select="concat(substring-before(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE, ','),substring-after(./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE, ','))"/></xsl:variable>
														<fo:inline>
															<xsl:value-of select="./child::RIEPILOGO_MULTISITO_ENERGIA/child::BONUS_SOCIALE"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											
											<xsl:if test="not(./child::RIEPILOGO_MULTISITO_ENERGIA)">
												<xsl:choose>
													<xsl:when test="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE='0,00'">
														<fo:inline>-</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<xsl:variable name="bs"><xsl:value-of select="concat(substring-before(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE, ','),substring-after(./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE, ','))"/></xsl:variable>
														<fo:inline>
															<xsl:value-of select="./child::CONTRATTO_GAS/child::SINTESI_GAS/child::BONUS_SOCIALE"/>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if> -->
											<xsl:value-of select="translate($bs div 100,'.',',')"/>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell border-bottom="0.5 solid black" display-align="after">
									<fo:block font-family="universccors">
										<xsl:if test="round($bs div $doc * 10000) div 100 = 0">
											(0)%
										</xsl:if>
										<xsl:if test="not(round($bs div $doc * 10000) div 100 = 0)">
											<!-- (<xsl:value-of select="concat(substring-before(round(math:abs($bs) div $doc * 10000) div 100,'.'),',',substring-after(round(math:abs($bs) div $doc * 10000) div 100,'.'))"/>%) -->
											(<xsl:value-of select="translate(round(math:abs($bs) div $doc * 10000) div 100,'.',',')"/>%)
										</xsl:if>
									</fo:block>
								</fo:table-cell>
								</fo:table-row>
							</xsl:if>
													
							

							
							
							<fo:table-row font-family="universcbold" height="5mm"> <!--  10mm -->
								<fo:table-cell padding-left="2mm" border-bottom="1 solid black" display-align="after">
									<fo:block>
										<fo:inline>TOTALE DA PAGARE</fo:inline>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell number-columns-spanned="2" border-bottom="1 solid black" display-align="after">
									<fo:block>
										<xsl:value-of select="./child::IMPORTO_DOCUMENTO"/>
										<!-- <xsl:value-of select="translate($doc div 100,'.',',')"/> -->
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
  							 
<!--  							<fo:table-row font-family="universcbold" height="10mm">
											<fo:table-cell padding-left="2mm" display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell number-columns-spanned="2" display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
							</fo:table-row>  -->

							
							<xsl:choose>
								<xsl:when test="$sv = 0">
									  <fo:table-row font-family="universcbold" height="10mm">
											<fo:table-cell padding-left="2mm" display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
									  </fo:table-row>
								</xsl:when>	
								<xsl:otherwise></xsl:otherwise>
							</xsl:choose>
							<xsl:choose>
								<xsl:when test="$sr = 0">
									  <fo:table-row font-family="universcbold" height="10mm">
											<fo:table-cell padding-left="2mm" display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
									  </fo:table-row>
								</xsl:when>
								<xsl:otherwise></xsl:otherwise>
							</xsl:choose>
							<xsl:choose>
								<xsl:when test="$imp = 0">
									  <fo:table-row font-family="universcbold" height="10mm">
											<fo:table-cell padding-left="2mm" border-bottom="0.5" display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
									  </fo:table-row>
								</xsl:when>
								<xsl:otherwise></xsl:otherwise>  	     
							</xsl:choose>
							<xsl:choose>
								<xsl:when test="iva = 0">
									  <fo:table-row font-family="universcbold" height="10mm">
											<fo:table-cell padding-left="2mm" border-bottom="0.5" display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
									  </fo:table-row>
								</xsl:when>
								<xsl:otherwise></xsl:otherwise>
							</xsl:choose>
							<xsl:choose>
								<xsl:when test="$odiv = 0">
									  <fo:table-row font-family="universcbold" height="10mm">
											<fo:table-cell padding-left="2mm" border-bottom="0.5" display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  display-align="after">
												<fo:block>
												</fo:block>
											</fo:table-cell>
									  </fo:table-row>  	     
								</xsl:when>
								<xsl:otherwise></xsl:otherwise>
						</xsl:choose>	
	  
								 
<!-- 							<xsl:if test="$imponibile_sv &gt; 0 and $consumi_fatturati &gt; 0"> -->
								<fo:table-row height="3mm">
									<fo:table-cell number-columns-spanned="3" padding-left="2mm" display-align="after">
										<fo:block font-size="7pt">
											<!-- Prezzo medio servizi di vendita <xsl:value-of select="concat(substring-before($costo_medio_sv,'.'),',',substring-after($costo_medio_sv,'.'))"/> euro/Smc -->
											<!--  Prezzo medio servizi di vendita <xsl:value-of select="translate($costo_medio_sv,'.',',')"/> euro/Smc  -->
											Prezzo medio servizi di vendita <xsl:value-of select="$costo_medio_sv"/> euro/Smc
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
<!-- 							</xsl:if> -->
							
<!-- 							<xsl:if test="$imponibile_no_altrepartite &gt; 0 and $consumi_fatturati &gt; 0">  -->
								<fo:table-row>
									<fo:table-cell number-columns-spanned="3" padding-left="2mm" display-align="after">
										<fo:block font-size="7pt">
											<!-- Prezzo medio fornitura <xsl:value-of select="concat(substring-before($costo_medio_,'.'),',',substring-after($costo_medio,'.'))"/> euro/Smc -->
											Prezzo medio fornitura <xsl:value-of select="translate($costo_medio, '.' , ',') "/> euro/Smc
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
<!--							</xsl:if> -->
							
							

							
						</fo:table-body>
					</fo:table>
					
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
	
</xsl:template>	

<!-- </xsl:when>
<xsl:otherwise>
</xsl:otherwise>

</xsl:choose> -->

</xsl:stylesheet>
