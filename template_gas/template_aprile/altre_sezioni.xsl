<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
**************************************************************
**															**
**			Matching dei template							**
**				- SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']		**
**				- CONTRATTO_SERVIZI_VARI					**
**				- CONTRATTO_DEPOSITO_CAUZIONALE				**
**				- CONTRATTO_FONIA							**
**				- CONTRATTO_AREASERVER						**
**															**
**			(richiamano il template "altre_sezioni")		**
**															**
**************************************************************
-->
<xsl:template match="SEZIONE[@TIPOLOGIA='ONERI_DIVERSI']">
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:param name="ultima_sezione_reti"/>
	<xsl:param name="ultima_sezione_vendita"/>
	<xsl:param name="ultima_sezione_imposte"/>
	<xsl:if test="PERIODO_RIFERIMENTO">
		<xsl:call-template name="altre_sezioni">
			<xsl:with-param name="bordi" select="$bordi"/>
			<xsl:with-param name="sezione" select="'ONERI_DIVERSI'"/>
			<xsl:with-param name="color" select="$color"/>
			<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
			<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
			<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
			<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
			<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<xsl:template match="CONTRATTO_SERVIZI_VARI">
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:call-template name="altre_sezioni">
		<xsl:with-param name="bordi" select="$bordi"/>
		<xsl:with-param name="sezione" select="'SERVIZI_VARI'"/>
		<xsl:with-param name="color" select="$color"/>
		<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
			<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
	</xsl:call-template>
</xsl:template>

<xsl:template match="CONTRATTO_DEPOSITO_CAUZIONALE">
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:call-template name="altre_sezioni">
		<xsl:with-param name="bordi" select="$bordi"/>
		<xsl:with-param name="sezione" select="'DEPOSITO_CAUZIONALE'"/>
		<xsl:with-param name="color" select="$color"/>
		<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
		<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
	</xsl:call-template>
</xsl:template>

<xsl:template match="CONTRATTO_FONIA">
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:call-template name="altre_sezioni">
		<xsl:with-param name="bordi" select="$bordi"/>
		<xsl:with-param name="sezione" select="'FONIA'"/>
		<xsl:with-param name="color" select="$color"/>
		<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
		<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
	</xsl:call-template>
</xsl:template>

<xsl:template match="CONTRATTO_AREASERVER">
	<xsl:param name="bordi"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:call-template name="altre_sezioni">
		<xsl:with-param name="bordi" select="$bordi"/>
		<xsl:with-param name="sezione" select="'AREASERVER'"/>
		<xsl:with-param name="color" select="$color"/>
		<xsl:with-param name="color_titolo_dettaglio" select="$color_titolo_dettaglio"/>
		<xsl:with-param name="svg-sezioni" select="$svg-sezioni"/>
	</xsl:call-template>
</xsl:template>



<!--
**************************************************************
**															**
**			Template contenitore per template:				**
**				- GENERICO									**
**				- ONERI_DIVERSI								**
**															**
**************************************************************
-->
<xsl:template name="altre_sezioni">

	<xsl:param name="bordi"/>
	<xsl:param name="sezione"/>
	<xsl:param name="color"/>
	<xsl:param name="color_titolo_dettaglio"/>
	<xsl:param name="svg-sezioni"/>
	<xsl:param name="ultima_sezione_reti"/>
	<xsl:param name="ultima_sezione_vendita"/>
	<xsl:param name="ultima_sezione_imposte"/>
	
	<fo:table space-before="10mm" font-family="universc" font-size="8pt" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		<xsl:if test="not($ultima_sezione_reti='') or
					  not($ultima_sezione_vendita='') or
					  not($ultima_sezione_imposte='')">
			<xsl:attribute name="space-before">1mm</xsl:attribute>
		</xsl:if>
			
		<fo:table-column column-width="proportional-column-width(100)"/>
		<xsl:if test="not($sezione='ONERI_DIVERSI')">	
			<fo:table-header>
			
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_header_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
			
				<fo:table-row keep-with-next="always">
					<fo:table-cell xsl:use-attribute-sets="cell.settings">
						<!-- <xsl:attribute name="color"><xsl:value-of select="$color_titolo_dettaglio"/></xsl:attribute> -->
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:if test="not($bordi='NO')">
							<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="not($color='black')">
							<xsl:attribute name="background-image">url(svg/rectangle_table_color_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						<xsl:if test="$bordi='NO' and $color='black'">
							<xsl:attribute name="background-image">url(svg/rectangle_table_body_dettaglio.svg)</xsl:attribute>
						</xsl:if>
						
						<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
							<fo:table-column column-width="proportional-column-width(100)"/>
							<fo:table-body end-indent="0pt" start-indent="0pt">
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="pad.l.000">
										<fo:block xsl:use-attribute-sets="blk.003 chrtitolodettaglio">
											<xsl:value-of select="PRODOTTO_SERVIZIO" /> - SINTESI
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-next="always" height="0.5mm">
									<fo:table-cell border-bottom="0.5 dashed thick black">
										<fo:block>
											<fo:inline></fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-next="always" height="0.5mm">
									<fo:table-cell>
										<fo:block>
											<fo:inline></fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							
								<fo:table-row keep-with-next="always">
									<fo:table-cell xsl:use-attribute-sets="cell.settings">
										<fo:block xsl:use-attribute-sets="blk.010">
											<fo:inline font-family="universcbold">CONTRATTO N. </fo:inline>
											<fo:inline xsl:use-attribute-sets="chr.009"><xsl:value-of select="CONTRACT_NO" /></fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
							</fo:table-body>
						</fo:table>
						
						
						<!-- <fo:block xsl:use-attribute-sets="blk.003">
							<fo:inline xsl:use-attribute-sets="chrtitolodettaglio">
								<xsl:value-of select="PRODOTTO_SERVIZIO" /> - SINTESI
							</fo:inline>
						</fo:block> -->
					</fo:table-cell>
				</fo:table-row>
				
				<!-- <fo:table-row keep-with-next="always" height="0.5mm">
					<fo:table-cell border-bottom="0.5 dashed thick black">
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row keep-with-next="always" height="0.5mm">
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			
				<fo:table-row keep-with-next="always">
					<fo:table-cell xsl:use-attribute-sets="cell.settings">
						<fo:block xsl:use-attribute-sets="blk.010">
							<fo:inline font-family="universcbold">CONTRATTO N. </fo:inline>
							<fo:inline xsl:use-attribute-sets="chr.009"><xsl:value-of select="CONTRACT_NO" /></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row> -->
				
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="pad.lr.000" display-align="center">
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
							<xsl:if test="not($bordi='NO')">
								<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="not($color='black')">
								<xsl:attribute name="background-image">url(svg/rectangle_table_color_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
							<xsl:if test="$bordi='NO' and $color='black'">
								<xsl:attribute name="background-image">url(svg/rectangle_table_footer_dettaglio.svg)</xsl:attribute>
							</xsl:if>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row height="1mm">
					<fo:table-cell>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
		</xsl:if>	
		
		<fo:table-body>
			<xsl:if test="not($bordi='NO')">
				<fo:table-row height="3mm">
					<fo:table-cell xsl:use-attribute-sets="cell.settings" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_header.svg)</xsl:attribute>
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
			
			<fo:table-row>
				<fo:table-cell xsl:use-attribute-sets="cell.settings" display-align="center">
					<xsl:if test="not($bordi='NO')">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_body.svg)</xsl:attribute>
					</xsl:if>
					
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(100)"/>
						<fo:table-header>		
							<fo:table-row keep-with-next="always">
								<fo:table-cell>
									<fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									<fo:table-body end-indent="0pt" start-indent="-1pt">
										<fo:table-row>
											<xsl:attribute name="height">6mm</xsl:attribute>
											<!-- <xsl:attribute name="height">
												<xsl:if test="$sezione='ONERI_DIVERSI'">6mm</xsl:if>
												<xsl:if test="not($sezione='ONERI_DIVERSI')">9mm</xsl:if>
											</xsl:attribute> -->
											<fo:table-cell display-align="center" background-repeat="no-repeat"> 
												<xsl:attribute name="background-image">
												<xsl:if test="$sezione='ONERI_DIVERSI'">
													<xsl:value-of select="$svg-sezioni"/>
												</xsl:if>
												<xsl:if test="not($sezione='ONERI_DIVERSI')">
													<xsl:value-of select="$svg-sezioni"/>
												</xsl:if>
												</xsl:attribute>
												<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
												<!-- <xsl:attribute name="color"><xsl:value-of select="$color_titolo_dettaglio"/></xsl:attribute> -->
												<fo:table-body end-indent="0pt" start-indent="0pt">
													<fo:table-row keep-with-next="always">
														<xsl:if test="$sezione='ONERI_DIVERSI'">
															<fo:table-cell xsl:use-attribute-sets="cell.settings">
																<fo:block xsl:use-attribute-sets="blk.004">
																	<fo:inline font-family="universcbold" font-size="8.5pt">ONERI DIVERSI DA QUELLI DOVUTI PER LA FORNITURA DI GAS NATURALE</fo:inline>
																</fo:block>
															</fo:table-cell>
														</xsl:if>
														
														<xsl:if test="not($sezione='ONERI_DIVERSI')">
															<fo:table-cell xsl:use-attribute-sets="cell.settings">
																<fo:block xsl:use-attribute-sets="blk.004" text-align="left">
																	<fo:inline font-family="universcbold" font-size="8.5pt">PRODOTTO/SERVIZIO </fo:inline>
																	<xsl:value-of select="PRODOTTO_SERVIZIO" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell xsl:use-attribute-sets="cell.settings">
																<fo:block xsl:use-attribute-sets="blk.004" text-align="right">
																	<fo:inline font-family="universcbold" font-size="8.5pt">SEDE FORNITURA </fo:inline>
																	<xsl:value-of select="SEDE_OPERATIVA" />
																</fo:block>
															</fo:table-cell>
														</xsl:if>
													</fo:table-row>				
												</fo:table-body>
												</fo:table>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
						
						<fo:table-body end-indent="0pt" start-indent="-1pt">
							<fo:table-row>
								<fo:table-cell>
									<xsl:choose>
										<xsl:when test="$sezione='ONERI_DIVERSI'">
											<xsl:call-template name="ONERI_DIVERSI">
												<xsl:with-param name="ultima_sezione_reti" select="$ultima_sezione_reti"/>
												<xsl:with-param name="ultima_sezione_vendita" select="$ultima_sezione_vendita"/>
												<xsl:with-param name="ultima_sezione_imposte" select="$ultima_sezione_imposte"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="GENERICO">
												<xsl:with-param name="sezione" select="$sezione"/>
												<xsl:with-param name="bordi" select="$bordi"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					
				</fo:table-cell>
			</fo:table-row>
			
			<xsl:if test="not($bordi='NO')">
				<fo:table-row keep-with-previous="always" height="2mm">
					<fo:table-cell xsl:use-attribute-sets="cell.settings" display-align="center">
						<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						<xsl:attribute name="background-image">url(svg/<xsl:value-of select="$bordi"/>_footer.svg)</xsl:attribute>
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
		</fo:table-body>
	</fo:table>
</xsl:template>



<!--
**************************************************************
**															**
**			Template usato per:								**
**				- Contratto Deposito Cauzionale				**
**				- Contratto Servizi Vari					**
**				- Contratto Areaserver						**
**															**
**************************************************************
-->
<xsl:template name="GENERICO">

	<xsl:param name="sezione"/>
	<xsl:param name="bordi"/>
	<xsl:param name="colonne">
		<xsl:choose>
			<xsl:when test="not($sezione='FONIA')">5</xsl:when>
			<xsl:otherwise>7</xsl:otherwise>
		</xsl:choose>
	</xsl:param>
	
	<fo:table font-family="universcbold" font-size="8.5pt" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
		
		<xsl:choose>
			<xsl:when test="not($sezione='FONIA')">
				<fo:table-column column-width="proportional-column-width(9)"/>
				<fo:table-column column-width="proportional-column-width(49)"/>	
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-column column-width="proportional-column-width(9)"/>
				<fo:table-column column-width="proportional-column-width(31)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
				<fo:table-column column-width="proportional-column-width(9)"/>
				<fo:table-column column-width="proportional-column-width(9)"/>
				<fo:table-column column-width="proportional-column-width(14)"/>
			</xsl:otherwise>
		</xsl:choose>
		
		<fo:table-header text-align="center">
			<fo:table-row>
				<fo:table-cell xsl:use-attribute-sets="cell.settings border.right">
					<fo:block>Q.TA'</fo:block>
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings border.right">
					<fo:block>DESCRIZIONE</fo:block>
				</fo:table-cell>
				
				<xsl:if test="$sezione='FONIA'">
					<fo:table-cell xsl:use-attribute-sets="cell.settings border.right">
						<fo:block>DURATA</fo:block>
					</fo:table-cell>
					
					<fo:table-cell xsl:use-attribute-sets="cell.settings border.right">
						<fo:block>DURATA MEDIA</fo:block>
					</fo:table-cell>
				</xsl:if>

				<fo:table-cell number-columns-spanned="2" xsl:use-attribute-sets="cell.settings border.right">
					<fo:block>PERIODO</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block>IMPORTO IN EURO</fo:block>
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row>
				<fo:table-cell xsl:use-attribute-sets="cell.settings border.bottom border.right">
					<fo:block />
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings border.bottom">
					<fo:block />
				</fo:table-cell>
				
				<xsl:if test="$sezione='FONIA'">
					<fo:table-cell xsl:use-attribute-sets="cell.settings border.bottom border.left">
						<fo:block />
					</fo:table-cell>
					
					<fo:table-cell xsl:use-attribute-sets="cell.settings border.bottom border.left">
						<fo:block />
					</fo:table-cell>
				</xsl:if>

				<fo:table-cell xsl:use-attribute-sets="cell.settings border.bottom border.left">
					<fo:block>DAL</fo:block>
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings border.bottom border.left">
					<fo:block>AL</fo:block>
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings border.bottom border.left">
					<fo:block />
				</fo:table-cell>
			</fo:table-row>			
		</fo:table-header>
		
		<fo:table-body>
			<xsl:choose>
				<xsl:when test="$sezione='SERVIZI_VARI'">
					<xsl:for-each select="DETTAGLIO_SERVIZI_VARI">
						<xsl:call-template name="DETT"/>
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="$sezione='DEPOSITO_CAUZIONALE'">
					<xsl:for-each select="DETTAGLIO_DEPOSITO_CAUZIONALE">
						<xsl:call-template name="DETT"/>
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="$sezione='AREASERVER'">
					<xsl:for-each select="DETTAGLIO_AREASERVER">
						<xsl:call-template name="DETT"/>
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="$sezione='FONIA'">
					<xsl:call-template name="DETT_FONIA"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="DETT"/>
				</xsl:otherwise>
			</xsl:choose>

			<fo:table-row keep-with-previous="always" height="2mm">
				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row keep-with-previous="always">
				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block />
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne - 2"/></xsl:attribute>
					<fo:block>TOTALE IMPONIBILE</fo:block>
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block text-align="end">
						<xsl:choose>
							<xsl:when test="TOTALE_IMPONIBILE='0,00'">
								-
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="TOTALE_IMPONIBILE" />
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row keep-with-previous="always" height="1mm">
				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row keep-with-previous="always">
				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block />
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne - 2"/></xsl:attribute>
					<fo:block>Totale IVA</fo:block>
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block text-align="end">
						<xsl:choose>
							<xsl:when test="TOTALE_IVA='0,00'">
								-
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="TOTALE_IVA" />
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row keep-with-previous="always" height="1mm">
				<fo:table-cell xsl:use-attribute-sets="cell.settings border.bottom">
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row keep-with-previous="always" height="2mm">
				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
					<fo:block />
				</fo:table-cell>
			</fo:table-row>

			<fo:table-row keep-with-previous="always">
				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block />
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne - 2"/></xsl:attribute>
					<fo:block>TOTALE IVA INCLUSA</fo:block>
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block text-align="end">
						<xsl:choose>
							<xsl:when test="TOTALE='0,00'">
								-
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="TOTALE" />
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<xsl:if test="$bordi='NO'">
				<fo:table-row keep-with-previous="always" height="1mm">
					<fo:table-cell xsl:use-attribute-sets="cell.settings border.bottom">
						<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
						<fo:block />
					</fo:table-cell>
				</fo:table-row>		
			</xsl:if>
			
		</fo:table-body>
	</fo:table>
</xsl:template>



<!--
**************************************************************
**															**
**			Template usato per:								**
**				- Sezione oneri diversi						**
**															**
**************************************************************
-->
<xsl:template name="ONERI_DIVERSI">
	<xsl:param name="ultima_sezione_reti"/>
	<xsl:param name="ultima_sezione_vendita"/>
	<xsl:param name="ultima_sezione_imposte"/>
	
	<xsl:if test="./child::PERIODO_RIFERIMENTO">
		<fo:table xsl:use-attribute-sets="blk.000" font-family="universc" font-size="8pt" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(85)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-body>
				<xsl:for-each select="./child::PERIODO_RIFERIMENTO/child::PRODOTTO">
					<xsl:if test="not(./child::PREZZO_TOTALE='0,0000' or ./child::PREZZO_TOTALE='0,00')">
						<fo:table-row>
							<fo:table-cell xsl:use-attribute-sets="cell.settings">
								<!--<xsl:if test="child::DESCRIZIONE='Interessi di mora pag. fatt 301166-2010 al tasso medio del 6,04 % per 8 giorni'">
									<xsl:attribute name="background-color">#00ff00</xsl:attribute>
								</xsl:if>
								-->
								<fo:block>
									<fo:inline>
										<xsl:if test="contains(child::DESCRIZIONE,'Consumi e oneri con fatturazione differita')">
											Consumi e oneri con fatturazione differita
										</xsl:if>
										<xsl:if test="not(contains(child::DESCRIZIONE,'Consumi e oneri con fatturazione differita'))">
											<xsl:if test="contains(child::DESCRIZIONE,'Conguaglio fattura precedente')">
												Conguaglio fattura precedente
											</xsl:if>
											<xsl:if test="not(contains(child::DESCRIZIONE,'Conguaglio fattura precedente'))">
												<xsl:value-of select="child::DESCRIZIONE"/>
											</xsl:if>
										</xsl:if>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<xsl:choose>
										<xsl:when test="./child::PREZZO_TOTALE='0,00'">
											<fo:inline>-</fo:inline>
										</xsl:when>
										
										<xsl:otherwise>
											<fo:inline>
												<xsl:value-of select="./child::PREZZO_TOTALE"/>
											</fo:inline>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:if>
				</xsl:for-each>

				<fo:table-row font-family="universcbold" xsl:use-attribute-sets="border.bottom">
					<fo:table-cell xsl:use-attribute-sets="cell.settings">
						<fo:block>
							<fo:inline>Iva su oneri diversi</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./child::IVA_SU_ONERI_DIVERSI='0,00'">
									<fo:inline>-</fo:inline>
								</xsl:when>
								
								<xsl:otherwise>
									<fo:inline>
										<xsl:value-of select="./child::IVA_SU_ONERI_DIVERSI"/>
									</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row font-family="universcbold" keep-with-previous="always">
					<fo:table-cell>
						<fo:block>
							<fo:inline>TOTALE ONERI DIVERSI</fo:inline>
							<xsl:if test="$lettere_sezioni='SI'">
								<fo:inline xsl:use-attribute-sets="condensedcors.008">
									<xsl:choose>
										<xsl:when test="$ultima_sezione_reti='' and
														$ultima_sezione_vendita='' and
														$ultima_sezione_imposte=''">
											(A)
										</xsl:when>
										<xsl:when test="($ultima_sezione_reti='' and
														$ultima_sezione_vendita='' and
														not($ultima_sezione_imposte=''))
														or
														($ultima_sezione_reti='' and
														not($ultima_sezione_vendita='') and
														$ultima_sezione_imposte='')
														or
														(not($ultima_sezione_reti='') and
														$ultima_sezione_vendita='' and
														$ultima_sezione_imposte='')">
											(C)
										</xsl:when>
										<xsl:when test="($ultima_sezione_reti='' and
														not($ultima_sezione_vendita='') and
														not($ultima_sezione_imposte=''))
														or
														(not($ultima_sezione_reti='') and
														not($ultima_sezione_vendita='') and
														$ultima_sezione_imposte='')
														or
														(not($ultima_sezione_reti='') and
														$ultima_sezione_vendita='' and
														not($ultima_sezione_imposte=''))">
											(D)
										</xsl:when>
										<xsl:otherwise>
											(E)
										</xsl:otherwise>
									</xsl:choose>
								</fo:inline>
							</xsl:if>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell>
						<fo:block>
							<xsl:choose>
								<xsl:when test="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_ONERI_DIVERSI='0,00'">
									<fo:inline>-</fo:inline>
								</xsl:when>
								
								<xsl:otherwise>
									<fo:inline>
										<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_ONERI_DIVERSI"/>
									</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<xsl:if test="./ancestor::CONTRATTO_ENERGIA/ancestor::DOCUMENTO/PROMO_SERVIZI[@TIPOLOGIA='IND_QUALITA']">
					<fo:table-row xsl:use-attribute-sets="chr.007 blk.000" keep-with-previous="always">
						<fo:table-cell number-columns-spanned="2">
							<fo:list-block>
								<fo:list-item>
									<fo:list-item-label end-indent="0pt">
										<fo:block>*</fo:block>
									</fo:list-item-label>
									<fo:list-item-body start-indent="5pt">
										<fo:block>Indennizzo automatico per mancato rispetto dei livelli specifici di qualità definiti dall'Autorità per l'energia elettrica e il gas.</fo:block>
										<fo:block>La corresponsione dell'indennizzo automatico non esclude la possibilità per il richiedente di richiedere nelle opportune sedi il risarcimento dell'eventuale danno ulteriore subito</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-body>
		</fo:table>
	</xsl:if>
</xsl:template>



<!--
**************************************************************
**															**
**			Template usato per:								**
**				- For-each del template "GENERICO"			**
**															**
**************************************************************
-->
<xsl:template name="DETT">
	<fo:table-row font-family="universc" font-size="8pt" xsl:use-attribute-sets="blk.004" text-align="center" display-align="center">
		<xsl:if test="(position() = last())">
			<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
		</xsl:if>
		<fo:table-cell xsl:use-attribute-sets="cell.settings">
			<fo:block text-align="left">
				<xsl:value-of select="QUANTITA" />
			</fo:block>
		</fo:table-cell>

		<fo:table-cell xsl:use-attribute-sets="cell.settings">
			<fo:block>
				<xsl:value-of select="DESCRIZIONE" />
			</fo:block>
		</fo:table-cell>

		<fo:table-cell xsl:use-attribute-sets="cell.settings">
			<fo:block>
				<xsl:value-of select="DATA_INIZIO" />
			</fo:block>
		</fo:table-cell>

		<fo:table-cell xsl:use-attribute-sets="cell.settings">
			<fo:block>
				<xsl:value-of select="DATA_FINE" />
			</fo:block>
		</fo:table-cell>

		<fo:table-cell xsl:use-attribute-sets="cell.settings">
			<fo:block text-align="end">
				<xsl:choose>
					<xsl:when test="PREZZO_TOTALE='0,00'">
						-
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="PREZZO_TOTALE" />
					</xsl:otherwise>
				</xsl:choose>
			</fo:block>
		</fo:table-cell>
	</fo:table-row>
</xsl:template>



<!--
**************************************************************
**															**
**			Template usato per:								**
**				- Contratto Fonia							**
**															**
**************************************************************
-->
<xsl:template name="DETT_FONIA">
	<xsl:for-each select="RIEPILOGO_LINEA_TELEFONICA">				
		<fo:table-row font-family="universcbold" font-size="8pt">
			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block />
			</fo:table-cell>
			
			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block>Linea</fo:block>
			</fo:table-cell>
			
			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block><xsl:value-of select="LINEA_TELEFONICA" /></fo:block>
			</fo:table-cell>
			
			<fo:table-cell xsl:use-attribute-sets="cell.settings"
					number-columns-spanned="4">
				<fo:block />
			</fo:table-cell>
		</fo:table-row>

		<xsl:for-each select="RIEPILOGO_CHIAMATE">
			<fo:table-row font-family="universc" font-size="8pt" text-align="center">
				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block>
						<xsl:value-of select="QUANTITA" />
					</fo:block>
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block text-align="left">
						<xsl:value-of select="DESCRIZIONE" />
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block text-align="left">
						<xsl:value-of select="DURATA/MINUTI" />
						min.
						<xsl:value-of select="DURATA/SECONDI" />
						sec.
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block text-align="left">
						<xsl:value-of select="DURATA_MEDIA/MINUTI" />
						min.
						<xsl:value-of select="DURATA_MEDIA/SECONDI" />
						sec.
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block>
						<xsl:value-of select="DATA_INIZIO" />
					</fo:block>
				</fo:table-cell>

				<fo:table-cell>
					<fo:block>
						<xsl:value-of select="DATA_FINE" />
					</fo:block>
				</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="cell.settings">
					<fo:block text-align="end">
						<xsl:choose>
							<xsl:when test="PREZZO_TOTALE='0,00'">
								-
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="PREZZO_TOTALE" />
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:for-each>
	</xsl:for-each>

	<xsl:if test="SCONTI_TELEFONIA">
		<fo:table-row font-family="universc" font-size="8pt" keep-with-previous="always" height="2mm">
			<fo:table-cell xsl:use-attribute-sets="cell.settings"
				number-columns-spanned="7">
				<fo:block />
			</fo:table-cell>
		</fo:table-row>
	</xsl:if>

	<xsl:for-each select="SCONTI_TELEFONIA">
		<fo:table-row font-family="universc" font-size="8pt" text-align="center">
			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block />
			</fo:table-cell>

			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block text-align="left">
					<xsl:value-of select="DESCRIZIONE" />
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell xsl:use-attribute-sets="cell.settings"
					number-columns-spanned="2">
				<fo:block />
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<xsl:value-of select="DATA_INIZIO" />
				</fo:block>
			</fo:table-cell>

			<fo:table-cell>
				<fo:block>
					<xsl:value-of select="DATA_FINE" />
				</fo:block>
			</fo:table-cell>

			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block text-align="end">
					<xsl:choose>
						<xsl:when test="PREZZO_TOTALE='0,00'">
							-
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="PREZZO_TOTALE" />
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:for-each>

	<xsl:if test="CANONI_MENSILI">
		<fo:table-row font-family="universc" font-size="8pt" keep-with-previous="always" height="2mm">
			<fo:table-cell xsl:use-attribute-sets="cell.settings"
				number-columns-spanned="7">
				<fo:block />
			</fo:table-cell>
		</fo:table-row>
	</xsl:if>

	<xsl:for-each select="CANONI_MENSILI">
		<fo:table-row font-family="universc" font-size="8pt" text-align="center">
			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block />
			</fo:table-cell>

			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block text-align="left">
					<xsl:value-of select="DESCRIZIONE" />
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell xsl:use-attribute-sets="cell.settings"
					number-columns-spanned="2">
				<fo:block />
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<xsl:value-of select="DATA_INIZIO" />
				</fo:block>
			</fo:table-cell>

			<fo:table-cell>
				<fo:block>
					<xsl:value-of select="DATA_FINE" />
				</fo:block>
			</fo:table-cell>

			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block text-align="end">
					<xsl:choose>
						<xsl:when test="PREZZO_TOTALE='0,00'">
							-
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="PREZZO_TOTALE" />
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:for-each>

	<xsl:if test="INTERESSI_MORA">
		<fo:table-row font-family="universc" font-size="8pt" keep-with-previous="always" height="2mm">
			<fo:table-cell xsl:use-attribute-sets="cell.settings"
				number-columns-spanned="7">
				<fo:block />
			</fo:table-cell>
		</fo:table-row>
	</xsl:if>

	<xsl:for-each select="INTERESSI_MORA">
		<fo:table-row font-family="universc" font-size="8pt">
			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block />
			</fo:table-cell>

			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block text-align="left">
					<xsl:value-of select="DESCRIZIONE" />
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell xsl:use-attribute-sets="cell.settings"
					number-columns-spanned="4">
				<fo:block />
			</fo:table-cell>

			<fo:table-cell xsl:use-attribute-sets="cell.settings">
				<fo:block text-align="end">
					<xsl:choose>
						<xsl:when test="PREZZO_TOTALE='0,00'">
							-
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="PREZZO_TOTALE" />
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:for-each>
</xsl:template>



<xsl:attribute-set name="cell.settings">
	<xsl:attribute name="padding-left">3mm</xsl:attribute>
	<xsl:attribute name="padding-right">3mm</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="border">
	<xsl:attribute name="border">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="border.left">
	<xsl:attribute name="border-left">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="border.right">
	<xsl:attribute name="border-right">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="border.top">
	<xsl:attribute name="border-top">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="border.bottom">
	<xsl:attribute name="border-bottom">0.5pt solid black</xsl:attribute>
</xsl:attribute-set>

</xsl:stylesheet>


