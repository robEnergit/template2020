<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">
<xsl:output encoding="UTF-8"/>

<!--
*******************************************************
**                  Header sezione                   **
*******************************************************
-->
<xsl:template name="Header_sezione_dettaglio_012011">
	<xsl:param name="path"/>
	<xsl:param name="titolo"/>
	<xsl:param name="header"/>
	<xsl:param name="colonne"/>
	<xsl:param name="color"/>
	<xsl:param name="macro_sezione"/>
	<xsl:if test="not($titolo='no_title') or $header='SI'">
		<fo:table-header>
			<xsl:if test="not($titolo='no_title')">
				<fo:table-row keep-with-next="always">
					<xsl:attribute name="color"><xsl:value-of select="$color"/></xsl:attribute>
					<fo:table-cell>
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
						<fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(100)"/>
						<fo:table-body end-indent="0pt" start-indent="-1pt">
							<fo:table-row height="6mm">
								<fo:table-cell display-align="center" background-repeat="no-repeat"> 
									<xsl:attribute name="background-image"><xsl:value-of select="$path"/></xsl:attribute>
									<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<fo:table-row keep-with-next="always">
											<fo:table-cell xsl:use-attribute-sets="pad.l.000">
												<fo:block xsl:use-attribute-sets="blk.004">
													<xsl:if test="$macro_sezione='SI'">
														<fo:inline font-family="universcbold" font-size="9.5pt"><xsl:value-of select="$titolo"/></fo:inline>
													</xsl:if>
													<xsl:if test="$macro_sezione='NO'">
														<fo:inline font-family="universcbold" font-size="8.5pt"><xsl:value-of select="$titolo"/></fo:inline>
													</xsl:if>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>				
									</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
			
			<!--
			**********************************************************
			** Colonne di default per un numero di colonne pari a 5 **
			**********************************************************
			-->
			<xsl:if test="$header='SI'">		
				<fo:table-row keep-with-next="always" height="4mm" xsl:use-attribute-sets="blk.002 condensedcors.008">
					<fo:table-cell xsl:use-attribute-sets="pad.l.000">
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Componente</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline>Scaglione</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Unità di misura</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block >
							<fo:inline>Corrispettivo unitario</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Quantità</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Totale (euro)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline>Aliquota</fo:inline>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
				
			</xsl:if>
		</fo:table-header>
	</xsl:if>
</xsl:template>
<xsl:template name="Header_sezione_dettaglio_012011_">
	<xsl:param name="path"/>
	<xsl:param name="titolo"/>
	<xsl:param name="header"/>
	<xsl:param name="colonne"/>
	<xsl:param name="color"/>
	<xsl:param name="macro_sezione"/>
	<xsl:if test="not($titolo='no_title') or $header='SI'">
		<fo:table-header>
			<xsl:if test="not($titolo='no_title')">
				<fo:table-row keep-with-next="always">
					<xsl:attribute name="color"><xsl:value-of select="$color"/></xsl:attribute>
					<fo:table-cell>
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
						<fo:table space-before="3mm" end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(100)"/>
						<fo:table-body end-indent="0pt" start-indent="-1pt">
							<fo:table-row height="6mm">
								<fo:table-cell display-align="center" background-repeat="no-repeat"> 
									<xsl:attribute name="background-image"><xsl:value-of select="$path"/></xsl:attribute>
									<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<fo:table-column column-width="proportional-column-width(100)"/>
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<fo:table-row keep-with-next="always">
											<fo:table-cell xsl:use-attribute-sets="pad.l.000">
												<fo:block xsl:use-attribute-sets="blk.004">
													<xsl:if test="$macro_sezione='SI'">
														<fo:inline font-family="universcbold" font-size="9.5pt"><xsl:value-of select="$titolo"/></fo:inline>
													</xsl:if>
													<xsl:if test="$macro_sezione='NO'">
														<fo:inline font-family="universcbold" font-size="8.5pt"><xsl:value-of select="$titolo"/></fo:inline>
													</xsl:if>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>				
									</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</xsl:if>
			
			<!--
			**********************************************************
			** Colonne di default per un numero di colonne pari a 5 **
			**********************************************************
			-->
			<xsl:if test="$header='SI'">		
				<fo:table-row keep-with-next="always" height="4mm" xsl:use-attribute-sets="blk.002 condensedcors.008">
					<fo:table-cell xsl:use-attribute-sets="pad.l.000">
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Componente</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline>Scaglione</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Unità di misura</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
					<!--
						<fo:block>
							<fo:inline>Corrispettivo unitario</fo:inline>
						</fo:block>
						-->
						<fo:table table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(50)"/> 
						<fo:table-column column-width="proportional-column-width(50)"/>
						<fo:table-body>
						<fo:table-row height="3mm">
							<fo:table-cell>
								<fo:block>
									<fo:inline>Prezzo di listino*</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
							<fo:block>
								<fo:inline>Prezzo fatturato*</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						</fo:table-body>
						</fo:table>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline>Quantità</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block >
							<fo:inline>Totale (euro)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline>Aliquota IVA</fo:inline>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
				<!--
				<fo:table-row space-before="2mm" keep-with-next="always" height="4mm" xsl:use-attribute-sets="blk.002 condensedcors.008">
					<fo:table-cell xsl:use-attribute-sets="pad.l.000">
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-size="7pt">
						<fo:table table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(50)"/> 
						<fo:table-column column-width="proportional-column-width(50)"/>
						<fo:table-body>
						<fo:table-row height="3mm">
							<fo:table-cell>
								<fo:block font-size="5pt">
									<fo:inline>Prezzo a Pcs Standard</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
							<fo:block font-size="5pt">
								<fo:inline>Prezzo a Pcs Specifico</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						</fo:table-body>
						</fo:table>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline></fo:inline>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
				-->
			</xsl:if>
		</fo:table-header>
	</xsl:if>
</xsl:template>



<!--
*******************************************************
**                   Body sezione                    **
*******************************************************
-->
<xsl:template name="Sezione_dettaglio_generica_012011">
	<xsl:param name="nome_sezione"/>
	<xsl:param name="tipo_quota"/>
	<xsl:param name="last_section"/>
	<xsl:param name="info_totale"/>
	<xsl:param name="lines"/>
	<xsl:param name="ultima_sezione_reti"/>
	<xsl:param name="ultima_sezione_vendita"/>
	<xsl:param name="ultima_sezione_imposte"/>
	<xsl:param name="titolo"/>
	
	<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$nome_sezione]">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.002 chr.008">
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(19)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(9)"/>
			<fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(9)"/>
			<fo:table-column column-width="proportional-column-width(6)"/>
			<fo:table-header>
				<xsl:if test="not($nome_sezione='')">
					<fo:table-row keep-with-next="always">
						<fo:table-cell xsl:use-attribute-sets="pad.l.000">
							<fo:block>
								<fo:inline xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="$tipo_quota"/></fo:inline>
							</fo:block>
						</fo:table-cell>
						
						<fo:table-cell number-columns-spanned="7"> <!-- era 6 -->
							<fo:block>
								<fo:inline xsl:use-attribute-sets="condensedcors.008"></fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-header>
			
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<xsl:for-each select="./child::SEZIONE[@TIPOLOGIA=$nome_sezione]">
					<xsl:for-each select="./child::PERIODO_RIFERIMENTO">
						<fo:table-row>
							<fo:table-cell display-align="center" xsl:use-attribute-sets="pad.l.000">
								<fo:block>
									<fo:inline>Dal <xsl:value-of select="./child::DAL"/> al <xsl:value-of select="./child::AL"/>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="7">
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<!--<fo:table-column column-width="proportional-column-width(24)"/>-->
									<fo:table-column column-width="proportional-column-width(19)"/>
									<fo:table-column column-width="proportional-column-width(15)"/>
									<fo:table-column column-width="proportional-column-width(11)"/>
									<fo:table-column column-width="proportional-column-width(9)"/>
									<fo:table-column column-width="proportional-column-width(11)"/>
									<fo:table-column column-width="proportional-column-width(9)"/>
									<fo:table-column column-width="proportional-column-width(6)"/>
									<fo:table-body end-indent="0pt" start-indent="0pt">
									
									
									
										<xsl:for-each select="./child::PRODOTTO">
											<fo:table-row keep-with-next="always">
											
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:if test="contains(./child::SCAGLIONE,'Prec. Fatt.')">
																Prec. Fatturato
															</xsl:if>
															<xsl:value-of select="./child::DESCRIZIONE"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell>
													
													<fo:block>
														<fo:inline>Unico</fo:inline>
													</fo:block>
												</fo:table-cell>
													
												
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="./child::UNITA_DI_MISURA"/>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell>
												
													<fo:block>
														<fo:inline><xsl:value-of select="./child::CORRISPETTIVO_UNITARIO"/></fo:inline>
													</fo:block>
													
													
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:choose>
																<xsl:when test="./child::UNITA_DI_MISURA='Mese' or $nome_sezione='QUOTA_FISSA_EB'">
																	<xsl:choose>
																		<xsl:when test="./child::QUANTITA>1">
																			<xsl:value-of select="./child::QUANTITA"/>
																			Mesi
																		</xsl:when>
																		
																		<xsl:otherwise>
																			<xsl:value-of select="./child::QUANTITA"/>
																			<!--Mese-->
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:when>
																
																<xsl:otherwise>
																	<xsl:value-of select="./child::QUANTITA"/>
																	<!--<xsl:value-of select="./child::UNITA_DI_MISURA"/>-->
																</xsl:otherwise>
															</xsl:choose>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::PREZZO_TOTALE"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::ALIQUOTA_IVA"/>&#160;%
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												
											</fo:table-row>
										</xsl:for-each>
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
						</fo:table-row>
						
						
						<!-- <xsl:if test="position()&lt;last()"> -->
						
						
						<xsl:if test="((position()&lt;last()) or (./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO and not(./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,00' or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,000000'))) and $lines='SI'">
							<fo:table-row height="1mm">
								<fo:table-cell>
									<fo:block></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-right="12mm" number-columns-spanned="7">	<!-- era 5 -->
									<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
										<fo:table-column column-width="proportional-column-width(100)"/>
										<fo:table-body end-indent="0pt" start-indent="0pt">
											<fo:table-row height="1mm">	
												<fo:table-cell border-bottom="0.25pt dashed black">
													<fo:block></fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row height="1mm">
								<fo:table-cell number-columns-spanned="7"> <!--era 7 -->
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:if> 
						<!-- </xsl:if> -->
						
						
					</xsl:for-each>
				
					<xsl:call-template name="Chiudi_sezione_012011">
						<xsl:with-param name="lines_flag" select="$lines"/>
						<xsl:with-param name="colonne" select="8"/>
						<xsl:with-param name="nome_sezione" select="$nome_sezione"/>
					</xsl:call-template>

					<xsl:choose>
						<xsl:when test="$info_totale='VENDITA'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="brd.b.000">	<!-- era 6 -->
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="6">
										<fo:block>
											<fo:inline>TOTALE <xsl:value-of select="$titolo"/></fo:inline>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													(A)
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA"/>
										</fo:block>
									</fo:table-cell>
									
								</fo:table-row>
								
							</xsl:if>
						</xsl:when>
						
						<xsl:when test="$info_totale='USO RETI'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="brd.b.000">	<!-- era 6 -->
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="6">
										<fo:block>
											TOTALE <xsl:value-of select="$titolo"/>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_vendita=''">
															(A)
														</xsl:when>
														<xsl:otherwise>
															(B)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
							</xsl:if>
						</xsl:when>
						
						<xsl:when test="$info_totale='IMPOSTE'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell number-columns-spanned="8" xsl:use-attribute-sets="brd.b.000"> <!-- era 6 -->
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="6">			<!-- era 5 -->
										<fo:block>
											TOTALE <xsl:value-of select="$titolo"/>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_reti='' and
																		$ultima_sezione_vendita=''">
															(A)
														</xsl:when>
														<xsl:when test="$ultima_sezione_reti='' or
																		$ultima_sezione_vendita=''">
															(B)
														</xsl:when>
														<xsl:otherwise>
															(C)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
					
					
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:if>
</xsl:template>

<xsl:template name="Sezione_dettaglio_generica_012011_">
	<xsl:param name="nome_sezione"/>
	<xsl:param name="tipo_quota"/>
	<xsl:param name="last_section"/>
	<xsl:param name="info_totale"/>
	<xsl:param name="lines"/>
	<xsl:param name="ultima_sezione_reti"/>
	<xsl:param name="ultima_sezione_vendita"/>
	<xsl:param name="ultima_sezione_imposte"/>
	<xsl:param name="titolo"/>
	
	<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$nome_sezione]">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.002 chr.008">
			
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(19)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(9)"/>
			<fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(9)"/>
			<fo:table-column column-width="proportional-column-width(6)"/>
			<fo:table-header>
				<xsl:if test="not($nome_sezione='')">
					<fo:table-row keep-with-next="always">
						<fo:table-cell xsl:use-attribute-sets="pad.l.000">
							<fo:block>
								<fo:inline xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="$tipo_quota"/></fo:inline>
							</fo:block>
						</fo:table-cell>
						
						<fo:table-cell number-columns-spanned="7"> <!-- era 6 -->
							<fo:block>
								<fo:inline xsl:use-attribute-sets="condensedcors.008"></fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-header>
			
			
			
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<xsl:for-each select="./child::SEZIONE[@TIPOLOGIA=$nome_sezione]">
					<xsl:for-each select="./child::PERIODO_RIFERIMENTO">
						<fo:table-row>
							<fo:table-cell display-align="center" xsl:use-attribute-sets="pad.l.000">
								<fo:block>
									<fo:inline>Dal <xsl:value-of select="./child::DAL"/> al <xsl:value-of select="./child::AL"/>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="7">
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<!--<fo:table-column column-width="proportional-column-width(24)"/>-->
									<fo:table-column column-width="proportional-column-width(19)"/>
									<fo:table-column column-width="proportional-column-width(15)"/>
									<fo:table-column column-width="proportional-column-width(11)"/>
									<fo:table-column column-width="proportional-column-width(9)"/>
									<fo:table-column column-width="proportional-column-width(11)"/>
									<fo:table-column column-width="proportional-column-width(9)"/>
									<fo:table-column column-width="proportional-column-width(6)"/>
									
									<fo:table-body end-indent="0pt" start-indent="0pt">
																			
										<xsl:for-each select="./child::PRODOTTO">
											<fo:table-row keep-with-next="always" >
											
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:if test="contains(./child::SCAGLIONE,'Prec. Fatt.')">
																Prec. Fatturato
															</xsl:if>
															<xsl:value-of select="./child::DESCRIZIONE"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell>
													
													<fo:block>
														<fo:inline>
															<xsl:if test="not(contains(./child::SCAGLIONE,'Prec. Fatt.'))">
																<xsl:value-of select="./child::SCAGLIONE"/>
															</xsl:if>
														</fo:inline>
													</fo:block>
													
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::UNITA_DI_MISURA"/>
														</fo:inline>
													</fo:block>
													
													
													
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::CORRISPETTIVO_UNITARIO"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												<!--
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::CORRISPETTIVO_UNITARIO_2"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>		
												-->
													
													
												
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::QUANTITA"/>
														</fo:inline>
													</fo:block>
												
												</fo:table-cell>
												
												<fo:table-cell>
												
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::PREZZO_TOTALE"/>
														</fo:inline>
													</fo:block>
													
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:value-of select="./child::ALIQUOTA_IVA"/>
														</fo:inline>
													</fo:block>
												
												
												</fo:table-cell>
												
											</fo:table-row>
										</xsl:for-each>
										
									</fo:table-body>
									
								</fo:table>
							</fo:table-cell>
						</fo:table-row>
						
						
						<!-- <xsl:if test="position()&lt;last()"> -->
						
						
						<xsl:if test="((position()&lt;last()) or (./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO and not(./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,00' or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,000000'))) and $lines='NO'">
							<fo:table-row height="1mm">
								<fo:table-cell>
									<fo:block></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-right="12mm" number-columns-spanned="7">	<!-- era 5 -->
									<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
										<fo:table-column column-width="proportional-column-width(100)"/>
										<fo:table-body end-indent="0pt" start-indent="0pt">
											<fo:table-row height="1mm">	
												<fo:table-cell border-bottom="0.25pt dashed black">
													<fo:block></fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row height="1mm">
								<fo:table-cell number-columns-spanned="7"> <!--era 7 -->
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:if> 
						<!-- </xsl:if> -->
						
						
					</xsl:for-each>
					
					<xsl:call-template name="Chiudi_sezione_012011">
						<xsl:with-param name="lines_flag" select="$lines"/>
						<xsl:with-param name="colonne" select="8"/>
						<xsl:with-param name="nome_sezione" select="$nome_sezione"/>
					</xsl:call-template>
					
					
					
					<xsl:choose>
						<xsl:when test="$info_totale='VENDITA'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="brd.b.000">	<!-- era 6 -->
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="6">
										<fo:block>
											<fo:inline>TOTALE <xsl:value-of select="$titolo"/></fo:inline>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													(A)
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA"/>
										</fo:block>
									</fo:table-cell>
									
								</fo:table-row>
								
							</xsl:if>
						</xsl:when>
						
						<xsl:when test="$info_totale='USO RETI'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="brd.b.000">	<!-- era 6 -->
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="6">
										<fo:block>
											TOTALE <xsl:value-of select="$titolo"/>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_vendita=''">
															(A)
														</xsl:when>
														<xsl:otherwise>
															(B)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
							</xsl:if>
						</xsl:when>
						
						<xsl:when test="$info_totale='IMPOSTE'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell number-columns-spanned="8" xsl:use-attribute-sets="brd.b.000"> <!-- era 6 -->
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="6">			<!-- era 5 -->
										<fo:block>
											TOTALE <xsl:value-of select="$titolo"/>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_reti='' and
																		$ultima_sezione_vendita=''">
															(A)
														</xsl:when>
														<xsl:when test="$ultima_sezione_reti='' or
																		$ultima_sezione_vendita=''">
															(B)
														</xsl:when>
														<xsl:otherwise>
															(C)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
					
					
				</xsl:for-each>
				
				
			</fo:table-body>
		</fo:table>
	</xsl:if>
</xsl:template>
<xsl:template name="Sezione_dettaglio_generica_012011_ConScaglioniMaAdUnPrezzo">
	<xsl:param name="nome_sezione"/>
	<xsl:param name="tipo_quota"/>
	<xsl:param name="last_section"/>
	<xsl:param name="info_totale"/>
	<xsl:param name="lines"/>
	<xsl:param name="ultima_sezione_reti"/>
	<xsl:param name="ultima_sezione_vendita"/>
	<xsl:param name="ultima_sezione_imposte"/>
	<xsl:param name="titolo"/>
	
	<xsl:if test="./child::SEZIONE[@TIPOLOGIA=$nome_sezione]">
		<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%" xsl:use-attribute-sets="blk.002 chr.008">
			
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(19)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(9)"/>
			<fo:table-column column-width="proportional-column-width(11)"/>
			<fo:table-column column-width="proportional-column-width(9)"/>
			<fo:table-column column-width="proportional-column-width(6)"/>
			<fo:table-header>
				<xsl:if test="not($nome_sezione='')">
					<fo:table-row keep-with-next="always">
						<fo:table-cell xsl:use-attribute-sets="pad.l.000">
							<fo:block>
								<fo:inline xsl:use-attribute-sets="chrbold.008"><xsl:value-of select="$tipo_quota"/></fo:inline>
							</fo:block>
						</fo:table-cell>
						
						<fo:table-cell number-columns-spanned="7"> <!-- era 6 -->
							<fo:block>
								<fo:inline xsl:use-attribute-sets="condensedcors.008"></fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
			</fo:table-header>
			
			<fo:table-body end-indent="0pt" start-indent="0pt">
				<xsl:for-each select="./child::SEZIONE[@TIPOLOGIA=$nome_sezione]">
					<xsl:for-each select="./child::PERIODO_RIFERIMENTO">
						<fo:table-row>
							<fo:table-cell display-align="center" xsl:use-attribute-sets="pad.l.000">
								<fo:block>
									<fo:inline>Dal <xsl:value-of select="./child::DAL"/> al <xsl:value-of select="./child::AL"/>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							
							<fo:table-cell number-columns-spanned="7">
								<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
									<!--<fo:table-column column-width="proportional-column-width(24)"/>-->
									<fo:table-column column-width="proportional-column-width(19)"/>
									<fo:table-column column-width="proportional-column-width(15)"/>
									<fo:table-column column-width="proportional-column-width(11)"/>
									<fo:table-column column-width="proportional-column-width(9)"/>
									<fo:table-column column-width="proportional-column-width(11)"/>
									<fo:table-column column-width="proportional-column-width(9)"/>
									<fo:table-column column-width="proportional-column-width(6)"/>
									<fo:table-body end-indent="0pt" start-indent="0pt">
										<xsl:for-each select="./child::PRODOTTO">
											<fo:table-row keep-with-next="always" >
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:if test="contains(./child::SCAGLIONE,'Prec. Fatt.')">
																Prec. Fatturato
															</xsl:if>
															<xsl:value-of select="./child::DESCRIZIONE"/>
															<xsl:if test="./child::DESCRIZIONE='Materia prima gas'">
																<fo:inline>*</fo:inline>
															</xsl:if>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline>
															<xsl:if test="not(contains(./child::SCAGLIONE,'Prec. Fatt.'))">
																<xsl:value-of select="./child::SCAGLIONE"/>
															</xsl:if>
														</fo:inline>
													</fo:block>
													
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline><xsl:value-of select="./child::UNITA_DI_MISURA"/></fo:inline>
													</fo:block>
													
													
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline><xsl:value-of select="./child::CORRISPETTIVO_UNITARIO"/></fo:inline>
													</fo:block>

													
												</fo:table-cell>
												
												<fo:table-cell>
												<fo:block>
													<fo:inline><xsl:value-of select="./child::QUANTITA"/></fo:inline>
												</fo:block>
												
												</fo:table-cell>
												
												<fo:table-cell>
												<fo:block>
													<fo:inline><xsl:value-of select="./child::PREZZO_TOTALE"/></fo:inline>
												</fo:block>
												
												</fo:table-cell>
												
												<fo:table-cell>
													<fo:block>
														<fo:inline><xsl:value-of select="./child::ALIQUOTA_IVA"/>&#160;%</fo:inline>
													</fo:block>
													
												</fo:table-cell>
											
											</fo:table-row>
											
										
										</xsl:for-each>
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
						</fo:table-row>
						
						
						<!-- <xsl:if test="position()&lt;last()"> -->
						
						
						<xsl:if test="((position()&lt;last()) or (./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO and not(./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,00' or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./ancestor::SEZIONE/child::PRECEDENTEMENTE_FATTURATO='0,000000'))) and $lines='NO'">
							<fo:table-row height="1mm">
								<fo:table-cell>
									<fo:block></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-right="12mm" number-columns-spanned="7">	<!-- era 5 -->
									<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
										<fo:table-column column-width="proportional-column-width(100)"/>
										<fo:table-body end-indent="0pt" start-indent="0pt">
											<fo:table-row height="1mm">	
												<fo:table-cell border-bottom="0.25pt dashed black">
													<fo:block></fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row height="1mm">
								<fo:table-cell number-columns-spanned="7"> <!--era 7 -->
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:if> 
						<!-- </xsl:if> -->
						
						
					</xsl:for-each>
					
					<xsl:call-template name="Chiudi_sezione_012011">
						<xsl:with-param name="lines_flag" select="$lines"/>
						<xsl:with-param name="colonne" select="8"/>
						<xsl:with-param name="nome_sezione" select="$nome_sezione"/>
					</xsl:call-template>
					
					
					
					<xsl:choose>
						<xsl:when test="$info_totale='VENDITA'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell number-columns-spanned="8" xsl:use-attribute-sets="brd.b.000">	<!-- era 6 -->
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="6">
										<fo:block>
											<fo:inline>TOTALE <xsl:value-of select="$titolo"/></fo:inline>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													(A)
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell number-columns-spanned="2">
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_VENDITA"/>
										</fo:block>
									</fo:table-cell>
									
								</fo:table-row>
								
							</xsl:if>
						</xsl:when>
						
						<xsl:when test="$info_totale='USO RETI'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell number-columns-spanned="8" xsl:use-attribute-sets="brd.b.000">	<!-- era 6 -->
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="6">
										<fo:block>
											TOTALE <xsl:value-of select="$titolo"/>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_vendita=''">
															(A)
														</xsl:when>
														<xsl:otherwise>
															(B)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell number-columns-spanned="2">
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_SERVIZI_DI_RETE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
							</xsl:if>
						</xsl:when>
						
						<xsl:when test="$info_totale='IMPOSTE'">
							<xsl:if test="$last_section=$nome_sezione">
								<fo:table-row height="1mm" keep-with-previous="always">
									<fo:table-cell number-columns-spanned="8" xsl:use-attribute-sets="brd.b.000"> <!-- era 6 -->
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.004 chrbold.008">
									<fo:table-cell number-columns-spanned="6">			<!-- era 5 -->
										<fo:block>
											TOTALE <xsl:value-of select="$titolo"/>
											<xsl:if test="$lettere_sezioni='SI'">
												<fo:inline xsl:use-attribute-sets="condensedcors.008">
													<xsl:choose>
														<xsl:when test="$ultima_sezione_reti='' and
																		$ultima_sezione_vendita=''">
															(A)
														</xsl:when>
														<xsl:when test="$ultima_sezione_reti='' or
																		$ultima_sezione_vendita=''">
															(B)
														</xsl:when>
														<xsl:otherwise>
															(C)
														</xsl:otherwise>
													</xsl:choose>
												</fo:inline>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell number-columns-spanned="2">
										<fo:block>
											<xsl:value-of select="./ancestor::CONTRATTO_GAS/child::SINTESI_GAS/child::TOTALE_IMPOSTE"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
					
					
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:if>
</xsl:template>


<xsl:template name="Chiudi_sezione_012011">
	<xsl:param name="lines_flag"/>
	<xsl:param name="colonne"/>
	<xsl:param name="nome_sezione"/>
	
	
	<xsl:if test="./child::PRECEDENTEMENTE_FATTURATO and not(./child::PRECEDENTEMENTE_FATTURATO='0,00' or ./child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./child::PRECEDENTEMENTE_FATTURATO='0,000000')">
		<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.002 chr.008">
			<fo:table-cell xsl:use-attribute-sets="pad.l.000">
				<fo:block>
					<fo:inline>Precedentemente fatturato</fo:inline>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/>-3</xsl:attribute>
				<fo:block></fo:block>
			</fo:table-cell>
			
			<fo:table-cell>
				<!--
					******** QUI IL CODICE PER SPLITTAMENTO DEL PRECEDENTE FATTURATO **** 
				
				
				-->
				<fo:block>
					<xsl:choose>
						<xsl:when test="./child::PRECEDENTEMENTE_FATTURATO='0,00' or ./child::PRECEDENTEMENTE_FATTURATO='0,0000' or ./child::PRECEDENTEMENTE_FATTURATO='0,000000'">
							<fo:inline>-</fo:inline>
						</xsl:when>
						
						<xsl:otherwise>
							<fo:inline>
								<xsl:value-of select="./child::PRECEDENTEMENTE_FATTURATO"/>
							</fo:inline>
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
				
			</fo:table-cell>
			
			
		</fo:table-row>
	</xsl:if>
	
	<xsl:if test="not($nome_sezione='IMPOSTE')">
		<xsl:if test="$lines_flag='SI'">
			<fo:table-row keep-with-previous="always" height="1mm">
				<fo:table-cell xsl:use-attribute-sets="pad.lr.000">
					<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
					<fo:table end-indent="0pt" start-indent="0pt" table-layout="fixed" width="100%">
						<fo:table-column column-width="proportional-column-width(100)"/>
						<fo:table-body end-indent="0pt" start-indent="0pt">
							<fo:table-row height="1mm">	
								<fo:table-cell border-bottom="0.25pt dashed black">
									<fo:block/>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
		
		<!-- totale servizi di rete -->
		<fo:table-row keep-with-previous="always" xsl:use-attribute-sets="blk.002 chrbold.008">
			<fo:table-cell xsl:use-attribute-sets="pad.l.000">
				<fo:block>
					Totale
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell number-columns-spanned="8">	
				<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/>-3</xsl:attribute>
				<fo:block/>
			</fo:table-cell>
			
			<fo:table-cell>
				<fo:block>
					<xsl:value-of select="./child::TOTALE_SEZIONE"/> <!--RIEPILOGO_TOTALE_SEZIONE -->
				</fo:block>
			</fo:table-cell>
			
			
		</fo:table-row>
		
		
		<fo:table-row keep-with-previous="always" height="1.5mm">
			<fo:table-cell number-columns-spanned="7">	
				<xsl:attribute name="number-columns-spanned"><xsl:value-of select="$colonne"/></xsl:attribute>
				<fo:block/>
			</fo:table-cell>
		</fo:table-row>
				
	</xsl:if>
	
</xsl:template>


</xsl:stylesheet>